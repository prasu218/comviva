import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SuccessComponent } from '../../../src/app/mainpages/success';
import { BuybundlessuccessComponent } from '../mainpages/buybundlessuccess/buybundlessuccess.component';
import { BuybundlesfailureComponent } from '../mainpages/buybundlesfailure/buybundlesfailure.component';
import { FailureComponent } from '../mainpages/failure/failure.component';
import { DashboardComponent } from 'src/app/mainpages/dashboard/dashboard.component';
import { AuthGuard } from '../auth/auth.guard';
import { dashboardmenuroutes } from '../mainpages/dashboard/dashboard-submenu/dashboard-submenu.routing';
import { LoginComponent } from '../mainpages/login/login.component';
import { Loginmenuroutes } from '../mainpages/login/login-submenu/login-submenu.routing';
import { Rechargemenuroutes } from '../mainpages/recharge/recharge-submenu/recharge-submenu.routing';
import { RechargeComponent } from '../mainpages/recharge/recharge.component';
import { NotificationprocedComponent } from '../mainpages/notificationproced/notificationproced.component';
import { NotificationComponent } from '../mainpages/notification/notification.component';
import { TarrifplanComponent } from '../mainpages/tarrifplan/tarrifplan.component';
import { Tarrifmenuroutes } from '../mainpages/tarrifplan/tarrifsubmenu/tarrifsubmenu.routing';
import { BuybundlesComponent } from '../mainpages/buybundles/buybundles.component';
import { Buybundlesmenuroutes } from '../mainpages/buybundles/buybundle-submenu/buybundles-submenu.routing';
import { AccountHistoryComponent } from '../mainpages/account-history/account-history.component';
import { Accounthistorymenuroutes } from '../mainpages/account-history/account-history-submenu/account-history-submenu.routing';
import { AddaccountComponent } from '../mainpages/addaccount/addaccount.component';
import { MtnservicestoresComponent } from '../mainpages/mtnservicestores/mtnservicestores.component';
import { Storelocaterroutes } from '../mainpages/mtnservicestores/mtnstore-submenu/mtnstore-submenu.routing';
import { AddAccountProceedComponent } from '../mainpages/addaccount/addaccount-proceed/addaccount-proceed.component';
import { LegalComponent } from '../mainpages/legal/legal.component';
import { SecurityComponent } from '../mainpages/security/security.component';
import { FeedbackComponent } from '../mainpages/feedback/feedback.component';
import { ContactusComponent } from '../mainpages/contactus/contactus.component';
import { TermscondComponent } from '../mainpages/terms-condition/terms-condition.component';
import { PrivacypolicyComponent } from '../mainpages/privacypolicy/privacypolicy.component';
import { FaqsComponent } from '../mainpages/faqs/faqs.component';
import { ComplaintsComponent } from '../mainpages/complaints/complaints.component';
import { Complaintsmenuroutes } from '../mainpages/complaints/complaints-submenu/complaints-submenu.routing';
import { DetailsofprofileComponent } from '../mainpages/detailsofprofile/detailsofprofile.component';
import { DetailsProfilemenuroutes } from '../mainpages/detailsofprofile/detailsprofile-submenu/detailsprofile-submenu.routing';
import { OtherdetailsComponent } from '../mainpages/detailsofprofile/detailsprofile-submenu/otherdetails/otherdetails.component';
import { SocialmediaComponent } from '../mainpages/socialmedia/socialmedia.component';
import { SwitchmainAccountComponent } from '../mainpages/switchmainaccount/switchmainaccount.component';
import { OopspageComponent } from '../mainpages/oopspage/oopspage.component';
import { ConfirmationComponent } from '../mainpages/confirmation';
import { OtpprofileComponent } from '../mainpages/otpprofile/otpprofile.component';
import { BuybundleProccedComponent } from '../theme/component/buybundle-procced/buybundle-procced.component';
import {ProceedComponent} from '../mainpages/complaints/complaints-submenu/log-complaints/proceed/proceed.component';

const routes: Routes = [
  // { path: '**', redirectTo: 'login', pathMatch: 'full'},
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login/', component: LoginComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ...Loginmenuroutes,
  { path: 'airtime/success', component: SuccessComponent },
  { path: 'buybundle/success', component: BuybundlessuccessComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'buybundlesfailure', component: BuybundlesfailureComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'failure', component: FailureComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'dashboard', component: DashboardComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ...dashboardmenuroutes,
  { path: 'recharge/', component: RechargeComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ...Rechargemenuroutes,
  { path: 'notification', component: NotificationComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'notificationproced', component: NotificationprocedComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'tarrifplan', component: TarrifplanComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ...Tarrifmenuroutes,
  { path: 'buybundles/', component: BuybundlesComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ...Buybundlesmenuroutes,
  { path: 'accounthistory/', component: AccountHistoryComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ...Accounthistorymenuroutes,
  { path: 'addaccount', component: AddaccountComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'storelocater', component: MtnservicestoresComponent, pathMatch: 'full' },
  ...Storelocaterroutes,
  { path: 'addaccount/proceed', component: AddAccountProceedComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'legal', component: LegalComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'security', component: SecurityComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'feedback', component: FeedbackComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'contactus', component: ContactusComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'terms-condition', component: TermscondComponent, pathMatch: 'full' },
  { path: 'privacy-policy', component: PrivacypolicyComponent, pathMatch: 'full' },
  { path: 'faqs', component: FaqsComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'complaints/', component: ComplaintsComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ...Complaintsmenuroutes,
  { path: 'logComplaints/proceed', component: ProceedComponent, pathMatch: 'full' , canActivate: [AuthGuard] },
  { path: 'detailsofprofile/', component: DetailsofprofileComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ...DetailsProfilemenuroutes,
  { path: 'otherdetails/', component: OtherdetailsComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  ...DetailsProfilemenuroutes,
  { path: 'addaccount/proceed', component: AddAccountProceedComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'socialmedia/:id', component: SocialmediaComponent, pathMatch: 'full' },
  { path: 'switchaccount', component: SwitchmainAccountComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'oops', component: OopspageComponent, pathMatch: 'full' },
{ path: 'confirmation', component: ConfirmationComponent, pathMatch: 'full',canActivate: [AuthGuard] },
{ path: 'otpprofile', component: OtpprofileComponent, pathMatch: 'full', canActivate: [AuthGuard] },
{ path: 'buybundles/procced/:productId', component: BuybundleProccedComponent, pathMatch: 'full', canActivate: [AuthGuard] }
];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(routes, {
  scrollPositionRestoration: 'top'
});

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }
