import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import {
  PageSettingsModel, GridComponent, DetailRowService, ToolbarItems,
  PdfExportService, ExcelExportService, ToolbarService, PdfExportProperties, ExcelExportProperties, Column, hierarchyPrint
} from '@syncfusion/ej2-angular-grids';
import { ClickEventArgs } from '@syncfusion/ej2-angular-navigations';
import { reportTypes } from '../config';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-gridtable',
  templateUrl: './gridtable.component.html',
  styleUrls: ['./gridtable.component.scss'],
  providers: [DetailRowService, PdfExportService, ExcelExportService, ToolbarService]
})
export class GridtableComponent implements OnInit {

  @Input() tableData;
  @Input() listOfFields;
  @Input() childGridData;
  @Input() startDate;
  @Input() endDate;
  @Input() pdfLabel;
  @Input() excelLabel;
  @Input() historyType;
  @ViewChild('grid') public grid: GridComponent;
  @Input() enableRtl;
  @Input() showDwnldBtn;
  @Input() hierarchyDataToExport;
  toolbarOptions: ToolbarItems[];

  pageSettings: PageSettingsModel;

  constructor() { }

  ngOnInit() {
    this.pageSettings = { pageSize: 6 };
    this.toolbarOptions = ['PdfExport', 'ExcelExport'];
  }

  dataBound() {
    this.grid.hideColumns(['Consumed'])
  }

  exportReport(reportType): void {
    switch (reportType) {
      case reportTypes.pdf: {
        const pdfExportProperties: PdfExportProperties = {
          fileName: this.historyType + ' history (' + this.formatDateByDash(this.startDate) + ' - ' + this.formatDateByDash(this.endDate) + ').pdf',
          header: {
            fromTop: 0,
            height: 130,
            contents: [
              {
                type: 'Text',
                value: this.historyType + " history from " + this.formatDateBySlash(this.startDate) + " to " + this.formatDateBySlash(this.endDate),
                position: { x: 0, y: 50 },
                style: { textBrushColor: '#000000', fontSize: 18 }
              },
            ]
          },
          footer: {
            fromBottom: 160,
            height: 150,
            contents: [
              {
                type: 'PageNumber',
                pageNumberType: 'Arabic',
                format: 'Page {$current} of {$total}',
                position: { x: 300, y: 25 },
                style: { textBrushColor: '#6b6363', fontSize: 15 }
              }
            ]
          },
          pageSize: 'Letter',
          theme: {
            header: {
              fontColor: '#000000', fontName: 'TimesRoman', fontSize: 14, bold: true
            },
            record: {
              fontColor: '#000000', fontName: 'TimesRoman', fontSize: 10
            }
          },
          dataSource: this.hierarchyDataToExport
        }
        this.exportComplete();
        //console.log("GRID::::::::::::", this.grid);
        this.grid.pdfExport(pdfExportProperties);
        break;
      }
      case reportTypes.excel: {
        const excelExportProperties: ExcelExportProperties = {
          fileName: this.historyType + ' history (' + this.formatDateByDash(this.startDate) + ' - ' + this.formatDateByDash(this.endDate) + ').xlsx',
          dataSource: this.hierarchyDataToExport
        }
        this.exportComplete();
        this.grid.excelExport(excelExportProperties);
        break;
      }
    }
  }

  formatDateBySlash(selectedDate) {
    var datePipe = new DatePipe("en-US");
    return datePipe.transform(selectedDate, 'dd/MM/yyyy');
  }

  formatDateByDash(selectedDate) {
    var datePipe = new DatePipe("en-US");
    return datePipe.transform(selectedDate, 'dd-MM-yyyy');
  }

  exportComplete(): void {
    if (this.enableRtl) {
      (this.grid.columns[1] as Column).visible = true;
      (this.grid.columns[0] as Column).field = "Event_dt";
      (this.grid.columns[0] as Column).headerText = "Date";
      (this.grid.columns[2] as Column).field = "Charged_amount";
      (this.grid.columns[2] as Column).headerText = "Amount";
    }
  }

}
