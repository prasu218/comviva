import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
// import { Store } from '@ngrx/store';

// import * as fromRoot from '../../../reducers';
// import { Observable } from 'rxjs/Observable';
// import '~bootstrap/scss/bootstrap';

// import "styles/spinner";
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-slimloader',
  templateUrl: './slimloader.component.html',
  styleUrls: ['./slimloader.component.scss'],
})
export class SlimloaderComponent implements OnInit {

  loadertimeout:HTMLElement;
  @Input() message = '';
  @Input() showSlimloader: boolean = true;
  constructor() {

  }

  ngOnInit() {
    setTimeout(()=>{
      this.loadertimeout =document.getElementById('loadertimeout');
    },600000);

  }


  ngOnChanges(){

  }

}
