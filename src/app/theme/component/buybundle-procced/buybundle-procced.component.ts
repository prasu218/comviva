import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/plugins/datastore/datastore.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EligibilityService } from 'src/app/plugins/eligibility';
import { Location } from '@angular/common';
import { BuyBundlesService } from 'src/app/plugins/buybundles';
import { format } from 'src/app/utils/formatter';
import { AngularCDRService } from 'src/app/plugins/angularCDR/angularCDR.service';

@Component({
  selector: 'app-buybundle-procced',
  templateUrl: './buybundle-procced.component.html',
  styleUrls: ['./buybundle-procced.component.scss']
})
export class BuybundleProccedComponent implements OnInit {
  public msisdnval: any;
  public proceedData: any;
  public eligibility: any;
  showLoader: boolean = false;
  header = true;
  startTime: any;
  category: any = 'buybundles';
  value = "";
  mtnOnlineProductId: any;
  productList: any;
  internalCardDetails: any = {};
  errMsg: string = '';

  constructor(
    private _eligibilityService: EligibilityService, private _storeVal: StoreService,
    private router: Router, private _cdr: AngularCDRService, private location: Location,
    private routeParam: ActivatedRoute, private _buyBundleService: BuyBundlesService) { }

  ngOnInit() {
    this.mtnOnlineProductId = this.routeParam.snapshot.paramMap.get("productId");
    if (this.mtnOnlineProductId != 'undefined') {
      this.showLoader = true;
      this.msisdnval = format.msisdn(this._storeVal.getMSISDNData());
      let existingInternalCardData = this._storeVal.getBuyBundlePlanDetails();
      if (existingInternalCardData) {
        this.checkElegibility();
      } else {
        this.bundleProductDetails();
      }
    } else {
      this.errMsg = "Please select valid bundle to proceed";
    }
  }

  bundleProductDetails() {
    this.errMsg = '';
    this._buyBundleService.getAllBuyBundleProducts(this.msisdnval).then(resp => {
      if (resp.ResoponseCode == 0) {
        let bundleProdList = resp.ResponseData.ProductDetails.ProductDetails;
        if (bundleProdList.length > 0) {
          this.productList = bundleProdList;
          for (var i = 0; i < this.productList.length; i++) {
            if (this.productList[i].ProductID == this.mtnOnlineProductId) {
              let prodObj = {
                pname: this.productList[i].ProductName,
                description: this.productList[i].Description,
                price: this.productList[i].Price,
                vailidity: this.productList[i].Validity,
                renewal: this.productList[i].Renewal,
                isConsentRequired: this.productList[i].isConsentRequired,
                BuyForOthers: this.productList[i].BuyForOthers,
                action: this.productList[i].Action,
                active: false,
                eligibiltyId: this.productList[i].eligibiltyId,
                productId: this.productList[i].ProductID
              };
              this.internalCardDetails = prodObj;
              this.internalCardDetails['msisdn'] = this.msisdnval;
              this.internalCardDetails['selfOthers'] = false;
              this.internalCardDetails['beneficiaryMsisdn'] = '';
              this._storeVal.setBuyBundlePlanDetails(this.internalCardDetails);
              this.checkElegibility();
              break;
            }
          }
        }
      }
    }).catch(err => {
      this._cdr.writeCDR(this.startTime, this.category, this.value, 'buybundles procced', 'Failuer to procced');
      this.errMsg = "Unable to connect, please try again.";
      this.showLoader = false;
    });
  }

  checkElegibility() {
    this.errMsg = '';
    this.proceedData = this._storeVal.getBuyBundlePlanDetails();
    let eligibiltyId = '';
    let action = '';
    this.value = this.proceedData.action;
    if (this.proceedData.eligibiltyId) {
      eligibiltyId = this.proceedData.eligibiltyId.split(':')[1];

      this._eligibilityService.getEligibility(this.msisdnval, eligibiltyId, action).then((resp) => {

        this.showLoader = false;
        this.eligibility = resp.ResponseCode;
        this._cdr.writeCDR(this.startTime, this.category, this.value, 'buybundles procced', 'Success to procced');
      }).catch(err => {
        this._cdr.writeCDR(this.startTime, this.category, this.value, 'buybundles procced', 'Failuer to procced');
        this.errMsg = "Unable to connect, please try again."
        this.showLoader = false;
      });
    } else {
      this.eligibility = '0';
      this.showLoader = false;
    }

    if (this.proceedData.action) {
      action = this.proceedData.action.split(':')[1];
    }
  }

  backout() {
    this.location.back();
  }

}
