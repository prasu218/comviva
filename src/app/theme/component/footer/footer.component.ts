import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { config } from '../../../config/config';


declare var jquery:any;
declare var $ :any;


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
   public ASSETS_PATH: string = config.ASSETS;
 
   showHello: any = true;
    leftColFooterElements = [
    {id:'dashboard',value:'Dashboard',url: '/dashboard'},
    {id:'Recharge',value:'Recharge',url: '/recharge'},
    {id:'buy_bundles',value:'Buy bundles', url: '/buybundles'},
    {id:'vas',value:'vas'},
    {id:'help',value:'Help'},
    {id:'home',value:'Home'}
  ];
  leftColFooterElementsAboutUs=[
    {id:'Shop',value:'Shop'},
    {id:'Pay',value:'Pay'},
    {id:'Careers',value:'Careers'},
    {id:'Newsroom',value:'MoMo'},
    {id:'help',value:'Help'}, 
  ];
  leftColFooterElementsBusiness=[
    {id:'Business Solutions',value:'Business Solutions'},
    {id:'Insights',value:'Insights'},
    {id:'About MTN Business',value:'About MTN Business'},
    {id:'Support',value:'Support'},
    {id:'Shop',value:'Shop'}, 
  ];
  leftColFooterElementsMomo=[
    {id:'MoMo Users',value:'MoMo Users'},
    {id:'Merchants',value:'Merchants'},
    {id:'Agents',value:'Agents'},
    {id:'Corporate',value:'Corporate'},
    {id:'Features',value:'Features'}, 
    {id:'Insights',value:'Insights'},
    {id:'Help',value:'Help'},
  ];
  leftColFooterElementsHelp=[
   // {id:'Getting Started',value:'Getting Started'},
  //  {id:'With Devices',value:'With Devices'},
   // {id:'With your Account',value:'With your Account'},
   // {id:'With Your Order',value:'With Your Order'},
    {id:'legal',value:'Legal', url: '/legal'},
    {id:'terms-condition',value:'Terms & Condition', url: '/terms-condition'},
    {id:'security',value:'Security',url: '/security'},
    {id:'contactus',value:'ContactUs',url: '/contactus'},
{id:'mtnservicestores',value:'Locate MTN Store',url: '/mtnservicestores'},
    {id:'FAQs',value:'FAQs',url:'/faqs'}, 
  //  {id:'Guides & Downloads',value:'Guides & Downloads'},
  //  {id:'HeAsk Community',value:'Ask Community'},
   // {id:'My MTN App',value:'My MTN App'},
    {id:'complaints',value:'Complaints',url:'/complaints'},
    {id:'FeedBack',value:'FeedBack',url:'/feedback'},
     ];
  searchCharacters=[
    'home',
    'dashboard',
    'Recharge',
    'vas',
    'help',
    'dashboard'
  ];
   public category = [{
    imageurl: config.ASSETS + '/icons/fb.svg',
    categoryname: 'facebook',
    url:"https://www.facebook.com/MTNLoaded",
    id:1
  }, {
    imageurl: config.ASSETS + '/icons/insta.svg',
    categoryname: 'Instagram',
    url:"https://www.instagram.com/mtnng/",
    id:2
  },{
    imageurl: config.ASSETS + '/icons/twitter.svg',
    categoryname: 'Twitter',
    url:"https://twitter.com/MTNNG",
    id:3
  },{
    imageurl: config.ASSETS + '/icons/twitter180.svg',
    categoryname: 'Twitter180',
    url:"https://twitter.com/MTN180",
    id:4
  },
  {
    imageurl: config.ASSETS + '/icons/youtube.svg',
    categoryname: 'YouTube',
    url:"https://www.youtube.com/user/MTNNG",
    id:5
  },
  {
    imageurl: config.ASSETS + '/icons/linkedin.svg',
    categoryname: 'LinkedIn',
    url:"https://www.linkedin.com/company/mtn-nigeria/",
    id:6
  }];
  
  myArray: any = [true, true, true ,true, true, true];
  Socialweburl(url){
    window.open(url);
  }
  
  constructor(private _router: Router) { }

  ngOnInit() {
     
	//if(this.bgColor=="white")
   //{
    // this.bgColor="top_curve1";
   //}
   //else{
    //this.bgColor="top_curve";
   //}
  	
}
  ngAfterViewInit(){
 $(document).ready(function(){

  $('#homeheading i').click(function(){
    //home
    $('a').show();
    $('a').css({'display':'block'})
    $('.fa-minus').show();
    $('#homeheading .fa-plus').hide();
     $('#about a').hide();
     $('#about i').hide();
    $('#home a').show();
    $('#recharge a').hide();
    $('#recharge i').hide();
    $('#help i').hide();
    $('#help a').hide()
    $('#buybundle i').hide();
    $('#buybundle a').hide();
    

    
    
  });
//about us
  $('#aboutheading i').click(function(){
    $('a').show();
    $('a').css({'display':'block'})
    $('.fa-minus').show();
    $('#aboutheading .fa-plus').hide();
    $('#about a').show();
    $('#home a').hide();
    $('#home i').hide();
    $('#recharge a').hide();
    $('#recharge i').hide();
    $('#help i').hide();
  $('#help a').hide()
  $('#buybundle i').hide();
  $('#buybundle a').hide();
});

//recharge
$('#rechargeheading i').click(function(){
  $('a').show();
  $('a').css({'display':'block'})
  $('.fa-minus').show();
  $('#rechargeheading .fa-plus').hide();
  $('#about a').hide();
  $('#home a').hide();
  $('#about i').hide();
  $('#home i').hide();
  $('#recharge a').show();
  $('#recharge i').show();
  $('#help i').hide();
  $('#help a').hide()
  $('#buybundle i').hide();
  $('#buybundle a').hide();
});


//buy bundle

$('#buynundleheading i').click(function(){
  $('a').show();
  $('a').css({'display':'block'})
  $('.fa-minus').show();
  $('#buynundleheading .fa-plus').hide();
  $('#about a').hide();
  $('#home a').hide();
  $('#about i').hide();
  $('#home i').hide();
  $('#recharge a').hide();
  $('#recharge i').hide();
  $('#buybundle i').show();
  $('#buybundle a').show();
  $('#help i').hide();
  $('#help a').hide();
});



//help
$('#helpheading i').click(function(){
  $('a').show();
  $('a').css({'display':'block'})
  $('.fa-minus').show();
  $('#helpheading .fa-plus').hide();
  $('#about a').hide();
  $('#home a').hide();
  $('#about i').hide();
  $('#home i').hide();
  $('#recharge a').hide();
  $('#recharge i').hide();
  $('#buybundle i').hide();
  $('#buybundle a').hide();
  $('#help i').show();
  $('#help a').show();
});

  $('.fa-minus').click(function(){
    $('a').hide();
   $('.fa-minus').hide();
    $('.fa-plus').show();
    $('.icon-align a').show();
    
  });
}); 
}



}
