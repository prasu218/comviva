import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
import { ProfileService } from '../../../plugins/profile';
import { StoreService } from 'src/app/plugins/datastore';
import { loginrcgService } from 'src/app/plugins/loginrcg';
import { config } from '../../../config/config';
import { ApiService } from 'src/app/plugins/commonAPI';
//import { config } from '../../../config';
// import { ButtonComponent } from '../button';

//import { ViewEncapsulation } from '@angular/core'

declare var jquery: any;
declare var $: any;

//declare var $:JQueryStatic;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
	//encapsulation: ViewEncapsulation.ShadowDom
  //encapsulation: ViewEncapsulation.None
  // styleUrls: ['../../../theme/scss/_login.scss', '../../../theme/scss/_card.scss','./header.component.scss']

})
export class HeaderComponent implements OnInit {
  switched: boolean;
 // public ASSETS_PATH: string = config.ASSETS;
  constructor(private router: Router,private bnIdle: BnNgIdleService,private _storeVal: StoreService, private apiService: ApiService, private loginService: loginrcgService, private _profile:ProfileService) { }
  public ASSETS_PATH: string = config.ASSETS;
  //   $("#leftsliderheadder").click(function () {
  // $(this).show("slide", { direction: "left" }, 1000);
  //   });

  public msisdnval: any;
  msisdn:string;
  sec_msisdn:string;
  sec_account:boolean = false;
  sec_switch_accnt:boolean = false;
  @Input() migrateMode : boolean = false;
  showLoader: boolean = false;
  profilePic: any;
  main_acc;
  firstName:string;

  ngOnInit() {
    if(localStorage.getItem("switched") == "true"){
     // console.log("The switching gotten is ", localStorage.getItem("switched") );
      this.switched = true;
      this.main_acc = JSON.parse(localStorage.getItem("manna")).SecondaryNumber;
    }else{
     // console.log("The switched status gotten is ", localStorage.getItem("switched") );
      this.switched = false;
    }
  // if(sessionStorage.getItem("chakra") != null){
    if(this._storeVal.getToken() != null){
      this._profile.getfetchingprofile(this.msisdnval);
      this.msisdn = this._storeVal.getMSISDNData();
      this.sec_msisdn = this._storeVal.getLinkedAccount();
      this.sec_account = this._storeVal.getSecondaryAccount();
      this.sec_switch_accnt = this._storeVal.getSecondaryAccountfalse();
      this.firstName = this._storeVal.getFirstNameData();
      this.fetchingprofile(this.msisdn);
      this._profile.currentProfilePic.subscribe(imageUrl => this.profilePic = imageUrl)
    }
  }
  rechargenv() {
    this.router.navigate(['/recharge']);
  }
  helpnav(){
    this.router.navigate(['/help']);
  }

  profilepage() {
this.sec_switch_accnt = this._storeVal.getSecondaryAccountfalse();
    // this.router.navigate(['/detailsofprofile']);
    this.router.navigate(['/otpprofile']);
  }
  
  subscription(target){
    target = document.getElementById('target');
    target.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});  
  }
   
 logout() {
    this.showLoader = true;
   this.loginService.logout(this.msisdn).then(resp => {
     if (resp.status == 0) {
       sessionStorage.clear();
        localStorage.clear();
        this.router.navigateByUrl('/login');
       this.bnIdle.stopTimer();
      }
      this.showLoader = false;
    }).catch(err => {
      sessionStorage.clear();
      localStorage.clear();
      this.router.navigateByUrl('/login');
     this.bnIdle.stopTimer();
      this.showLoader = false;
      Promise.reject(err);
    }); 
  }

  ngAfterViewInit(){
    $(".myButton").click(function () {
      $('#myDiv').slideToggle(1000);
      $('#tgl').toggleClass('fa-times fa-bars');


      // Set the effect type
     // var effect = 'slideDown';
  
      // Set the options for the effect type chosen
     // var options = { direction: $('.mySelect').val() };
  
      // Set the duration (default: 400 milliseconds)
    //  var duration = 500;
  
     
  });
 

  $('.has_sub a:not(:only-child)').click(function(e) {
    $(this).siblings('.sub-nav').toggle();
    // Close one dropdown when selecting another
    $('.sub-nav').not($(this).siblings()).hide();
    e.stopPropagation();
  });
  // Clicking away from dropdown will remove the dropdown class
  $('html').click(function() {
    $('.sub-nav').hide();
  });
 

  
  // $('.mob-subnav:not(:only-child)').click(function(e) {
  //   $(this).siblings('.mtn-accordion__content').toggle();
  //   $(this).find('#mob-tgl').toggleClass(' fa-plus fa-minus')
  //   // Close one dropdown when selecting another
  //   $('.mtn-accordion__content').not($(this).siblings()).hide();
    
  //   e.stopPropagation();
  // });

  // // Clicking away from dropdown will remove the dropdown class
  // $('html').click(function() {
  //   $('.mtn-accordion__content').hide();
  // });


  $(".mob-subnav").click(function () {
    //slide up all the link lists
    $(".mtn-accordion__content").slideUp(500);
    $('.plus',this).html('+');
    //slide down the link list below the h3 clicked - only if its closed
    if (!$(this).next().is(":visible")) {
        $(this).next().slideDown(500);
        $('.plus').html('+');
        $('.plus',this).html('-');
    }
})

 
}
hide_nav () {
  var effect = 'slideDown';
  var options = { direction: $('.mySelect').val() };
  var duration = 500;
  $('#myDiv').hide(effect);
}

onClickedOutside(msg: Event) {
  this.hide_nav ();
  } 

  public category = [{
    imageurl: config.ASSETS + '/icons/fb.svg',
    categoryname: 'Facebook',
    url:"https://www.facebook.com/MTNLoaded",
    id:1
  }, {
    imageurl: config.ASSETS + '/icons/insta.svg',
    categoryname: 'Instagram',
    url:"https://www.instagram.com/mtnng/",
    id:2
  },{
    imageurl: config.ASSETS + '/icons/twitter.svg',
    categoryname: 'Twitter',
    url:"https://twitter.com/MTNNG",
    id:3
  },{
    imageurl: config.ASSETS + '/icons/twitter180.svg',
    categoryname: 'Twitter180',
    url:"https://twitter.com/MTN180",
    id:4
  },
  {
    imageurl: config.ASSETS + '/icons/youtube.svg',
    categoryname: 'YouTube',
    url:"https://www.youtube.com/user/MTNNG",
    id:5
  },
  {
    imageurl: config.ASSETS + '/icons/linkedin.svg',
    categoryname: 'LinkedIn',
    url:"https://www.linkedin.com/company/mtn-nigeria/",
    id:6
  }];

   fetchingprofile(msisdnval) {
    this._profile.getProfilePic(msisdnval).then((res) => {
      var base64image = atob(res.encodedprofilepic);
      if (base64image) {
        this.profilePic = 'data:image/png;base64,' + base64image;
      }
    }).catch((err) => {
      this.profilePic = ''
    });
  }
}
