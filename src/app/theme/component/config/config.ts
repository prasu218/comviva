export const icons = {
  'right_arrow': 'fa-arrow-right',
  'left_arrow': 'fa-arrow-left',
  'redo': 'fa-repeat',
  'logout': 'fa-sign-out',
  'change': 'fa-exchange',
  'plus' : 'fa-plus'
};

export const type = {
  'filled': 'filled',
  'outline': 'borderline'
};

export const size = {
  'full': 'full-btn',
  'quarter': 'quater-btn',
  'half': 'half-btn',
  'round': 'round-btn'
};

export const reportTypes = {
  'pdf': 'PdfExport',
  'excel': 'ExcelExport'
}
