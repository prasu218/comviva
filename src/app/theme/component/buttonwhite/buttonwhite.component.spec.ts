import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonwhiteComponent } from './buttonwhite.component';

describe('ButtonwhiteComponent', () => {
  let component: ButtonwhiteComponent;
  let fixture: ComponentFixture<ButtonwhiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonwhiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonwhiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
