import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { icons, type, size } from '../config';

@Component({
  selector: 'app-buttonwhite',
  templateUrl: './buttonwhite.component.html',
  styleUrls: ['./buttonwhite.component.scss']
})
export class ButtonwhiteComponent implements OnInit {
  @Input() size: string;
  @Input() label: string;
  constructor() { }

  ngOnInit() {
    this.size = size[this.size];
  }

}
