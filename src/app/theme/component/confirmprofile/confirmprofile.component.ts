import { Component, OnInit, Input } from '@angular/core';
import { StoreService } from '../../../plugins/datastore';
// import { StoreService } from '../../../../../../../plugins/datastore';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ProfileService } from '../../../plugins/profile';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { letProto } from 'rxjs-compat/operator/let';
@Component({
  selector: 'app-confirmprofile',
  templateUrl: './confirmprofile.component.html',
  styleUrls: ['./confirmprofile.component.scss']
})
export class ConfirmprofileComponent implements OnInit {

message :string;

showLoader: boolean = false;
// public existing_cust: FormGroup;
public msisdnother:any;
public msisdnval: any;
public voicemail :any;
public suceesDiv:Boolean=false;
public failurDiv:Boolean=false;
public phonconf:Boolean=false;
public voiceconf:Boolean=false;
public deacticonf: Boolean = false;
public resetconf: Boolean = false;
public barrconf: Boolean = false;
public unbarrcinf: Boolean = false;
constructor( private _router: Router, private _profile: ProfileService, private _storeVal: StoreService, public dialog: MatDialog, private fb: FormBuilder, private router: Router) { }
  ngOnInit() {
    this.msisdnother = this._storeVal.getproceprofileData().msisdnother;
    switch( this._storeVal.getproceprofileData().proceedData){
      case 'Phone' : this.phonconf = true;
      break;
      case 'Voice' : this.voiceconf = true;
      break;
      case 'deactivecall' : this.deacticonf = true;
      break;
      case 'Resetcall' : this.resetconf = true;
      break;
      case 'barring' : this.barrconf = true;
      break;
      case 'unbarring' : this.unbarrcinf = true;
      break;
    }
    
this.msisdnval = this._storeVal.getMSISDNData();
//console.log("confirmpagevalue"+ this.msisdnval);
  }

  backout() {
    this.router.navigate(['detailsofprofile/otherdetails/']);
  }
  
  phoneconfirm(){
    //  this.phonconf = true;
     this.showLoader = true;
   let msisdnother = this._storeVal.getproceprofileData();
      this._profile.getfarwphone(this.msisdnval,msisdnother).then((resp) => {
        if (resp.status_code == 0) {
          
          this.showLoader = false;
          this.suceesDiv=true;
          this.message = " Y'ello. Your request has been processed successfully. Thank you.";
        }
        else{
            this.showLoader = false;
            this.failurDiv=true;
           this.message = 'Sorry, your request could not completed. Please try again.';
       
        } 
      }).catch((err) => {
        this.showLoader = false;
        this.failurDiv=true;
        this.message = 'Sorry, your request could not completed. Please try again.';
       
      });
    
    }

    voicecallconfirm(){
      this.showLoader = true;
    this._profile.getfarwvoicemail(this.msisdnval,this.voicemail).then((resp) => {
      if (resp.status_code == 0) {
        this.showLoader = false;
        this.suceesDiv=true;
        this.message = "Y'ello. Your request has been processed successfully. Thank you.";
  
      }
      else{
        this.showLoader = false;
        this.failurDiv=true;
         this.message = 'Sorry, your request could not completed. Please try again.';
         
      } 
    }).catch((err) => {
      this.showLoader = false;
      this.failurDiv=true;
      this.message = 'Sorry, your request could not completed. Please try again.';
    
    });
  
  }

  deactcallforconfirm(){
    this.showLoader = true;
   this._profile.getdeactivatecallfarw(this.msisdnval,this.voicemail).then((resp) => {
    if (resp.status_code == 0) {
      this.showLoader = false;
      this.suceesDiv=true;
      this.message = "Y'ello. Your request has been processed successfully. Thank you.";

    }
    else{
      this.showLoader = false;
      this.failurDiv=true;
       this.message = 'Sorry, your request could not completed. Please try again.';
     
    } 
  }).catch((err) => {
    this.showLoader = false;
    this.failurDiv=true;
    this.message = 'Sorry, your request could not completed. Please try again.';
   
  });

}

resetconfirm(){
  this.showLoader = true;
  this._profile.getresetbarr(this.msisdnval).then((resp) => {
    if (resp.status_code == 0) {
      this.showLoader = false;
      this.suceesDiv=true;
      this.message = "Y'ello. Your request has been processed successfully. Thank you.";
    }
    else{
      this.showLoader = false;
      this.failurDiv=true;
       this.message = 'Sorry, your request could not completed. Please try again.';
    } 
  }).catch((err) => {
    this.showLoader = false;
    this.failurDiv=true;
    this.message = 'Sorry, your request could not completed. Please try again.';
   

  });

}

barincominconfirm(){
  this.showLoader = true;
  this._profile.getcallbarring(this.msisdnval).then((resp) => {
    if (resp.status_code == 0) {
      this.showLoader = false;
      this.suceesDiv=true;
      this.message = "Y'ello. Your request has been processed successfully. Thank you.";
    }
    else{
      this.showLoader = false;
      this.failurDiv=true;
       this.message = 'Sorry, your request could not completed. Please try again.';
      
    } 
  }).catch((err) => {
    this.showLoader = false;
    this.failurDiv=true;
    this.message = 'Sorry, your request could not completed. Please try again.';
  });

}


unbarincominconfirm(){
  this.showLoader = true;
this._profile.getcallunbarring(this.msisdnval).then((resp) => {
  if (resp.status_code == 0) {
    this.showLoader = false;
    this.suceesDiv=true;
    this.message = "Y'ello. Your request has been processed successfully. Thank you.";
  }
  else{
    this.showLoader = false;
    this.failurDiv=true;
     this.message = 'Sorry, your request could not completed. Please try again.';
  } 
}).catch((err) => {
  this.showLoader = false;
  this.failurDiv=true;
  this.message = 'Sorry, your request could not completed. Please try again.';
 
});

}

}
