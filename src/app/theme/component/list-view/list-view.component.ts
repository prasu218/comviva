import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnInit {

  // @Input() label: string;
  // @Input() price: string;
  // @Output() selected_list = new EventEmitter<string>();
  // @Output() buttonEve = new EventEmitter();
  buttonLabel ="Buy";
  mouseovered: boolean = false;
  priceHidden:boolean = true;
  constructor() { }

  ngOnInit() {
    // if(this.price===undefined){
    //   this.priceHidden=false;
    //   this.buttonLabel = "Borrow";
    // }
  }
  // buyButton() {
  //   this.buttonEve.emit();
  // }

  highlightedList(event) {
    if (event.type === "mouseenter") {
      this.mouseovered = true;
    } else {
      this.mouseovered = false;
    }
  }

}

