import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonviewComponent } from './buttonview.component';

describe('ButtonqComponent', () => {
  let component: ButtonviewComponent;
  let fixture: ComponentFixture<ButtonviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
