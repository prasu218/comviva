import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-buttonview',
  templateUrl: './buttonview.component.html',
  styleUrls: ['./buttonview.component.scss']
})
export class ButtonviewComponent implements OnInit {
  @Input() label: string;
  constructor() { }

  ngOnInit() {
  }

}
