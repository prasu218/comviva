import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { icons, type, size } from '../../../config';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['../../scss/_button.scss']
})
export class ButtonComponent implements OnInit {
  @Input() type: string;
  @Input() label: string;
  @Input() icon: string;
  @Input() disabled: boolean;
  @Input() size: string;

  @Output() onClick: EventEmitter<any> = new EventEmitter();
  public fa_icon: string;
  public btn_class: string;
  constructor() { }

  ngOnInit() {
    this.fa_icon = icons[this.icon];
    this.btn_class = type[this.type];
    this.size = size[this.size];
  }
}
