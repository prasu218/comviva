import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashyellowComponent } from './dashyellow.component';

describe('DashyellowComponent', () => {
  let component: DashyellowComponent;
  let fixture: ComponentFixture<DashyellowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashyellowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashyellowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
