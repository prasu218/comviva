import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { icons, type, size } from '../config';

@Component({
  selector: 'app-dashyellow',
  templateUrl: './dashyellow.component.html',
  styleUrls: ['./dashyellow.component.scss']
})
export class DashyellowComponent implements OnInit {

  @Input() size: string;
  @Input() label: string;
  @Input() idicon: string;
  constructor() { }

  ngOnInit() {
    this.size = size[this.size];
  }
 
}
