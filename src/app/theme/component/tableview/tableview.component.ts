import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
//import { StoreService } from'../datastore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tableview',
  templateUrl: './tableview.component.html',
  styleUrls: ['./tableview.component.scss']
})
export class TableviewComponent implements OnInit {
  @Input() bundleValue='';
  @Input() bundleExpiry='';
  @Input() bundleType='';
  @Input() bundleName='';
  @Input() other=[];
  @Output() crossIcon = new EventEmitter();
  // @Input() bundle=''
  //@Input() otherDataBalance:any;

  constructor(private _router: Router) { }

  ngOnInit() {
    
  }

}
