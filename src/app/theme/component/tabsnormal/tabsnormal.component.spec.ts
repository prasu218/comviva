import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsnormalComponent } from './tabsnormal.component';

describe('TabsnormalComponent', () => {
  let component: TabsnormalComponent;
  let fixture: ComponentFixture<TabsnormalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsnormalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsnormalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
