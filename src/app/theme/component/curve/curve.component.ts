import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-curve',
  templateUrl: './curve.component.html',
  styleUrls: ['./curve.component.scss']
})
export class CurveComponent implements OnInit {
  @Input() bgColorpp : string;

  // 1. Black Bottom-curve
  // 2. Grey Bottom-curve
  // 3. White Bottom-curve
  // 4. Yellow Bottom-curve
  // 5. White Top-curve

  @Input() type = 1;
  @Input()image_curve_bottom = false;
  @Input()image_curve_top = false;

  Images = [
    {
      type: 1,
      srcDesk: 'app/appassets/curves/1-web-bottom-curve.svg',
      srcMob: 'app/appassets/curves/1-mob-bottom-curve.svg',
      desc: 'Black Bottom-curve',
    },
    {
      type: 2,
      srcDesk: 'app/appassets/curves/2-web-bottom-curve.svg',
      srcMob: 'app/appassets/curves/2-mob-bottom-curve.svg',
      desc: 'Grey Bottom-curve',
    },
    {
      type: 3,
      srcDesk: 'app/appassets/curves/3-web-bottom-curve.svg',
      srcMob: 'app/appassets/curves/3-mob-bottom-curve.svg',
      desc: 'White Bottom-curve',
    },
    {
      type: 4,
      srcDesk: 'app/appassets/curves/4-web-bottom-curve.svg',
      srcMob: 'app/appassets/curves/4-mob-bottom-curve.svg',
      desc: 'Yellow Bottom-curve',
    },
    {
      type: 5,
      srcDesk: 'app/appassets/curves/5-web-top-curve.svg',
      srcMob: 'app/appassets/curves/5-mob-top-curve.svg',
      desc: 'White Top-curve',
    },
  ]
  srcDesk: string;
  srcMob: string;

  
  constructor() { }

  ngOnInit() {
    this.srcDesk = this.Images[this.type-1].srcDesk;
    this.srcMob = this.Images[this.type-1].srcMob;

    // if(this.bgColor=="grey")
    // {
    //   this.bgColor="top_curve1";
    // }
    // else{
    //  this.bgColor="top_curve";
    // }
     
  }

}
