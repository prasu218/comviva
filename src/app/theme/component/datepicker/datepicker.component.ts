import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['../input-floating/input-floating.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: DatepickerComponent,
    multi: true
  }]
})
export class DatepickerComponent implements OnInit {

  @Input() valid: boolean;
  @Input() placeholder;
  @Input() disabled;
  @Output() selectedDate = new EventEmitter();
  @Input() maxDate: Date;
  @Input() minDate: Date;
  @Output() dateChange = new EventEmitter()
  dirty: boolean;
  imagePath: any;
  imageErrorPath: any;
  showErrorMessage: any = false;
  ASSETS_PATH: any = 'assets/images/';
  wrapperClass: any;
  labelClass: any;
  public _value: any = '';
  onChange: any = () => { };
  onTouch: any = () => { };

  get value(): any {
    return this._value;
  }

  set value(v: any) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(this._value);
    }
  }

  constructor() { }


  ngOnInit() {
    // this.imagePath = this.ASSETS_PATH + this.iconName + '.svg';
    // this.imageErrorPath = this.ASSETS_PATH + this.iconName + '-error.svg';
    this.labelClass = 'label-off';
  }

  renderChange(value: any) {
    if (!this.valid && this.showErrorMessage) {
      this.showErrorMessage = false;
      this.wrapperClass = 'app-input-focus';
    } else if (this.valid && !this.showErrorMessage) {
      this.showErrorMessage = true;
      this.wrapperClass = 'app-input-error';
    }
    this._value = value;
    this.onChange(this.value);
  }

  onBlur() {
    this.wrapperClass = this._value ? 'app-input-blur' : 'app-input-focus';
    if (!this.valid) {
      //  this.showErrorMessage = true;
      //  this.wrapperClass = 'app-input-error';
    } else {
      this.showErrorMessage = false;
      this.wrapperClass = 'app-input-blur';
    }
    if (!this.value) {
      this.labelClass = 'label-off';
    }
  }

  onFocus() {
    this.dirty = true;
    this.labelClass = 'label-on';
    if (this.showErrorMessage) {
      this.wrapperClass = 'app-input-error';
    } else {
      this.wrapperClass = 'app-input-focus';
    }
  }

  writeValue(value: any): void {
    this._value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  getSelectedDate(date) {
    this.selectedDate.emit(date);
  }

}
