import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-internalcards',
  templateUrl: './internalcards.component.html',
  styleUrls: ['./internalcards.component.scss']
})
export class InternalcardsComponent implements OnInit {
  @Input() label: string;
  @Input() price: string;
  @Input() desc: string;
  @Output() selected_list = new EventEmitter<string>();
  @Output() buttonEve = new EventEmitter();
  @Input() active : boolean = false;
  mouseovered: boolean = false;
  buttonLabel ="Buy";


    constructor() { }

  ngOnInit() {
  }
  
   buyButton() {
    this.buttonEve.emit();
  }

highlightedList(event) {
  if (event.type === "mouseenter") {
    this.mouseovered = true;
  } else {
    this.mouseovered = false;
  }
}  
}
