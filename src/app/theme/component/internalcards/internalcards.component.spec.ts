import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalcardsComponent } from './internalcards.component';

describe('InternalcardsComponent', () => {
  let component: InternalcardsComponent;
  let fixture: ComponentFixture<InternalcardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalcardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalcardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
