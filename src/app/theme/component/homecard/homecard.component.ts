import { Component, OnInit,Input, Output, EventEmitter,SimpleChange, SimpleChanges } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-homecard',
  templateUrl: './homecard.component.html',
  styleUrls: ['./homecard.component.scss']
})
export class HomecardComponent implements OnInit {
public greeting : string;
  @Input() check_postpaid :  string =  '';
  @Output() balances = new EventEmitter();
  @Output() details = new EventEmitter();
  @Input() airtimeBalance='';
  @Input() tariffPlanName='';
  @Input() dataBalance='';
  @Input() airtimeBonusBalance='';
  @Input() dataBonusBalance='';
  @Input() firstName='';
  @Input() outstandingBalance='';
  @Input() creditLimitAssignedBalance='';
  @Input() creditLimitBalance='';
 @Output() subscription = new EventEmitter();

 @Input("mynumber") mynumber="";
 @Input ("mainnumber") mainnumber;
 @Input("switched") switched;
 @Input("parentNumber") parentNumber;
 @Input("numberLists") numberLists  = [];
 @Input("linkedMainnumbers") linkedMainnumbers;
 @Input("linkedGotten") linkedGotten;

 primaryNum;
 parentNumbe: any;

  constructor() { }

  ngOnInit() {
  
  //mobile carousel
    $( document ).ready(function() {
    $('.carousel-hmcrd').flickity({
      groupCells: true,
      freeScroll: true,
      cellAlign: 'left', 
      contain: true, 
      prevNextButtons: false,
      pageDots: true, 
      draggable: true,
      watchCSS: true,
      setGallerySize: false,
      resize: true,
      reposition: true,
    });
  });

    $(function ($) {
      $.fn.hScroll = function (amount) {
          amount = amount || 120;
          $(this).bind("DOMMouseScroll mousewheel", function (event) {
              var oEvent = event.originalEvent, 
                  direction = oEvent.detail ? oEvent.detail * -amount : oEvent.wheelDelta, 
                  position = $(this).scrollLeft();
              position += direction > 0 ? -amount : amount;
              $(this).scrollLeft(position);
              event.preventDefault();
          })
      };
  });
  
  $(document).ready(function() {
      $('.mobb').hScroll(60); // You can pass (optionally) scrolling amount
  });


  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    // if(changes["numberLists"]){
     // console.log("This is chanhges", changes);
      if(changes && changes.numberLists){
        this.numberLists = changes.numberLists.currentValue;
      }
  }

  ngAfterViewInit() {
    var hour = new Date().getHours(); 
    var greeting;
    if (hour < 12) {
      greeting = "Good Morning";
    } else if(hour <17) {
      greeting = "Good Afternoon";
    }else{
      greeting = "Good Evening";
    }
    document.getElementById("demo").innerHTML = greeting;
  }
  
   subscriptionapi(target){
    target = document.getElementById('target');
    target.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    
    }
}
