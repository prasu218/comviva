import { Component, OnInit } from '@angular/core';
import { config } from '../../../config/config';
import { Router } from '@angular/router';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit() {
  // mobile carousel
   $( document ).ready(function() {
    $('.carousel-cards2').flickity({
      groupCells: true,
      freeScroll: true,
      cellAlign: 'left', 
      contain: true, 
      prevNextButtons: false,
      pageDots: true, 
      draggable: true,
      watchCSS: true,
      setGallerySize: false,
      resize: true,
      reposition: true
    });
  });
  }
recharge(){
     this._router.navigate(['recharge']);
  }
buybundleview(){
    this._router.navigate(['buybundles'])
  }

history() {
    this._router.navigate(['history'])
  }
tarrifPlanView(){
	  this._router.navigate(['tarrifplan/activeplan'])
}
storelocater(){
  this._router.navigate(['mtnservicestores'])
}
 subscriptionapi(){
    let target = document.getElementById('target');
    target.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});  
  }
}