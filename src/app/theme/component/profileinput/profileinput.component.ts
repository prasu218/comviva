import { Component, OnInit, ViewEncapsulation, Input,EventEmitter, Output } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

import { ChangeDetectionStrategy } from '@angular/core';
@Component({
  selector: 'app-profileinput',
  templateUrl: './profileinput.component.html',
  styleUrls: ['./profileinput.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: ProfileinputComponent,
    multi: true
  }],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})

  export class ProfileinputComponent implements OnInit, ControlValueAccessor {
  @Input() valid: boolean;
  @Input() type: any;
  @Input() iconName: any;
  @Input() errorMsg: any;
  @Input() maxlengthVal: number;
  @Input() placeholder;
  @Input() iconPosition: any;
  @Input() disabled;
 @Output() tooltip = new EventEmitter();
   dirty: boolean;
  imagePath: any;
  imageErrorPath: any;
  showErrorMessage: any = false;
  ASSETS_PATH: any = 'assets/images/';
  wrapperClass: any;
  labelClass: any;
  public _value: any = '';
  onChange: any = () => {};
  onTouch: any = () => {};
  get value(): any {
    return this._value;
  }
  set value(v: any) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(this._value);
    }
  }
  ngOnInit() {
    this.imagePath = this.ASSETS_PATH + this.iconName + '.svg';
    this.imageErrorPath = this.ASSETS_PATH + this.iconName + '-error.svg';
    this.labelClass = 'label-off';
  }
  
  onPaste(event) {
    const str = event.clipboardData.getData('Text');
    event.preventDefault();
    this.renderChange(str);
  }
  onInputChange(data: any) {
    this.renderChange(data);
  }
  renderChange(value: any) {
    if (!this.valid && this.showErrorMessage) {
      this.showErrorMessage = false;
      this.wrapperClass = 'app-input-focus';
    } else if (this.valid && !this.showErrorMessage) {
      this.showErrorMessage = true;
      this.wrapperClass = 'app-input-error';
    }
    this._value = value;
    this.onChange(this.value);
  }
  onBlur() {
    this.wrapperClass = this._value  ? 'app-input-blur' : 'app-input-focus';
    if (!this.valid) {
    //  this.showErrorMessage = true;
    //  this.wrapperClass = 'app-input-error';
    } else {
      this.showErrorMessage = false;
      this.wrapperClass = 'app-input-blur';
    }
    if (!this.value) {
      this.labelClass = 'label-off';
    }
  }
  onFocus() {
   this.dirty = true;
    this.labelClass = 'label-on';
    if (this.showErrorMessage) {
      this.wrapperClass = 'app-input-error';
    } else {
      this.wrapperClass = 'app-input-focus';
    }
  }
  writeValue(value: any): void {
    this._value = value;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
 callMe(){
    
  }
}






