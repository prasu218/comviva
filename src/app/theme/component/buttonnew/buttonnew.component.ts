import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { icons, type, size } from '../config';

@Component({
  selector: 'app-buttonnew',
  templateUrl: './buttonnew.component.html',
  styleUrls: ['./buttonnew.component.scss']
})
export class ButtonnewComponent implements OnInit {
  @Input() size: string;
  @Input() label: string;
  constructor() { }

  ngOnInit() {
    this.size = size[this.size];
  }

}
