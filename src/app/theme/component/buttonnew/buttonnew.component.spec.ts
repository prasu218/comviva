import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonnewComponent } from './buttonnew.component';

describe('ButtonnewComponent', () => {
  let component: ButtonnewComponent;
  let fixture: ComponentFixture<ButtonnewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonnewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonnewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
