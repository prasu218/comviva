import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XtravaluetabsComponent } from './xtravaluetabs.component';

describe('XtravaluetabsComponent', () => {
  let component: XtravaluetabsComponent;
  let fixture: ComponentFixture<XtravaluetabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtravaluetabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtravaluetabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
