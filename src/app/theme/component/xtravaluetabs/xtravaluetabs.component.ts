import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-xtravaluetabs',
  templateUrl: './xtravaluetabs.component.html',
  styleUrls: ['./xtravaluetabs.component.scss']
})
export class XtravaluetabsComponent implements OnInit {

  public active_nav: string;
  @Input() faq_tab_header_Elements: any;
  @Output() selected_tab = new EventEmitter<string>();
  percentage: number;
  constructor() { }

  ngOnInit() {
    this.active_nav = this.faq_tab_header_Elements[0];
    //console.log('this.faq_tab_header_Elements.length--' + this.faq_tab_header_Elements);
    this.percentage = 100/this.faq_tab_header_Elements.length;
    //console.log("Selected Object in tabs normal : "+this.active_nav);
  }
  
  setActiveNav(){
    this.active_nav = this.faq_tab_header_Elements[0];
  }

  onLoadQA(_selectedObj){
    this.active_nav = _selectedObj;
    this.selected_tab.emit(this.active_nav);
  }
  

}