import { Component, OnInit, Input } from '@angular/core';
import { StoreService } from '../../../plugins/datastore';
import { BuyBundlesService } from '../../../plugins/buybundles';
import { ProfileService } from '../../../plugins/profile';
// import { bundleMasterConfig } from '../../../app/config/config';
import { bundleMasterConfig } from '../../../../app/config/config';
import { config } from '../../../config';
declare var $: any;
import { Router } from '@angular/router';
import { DbCallsService } from 'src/app/plugins/dbCalls';
import { AngularCDRService } from '../../../plugins/angularCDR';
 import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from 'src/app/plugins/commonAPI';
import { GoogleAnalyticsService } from "../../../plugins/googleanalytics";
import { Location } from '@angular/common';

@Component({
  selector: 'app-buybundlespayment',
  templateUrl: './buybundlespayment.component.html',
  styleUrls: ['./buybundlespayment.component.scss']
})
export class BuybundlespaymentComponent implements OnInit {
  airttt: boolean = true;
  airttt1: boolean = false;
  showsuccessdiv: Boolean = true;
  failurDiv: Boolean = false;
  suceesDiv: Boolean = false;
  showconfimdiv: Boolean = false;



  public colorpayment: boolean = false;
  showLoader: Boolean = false;
  message = '';
  @Input() public masisdnpayment;
  @Input() hideDebit;
  @Input() bundleErrMsg;
  public elementVal: any = [];
  public periodicityData: any = [];
  public active_nav_selected: string;
  public _selectedObj: any;
  public buybundalval: any;
  public buybundalname: any;
  public airtime: boolean = true;
  public msisdnval: any;
  public isDataLoaded: boolean = false;
  public modeOfActivation: number = 1;
  public modeOfPayment: number = 1;
  public modeOfPaymentO;
  @Input() getInternalCardDetails;
  public bundle_Category: any;
  public bundle_SubCategory: any;
  public benefeciaryMSISDN: any;
  public msisdnOthers: any;
  public bundelprice: any;
  public bundelname:any;
  public sessionId:any;
   mesagestatus='';
   startTime: any;
  category: any = 'Buy Bundles Payment';
 value="";
 price:any;
 path: any;
 
 renewal:boolean;

  selecttab: boolean = false;

  eventCategory: any = "Data General";

  constructor(private _profile: ProfileService, private _storeVal: StoreService, private _buyBundleService: BuyBundlesService, private router: Router, private mydb: DbCallsService,private _cdr: AngularCDRService ,private fb: FormBuilder,private apiService: ApiService, private location: Location,
  public googleAnalyticsService: GoogleAnalyticsService) { }

  // @Input() paymentuipagemigerate : boolean = false;

  ngOnInit() {
  this.masisdnpayment=this.masisdnpayment.split("234")[1];
  this.benefeciaryMSISDN=this.getInternalCardDetails['beneficiaryMsisdn'].split("234")[1];
     this.startTime = this._cdr.getTransactionTime(0);
    this.sessionId = this._storeVal.getSessionId();
    // google analytics for selected plan
    this.googleAnalyticsService.eventEmitter("Data General", "Selected Plan", this.getInternalCardDetails['pname'], 1);

  }

  payemntproceed(bundleData) {
    this.showconfimdiv = true;
    this.airtime = false;

  }


  paymentconfirm() {
    this.showLoader = true;
    // this.suceesDiv = true;
    // this.debitpaymebtconfirm();
    //console.log(this.modeOfPaymentO);
    this.bundelprice=this.getInternalCardDetails['price'];
    this.bundelname=this.getInternalCardDetails['subcat'];
     let msisdn: string;
      msisdn = this.getInternalCardDetails['msisdn'];
      
      let action = this.getInternalCardDetails['action'];
      let onlyAction = action.replace("Subscription:", '');
      let price = this.getInternalCardDetails['price'];
      
    // google analytics for confirmation page airtime
    this.googleAnalyticsService.eventEmitter("Data General", "Confirmation (Airtime)", this.getInternalCardDetails['pname'], 1);

      
    if (this.getInternalCardDetails['selfOthers']) {
      
      // google analytics for payment type page - airtime (autorenewal off)
      this.googleAnalyticsService.eventEmitter("Data General", "Payment Type (Airtime)", this.getInternalCardDetails['pname'], 1);
   
      //other 
      this._buyBundleService.getBuyForOthersCIS(msisdn, onlyAction, price, this.getInternalCardDetails['beneficiaryMsisdn'])
        .then((resp) => {
          this.showLoader = false;

          if (resp.ResponseCode === "0" || resp.ResponseCode === 0) {

            // google analytics for successful payment airtime - for others
            this.googleAnalyticsService.eventEmitter("Data General", "Payment Successful (Airtime)", this.getInternalCardDetails['pname'], 1);

	    this.suceesDiv = true;
	    this.showsuccessdiv = false;
            this.failurDiv = false;            
            this.message = resp.ResponseData.ProductBuy.Notification;
            this.mesagestatus = resp.Status;
            this._cdr.writeCDR(this.startTime, this.category,this.bundelname,this.bundelprice, this.message);
          } else {

            // google analytics for unsuccessful payment airtime - for others
            this.googleAnalyticsService.eventEmitter("Data General", "Payment Failure (Airtime)", this.getInternalCardDetails['pname'], 1);

            this.failurDiv = true;
            this.suceesDiv = false;
            this.showsuccessdiv = false;
            this.message = resp.RespDescription;
            this.mesagestatus = resp.Status;
	    this._cdr.writeCDR(this.startTime, this.category,this.bundelname ,this.bundelprice, this.message);          
	    }
        });
    } else {
      // google analytics for payment type page - card (autorenewal off)
      this.googleAnalyticsService.eventEmitter("Data General", "Payment Type (Airtime)", this.getInternalCardDetails['pname'], 1);
      let renewal = 'true';
      if(this.modeOfActivation == 1){
          renewal = 'false';
      }
      //console.log(this.getInternalCardDetails['action']);
      this._buyBundleService.getBuyForSelfCIS(msisdn, onlyAction, price, renewal)
        .then((resp) => {
          this.showLoader = false;
          if (resp.ResponseCode == "0" || resp.ResponseCode == 0) {
            
	    // google analytics for successful payment airtime - for self
            this.googleAnalyticsService.eventEmitter("Data General", "Payment Successful (Airtime)", this.getInternalCardDetails['pname'], 1);
	    
	    this.suceesDiv = true;
            this.showsuccessdiv = false;
            this.failurDiv = false;
            this.message = resp.ResponseData.ProductBuy.Notification;
           this.mesagestatus = resp.Status;
		 this._cdr.writeCDR(this.startTime, this.category,this.bundelname ,this.bundelprice,  this.message);
            // data: { message:'"'+bundleData.description+'" Bundle activated successfully' }
          } else {
            // google analytics for unsuccessful payment airtime - for others
            this.googleAnalyticsService.eventEmitter("Data General", "Payment Failure (Airtime)", this.getInternalCardDetails['pname'], 1);
            this.showsuccessdiv = false;
            this.failurDiv = true;
            this.suceesDiv = false;
            this.message = resp.RespDescription;
            this.mesagestatus = resp.Status;
	    this._cdr.writeCDR(this.startTime, this.category,this.bundelname ,this.bundelprice, this.message);          }
        });
    }


  }

/*
paymentconfirm() {
    this.showLoader = true;   
    
    // google analytics for confirmation page - card
    this.googleAnalyticsService.eventEmitter("Data General", "Confirmation (Card)", this.getInternalCardDetails['pname'], 1);
   
    if (this.getInternalCardDetails['selfOthers']) {

      // google analytics for payment type page - card (autorenewal on)
      this.googleAnalyticsService.eventEmitter("Data General", "Payment Type (Card)", this.getInternalCardDetails['pname'], 1);
   
          this.showLoader = false;
          let renewal = false;
          let action = this.getInternalCardDetails['action'];
          let onlyAction = action.replace("Subscription:", '');
          let price = this.getInternalCardDetails['price'];
 

          const msisdn = this._storeVal.getMSISDNData();
          const email = this._storeVal.getEmailData();
          const firstName = this._storeVal.getFirstNameData();
          const mtnepg_cust_mobile = this.getInternalCardDetails['beneficiaryMsisdn'];
 
       const payload = {
	     'msisdn': this._storeVal.getMSISDNData(),
            'mtnepg_transaction_amount': this.getInternalCardDetails['price'],
            'mtnepg_cust_id': this._storeVal.getMSISDNData(),
            'mtnepg_cust_name': this._storeVal.getFirstNameData(),
            'mtnepg_cust_mobile': this.getInternalCardDetails['beneficiaryMsisdn'],
            'mtnepg_cust_email': this._storeVal.getEmailData(),
            'transaction_type':'Bundle'
          };
        this.path = "/api/debitcarddataplanothers"

  return this.apiService.postRedirectSessionValidation(this.path, msisdn, this.sessionId, payload);


    } else {
    
      // google analytics for payment type page - card (autorenewal off)
      this.googleAnalyticsService.eventEmitter("Data General", "Payment Type (Card)", this.getInternalCardDetails['pname'], 1);
   
     
          this.showLoader = false;
         let  renewal = false;
          let action = this.getInternalCardDetails['action'];
          let onlyAction = action.replace("Subscription:", '');
          let price = this.getInternalCardDetails['price'];

            const payload = {
	          'msisdn': this._storeVal.getMSISDNData(),
            'mtnepg_transaction_amount': this.getInternalCardDetails['price'],
            'mtnepg_cust_id': this._storeVal.getMSISDNData(),
            'mtnepg_cust_name': this._storeVal.getFirstNameData(),
            'mtnepg_cust_mobile': this._storeVal.getMSISDNData(),
            'mtnepg_cust_email': this._storeVal.getEmailData(),
            'transaction_type':'Bundle',
            'only_action' : onlyAction
          };

           this.path ="/api/debitcarddataplanself"
         return this.apiService.postRedirectSessionValidation(this.path, this._storeVal.getMSISDNData(), this.sessionId, payload);    
    }
}*/
  debitpaymebtconfirm() {
    this.showLoader = true;   
    
    // google analytics for confirmation page - card
    this.googleAnalyticsService.eventEmitter("Data General", "Confirmation (Card)", this.getInternalCardDetails['pname'], 1);
   
    if (this.getInternalCardDetails['selfOthers']) {

      // google analytics for payment type page - card (autorenewal on)
      this.googleAnalyticsService.eventEmitter("Data General", "Payment Type (Card)", this.getInternalCardDetails['pname'], 1);
   
          this.showLoader = false;
          let renewal = false;
          let action = this.getInternalCardDetails['action'];
          let onlyAction = action.replace("Subscription:", '');
          let price = this.getInternalCardDetails['price'];
 

          const msisdn = this._storeVal.getMSISDNData();
          const email = this._storeVal.getEmailData();
          const firstName = this._storeVal.getFirstNameData();
          const mtnepg_cust_mobile = this.getInternalCardDetails['beneficiaryMsisdn'];
 
       const payload = {
	          'msisdn': this._storeVal.getMSISDNData(),
            'mtnepg_transaction_amount': this.getInternalCardDetails['price'],
            'mtnepg_cust_id': this._storeVal.getMSISDNData(),
            'mtnepg_cust_name': this._storeVal.getFirstNameData(),
            'mtnepg_cust_mobile': this.getInternalCardDetails['beneficiaryMsisdn'],
            'mtnepg_cust_email': this._storeVal.getEmailData(),
            'transaction_type':'Bundle'
          };
        this.path = "/api/debitcarddataplanothers"

  return this.apiService.postRedirectSessionValidation(this.path, msisdn, this.sessionId, payload);


    } else {
    
      // google analytics for payment type page - card (autorenewal off)
      this.googleAnalyticsService.eventEmitter("Data General", "Payment Type (Card)", this.getInternalCardDetails['pname'], 1);
   
     
          this.showLoader = false;
         let  renewal = false;
          let action = this.getInternalCardDetails['action'];
          let onlyAction = action.replace("Subscription:", '');
          let price = this.getInternalCardDetails['price'];

            const payload = {
	          'msisdn': this._storeVal.getMSISDNData(),
            'mtnepg_transaction_amount': this.getInternalCardDetails['price'],
            'mtnepg_cust_id': this._storeVal.getMSISDNData(),
            'mtnepg_cust_name': this._storeVal.getFirstNameData(),
            'mtnepg_cust_mobile': this._storeVal.getMSISDNData(),
            'mtnepg_cust_email': this._storeVal.getEmailData(),
            'transaction_type':'Bundle',
            'only_action' : onlyAction
          };

           this.path ="/api/debitcarddataplanself"
         return this.apiService.postRedirectSessionValidation(this.path, this._storeVal.getMSISDNData(), this.sessionId, payload);    
    }
}
 



  changeStatus(index) {
    this.colorpayment = true;

  }

  onslectradiopayment(value) {
    if (value == 1) {
      this.airttt = true;
      this.airttt1 = false;
    }
    //console.log("selected  onslectradiopayment value ------ " + value)
    this.modeOfPayment = value;
    localStorage.setItem('modeOfPayment', JSON.stringify(this.modeOfPayment));
  }

  onslectradiopaymentO(value) {
    if (value == 2) {
      this.airttt = false;
      this.airttt1 = true;
    }
    //console.log("selected  onslectradiopaymentO value ------ " + value)
    this.modeOfPaymentO = value;
    //console.log(this.modeOfPaymentO);

  }

  onslectradiomodeofactive(value) {
    //console.log("selected onslectradiomodeofactive value ------ " + value)
    this.modeOfActivation = value;
    localStorage.setItem('modeOfActivation', JSON.stringify(this.modeOfActivation));
  }

  backtologinpayment(){
    this.location.back();
  }
}
