import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybundlespaymentComponent } from './buybundlespayment.component';

describe('BuybundlespaymentComponent', () => {
  let component: BuybundlespaymentComponent;
  let fixture: ComponentFixture<BuybundlespaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybundlespaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybundlespaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
