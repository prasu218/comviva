import { Component, OnInit,EventEmitter,Input,Output } from '@angular/core';

@Component({
  selector: 'app-slidetoggle',
  templateUrl: './slidetoggle.component.html',
  styleUrls: ['./slidetoggle.component.scss']
})
export class SlidetoggleComponent implements OnInit {
  public status: string;
  @Input() label: string;
  @Output() statusValue = new EventEmitter();
  @Input() toggleStatus=[];
  @Output() slideToggleEve = new EventEmitter();
 //@Output() toggleStatus;
  constructor() { }

  ngOnInit() {
  }
  // ngOnChanges(){
  //   console.log("inside on changes"+this.toggleStatus)
  //   //this.toggleStatus = !this.toggleStatus;
  //   if (this.toggleStatus == "true") {
  //     this.toggleStatus = true;
  //     console.log("inside on changes true "+this.toggleStatus)
  //   } else {
  //     this.toggleStatus = false;
  //     console.log("inside on changes false "+this.toggleStatus)
  //   }  
    
  //   this.statusValue.emit(this.status);
  // }


  
  // toggleStatusChange() {
  //   this.toggleStatus = !this.toggleStatus;
  //   console.log('this.toggleStatus---' + this.toggleStatus);
  //   if (this.toggleStatus) {
  //     this.status = 'on';
  //     console.log(" this.status", this.status);
  //   } else {
  //     this.status = 'off';
  //     console.log(" this.status", this.status);
  //   }
    
  //   this.statusValue.emit(this.status);
  // }

  changeSliderToggle(event: any, data:any ){

    this.slideToggleEve.emit(event.target.value);
  }
}
