import 'hammerjs';
import { NgModule,ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule} from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';

import { InputComponent } from './component/input/input.component';
import { TableviewComponent } from './component/tableview/tableview.component';
import { CouroselComponent } from './component/courosel/courosel.component';
import { ListViewComponent } from './component/list-view/list-view.component';
import { InternalcardsComponent } from './component/internalcards/internalcards.component';
import { ButtonComponent } from './component/button/button.component';
import { CardsComponent } from './component/cards/cards.component';
import { ButtonnewComponent} from './component/buttonnew/buttonnew.component';
import { ButtonviewComponent} from './component/buttonview/buttonview.component';
import { ButtonwhiteComponent} from './component/buttonwhite/buttonwhite.component';
import { BackbuttonComponent} from './component/backbutton/backbutton.component';
import { InputFloatingComponent } from './component/input-floating/input-floating.component';
import { DashyellowComponent} from './component/dashyellow/dashyellow.component';
import { HomecardComponent} from './component/homecard/homecard.component';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { MainbuttonComponent} from './component/mainbutton/mainbutton.component';
import { SlimloaderComponent} from './component/slimloader/slimloader.component';
import { XtravaluetabsComponent } from './component/xtravaluetabs/xtravaluetabs.component';
import { SuccessComponent } from './component/success/success.component';
import { FailureComponent } from './component/failure/failure.component';
import { TabsnormalComponent } from './component/tabsnormal/tabsnormal.component';
import { SlidetoggleComponent } from './component/slidetoggle/slidetoggle.component';
import { CurveComponent } from './component/curve/curve.component';
import { BuybundlespaymentComponent} from './component/buybundlespayment/buybundlespayment.component';
import { ClickOutsideModule } from 'ng-click-outside';
import { ConfirmprofileComponent } from './component/confirmprofile/confirmprofile.component';
import { ProfileinputComponent } from './component/profileinput/profileinput.component';
import { GridModule } from '@syncfusion/ej2-angular-grids';
import { ResizeService, PageService } from '@syncfusion/ej2-angular-grids';
import {
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatListModule,
  MatCardModule,
  MatExpansionModule,
  MatDialogModule,
  MatCheckboxModule,
  MatRadioModule,
  MatTableModule,
  MatSliderModule,
  MatDividerModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MAT_LABEL_GLOBAL_OPTIONS,
  MAT_DATE_LOCALE
} from '@angular/material';



import {
  searchPipe,
} from './pipes';
import { DatepickerComponent } from './component/datepicker/datepicker.component';
import { GridtableComponent } from './component/gridtable/gridtable.component';
import { BuybundleProccedComponent } from './component/buybundle-procced/buybundle-procced.component';
const COM_COMPONENTS = [
 SlimloaderComponent,
  HeaderComponent,
  FooterComponent,
  InputComponent,
  InputFloatingComponent,
 ProfileinputComponent,
  ConfirmprofileComponent,
  ListViewComponent,
  CardsComponent,
  TableviewComponent,
  CouroselComponent,
  InternalcardsComponent,
  ButtonComponent,
  ButtonnewComponent,
  ButtonviewComponent,
  ButtonwhiteComponent,
  BackbuttonComponent,
  HomecardComponent,
  DashyellowComponent,
  MainbuttonComponent,
  TabsnormalComponent,
  SlidetoggleComponent,
  XtravaluetabsComponent,
BuybundlespaymentComponent,
 SuccessComponent,
  FailureComponent,
  DatepickerComponent,
  GridtableComponent,
  CurveComponent,
  BuybundleProccedComponent

];
const MAT_MODULES = [
  MatButtonModule,
  MatInputModule,
  MatToolbarModule,
  MatIconModule,
  MatProgressBarModule,
  MatSelectModule,
  MatSidenavModule,
  MatListModule,
  MatCardModule,
  MatExpansionModule,
  MatSlideToggleModule,
  MatDialogModule, 
  MatCheckboxModule,
  MatRadioModule,
  MatTableModule,
  MatSliderModule,
  MatPaginatorModule,
  MatDividerModule,
  MatFormFieldModule,
  MatDatepickerModule,
  MatNativeDateModule,
  ClickOutsideModule
];
const COM_PIPES = [
  searchPipe
];

@NgModule({
  declarations: [...COM_COMPONENTS, ...COM_PIPES, InputFloatingComponent, DatepickerComponent],
  imports: [
    ...MAT_MODULES,
    CommonModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MatCardModule,
    GridModule,
    StoreModule.forFeature('theme', reducers),
 ],
 exports:[...COM_COMPONENTS,...MAT_MODULES,...COM_PIPES ],
 providers: [
  {provide: MAT_LABEL_GLOBAL_OPTIONS, useValue: {float: 'always'}},
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    ResizeService, PageService
]
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ThemeModule,
      providers: [
      ]
    };
  }
}


 
    

