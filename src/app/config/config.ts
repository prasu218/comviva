export const icons = {
  'right_arrow': 'fa-arrow-right',
  'left_arrow': 'fa-arrow-left',
  'redo': 'fa-repeat',
  'logout': 'fa-sign-out',
  'change': 'fa-exchange',
  'plus' : 'fa-plus'
};

export const type = {
  'filled': 'filled',
  'outline': 'borderline'
};

export const size = {
  'full': 'full-btn',
  'quarter': 'quater-btn',
  'half': 'half-btn',
  'round': 'round-btn'
};

export const maxAccountHistoryDate = {
  'maxDays': 6, // Max days should be one day less than total days, bcoz days consider from (0-6)
  'dataMaxDays':3 // Max days should be one day less than total days, bcoz days consider from (0-6)
}


export const Hotdeals_resp = { 
  "ProductDetails": [ {
    
    "ProductName": "100MB Daily Plan",
    "ProductID": "307",
    "Category": "DataPlan",
    "SubCategory": "DataPlan Daily",
    "Price": "100",
    "Validity": "1 day",
    "Renewal": "true",
    "isConsentRequired": "true",
    "BuyForOthers": "true",
    "PaymentMode": "SCAPv2",
    "Description": "Get 100MB for N100. Valid for 24 hrs",
    "Status": "ACTIVE_FOR_MARKET",
    "OfferId": "14228",
    "GracePeriod": "7 days",
    "DataShareDenomination": "NA",
    "FixedRenewalDate": "NA",
    "LastModifiedDate": "2019-8-3 1:1:5",
    "Parking": "Yes",
    "optout": "Opt-Out:307",
    "optin": "Opt-In:307",
    "Action": "Subscription:RACT_NG_Data_307"
    },
  { 
  "ProductName":"Xtradata 1000 Monthly Bundle",
  "ProductID":"92",
  "Category":"Data and Voice",
  "SubCategory":"XTRADATA",
  "Price":"1000",
  "Validity":"30 days",
  "Renewal":"true",
  "isConsentRequired":"true",
  "BuyForOthers":"false",
  "PaymentMode":"SCAPv2",
  "Description":"Gives 1GB Data and N2000 Talk Time, valid for 30 Days",
  "Status":"ACTIVE_FOR_MARKET",
  "OfferId":"25032",
  "GracePeriod":"0 day",
  "DataShareDenomination":"NA",
  "FixedRenewalDate":"NA",
  "LastModifiedDate":"2019-9-6 1:31:59",
  "Parking":"No",
  "Action":"Subscription:RACT_NG_Combo_92",
  "eligibiltyId":"EligibilityCheck:ECHK_NG_Combo_92",
  "optin":"Opt-In:92",
  "optout":"Opt-Out:92"
}, { 
  "ProductName":"100GB 60 Days Plan",
  "ProductID":"210",
  "Category":"DataPlan",
  "SubCategory":"DataPlan 60Days",
  "Price":"30000",
  "Validity":"60 days",
  "Renewal":"true",
  "isConsentRequired":"true",
  "BuyForOthers":"true",
  "PaymentMode":"SCAPv2",
  "Description":"Get 100GB for N30000. Valid for 60 days.",
  "Status":"ACTIVE_FOR_MARKET",
  "OfferId":"969",
  "GracePeriod":"30 days",
  "DataShareDenomination":"NA",
  "FixedRenewalDate":"NA",
  "LastModifiedDate":"2019-4-6 1:55:55",
  "Parking":"Yes",
  "Action":"Subscription:RACT_NG_Data_210",
  "deactivationId":"RSDeprovisionProduct:DACT_NG_Data_210",
  "optin":"Opt-In:210",
  "optout":"Opt-Out:210"
}, { 
  "ProductName":"3GB Monthly Plan",
  "ProductID":"211",
  "Category":"DataPlan",
  "SubCategory":"DataPlan Monthly",
  "Price":"1500",
  "Validity":"30 days",
  "Renewal":"true",
  "isConsentRequired":"true",
  "BuyForOthers":"true",
  "PaymentMode":"SCAPv2",
  "Description":"Get 3GB for N1500. Valid for 30 days.",
  "Status":"ACTIVE_FOR_MARKET",
  "OfferId":"10312",
  "GracePeriod":"30 days",
  "DataShareDenomination":"NA",
  "FixedRenewalDate":"NA",
  "LastModifiedDate":"2019-4-5 10:57:52",
  "Parking":"Yes",
  "Action":"Subscription:RACT_NG_Data_211",
  "deactivationId":"RSDeprovisionProduct:DACT_NG_Data_211",
  "optin":"Opt-In:211",
  "optout":"Opt-Out:211"
},  { 
  "ProductName":"15GB Monthly Plan",
  "ProductID":"212",
  "Category":"DataPlan",
  "SubCategory":"DataPlan Monthly",
  "Price":"6000",
  "Validity":"30 days",
  "Renewal":"true",
  "isConsentRequired":"true",
  "BuyForOthers":"true",
  "PaymentMode":"SCAPv2",
  "Description":"Get 15GB for N6000. Valid for 30 days.",
  "Status":"ACTIVE_FOR_MARKET",
  "OfferId":"10313",
  "GracePeriod":"30 days",
  "DataShareDenomination":"NA",
  "FixedRenewalDate":"NA",
  "LastModifiedDate":"2019-4-5 10:58:28",
  "Parking":"Yes",
  "Action":"Subscription:RACT_NG_Data_212",
  "deactivationId":"RSDeprovisionProduct:DACT_NG_Data_212",
  "optin":"Opt-In:212",
  "optout":"Opt-Out:212"
}]
}

export const getRoamingRatesCountries_Response = {
    "AppsName": "selfcare",
      "Format": "json",
      "plugin": "dba",
    "prefLang": "EN",
      "transID": "6458096134",
      "dbName": "appDB",
      "query": "getRoamingRatesCountries",
      "opcoID": "NG",
      "msisdn": "2349062058855",
      "message": "success",
      "Interface": "client",
      "status": "0",
      "prodInfo": [{
        "COUNTRY": "Please select the country"
      },
  {
  
		"COUNTRY": "Afghanistan"
	}, {
		"COUNTRY": "Albania"
	}, {
		"COUNTRY": "Algeria"
	}, {
		"COUNTRY": "Angola"
	}, {
		"COUNTRY": "Anguilla"
	}, {
		"COUNTRY": "Antigua"
	}, {
		"COUNTRY": "Argentina"
	}, {
		"COUNTRY": "Armenia"
	}, {
		"COUNTRY": "Australia"
	}, {
		"COUNTRY": "Austria"
	}, {
		"COUNTRY": "Azerbaijan"
	}, {
		"COUNTRY": "Bahamas"
	}, {
		"COUNTRY": "Bahrain"
	}, {
		"COUNTRY": "Bangladesh"
	}, {
		"COUNTRY": "Barbados"
	}, {
		"COUNTRY": "Belarus"
	}, {
		"COUNTRY": "Belgium"
	}, {
		"COUNTRY": "Belize"
	}, {
		"COUNTRY": "Benin Republic"
	}, {
		"COUNTRY": "Bermuda"
	}, {
		"COUNTRY": "Bosnia & Herzegovina"
	}, {
		"COUNTRY": "Botswana"
	}, {
		"COUNTRY": "Brazil"
	}, {
		"COUNTRY": "British Virgin Islands"
	}, {
		"COUNTRY": "Bulgaria"
	}, {
		"COUNTRY": "Burkina Faso"
	}, {
		"COUNTRY": "Burundi"
	}, {
		"COUNTRY": "Cambodia"
	}, {
		"COUNTRY": "Cameroon"
	}, {
		"COUNTRY": "Canada"
	}, {
		"COUNTRY": "Cape Verde"
	}, {
		"COUNTRY": "Cayman Islands"
	}, {
		"COUNTRY": "Centrafrique"
	}, {
		"COUNTRY": "Chad"
	}, {
		"COUNTRY": "Chile"
	}, {
		"COUNTRY": "China"
	}, {
		"COUNTRY": "Colombia"
	}, {
		"COUNTRY": "Congo Brazaville"
	}, {
		"COUNTRY": "Congo DRC"
	}, {
		"COUNTRY": "Cote d'Ivoire"
	}, {
		"COUNTRY": "Croatia"
	}, {
		"COUNTRY": "Cuba"
	}, {
		"COUNTRY": "Curacao"
	}, {
		"COUNTRY": "Cyprus"
	}, {
		"COUNTRY": "Czech"
	}, {
		"COUNTRY": "Denmark"
	}, {
		"COUNTRY": "Djibouti"
	}, {
		"COUNTRY": "Dominica"
	}, {
		"COUNTRY": "Ecuador"
	}, {
		"COUNTRY": "Egypt"
	}, {
		"COUNTRY": "El Salvador"
	}, {
		"COUNTRY": "Equatorial Guinea"
	}, {
		"COUNTRY": "Estonia"
	}, {
		"COUNTRY": "Ethiopia"
	}, {
		"COUNTRY": "Faroe Islands"
	}, {
		"COUNTRY": "Fiji"
	}, {
		"COUNTRY": "Finland"
	}, {
		"COUNTRY": "France"
	}, {
		"COUNTRY": "Gabon"
	}, {
		"COUNTRY": "Gambia"
	}, {
		"COUNTRY": "Georgia"
	}, {
		"COUNTRY": "Germany"
	}, {
		"COUNTRY": "Ghana"
	}, {
		"COUNTRY": "Gibraltar"
	}, {
		"COUNTRY": "Greece"
	}, {
		"COUNTRY": "Grenada"
	}, {
		"COUNTRY": "Guatemala"
	}, {
		"COUNTRY": "Guernsey"
	}, {
		"COUNTRY": "Guinea Bissau"
	}, {
		"COUNTRY": "Guinea Conakry"
	}, {
		"COUNTRY": "Guyana"
	}, {
		"COUNTRY": "Haiti"
	}, {
		"COUNTRY": "Honduras"
	}, {
		"COUNTRY": "Hong Kong"
	}, {
		"COUNTRY": "Hungary"
	}, {
		"COUNTRY": "Iceland"
	}, {
		"COUNTRY": "India"
	}, {
		"COUNTRY": "Indonesia"
	}, {
		"COUNTRY": "International Airspace"
	}, {
		"COUNTRY": "International Networks"
	}, {
		"COUNTRY": "Iran"
	}, {
		"COUNTRY": "Iraq"
	}, {
		"COUNTRY": "Ireland"
	}, {
		"COUNTRY": "Israel"
	}, {
		"COUNTRY": "Italy"
	}, {
		"COUNTRY": "Jamaica"
	}, {
		"COUNTRY": "Japan"
	}, {
		"COUNTRY": "Jordan"
	}, {
		"COUNTRY": "Kazakhstan"
	}, {
		"COUNTRY": "Kenya"
	}, {
		"COUNTRY": "Kuwait"
	}, {
		"COUNTRY": "Kyrgyz Republic "
	}, {
		"COUNTRY": "Lao"
	}, {
		"COUNTRY": "Latvia"
	}, {
		"COUNTRY": "Lebanon"
	}, {
		"COUNTRY": "Lesotho"
	}, {
		"COUNTRY": "Liberia"
	}, {
		"COUNTRY": "Libya"
	}, {
		"COUNTRY": "Liechtenstein"
	}, {
		"COUNTRY": "Lithuania"
	}, {
		"COUNTRY": "Luxembourg"
	}, {
		"COUNTRY": "Macau"
	}, {
		"COUNTRY": "Macedonia"
	}, {
		"COUNTRY": "Madagascar"
	}, {
		"COUNTRY": "Malawi"
	}, {
		"COUNTRY": "Malaysia"
	}, {
		"COUNTRY": "Maldives"
	}, {
		"COUNTRY": "Mali"
	}, {
		"COUNTRY": "Malta"
	}, {
		"COUNTRY": "Mauritania"
	}, {
		"COUNTRY": "Mauritius"
	}, {
		"COUNTRY": "Mexico"
	}, {
		"COUNTRY": "Monaco"
	}, {
		"COUNTRY": "Mongolia"
	}, {
		"COUNTRY": "Montenegro"
	}, {
		"COUNTRY": "Montserrat"
	}, {
		"COUNTRY": "Morocco"
	}, {
		"COUNTRY": "Mozambique"
	}, {
		"COUNTRY": "Namibia"
	}, {
		"COUNTRY": "Netherlands"
	}, {
		"COUNTRY": "New Zealand"
	}, {
		"COUNTRY": "Nicaragua"
	}, {
		"COUNTRY": "Niger"
	}, {
		"COUNTRY": "Norway"
	}, {
		"COUNTRY": "Oman "
	}, {
		"COUNTRY": "Pakistan"
	}, {
		"COUNTRY": "Palestine"
	}, {
		"COUNTRY": "Panama"
	}, {
		"COUNTRY": "Papua new Guinea"
	}, {
		"COUNTRY": "Paraguay"
	}, {
		"COUNTRY": "Peru"
	}, {
		"COUNTRY": "Philippines"
	}, {
		"COUNTRY": "Poland"
	}, {
		"COUNTRY": "Portugal"
	}, {
		"COUNTRY": "Puerto Rico"
	}, {
		"COUNTRY": "Qatar"
	}, {
		"COUNTRY": "Reunion"
	}, {
		"COUNTRY": "Romania"
	}, {
		"COUNTRY": "Russia"
	}, {
		"COUNTRY": "Rwanda"
	}, {
		"COUNTRY": "Samoa"
	}, {
		"COUNTRY": "Sao Tome & Principle"
	}, {
		"COUNTRY": "Saudi Arabia"
	}, {
		"COUNTRY": "Senegal"
	}, {
		"COUNTRY": "Serbia"
	}, {
		"COUNTRY": "Seychelles"
	}, {
		"COUNTRY": "Sierra Leone"
	}, {
		"COUNTRY": "Singapore"
	}, {
		"COUNTRY": "Slovenia"
	}, {
		"COUNTRY": "South Africa"
	}, {
		"COUNTRY": "South Korea "
	}, {
		"COUNTRY": "South Sudan"
	}, {
		"COUNTRY": "South sudan"
	}, {
		"COUNTRY": "Spain"
	}, {
		"COUNTRY": "Sri Lanka"
	}, {
		"COUNTRY": "St. Kitts"
	}, {
		"COUNTRY": "St. Lucia"
	}, {
		"COUNTRY": "St. Vincent and the Grenadines"
	}, {
		"COUNTRY": "Sudan"
	}, {
		"COUNTRY": "Swaziland"
	}, {
		"COUNTRY": "Sweden"
	}, {
		"COUNTRY": "Switzerland"
	}, {
		"COUNTRY": "Syria"
	}, {
		"COUNTRY": "Taiwan"
	}, {
		"COUNTRY": "Tajikistan"
	}, {
		"COUNTRY": "Tanzania"
	}, {
		"COUNTRY": "Thailand"
	}, {
		"COUNTRY": "Timor Leste"
	}, {
		"COUNTRY": "Togo"
	}, {
		"COUNTRY": "Trinidad and Tobago"
	}, {
		"COUNTRY": "Tunisia"
	}, {
		"COUNTRY": "Turkey"
	}, {
		"COUNTRY": "Turks and Caicos"
	}, {
		"COUNTRY": "UAE"
	}, {
		"COUNTRY": "USA"
	}, {
		"COUNTRY": "Uganda"
	}, {
		"COUNTRY": "Ukraine"
	}, {
		"COUNTRY": "United Kingdom"
	}, {
		"COUNTRY": "Uruguay"
	}, {
		"COUNTRY": "Uzbekistan"
	}, {
		"COUNTRY": "Venezuela"
	}, {
		"COUNTRY": "Vietnam"
	}, {
		"COUNTRY": "Yemen"
	}, {
		"COUNTRY": "Zambia"
	}, {
		"COUNTRY": "Zimbabwe"
	}]
}

export const buyBundlesGdsResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getBundleSubCategory",
                "values": "XtraData;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "VALIDITY": "Providers"
                }, {
                                "VALIDITY": "Consumers"
                }]
}

export const buyBundlesGdsWeeklyResponse_Array ={
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}
export const buyBundlesGdsMonthlyResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300 Monthly Facebook",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500 Monthly Facebook",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000 Monthly Facebook",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}


export const config = {
  ASSETS: 'app/appassets',
  PLUGIN_URL : 'http://172.16.5.239:9300/app/api/',
  MIN_MSISDN_LENGTH: 10,
  MAX_MSISDN_LENGTH: 11,
  MSISDN_LENGTH: 15,
  MIN_PIN_LENGTH: 4,
  MAX_PIN_LENGTH: 4,
  MAX_VOUCHER_CODE: 17,
  MIN_VOUCHER_CODE:17,
  COUNTRY_CODE: 234,
  SHOW_OTP: 'agent'
};

export const captchaVerifyResponse_Array = {
    'success': true,
    'challenge_ts': '2018-09-04T06:26:01Z',
    'hostname': '127.0.0.1'
};


export const bundleMasterConfig = {
                "prodInfo": [{ "id": "dataplans", "value": "Data plans" , "activationChannel" : "CIS", "bundleCategory" :"DataPlan", "bundleSubCategory":["Hotdeals","DataPlan Daily","DataPlan Weekly","DataPlan Monthly","DataPlan 60Days","DataPlan 90Days","DataPlan 180Days","DataPlan Yearly"],"bundleTabs":["Hot Deals","Daily","Weekly","Monthly","2 Months","3 Months","6 Months","Yearly"] },
                { "id": "xtraValue", "value": "XtraValue" , "activationChannel" : "CIS", "bundleCategory" :"Data and Voice", "bundleSubCategory":["XTRADATA","XTRATALK"],"bundleTabs":["Weekly","Monthly"]},
                { "id": "intlcalling", "value": "Int'l calling" , "activationChannel" : "CIS", "bundleCategory" :"IDB", "bundleSubCategory":["IDB300","IDB500","IDB1500"],"bundleTabs":["Daily","Weekly","Monthly"]},
                { "id": "roaming", "value": "Roaming" , "activationChannel" : "CIS", "bundleCategory" :"Roaming Bundles", "bundleSubCategory":["Roaming Data Bundle","Roaming Voice Bundle","Roaming Voice and Data Bundle","Roaming Data MTN Countries","Roaming UAE","Roaming Discount Call Rate"],"bundleTabs":["Roaming Data Bundle","Roaming Voice Bundle","Roaming Voice and Data Bundle"]},             
                

                {
                  "id": "goodybag",
                  "value": "Goodybag",
                  "activationChannel": "CIS",
                  "bundleCategory": "GoodyBag",
                  "bundleSubCategory": {
                    "Whatsapp": ["Daily", "Weekly", "Monthly"],
                    "Facebook": ["Daily", "Weekly", "Monthly"],
                    "Instagram": ["Daily", "Weekly", "Monthly"],
                    "2Go": ["Daily", "Weekly", "Monthly"],
                    "Wechat": ["Daily", "Weekly", "Monthly"],
                    "ESKIMI": ["Daily", "Weekly", "Monthly"],
                    "Youtube and Instagram": ["Daily"],
                    "Social Bundle": ["Daily", "Weekly", "Monthly"]
              }
              },

                
                { 
                  "id": "smebundles",
                   "value": "SMEBundles",
                    "activationChannel" : "CIS",
                    "bundleCategory" :"SME Bundles",
                    "bundleSubCategory":{
                      "SME BizPlus": ["Monthly"],
                      "SME Data Share Bundles": ["Monthly", "2 Months"]
                    }
                    },


                { "id": "hynet", "value": "SMEBundles" , "activationChannel" : "CIS", "bundleCategory" :"SME Bundles", "bundleSubCategory":["New SME Bundles","New SME Bundles","New SME Bundles","New SME Bundles","New SME Bundles"],"bundleTabs":["Monthly","2 Month","6 Month","1 Year"] },
                { "id": "intcall", "value": "Int\'l Call" , "activationChannel" : "CIS", "bundleCategory" :"IDB", "bundleSubCategory":["IDB300","IDB500","IDB1500"],"bundleTabs":["Daily","Weekly","Monthly"]},
                { "id": "hynet", "value": "SMEBundles" , "activationChannel" : "CIS", "bundleCategory" :"SME Bundles", "bundleSubCategory":["New SME Bundles","New SME Bundles"],"bundleTabs":["Weekly","Monthly"] },
                ]

};

export const loginResponse_Array = {
  'status_code': 0,
  'status_message': 'OK',
  'transaction_id': '1829907244',
  'sender_id': 'AGL',
  'first_login': 'N'
};




  
export const profileResponse_Array = {
  'status_code': '0',
  'status_message': 'Success',
  'subsType': 'Prepaid',
  'customer_Profile': {
    'title': '',
    'firstName': 'Joseph',
    'familyName': 'Emiowele',
    'dob': '19860101',
    'activationDate': '28/06/2016',
    'primaryNumber': '8143995081',
    'alternateNumber': '8143995081',
    'email': ''
  }
};

export const requestpinResponse_Array = {
  'status_code': '1',
  'status_message': 'Failure',
  'add_status_code': '119',
  'add_status_message': 'Account Is Already In Active Status.You Can Proceed To Login.'
};


export const rechargeResponse_Array = {
  'status_code': '0',
  'add_status_code': '1',
  'status_message': 'Failure',
  'transaction_id': '2777173216'
};


export const rechargeCardStatusResponse_Array = {
                
                "status_code": 0,
                "status_message": "OK",
                "add_status_code": "0",
                "add_status_message": "No Error",
                "transaction_id": "3756871220",
                "card_status": {
                                "voucherInfo": "Used",
                                "status": "Used",
                                "cost": "200",
                                "serialDetails": "Voucher is already used:,Msisdn :8064441129,Date Recharge :20/07/2018 10:05:43 PM,"
                }
                

};

export const rechargeDamagedResponse_Array = {
  'status_code': '1',
  'add_status_code': '1',
  'status_message': 'Failure',
  'transaction_id': '2777173216'
};

export const prepaidBalanceResponse_Array = {
  'status': 0,
  'status_message': 'OK',
  'transaction_id': '1610204292',
  'result': [{
    'Amount': '13.4',
    'planName': '44',
    'planExpiryDate': '2037-12-30T12:00:00Z'
  }]
};

export const postpaidBalanceResponse_Array = {
  'status': 0,
  'status_message': 'OK',
  'transaction_id': '1610292',
  'result': [{
    'Amount': '590.55',
    'Amount1': '50000',
    'Amount4': '5247.31',
    'planName': '402',
    'planExpiryDate': '2037-12-29T12:00:00Z'
  }]
};

export const bundleOfferResponse_Array = {
  'status_code': 0,
  'status_message': 'OK',
  'transaction_id': '1610334245',
  'sender_id': '1610334245NGCS5',
  'subscriptionListconfig_result': [{
                                'offerid': '45'
                }, {
                                'offerid': '75'
                }, {
                                'offerid': '162'
                }, {
                                'offerid': '217'
                }, {
                                'offerid': '422'
                }, {
                                'offerid': '423'
                }, {
                                'offerid': '503'
                }, {
                                'offerid': '970'
                }, {
                                'offerid': '970'
                }, {
                                'offerid': '970'
                }, {
                                'offerid': '971'
                }, {
                                'offerid': '1024'
                }, {
                                'offerid': '1690'
                }, {
                                'offerid': '1972'
                }, {
                                'offerid': '4021'
                }, {
                                'offerid': '5043'
                }, {
                                'offerid': '12154'
                }, {
                                'offerid': '13650'
                }, {
                                'offerid': '13835'
                }, {
                                'offerid': '60101'
                }]
};

export const accountHistoryResponse_Array = {
                'status_code': 0,
                'status_message': 'OK',
                'transaction_id': '1610380785',
                'sender_id': 'ODS',
                'call_details': [{
                                'callingPartyNum': 'null',
                                'calledPartyNum': 'internet',
                                'data': '6608445',
                                'type': 'GPRS',
                                'callDateTime': '2018-07-13T23:42:56.000+01:00',
                                'callDuration': '0',
                                'callAmount': '3.227'
                }, {
                                'callingPartyNum': 'null',
                                'calledPartyNum': 'internet',
                                'data': '815916',
                                'type': 'GPRS',
                                'callDateTime': '2018-07-13T23:42:56.000+01:00',
                                'callDuration': '0',
                                'callAmount': '0.3985'
                }, {
                                'callingPartyNum': 'null',
                                'calledPartyNum': 'internet',
                                'data': '7583906',
                                'type': 'GPRS',
                                'callDateTime': '2018-07-13T23:26:31.000+01:00',
                                'callDuration': '0',
                                'callAmount': '3.7035'
                }, {
                                'callingPartyNum': 'null',
                                'calledPartyNum': 'internet',
                                'data': '34541',
                                'type': 'GPRS',
                                'callDateTime': '2018-07-13T23:26:31.000+01:00',
                                'callDuration': '0',
                                'callAmount': '0.017'
                }, {
                                'callingPartyNum': 'null',
                                'calledPartyNum': '2348064457068',
                                'data': '0',
                                'type': 'Voice',
                                'callDateTime': '2018-07-13T22:50:39.000+01:00',
                                'callDuration': '738',
                                'callAmount': '81.18'
                }, {
                                'callingPartyNum': 'null',
                                'calledPartyNum': '',
                                'data': '',
                                'type': 'Adjustment DA',
                                'callDateTime': '2018-07-13T22:49:14.000+01:00',
                                'callDuration': '',
                                'callAmount': '85'
                }, {
                                'callingPartyNum': 'null',
                                'calledPartyNum': '',
                                'data': '',
                                'type': 'Adjustment Main',
                                'callDateTime': '2018-07-13T22:49:14.000+01:00',
                                'callDuration': '',
                                'callAmount': '100'
                }, {
                                'callingPartyNum': 'null',
                                'calledPartyNum': '2348064457068',
                                'data': '0',
                                'type': 'Voice',
                                'callDateTime': '2018-07-13T22:35:30.000+01:00',
                                'callDuration': '846',
                                'callAmount': '98.56'
                }, {
                                'callingPartyNum': 'null',
                                'calledPartyNum': '',
                                'data': '',
                                'type': 'Recharge DA',
                                'callDateTime': '2018-07-13T22:33:21.000+01:00',
                                'callDuration': '',
                                'callAmount': '10.24'
                }, {
                                'callingPartyNum': 'null',
                                'calledPartyNum': '',
                                'data': '',
                                'type': 'Recharge DA',
                                'callDateTime': '2018-07-13T22:33:21.000+01:00',
                                'callDuration': '',
                                'callAmount': '200'
                }, {
                                'callingPartyNum': 'null',
                                'calledPartyNum': '',
                                'data': '',
                                'type': 'Recharge Main',
                                'callDateTime': '2018-07-13T22:33:21.000+01:00',
                                'callDuration': '',
                                'callAmount': '200'
                }, {
                                'callingPartyNum': 'null',
                                'calledPartyNum': '2348142553515',
                                'data': '0',
                                'type': 'Voice',
                                'callDateTime': '2018-07-09T11:07:14.000+01:00',
                                'callDuration': '11',
                                'callAmount': '1.87'
                }, {
                                'callingPartyNum': 'null',
                                'calledPartyNum': '2347032233333',
                                'data': '0',
                                'type': 'Voice',
                                'callDateTime': '2018-07-09T09:30:53.000+01:00',
                                'callDuration': '44',
                                'callAmount': '9.68'
                }, {
                                'callingPartyNum': 'null',
                                'calledPartyNum': 'internet',
                                'data': '0',
                                'type': 'USSD *540*240*50',
                                'callDateTime': '2018-07-09T07:48:53.000+01:00',
                                'callDuration': '0',
                                'callAmount': '2'
                }]
};

export const bundleAPIResponse_Array = {
                'status_code': 0,
                'status_message': 'OK',
                'transaction_id': '1610334224',
                'balance_result': [{
                                'mapid': '5',
                                'used': '2100.00'
                }, {
                                'mapid': '191',
                                'used': '3.16',
                                'total': '128.00'
                }, {
                                'mapid': '222',
                                'used': '256.00',
                                'total': '256.00'
                }, {
                                'mapid': '222',
                                'used': '256.00',
                                'total': '256.00'
                }, {
                                'mapid': '222',
                                'used': '256.00',
                                'total': '256.00'
                }, {
                                'mapid': '620',
                                'used': '25.60',
                                'total': '25.60'
                }, {
                                'mapid': '621',
                                'used': '76.80',
                                'total': '76.80'
                }, {
                                'mapid': '622',
                                'used': '29.35',
                                'total': '-11.26'
                }, {
                                'mapid': '623',
                                'used': '7.68',
                                'total': '7.68'
                }, {
                                'mapid': '1001',
                                'used': '10.00'
                }, {
                                'mapid': '1908',
                                'used': '576.00'
                }, {
                                'mapid': '2002'
                }, {
                                'mapid': '2091'
                }, {
                                'mapid': '3000',
                                'used': '1453.19'
                }, {
                                'mapid': '3003',
                                'used': '200.00'
                }, {
                                'mapid': '3501'
                }, {
                                'mapid': '3507',
                                'used': '745.68'
                }, {
                                'mapid': '4305',
                                'used': '25907.00'
                }, {
                                'mapid': '5001'
                }, {
                                'mapid': '5003',
                                'used': '29.35',
                                'total': '257.02'
                }, {
                                'mapid': '5044'
                }, {
                                'mapid': '5046'
                }, {
                                'mapid': '5049'
                }, {
                                'mapid': '5100',
                                'used': '197.16'
                }, {
                                'mapid': '7000',
                                'used': '5464.67'
                }, {
                                'mapid': '8000',
                                'used': '2504.12'
                }, {
                                'mapid': '9000',
                                'used': '1356.52'
                }, {
                                'mapid': '9632',
                                'used': '30.08',
                                'total': '10000.00'
                }, {
                                'mapid': '9824',
                                'used': '2.02',
                                'total': '5.12'
                }, {
                                'mapid': '25',
                                'used': '0',
                                'total': '76.80'
                }, {
                                'mapid': '26',
                                'used': '0',
                                'total': '380.00'
                }, {
                                'mapid': '191',
                                'used': '0',
                                'total': '128.00'
                }, {
                                'mapid': '222',
                                'used': '0',
                                'total': '256.00'
                }, {
                                'mapid': '1439',
                                'total': ''
                }, {
                                'mapid': '5004',
                                'used': '0',
                                'total': '.'
                }, {
                                'mapid': '9652',
                                'total': ''
                }, {
                                'mapid': '9701',
                                'total': ''
                }],
                'da_result': [{
                                'daid': '1',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '2',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '3',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '4',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '5',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '6',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '7',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '8',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '9',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '10',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '12',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '13',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '18',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '22',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '29',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '31',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '34',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '36',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '38',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '54',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '61',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '75',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '85',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '88',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '99',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '100',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '2019-01-01T12:00:00+00'
                }, {
                                'daid': '104',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '107',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '116',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '121',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '2018-07-21T12:00:00+00'
                }, {
                                'daid': '128',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '137',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '138',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '140',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '145',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '170',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '180',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '189',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '190',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '191',
                                'balance': '12484',
                                'startDate': '',
                                'expiryDate': ''
                }, {
                                'daid': '191',
                                'balance': '12800',
                                'startDate': '',
                                'expiryDate': ''
                }, {
                                'daid': '191',
                                'balance': '12800',
                                'startDate': '',
                                'expiryDate': ''
                }, {
                                'daid': '197',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '199',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '211',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '213',
                                'balance': '22767',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '2018-07-24T12:00:00+00'
                }, {
                                'daid': '222',
                                'balance': '0',
                                'startDate': '',
                                'expiryDate': ''
                }, {
                                'daid': '222',
                                'balance': '0',
                                'startDate': '',
                                'expiryDate': ''
                }, {
                                'daid': '222',
                                'balance': '0',
                                'startDate': '',
                                'expiryDate': ''
                }, {
                                'daid': '240',
                                'balance': '46031742',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '247',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '249',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '252',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '253',
                                'balance': '0',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '9999-12-31T00:00:00+12'
                }, {
                                'daid': '254',
                                'balance': '430000',
                                'startDate': '9999-12-31T00:00:00+12',
                                'expiryDate': '2018-07-31T12:00:00+00'
                }]
};

export const buyBundlesResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3290776903",
                "dbName": "appDB",
                "query": "getBundleCategoryNew",
                "values": "Data;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "VALIDITY": "Daily"
                }, {
                                "VALIDITY": "Monthly"
                }, {
                                "VALIDITY": "Weekly"
                }, {
                                "VALIDITY": "2 Months"
                }]
};

export const buyBundlesDailyData_Response = {
                "AppsName": "selfcare",
                "transID": "3290776903",
                "dbName": "appDB",
                "query": "getBundleListNew",
                "values": "Data;Daily;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "100",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000011356",
                                "SERVICENAME": "50MB + 25MB Bonus Daily Plan",
                                "NAME": "50MB + 25MB Bonus Daily Plan",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "50MB + 25MB Bonus Daily Plan",
                                "SERVICEID": "234012000009670",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Daily"
                }, {
                                "PRICE": "200",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000011360",
                                "SERVICENAME": "Daily Plan 150MB",
                                "NAME": "Daily Plan 150MB",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "Daily Plan 150MB",
                                "SERVICEID": "234012000009672",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Daily"
                }]
};

export const mySubscription_Response ={
                "status_code": 0,
                "status_message": "OK",
                "subscriptionListconfig_result": [{
                                "autorenewal": "1",
                                "renewaldate": "20180809071100",
                                "desc": "UAE Roaming Bundle|For the subscriber group Default Group,The rental is 0 naira per 168 hours, and automatic renewal is supported.",
                                "productid": "23401220000020386",
                                "name": "UAE Roaming Bundle",
                                "spid": "2340110004119",
                                "startdate": "20180726061100",
                                "expirydate": "20370101000000",
                                "servicePayType": "0",
                                "amount": "0",
                                "CycleLength": "168",
                                "priceType": "436",
                                "isScream": "1",
                                "renewalDate": "20180726061101",
                                "isSubscribe": "1",
                                "rentFee": "0",
                                "subProductType": "1",
                                "manualRenewalFlag": "0",
                                "chargeMode": "22",
                                "subScriptionType": "0",
                                "parentName2": "UAE Roaming Bundle",
                                "ratefee": "0",
                                "renewConfirmStatus": "3",
                                "shortDestinationCode": "131",
                                "prodSubType": "1"
                }, {
                                "autorenewal": "1",
                                "renewaldate": "20361231235950",
                                "desc": "XtraValue Bundle V2000 |For the subscriber group Default Group,The rental is 0 naira per 162000 hours, and automatic renewal is supported.",
                                "productid": "23401220000014873",
                                "name": "XtraTalk 2000",
                                "spid": "2340110004119",
                                "startdate": "20180722185415",
                                "expirydate": "20370101000000",
                                "servicePayType": "0",
                                "amount": "0",
                                "CycleLength": "162000",
                                "priceType": "436",
                                "isScream": "1",
                                "renewalDate": "20180722185415",
                                "isSubscribe": "1",
                                "rentFee": "0",
                                "subProductType": "1",
                                "manualRenewalFlag": "0",
                                "chargeMode": "22",
                                "subScriptionType": "0",
                                "parentName2": "XtraValue Bundle V2000",
                                "ratefee": "0",
                                "renewConfirmStatus": "3",
                                "shortDestinationCode": "131",
                                "prodSubType": "1"
                }, {
                                "autorenewal": "1",
                                "renewaldate": "20361231235950",
                                "desc": "XtraValue Bundle D2000 |For the subscriber group Default Group,The rental is 0 naira per 162000 hours, and automatic renewal is supported.",
                                "productid": "23401220000014879",
                                "name": "XtraData 2000",
                                "spid": "2340110004119",
                                "startdate": "20180709062713",
                                "expirydate": "20370101000000",
                                "servicePayType": "0",
                                "amount": "0",
                                "CycleLength": "162000",
                                "priceType": "436",
                                "isScream": "1",
                                "renewalDate": "20180709062713",
                                "isSubscribe": "1",
                                "rentFee": "0",
                                "subProductType": "1",
                                "manualRenewalFlag": "0",
                                "chargeMode": "22",
                                "subScriptionType": "0",
                                "parentName2": "XtraValue Bundle D2000",
                                "ratefee": "0",
                                "renewConfirmStatus": "3",
                                "shortDestinationCode": "131",
                                "prodSubType": "1"
                }
                , {
                                "autorenewal": "1",
                                "renewaldate": "20181223165736",
                                "desc": "DND Service|For the subscriber group Default Group,The rental is 0 naira per 4320 hours, and automatic renewal is supported.",
                                "productid": "23401220000013859",
                                "name": "DND",
                                "spid": "2340110003119",
                                "startdate": "20180626165736",
                                "expirydate": "20370101000000",
                                "servicePayType": "0",
                                "amount": "0",
                                "CycleLength": "4320",
                                "featureCode": "654",
                                "priceType": "2",
                                "apiChannelID": "258",
                                "renewalDate": "20180626165736",
                                "isSubscribe": "1",
                                "rentFee": "0",
                                "subProductType": "1",
                                "AccessNo": "403",
                                "manualRenewalFlag": "0",
                                "chargeMode": "22",
                                "subScriptionType": "0",
                                "accessCode": "2442",
                                "parentName2": "DND Service",
                                "ratefee": "0",
                                "shortDestinationCode": "2442",
                                "prodSubType": "0",
                                "SPID": "2340110006859"
                }
]
}


export const buyBundlesWeeklyData_Response ={
                "AppsName": "selfcare",
                "transID": "3290776903",
                "dbName": "appDB",
                "query": "getBundleListNew",
                "values": "Data;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000020082",
                                "SERVICENAME": "Weekly Plan 150MB",
                                "NAME": "Weekly Plan 150MB",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "Weekly Plan 150MB",
                                "SERVICEID": "234012000015775",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000013411",
                                "SERVICENAME": "Weekly 500MB + 500MB Bonus (All Day)",
                                "NAME": "Weekly 500MB + 500MB Bonus (All Day)",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "(For Pulse Customers ONLY)",
                                "SERVICEID": "234012000011592",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000011361",
                                "SERVICENAME": "Weekly Plan 750MB",
                                "NAME": "Weekly Plan 500MB + 250MB bonus (1am-7am)",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "Weekly Plan 500MB + 250MB bonus (1am-7am)",
                                "SERVICEID": "234012000009674",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
};

export const buyBundlesMonthlyData_Response ={
                "AppsName": "selfcare",
                "transID": "3290776903",
                "dbName": "appDB",
                "query": "getBundleListNew",
                "values": "Data;Monthly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000011363",
                                "SERVICENAME": "Monthly Plan 1.5GB",
                                "NAME": "Monthly Plan 1GB + 500MB Bonus (1am-7am)",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "Monthly Plan 1GB + 500MB Bonus (1am-7am)",
                                "SERVICEID": "234012000009675",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Monthly"
                }, {
                                "PRICE": "1200",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000021248",
                                "SERVICENAME": "Monthly Plan 1.5GB",
                                "NAME": "Monthly Plan 1.5GB",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "Monthly Plan 1.5GB",
                                "SERVICEID": "234012000015776",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Monthly"
                }, {
                                "PRICE": "2000",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000011365",
                                "SERVICENAME": "Monthly Plan 3.5GB",
                                "NAME": "Monthly Plan 2.5GB + 1GB Bonus (1am-7am)",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "Monthly Plan 2.5GB + 1GB Bonus (1am-7am)",
                                "SERVICEID": "234012000009678",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Monthly"
                }, {
                                "PRICE": "3500",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000020086",
                                "SERVICENAME": "Monthly Plan 5GB",
                                "NAME": "Monthly Plan 5GB",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "Monthly Plan 5GB",
                                "SERVICEID": "234012000015776",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Monthly"
                }, {
                                "PRICE": "5000",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000012619",
                                "SERVICENAME": "FastLink 10GB Monthly",
                                "NAME": "Monthly Plan 10GB",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "Monthly Plan 10GB",
                                "SERVICEID": "234012000010790",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Monthly"
                }, {
                                "PRICE": "10000",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000012620",
                                "SERVICENAME": "FastLink 22GB Monthly",
                                "NAME": "Monthly Plan 22GB",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "Monthly Plan 22GB",
                                "SERVICEID": "234012000010791",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Monthly"
                }]
};


export const buyBundles2MonthsData_Response ={
                "AppsName": "selfcare",
                "transID": "2992435659",
                "dbName": "appDB",
                "query": "getBundleListNew",
                "values": "Data;Monthly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "2000",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000011365",
                                "SERVICENAME": "Monthly Plan 3.5GB",
                                "NAME": "Monthly Plan 3.5GB",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "Monthly Plan 3.5GB",
                                "SERVICEID": "234012000009678",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Monthly"
                }, {
                                "PRICE": "5000",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000012619",
                                "SERVICENAME": "FastLink 10GB Monthly",
                                "NAME": "FastLink 10GB Monthly",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "FastLink 10GB Monthly",
                                "SERVICEID": "234012000010790",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Monthly"
                }, {
                                "PRICE": "10000",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000012620",
                                "SERVICENAME": "FastLink 22GB Monthly",
                                "NAME": "FastLink 22GB Monthly",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "FastLink 22GB Monthly",
                                "SERVICEID": "234012000010791",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Monthly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "23401220000012348",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "null",
                                "PRODUCTID": "23401220000011363",
                                "SERVICENAME": "Monthly Plan 1.5GB",
                                "NAME": "Monthly Plan 1GB + 500MB Bonus (1am-7am)",
                                "BUNDLETYPE": "Data",
                                "DESCRIPTION": "Monthly Plan 1GB + 500MB Bonus (1am-7am)",
                                "SERVICEID": "234012000009675",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "234012000010556",
                                "AUTORENEW": "null",
                                "VALIDITY": "Monthly"
                }]
};

export const buyBundlesXtravalueResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getBundleSubCategory",
                "values": "XtraData;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "VALIDITY": "Weekly"
                }, {
                                "VALIDITY": "Monthly"
                }]
}

export const buyBundlesXtravalueWeeklyResponse_Array ={
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesXtravaluMonthlyResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300 Monthly",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500 Monthly",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000 Monthly",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesXtraTalkResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getBundleSubCategory",
                "values": "XtraData;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "VALIDITY": "Weekly"
                }, {
                                "VALIDITY": "Monthly"
                }]
}


export const buyBundlesXtraTalkWeeklyResponse_Array ={
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesXtraTalkMonthlyResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300 Monthly Talk",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                },
                {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500 Monthly Talk",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                },
                {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000 Monthly Talk",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesGoodyBagFaceBookResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getBundleSubCategory",
                "values": "XtraData;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "VALIDITY": "Weekly"
                }, {
                                "VALIDITY": "Monthly"
                }]
}

export const buyBundlesGoodyBagFaceBookWeeklyResponse_Array ={
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesGoodyBagFaceBookMonthlyResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300 Monthly Facebook",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500 Monthly Facebook",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000 Monthly Facebook",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}



export const buyBundlesGoodyBagWhatsAppResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getBundleSubCategory",
                "values": "XtraData;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "VALIDITY": "Weekly"
                }, {
                                "VALIDITY": "Monthly"
                }]
}

export const buyBundlesGoodyBagWhatsAppWeeklyResponse_Array ={
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesGoodyBagWhatsAppMonthlyResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300 Monthly WhatsApp",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500 Monthly WhatsApp",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000 Monthly WhatsApp",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesGoodyBagInstagramResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getBundleSubCategory",
                "values": "XtraData;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "VALIDITY": "Weekly"
                }, {
                                "VALIDITY": "Monthly"
                }]
}

export const buyBundlesGoodyBagInstagramWeeklyResponse_Array ={
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesGoodyBagInstagramMonthlyResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300 Monthly Instagram",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500 Monthly Instagram",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000 Monthly Instagram",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesGoodyBagSMEResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getBundleSubCategory",
                "values": "XtraData;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "VALIDITY": "Weekly"
                }, {
                                "VALIDITY": "Monthly"
                }]
}

export const buyBundlesGoodyBagSMEWeeklyResponse_Array ={
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesGoodyBagSMEMonthlyResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300 Monthly SME",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500 Monthly SME",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000 Monthly SME",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesGoodyBagEskimiResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getBundleSubCategory",
                "values": "XtraData;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "VALIDITY": "Weekly"
                }, {
                                "VALIDITY": "Monthly"
                }]
}

export const buyBundlesGoodyBagEskimiWeeklyResponse_Array ={
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesGoodyBagEskimiMonthlyResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300 Monthly Eskimi",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500 Monthly Eskimi",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000 Monthly Eskimi",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}


export const buyBundlesGoodyBagTwoGoResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getBundleSubCategory",
                "values": "XtraData;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "VALIDITY": "Weekly"
                }, {
                                "VALIDITY": "Monthly"
                }]
}

export const buyBundlesGoodyBagTwoGoWeeklyResponse_Array ={
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesGoodyBagTwoGoMonthlyResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300 Monthly 2Go",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500 Monthly 2Go",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000 Monthly 2Go",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}


export const buyBundlesGoodyBagWeChatResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getBundleSubCategory",
                "values": "XtraData;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "VALIDITY": "Weekly"
                }, {
                                "VALIDITY": "Monthly"
                }]
}

export const buyBundlesGoodyBagWeChatWeeklyResponse_Array ={
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const buyBundlesGoodyBagWeChatMonthlyResponse_Array = {
                "AppsName": "selfcare",
                "transID": "3291254175",
                "dbName": "appDB",
                "query": "getSubBundleList",
                "values": "XtraData;Weekly;NONEBU",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "PRICE": "300",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014875",
                                "SERVICENAME": "XtraData 300",
                                "NAME": "XtraData 300",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 300 Monthly WeChat",
                                "SERVICEID": "234012000012696",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "500",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014876",
                                "SERVICENAME": "XtraData 500",
                                "NAME": "XtraData 500",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 500 Monthly WeChat",
                                "SERVICEID": "234012000012697",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }, {
                                "PRICE": "1000",
                                "CHK_BALANCE_PID": "null",
                                "EBUENABLED": "BOTH",
                                "BUNDLESUBTYPE": "XtraData",
                                "PRODUCTID": "23401220000014877",
                                "SERVICENAME": "XtraData 1000",
                                "NAME": "XtraData 1000",
                                "BUNDLETYPE": "XtraValue",
                                "DESCRIPTION": "XtraData 1000 Monthly WeChat",
                                "SERVICEID": "234012000012698",
                                "CATEGORY": "Scream",
                                "CHK_BALANCE_SID": "null",
                                "AUTORENEW": "null",
                                "VALIDITY": "Weekly"
                }]
}

export const crbtSubscribe_Response = {
                "status_code": 0,
                //"status_message": "OK",
                "status_message": "Subscribe",
                "transaction_id": "2770572281"
}


// export const sendDataSetting_Response = {
//            "status_code": 0,
//            "status_message": "OK",
//            "transaction_id": "1609417467_20180715192254",
//            "sender_id": "DMC"
// }
export const crbtUnSubscribe_Response = {
                "status_code": 0,
                //"status_message": "OK",
                "status_message": "UnSubscribe",
                "transaction_id": "2770572281"
}

// export const inboxTonesList_Response = {
//            "status_code": 0,
//            "status_message": "OK",
//            "transaction_id": "2737206074",
//            "list": [{
//                            "price": "50",
//                            "singername": "REV UMA UKPAI",
//                            "id": "027960",
//                            "toneID": "17560692",
//                            "name": "ONUMU JURU EKENE",
//                            "validity": "2050-01-20 23:59:59"
//            }]
// }


export const tonesList_Response = {
                "status_code": 0,
                "status_message": "OK",
                "transaction_id": "C3F080DD01AD64",
                "list": [{
                                "id": "004970",
                                "name": "BOB MONEY"
                }, {
                                "id": "027130",
                                "name": "Ubom Mi"
                }, {
                                "id": "027135",
                                "name": "Paul and Silas"
                }, {
                                "id": "027136",
                                "name": "Rigirigi"
                }, {
                                "id": "027146",
                                "name": "Jinu Love Gawa"
                }, {
                                "id": "027147",
                                "name": "Zulu Ka Emee"
                }, {
                                "id": "027151",
                                "name": "Were Otu Osi Eme Ihe"
                }, {
                                "id": "027152",
                                "name": "One"
                }, {
                                "id": "027153",
                                "name": "Orinwa"
                }, {
                                "id": "027154",
                                "name": "ndibe mi"
                }, {
                                "id": "027156",
                                "name": "Ubo"
                }, {
                                "id": "027158",
                                "name": "no"
                }, {
                                "id": "027161",
                                "name": "Know Him"
                }, {
                                "id": "027164",
                                "name": "Idiogo"
                }, {
                                "id": "027167",
                                "name": "Handset"
                }, {
                                "id": "027171",
                                "name": "Ha Nye Chukwu Otito"
                }, {
                                "id": "027175",
                                "name": "Yan Idiongo"
                }, {
                                "id": "027176",
                                "name": "Nabania"
                }, {
                                "id": "027180",
                                "name": "Gbaharam"
                }, {
                                "id": "027182",
                                "name": "NtakNsidi Ntak"
                }, {
                                "id": "027183",
                                "name": "Baby"
                }, {
                                "id": "027188",
                                "name": "niobi m"
                }, {
                                "id": "027197",
                                "name": "Ngozim"
                }, {
                                "id": "702358",
                                "name": "Le Kwa Ukwu"
                }, {
                                "id": "702359",
                                "name": "She can get it"
                }, {
                                "id": "702069",
                                "name": "For Daddy"
                }, {
                                "id": "702190",
                                "name": "Powerful"
                }, {
                                "id": "702275",
                                "name": "Butter was Here"
                }, {
                                "id": "702056",
                                "name": "Hire Killer"
                }, {
                                "id": "702360",
                                "name": "Parcel"
                }, {
                                "id": "702411",
                                "name": "Its Over"
                }, {
                                "id": "702412",
                                "name": "Before Then"
                }, {
                                "id": "702425",
                                "name": "Eji Owuro"
                }, {
                                "id": "012034",
                                "name": "baby mi"
                }, {
                                "id": "012035",
                                "name": "Bouns track rotten freestyle"
                }, {
                                "id": "012036",
                                "name": "conscious"
                }, {
                                "id": "012037",
                                "name": "dance with me"
                }, {
                                "id": "012038",
                                "name": "do what i want"
                }, {
                                "id": "012039",
                                "name": "everybody wan rap"
                }, {
                                "id": "012043",
                                "name": "no contest"
                }, {
                                "id": "012045",
                                "name": "Proceed"
                }, {
                                "id": "012046",
                                "name": "puff puff"
                }, {
                                "id": "012048",
                                "name": "skillz"
                }, {
                                "id": "012050",
                                "name": "thank you mr dj"
                }, {
                                "id": "012053",
                                "name": "warning"
                }, {
                                "id": "007827",
                                "name": "Yahoozee Rmx"
                }, {
                                "id": "017994",
                                "name": "Mo Gbono Feli Feli"
                }, {
                                "id": "017995",
                                "name": "Suddenly"
                }, {
                                "id": "017958",
                                "name": "Suddenly1"
                }, {
                                "id": "017694",
                                "name": "WHERE I WANNA BE"
                }]
}


export const toneDetailsList_Response = {
                "status_code": 0,
                "status_message": "OK",
                "transaction_id": "2749787188",
                "list": [{
                                "price": "50",
                                "singername": "SYDOM J",
                                "id": "004970",
                                "toneID": "1010154",
                                "name": "BOB MONEY",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Prophet Daniel",
                                "id": "027130",
                                "toneID": "1010386",
                                "name": "Ubom Mi",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Prince Phillips",
                                "id": "027135",
                                "toneID": "1010392",
                                "name": "Paul and Silas",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Nabania",
                                "id": "027136",
                                "toneID": "1010393",
                                "name": "Rigirigi",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Anyi",
                                "id": "027146",
                                "toneID": "1010411",
                                "name": "Jinu Love Gawa",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Anyi",
                                "id": "027147",
                                "toneID": "1010412",
                                "name": "Zulu Ka Emee",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Chukwu",
                                "id": "027151",
                                "toneID": "1010416",
                                "name": "Were Otu Osi Eme Ihe",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "D",
                                "id": "027152",
                                "toneID": "1010417",
                                "name": "One",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Doctor",
                                "id": "027153",
                                "toneID": "1010418",
                                "name": "Orinwa",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Ebiet",
                                "id": "027154",
                                "toneID": "1010419",
                                "name": "ndibe mi",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Egwu",
                                "id": "027156",
                                "toneID": "1010421",
                                "name": "Ubo",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Fen",
                                "id": "027158",
                                "toneID": "1010423",
                                "name": "no",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "I",
                                "id": "027161",
                                "toneID": "1010426",
                                "name": "Know Him",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Idiogo",
                                "id": "027164",
                                "toneID": "1010429",
                                "name": "Idiogo",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Ima",
                                "id": "027167",
                                "toneID": "1010432",
                                "name": "Handset",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Ka",
                                "id": "027171",
                                "toneID": "1010436",
                                "name": "Ha Nye Chukwu Otito",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Mme",
                                "id": "027175",
                                "toneID": "1010440",
                                "name": "Yan Idiongo",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Nabania",
                                "id": "027176",
                                "toneID": "1010441",
                                "name": "Nabania",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Nna",
                                "id": "027180",
                                "toneID": "1010445",
                                "name": "Gbaharam",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Nsidi",
                                "id": "027182",
                                "toneID": "1010446",
                                "name": "NtakNsidi Ntak",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Nwa",
                                "id": "027183",
                                "toneID": "1010447",
                                "name": "Baby",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Odim",
                                "id": "027188",
                                "toneID": "1010451",
                                "name": "niobi m",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Afo",
                                "id": "027197",
                                "toneID": "1010461",
                                "name": "Ngozim",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Iyanya",
                                "id": "702358",
                                "toneID": "1005593",
                                "name": "Le Kwa Ukwu",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Iyanya",
                                "id": "702359",
                                "toneID": "1005594",
                                "name": "She can get it",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Bridge ft Yung",
                                "id": "702069",
                                "toneID": "1005595",
                                "name": "For Daddy",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Illbliss",
                                "id": "702190",
                                "toneID": "1005598",
                                "name": "Powerful",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Ajebutter22",
                                "id": "702275",
                                "toneID": "1005602",
                                "name": "Butter was Here",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Ajebutter22",
                                "id": "702056",
                                "toneID": "1005603",
                                "name": "Hire Killer",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Phyno",
                                "id": "702360",
                                "toneID": "1005608",
                                "name": "Parcel",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Juliet Ibrahim ft General Pype",
                                "id": "702411",
                                "toneID": "1005614",
                                "name": "Its Over",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Wonda ft Sarkodie",
                                "id": "702412",
                                "toneID": "1005615",
                                "name": "Before Then",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Shola Allyson",
                                "id": "702425",
                                "toneID": "1005627",
                                "name": "Eji Owuro",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Dj Jimmy Jatt",
                                "id": "012034",
                                "toneID": "1005993",
                                "name": "baby mi",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Dj Jimmy Jatt",
                                "id": "012035",
                                "toneID": "1005994",
                                "name": "Bouns track rotten freestyle",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Dj Jimmy Jatt",
                                "id": "012036",
                                "toneID": "1005995",
                                "name": "conscious",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Dj Jimmy Jatt",
                                "id": "012037",
                                "toneID": "1005996",
                                "name": "dance with me",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Dj Jimmy Jatt",
                                "id": "012038",
                                "toneID": "1005997",
                                "name": "do what i want",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Dj Jimmy Jatt",
                                "id": "012039",
                                "toneID": "1005998",
                                "name": "everybody wan rap",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Dj Jimmy Jatt",
                                "id": "012043",
                                "toneID": "1006002",
                                "name": "no contest",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Dj Jimmy Jatt",
                                "id": "012045",
                                "toneID": "1006004",
                                "name": "Proceed",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Dj Jimmy Jatt",
                                "id": "012046",
                                "toneID": "1006005",
                                "name": "puff puff",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Dj Jimmy Jatt",
                                "id": "012048",
                                "toneID": "1006007",
                                "name": "skillz",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Dj Jimmy Jatt",
                                "id": "012050",
                                "toneID": "1006009",
                                "name": "thank you mr dj",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Dj Jimmy Jatt",
                                "id": "012053",
                                "toneID": "1006012",
                                "name": "warning",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "Olu Maintain",
                                "id": "007827",
                                "toneID": "1006037",
                                "name": "Yahoozee Rmx",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "D Banj",
                                "id": "017994",
                                "toneID": "1006089",
                                "name": "Mo Gbono Feli Feli",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "D Banj",
                                "id": "017995",
                                "toneID": "1006090",
                                "name": "Suddenly",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "D Banj",
                                "id": "017958",
                                "toneID": "1006091",
                                "name": "Suddenly1",
                                "validity": "2020-12-31 23:59:59"
                }, {
                                "price": "50",
                                "singername": "GUITAR MAN",
                                "id": "017694",
                                "toneID": "1006092",
                                "name": "WHERE I WANNA BE",
                                "validity": "2020-12-31 23:59:59"
                }]
}

export const reserveNumberList_Response ={
                "status_code": 0,
                "status_message": "OK",
                "req_number": "3017996658",
                "transaction_id": "3288546681",
                "list": [{
                                "msisdn": "8104750475",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8104780478",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8104820482",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8104870487",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8104890489",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8104910491",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8104930493",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8104970497",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105170517",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105180518",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105193709",
                                "price": "150.00",
                                "numberstatus": "F",
                                "desc": "This is a normal number and it costs N150.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105260526",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105270527",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105340534",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105370537",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105380538",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105410541",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105480548",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105490549",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105620562",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105630563",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105710571",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105760576",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105790579",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105820582",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105830583",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105840584",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105860586",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105890589",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105910591",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105920592",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105930593",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105940594",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105950595",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105960596",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8105970597",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8106120612",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8106140614",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8106180618",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }, {
                                "msisdn": "8106290629",
                                "price": "2000.00",
                                "numberstatus": "F",
                                "desc": "This is a platinum number and it costs N2000.00. Do you wish to continue and reserve the number?"
                }]
}


export const sendMsisdnReserveReq_Response = {
                "status_code": "1",
                "status_message": "Failure",
                "transaction_id": "3288546681",
                "add_status_message": "Entered Mobile Number is Not Valid"
}

export const crbtStatus_Response = {
                "status_code": 0,
                "status_message": "OK",
                "transaction_id": "1829907244"
}

export const lastRecharges_Response = {
                "status_code": 0,
                "status_message": "OK",
                "transaction_id": "3755941708",
                "sender_id": "PPMS",
                "recent_recharges": [{
                                "cost": "100",
                                "date": "2018-03-15T10:24:40.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2018-03-15T10:16:00.000+01:00"
                }, {
                                "cost": "500",
                                "date": "2018-01-12T18:38:20.000+01:00"
                }, {
                                "cost": "500",
                                "date": "2017-11-24T18:20:43.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2017-11-24T18:19:27.000+01:00"
                }
                , {
                                "cost": "100",
                                "date": "2016-04-11T19:20:33.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2016-04-10T20:06:32.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2016-02-22T12:01:57.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2016-02-21T14:06:54.000+01:00"
                }, {
                                "cost": "200",
                                "date": "2016-02-20T13:02:06.000+01:00"
                }, {
                                "cost": "200",
                                "date": "2016-02-20T13:01:41.000+01:00"
                }, {
                                "cost": "200",
                                "date": "2016-02-19T13:20:29.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2016-02-19T09:43:50.000+01:00"
                }, {
                                "cost": "200",
                                "date": "2016-02-18T20:23:50.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2015-12-27T21:07:31.000+01:00"
                }, {
                                "cost": "200",
                                "date": "2015-09-12T12:04:24.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2015-09-04T13:49:31.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2015-08-30T15:14:09.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2015-08-07T16:33:53.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2015-08-04T21:17:16.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2015-07-19T21:13:09.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2015-06-26T20:20:44.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2015-06-15T19:35:01.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2014-04-07T15:44:29.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2014-04-03T20:53:35.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2014-04-02T20:03:59.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2014-04-01T20:17:04.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2013-10-13T10:55:34.000+01:00"
                }, {
                                "cost": "100",
                                "date": "2013-10-12T08:04:44.000+01:00"
                }
]
}

export const activateCallForward_Response ={
                "status_code": 0,
                "status_message": "Activated OK",
                "transaction_id": "3751309655",
                "sender_id": "ESF"
}

export const editpreferences_Response ={
                "status_code": 0,
                "status_message": "OK",
                "transaction_id": "9534",
                "sender_id": "CLM"
}

export const editphone_Response ={
                "status_code": 0,
                "status_message": "OK",
                "transaction_id": "3691661010",
                "sender_id": "CLM"
}

export const rechargetopupother_Response ={
                "status_code": 0,
                "status_message": "Success"
}

export const airtimeRecgDebitSelf_Response ={
                "status_code": 0,
                "status_message": "Success"
}

export const deActivateCallForward_Response ={
                "status_code": 0,
                "status_message": "DEACTIVATED OK",
                "transaction_id": "3690412066",
                "sender_id": "ESF"
}

export const unSubscribeVas_Response ={
                "status_code": "22007219",
                "status_message": "UnSubscribe Product Fail. Detailed description:  userID : MSISDN[2348102947676] does not order this product !"
}

export const purchaseTune_Response ={
                "status_code": 0,
                "status_message": "Success"
}


export const activateCallBarring_Response ={
                "status_code": 0,
                "status_message": "Activate Call Barring OK",
                "transaction_id": "3693797236",
                "sender_id": "ESF"
}

export const deActivateCallBarring_Response ={
                "status_code": 0,
                "status_message": "DeActivated Call Barring OK",
                "transaction_id": "3693797236",
                "sender_id": "ESF"
}

export const simSwapStatus_Response ={
                "status_code": 0,
                "status_message": "Sim Swap Status OK",
                "transaction_id": "3692017805",
                "sender_id": "AGL",
                "details": {
                                "order_number": "384454818",
                                "order_date_time": "28/05/2018 12:03:59",
                                "typecode": "CHANGE OF SIM",
                                "note": "Executed",
                                "description": "GSM",
                                "shipped_date": "28/05/2018 12:03:59"
                }
}

// export const checkBorrowEligibility_Response ={
//            "status_code": "0",
//            "status_message": "ELIGIBLE",
//            "availableList": [{
//                            "serviceid": "XTRABYTE150"
//            }, {
//                            "serviceid": "XTRABYTE50"
//            }, {
//                            "serviceid": "XTRABYTE20"
//            }]
// }




export const eligibilitycheckBorrow_Error = [
                {
                                "id":"NE_OTHER",
                                "message":"Yello. You are not eligible for this service."
                }, {
                               "NE_TCL_REACHED": "Yello. You are not eligible for this service."
                }, {
                               "NE_ZERO_TCL": "Yello. You are not eligible for this service."
                }, {
                               "NE_ACTIVATION_DATE": "Yello. The MTN xtraTime service is available only to prepaid subscribers who have been on the MTN network for more than 90 days."
                }, {
                                "NE_SERVICE_CLASS": "Dear Customer, you will be not be able to use this service as a result of your service class."
                }, {
                                "NE_LOAN": "Yello. You are not eligible for this service, as you outstanding loan."
                }, {
                               "NE_BALANCE": "Yello. You are not eligible for this service, as your current balance is above threshold."
                }, {
                               "NE_LAST_TOPUP_DATE": "Yello. You are not eligible for this service, as your last topup event before Z months."
                }, {
                               "NE_LAST_BLACKLIST": "Yello. You are not eligible for this service, as you belong to service blacklist."
                }, {
                               "NE_INSUFFICIENT_HISTORY": "Yello. You are not eligible for this service, as you do not have sufficient transactions."
                }, {
                               "NE_LOW_AVG_TOPUP_AMOUNT": "Yello. You are not eligible for this service, as you do not have enough average recharge amount in the last Z months."
                }, {
                               "NE_SIM_REGISTRATION": "Yello. You are not eligible for this service, as your sim card is not registered."
                }, {
                               "": "Yello. You are not eligible for this service, as your current balance is above threshold."
                }
];

export const checkBorrowEligibility_Response ={
	"status_code": "0",
	"status_message": "ELIGIBLE",
	"availableList": [{
		"serviceid": "XTRABYTE150"
	}, {
		"serviceid": "XTRABYTE50"
	}, {
		"serviceid": "XTRABYTE20"
	}]
}

export const getBorrowAirTime_Response ={
  
	"status_code": "0",
	"status_message": "Success"
}

export const checkBorrowAirBalance_Response ={
	"status_code": "0",
	"balance": "-99.28"
}

export const vasSubscription_Response ={
                "status_code": "22007306",
                "status_message": " User is in systemLevel blacklist! "
}

export const bundleActivation_Response ={
                "response": {
                                "result": {
                                                "resultCode": "00000000",
                                                "resultDescription": "Operation Success"
                                },
                                "operation": "Activation",
                                "info": [{
                                                "name": "MSISDN",
                                                "value": "2348142103970"
                                }, {
                                                "name": "Product ID",
                                                "value": "23401220000026252"
                                }, {
                                                "name": "Amount",
                                                "value": "0.0"
                                }, {
                                                "name": "Expiry Date",
                                                "value": "20180817020521"
                                }, {
                                                "name": "AutoRenew",
                                                "value": "No"
                                }, {
                                                "name": "NOTIFICATIONMESSAGE",
                                                "value": "Hi, Your Activation to 100MB for MyMTN App Adoption is Successful. You have been charged with 0.0 and expiry date is 17/08/2018 03:05:21."
                                }]
                }
}

export const sendAirtime_Response ={
                "response": {
                                "result": {
                                                "resultCode": "00000000",
                                                "resultDescription": "Operation Success"
                                },
                                "operation": "Transfer",
                                "info": [{
                                                "name": "APARTYMSISDN",
                                                "value": "2348033493810"
                                }, {
                                                "name": "BPARTYMSISDN",
                                                "value": "2348102949681"
                                }, {
                                                "name": "AMOUNT",
                                                "value": "5000"
                                }]
                }
}

export const rechargeSendAirPinCahnge_Response ={
                "response": {
                                "result": {
                                                "resultCode": "00000000",
                                                "resultDescription": "Operation Success"
                                },
                                "operation": "pinchange",
                                "info": [{
                                                "name": "MSISDN",
                                                "value": "2348035035110"
                                }]
                }
}


export const activePlan_Response ={
                "AppsName": "selfcare",
                "transID": "2992435659",
                "dbName": "appDB",
                "query": "checkPlan",
                "values": "403",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "MIGRATION": "null",
                                "SUBSTYPE": "null",
                                "PLAN_CHANGE_SID": "null",
                                "PLAN_CHANGE_PID": "null",
                                "ISMAINPROD": "null",
                                "NAME": "MTN XtraSpecial ePostpaid",
                                //"NAME" : "null",
                                "EBUTYPE": "null",
                                "ISBESTPLAN": "null",
                                "BENIFIT": "null",
                                "DESCRIPTION": "null",
                                "ID": "403",
                                "VALIDITY": "null",
                                "CHARGE": "null"
                }]
}

export const otherPlan_Response ={
                "AppsName": "selfcare",
                "transID": "2992435659",
                "dbName": "appDB",
                "query": "viewOtherPlan",
                "values": "403",
                "opcoID": "NG",
                "message": "success",
                "Format": "json",
                "plugin": "dba",
                "prefLang": "EN",
                "msisdn": "2348102947676",
                "Interface": "client",
                "status": "0",
                "prodInfo": [{
                                "MIGRATION": "YES",
                                "SUBSTYPE": "null",
                                "PLAN_CHANGE_SID": "234012000018101",
                                "PLAN_CHANGE_PID": "23401220000020905",
                                "ISMAINPROD": "null",
                                "NAME": "MTN Alumnus",
                                "EBUTYPE": "null",
                                "ISBESTPLAN": "null",
                                "BENIFIT": "null",
                                "DESCRIPTION": "The MTN Alumnus plan is a basic plan that allows you enjoy call rate of N6.6/Min on National Calls, Free SMS to all MTN numbers, N2/SMS to other Networks and data bonus on data bundle purchase.N6.6/Min on National Calls,Free SMS to all MTN numbers,N2/SMS to other Networks and data bonus on data bundle purchase",
                                "ID": "20",
                                "VALIDITY": "null",
                                "CHARGE": "null"
                }
                , {
                                "MIGRATION": "YES",
                                "SUBSTYPE": "null",
                                "PLAN_CHANGE_SID": "234012000010219",
                                "PLAN_CHANGE_PID": "23401220000015772",
                                "ISMAINPROD": "null",
                                "NAME": "MTN NEW BetaTalk",
                                "EBUTYPE": "null",
                                "ISBESTPLAN": "null",
                                "BENIFIT": "150% bonus on all recharges below N100 and 250% bonus on all recharges above N100.",
                                "DESCRIPTION": "The MTN BetaTalk is thedefault  prepaid tariff plan that offers you 250% bonus on recharge and Flat rate for calls to all networks from both Main Accounts and Bonus Account now @42kobo/second.",
                                "ID": "46",
                                "VALIDITY": "null",
                                "CHARGE": "null"
                }, {
                                "MIGRATION": "YES",
                                "SUBSTYPE": "null",
                                "PLAN_CHANGE_SID": "234012000018023",
                                "PLAN_CHANGE_PID": "23401220000020773",
                                "ISMAINPROD": "null",
                                "NAME": "MTN YAFUNYAFUN",
                                "EBUTYPE": "null",
                                "ISBESTPLAN": "null",
                                "BENIFIT": "null",
                                "DESCRIPTION": "null",
                                "ID": "25",
                                "VALIDITY": "null",
                                "CHARGE": "null"
                }, {
                                "MIGRATION": "YES",
                                "SUBSTYPE": "null",
                                "PLAN_CHANGE_SID": "234012000010217",
                                "PLAN_CHANGE_PID": "23401220000011997",
                                "ISMAINPROD": "null",
                                "NAME": "MTN Pulse",
                                "EBUTYPE": "null",
                                "ISBESTPLAN": "null",
                                "BENIFIT": "FLAT rate of 11kobo/sec for calls ACROSS ALL local Networks in Nigeria after spending NGN10 daily.<br>Music streaming on Music plus at N10/day.<br>Happy Hour night browsing at N25/day.<br>100% data bonus on purchase of 500MB weekly bundle.<br>Data bonus on recharge (10MB on ?100 recharge or 20MB on ?200 and above(ONLY on first recharge of the week).",
                                "DESCRIPTION": "The New MTN Pulse will allow customers enjoy a FLAT rate of 11kobo/sec for calls ACROSS ALL local Networks in Nigeria after spending NGN10 daily. Customers on this plan will also enjoy the following offers: Music streaming on Music+ at N10/day. Happy Hour night browsing at N25/day. 100% data bonus on purchase of 500MB weekly bundle.Data bonus on recharge. Plus other life-enriching MTN products and services. Current customers on MTN iPulse will be required to migrate to the new MTN Pulse to enjoy the discounted call rates and other offers.",
                                "ID": "33",
                                "VALIDITY": "null",
                                "CHARGE": "null"
                }
                //, {
                //            "MIGRATION": "YES",
                //            "SUBSTYPE": "null",
                //            "PLAN_CHANGE_SID": "234012000010219",
                //            "PLAN_CHANGE_PID": "23401220000011999",
                //            "ISMAINPROD": "null",
                //            "NAME": "MTN BetaTalk",
                //            "EBUTYPE": "null",
                //            "ISBESTPLAN": "null",
                //            "BENIFIT": "150% bonus on all recharges below N100 and 200% bonus on all recharges above N100.",
                //            "DESCRIPTION": "The MTN BetaTalk is thedefault  prepaid tariff plan that offers you 200% bonus on recharge and Flat rate for calls to all networks from both Main Accounts and Bonus Account now @40kobo/second.",
                //            "ID": "39",
                //            "VALIDITY": "null",
                //            "CHARGE": "null"
                // }, {
                //            "MIGRATION": "YES",
                //            "SUBSTYPE": "null",
                //            "PLAN_CHANGE_SID": "234012000010216",
                //            "PLAN_CHANGE_PID": "23401220000011996",
                //            "ISMAINPROD": "null",
                //            "NAME": "MTN Zone",
                //            "EBUTYPE": "null",
                //            "ISBESTPLAN": "null",
                //            "BENIFIT": "Enjoy up to 70% on calls during depending on the area and time of the day.",
                //            "DESCRIPTION": "MTN Zone with off peak pricing at 70% of default tariff plan rate 40k and peak pricing at 30% of the default plan rate for On net and Off net calls. Peak time is 6pm - 9pm and Off Peak is 9.01pm � 5.59pm.",
                //            "ID": "135",
                //            "VALIDITY": "null",
                //            "CHARGE": "null"
                // }, {
                //            "MIGRATION": "YES",
                //            "SUBSTYPE": "null",
                //            "PLAN_CHANGE_SID": "234012000010218",
                //            "PLAN_CHANGE_PID": "23401220000011998",
                //            "ISMAINPROD": "null",
                //            "NAME": "MTN Yello Life Plus",
                //            "EBUTYPE": "YES",
                //            "ISBESTPLAN": "null",
                //            "BENIFIT": "Affordable life & accident insurance for everyone on Y'ello Life Plus�Simply spend N300 a week & get a cover of N150, 000! What are you waiting for? ",
                //            "DESCRIPTION": "It does not matter who you are � an artisan, a market woman, a driver or a salesperson, Y�ello Life Plus offers affordable insurance for everyone. Yes, with Yello Life Plus, you can enjoy a rich combo of life and accident insurance cover of N150,000 when you spend N300 a week on calls and SMS.",
                //            "ID": "184",
                //            "VALIDITY": "null",
                //            "CHARGE": "null"
                // }
]
}


export const migrate_tariff_plan_Response = {
                "response": {
                                "result": {
                                                "resultCode": "00000000",
                                                "resultDescription": "Operation Success"
                                },
                                "operation": "Migrate",
                                "info": [{
                                                "name": "MSISDN",
                                                "value": "2348062750843"
                                }, {
                                                "name": "Product ID",
                                                "value": "23401220000013169"
                                }]
                }
}

export const getState_Response ={
  "AppsName": "selfcare",
  "Format": "json",
  "plugin": "dba",
  "prefLang": "EN",
  "transID": "5537249299",
  "dbName": "appDB",
  "query": "getState",
  "opcoID": "NG",
  "msisdn": "2348142050171",
  "message": "success",
  "Interface": "client",
  "status": "0",
  "prodInfo": [{
                  "STATE": "Abia"
  }, {
                  "STATE": "Adamawa"
  }, {
                  "STATE": "Akwa Ibom"
  }, {
                  "STATE": "Anambra"
  }, {
                  "STATE": "Bauchi"
  }, {
                  "STATE": "Bayelsa"
  }, {
                  "STATE": "Benue"
  }, {
                  "STATE": "Borno"
  }, {
                  "STATE": "Cross Rivers"
  }, {
                  "STATE": "Delta"
  }, {
                  "STATE": "Ebonyi"
  }, {
                  "STATE": "Edo"
  }, {
                  "STATE": "Ekiti"
  }, {
                  "STATE": "Enugu"
  }, {
                  "STATE": "Fct"
  }, {
                  "STATE": "Gombe"
  }, {
                  "STATE": "Imo"
  }, {
                  "STATE": "Jigawa"
  }, {
                  "STATE": "Kaduna"
  }, {
                  "STATE": "Kano"
  }, {
                  "STATE": "Katsina"
  }, {
                  "STATE": "Kebbi"
  }, {
                  "STATE": "Kogi"
  }, {
                  "STATE": "Kwara"
  }, {
                  "STATE": "Lagos"
  }, {
                  "STATE": "Nasarawa"
  }, {
                  "STATE": "Niger"
  }, {
                  "STATE": "Ogun"
  }, {
                  "STATE": "Ondo"
  }, {
                  "STATE": "Osun"
  }, {
                  "STATE": "Oyo"
  }, {
                  "STATE": "Plateau"
  }, {
                  "STATE": "Rivers"
  }, {
                  "STATE": "Sokoto"
  }, {
                  "STATE": "Taraba"
  }, {
                  "STATE": "Yobe"
  }, {
                  "STATE": "Zamfara"
  }]
}


export const getLga_Response ={
  "AppsName": "selfcare",
  "transID": "5537249299",
  "dbName": "appDB",
  "query": "getLGAS",
  "values": "Abia",
  "opcoID": "NG",
  "message": "success",
  "Format": "json",
  "plugin": "dba",
  "prefLang": "EN",
  "msisdn": "2348142050171",
  "Interface": "client",
  "status": "0",
  "prodInfo": [{
                  "LGA": "Ohafia"
  }, {
                  "LGA": "Isikwuato"
  }, {
                  "LGA": "Ikwuano"
  }, {
                  "LGA": "Aba South"
  }, {
                  "LGA": "Aba North"
  }, {
                  "LGA": "Umuahia North"
  }, {
                  "LGA": "Ugwunagbo"
  }]
}

export const serviceProvinceCentre_Response ={
  "AppsName": "selfcare",
  "transID": "5537249299",
  "dbName": "appDB",
  "query": "getServiceProvinceCenters",
  "values": "Abia",
  "opcoID": "NG",
  "message": "success",
  "Format": "json",
  "plugin": "dba",
  "prefLang": "EN",
  "msisdn": "2348142050171",
  "Interface": "client",
  "status": "0",
  "prodInfo": [{
                  "CELLPHONE": "8032038081",
                  "CONTROLLER": "Nnenna Esonu",
                  "LGA": "Aba South",
                  "ADDRESS": "80 School Road by Asa Road, Opposite Christ the King Cathedral Church, Aba",
                  "STATE": "Abia",
                  "NAME": "Center Name"
  }, {
                  "CELLPHONE": "8032030833",
                  "CONTROLLER": "Juliet Obike",
                  "LGA": "Aba North",
                  "ADDRESS": "186 Faulks road, Aba",
                  "STATE": "Abia",
                  "NAME": "Center Name"
  }, {
                  "CELLPHONE": "8032038081",
                  "CONTROLLER": "Nnenna Esonu",
                  "LGA": "Aba North",
                  "ADDRESS": "44 Aba-Owerri Road,Aba.",
                  "STATE": "Abia",
                  "NAME": "Center Name"
  }, {
                  "CELLPHONE": "8032030221",
                  "CONTROLLER": "Michael Ogbonnaya",
                  "LGA": "Ohafia",
                  "ADDRESS": "No 47 Arochukwu Road, Amaekpu Ohafia ",
                  "STATE": "Abia",
                  "NAME": "Center Name"
  }, {
                  "CELLPHONE": "8032034511",
                  "CONTROLLER": "Ogechi Nteh",
                  "LGA": "Umuahia North",
                  "ADDRESS": "47 Aba Road, Umuahia",
                  "STATE": "Abia",
                  "NAME": "Center Name"
  }, {
                  "CELLPHONE": "8032037704",
                  "CONTROLLER": "Joshua Iyun",
                  "LGA": "Umuahia North",
                  "ADDRESS": "Oando Filling Station, 7 Aba Road Umuahia Abia State",
                  "STATE": "Abia",
                  "NAME": "Center Name"
  }, {
                  "CELLPHONE": "8032034499",
                  "CONTROLLER": "Glory Ochuba",
                  "LGA": "Isikwuato",
                  "ADDRESS": "Abia State University Uturu Okigwe",
                  "STATE": "Abia",
                  "NAME": "Center Name"
  }, {
                  "CELLPHONE": "8032034585",
                  "CONTROLLER": "Rita Nwokocha",
                  "LGA": "Umuahia North",
                  "ADDRESS": "State Secretariat Ihie Ndume",
                  "STATE": "Abia",
                  "NAME": "Center Name"
  }, {
                  "CELLPHONE": "8032030476",
                  "CONTROLLER": "Chima Ogbonna",
                  "LGA": "Ikwuano",
                  "ADDRESS": "Michael Okpara University Umudike Abia State",
                  "STATE": "Abia",
                  "NAME": "Center Name"
  }, {
                  "CELLPHONE": "8032030858",
                  "CONTROLLER": "Joseph Ukaegbu",
                  "LGA": "Aba South",
                  "ADDRESS": "Oando Filling Station Aba Main Park Aba Abia State",
                  "STATE": "Abia",
                  "NAME": "Center Name"
  }, {
                  "CELLPHONE": "8032037714",
                  "CONTROLLER": "Mary Rose Orah",
                  "LGA": "Aba South",
                  "ADDRESS": "60 Jubilee Road Aba",
                  "STATE": "Abia",
                  "NAME": "Center Name"
  }, {
                  "CELLPHONE": "8032034586",
                  "CONTROLLER": "Chinonso Anusionwu",
                  "LGA": "Ugwunagbo",
                  "ADDRESS": "Enyimba Junction Aba Port Harcourt Express Aba",
                  "STATE": "Abia",
                  "NAME": "Center Name"
  }, {
                  "CELLPHONE": "8032034559",
                  "CONTROLLER": "Francisco Eze",
                  "LGA": "Aba South",
                  "ADDRESS": "180 Faulks Road, Aba",
                  "STATE": "Abia",
                  "NAME": "Center Name"
  }]
}

var STR_TITLE ="Locate MTN Store";

export const cObj = {
	"country" : [ {
		"title" : "Nigeria",
		"country_code" : "NG",
		"province" : [ {
			"title" : "Delta",
			"city" : [ {
				"title" : "Asaba"
			} ]
		}, {
			"title" : "Enugu",
			"city" : [ {
				"title" : "Enugu"
			} ]
		}, {
			"title" : "FCT",
			"city" : [ {
				"title" : "Abuja SC"
			}, {
				"title" : "Transcorp Lite"
			} ]
		}, {
			"title" : "Kaduna",
			"city" : [ {
				"title" : "Kaduna"
			} ]
		}, {
			"title" : "Kano",
			"city" : [ {
				"title" : "Kano"
			} ]
		}, {
			"title" : "Lagos",
			"city" : [ {
				"title" : "Ikoyi"
			}, {
				"title" : "Apapa"
			}, {
				"title" : "Oshodi"
			}, {
				"title" : "Victoria island"
			}, {
				"title" : "Ikeja"
			}, {
				"title" : "Ikeja airport"
			} ]
		}, {
			"title" : "Oyo",
			"city" : [ {
				"title" : "Ibadan"
			} ]
		}, {
			"title" : "Rivers",
			"city" : [ {
				"title" : "Rivers"
			} ]
		}, {
			"title" : "Sokoto",
			"city" : [ {
				"title" : "Sokoto"
			} ]
		} ]
	} ]
};



export const MTN = {
        "service_center": 
[
 {
            "NAME": "Sariking Nigerian Company LTD",
            "TYPE": "New Dawn",
            "STATE": "Kaduna",
            "LGA": "Jema'A",
            "ADDRESS": "86, Emir Road, Kafanchan.",
            "CELLPHONE": "8032039563",
            "CONTROLLER": "Joseph Halidu",
            "LATITUDE": "9.582341",
            "LONGITUDE": "8.291223"
        },
        {
            "NAME": "Easy Gsm Global Communications - Zaria",
            "TYPE": "New Dawn",
            "STATE": "Kaduna",
            "LGA": "Kaduna South",
            "ADDRESS": "Makera Plaza. Airforce Road, Kakuri, Kaduna.",
            "CELLPHONE": "8032039691",
            "CONTROLLER": "Farouq Abdussalam",
            "LATITUDE": "10.470224",
            "LONGITUDE": "7.419315"
        },
        {
            "NAME": "Sonite Communications LTD ",
            "TYPE": "New Dawn",
            "STATE": "Niger",
            "LGA": "Bida",
            "ADDRESS": "Y.S.K.B House, Beside old Savanah Bank Building, BCC Road, Bida.",
            "CELLPHONE": "8032033082",
            "CONTROLLER": "Raji  Funsho I.",
            "LATITUDE": "9.0937983",
            "LONGITUDE": "6.00442181"
        },
        {
            "NAME": "Galaphone Nigeria LTD",
            "TYPE": "New Dawn",
            "STATE": "Kaduna",
            "LGA": "Kaduna North",
            "ADDRESS": "Choice Plaza, Ali Akilu Road, Kaduna.",
            "CELLPHONE": "8032039554",
            "CONTROLLER": "Gloria Asuquo",
            "LATITUDE": "10.5811494",
            "LONGITUDE": "7.4474317"
        },
        {
            "NAME": "Gwills Nig LTD - Minna",
            "TYPE": "New Dawn",
            "STATE": "Niger",
            "LGA": "Bosso",
            "ADDRESS": "Randan Ruwa, beside Mypa junction Bosso, Minna Niger Sate",
            "CELLPHONE": "8032031353",
            "CONTROLLER": "Bilyaminu Salihu",
            "LATITUDE": "9.650705",
            "LONGITUDE": "6.534195"
        },
        {
            "NAME": "Alennsar Infinity Company Nig LTD",
            "TYPE": "New Dawn",
            "STATE": "Plateau",
            "LGA": "Jos North",
            "ADDRESS": "No. 23 Beach Road Jos",
            "CELLPHONE": "8032032944",
            "CONTROLLER": "Peters Benjamin Elaigwu",
            "LATITUDE": "9.913914",
            "LONGITUDE": "8.890131"
        },
        {
            "NAME": "Maiburgami Company Nig LTD",
            "TYPE": "New Dawn",
            "STATE": "Bauchi",
            "LGA": "Buachi",
            "ADDRESS": "No.1, Yandoka Road Opposite Total Filling Station Bauchi",
            "CELLPHONE": "8032039484",
            "CONTROLLER": "Joel Poopola",
            "LATITUDE": "10.318483",
            "LONGITUDE": "9.83091"
        },
        {
            "NAME": "Fusaha Ventures Nig LTD",
            "TYPE": "New Dawn",
            "STATE": "Gombe",
            "LGA": "Gombe",
            "ADDRESS": "No.15, Biu Road Opposite Central Primary School Gombe",
            "CELLPHONE": "8032036140",
            "CONTROLLER": "Nuru Zahraddeen Musa",
            "LATITUDE": "10.28969",
            "LONGITUDE": "11.16729"
        },
        {
            "NAME": "Maiburgami Company Nig LTD",
            "TYPE": "New Dawn",
            "STATE": "Bauchi",
            "LGA": "Katagum",
            "ADDRESS": "No. 63 Sardauna Road Azare, Katagum LGA",
            "CELLPHONE": "8032036601",
            "CONTROLLER": "Hashim Saheed",
            "LATITUDE": "10.31344",
            "LONGITUDE": "9.84327"
        },
        {
            "NAME": "Awake Interlink",
            "TYPE": "New Dawn",
            "STATE": "Ogun State",
            "LGA": "Ijebu Ode",
            "ADDRESS": "45,Ibadan Road,Ijebu Ode",
            "CELLPHONE": "8032035589",
            "CONTROLLER": "OYEWALE AJALA",
            "LATITUDE": "3.921132",
            "LONGITUDE": "6.83134"
        },
        {
            "NAME": "Emy V Global Services",
            "TYPE": "New Dawn",
            "STATE": "Taraba State",
            "LGA": "Jalingo",
            "ADDRESS": "Kwase House, New Era Junction, Wukari Rd",
            "CELLPHONE": "8032031820",
            "CONTROLLER": "Cynthia Joshua",
            "LATITUDE": "8.9324",
            "LONGITUDE": "11.3301"
        },
        {
            "NAME": "Zealstar Limited",
            "TYPE": "New Dawn",
            "STATE": "Adamawa",
            "LGA": "Numan",
            "ADDRESS": "No 1, Gombe Road, Numan",
            "CELLPHONE": "8032031823",
            "CONTROLLER": "Kenneth Osuji",
            "LATITUDE": "9.458172",
            "LONGITUDE": "12.0371065"
        },
        {
            "NAME": "Echologistics",
            "TYPE": "New Dawn",
            "STATE": "Adamawa",
            "LGA": "Yola North",
            "ADDRESS": "No 12, Aliyu Mustapha Way Opp Federal Secretariat",
            "CELLPHONE": "8032031520",
            "CONTROLLER": "Ibrahim Lawal",
            "LATITUDE": "9.251236",
            "LONGITUDE": "12.455178"
        },
        {
            "NAME": "Cherish Global",
            "TYPE": "New Dawn",
            "STATE": "Yobe",
            "LGA": "Damaturu",
            "ADDRESS": "Potiskum Road, Opposite Mass Transit, Damaturu",
            "CELLPHONE": "8030701641",
            "CONTROLLER": "tanko Hyelluwa",
            "LATITUDE": "11.7388578",
            "LONGITUDE": "11.9520679"
        },
        {
            "NAME": "New Mellennium Resources LTD",
            "TYPE": "New Dawn",
            "STATE": "Borno ",
            "LGA": "Maiduguri",
            "ADDRESS": "D25, Ramat Shopping Complex, Kashim Ibrahim Rd, Maiduguri",
            "CELLPHONE": "8032038594",
            "CONTROLLER": "Idris Mohammed Maina",
            "LATITUDE": "13.145938",
            "LONGITUDE": "11.842821"
        },
        {
            "NAME": "Velay Communicatins LTD",
            "TYPE": "New Dawn",
            "STATE": "Borno ",
            "LGA": "Biu",
            "ADDRESS": "253, Opposite UBA Bank, Gombi Rd, Biu",
            "CELLPHONE": "8032037103",
            "CONTROLLER": "Alhaji Ali Abba Ali",
            "LATITUDE": "13.145938",
            "LONGITUDE": "11.842821"
        },
        {
            "NAME": "Alennsar Enugu",
            "TYPE": "New Dawn",
            "STATE": "Enugu State",
            "LGA": "Enugu North",
            "ADDRESS": "58 Ogui Road",
            "CELLPHONE": "8032031126",
            "CONTROLLER": "Judith Aleke",
            "LATITUDE": "6.44335",
            "LONGITUDE": "7.49814"
        },
        {
            "NAME": "Chicadef",
            "TYPE": "New Dawn",
            "STATE": "Benue State",
            "LGA": "Makurdi",
            "ADDRESS": "No 18 Old Otukpo Road, opposite Zone 4 Police Headquarters Makurdi",
            "CELLPHONE": "8032035780",
            "CONTROLLER": "Akogwu Lisa",
            "LATITUDE": "7.709363",
            "LONGITUDE": "8.532601"
        },
        {
            "NAME": "Awake Interlink Ijebu Ode",
            "TYPE": "New Dawn",
            "STATE": "Ogun State",
            "LGA": "Ijebu Ode",
            "ADDRESS": "45, Ibadan Road, Ijebu Ode",
            "CELLPHONE": "8032035589",
            "CONTROLLER": "OYEWALE AJALA",
            "LATITUDE": "3.921132",
            "LONGITUDE": "6.83134"
        },
        {
            "NAME": "Golden Rule Communications LTD",
            "TYPE": "New Dawn",
            "STATE": "Lagos ",
            "LGA": "Oshodi-Isolo",
            "ADDRESS": "Flat 1, Block 3, Godmon Estate, Okota",
            "CELLPHONE": "8032030140",
            "CONTROLLER": "Gbemisola Macauley",
            "LATITUDE": "6.517247",
            "LONGITUDE": "3.318684"
        },
        {
            "NAME": "Data Partners LTD",
            "TYPE": "New Dawn",
            "STATE": "Cross River",
            "LGA": "Calabar Municipal",
            "ADDRESS": "90B MCC Road, Calabar",
            "CELLPHONE": "8032037207  ",
            "CONTROLLER": "Agatha",
            "LATITUDE": "4.9582",
            "LONGITUDE": "8.32837"
        },
 {
   "NAME": "Aba 3 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abia",
   "LGA": "Aba North",
   "ADDRESS": "186 Faulks Road, Aba",
   "CELLPHONE": "8032037433",
   "CONTROLLER": "Christopher",
   "LATITUDE": "5.1141",
   "LONGITUDE": "7.341281"
 },
 {
   "NAME": "Aba Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abia",
   "LGA": "Aba North",
   "ADDRESS": "44 Aba-Owerri Road,Aba.",
   "CELLPHONE": "8032038081",
   "CONTROLLER": "Nnenna Esonu",
   "LATITUDE": "5.128333",
   "LONGITUDE": "7.360562"
 },
 {
   "NAME": "Aba 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abia",
   "LGA": "Aba South",
   "ADDRESS": "80 School Road, By Asa Road, Opposite Christ the King Cathedral Church, Aba",
   "CELLPHONE": "8032038081",
   "CONTROLLER": "Nnenna Esonu",
   "LATITUDE": "5.104299",
   "LONGITUDE": "7.364571"
 },
 {
   "NAME": "Faulks Road Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abia",
   "LGA": "Aba South",
   "ADDRESS": "180 Faulks Road, Aba",
   "CELLPHONE": "8032037701",
   "CONTROLLER": "Innocent Amaefuna",
   "LATITUDE": "7.341584",
   "LONGITUDE": "5.114889"
 },
 {
   "NAME": "Jubilee Road Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abia",
   "LGA": "Aba South",
   "ADDRESS": "60 Jubilee Road, Aba",
   "CELLPHONE": "8032034586",
   "CONTROLLER": "Anusionwu Chinonso",
   "LATITUDE": "7.365646",
   "LONGITUDE": "5.109887"
 },
 {
   "NAME": "Oando Lorry Park Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abia",
   "LGA": "Aba South",
   "ADDRESS": "Oando Filling Station, Aba Main Park, Aba, Abia State",
   "CELLPHONE": "8032030858",
   "CONTROLLER": "Joseph Ukaegbu",
   "LATITUDE": "7.3725",
   "LONGITUDE": "5.11355"
 },
 {
   "NAME": "Umudike Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abia",
   "LGA": "Ikwuano",
   "ADDRESS": "Michael Okpara University, Umudike, Abia State",
   "CELLPHONE": "8032034498",
   "CONTROLLER": "Osimira Alaezi",
   "LATITUDE": "7.55388",
   "LONGITUDE": "5.51707"
 },
 {
   "NAME": "ABSU Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abia",
   "LGA": "Isikwuato",
   "ADDRESS": "Abia State University, Uturu, Okigwe",
   "CELLPHONE": "8032034509",
   "CONTROLLER": "Charles Udo",
   "LATITUDE": "5.826158",
   "LONGITUDE": "7.394275"
 },
 {
   "NAME": "Ohafia Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abia",
   "LGA": "Ohafia",
   "ADDRESS": "47 Arochukwu Road, Amaekpu, Ohafia",
   "CELLPHONE": "8032030221",
   "CONTROLLER": "Michael Ogbonnaya",
   "LATITUDE": "5.534306",
   "LONGITUDE": "7.88918"
 },
 {
   "NAME": "Oando Aba Road Umuahia ",
   "TYPE": "Connect Point",
   "STATE": "Abia",
   "LGA": "Umuahia North",
   "ADDRESS": "Oando Filling Station, 7 Aba Road, Umuahia, Abia State",
   "CELLPHONE": "8032037704",
   "CONTROLLER": "Joshua Iyun",
   "LATITUDE": "5.528911",
   "LONGITUDE": "7.49687"
 },
 {
   "NAME": "State Secretariat Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abia",
   "LGA": "Umuahia North",
   "ADDRESS": "Between New and old Secretariat Complex, State Secretariat, Umuahia",
   "CELLPHONE": "8032034585",
   "CONTROLLER": "Rita Nwokocha",
   "LATITUDE": "5.521937",
   "LONGITUDE": "7.512979"
 },
 {
   "NAME": "Umuahia Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abia",
   "LGA": "Umuahia North",
   "ADDRESS": "47 Aba Road, Umuahia",
   "CELLPHONE": "8032034511",
   "CONTROLLER": "Ogechi Nteh",
   "LATITUDE": "5.4971769",
   "LONGITUDE": "7.4850322"
 },
 {
   "NAME": "Abaji Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abuja",
   "LGA": "Abaji",
   "ADDRESS": "Toto Road, Opposite Sokodabo Junction (Adjacent First Bank), Abaji, FCT",
   "CELLPHONE": "8032032733",
   "CONTROLLER": "Tenimu Rabiu",
   "LATITUDE": "9.05",
   "LONGITUDE": "7.214"
 },
 {
   "NAME": "Zuba Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Tonsjos Filling Station, Along Abuja Kaduna Express Way, Abuja, Fct",
   "CELLPHONE": "8032034702",
   "CONTROLLER": "Moyaki Joseph",
   "LATITUDE": "9.100136",
   "LONGITUDE": "7.218326"
 },
 {
   "NAME": "Abuja Connect Store Wuse",
   "TYPE": "Connect Store",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "2121 Ndola Square, Wuse Zone 5, Abuja.",
   "CELLPHONE": "8032032171",
   "CONTROLLER": "Ifeyinwa Umudu",
   "LATITUDE": "9.0593363",
   "LONGITUDE": "7.4543792"
 },
 {
   "NAME": "Abuja Intl. Airport Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Nnamdi Azikiwe International Airport, International Wing, Abuja",
   "CELLPHONE": "8032037421",
   "CONTROLLER": "Helen Inwang",
   "LATITUDE": "9.007115",
   "LONGITUDE": "7.2634001"
 },
 {
   "NAME": "Abuja Service Center",
   "TYPE": "Service Centre",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Plot 2784, Shehu Shagari Way, Maitama, Abuja",
   "CELLPHONE": "8032002260",
   "CONTROLLER": "Olabode Ajose",
   "LATITUDE": "9.08548",
   "LONGITUDE": "7.488097"
 },
 {
   "NAME": "Abuja Shoprite Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Shoprite, Murtala Muhammed Way, Apo, Abuja",
   "CELLPHONE": "8032030344",
   "CONTROLLER": "Chukwuma  Ikedi",
   "LATITUDE": "8.98486",
   "LONGITUDE": "7.493222"
 },
 {
   "NAME": "DEI DEI Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Umuaratu Filling Station, Dei Dei Zuba, Abuja",
   "CELLPHONE": "8032037393",
   "CONTROLLER": "Garba SALEH",
   "LATITUDE": "9.027732",
   "LONGITUDE": "7.578738"
 },
 {
   "NAME": "Dutsen Alhaji Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Mobil Filling Station, Dutsen Alhaji, Bwari Road, Abuja",
   "CELLPHONE": "8032032745",
   "CONTROLLER": "Umar Baba Mohammed",
   "LATITUDE": "9.074503",
   "LONGITUDE": "7.495476"
 },
 {
   "NAME": "Efab Mall Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "B03 Efab Shopping Centre, Area 11 Garki, Abuja.",
   "CELLPHONE": "8032032171",
   "CONTROLLER": "Ifeyinwa Umudu",
   "LATITUDE": "9.043416",
   "LONGITUDE": "7.503805"
 },
 {
   "NAME": "Garki 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Plot 289 Lagos Street, Off Ladoke Akintola Boulevard, Garki, Abuja",
   "CELLPHONE": "8032030327",
   "CONTROLLER": "Lydia Babarimisa-Ojuolape",
   "LATITUDE": "9.02383",
   "LONGITUDE": "7.487704"
 },
 {
   "NAME": "Gwarimpa Connect point",
   "TYPE": "Connect Point",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Shop 109A, DBB Plaza, 1st Avenue, Gwarimpa, Abuja",
   "CELLPHONE": "8032032740",
   "CONTROLLER": "Umar Ibrahim",
   "LATITUDE": "9.09161",
   "LONGITUDE": "7.41394"
 },
 {
   "NAME": "Jabi Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Oando Filling Station, Alex Ekwueme Way, Jabi, Abuja",
   "CELLPHONE": "8032032737",
   "CONTROLLER": "Assam Assam",
   "LATITUDE": "9.07089",
   "LONGITUDE": "7.41458"
 },
 {
   "NAME": "Kugbo Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Connect Point Kugbo, Kugbo Funiture Market, Abuja",
   "CELLPHONE": "8032030671",
   "CONTROLLER": "Blessing Obochi",
   "LATITUDE": "9.026374",
   "LONGITUDE": "7.54827"
 },
 {
   "NAME": "Kubwa Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Oando Filling Station Katampe Extension, off Zuba Expressway, Katampe, Abuja",
   "CELLPHONE": "8032034702",
   "CONTROLLER": "jolly Moyaki Joseph",
   "LATITUDE": "7.43288",
   "LONGITUDE": "9.12357"
 },
 {
   "NAME": "Lugbe Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "House 12, 1st Avenue Road, FHA Estate, Lugbe, Abuja",
   "CELLPHONE": "8032037369",
   "CONTROLLER": "Kingsley Amiadamhen",
   "LATITUDE": "9.19375",
   "LONGITUDE": "7.1825"
 },
 {
   "NAME": "Mpape Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Shop A7/A8 Ibiye Plaza, Plot C58, next to Zenith Bank, Mpape Road (Berger Quarry Bus stop), Mpape, Abuja.",
   "CELLPHONE": "8032037415",
   "CONTROLLER": "obinna Okereafor",
   "LATITUDE": "9.139086",
   "LONGITUDE": "7.493773"
 },
 {
   "NAME": "National Assembly Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Ground Floor, National Assembly Complex, Central Area, Abuja",
   "CELLPHONE": "8032032743",
   "CONTROLLER": "Bisola Olayinka",
   "LATITUDE": "9.067302",
   "LONGITUDE": "7.509854"
 },
 {
   "NAME": "TranService Centerorp Service Center Lite",
   "TYPE": "Service Centre",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Transcorp Hilton Lobby, Abuja",
   "CELLPHONE": "8032002260",
   "CONTROLLER": "Olabode Ajose",
   "LATITUDE": "9.0743181",
   "LONGITUDE": "7.4947183"
 },
 {
   "NAME": "Wuse 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Omega Centre, Plot 527, Aminu Kano Crescent, Wuse 2, Abuja.",
   "CELLPHONE": "8032037438",
   "CONTROLLER": "Abiola Fadare",
   "LATITUDE": "9.0799535",
   "LONGITUDE": "7.4627641"
 },
 {
   "NAME": "Zone 1 Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abuja",
   "LGA": "Amac",
   "ADDRESS": "Oando Filling Station, Olusegun Obasanjo Way, Wuse Zone 1, Abuja",
   "CELLPHONE": "8032034257",
   "CONTROLLER": "Nancy Nwoye",
   "LATITUDE": "9.05478",
   "LONGITUDE": "7.46258"
 },
 {
   "NAME": "Bwari Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abuja",
   "LGA": "Bwari",
   "ADDRESS": "Nitbok Plaza, Along Law School Road, Bwari, Abuja",
   "CELLPHONE": "8032034663",
   "CONTROLLER": "Ibrahim Abubakar",
   "LATITUDE": "9.055192",
   "LONGITUDE": "7.489809"
 },
 {
   "NAME": "Kubwa Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abuja",
   "LGA": "Bwari",
   "ADDRESS": "No 1 Igwe Nwala Close, off okitipupa street phase 4, Gado Nasco Road, Kubwa, Abuja",
   "CELLPHONE": "8032030355",
   "CONTROLLER": "Oluwagbenga Olorunfemi",
   "LATITUDE": "9.1427397",
   "LONGITUDE": "7.3233511"
 },
 {
   "NAME": "Gwagwalada Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Abuja",
   "LGA": "Gwagwalada",
   "ADDRESS": "Kaita Shopping complex, Near Japaro Hotel, Along Lokoja-Kaduna Expressway, Gwagwalada, Abuja.",
   "CELLPHONE": "8032030258",
   "CONTROLLER": "Festus Sunday",
   "LATITUDE": "8.94331",
   "LONGITUDE": "7.094526"
 },
 {
   "NAME": "Karu Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Nasarawa",
   "LGA": "Karu",
   "ADDRESS": "Plot NC1 Karu Shoping Plaza, Opposite Karu Park, Nasarawa",
   "CELLPHONE": "8032034668",
   "CONTROLLER": "Umar Ibrahim",
   "LATITUDE": "9.033778",
   "LONGITUDE": "7.600573"
 },
 {
   "NAME": "Kwali Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Abuja",
   "LGA": "Kwali",
   "ADDRESS": "Connect Point Kwali, By Unity Bank Junction Kwali, Abuja",
   "CELLPHONE": "8032030258",
   "CONTROLLER": "Festus",
   "LATITUDE": "9.062448",
   "LONGITUDE": "7.478871"
 },
 {
   "NAME": "Gombi Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Adamawa",
   "LGA": "Gombi",
   "ADDRESS": "Open Space Gombi, Garkida Road, Adamawa State",
   "CELLPHONE": "8032038682",
   "CONTROLLER": "Elisha Drambi",
   "LATITUDE": "10.24563",
   "LONGITUDE": "9.34586"
 },
 {
   "NAME": "Mubi Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Adamawa",
   "LGA": "Mubi",
   "ADDRESS": "Open Space-Kanbang Juction, Mubi, Adamawa State",
   "CELLPHONE": "8032038943",
   "CONTROLLER": "mohamood mohammed",
   "LATITUDE": "10.2679",
   "LONGITUDE": "13.25945"
 },
 {
   "NAME": "Mubi Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Adamawa",
   "LGA": "Mubi",
   "ADDRESS": "84 Sokoto Street, Kolere, Mubi, Adamawa State",
   "CELLPHONE": "8032030442",
   "CONTROLLER": "Arhyel James",
   "LATITUDE": "10.263442",
   "LONGITUDE": "13.263128"
 },
 {
   "NAME": "MTN Mubi Connect  Point",
   "TYPE": "Connect Point",
   "STATE": "Adamawa",
   "LGA": "Mubi South",
   "ADDRESS": "Open Space, Top Ten, Yola Town Bye Pass, Adamawa State",
   "CELLPHONE": "8032037477",
   "CONTROLLER": "Tariela Jantiku",
   "LATITUDE": "12.48132",
   "LONGITUDE": "9.19424"
 },
 {
   "NAME": "MTN Yola Connect",
   "TYPE": "Connect Store",
   "STATE": "Adamawa",
   "LGA": "Yola North",
   "ADDRESS": "42 Ahmadu Bello Way, Opposite  Nta Jimeta, Yola, Adamawa State",
   "CELLPHONE": "8032032156",
   "CONTROLLER": "Jacob Oloyede",
   "LATITUDE": "9.2779431",
   "LONGITUDE": "12.4595224"
 },
 {
   "NAME": "Numan Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Adamawa",
   "LGA": "Yola North",
   "ADDRESS": "Open Space, 1 Numan Road Jimeta/Yola, Adamawa State",
   "CELLPHONE": "8032030991",
   "CONTROLLER": "Felicia Malgwi",
   "LATITUDE": "12.50036",
   "LONGITUDE": "9.18912"
 },
 {
   "NAME": "FUTY Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Adamawa",
   "LGA": "Yola South",
   "ADDRESS": "FUTY, Near The Students Centre, Futy, Yola, Adamawa State",
   "CELLPHONE": "8032031082",
   "CONTROLLER": "Sakiyo Menti",
   "LATITUDE": "9",
   "LONGITUDE": "8.5"
 },
 {
   "NAME": "MTN Yola Connect",
   "TYPE": "Connect Point",
   "STATE": "Adamawa",
   "LGA": "Yola South",
   "ADDRESS": "Unilite Aun, Near The Cafeteria, Aun, Yola, Adamawa State",
   "CELLPHONE": "8032032156",
   "CONTROLLER": "Jacob Oloyede",
   "LATITUDE": "12.42457",
   "LONGITUDE": "9.27741"
 },
 {
   "NAME": "Eket Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Akwa Ibom",
   "LGA": "Eket",
   "ADDRESS": "22 Grace Bill Road, Eket, Akwa Ibom State",
   "CELLPHONE": "8032038100",
   "CONTROLLER": "Ekom Archibong",
   "LATITUDE": "4.636228",
   "LONGITUDE": "7.930996"
 },
 {
   "NAME": "Ibeno Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Akwa Ibom",
   "LGA": "Ibeno",
   "ADDRESS": "Qit Junction Terminal Road, Ibeno, Akwa Ibom State",
   "CELLPHONE": "8032034510",
   "CONTROLLER": "Amy Hagan",
   "LATITUDE": "4.5437",
   "LONGITUDE": "8.0036"
 },
 {
   "NAME": "Ikot Ekpene Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Akwa Ibom",
   "LGA": "Ikot Ekpene",
   "ADDRESS": "33 Umuahia Road, Ikot Ekpene, Akwa Ibom State",
   "CELLPHONE": "8032034513",
   "CONTROLLER": "Unwana Jimmy",
   "LATITUDE": "5.190135",
   "LONGITUDE": "7.710835"
 },
 {
   "NAME": "Oron Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Akwa Ibom",
   "LGA": "Oron",
   "ADDRESS": "79  Murtala Way, Oron, Akwa-Ibom State",
   "CELLPHONE": "8032038245",
   "CONTROLLER": "Kelechi Chinwo",
   "LATITUDE": "4.796453",
   "LONGITUDE": "8.22468"
 },
 {
   "NAME": "Uyo Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Akwa Ibom",
   "LGA": "Uyo",
   "ADDRESS": "41 Ikot-Ekpene Road, Uyo, Akwa Ibom State",
   "CELLPHONE": "8032032131",
   "CONTROLLER": "Fred Udoh",
   "LATITUDE": "5.0392857",
   "LONGITUDE": "7.9149393"
 },
 {
   "NAME": "Ekwulobia Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Anambra",
   "LGA": "Aguata",
   "ADDRESS": "31 Nnewi Road Ekwulobia, Anambra State",
   "CELLPHONE": "8032030106",
   "CONTROLLER": "Chioma Okonkwo",
   "LATITUDE": "6.024341",
   "LONGITUDE": "7.078856"
 },
 {
   "NAME": "Awka Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Anambra",
   "LGA": "Awka South",
   "ADDRESS": "Ground Floor Shop: 331 Zik Avenue, Awka, Anambra State",
   "CELLPHONE": "8032030086",
   "CONTROLLER": "Ogochukwu Dorathy",
   "LATITUDE": "6.022099",
   "LONGITUDE": "7.08307"
 },
 {
   "NAME": "UNIZIK Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Anambra",
   "LGA": "Awka South",
   "ADDRESS": "Nnamdi Azikiwe University (Unizik), Awka, Anambra State",
   "CELLPHONE": "8032037607",
   "CONTROLLER": "Faustina Nwankwu",
   "LATITUDE": "6.2484540",
   "LONGITUDE": "7.1153641"
 },
 {
   "NAME": "Obosi Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Anambra",
   "LGA": "Idemili North",
   "ADDRESS": "KM 4 Onitsha Owerri Road, Onitsha, Anambra State",
   "CELLPHONE": "8032037622",
   "CONTROLLER": "Chidiebere Nwankwo",
   "LATITUDE": "6.1337341",
   "LONGITUDE": "6.7721222"
 },
 {
   "NAME": "Ogidi Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Anambra",
   "LGA": "Idemili North",
   "ADDRESS": "67, Limca Road Nkpor, Anambra State",
   "CELLPHONE": "8032034360",
   "CONTROLLER": "Chinedu Chidozie",
   "LATITUDE": "6.1485924",
   "LONGITUDE": "6.835232"
 },
 {
   "NAME": "Ihiala Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Anambra",
   "LGA": "Ihiala",
   "ADDRESS": "Ihiala Owerri Road, Inside Frank Gill Filling Station, Opposite Ihiala Motor Park, Ihiala, Anambra State",
   "CELLPHONE": "8032034337",
   "CONTROLLER": "Chidi Obilor",
   "LATITUDE": "5.855589",
   "LONGITUDE": "6.859635"
 },
 {
   "NAME": "Uli Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Anambra",
   "LGA": "Ihiala",
   "ADDRESS": "Anambra State University, Uli, Anambra State",
   "CELLPHONE": "8032034307",
   "CONTROLLER": "Bright Nwokolo",
   "LATITUDE": "5.792611",
   "LONGITUDE": "6.860417"
 },
 {
   "NAME": "Nnewi Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Anambra",
   "LGA": "Nnewi North",
   "ADDRESS": "No 15 Onitsha Road, Nnewi, Anambra State",
   "CELLPHONE": "8032030922",
   "CONTROLLER": "Nneamaka Ofoma",
   "LATITUDE": "6.0946548",
   "LONGITUDE": "6.8111692"
 },
 {
   "NAME": "New Market Road Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Anambra",
   "LGA": "Onitsha North",
   "ADDRESS": "New Market Road, Opposite Emeka Offor Plaza, Onitsha, Anambra",
   "CELLPHONE": "8032030682",
   "CONTROLLER": "Ikenna Enem",
   "LATITUDE": "6.150762",
   "LONGITUDE": "6.777195"
 },
 {
   "NAME": "Onitsha Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Anambra",
   "LGA": "Onitsha North",
   "ADDRESS": "75 Old Market Road Onitsha, Anambra State",
   "CELLPHONE": "8032038082",
   "CONTROLLER": "Chizoba Dike",
   "LATITUDE": "6.152819",
   "LONGITUDE": "6.78236"
 },
 {
   "NAME": "Onitsha 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Anambra",
   "LGA": "Onitsha South",
   "ADDRESS": "46, Iweka Road Onitsha, By Diamond Bank, Anambra State",
   "CELLPHONE": "8032034614",
   "CONTROLLER": "Thecla Okpara",
   "LATITUDE": "6.142035",
   "LONGITUDE": "6.782842"
 },
 {
   "NAME": "Onitsha Connect Point 1",
   "TYPE": "Connect Point",
   "STATE": "Anambra",
   "LGA": "Onitsha South",
   "ADDRESS": "Oando Filling Station, Zik Avenue Road, Onitsha, Anambra State",
   "CELLPHONE": "8032030689",
   "CONTROLLER": "Chukwuekezie Mary rose",
   "LATITUDE": "6.77644",
   "LONGITUDE": "6.137728"
 },
 {
   "NAME": "Oyi Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Anambra",
   "LGA": "Oyi",
   "ADDRESS": "Ogbunike Building Material Market, Behind First Bank, Oyi, Anambra State",
   "CELLPHONE": "8032037604",
   "CONTROLLER": "Ann Nnadi",
   "LATITUDE": "6.184736",
   "LONGITUDE": "6.870658"
 },
 {
   "NAME": "ATBU Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Bauchi",
   "LGA": "Bauchi",
   "ADDRESS": "Abubakar Tafawa Balewa University, Behind Fin Bank, Bauchi",
   "CELLPHONE": "8032030868",
   "CONTROLLER": "Sunday Chukwuemeka Nnamani",
   "LATITUDE": "9.790717",
   "LONGITUDE": "10.278765"
 },
 {
   "NAME": "Bauchi Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Bauchi",
   "LGA": "Bauchi",
   "ADDRESS": "Giwo House 6 Ahmadu Bello Way, Opposite Eagle Sina Park, Bauchi",
   "CELLPHONE": "8032030870",
   "CONTROLLER": "Anthony Akubueze",
   "LATITUDE": "10.2903013",
   "LONGITUDE": "9.8034042"
 },
 {
   "NAME": "Azare Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Bauchi",
   "LGA": "Katagum",
   "ADDRESS": "Open Space, Adjacent Yankari Park, Azare, Bauchi",
   "CELLPHONE": "8032030374",
   "CONTROLLER": "Aminu Garba",
   "LATITUDE": "11.675723",
   "LONGITUDE": "10.20876"
 },
 {
   "NAME": "Amassoma Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Bayelsa",
   "LGA": "Southern Ijaw",
   "ADDRESS": "Opposite Niger Delta University Gate, By Winners Chappel",
   "CELLPHONE": "8032030445",
   "CONTROLLER": "Nneamaka Enomah",
   "LATITUDE": "4.973087",
   "LONGITUDE": "6.10897"
 },
 {
   "NAME": "Yenagoa Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Bayelsa",
   "LGA": "Yenagoa",
   "ADDRESS": "Plot 5 DSP Alameseigha Road, Ovon -Epe (Corner Of Sand Field Road)",
   "CELLPHONE": "8032038093",
   "CONTROLLER": "Ukana",
   "LATITUDE": "4.939613",
   "LONGITUDE": "6.276317"
 },
 {
   "NAME": "Gboko Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Benue",
   "LGA": "Gboko",
   "ADDRESS": "No 541, Makir Zakpe Way, Gboko",
   "CELLPHONE": "8032038302",
   "CONTROLLER": "Shiaka Usman",
   "LATITUDE": "7.3254638",
   "LONGITUDE": "8.997238"
 },
 {
   "NAME": "Kastina Ala Connect point",
   "TYPE": "Connect Point",
   "STATE": "Benue",
   "LGA": "Kastina-Ala",
   "ADDRESS": "Hilltop Avenue, Close To College Of Education, Kastina Ala",
   "CELLPHONE": "8032035141",
   "CONTROLLER": "Jude Vekaa",
   "LATITUDE": "7.04411",
   "LONGITUDE": "9.08871"
 },
 {
   "NAME": "Makurdi Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Benue",
   "LGA": "Makurdi",
   "ADDRESS": "Bolek Filling Station, KM 2 Gboko Road, Opposite College Of Science, Makurdi, Benue State.",
   "CELLPHONE": "8032030723",
   "CONTROLLER": "John Onoja",
   "LATITUDE": "8.5121",
   "LONGITUDE": "7.7411"
 },
 {
   "NAME": "Markurdi Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Benue",
   "LGA": "Makurdi",
   "ADDRESS": "17 Railway Byepass, High Level, Makurdi, Benue State.",
   "CELLPHONE": "8032030204",
   "CONTROLLER": "Vera Obe",
   "LATITUDE": "7.725558",
   "LONGITUDE": "8.526001"
 },
 {
   "NAME": "Otukpo Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Benue",
   "LGA": "Otukpo",
   "ADDRESS": "Oando Filling Station, Opp Kadigbo Hotel, Ahmedu Bello Way, Otukpo, Benue State.",
   "CELLPHONE": "8032030723",
   "CONTROLLER": "John Onoja",
   "LATITUDE": "7.1982133",
   "LONGITUDE": "8.1393179"
 },
 {
   "NAME": "Otukpo Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Benue",
   "LGA": "Otukpo",
   "ADDRESS": "No 1 Federal Road, Otukpo, Benue State",
   "CELLPHONE": "8032032869",
   "CONTROLLER": "Mathew Omudu",
   "LATITUDE": "7.200763",
   "LONGITUDE": "8.129444"
 },
 {
   "NAME": "Kano Road  Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Borno",
   "LGA": "Maiduguri",
   "ADDRESS": "Oando Connect Point, After Borno Express Terminus,Kano Road, Maiduguri",
   "CELLPHONE": "8032030449",
   "CONTROLLER": "Iliya Duwara",
   "LATITUDE": "11.8351",
   "LONGITUDE": "13.0922"
 },
 {
   "NAME": "Maduguri 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Borno",
   "LGA": "Maiduguri",
   "ADDRESS": "8 Lagos Road, Opposite Lagos House, Old GRA (Former High Court)",
   "CELLPHONE": "8032034381",
   "CONTROLLER": "ISAIAH AGBER",
   "LATITUDE": "11.8215104",
   "LONGITUDE": "13.1604327"
 },
 {
   "NAME": "Sir Kashim Connect  Point",
   "TYPE": "Connect Point",
   "STATE": "Borno",
   "LGA": "Maiduguri",
   "ADDRESS": "Oando Connect Point, Opp GTBank, Sir Kashim Road, Maiduguri",
   "CELLPHONE": "8032030480",
   "CONTROLLER": "Usman Ishaq",
   "LATITUDE": "11.850577",
   "LONGITUDE": "13.14229"
 },
 {
   "NAME": "Uni Maid Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Borno",
   "LGA": "Maiduguri",
   "ADDRESS": "University Of Maiduguri, Opp The University Caf, Maiduguri",
   "CELLPHONE": "8032030334",
   "CONTROLLER": "Aisha Salau",
   "LATITUDE": "11.805518",
   "LONGITUDE": "13.200309"
 },
 {
   "NAME": "MCC Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Cross River",
   "LGA": "Calabar Municipal",
   "ADDRESS": "MCC Junction, Calabar",
   "CELLPHONE": "8032034500",
   "CONTROLLER": "Enangha Eyaba",
   "LATITUDE": "4.987056",
   "LONGITUDE": "8.3333866"
 },
 {
   "NAME": "Calabar Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Cross Rivers",
   "LGA": "Calabar Municipal",
   "ADDRESS": "72 Etta Agbor, Calabar, Cross Rivers",
   "CELLPHONE": "8032038090",
   "CONTROLLER": "Samuel Jonah",
   "LATITUDE": "4.956984",
   "LONGITUDE": "8.343318"
 },
 {
   "NAME": "Ikom Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Cross Rivers",
   "LGA": "Ikom",
   "ADDRESS": "70 Calabar Road, Master Energy Filling Station Opposite Border Road, Ikom, Cross Rivers State",
   "CELLPHONE": "8032034515",
   "CONTROLLER": "Erasmus Ekara",
   "LATITUDE": "8.43547",
   "LONGITUDE": "5.57746"
 },
 {
   "NAME": "Ogwuashiuku Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Delta",
   "LGA": "Aniocha South",
   "ADDRESS": "5 Ogbe Umuokwuni Qtrs,  Beside Ogwashi-Uku Magistrate Court, Close To Ogwashi-Uku Library, Delta State",
   "CELLPHONE": "8032032153",
   "CONTROLLER": "Cajethan Eze",
   "LATITUDE": "6.170348",
   "LONGITUDE": "6.527313"
 },
 {
   "NAME": "Abraka Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Delta",
   "LGA": "Ethiop East",
   "ADDRESS": "Sudoz Filling Station, Abraka Obaruku Rd, Abraka, Delta State.",
   "CELLPHONE": "8032034618",
   "CONTROLLER": "Cynthia Okotie",
   "LATITUDE": "5.789507",
   "LONGITUDE": "6.102347"
 },
 {
   "NAME": "Agbor Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Delta",
   "LGA": "Ika North East",
   "ADDRESS": "197, Old Lagos-Asaba Road, Agbor, Delta State.",
   "CELLPHONE": "8032030686",
   "CONTROLLER": "Victoria Olowogbayi",
   "LATITUDE": "6.255004",
   "LONGITUDE": "6.190125"
 },
 {
   "NAME": "Ozoro Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Delta",
   "LGA": "Isoko North",
   "ADDRESS": "G.F.S International Filling Station, Ozoro-Kwale Expressway, Opp Elim Guest House, Ozoro, Delta State.",
   "CELLPHONE": "8032030171",
   "CONTROLLER": "Mary Akankali",
   "LATITUDE": "5.5383",
   "LONGITUDE": "6.2161"
 },
 {
   "NAME": "Ole Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Delta",
   "LGA": "Isoko South",
   "ADDRESS": "1 Kefas Road Oleh, Isoko South, Delta State",
   "CELLPHONE": "8032037634",
   "CONTROLLER": "Emordi Ngozi",
   "LATITUDE": "5.356472",
   "LONGITUDE": "6.200294"
 },
 {
   "NAME": "Kwale Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Delta",
   "LGA": "Ndokwa East",
   "ADDRESS": "Ferdikat Filling Station Umusadege Road Opp Delta Line, Kwale, Delta State.",
   "CELLPHONE": "8032034590",
   "CONTROLLER": "Faustina Nwankwo",
   "LATITUDE": "5.700202",
   "LONGITUDE": "6.434383"
 },
 {
   "NAME": "Asaba Service Center",
   "TYPE": "Service Centre",
   "STATE": "Delta",
   "LGA": "Oshimili south",
   "ADDRESS": "KM 129 Benin-Asaba Expressway, Asaba, Delta State.",
   "CELLPHONE": "8032009338",
   "CONTROLLER": "Rosemary Ukpebor",
   "LATITUDE": "6.267073",
   "LONGITUDE": "6.188527"
 },
 {
   "NAME": "Sapele Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Delta",
   "LGA": "Sapele",
   "ADDRESS": "31, New Ogorode Road, Sapele, Delta State.",
   "CELLPHONE": "8032034620",
   "CONTROLLER": "Christiana Adedigba",
   "LATITUDE": "5.916879",
   "LONGITUDE": "5.648101"
 },
 {
   "NAME": "Ughelli Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Delta",
   "LGA": "Ughelli North",
   "ADDRESS": "120 Isoko Road By NNPC Roundabout, Delta State.",
   "CELLPHONE": "8032034609",
   "CONTROLLER": "Francis Iwhewhe",
   "LATITUDE": "5.490919",
   "LONGITUDE": "6.00166"
 },
 {
   "NAME": "Warri Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Delta",
   "LGA": "Uvwie",
   "ADDRESS": "46 Effurun-Sapele Road, Delta State",
   "CELLPHONE": "8032038105",
   "CONTROLLER": "Victoria James",
   "LATITUDE": "5.515593",
   "LONGITUDE": "5.75333"
 },
 {
   "NAME": "Decco Road Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Delta",
   "LGA": "Warri South",
   "ADDRESS": "Opposite Robinson Plaza, Deco Road, Warri, Delta State",
   "CELLPHONE": "8032037599",
   "CONTROLLER": "Jaiyeola",
   "LATITUDE": "5.525941",
   "LONGITUDE": "5.760564"
 },
 {
   "NAME": "Warri 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Delta",
   "LGA": "Warri south",
   "ADDRESS": "98, Warri Sapele Road, Delta State.",
   "CELLPHONE": "8032034605",
   "CONTROLLER": "stephanie idehen",
   "LATITUDE": "5.4908719",
   "LONGITUDE": "6.0017314"
 },
 {
   "NAME": "Abakaliki Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Ebonyi",
   "LGA": "Abakaliki",
   "ADDRESS": "38 Ogoja Rd, opp FCMB Bank, Abakaliki, Ebonyi State.",
   "CELLPHONE": "8032032077",
   "CONTROLLER": "Esther Ojiakor",
   "LATITUDE": "6.317536",
   "LONGITUDE": "8.115743"
 },
 {
   "NAME": "EBSU Unilite",
   "TYPE": "Connect Point",
   "STATE": "Ebonyi",
   "LGA": "Abakaliki",
   "ADDRESS": "Ebonyi State University ,Cas Campus, Ebonyi State.",
   "CELLPHONE": "8032034346",
   "CONTROLLER": "Emmanuel Agbom",
   "LATITUDE": "6.309957",
   "LONGITUDE": "8.1021"
 },
 {
   "NAME": "Benin 3 Connect Store",
   "TYPE": "Connect Point",
   "STATE": "Edo",
   "LGA": "Egor",
   "ADDRESS": "155 Uselu/Lagos Road, Opposite Eco Bank, Office, Benin City, Edo State.",
   "CELLPHONE": "8032030168",
   "CONTROLLER": "Sunny Umoru",
   "LATITUDE": "6.385229",
   "LONGITUDE": "5.60963"
 },
 {
   "NAME": "Ekenwa Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Edo",
   "LGA": "Egor",
   "ADDRESS": "Plymount Junction, Ekenwa, Benin City, Edo State.",
   "CELLPHONE": "8032038700",
   "CONTROLLER": "Daramola Labake",
   "LATITUDE": "6.337443",
   "LONGITUDE": "5.611608"
 },
 {
   "NAME": "Oluku Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Edo",
   "LGA": "Egor",
   "ADDRESS": "PEC filling station, Oluku, Benin City, Edo State.",
   "CELLPHONE": "7031585809",
   "CONTROLLER": "Ikeahor Oviawe",
   "LATITUDE": "6.453717",
   "LONGITUDE": "5.595868"
 },
 {
   "NAME": "Ekpoma Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Edo",
   "LGA": "Esan Central",
   "ADDRESS": "Obasanjo Way, Ekpoma, Edo State",
   "CELLPHONE": "8032037636",
   "CONTROLLER": "Sule Sekinatu",
   "LATITUDE": "6.752703",
   "LONGITUDE": "6.073337"
 },
 {
   "NAME": "Uromi Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Edo",
   "LGA": "Esan North East",
   "ADDRESS": "Oando Filling Station, 5 Ubiaja Road, Uromi, Edo State.",
   "CELLPHONE": "8032034317",
   "CONTROLLER": "Amiuiki Abure",
   "LATITUDE": "6.709191",
   "LONGITUDE": "6.330461"
 },
 {
   "NAME": "Auchi Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Edo",
   "LGA": "Etsako West",
   "ADDRESS": "8 Old court road by public field Auchi, Edo State.",
   "CELLPHONE": "8032030169",
   "CONTROLLER": "Momodu",
   "LATITUDE": "7.04043",
   "LONGITUDE": "6.15485"
 },
 {
   "NAME": "Benin 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Edo",
   "LGA": "Ikpoba-Okha",
   "ADDRESS": "155 Sapele Road, Edo State",
   "CELLPHONE": "8032034640",
   "CONTROLLER": "Usman mohammed",
   "LATITUDE": "6.183323",
   "LONGITUDE": "5.378467"
 },
 {
   "NAME": "Benin Agbor Road Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Edo",
   "LGA": "Ikpoba-Okha",
   "ADDRESS": "Oando Fillig Station, 122, Benin-Agbor Road, Opposite Doris Dey Hotel, Edo State.",
   "CELLPHONE": "8032030521",
   "CONTROLLER": "Aigbokhaewolo Eseigbe",
   "LATITUDE": "6.342493",
   "LONGITUDE": "5.694804"
 },
 {
   "NAME": "Akpakpava Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Edo",
   "LGA": "Oredo",
   "ADDRESS": "Oando Fillig Station, Akpakpava, Edo State.",
   "CELLPHONE": "8032030450",
   "CONTROLLER": "Joy Okpukpu",
   "LATITUDE": "6.340912",
   "LONGITUDE": "5.632123"
 },
 {
   "NAME": "Benin Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Edo",
   "LGA": "Oredo",
   "ADDRESS": "89 Akapakpava Road, Benin City, Edo State.",
   "CELLPHONE": "8032038286",
   "CONTROLLER": "Joshua  Adeyemo",
   "LATITUDE": "6.309292",
   "LONGITUDE": "5.640212"
 },
 {
   "NAME": "New Lagos Road Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Edo",
   "LGA": "Oredo",
   "ADDRESS": "Oando Fillig Station, 45 New Lagos Road, Benin, Edo State.",
   "CELLPHONE": "8032030522",
   "CONTROLLER": "Lucky Ehizogie",
   "LATITUDE": "6.346494",
   "LONGITUDE": "5.634738"
 },
 {
   "NAME": "Sapele Road Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Edo",
   "LGA": "Oredo",
   "ADDRESS": "Great foods, 107 Ekenwan road, Benin City, Edo State.",
   "CELLPHONE": "8032030510",
   "CONTROLLER": "Kpobor Ogheovo A",
   "LATITUDE": "6.330193",
   "LONGITUDE": "5.603993"
 },
 {
   "NAME": "Urubi Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Edo",
   "LGA": "Oredo",
   "ADDRESS": "Oando Fillig Station,9B Urubi Road, Iyaro, Benin City, Edo State.",
   "CELLPHONE": "8032030474",
   "CONTROLLER": "Imade Ihempken",
   "LATITUDE": "6.346871",
   "LONGITUDE": "5.624161"
 },
 {
   "NAME": "Ado Ekiti 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Ekiti",
   "LGA": "Ado-Ekiti",
   "ADDRESS": "11 Iyin Road, Beside Captain Cook, Ado Ekiti, Ekiti State.",
   "CELLPHONE": "8032035028",
   "CONTROLLER": "Daodu Oluwamodupe",
   "LATITUDE": "7.6346214",
   "LONGITUDE": "5.212052"
 },
 {
   "NAME": "Ado Ekiti Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Ekiti",
   "LGA": "Ado-Ekiti",
   "ADDRESS": "5, PNN House, Beside Mr Bigggs, Fajuyi, Ado Ekiti, Ekiti State.",
   "CELLPHONE": "8032037770",
   "CONTROLLER": "olumayowa.aluko",
   "LATITUDE": "7.612446",
   "LONGITUDE": "5.237788"
 },
 {
   "NAME": "EKSU Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Ekiti",
   "LGA": "Ado-Ekiti",
   "ADDRESS": "Campus Inner Road, Opp Old Sug Building Eksu, Ado Ekiti, Ekiti State.",
   "CELLPHONE": "8032034553",
   "CONTROLLER": "Mayowa Adekola",
   "LATITUDE": "5.257062",
   "LONGITUDE": "7.611229"
 },
 {
   "NAME": "Federal Poly Ado Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Ekiti",
   "LGA": "Ado-Ekiti",
   "ADDRESS": "Motion Ground, Federal Poly. Ado Ekiti, Ekiti State.",
   "CELLPHONE": "8032032825",
   "CONTROLLER": "Stella Afolabi",
   "LATITUDE": "7.605665",
   "LONGITUDE": "5.288578"
 },
 {
   "NAME": "Efon Alaye Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Ekiti",
   "LGA": "Akoko North East",
   "ADDRESS": "Itamesi, Opp Sunbeam Microfinance Bank, Efon, Ekiti State.",
   "CELLPHONE": "8032037688",
   "CONTROLLER": "Mohammed Ogunro",
   "LATITUDE": "4.922527",
   "LONGITUDE": "7.655888"
 },
 {
   "NAME": "Ikare-Akoko (Oba Palace Connect Point)",
   "TYPE": "Connect Point",
   "STATE": "Ekiti",
   "LGA": "Akoko North-East",
   "ADDRESS": "Ikare-Akoko (Oba Palace CS Point), Ekiti State.",
   "CELLPHONE": "8032034552",
   "CONTROLLER": "Oluwafemi Olatunde",
   "LATITUDE": "5.75278",
   "LONGITUDE": "7.52374"
 },
 {
   "NAME": "Abakpa Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Enugu",
   "LGA": "Enugu East",
   "ADDRESS": " 56b Amuri Road, Federal Housing estate by Mekan filling station. Trans-Ekulu/Abakpa, Enugu State",
   "CELLPHONE": "8032032155",
   "CONTROLLER": "Phynian Obiora",
   "LATITUDE": "6.4820328",
   "LONGITUDE": "7.5058277"
 },
 {
   "NAME": "Afikpo",
   "TYPE": "Connect Point",
   "STATE": "Enugu",
   "LGA": "Afikpo North",
   "ADDRESS": "53A Eke Market Road, Amangballa Roundaboubt, Ebonyi State",
   "CELLPHONE": "8032037734",
   "CONTROLLER": "Ibiam Ezi",
   "LATITUDE": "5.902567",
   "LONGITUDE": "7.922301"
 },
 {
   "NAME": "Enugu Shoprite Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Enugu",
   "LGA": "Enugu North",
   "ADDRESS": "Shop 55, Polopark mall, MTN Connect Store, shoprite, Enugu State",
   "CELLPHONE": "8032030757",
   "CONTROLLER": "Nwoye",
   "LATITUDE": "6.459288",
   "LONGITUDE": "7.49594"
 },
 {
   "NAME": "New Haven Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Enugu",
   "LGA": "Enugu North",
   "ADDRESS": "Discovery Plaza, 96/98 Chime Avenue, New Haven- Enugu.",
   "CELLPHONE": "8032030261",
   "CONTROLLER": "Chukwudi Iwuoha",
   "LATITUDE": "6.4578462",
   "LONGITUDE": "7.5235329"
 },
 {
   "NAME": "IMT Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Enugu",
   "LGA": "Enugu South",
   "ADDRESS": "Institution Of Management Technology (IMT), Enugu",
   "CELLPHONE": "8032037612",
   "CONTROLLER": "Nkeiru",
   "LATITUDE": "6.448413",
   "LONGITUDE": "7.489455"
 },
 {
   "NAME": "UNEC Unilite",
   "TYPE": "Connect Point",
   "STATE": "Enugu",
   "LGA": "Enugu South",
   "ADDRESS": "34 Zik Avenue, Uwani, Enugu",
   "CELLPHONE": "8032038695",
   "CONTROLLER": "Oluwaseun Akinrotimi",
   "LATITUDE": "6.43242",
   "LONGITUDE": "7.49388"
 },
 {
   "NAME": "ESUT Unilite",
   "TYPE": "Connect Point",
   "STATE": "Enugu",
   "LGA": "Nkanu West",
   "ADDRESS": "Enugu State University",
   "CELLPHONE": "8032034348",
   "CONTROLLER": "Kingsley",
   "LATITUDE": "6.887109",
   "LONGITUDE": "7.410582"
 },
 {
   "NAME": "Nsukka Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Enugu",
   "LGA": "Nsukka",
   "ADDRESS": "56 university Market road nsukka",
   "CELLPHONE": "8032033892",
   "CONTROLLER": "Nwakalor",
   "LATITUDE": "6.8644553",
   "LONGITUDE": "7.4082883"
 },
 {
   "NAME": "UNN Road ",
   "TYPE": "Connect Point",
   "STATE": "Enugu",
   "LGA": "Nsukka",
   "ADDRESS": "Oando Filling Station, 18 Unn Road, Nssuka, Enugu",
   "CELLPHONE": "8032034305",
   "CONTROLLER": "Edith",
   "LATITUDE": "6.500505",
   "LONGITUDE": "7.514122"
 },
 {
   "NAME": "Garki Connect Point Enugu",
   "TYPE": "Connect Point",
   "STATE": "Enugu",
   "LGA": "Udi",
   "ADDRESS": "Opposite Satellite (MTD), By First Bank Plc, Garki, Agbani Road, Enugu",
   "CELLPHONE": "8032038694",
   "CONTROLLER": "William Paul",
   "LATITUDE": "9.404974",
   "LONGITUDE": "7.607418"
 },
 {
   "NAME": "Kumo Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Gombe",
   "LGA": "Akko",
   "ADDRESS": "Open Space, Central Primary School Kumo, Akko LGA, Gombe",
   "CELLPHONE": "8032032671",
   "CONTROLLER": "Auwal Ahmed",
   "LATITUDE": "12.3222",
   "LONGITUDE": "10.293478"
 },
 {
   "NAME": "Biliri Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Gombe",
   "LGA": "Biliri",
   "ADDRESS": "Open Space, Biliri Local Gov'T Shopping Complex, Biliri, Gombe",
   "CELLPHONE": "8032037674",
   "CONTROLLER": "Seun Olojide",
   "LATITUDE": "11.166667",
   "LONGITUDE": "10.283333"
 },
 {
   "NAME": "Gombe 1 open space Connect Point.",
   "TYPE": "Connect Point",
   "STATE": "Gombe",
   "LGA": "Gombe",
   "ADDRESS": "Open Space, Opp Ostrich Bakery, Gombe",
   "CELLPHONE": "8032030390",
   "CONTROLLER": "Yusuf Lamido Shuaibu",
   "LATITUDE": "11.28",
   "LONGITUDE": "10.333245"
 },
 {
   "NAME": "Gombe Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Gombe",
   "LGA": "Gombe",
   "ADDRESS": "10, Federal Low-Cost Housing Road, Off Olusegun Obasanjo, Gombe",
   "CELLPHONE": "8032032164",
   "CONTROLLER": "Godwin Adole",
   "LATITUDE": "10.29025",
   "LONGITUDE": "11.146777"
 },
 {
   "NAME": "Gombe Shopping Complex Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Gombe",
   "LGA": "Gombe",
   "ADDRESS": "Open Space Along Emir's Place Road, Shopping Complex, Gombe",
   "CELLPHONE": "8032032672",
   "CONTROLLER": "Blessing Benard",
   "LATITUDE": "12.11112",
   "LONGITUDE": "10.183333"
 },
 {
   "NAME": "Akwuakuma Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Imo",
   "LGA": "Mbaitolu",
   "ADDRESS": "Oweni Plaza, Akwakuma Junction, Owerri",
   "CELLPHONE": "8032034616",
   "CONTROLLER": "Kingsley Nnodim",
   "LATITUDE": "5.482375",
   "LONGITUDE": "7.015096"
 },
 {
   "NAME": "Orlu Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Imo",
   "LGA": "Orlu",
   "ADDRESS": "1/2 Bank Road, Orlu",
   "CELLPHONE": "8032034339",
   "CONTROLLER": "Jane.Osondu",
   "LATITUDE": "5.28571",
   "LONGITUDE": "7.02252"
 },
 {
   "NAME": "Alvan Ikoku Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Imo",
   "LGA": "Owerri Municipal",
   "ADDRESS": "Alvan Ikoku Federal College of Education Owerri, Behind Lawn Tennis Court",
   "CELLPHONE": "8032037615",
   "CONTROLLER": "Sunday Nweke",
   "LATITUDE": "7.023613",
   "LONGITUDE": "5.500261"
 },
 {
   "NAME": "Owerri Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Imo",
   "LGA": "Owerri Municipal",
   "ADDRESS": "116 Wetheral Road, Owerri ",
   "CELLPHONE": "8032030114",
   "CONTROLLER": "Chinenye Monye",
   "LATITUDE": "5.4911991",
   "LONGITUDE": "7.0356607"
 },
 {
   "NAME": "State Secretarite Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Imo",
   "LGA": "Owerri Municipal",
   "ADDRESS": "State Secreteriate New Owerri By Portharcourt Road,Owerri",
   "CELLPHONE": "8032037614",
   "CONTROLLER": "Nwachukwu Uzoma",
   "LATITUDE": "7.01223",
   "LONGITUDE": "5.475417"
 },
 {
   "NAME": "IMSU Unilite",
   "TYPE": "Connect Point",
   "STATE": "Imo",
   "LGA": "Owerri North",
   "ADDRESS": "Imo State University (IMSU)",
   "CELLPHONE": "8032034578",
   "CONTROLLER": "Elizabeth Okebata",
   "LATITUDE": "5.506",
   "LONGITUDE": "7.043"
 },
 {
   "NAME": "Naze Timber Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Imo",
   "LGA": "Owerri North",
   "ADDRESS": "Naze Timber/Buiding Material, Beside Obinta Hall, Owerri North",
   "CELLPHONE": "8032037616",
   "CONTROLLER": "Nkechi Egenonu",
   "LATITUDE": "5.601",
   "LONGITUDE": "7.321"
 },
 {
   "NAME": "FUTO Unilite",
   "TYPE": "Connect Point",
   "STATE": "Imo",
   "LGA": "Owerri West",
   "ADDRESS": "Federal University Of Technology, Futo",
   "CELLPHONE": "8032030112",
   "CONTROLLER": "Chioma Mgbachi",
   "LATITUDE": "5.384613",
   "LONGITUDE": "6.999442"
 },
 {
   "NAME": "Nekede Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Imo",
   "LGA": "Owerri West",
   "ADDRESS": "Nekede Federal Polytechnic, Owerri",
   "CELLPHONE": "8032037598",
   "CONTROLLER": "Chinwe Ikwuagwu",
   "LATITUDE": "7.030263",
   "LONGITUDE": "5.433503"
 },
 {
   "NAME": "Owerri 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Imo",
   "LGA": "Owerri west",
   "ADDRESS": "Plot 21 old D World Bank Road Owerri, New Owerri Housing Estate, Owerri",
   "CELLPHONE": "8032030517",
   "CONTROLLER": "Unegbu",
   "LATITUDE": "5.501229",
   "LONGITUDE": "6.986989"
 },
 {
   "NAME": "Dutse Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Jigawa",
   "LGA": "Dutse",
   "ADDRESS": "Beside Asmau House, Sani Abacha Way, Dutse.",
   "CELLPHONE": "8032032177",
   "CONTROLLER": "Mohammed",
   "LATITUDE": "11.712596",
   "LONGITUDE": "9.353894"
 },
 {
   "NAME": "Gumel Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Jigawa",
   "LGA": "Gumel",
   "ADDRESS": "5 Maigatari Road, Opposite Oando Filling Station, Gumel",
   "CELLPHONE": "8032034017",
   "CONTROLLER": "Yusuf   Alkasim ",
   "LATITUDE": "12.633685",
   "LONGITUDE": "9.38474"
 },
 {
   "NAME": "Hadejia Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Jigawa",
   "LGA": "Hadejia",
   "ADDRESS": "7 Malam Madori Road, Opposite Oando Filling Station, Hadejia, Jigawa State",
   "CELLPHONE": "8032038946",
   "CONTROLLER": "Abdullahi  Dauda",
   "LATITUDE": "12.61844",
   "LONGITUDE": "9.737462"
 },
 {
   "NAME": "Igabi Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kaduna",
   "LGA": "Igabi",
   "ADDRESS": "Abuja Road Bypass, Amingo Junction, beside GT Bank, Kaduna",
   "CELLPHONE": "8032034699",
   "CONTROLLER": "LARAI",
   "LATITUDE": "9.614174",
   "LONGITUDE": "8.393537"
 },
 {
   "NAME": "Kafanchan Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kaduna",
   "LGA": "Jema'a",
   "ADDRESS": "Opposite Access Bank, Jos Road, Kafanachan, Kaduna State",
   "CELLPHONE": "8032038961",
   "CONTROLLER": "Oludare Laniru",
   "LATITUDE": "9.58333",
   "LONGITUDE": "8.38129"
 },
 {
   "NAME": "Kachia Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kaduna",
   "LGA": "Kachia",
   "ADDRESS": "Mai Jama'a Shopping Complex, Awan Junction, Kachia",
   "CELLPHONE": "8032037024",
   "CONTROLLER": "Mustapha",
   "LATITUDE": "10.6098",
   "LONGITUDE": "7.17277"
 },
 {
   "NAME": "Junction Road Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kaduna",
   "LGA": "Kaduna North",
   "ADDRESS": "8, Junction Road, Kaduna",
   "CELLPHONE": "8032034749",
   "CONTROLLER": "Jackson peter Isangedige",
   "LATITUDE": "10.5053132",
   "LONGITUDE": "7.428512"
 },
 {
   "NAME": "Kadpoly Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kaduna",
   "LGA": "Kaduna North",
   "ADDRESS": "Kaduna Polytechnic, Tudun Wada Main Campus, Behind Catering Kitchen, Kaduna",
   "CELLPHONE": "8032035144",
   "CONTROLLER": "Magdaline Okwara",
   "LATITUDE": "10.522856",
   "LONGITUDE": "7.415388"
 },
 {
   "NAME": "Kaduna Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kaduna",
   "LGA": "Kaduna North",
   "ADDRESS": "5, Yakubu Gowon Way, Kaduna",
   "CELLPHONE": "8032038229",
   "CONTROLLER": "Ejikeme Udah",
   "LATITUDE": "10.5223599",
   "LONGITUDE": "7.4309528"
 },
 {
   "NAME": "Kaduna Service Center",
   "TYPE": "Service Centre",
   "STATE": "Kaduna",
   "LGA": "Kaduna North",
   "ADDRESS": "Bank Road, Off Ahmadu Bello Way, Kaduna",
   "CELLPHONE": "8032003006",
   "CONTROLLER": "Yakubu Audu",
   "LATITUDE": "10.52889",
   "LONGITUDE": "7.42973"
 },
 {
   "NAME": "Kawo Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kaduna",
   "LGA": "Kaduna North",
   "ADDRESS": "Oando Filling Station Near Motor Park, Kawo, Kaduna",
   "CELLPHONE": "8032030812",
   "CONTROLLER": "Jamilu Hassan",
   "LATITUDE": "10.585296",
   "LONGITUDE": "7.447167"
 },
 {
   "NAME": "Kakuri Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kaduna",
   "LGA": "Kaduna South",
   "ADDRESS": "Oando Filling Station, Textile Road, Kaduna",
   "CELLPHONE": "8032035143",
   "CONTROLLER": "Aliyu Mohammed",
   "LATITUDE": "10.52203",
   "LONGITUDE": "7.40347"
 },
 {
   "NAME": "Lere Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kaduna",
   "LGA": "Lere",
   "ADDRESS": "Lere Junction, Kachia Road ",
   "CELLPHONE": "8032030821",
   "CONTROLLER": "Falalu Mato",
   "LATITUDE": "10.45098",
   "LONGITUDE": "7.44291"
 },
 {
   "NAME": "Saminaka Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kaduna",
   "LGA": "Lere",
   "ADDRESS": "Oando Filling Station, Saminaka, Kaduna",
   "CELLPHONE": "8032038961",
   "CONTROLLER": "Maxwell Hycent Ocheme",
   "LATITUDE": "10.411112",
   "LONGITUDE": "8.665681"
 },
 {
   "NAME": "PZ Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kaduna",
   "LGA": "Zaria",
   "ADDRESS": "1 Park Road, Oando Filling Station, PZ, Zaria",
   "CELLPHONE": "8032030809",
   "CONTROLLER": "Yahaya Hassan",
   "LATITUDE": "11.1018",
   "LONGITUDE": "7.720498"
 },
 {
   "NAME": "Samaru Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kaduna",
   "LGA": "Zaria",
   "ADDRESS": "1 Leather Road, Samaru, Zaria",
   "CELLPHONE": "8032030816",
   "CONTROLLER": "Ehiz Oyinbo",
   "LATITUDE": "11.156187",
   "LONGITUDE": "7.657139"
 },
 {
   "NAME": "Zaria Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kaduna",
   "LGA": "Zaria",
   "ADDRESS": "20 Ahmed Makarfi Road, Tudunwada, Zaria",
   "CELLPHONE": "8032030625",
   "CONTROLLER": "Musa",
   "LATITUDE": "11.1404144",
   "LONGITUDE": "7.7131479"
 },
 {
   "NAME": "Lemu Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kano",
   "LGA": "Dala",
   "ADDRESS": "Oando Filling Station, KM 15 Daura Road, Rijjiya Lemu, Kano",
   "CELLPHONE": "8032037719",
   "CONTROLLER": "George Amegbe",
   "LATITUDE": "12.05683",
   "LONGITUDE": "8.470706"
 },
 {
   "NAME": "Ungogo Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kano",
   "LGA": "Dala",
   "ADDRESS": "Katsina Road, Ungogo, Kano State",
   "CELLPHONE": "8032037732",
   "CONTROLLER": "Emem Bassey",
   "LATITUDE": "12.042714",
   "LONGITUDE": "8.483961"
 },
 {
   "NAME": "Kano Service Center",
   "TYPE": "Service Centre",
   "STATE": "Kano",
   "LGA": "Fagge",
   "ADDRESS": "2, Civic Centre, By Post Office, Kano",
   "CELLPHONE": "8032008064",
   "CONTROLLER": "Abubakar Zubairu",
   "LATITUDE": "12.0004497",
   "LONGITUDE": "8.5371659"
 },
 {
   "NAME": "Kano 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kano",
   "LGA": "Gwale",
   "ADDRESS": "2334, Sani Mai Nagge Road, Kano",
   "CELLPHONE": "8032038052",
   "CONTROLLER": "Ibrahim",
   "LATITUDE": "11.989047",
   "LONGITUDE": "8.502659"
 },
 {
   "NAME": "Challawa Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kano",
   "LGA": "Kumbotso",
   "ADDRESS": "Oando Filling Station, Sani Mainage, Challawa, Kano",
   "CELLPHONE": "8032032705",
   "CONTROLLER": "Kabiru Shehu Mohammed",
   "LATITUDE": "11.995952",
   "LONGITUDE": "8.515966"
 },
 {
   "NAME": "Tarauni Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kano",
   "LGA": "Nassarawa",
   "ADDRESS": "64 Hadejia Road, Tarauni, Kano",
   "CELLPHONE": "8032038048",
   "CONTROLLER": "Joshua Ogbaje",
   "LATITUDE": "12.0087904",
   "LONGITUDE": "8.5458213"
 },
 {
   "NAME": "Kano Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kano",
   "LGA": "Tarauni",
   "ADDRESS": "5A, Zoo Road, Kano",
   "CELLPHONE": "8032038052",
   "CONTROLLER": "Danjuma",
   "LATITUDE": "11.987632",
   "LONGITUDE": "8.497655"
 },
 {
   "NAME": "BUK Unilite",
   "TYPE": "Connect Point",
   "STATE": "Kano",
   "LGA": "Ungogo",
   "ADDRESS": "Bayero University, Cocoa Village Buk New Site, Kano (Buk)",
   "CELLPHONE": "8032032707",
   "CONTROLLER": "Thompson Ogagefe",
   "LATITUDE": "11.973784",
   "LONGITUDE": "8.425759"
 },
 {
   "NAME": "Wudil Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kano",
   "LGA": "Wudil",
   "ADDRESS": "Kano University Of Science & Technology, Behind Engineering Complex, Wudil, Kano",
   "CELLPHONE": "8032032710",
   "CONTROLLER": "Garba Wudil",
   "LATITUDE": "11.801253",
   "LONGITUDE": "8.84117"
 },
 {
   "NAME": "Bakori Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Katsina",
   "LGA": "Bakori",
   "ADDRESS": "Opp House 1, Filin Yan Haya,  Bakori, Katsina",
   "CELLPHONE": "8032032718",
   "CONTROLLER": "Ibrahim   Sada                              ",
   "LATITUDE": "11.557737",
   "LONGITUDE": "7.424894"
 },
 {
   "NAME": "Daura Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Katsina",
   "LGA": "Daura",
   "ADDRESS": "Oando Filling Station, Old Kano Road, Daura, Katsina",
   "CELLPHONE": "8032030893",
   "CONTROLLER": "Sabiu Abdu Zango",
   "LATITUDE": "12.473629",
   "LONGITUDE": "7.873505"
 },
 {
   "NAME": "Dutsin Ma Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Katsina",
   "LGA": "Dutsin-Ma",
   "ADDRESS": "12 Dutsin Ma Road, Adjacent Unity Bank Plc, Katsina",
   "CELLPHONE": "8032032818",
   "CONTROLLER": "AbbA Karare",
   "LATITUDE": "12.45815",
   "LONGITUDE": "7.496766"
 },
 {
   "NAME": "Funtua Connect Point (Oando)",
   "TYPE": "Connect Point",
   "STATE": "Katsina",
   "LGA": "Funtua",
   "ADDRESS": "Funtua Connect Point (Oando), Funtua",
   "CELLPHONE": "7038993334",
   "CONTROLLER": "Yahaya Umar",
   "LATITUDE": "11.531917",
   "LONGITUDE": "7.326486"
 },
 {
   "NAME": "Malumfashi Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Katsina",
   "LGA": "Funtua",
   "ADDRESS": "Opp. Funtua Police Station, Kano Road, Malunfashi - Katsina State.",
   "CELLPHONE": "8032034689",
   "CONTROLLER": "Sadeeq",
   "LATITUDE": "11.793587",
   "LONGITUDE": "7.623481"
 },
 {
   "NAME": "Jibia Connect Point",
   "TYPE": "Connect Point",
   "STATE": "katsina",
   "LGA": "jibia",
   "ADDRESS": "Afdin Filling Station, Custom Barrack Road, Magama Jibia, Katsina",
   "CELLPHONE": "8032032804",
   "CONTROLLER": "Tirmizi yahaya",
   "LATITUDE": "12.969229",
   "LONGITUDE": "7.599249"
 },
 {
   "NAME": "Katsina Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Katsina",
   "LGA": "katsina",
   "ADDRESS": "Ground Floor Shop, Madugu House, Dustin-Ma Road, Kwaya, Katsina State",
   "CELLPHONE": "8032030265",
   "CONTROLLER": "Muhammad",
   "LATITUDE": "12.9833333",
   "LONGITUDE": "7.6"
 },
 {
   "NAME": "Lorry Park connect Point",
   "TYPE": "Connect Point",
   "STATE": "Katsina",
   "LGA": "Katsina",
   "ADDRESS": "Oando Filling Station, Lorry Park, Katsina",
   "CELLPHONE": "8032034020",
   "CONTROLLER": "stella okereke",
   "LATITUDE": "7.585716",
   "LONGITUDE": "12.988429"
 },
 {
   "NAME": "Funtua Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Katsina",
   "LGA": "Malumfashi",
   "ADDRESS": "159 Zaria Road, Adjacent To Uba Bank, Funtua, Katsina State.",
   "CELLPHONE": "8032035031",
   "CONTROLLER": "Sabdat",
   "LATITUDE": "11.517462",
   "LONGITUDE": "7.313713"
 },
 {
   "NAME": "Argungu Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kebbi",
   "LGA": "Argungu",
   "ADDRESS": "Opposite Sama Secondary School, Argungu, Kebbi",
   "CELLPHONE": "8032032810",
   "CONTROLLER": "Abubakar Adamu",
   "LATITUDE": "12.7531416",
   "LONGITUDE": "4.5401583"
 },
 {
   "NAME": "Jega Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kebbi",
   "LGA": "Jega",
   "ADDRESS": "Beside Old CPC Office, Niima, Jega",
   "CELLPHONE": "8032032812",
   "CONTROLLER": "Atiku Alhaji Yusuf",
   "LATITUDE": "12.225827",
   "LONGITUDE": "4.382195"
 },
 {
   "NAME": "Birnin Kebbi Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kebbi",
   "LGA": "Zuru",
   "ADDRESS": "3, Jos Road, Opposite Access Bank, GRA Birnin Kebbi, Kebbi State.",
   "CELLPHONE": "8032030280",
   "CONTROLLER": "Mustapha Malami",
   "LATITUDE": "12.4593286",
   "LONGITUDE": "4.2087459"
 },
 {
   "NAME": "Zuru Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kebbi",
   "LGA": "Zuru",
   "ADDRESS": "Fada Area, Zuru",
   "CELLPHONE": "8032032808",
   "CONTROLLER": "Bello Ibrahim Bulala",
   "LATITUDE": "11.432822",
   "LONGITUDE": "5.234479"
 },
 {
   "NAME": "Ankpa Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kogi",
   "LGA": "Ankpa",
   "ADDRESS": "Oando Filling Station, Ejeh Road, Ankpa, Kogi State",
   "CELLPHONE": "8032030634",
   "CONTROLLER": "Sadiya Husseini",
   "LATITUDE": "7.6222",
   "LONGITUDE": "7.377822"
 },
 {
   "NAME": "Anyigba Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kogi",
   "LGA": "Dekina",
   "ADDRESS": "Oando Filling Station, Idah Road, Anyigba, Kogi State",
   "CELLPHONE": "8032030631",
   "CONTROLLER": "Comfort Ojoma Achimugu",
   "LATITUDE": "7.2854",
   "LONGITUDE": "7.105"
 },
 {
   "NAME": "Lokoja Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kogi",
   "LGA": "Lokoja",
   "ADDRESS": "Oando Filling Station, 64 Muritala MohD Way, Lokoja",
   "CELLPHONE": "8032038300",
   "CONTROLLER": "Emmanuel Salifu",
   "LATITUDE": "7.7942398",
   "LONGITUDE": "6.7312748"
 },
 {
   "NAME": "MM Lokoja Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kogi",
   "LGA": "Lokoja",
   "ADDRESS": "Yisab Oil & Gas Station, IBB Way, Lokoja, Kogi State.",
   "CELLPHONE": "8032030759",
   "CONTROLLER": "Toyin Bello",
   "LATITUDE": "7.795",
   "LONGITUDE": "6.737"
 },
 {
   "NAME": "Obajana Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kogi",
   "LGA": "Lokoja",
   "ADDRESS": "Opposite Factory Gate, Obajana, Kogi State",
   "CELLPHONE": "8032032734",
   "CONTROLLER": "Mercy Emmanuel",
   "LATITUDE": "6.433333",
   "LONGITUDE": "7.916667"
 },
 {
   "NAME": "Okene Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kogi",
   "LGA": "Okene",
   "ADDRESS": "Federal College Of Education Campus, By Main Gate, Okene, Kogi State",
   "CELLPHONE": "8032030629",
   "CONTROLLER": "Rekiya Suberu",
   "LATITUDE": "7.608",
   "LONGITUDE": "6.259"
 },
 {
   "NAME": "Okene Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kogi",
   "LGA": "Okene",
   "ADDRESS": "Al-Eemaan Plaza, 8 Atta Road, Opp. Okene Public Library, Auchi Benin Express Way, Okene Kogi State",
   "CELLPHONE": "8032037482",
   "CONTROLLER": "Evans Imhagbeghian",
   "LATITUDE": "7.550066",
   "LONGITUDE": "6.233309"
 },
 {
   "NAME": "Share Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kwara",
   "LGA": "Ifelodun",
   "ADDRESS": "Opposite Arolu Filling Station, Adanlawo Area, Share, Kwara State",
   "CELLPHONE": "8032032720",
   "CONTROLLER": "Sherrif Folorunsho",
   "LATITUDE": "8.267087",
   "LONGITUDE": "4.799813"
 },
 {
   "NAME": "Ilorin East Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kwara",
   "LGA": "Ilorin East",
   "ADDRESS": "39, Murtala Mohammed Way, Beside PHCN Office, Ilorin",
   "CELLPHONE": "8032034762",
   "CONTROLLER": "habibu Bello",
   "LATITUDE": "8.4812011",
   "LONGITUDE": "4.5629344"
 },
 {
   "NAME": "Ilorin Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kwara",
   "LGA": "Ilorin South",
   "ADDRESS": "24, Ahmadu Bello Way, G.R.A. Ilorin, Kwara State",
   "CELLPHONE": "8032034762",
   "CONTROLLER": "Habibu",
   "LATITUDE": "8.4815355",
   "LONGITUDE": "4.5754299"
 },
 {
   "NAME": "Kwara Mall Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kwara",
   "LGA": "Ilorin South",
   "ADDRESS": "Kwara Shopping Mall (Shoprite), Fate Road, Ilorin.",
   "CELLPHONE": "8032030942",
   "CONTROLLER": "OLUWATOYOSI",
   "LATITUDE": "8.499397",
   "LONGITUDE": "4.586028"
 },
 {
   "NAME": "Offa Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Kwara",
   "LGA": "Ilorin South",
   "ADDRESS": "149, Olofa Way, Old NEPA Office, Near Ibolo Bank, Offa, Kwara",
   "CELLPHONE": "8032035021",
   "CONTROLLER": "Oluseyi Bakare",
   "LATITUDE": "8.460972",
   "LONGITUDE": "4.520472"
 },
 {
   "NAME": "Unilorin Connect Ponit",
   "TYPE": "Connect Point",
   "STATE": "Kwara",
   "LGA": "Ilorin South",
   "ADDRESS": "Inside University Of Ilorin, Bank Area, Ilorin",
   "CELLPHONE": "8032037370",
   "CONTROLLER": "Esther Olarewaju",
   "LATITUDE": "8.479365",
   "LONGITUDE": "4.532406"
 },
 {
   "NAME": "College of Education ilorin",
   "TYPE": "Connect Point",
   "STATE": "Kwara",
   "LGA": "Ilorin West",
   "ADDRESS": "Sawmall Road Giri Alimi, Ilorin",
   "CELLPHONE": "8032032730",
   "CONTROLLER": "Bukola Ibitoye",
   "LATITUDE": "8.47906",
   "LONGITUDE": "4.54742"
 },
 {
   "NAME": "Oloje-Oko Ilorin Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kwara",
   "LGA": "Ilorin West",
   "ADDRESS": "Oloje-Oko Olowo Road, Ilorin",
   "CELLPHONE": "8032037650",
   "CONTROLLER": "Samuel Areh",
   "LATITUDE": "8.532346",
   "LONGITUDE": "4.488183"
 },
 {
   "NAME": "Kwara Polytechnic Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kwara",
   "LGA": "Moro",
   "ADDRESS": "Opp West-End Female Hostel, Kwara Poly. Ilorin",
   "CELLPHONE": "8032035012",
   "CONTROLLER": "Issa Mohammed",
   "LATITUDE": "8.553837",
   "LONGITUDE": "4.626601"
 },
 {
   "NAME": "Offa Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kwara",
   "LGA": "Offa",
   "ADDRESS": "108 Muritala Muhammed Way, Popo Area Off Oritamerin, Offa, Kwara State",
   "CELLPHONE": "8032037774",
   "CONTROLLER": "Idayat Jimoh",
   "LATITUDE": "8.155497",
   "LONGITUDE": "4.71133"
 },
 {
   "NAME": "Patigi Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Kwara",
   "LGA": "Patigi",
   "ADDRESS": "Opposite Emir Palace, Patigi, Kwara State",
   "CELLPHONE": "8032035011",
   "CONTROLLER": "Ndaman Mayaki",
   "LATITUDE": "9.38657",
   "LONGITUDE": "6.01749"
 },
 {
   "NAME": "Agege Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Agege",
   "ADDRESS": "119 Ipaja Road, Opp Union Bank, Agege, Lagos State",
   "CELLPHONE": "8032030184",
   "CONTROLLER": "Ada Manu",
   "LATITUDE": "6.6260867",
   "LONGITUDE": "3.3108549"
 },
 {
   "NAME": "Ajegunle Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Ajeromi-Ifelodun",
   "ADDRESS": "39, Bale Adeyemo Street, Boundary Ajegunle, Lagos State",
   "CELLPHONE": "8032030897",
   "CONTROLLER": "Temitope Ariyo",
   "LATITUDE": "6.455274",
   "LONGITUDE": "3.350331"
 },
 {
   "NAME": "Alimosho 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Alimosho",
   "ADDRESS": "8 Ejigbo Road, Orisunbare Street, Lagos State",
   "CELLPHONE": "8032030621",
   "CONTROLLER": "Emmanuel Falagbo",
   "LATITUDE": "6.572751",
   "LONGITUDE": "3.2845483"
 },
 {
   "NAME": "Alimosho Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Alimosho",
   "ADDRESS": "160 Akwonjo Road, Egbeda, Lagos State",
   "CELLPHONE": "8032038274",
   "CONTROLLER": "Itunu Ajayi",
   "LATITUDE": "6.6023613",
   "LONGITUDE": "3.3083135"
 },
 {
   "NAME": "Egbeda Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Alimosho",
   "ADDRESS": "4 Ijegun Road, Ikotun, Lagos State",
   "CELLPHONE": "8032034385",
   "CONTROLLER": "Adewale Aro",
   "LATITUDE": "6.5473256",
   "LONGITUDE": "3.267158"
 },
 {
   "NAME": "Ile Epo Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Alimosho",
   "ADDRESS": "Oando Filling Station, Ile-Epo Bus-Stop, Lagos - Abeokuta Epress Way, Lagos State",
   "CELLPHONE": "8032033901",
   "CONTROLLER": "Olushola",
   "LATITUDE": "6.635051",
   "LONGITUDE": "3.301815"
 },
 {
   "NAME": "Ipaja Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Alimosho",
   "ADDRESS": "Ipaja Bus Stop, Honeyland School, Alimosho, Lagos State",
   "CELLPHONE": "8032032781",
   "CONTROLLER": "Victoria Fagbemi",
   "LATITUDE": "6.63181",
   "LONGITUDE": "3.39193"
 },
 {
   "NAME": "Agboju Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Amuwo-Odofin",
   "ADDRESS": "206 Old Ojo Road Satellite Town, Lagos State",
   "CELLPHONE": "8032033859",
   "CONTROLLER": "Precious Nwibe",
   "LATITUDE": "6.456148",
   "LONGITUDE": "3.266441"
 },
 {
   "NAME": "Festac Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Amuwo-Odofin",
   "ADDRESS": "Community 1B, 72, By 23 Road, Festac Town, Lagos State",
   "CELLPHONE": "8032038021",
   "CONTROLLER": "Nurudeen Kuye",
   "LATITUDE": "6.471239",
   "LONGITUDE": "3.273847"
 },
 {
   "NAME": "Festac Police Station Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Amuwo-Odofin",
   "ADDRESS": "Festac Police Station, 2nd Avenue, Festac Town, Lagos State",
   "CELLPHONE": "8032032767",
   "CONTROLLER": "Ezekiel Nwaodu",
   "LATITUDE": "6.4622",
   "LONGITUDE": "3.294831"
 },
 {
   "NAME": "Apapa Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Apapa",
   "ADDRESS": "1 Commercial Road, Eleganza Plaza, Apapa Wharf, Apapa, Lagos State",
   "CELLPHONE": "8032030644",
   "CONTROLLER": "Benjamin Okonoboh",
   "LATITUDE": "6.4422872",
   "LONGITUDE": "3.3737428"
 },
 {
   "NAME": "Apapa Service Center",
   "TYPE": "Service Centre",
   "STATE": "Lagos",
   "LGA": "Apapa",
   "ADDRESS": "Maritime House, Oshodi-Apapa Expressway, Apapa, Lagos State",
   "CELLPHONE": "8032006674",
   "CONTROLLER": "Adebola Fagbule",
   "LATITUDE": "6.4449242",
   "LONGITUDE": "3.318611"
 },
 {
   "NAME": "Tincan Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Apapa",
   "ADDRESS": "Oando Filling Station, First Gate, Tincan, Lagos State",
   "CELLPHONE": "8032030041",
   "CONTROLLER": "Cynthia Obi",
   "LATITUDE": "6.43457",
   "LONGITUDE": "3.33897"
 },
 {
   "NAME": "Agbara Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Badagry",
   "ADDRESS": "Odofa Bus-Stop, Agbara, Lagos/Badagry Expressway, Lagos",
   "CELLPHONE": "8032030541",
   "CONTROLLER": "Olawale Owojori",
   "LATITUDE": "6.496284",
   "LONGITUDE": "3.08434"
 },
 {
   "NAME": "Badagry Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Badagry",
   "ADDRESS": "Shop 1, Adedeji Plaza Joseph Dosu Way, Badagry, Lagos State",
   "CELLPHONE": "8032030628",
   "CONTROLLER": "Samuel Hunkokoe",
   "LATITUDE": "6.420634",
   "LONGITUDE": "2.886964"
 },
 {
   "NAME": "Seme Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Badagry",
   "ADDRESS": "Oando Filling Station, Along Badagry Expressway, Seme Border, Lagos",
   "CELLPHONE": "8032030624",
   "CONTROLLER": "Paul",
   "LATITUDE": "6.431693",
   "LONGITUDE": "2.894915"
 },
 {
   "NAME": "Seme Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Badagry",
   "ADDRESS": "Oando Filling Station, Ashipa, Seme Board, Lagos State",
   "CELLPHONE": "8032030645",
   "CONTROLLER": "Paul Sapo",
   "LATITUDE": "6.431597",
   "LONGITUDE": "2.894894"
 },
 {
   "NAME": "Epe Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Epe",
   "ADDRESS": "36 Lagos Road, Epe, Lagos State",
   "CELLPHONE": "8032037645",
   "CONTROLLER": "Chukwudi Esionye",
   "LATITUDE": "6.58201",
   "LONGITUDE": "3.965802"
 },
 {
   "NAME": "Epe Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Epe",
   "ADDRESS": "Micheal Otedola College Of Education, Poka, Epe, Lagos State",
   "CELLPHONE": "8032038658",
   "CONTROLLER": "Chinwe Nwanze",
   "LATITUDE": "6.57091",
   "LONGITUDE": "3.5926"
 },
 {
   "NAME": "Ajah Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Eti-Osa",
   "ADDRESS": "Royal Cedar Plaza, 23 Leeki Epe Express Way, Opposite Fidelity Bank, Ajiwe Ajah, Lagos State",
   "CELLPHONE": "8032030263",
   "CONTROLLER": "Oluyemi Ogundare",
   "LATITUDE": "6.48931",
   "LONGITUDE": "3.57935"
 },
 {
   "NAME": "Aquino Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Eti-Osa",
   "ADDRESS": "1, Ajah Royal Complex, Berger Junction Ajah, Lagos State",
   "CELLPHONE": "8032032779",
   "CONTROLLER": "Aduaka Jacinta",
   "LATITUDE": "6.466792",
   "LONGITUDE": "3.562088"
 },
 {
   "NAME": "Aquino Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Eti-Osa",
   "ADDRESS": "Berger Junction, Beside Ikota Shoping Complex, Ajah, Lagos State",
   "CELLPHONE": "8032032779",
   "CONTROLLER": "Aduaka Jecinta Uchenna",
   "LATITUDE": "6.121",
   "LONGITUDE": "4.63811"
 },
 {
   "NAME": "Ikota Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Eti-Osa",
   "ADDRESS": "Shop D216/D223 Ikota Shopping Complex, VGC, Lagos State",
   "CELLPHONE": "8032034387",
   "CONTROLLER": "Fadipe Kehinde",
   "LATITUDE": "6.464276",
   "LONGITUDE": "3.55548"
 },
 {
   "NAME": "Ikoyi Service Center",
   "TYPE": "Service Centre",
   "STATE": "Lagos",
   "LGA": "Eti-Osa",
   "ADDRESS": "4 Aromire Road, Off Kingsway Road, Opposite Ikoyi Hotel, Lagos State",
   "CELLPHONE": "8032008992",
   "CONTROLLER": "Gbemisola Omotoyosi-Ogunlende",
   "LATITUDE": "6.453055",
   "LONGITUDE": "3.432888"
 },
 {
   "NAME": "Lekki Connect Point ",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Eti-Osa",
   "ADDRESS": "21 Adewunmi Adebimpe Street, Marwa Bus Stop, Lekki Phase 1, Lagos",
   "CELLPHONE": "8032032692",
   "CONTROLLER": "Maryam Abdulrahim",
   "LATITUDE": "6.435543",
   "LONGITUDE": "3.468959"
 },
 {
   "NAME": "Lewis Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Eti-Osa",
   "ADDRESS": "Oando Filling Station, Lewis Street, Lagos State",
   "CELLPHONE": "8032030733",
   "CONTROLLER": "Omotoyosi Odegbesan",
   "LATITUDE": "6.4515",
   "LONGITUDE": "3.40443"
 },
 {
   "NAME": "Lush Mall Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Eti-Osa",
   "ADDRESS": "25 Admiralty way, lekki phase 1 (Opposite Domino pizza), Lagos",
   "CELLPHONE": "8032032021",
   "CONTROLLER": "David Afolabi",
   "LATITUDE": "6.4481",
   "LONGITUDE": "3.4731"
 },
 {
   "NAME": "Mega Plaza Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Eti-Osa",
   "ADDRESS": "14 Idowu Martins Street, Victoria Island, Lagos",
   "CELLPHONE": "8032032021",
   "CONTROLLER": "David Afolabi",
   "LATITUDE": "6.4333406",
   "LONGITUDE": "3.4203805"
 },
 {
   "NAME": "Moloney Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Eti-Osa",
   "ADDRESS": "32, Moloney Street(beside GTBank)Obalende, Lagos",
   "CELLPHONE": "8032030022",
   "CONTROLLER": "Oluwakemi Olojede",
   "LATITUDE": "6.4312663",
   "LONGITUDE": "3.4271222"
 },
 {
   "NAME": "Palms Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Eti-Osa",
   "ADDRESS": "Unit 6 Entrance 2, The Palms Shopping Mall, Lekki, Lagos",
   "CELLPHONE": "8032032016",
   "CONTROLLER": "Folashade Akosile",
   "LATITUDE": "6.435436",
   "LONGITUDE": "3.451346"
 },
 {
   "NAME": "Silverbird Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Eti-Osa",
   "ADDRESS": "Silver Bird Galleria , 133 Ahmadu Bello Way , Victoria Island, Lagos",
   "CELLPHONE": "8032032303",
   "CONTROLLER": "Gold Asaka",
   "LATITUDE": "6.4276713",
   "LONGITUDE": "3.4087873"
 },
 {
   "NAME": "Y'ello City Service Center",
   "TYPE": "Service Centre",
   "STATE": "Lagos",
   "LGA": "Eti-Osa",
   "ADDRESS": "1267 Adeola Odeku, Victoria Island, Lagos",
   "CELLPHONE": "8032006595",
   "CONTROLLER": "Victoria Ogbeun",
   "LATITUDE": "6.4287828",
   "LONGITUDE": "3.4228745"
 },
 {
   "NAME": "Awoyaya Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Ibeju-Lekki",
   "ADDRESS": "Fresher Restaurant And Bar, Beside Forte Oil Petro Station, Awoyaya Bus Stop, Lekki Epe Express Way, Lagos State",
   "CELLPHONE": "8032032680",
   "CONTROLLER": "Helen Umo",
   "LATITUDE": "6.471015",
   "LONGITUDE": "3.710663"
 },
 {
   "NAME": "Abule-Egba Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Ifako Ijaiye",
   "ADDRESS": "513, Bada House, Lagos-Abeokuta Expressway, Lagos",
   "CELLPHONE": "8032037560",
   "CONTROLLER": "Omofolarin Ashiru",
   "LATITUDE": "6.661021",
   "LONGITUDE": "3.291413"
 },
 {
   "NAME": "Iju Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Ifako Ijaiye",
   "ADDRESS": "21, Agbado Road, Iju-Ishaga, Lagos State",
   "CELLPHONE": "8032035027",
   "CONTROLLER": "Mayowa Odetola",
   "LATITUDE": "6.523432",
   "LONGITUDE": "3.32234"
 },
 {
   "NAME": "Ogba Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Ifako Ijaiye",
   "ADDRESS": "57 Yaya Abatan Street, Ogba, Lagos",
   "CELLPHONE": "8032030079",
   "CONTROLLER": "Adaobi Anekwe",
   "LATITUDE": "6.6357499",
   "LONGITUDE": "3.3378066"
 },
 {
   "NAME": "Alausa Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "Secretariat Road, Alausa, Ikeja, Lagos State",
   "CELLPHONE": "8032030901",
   "CONTROLLER": "Olutayo Adetona?",
   "LATITUDE": "6.616929",
   "LONGITUDE": "3.364592"
 },
 {
   "NAME": "Allen Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "75, Allen Avenue, Ikeja, Lagos State",
   "CELLPHONE": "8032035046",
   "CONTROLLER": "Adaobi Madueke",
   "LATITUDE": "6.603134",
   "LONGITUDE": "3.350955"
 },
 {
   "NAME": "Computer Village Connect Store",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "11, Ola Ayeni Street, Computer Village, Ikeja, Lagos State",
   "CELLPHONE": "8032033876",
   "CONTROLLER": "Uche Ordu",
   "LATITUDE": "6.595399",
   "LONGITUDE": "3.339531"
 },
 {
   "NAME": "FAAN Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "Shop 34, Obi Village, FAAN Complex, Ikeja, Lagos State",
   "CELLPHONE": "8032033966",
   "CONTROLLER": "Rachel Onoja",
   "LATITUDE": "6.58702",
   "LONGITUDE": "3.333924"
 },
 {
   "NAME": "Ikeja City Mall Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "176/194, Obafemi Awolowo Way, Opp Screteriat Alaus (Ikeja City Mall Shopping Complex), Ikeja, Lagos State",
   "CELLPHONE": "8032034635",
   "CONTROLLER": "Stella Echinim",
   "LATITUDE": "6.6141193",
   "LONGITUDE": "3.3556502"
 },
 {
   "NAME": "Ikeja Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "Block A Ground Floor Alausa Shopping Mall, 131 Awolowo Way, Ikeja, Lagos State",
   "CELLPHONE": "8032030059",
   "CONTROLLER": "Ibiwunmi Oyewole",
   "LATITUDE": "6.530602",
   "LONGITUDE": "3.32575"
 },
 {
   "NAME": "Ikeja Service Center",
   "TYPE": "Service Centre",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "1 Opebi Road, Ikeja, Lagos State",
   "CELLPHONE": "8032005663",
   "CONTROLLER": "Victoria Okozide",
   "LATITUDE": "6.596082",
   "LONGITUDE": "3.355247"
 },
 {
   "NAME": "Maryland Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "1 Bank Anthony Way, Maryland, Lagos",
   "CELLPHONE": "8032034411",
   "CONTROLLER": "Blessing Okolugbo",
   "LATITUDE": "6.572965",
   "LONGITUDE": "3.366439"
 },
 {
   "NAME": "MMA 2 Service Center Lite ",
   "TYPE": "Service Centre",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "Muritala Mohammed Airport 2 Departure Lounge, Ikeja, Lagos",
   "CELLPHONE": "8032005161",
   "CONTROLLER": "Weyinmi Awuzie",
   "LATITUDE": "6.5847425",
   "LONGITUDE": "3.3327627"
 },
 {
   "NAME": "MMA Food Court",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "MMA 2 Food Court Kiosk, Ikeja, Lagos",
   "CELLPHONE": "8032037746",
   "CONTROLLER": "Olutayo Faseku",
   "LATITUDE": "6.585787",
   "LONGITUDE": "3.333181"
 },
 {
   "NAME": "MMA1 (GAT Point)",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "General Aviation Terminal, (Arik Air) Ikeja, Lagos",
   "CELLPHONE": "8032033962",
   "CONTROLLER": "Doris Domic-Abah",
   "LATITUDE": "6.61214",
   "LONGITUDE": "3.33713"
 },
 {
   "NAME": "MMA1(GAT Point)",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "Arik Terminal Muritala Muhammed Local Airport ",
   "CELLPHONE": "8032033962",
   "CONTROLLER": "Doris Domic-Abah",
   "LATITUDE": "6.589826",
   "LONGITUDE": "3.331389"
 },
 {
   "NAME": "MMIA Arrival Connect Store (International Airport)",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "Murtala Muhammed International Airport Arrival Lounge, Ikeja, Lagos",
   "CELLPHONE": "8032030774",
   "CONTROLLER": "Ezeh Ifeyinwa",
   "LATITUDE": "6.5818185",
   "LONGITUDE": "3.3211348"
 },
 {
   "NAME": "Ojodu Berger Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Ikeja",
   "ADDRESS": "16 Ogunnusi Road, Ojodu Berger, Lagos State",
   "CELLPHONE": "8032030032",
   "CONTROLLER": "Yamah Festus",
   "LATITUDE": "6.64021",
   "LONGITUDE": "3.36616"
 },
 {
   "NAME": "Agric Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Ikorodu",
   "ADDRESS": "Mobile Filling Station, Oriokuta Junction, Isawo Road, Ikorodu, Lagos State",
   "CELLPHONE": "8032032036",
   "CONTROLLER": "Ketuoja Agbkena",
   "LATITUDE": "6.651751",
   "LONGITUDE": "3.475781"
 },
 {
   "NAME": "Igbogbo Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Ikorodu",
   "ADDRESS": "Rabdempsy Filling Station, Igbogbo, Ikorodu, Lagos State",
   "CELLPHONE": "8032030626",
   "CONTROLLER": "Ayodeji Apejua",
   "LATITUDE": "6.512024",
   "LONGITUDE": "3.358808"
 },
 {
   "NAME": "Ikorodu Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Ikorodu",
   "ADDRESS": "84 Lagos Road, Haruna Bus-Stop, Ikorodu, Lagos State",
   "CELLPHONE": "8032032031",
   "CONTROLLER": "Olisa-Emeka Onoh",
   "LATITUDE": "6.6245701",
   "LONGITUDE": "3.4931695"
 },
 {
   "NAME": "Odogunyan Connect Point ",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Ikorodu",
   "ADDRESS": "Beemack Petroleum, Odogunyan Bustop, Ogijo-Sagamu Road, Ikorodu, Lagos",
   "CELLPHONE": "8032030444",
   "CONTROLLER": "Kehinde Adesanya",
   "LATITUDE": "6.703772",
   "LONGITUDE": "3.510689"
 },
 {
   "NAME": "Gbagada Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Kosofe",
   "ADDRESS": "9, William Street, Gbagada, Lagos State",
   "CELLPHONE": "8032033902",
   "CONTROLLER": "Adenrele Adewale",
   "LATITUDE": "6.553625",
   "LONGITUDE": "3.392741"
 },
 {
   "NAME": "Gbagada Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Kosofe",
   "ADDRESS": "32, Diya Street, Gbagada, Lagos State",
   "CELLPHONE": "8032035182",
   "CONTROLLER": "Abimbola Eleja",
   "LATITUDE": "6.553260",
   "LONGITUDE": "3.390990"
 },
 {
   "NAME": "Ketu Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Kosofe",
   "ADDRESS": "4 Owode Oyeleaja Drive, Opposite Sweet Sensation, Tiper Garage, Ketu, Lagos State",
   "CELLPHONE": "8032037761",
   "CONTROLLER": "Olateju Fasoranti",
   "LATITUDE": "6.5972069",
   "LONGITUDE": "3.3872672"
 },
 {
   "NAME": "Marina Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Lagos Island",
   "ADDRESS": "45/47 Marina Street, Lagos Island, Lagos",
   "CELLPHONE": "8032038066",
   "CONTROLLER": "Nwosu Charity",
   "LATITUDE": "6.4535451",
   "LONGITUDE": "3.3860079"
 },
 {
   "NAME": "Obalenda Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Lagos Island",
   "ADDRESS": "15 Okesuna street, Obalende, Lagos",
   "CELLPHONE": "8032030734",
   "CONTROLLER": "Glory Igiwa",
   "LATITUDE": "6.449962",
   "LONGITUDE": "3.403761"
 },
 {
   "NAME": "Apapa Rd Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Lagos Mainland",
   "ADDRESS": "102, Apapa Rd, Ebutte Metta, Lagos State",
   "CELLPHONE": "8032030627",
   "CONTROLLER": "Akintunde Ajayi",
   "LATITUDE": "6.482255",
   "LONGITUDE": "3.372429"
 },
 {
   "NAME": "E-Centre Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Lagos Mainland",
   "ADDRESS": "E-Centre, 1-11 Commercail Avenue, Sabo, Yaba, Lagos State",
   "CELLPHONE": "8032032003",
   "CONTROLLER": "Nkechi Ekwousa",
   "LATITUDE": "6.5062266",
   "LONGITUDE": "3.3761819"
 },
 {
   "NAME": "Oyingbo Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Lagos Mainland",
   "ADDRESS": "80 Muritala Mohammed Way, Oyingbo, Lagos",
   "CELLPHONE": "8032038063",
   "CONTROLLER": "Prisca Amadikwa",
   "LATITUDE": "6.482928",
   "LONGITUDE": "3.382886"
 },
 {
   "NAME": "Yabatech Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Lagos Mainland",
   "ADDRESS": "Yabatech, Lagos",
   "CELLPHONE": "8032033853",
   "CONTROLLER": "Danladi Shuiab",
   "LATITUDE": "3.371238",
   "LONGITUDE": "6.517866"
 },
 {
   "NAME": "Mushin Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Mushin",
   "ADDRESS": "Oando Filling Station, Mushin Bustop, Mushin, Lagos State",
   "CELLPHONE": "8032030540",
   "CONTROLLER": "Oketu John",
   "LATITUDE": "6.531",
   "LONGITUDE": "3.354"
 },
 {
   "NAME": "Alaba Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Ojo",
   "ADDRESS": "30, Ojo Alaba International, Alaba, Lagos State",
   "CELLPHONE": "8032034408",
   "CONTROLLER": "Emmanuel Charles",
   "LATITUDE": "6.459982",
   "LONGITUDE": "3.191409"
 },
 {
   "NAME": "LASU Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Ojo",
   "ADDRESS": "Lagos State Univeristy (Lasu) Ojo, Badagry Express Way, Lagos",
   "CELLPHONE": "8032038016",
   "CONTROLLER": "Mojisola Sanusi",
   "LATITUDE": "6.484957",
   "LONGITUDE": "3.199317"
 },
 {
   "NAME": "Tradefair Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Ojo",
   "ADDRESS": "Gl Line,Opposite Fidelity Bank Progressive Mkt., Tradefair , Lagos",
   "CELLPHONE": "8032030645",
   "CONTROLLER": "Benjamin Maduekwe",
   "LATITUDE": "6.466764",
   "LONGITUDE": "3.254008"
 },
 {
   "NAME": "Ajao Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Oshodi-Isolo",
   "ADDRESS": "52, International Airport Road, Junction Bus Stop, Mafoluku, Lagos State",
   "CELLPHONE": "8032031627",
   "CONTROLLER": "Fagbohun Oluwole",
   "LATITUDE": "6.5486238",
   "LONGITUDE": "3.3367693"
 },
 {
   "NAME": "Bolade Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Oshodi-Isolo",
   "ADDRESS": "Abia Line Park, 89 Igbehinadun Street, Bolade, Oshodi, Lagos State",
   "CELLPHONE": "8032032777/8032032775",
   "CONTROLLER": "Ramond Jomata/ Favour Omiachi",
   "LATITUDE": "6.56045",
   "LONGITUDE": "3.34972"
 },
 {
   "NAME": "Isolo Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Oshodi-Isolo",
   "ADDRESS": "73 Mushin Road Isolo, Lagos State",
   "CELLPHONE": "8032033859",
   "CONTROLLER": "Precious",
   "LATITUDE": "6.528366",
   "LONGITUDE": "3.324768"
 },
 {
   "NAME": "Isolo Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Oshodi-Isolo",
   "ADDRESS": "28 Ire Akari Estate Road, Isolo, Lagos State",
   "CELLPHONE": "8032037484",
   "CONTROLLER": "Paul Onyekwelu",
   "LATITUDE": "6.525799",
   "LONGITUDE": "3.3222244"
 },
 {
   "NAME": "Matori Service Center",
   "TYPE": "Service Centre",
   "STATE": "Lagos",
   "LGA": "Oshodi-Isolo",
   "ADDRESS": "16 Fatai Atere Way, Matori, Oshodi, Lagos",
   "CELLPHONE": "8032008454",
   "CONTROLLER": "Abiodun Otuedon",
   "LATITUDE": "6.5432589",
   "LONGITUDE": "3.3491205"
 },
 {
   "NAME": "Okota Connect Store ",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Oshodi-Isolo",
   "ADDRESS": "Grandmate Bustop, Ago Palace Way, Isolo, Lagos",
   "CELLPHONE": "8032037453",
   "CONTROLLER": "Agaezichi Okere",
   "LATITUDE": "6.504285",
   "LONGITUDE": "3.308082"
 },
 {
   "NAME": "Osolo Way Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Oshodi-Isolo",
   "ADDRESS": "Komo Filling Station, Osolo, Lagos State",
   "CELLPHONE": "8032032776",
   "CONTROLLER": "Nkoli Aguahamba",
   "LATITUDE": "6.550614",
   "LONGITUDE": "3.331713"
 },
 {
   "NAME": "Fola Agoro Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Shomolu",
   "ADDRESS": "31 Fola Agoro, Oando Filling Station, Lagos State",
   "CELLPHONE": "8032030184",
   "CONTROLLER": "Ada Manu",
   "LATITUDE": "6.527231",
   "LONGITUDE": "3.380798"
 },
 {
   "NAME": "Shomolu Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Shomolu",
   "ADDRESS": "58, St. Finbars Road, Opposite Chemist B/Stop, Akoka, Lagos",
   "CELLPHONE": "8032038306",
   "CONTROLLER": "Charles Akeredolu",
   "LATITUDE": "6.525569",
   "LONGITUDE": "3.38612"
 },
 {
   "NAME": "Unilag Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Shomolu",
   "ADDRESS": "Faculty of Social Sciences, off Comercial Avenue, University of Lagos (UNILAG)",
   "CELLPHONE": "8032038281",
   "CONTROLLER": "Ann Dike",
   "LATITUDE": "6.5123",
   "LONGITUDE": "3.3931"
 },
 {
   "NAME": "Itire Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Surulere",
   "ADDRESS": "68, Itire Road, Ishaga, Lagos State",
   "CELLPHONE": "8032034386",
   "CONTROLLER": "Oluwabunmi Adeyemo",
   "LATITUDE": "6.512916",
   "LONGITUDE": "3.354728"
 },
 {
   "NAME": "Lawanson Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Surulere",
   "ADDRESS": "Oando Filling Station, Lawanson, Lagos State",
   "CELLPHONE": "8032038032",
   "CONTROLLER": "Simon Ogiri",
   "LATITUDE": "6.512544",
   "LONGITUDE": "3.348417"
 },
 {
   "NAME": "Shoprite Surulere Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Surulere",
   "ADDRESS": "Adeniran Ogunsanya Mall, Adeniran Ogunsanya Street, Surulere",
   "CELLPHONE": "8032032793",
   "CONTROLLER": "Moses Iduwe/Chinonso Onyia",
   "LATITUDE": "6.492",
   "LONGITUDE": "3.358"
 },
 {
   "NAME": "Surulere 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Surulere",
   "ADDRESS": "77, Itire Road, Surulere, Lagos",
   "CELLPHONE": "8032032688",
   "CONTROLLER": "Yetunde Gambo",
   "LATITUDE": "6.506108",
   "LONGITUDE": "3.331293"
 },
 {
   "NAME": "Surulere Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Lagos",
   "LGA": "Surulere",
   "ADDRESS": "81 Bode Thomas, Surulere, Lagos",
   "CELLPHONE": "8032030981",
   "CONTROLLER": "Ndubisi Anyawu",
   "LATITUDE": "6.4897021",
   "LONGITUDE": "3.3567085"
 },
 {
   "NAME": "LUTH Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Lagos",
   "LGA": "Yaba",
   "ADDRESS": "Lagos University Teaching Hospital (LUTH), Lagos State",
   "CELLPHONE": "8032032694",
   "CONTROLLER": "Igbinedion Michael",
   "LATITUDE": "6.531099",
   "LONGITUDE": "3.35278"
 },
 {
   "NAME": "Akwanga Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Nasarawa",
   "LGA": "Akwanga",
   "ADDRESS": "Cliff Pharmacy Keffi Road, Akwanga, Nasarawa State",
   "CELLPHONE": "8032030326",
   "CONTROLLER": "Peter",
   "LATITUDE": "8.91066",
   "LONGITUDE": "8.402085"
 },
 {
   "NAME": "Mararaba Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Nasarawa",
   "LGA": "Karu",
   "ADDRESS": "2, Baba Street, Beside Amana Super Market, Opp Abacha Road, Mararaba, Nasarawa",
   "CELLPHONE": "8032034679",
   "CONTROLLER": "Peace Emmanuel",
   "LATITUDE": "9.032188",
   "LONGITUDE": "7.581442"
 },
 {
   "NAME": "Keffi Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Nasarawa",
   "LGA": "Keffi",
   "ADDRESS": "Oando Filling Station, Akwanga Road, Keffi, Nasarawa",
   "CELLPHONE": "8032030748",
   "CONTROLLER": "Michean Ayan",
   "LATITUDE": "8.839708",
   "LONGITUDE": "7.876321"
 },
 {
   "NAME": "Lafia Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Nasarawa",
   "LGA": "Lafia",
   "ADDRESS": "Plot 65/68 Jos Road, Lafia, Nasarawa State",
   "CELLPHONE": "8032030326",
   "CONTROLLER": "Onifade",
   "LATITUDE": "8.514968",
   "LONGITUDE": "8.521267"
 },
 {
   "NAME": "Markurdi Road Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Nasarawa",
   "LGA": "Lafia",
   "ADDRESS": "Oando Filling Station, KM 4 Makurdi Road, Lafia, Nasarawa State",
   "CELLPHONE": "8032030726",
   "CONTROLLER": "Benjamin Kigbu",
   "LATITUDE": "8.482307",
   "LONGITUDE": "8.540246"
 },
 {
   "NAME": "Bida(Niger) Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Niger",
   "LGA": "Bida",
   "ADDRESS": "Western Bye Pass (Oando), Bida Esso, Lagos Road, Bida, Niger State",
   "CELLPHONE": "8032030796",
   "CONTROLLER": "abubakar umar and hafsat Haliru",
   "LATITUDE": "9.095021",
   "LONGITUDE": "6.010708"
 },
 {
   "NAME": "Unilite Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Niger",
   "LGA": "Bosso",
   "ADDRESS": "Federal University of Technology Minna, Gidan Kwano Campus, Niger State",
   "CELLPHONE": "8032030799",
   "CONTROLLER": "Alaba Alade",
   "LATITUDE": "9.601749",
   "LONGITUDE": "6.558557"
 },
 {
   "NAME": "Minna Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Niger",
   "LGA": "Chanchaga",
   "ADDRESS": "Godana House, 12 Paiko Road, Minna, Niger State",
   "CELLPHONE": "8032031921",
   "CONTROLLER": "abubakar isah",
   "LATITUDE": "9.63368",
   "LONGITUDE": "6.5424801"
 },
 {
   "NAME": "Minna(Niger) Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Niger",
   "LGA": "Chanchaga",
   "ADDRESS": "Minna Entrance, Opp. State Secretariate Western ByePass, Niger State",
   "CELLPHONE": "8032032051",
   "CONTROLLER": "Usman Marafa",
   "LATITUDE": "9.596602",
   "LONGITUDE": "6.526917"
 },
 {
   "NAME": "Kontagora Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Niger",
   "LGA": "Kontagora",
   "ADDRESS": "Haziz Boutique, Lagos Kano Road, Kontagora, Niger State",
   "CELLPHONE": "8032037387",
   "CONTROLLER": "Idris Lemu",
   "LATITUDE": "10.406448",
   "LONGITUDE": "5.470451"
 },
 {
   "NAME": "Kontagora(Niger) Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Niger",
   "LGA": "Kontagora",
   "ADDRESS": "Oando Filling Station, Kontagora, Minna, Niger State",
   "CELLPHONE": "8032031921",
   "CONTROLLER": "Abubakar",
   "LATITUDE": "9.596603",
   "LONGITUDE": "6.526916"
 },
 {
   "NAME": "Suleja Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Niger",
   "LGA": "Suleja",
   "ADDRESS": "Plot 73, Suleiman Barau Road, Suleja, Niger State",
   "CELLPHONE": "8032034670",
   "CONTROLLER": "Abdulakeem Olatubosun",
   "LATITUDE": "9.1929915",
   "LONGITUDE": "7.1799051"
 },
 {
   "NAME": "Zungeru Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Niger",
   "LGA": "Zungeru",
   "ADDRESS": "Oando Filling Station,Angwan Katako, Zungeru - Wushishi Road, Zungeru, Niger State",
   "CELLPHONE": "8032037481",
   "CONTROLLER": "Yusuf Zungeru",
   "LATITUDE": "9.802706",
   "LONGITUDE": "6.152129"
 },
 {
   "NAME": "Abeokuta Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Ogun",
   "LGA": "Abeokuta North",
   "ADDRESS": "3 Lalubu Street, Okelewo, Abeokuta, Ogun State",
   "CELLPHONE": "8032038650",
   "CONTROLLER": "Martins Godwin",
   "LATITUDE": "7.1356614",
   "LONGITUDE": "3.3362288"
 },
 {
   "NAME": "Lafenwa Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Ogun",
   "LGA": "Abeokuta North",
   "ADDRESS": "Oando Filling Station ( Lafenwa), Ogun State",
   "CELLPHONE": "8032032081",
   "CONTROLLER": "Ambrose Egbon",
   "LATITUDE": "7.157281",
   "LONGITUDE": "3.326071"
 },
 {
   "NAME": "Kuto Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Ogun",
   "LGA": "Abeokuta South",
   "ADDRESS": "Oando Filling Station (KUTO Roundabout), Ogun State",
   "CELLPHONE": "8032030586",
   "CONTROLLER": "Abiodun Shokunbi",
   "LATITUDE": "7.135874",
   "LONGITUDE": "3.350793"
 },
 {
   "NAME": "Sango Toll Gate connect Point",
   "TYPE": "Connect Point",
   "STATE": "Ogun",
   "LGA": "Ado-Odo/Otta",
   "ADDRESS": "Oando Filling Station, Lagos Abeokuta Express Way, Sango, Ogun State",
   "CELLPHONE": "8032035162",
   "CONTROLLER": "Joy Oseghe",
   "LATITUDE": "6.691075",
   "LONGITUDE": "3.263952"
 },
 {
   "NAME": "Sango Otta Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Ogun",
   "LGA": "Ado-Odo/Otta",
   "ADDRESS": "56 Idiroko Road, Sango, Ogun State",
   "CELLPHONE": "8032030535",
   "CONTROLLER": "Dominic Odoemelem",
   "LATITUDE": "6.6826506",
   "LONGITUDE": "3.1721082"
 },
 {
   "NAME": "Ajuwon Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Ogun",
   "LGA": "Ifo",
   "ADDRESS": "1, Nationwide Filling Station, Akute Ajuwon, Ogun State",
   "CELLPHONE": "8032038958",
   "CONTROLLER": "Christopher Borode",
   "LATITUDE": "6.681369",
   "LONGITUDE": "3.358851"
 },
 {
   "NAME": "Akute Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Ogun",
   "LGA": "Ifo",
   "ADDRESS": "Opposite Peak Lane Nursery & Primary School, Alagbole, Akute, Ogun State",
   "CELLPHONE": "8032030125",
   "CONTROLLER": "Babatunde Aliu",
   "LATITUDE": "3.35643",
   "LONGITUDE": "6.6667"
 },
 {
   "NAME": "Ifo Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Ogun",
   "LGA": "Ifo",
   "ADDRESS": "20 Surulere Qtrs Iyana Coker, Ifo, Ogun State",
   "CELLPHONE": "8032035168",
   "CONTROLLER": "Olaoluwa Ogundele",
   "LATITUDE": "6.750337",
   "LONGITUDE": "3.22997"
 },
 {
   "NAME": "Ijebu Ode Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Ogun",
   "LGA": "Ijebu Ode",
   "ADDRESS": "Oando Staion, Lagos Garage RoundAbout, Ijebu Ode, Ogun State",
   "CELLPHONE": "8032037662",
   "CONTROLLER": "Adebimpe Olurombi",
   "LATITUDE": "6.810837",
   "LONGITUDE": "3.910506"
 },
 {
   "NAME": "Ijebu Ode Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Ogun",
   "LGA": "Ijebu Ode",
   "ADDRESS": "146 Folagbade Road, Opp Mr Bigg'S Ijebu Ode, Ogun State",
   "CELLPHONE": "8032032033",
   "CONTROLLER": "Nosakhare Igbinobaro",
   "LATITUDE": "6.8411142",
   "LONGITUDE": "3.9236742"
 },
 {
   "NAME": "Redeemed Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Ogun",
   "LGA": "Obafemi Owode",
   "ADDRESS": "KM 46/47 Redemption Way, Mowe, Ogun State",
   "CELLPHONE": "8032037666",
   "CONTROLLER": "Abiodun Kayode",
   "LATITUDE": "6.8055",
   "LONGITUDE": "3.438042"
 },
 {
   "NAME": "Makun Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Ogun",
   "LGA": "Sagamu",
   "ADDRESS": "Mobile Service Station, Makun, Sagamu, Ogun State",
   "CELLPHONE": "8032037750",
   "CONTROLLER": "Folashade Adeyeye",
   "LATITUDE": "6.84966",
   "LONGITUDE": "3.64819"
 },
 {
   "NAME": "Sagamu Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Ogun",
   "LGA": "Sagamu",
   "ADDRESS": "10A Akarigbo Road, Isale Oko, Sagamu, Ogun State",
   "CELLPHONE": "8032030440",
   "CONTROLLER": "Oluwatosin Iroko",
   "LATITUDE": "6.842651",
   "LONGITUDE": "3.644587"
 },
 {
   "NAME": "Unilite Akungba ",
   "TYPE": "Connect Point",
   "STATE": "Ondo",
   "LGA": "Akoko South West",
   "ADDRESS": "Beside Access Bank, Akungba, Ondo State",
   "CELLPHONE": "8032034549",
   "CONTROLLER": "Oluwabukola Olofinyokun",
   "LATITUDE": "7.479571",
   "LONGITUDE": "5.742885"
 },
 {
   "NAME": "Adeyemi Unilite",
   "TYPE": "Connect Point",
   "STATE": "Ondo",
   "LGA": "Akure South",
   "ADDRESS": "Main Gate Entrance, Adeyemi Adeyemi College of Education, Ondo State",
   "CELLPHONE": "8032034545",
   "CONTROLLER": "Eric Imoru",
   "LATITUDE": "7.076749",
   "LONGITUDE": "4.823319"
 },
 {
   "NAME": "Akure Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Ondo",
   "LGA": "Akure South",
   "ADDRESS": "Ricabim House, 47 Oba Adesida Road, Ondo State",
   "CELLPHONE": "8032030929",
   "CONTROLLER": "ABAYOMI RHODA",
   "LATITUDE": "7.2522496",
   "LONGITUDE": "5.1999675"
 },
 {
   "NAME": "FUTA Unilite",
   "TYPE": "Connect Point",
   "STATE": "Ondo",
   "LGA": "Akure South",
   "ADDRESS": "Infront of Agric. Department, FUTA, Akure, Ondo State",
   "CELLPHONE": "8032038319",
   "CONTROLLER": "Folasade Ajiboye",
   "LATITUDE": "7.303033",
   "LONGITUDE": "5.135072"
 },
 {
   "NAME": "Ondo Road Connect Point ",
   "TYPE": "Connect Point",
   "STATE": "Ondo",
   "LGA": "Akure south",
   "ADDRESS": "Oando Filing Station, Ondo Road, Akure, Ondo State",
   "CELLPHONE": "8032037682",
   "CONTROLLER": "Oluwatoyin Idowu",
   "LATITUDE": "7.062588",
   "LONGITUDE": "5.000713"
 },
 {
   "NAME": "Ore Connect Point 2",
   "TYPE": "Connect Point",
   "STATE": "Ondo",
   "LGA": "Odigbo",
   "ADDRESS": "Jay City Filling Station, Ondo Junction, Ore, Ondo State",
   "CELLPHONE": "8032033803",
   "CONTROLLER": "Olabode Korodele",
   "LATITUDE": "6.7621",
   "LONGITUDE": "4.87606"
 },
 {
   "NAME": "Ondo Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Ondo",
   "LGA": "Ondo West",
   "ADDRESS": "35 Mode Street, Yaba road, Ondo Town, Ondo State",
   "CELLPHONE": "8032038019",
   "CONTROLLER": "Olusayo Sunday",
   "LATITUDE": "7.103098",
   "LONGITUDE": "4.84706"
 },
 {
   "NAME": "Owo Unilite",
   "TYPE": "Connect Point",
   "STATE": "Ondo",
   "LGA": "Owo",
   "ADDRESS": "Bank Area Rufus Giwa Poly, Owo, Ondo State",
   "CELLPHONE": "8032037681",
   "CONTROLLER": "Akinseye Feyikemi",
   "LATITUDE": "7.227971",
   "LONGITUDE": "5.559235"
 },
 {
   "NAME": "Ede Unilite",
   "TYPE": "Connect Point",
   "STATE": "Osun",
   "LGA": "Ede South",
   "ADDRESS": "Federal Polytechnic, Osun State",
   "CELLPHONE": "8032030935",
   "CONTROLLER": "Oluwatosin Adedeji",
   "LATITUDE": "4.424232",
   "LONGITUDE": "7.732327"
 },
 {
   "NAME": "Osogbo Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Osun",
   "LGA": "Egbedore",
   "ADDRESS": "Plot 456 Dada Estate Iwo Road, By Fan Milk Oshogbo, Osun State",
   "CELLPHONE": "8032032085",
   "CONTROLLER": "Jonathan Amoo",
   "LATITUDE": "7.7938196",
   "LONGITUDE": "4.5173687"
 },
 {
   "NAME": "Ile-Ife Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Osun",
   "LGA": "Ife Central",
   "ADDRESS": "13 Adesoji Aderemi Road, Lagere,Ile Ife, Osun State",
   "CELLPHONE": "8032030322",
   "CONTROLLER": "MARYAM UMORU",
   "LATITUDE": "7.466667",
   "LONGITUDE": "4.566667"
 },
 {
   "NAME": "OAU Unilite",
   "TYPE": "Connect Point",
   "STATE": "Osun",
   "LGA": "Ife Central",
   "ADDRESS": "OAU  Central Market, Osun State",
   "CELLPHONE": "8032034551",
   "CONTROLLER": "Ayobami Odunfa",
   "LATITUDE": "4.585419",
   "LONGITUDE": "7.578009"
 },
 {
   "NAME": "Sabo Junction Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Osun",
   "LGA": "Ife Central",
   "ADDRESS": "Adeyemo Filling Station, Sabo Junction, Ile-ife, Osun State",
   "CELLPHONE": "8032034533",
   "CONTROLLER": "Adebajo Olufemi",
   "LATITUDE": "7.501203",
   "LONGITUDE": "4.458689"
 },
 {
   "NAME": "Ikirun Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Osun",
   "LGA": "Ifelodun",
   "ADDRESS": "Habeeb Filling Station, Alamisi Junction, Ikirun, Osun State",
   "CELLPHONE": "8032032721",
   "CONTROLLER": "Tinuola Olupinsaye",
   "LATITUDE": "7.907877",
   "LONGITUDE": "4.657293"
 },
 {
   "NAME": "Ilesa Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Osun",
   "LGA": "Ilesa West",
   "ADDRESS": "A108 Orinkiran Street, Ilesa West, Off Ita -Akogun, Osun State",
   "CELLPHONE": "8032035193",
   "CONTROLLER": "olasunkanmi.Tanimomo",
   "LATITUDE": "7.616701",
   "LONGITUDE": "4.73333"
 },
 {
   "NAME": "Bowen Unilite, Iwo ",
   "TYPE": "Connect Point",
   "STATE": "Osun",
   "LGA": "Iwo",
   "ADDRESS": "Main Gate Entrance Bowen University, Iwo, Osun State",
   "CELLPHONE": "8032032826",
   "CONTROLLER": "Idowu Olusegun",
   "LATITUDE": "7.623536",
   "LONGITUDE": "4.189496"
 },
 {
   "NAME": "Esa Oke Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Osun",
   "LGA": "Obokun",
   "ADDRESS": "Osun State College Of Technology, Esa Oke, Beside Post Office, Osun State",
   "CELLPHONE": "8032030983",
   "CONTROLLER": "Olanike Leramo",
   "LATITUDE": "7.746984",
   "LONGITUDE": "4.883251"
 },
 {
   "NAME": "JABU Unilite",
   "TYPE": "Connect Point",
   "STATE": "Osun",
   "LGA": "Oriade ",
   "ADDRESS": "Joseph Ayo Babalola University Campus, Ikeji Arakeji, Osun State",
   "CELLPHONE": "8032030937",
   "CONTROLLER": "Adebiyi Olawunmi",
   "LATITUDE": "4.98539",
   "LONGITUDE": "7.419716"
 },
 {
   "NAME": "Owode Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Oyo",
   "LGA": "Atiba",
   "ADDRESS": "Oando Filling Station, Owode, Oyo",
   "CELLPHONE": "8032034548",
   "CONTROLLER": "Gabriel Opatoyinbo",
   "LATITUDE": "6.949719",
   "LONGITUDE": "3.506855"
 },
 {
   "NAME": "New ife Road Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Oyo",
   "LGA": "Egbeda",
   "ADDRESS": "By Roundabout, New Ife Road, Iwo Road, Ibadan, Oyo",
   "CELLPHONE": "8032030702",
   "CONTROLLER": "Abimbila Aina",
   "LATITUDE": "7.403752",
   "LONGITUDE": "3.928129"
 },
 {
   "NAME": "Idi Ape Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Oyo",
   "LGA": "Ibadan North East",
   "ADDRESS": "Oando Filling Station, Adjacent NECO Office, Idi Ape",
   "CELLPHONE": "8032030926",
   "CONTROLLER": "Christian Onwuka",
   "LATITUDE": "7.401861",
   "LONGITUDE": "3.926194"
 },
 {
   "NAME": "Iwo Road Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Oyo",
   "LGA": "Ibadan North East",
   "ADDRESS": "73, Opposite Firstbank branch, Iwo Road Ibadan",
   "CELLPHONE": "8032030698",
   "CONTROLLER": "Oluwaseyi",
   "LATITUDE": "7.4038822",
   "LONGITUDE": "3.9269005"
 },
 {
   "NAME": "Bodija Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Oyo",
   "LGA": "Ibadan North West",
   "ADDRESS": "5 Gbenro Ogunbiyi Street, Old Bodija , Along Secretariat /University Of Ibadan Road, Ibadan",
   "CELLPHONE": "8032030533",
   "CONTROLLER": "Inibehe Atanda",
   "LATITUDE": "7.425035",
   "LONGITUDE": "3.909335"
 },
 {
   "NAME": "Ibadan 2 Service Center",
   "TYPE": "Service Centre",
   "STATE": "Oyo",
   "LGA": "Ibadan North West",
   "ADDRESS": "Opp NECO building, Idi Ape, Agodi Gate, Ibadan",
   "CELLPHONE": "8032001510",
   "CONTROLLER": "funmilola Akinteye",
   "LATITUDE": "7.411359",
   "LONGITUDE": "3.9121"
 },
 {
   "NAME": "Mokola Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Oyo",
   "LGA": "Ibadan North West",
   "ADDRESS": "Oando Filling Station, Opposite Mokola Police Station",
   "CELLPHONE": "8032030694",
   "CONTROLLER": "Bankole Akinyimika",
   "LATITUDE": "7.400893",
   "LONGITUDE": "3.890229"
 },
 {
   "NAME": "Unilite Ibadan",
   "TYPE": "Connect Point",
   "STATE": "Oyo",
   "LGA": "Ibadan NorthEast",
   "ADDRESS": "Unilite, University Of Ibadan, Main Gate Car Park.",
   "CELLPHONE": "8032030534",
   "CONTROLLER": "Kehinde Ogunosun",
   "LATITUDE": "7.441166",
   "LONGITUDE": "3.907361"
 },
 {
   "NAME": "Cocoa Mall Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Oyo",
   "LGA": "Ibadan South West",
   "ADDRESS": "Shop 344 Coco Mall, Coco House, Dugbe, Ibadan, Oyo.",
   "CELLPHONE": "8032038139",
   "CONTROLLER": "Oluwaseyo",
   "LATITUDE": "7.3870522",
   "LONGITUDE": "3.879766"
 },
 {
   "NAME": "Total Garden  Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Oyo",
   "LGA": "Ibadan South West?",
   "ADDRESS": "Adjacent Uch, Ibadan",
   "CELLPHONE": "8032030925",
   "CONTROLLER": "Chinelo Ezenwa",
   "LATITUDE": "7.4033",
   "LONGITUDE": "3.910141"
 },
 {
   "NAME": "Eruwa Polytechnic Unilite",
   "TYPE": "Connect Point",
   "STATE": "Oyo",
   "LGA": "Ibarapa East",
   "ADDRESS": "Beside Ibarapa East LG Secretariat, Eruwa, Oyo State",
   "CELLPHONE": "8032032732",
   "CONTROLLER": "Balogun kayode",
   "LATITUDE": "7.448451",
   "LONGITUDE": "3.274986"
 },
 {
   "NAME": "Eleyele Connect pont",
   "TYPE": "Connect Point",
   "STATE": "Oyo",
   "LGA": "Ido",
   "ADDRESS": "Oando Filling Station, Beside Midas Pharmacy, Eleyele.",
   "CELLPHONE": "8032030692",
   "CONTROLLER": "Friday Adodo",
   "LATITUDE": "3.860957",
   "LONGITUDE": "7.417943"
 },
 {
   "NAME": "Iseyin Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Oyo",
   "LGA": "Iseyin",
   "ADDRESS": "Infront Of Oba Palace, Iseyin, Oyo State",
   "CELLPHONE": "8032030699",
   "CONTROLLER": "Babajide Kolade",
   "LATITUDE": "7.972558",
   "LONGITUDE": "3.592744"
 },
 {
   "NAME": "Iseyin Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Oyo",
   "LGA": "Iseyin",
   "ADDRESS": "Beside Post Office, Oluwole Area, Iseyin, Oyo State.",
   "CELLPHONE": "8032030699",
   "CONTROLLER": "Babajide Kolade",
   "LATITUDE": "7.973154",
   "LONGITUDE": "3.590845"
 },
 {
   "NAME": "Akobo Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Oyo",
   "LGA": "Lagelu",
   "ADDRESS": "Inside Mr Biggs Building, Akobo Ojurin Road, Oyo State",
   "CELLPHONE": "8032030700",
   "CONTROLLER": "Adebayo Ojo",
   "LATITUDE": "7.450868",
   "LONGITUDE": "3.951549"
 },
 {
   "NAME": "Lautech Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Oyo",
   "LGA": "Ogbomosho North",
   "ADDRESS": "34, Under G Car Park, LAUTECH Ogbomosho",
   "CELLPHONE": "8032038660",
   "CONTROLLER": "Olubunmi Adefisioye",
   "LATITUDE": "8.167773",
   "LONGITUDE": "4.266911"
 },
 {
   "NAME": "Ogbomosho Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Oyo",
   "LGA": "Ogbomosho North",
   "ADDRESS": "Alice Ogunpeju House, Alata Supermarket Building, Oja Tuntun, Sabo Road, Ogbomoso",
   "CELLPHONE": "8032034985",
   "CONTROLLER": "Oluwaseun Fadeyi",
   "LATITUDE": "8.14648",
   "LONGITUDE": "4.232416"
 },
 {
   "NAME": "Ibadan Service Center",
   "TYPE": "Service Centre",
   "STATE": "Oyo",
   "LGA": "Oluyole",
   "ADDRESS": "1, Olubadan Avenue, MTN Ibadan Regional Office",
   "CELLPHONE": "8032002022",
   "CONTROLLER": "Obilor Lucius",
   "LATITUDE": "7.366684",
   "LONGITUDE": "3.861268"
 },
 {
   "NAME": "Oyo Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Oyo",
   "LGA": "Oyo East",
   "ADDRESS": "?Adjacent Union Bank, Odo Eran, Owode-Oyo Road, Owode, Oyo",
   "CELLPHONE": "8032035023",
   "CONTROLLER": "Kehinde Johnson",
   "LATITUDE": "7.8371",
   "LONGITUDE": "3.921899"
 },
 {
   "NAME": "Oyo Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Oyo",
   "LGA": "Oyo West",
   "ADDRESS": "Durbar Stadium Along Ogbomoso/Ibadan Express Way, Oyo Town",
   "CELLPHONE": "8032034999",
   "CONTROLLER": "Oluwasegun Olajide",
   "LATITUDE": "8.13373",
   "LONGITUDE": "4.24014"
 },
 {
   "NAME": "Saki Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Oyo",
   "LGA": "Saki West",
   "ADDRESS": "Open space, Beside Ile Onijo Compound, Ajegunle, Sango Road, Saki, Oyo State",
   "CELLPHONE": "8032035191",
   "CONTROLLER": "ISAAC ANJORIN",
   "LATITUDE": "7.850604",
   "LONGITUDE": "3.930611"
 },
 {
   "NAME": "Bauchi Road Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Plateau",
   "LGA": "Jos North",
   "ADDRESS": "Oando Fillig Station, Bauchi Road, Plateau",
   "CELLPHONE": "8032030526",
   "CONTROLLER": "Sadiya isiaku",
   "LATITUDE": "7.7652",
   "LONGITUDE": "9.9211"
 },
 {
   "NAME": "Jos Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Plateau",
   "LGA": "Jos North",
   "ADDRESS": "1B Ibrahim Taiwo Road, Plateau",
   "CELLPHONE": "8032038942",
   "CONTROLLER": "Lawrence",
   "LATITUDE": "9.898855",
   "LONGITUDE": "8.882366"
 },
 {
   "NAME": "Rwang Pam Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Plateau",
   "LGA": "Jos North",
   "ADDRESS": "Open Space, No 43, Rwang Pam Street, behind Zenith bank, Jos, Plateau",
   "CELLPHONE": "8032037668",
   "CONTROLLER": "Ikwu Simon",
   "LATITUDE": "9.921317",
   "LONGITUDE": "8.889271"
 },
 {
   "NAME": "Uni Jos Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Plateau",
   "LGA": "Jos North",
   "ADDRESS": "University Of Jos, Temp. Site, Bauchi Road, Plateau State",
   "CELLPHONE": "8032030508",
   "CONTROLLER": "Dunka David",
   "LATITUDE": "9.953",
   "LONGITUDE": "8.8894"
 },
 {
   "NAME": "Unijos permanent site Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Plateau",
   "LGA": "Jos North",
   "ADDRESS": "Open Space, near diamond ATM machine Abuja hostel, University of Jos",
   "CELLPHONE": "8032037468",
   "CONTROLLER": "Rita Walu",
   "LATITUDE": "8.8682",
   "LONGITUDE": "9.9591"
 },
 {
   "NAME": "Bukuru Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Plateau",
   "LGA": "Jos South",
   "ADDRESS": "Open Space Bukuru, Post Office Premises, Jos South, Plateau",
   "CELLPHONE": "8032037678",
   "CONTROLLER": "Gambo Titus",
   "LATITUDE": "9.9876",
   "LONGITUDE": "10.9341"
 },
 {
   "NAME": "Jos 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Plateau",
   "LGA": "Jos South",
   "ADDRESS": "22 Yakubu Gowon Way, Tilley Gyado Building, Jos South, Plateau State",
   "CELLPHONE": "8032038177",
   "CONTROLLER": "Edache Innocent",
   "LATITUDE": "9.888288",
   "LONGITUDE": "8.75656"
 },
 {
   "NAME": "Mangu Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Plateau",
   "LGA": "Jos West",
   "ADDRESS": "Open Space, After Mangu Police Station, Mangu, Plateau",
   "CELLPHONE": "8032038177",
   "CONTROLLER": "8032031089",
   "LATITUDE": "9.487715",
   "LONGITUDE": "9.134159"
 },
 {
   "NAME": "Pankshin Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Plateau",
   "LGA": "Pankshin",
   "ADDRESS": "Open Space, Opposite FCE Pankshin, Pankshin-Jos Road, Plateau",
   "CELLPHONE": "8032030468",
   "CONTROLLER": "Pwajok Sylvanus",
   "LATITUDE": "9.3279",
   "LONGITUDE": "9.4312"
 },
 {
   "NAME": "NAMU  Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Plateau",
   "LGA": "Qua'an Pan",
   "ADDRESS": "Open Space Namu, opp kasuwan Doya kanawa, Namu Qua'anpan, Plateau State.",
   "CELLPHONE": "8032038182",
   "CONTROLLER": "Jordan Titus",
   "LATITUDE": "9.1244",
   "LONGITUDE": "8.7797"
 },
 {
   "NAME": "Shendam Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Plateau",
   "LGA": "Shendam",
   "ADDRESS": "Open Space, By Agric Bank, Shendam, Plateau",
   "CELLPHONE": "8032032154",
   "CONTROLLER": "Danladi Johanes",
   "LATITUDE": "8.8955",
   "LONGITUDE": "9.4537"
 },
 {
   "NAME": "Ahoada Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Rivers",
   "LGA": "Ahoada East",
   "ADDRESS": "70, Hospital?Road, Opposite Delta Hotels Ltd, Ahoada",
   "CELLPHONE": "8032038081",
   "CONTROLLER": "Mary Akatobi",
   "LATITUDE": "5.082778",
   "LONGITUDE": "6.6523188"
 },
 {
   "NAME": "Bonny Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Rivers",
   "LGA": "Bonny",
   "ADDRESS": "22 Shell Road, Beside Shell Petroleum Main Gate, Bonny",
   "CELLPHONE": "8032030219",
   "CONTROLLER": "Emmanuel Owunna",
   "LATITUDE": "4.851525",
   "LONGITUDE": "7.060778"
 },
 {
   "NAME": "Eleme Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Eleme",
   "ADDRESS": "7 Ogale Road, by Civic Centre, Eleme.",
   "CELLPHONE": "8032034574",
   "CONTROLLER": "Anthony Okye",
   "LATITUDE": "7.125885",
   "LONGITUDE": "4.786935"
 },
 {
   "NAME": "Onne Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Eleme",
   "ADDRESS": "Iita Junction, Before Fot Junction, Onne",
   "CELLPHONE": "8032032700",
   "CONTROLLER": "Kalu Udo",
   "LATITUDE": "7.035316",
   "LONGITUDE": "4.744334"
 },
 {
   "NAME": "Elele Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Ikwerre",
   "ADDRESS": "Elele Round About, Beside Ecobank, Elele, Rivers State",
   "CELLPHONE": "8032037715",
   "CONTROLLER": "Alerechi Eze",
   "LATITUDE": "8.38222",
   "LONGITUDE": "6.6112"
 },
 {
   "NAME": "Port Harcourt Int'l Airport Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Rivers",
   "LGA": "Ikwerre",
   "ADDRESS": "Depature Hall, International Wing, Port Harcourt Int'L Airport, River State",
   "CELLPHONE": "8032034577",
   "CONTROLLER": "Kingsley Ekezie",
   "LATITUDE": "6.950219",
   "LONGITUDE": "5.007478"
 },
 {
   "NAME": "Bori Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Khana",
   "ADDRESS": "30 Poly Road, Bori Ogoni, After FCMB Bank",
   "CELLPHONE": "8032032752",
   "CONTROLLER": "Burabari Kpobari",
   "LATITUDE": "7.129045",
   "LONGITUDE": "4.777484"
 },
 {
   "NAME": "Agip Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "Chief Minikwu Plaza, 12 Agip Road, Rumueme, Port Harcourt",
   "CELLPHONE": "8032038257",
   "CONTROLLER": "Ezinwa Nwadinigwe",
   "LATITUDE": "6.982559",
   "LONGITUDE": "4.813216"
 },
 {
   "NAME": "Elekahia Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "Plot 3 Circular Road, Elekahia Housing Estate, Port Harcourt",
   "CELLPHONE": "8032030840",
   "CONTROLLER": "Jovita Soronnadi",
   "LATITUDE": "7.024667",
   "LONGITUDE": "4.823015"
 },
 {
   "NAME": "Federal Secretariat Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "Federal Secretariat Aba Road, Port Harcourt",
   "CELLPHONE": "8032034584",
   "CONTROLLER": "Ezinne Harbor",
   "LATITUDE": "5.75424",
   "LONGITUDE": "7.56392"
 },
 {
   "NAME": "Igwuruta Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "Igwuruta Park, Beside Anglican Church Airport Road, Igwuruta, River State",
   "CELLPHONE": "8032030084",
   "CONTROLLER": "Joy Emmanuel",
   "LATITUDE": "7.01162",
   "LONGITUDE": "4.95418"
 },
 {
   "NAME": "Iwofe Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "Beside EnjoyFull Bar, Iwofe Road, Rumuolumeni, Port Harcourt",
   "CELLPHONE": "8032030532",
   "CONTROLLER": "Sandra Iwuagwu",
   "LATITUDE": "6.96253",
   "LONGITUDE": "4.82623"
 },
 {
   "NAME": "NDDC Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "NDDC Secretariat Aba Road, Port Harcourt",
   "CELLPHONE": "8032030506",
   "CONTROLLER": "Ifesinachi Nsude",
   "LATITUDE": "6.999952",
   "LONGITUDE": "4.807727"
 },
 {
   "NAME": "Oando Ikwere Road Mile 3",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "Oando Filling Station, 174 Ikwere Road, Mile 3, Port Harcourt",
   "CELLPHONE": "8032037738",
   "CONTROLLER": "Immaculata Ezenwa",
   "LATITUDE": "6.984408",
   "LONGITUDE": "4.81871"
 },
 {
   "NAME": "Port Harcourt 1 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "5 Rumuokwuta Road, Port Harcourt, River State",
   "CELLPHONE": "8032038076",
   "CONTROLLER": "Onyema Ikewuchi",
   "LATITUDE": "6.987971",
   "LONGITUDE": "4.8394467"
 },
 {
   "NAME": "Port Harcourt 4 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "330 Aba Road, Opposite Access Bank, Oil Mill Junction, River State",
   "CELLPHONE": "8032037433",
   "CONTROLLER": "Christopher Ephraim",
   "LATITUDE": "7.0097138",
   "LONGITUDE": "4.8175666"
 },
 {
   "NAME": "Port Harcourt Service Center",
   "TYPE": "Service Centre",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "234b, Aba Expressway, Opp Hannah Fast Food, Port Harcourt",
   "CELLPHONE": "8032002274",
   "CONTROLLER": "Kate Nyeche Woluchor",
   "LATITUDE": "7.0093362",
   "LONGITUDE": "4.8175689"
 },
 {
   "NAME": "Rumuigbo Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "504 Ikwere Road, by Rumuigbo Junction,Port Harcourt, Rivers State",
   "CELLPHONE": "8032038006",
   "CONTROLLER": "Chinedum Dennis",
   "LATITUDE": "6.99269",
   "LONGITUDE": "4.855"
 },
 {
   "NAME": "Rupukwu Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "1 E-Star Pack Elikpokpodu ?Road, Rupukwu Junction, Portharcourt.",
   "CELLPHONE": "8032038004",
   "CONTROLLER": "Nwabunor Samuel",
   "LATITUDE": "6.999171",
   "LONGITUDE": "4.907824"
 },
 {
   "NAME": "Shell IA Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "Shell Industrial Area, Port Harcourt, Rivers State",
   "CELLPHONE": "8032030826",
   "CONTROLLER": "Izu Onisokien Reuben",
   "LATITUDE": "7.0327",
   "LONGITUDE": "4.8257"
 },
 {
   "NAME": "State Secretariat Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "Car Park, Podium Block, State Secretariat, Port Harcourt, Rivers State",
   "CELLPHONE": "8032037696",
   "CONTROLLER": "Stella Duruaku",
   "LATITUDE": "7.016615",
   "LONGITUDE": "4.777402"
 },
 {
   "NAME": "University Of Port Harcourt Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "Abuja Park, University of Port Harcourt, Choba, Rivers State",
   "CELLPHONE": "8032034488",
   "CONTROLLER": "Amaka Ohagwu",
   "LATITUDE": "6.920453",
   "LONGITUDE": "4.901798"
 },
 {
   "NAME": "Woji Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Rivers",
   "LGA": "Obio-Akpor",
   "ADDRESS": "55 Woji Estate Road, Port Harcourt, Rivers State",
   "CELLPHONE": "8032038260",
   "CONTROLLER": "Ijeoma Agwu",
   "LATITUDE": "7.0539727",
   "LONGITUDE": "4.8222044"
 },
 {
   "NAME": "Obigbo Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Rivers",
   "LGA": "Oyigbo",
   "ADDRESS": "34, Location Road, Besides Rennys Fast Food & Restaurant, Obigbo",
   "CELLPHONE": "8032030232",
   "CONTROLLER": "Stella Maduako",
   "LATITUDE": "7.13364",
   "LONGITUDE": "4.877886"
 },
 {
   "NAME": "Oyigbo Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Oyigbo",
   "ADDRESS": "36 Aba Express Road, Oyigbo, Rivers State",
   "CELLPHONE": "8032030460",
   "CONTROLLER": "Samson IKEMWEN",
   "LATITUDE": "7.13261",
   "LONGITUDE": "4.87257"
 },
 {
   "NAME": "Umuebule Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Oyigbo",
   "ADDRESS": "No 35 Umuebule Road, By Okrika Plantation, Etche, Rivers State",
   "CELLPHONE": "8032030518",
   "CONTROLLER": "Chinenye Ibekwe",
   "LATITUDE": "7.12517",
   "LONGITUDE": "4.886862"
 },
 {
   "NAME": "Port Harcourt Mall Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Rivers",
   "LGA": "Port Harcourt",
   "ADDRESS": "1 Azikiway Road, By Govt House, Port Harcourt",
   "CELLPHONE": "8032030209",
   "CONTROLLER": "Mary Olive",
   "LATITUDE": "4.7755322",
   "LONGITUDE": "7.0131803"
 },
 {
   "NAME": "Abuloma Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Port Harcourt",
   "ADDRESS": "Ezi Plaza, 208/204 Abuloma Road, Before Federal Junction, Port Harcourt",
   "CELLPHONE": "8032037718",
   "CONTROLLER": "Judith Anosike",
   "LATITUDE": "7.042599",
   "LONGITUDE": "4.781523"
 },
 {
   "NAME": "Borokiri Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Port Harcourt",
   "ADDRESS": "Borokiri, By Navy Secondary School Borokiri (Relocated From Uni Science And Tech Rivers )",
   "CELLPHONE": "8032034572",
   "CONTROLLER": "Jonathan Nzeh",
   "LATITUDE": "7.036402",
   "LONGITUDE": "4.746253"
 },
 {
   "NAME": "Ignatius Ajuru University of Education",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Port Harcourt",
   "ADDRESS": "Ignatius Ajuru University of Education, St John's Bus Stop, Port Harcourt",
   "CELLPHONE": "7062021263",
   "CONTROLLER": "Kelechi Owowo",
   "LATITUDE": "7.011374",
   "LONGITUDE": "4.812251"
 },
 {
   "NAME": "NPA Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Rivers",
   "LGA": "Port Harcourt",
   "ADDRESS": "MRS Filling Station NPA Road, By Flour Mills, Port Harcourt",
   "CELLPHONE": "8032037717",
   "CONTROLLER": "Bob Davies",
   "LATITUDE": "7.01069",
   "LONGITUDE": "4.76776"
 },
 {
   "NAME": "Port Harcourt 2 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Rivers",
   "LGA": "Port Harcourt",
   "ADDRESS": "67 Aba Road, Portharcourt, River State",
   "CELLPHONE": "8032030525",
   "CONTROLLER": "Benjamin Eke",
   "LATITUDE": "7.0094554",
   "LONGITUDE": "4.8175717"
 },
 {
   "NAME": "Port Harcourt 3 Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Rivers",
   "LGA": "Port Harcourt",
   "ADDRESS": "22 Aggrey Road, Port Harcourt, River State",
   "CELLPHONE": "8032037709",
   "CONTROLLER": "Ginikachi Ibekwe",
   "LATITUDE": "7.0190133",
   "LONGITUDE": "4.7614936"
 },
 {
   "NAME": "Rumuokoro Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Rivers",
   "LGA": "Port Harcourt",
   "ADDRESS": "42 Airport Road, Opposite Obio-Akpo Local Government Area secretariat, PH",
   "CELLPHONE": "8032038081",
   "CONTROLLER": "Success Eziuka",
   "LATITUDE": "6.998442",
   "LONGITUDE": "4.868391"
 },
 {
   "NAME": "Bodinga  Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Sokoto",
   "LGA": "Bodinga",
   "ADDRESS": "Sani Sahabi Multi Resources Ltd Filling Station, Bodinga, Sokoto",
   "CELLPHONE": "8032032815",
   "CONTROLLER": "HALIRU ABDULLHI",
   "LATITUDE": "12.838138",
   "LONGITUDE": "5.142612"
 },
 {
   "NAME": "Ilela Connect Point ",
   "TYPE": "Connect Point",
   "STATE": "Sokoto",
   "LGA": "Illela",
   "ADDRESS": "Off Gada Road, Illela",
   "CELLPHONE": "8032032813",
   "CONTROLLER": "UMAR SANI",
   "LATITUDE": "13.726",
   "LONGITUDE": "5.2975"
 },
 {
   "NAME": "Sokoto North Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Sokoto",
   "LGA": "Sokoto South",
   "ADDRESS": "Former Stanbic Bank Office, 2 Maiduguri Road, Sokoto",
   "CELLPHONE": "8032037728",
   "CONTROLLER": "Aliyu Ibrahim",
   "LATITUDE": "13.0589715",
   "LONGITUDE": "5.2327451"
 },
 {
   "NAME": "Sokoto Service Center",
   "TYPE": "Service Centre",
   "STATE": "Sokoto",
   "LGA": "Sokoto South",
   "ADDRESS": "6, Sultan Abubakar Road, Opp Women Centre, Sokoto",
   "CELLPHONE": "8032007421",
   "CONTROLLER": "Yusuf Yusuf",
   "LATITUDE": "13.052553",
   "LONGITUDE": "5.247477"
 },
 {
   "NAME": "Gusau Rd Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Sokoto",
   "LGA": "Sokoto South ",
   "ADDRESS": "Oando Filling Station, Gusau Road, Sokoto",
   "CELLPHONE": "8032033797",
   "CONTROLLER": "NURUDEEN IBRAHIM",
   "LATITUDE": "13.020137",
   "LONGITUDE": "5.24251"
 },
 {
   "NAME": "Jalingo Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Taraba",
   "LGA": "Jalingo",
   "ADDRESS": "Oando Filling Station, Road Block Opp Main Motor Park, Jalingo, Taraba State ",
   "CELLPHONE": "8032031085",
   "CONTROLLER": "Peter Daniel",
   "LATITUDE": "11.338491",
   "LONGITUDE": "8.936559"
 },
 {
   "NAME": "Jalingo Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Taraba",
   "LGA": "Jalingo",
   "ADDRESS": "Lenyol Investment Ltd. Off Taraba Motel Road, G.R.A, Jalingo, Taraba State ",
   "CELLPHONE": "8032030341",
   "CONTROLLER": "Joshua Marcus",
   "LATITUDE": "11.358439",
   "LONGITUDE": "8.902629"
 },
 {
   "NAME": "Wukari Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Taraba",
   "LGA": "Wukari",
   "ADDRESS": "Takum Junction, Best Albina Wukari, Takum Road, Wukari, Taraba State ",
   "CELLPHONE": "8032031086",
   "CONTROLLER": "Danjuma Jirassa",
   "LATITUDE": "9.97099",
   "LONGITUDE": "7.25469"
 },
 {
   "NAME": "Wukari Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Taraba",
   "LGA": "Wukari",
   "ADDRESS": "Old Bantage Road, Opposite Jenkabi Nursery And Primary School, Wukari, Taraba State ",
   "CELLPHONE": "8032031087",
   "CONTROLLER": "Comfort Yavala",
   "LATITUDE": "9.783248",
   "LONGITUDE": "7.878714"
 },
 {
   "NAME": "Zing Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Taraba",
   "LGA": "Zing",
   "ADDRESS": "Open Space, By Collage Of Education Main Gate, Along Yola Road. Zing, Taraba State ",
   "CELLPHONE": "8032032675",
   "CONTROLLER": "Joseph Gambo",
   "LATITUDE": "11.763724",
   "LONGITUDE": "8.989464"
 },
 {
   "NAME": "Damaturu Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Yobe",
   "LGA": "Damaturu",
   "ADDRESS": "Maiduguri Road, Opposite A.A. Baffa & Sons Filling Station, Damaturu, Yobe State ",
   "CELLPHONE": "8032032110",
   "CONTROLLER": "Mustapha Umar",
   "LATITUDE": "11.743874",
   "LONGITUDE": "11.977331"
 },
 {
   "NAME": "Portiskum Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Yobe",
   "LGA": "Potiskum",
   "ADDRESS": "Oando Filling Station, Near Portiskum Main Market, Off Kano Road, Portiskum, Yobe",
   "CELLPHONE": "8032030879",
   "CONTROLLER": "Mas'ud Abdulfatah",
   "LATITUDE": "11.077137",
   "LONGITUDE": "11.713668"
 },
 {
   "NAME": "Gusau Connect Store",
   "TYPE": "Connect Store",
   "STATE": "Zamfara",
   "LGA": "Gusau",
   "ADDRESS": "AP Filling Station Along Zaria Road, Gusau, Zamfara State",
   "CELLPHONE": "8032030345",
   "CONTROLLER": "Murtala Bello",
   "LATITUDE": "12.171429",
   "LONGITUDE": "6.679213"
 },
 {
   "NAME": "Kaura Namoda Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Zamfara",
   "LGA": "Kaura Namoda",
   "ADDRESS": "Oando Filling Station, Kaura Namoda, Zamfara State",
   "CELLPHONE": "8032030577",
   "CONTROLLER": "Ganiyat Mudasir",
   "LATITUDE": "12.600208",
   "LONGITUDE": "6.589745"
 },
 {
   "NAME": "Shinkafi Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Zamfara",
   "LGA": "Shinkafi",
   "ADDRESS": "Oando Filling Station, Shinkafi, Zamfara",
   "CELLPHONE": "8032033985",
   "CONTROLLER": "Stella Eneh",
   "LATITUDE": "13.092737",
   "LONGITUDE": "6.497211"
 },
 {
   "NAME": "Talata Mafara Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Zamfara",
   "LGA": "Talata Mafara",
   "ADDRESS": "Oando Filling Station, Talata Mafara, Zamfara",
   "CELLPHONE": "8032033988",
   "CONTROLLER": "Stella Eneh",
   "LATITUDE": "12.561781",
   "LONGITUDE": "6.065272"
 },
 {
   "NAME": "Tsafe Connect Point",
   "TYPE": "Connect Point",
   "STATE": "Zamfara",
   "LGA": "Tsafe",
   "ADDRESS": "Oando Filling Station, Tsafe, Zamfara",
   "CELLPHONE": "8032030572",
   "CONTROLLER": "Ibrahim Umar",
   "LATITUDE": "11.959042",
   "LONGITUDE": "6.916706"
 }
]
};










export const distanceMappedCenters = {
    "service_center":[
    ]
}
export const SuperAgent = {
   "Details" : [
  {
    "AgentName": "Maduka Emmanuel",
    "TYPE": "Retail Agent",
    "STATE": "Abia",
    "LGA": "Osisioma",
    "ADDRESS": "15 Nwaogu Street, Osisioma, Abia State",
    "CELLPHONE": 9034485989,
    "LATITUDE": 7.38112,
    "LONGITUDE": 5.10589
  },
  {
    "AgentName": "Okoroafor Nataniel",
    "TYPE": "Retail Agent",
    "STATE": "Abia",
    "LGA": "Osisioma",
    "ADDRESS": "Aru Ngwa Junction, Osisioma, Abia State",
    "CELLPHONE": 8062501811,
    "LATITUDE": 7.44499,
    "LONGITUDE": 5.49137
  },
  {
    "AgentName": "Ulaku Felicia",
    "TYPE": "Retail Agent",
    "STATE": "Abia",
    "LGA": "Aba North",
    "ADDRESS": "Brass Junction, Aba, Abia State",
    "CELLPHONE": 8136193673,
    "LATITUDE": 7.03813,
    "LONGITUDE": 4.83855
  },
  {
    "AgentName": "Asuquo Ernest Thomas",
    "TYPE": "Retail Agent",
    "STATE": "Akwa Ibom",
    "LGA": "Okobo",
    "ADDRESS": "Okobo Road, Beside Police Station, Okobo, Akwa-Ibom State",
    "CELLPHONE": 9067354832,
    "LATITUDE": 8.13204,
    "LONGITUDE": 4.82714
  },
  {
    "AgentName": "Anaka Godfrey Anaka",
    "TYPE": "Retail Agent",
    "STATE": "Anambra",
    "LGA": "Idemili North",
    "ADDRESS": "No 3 Limca Road Nkpor, Anambra State",
    "CELLPHONE": 8145178885,
    "LATITUDE": 6.835671,
    "LONGITUDE": 6.835671
  },
  {
    "AgentName": "Anaka Ifeanyi Godfrey",
    "TYPE": "Retail Agent",
    "STATE": "Anambra",
    "LGA": "Orumber South",
    "ADDRESS": "Nkwoumunze Roundabout, Orumber South, Anambra",
    "CELLPHONE": 9038807848,
    "LATITUDE": 6.147642,
    "LONGITUDE": 6.831891
  },
  {
    "AgentName": "Chiemezie Anthony",
    "TYPE": "Retail Agent",
    "STATE": "Anambra",
    "LGA": "Nnewi North",
    "ADDRESS": "150 Old Onitsha Rd Nnewi, Anambra State",
    "CELLPHONE": 8032741303,
    "LATITUDE": 6.907461,
    "LONGITUDE": 6.034975
  },
  {
    "AgentName": "Onyekwere Ifeoma Jacinta",
    "TYPE": "Retail Agent",
    "STATE": "Anambra",
    "LGA": "Nnewi North",
    "ADDRESS": "16 Onitsha Rd Nnewi , Anambra State",
    "CELLPHONE": 8135728918,
    "LATITUDE": 6.916693,
    "LONGITUDE": 6.020685
  },
  {
    "AgentName": "Abba Hajara Juji",
    "TYPE": "Retail Agent",
    "STATE": "Bauchi",
    "LGA": "Alkaleri",
    "ADDRESS": "Gombe Road,  Gwaram,  Alkaleri,  Bauchi",
    "CELLPHONE": 9039944477,
    "LATITUDE": 10.27906,
    "LONGITUDE": 9.79234
  },
  {
    "AgentName": "Ibrahim Ismail Yahaya",
    "TYPE": "Retail Agent",
    "STATE": "Bauchi",
    "LGA": "Tafawa Balewa",
    "ADDRESS": "Bal Bula District, Bununu,  Tafawa Balewa LGA,  Bauchi.",
    "CELLPHONE": 8032616138,
    "LATITUDE": 9.865799,
    "LONGITUDE": 9.699506
  },
  {
    "AgentName": "Ibrahim Ismail Yahaya",
    "TYPE": "Retail Agent",
    "STATE": "Bauchi",
    "LGA": "Bogoro",
    "ADDRESS": "Bakin Kasuwa, Bogoro,  Bogoro LGA Bauchi.",
    "CELLPHONE": 7031500008,
    "LATITUDE": 9.66667,
    "LONGITUDE": 9.6
  },
  {
    "AgentName": "Ibrahim Saadu",
    "TYPE": "Retail Agent",
    "STATE": "Bauchi",
    "LGA": "Bogoro",
    "ADDRESS": "Kasuwa,  Bogoro LGA,  Bauchi.",
    "CELLPHONE": 8065822022,
    "LATITUDE": 9.66667,
    "LONGITUDE": 9.6
  },
  {
    "AgentName": "Muhammed Nasiru Alhaji",
    "TYPE": "Retail Agent",
    "STATE": "Bauchi",
    "LGA": "Bauchi",
    "ADDRESS": "Yelwan Tudu Market,  Yelwa,  Bauchi.",
    "CELLPHONE": 8062330039,
    "LATITUDE": 7.102798,
    "LONGITUDE": 6.224098
  },
  {
    "AgentName": "Umar  Ibrahim Saleh",
    "TYPE": "Retail Agent",
    "STATE": "Bauchi",
    "LGA": "Tafawa Balewa",
    "ADDRESS": "Bununu, Tafawa Balewa, Bauchi",
    "CELLPHONE": 9030609643,
    "LATITUDE": 9.75986,
    "LONGITUDE": 9.55304
  },
  {
    "AgentName": "Ekong  George Sunday",
    "TYPE": "Retail Agent",
    "STATE": "Cross River",
    "LGA": "Ogoja",
    "ADDRESS": "No 15 Agiga New Layout Ogoja, Cross River State",
    "CELLPHONE": 8032208774,
    "LATITUDE": 9.404974,
    "LONGITUDE": 7.607418
  },
  {
    "AgentName": "Alexander Nwaraku",
    "TYPE": "Retail Agent",
    "STATE": "Delta",
    "LGA": "Warri South",
    "ADDRESS": "80 Olomu Street Warri, Delta State",
    "CELLPHONE": 7064624804,
    "LATITUDE": 5.547503,
    "LONGITUDE": 5.781682
  },
  {
    "AgentName": "Odiah Rose",
    "TYPE": "Retail Agent",
    "STATE": "Delta",
    "LGA": "Ndokwa East",
    "ADDRESS": "Pontu Beneku, Delta State",
    "CELLPHONE": 9039705050,
    "LATITUDE": 5.903262,
    "LONGITUDE": 6.641339
  },
  {
    "AgentName": "Omorere Henry",
    "TYPE": "Retail Agent",
    "STATE": "Delta",
    "LGA": "Warri South",
    "ADDRESS": "226 Warri Sapele Road, Delta State",
    "CELLPHONE": 7065121557,
    "LATITUDE": 5.569408,
    "LONGITUDE": 5.750012
  },
  {
    "AgentName": "Nworie Elochukwu Mathew",
    "TYPE": "Retail Agent",
    "STATE": "Ebonyi",
    "LGA": "Abakaliki",
    "ADDRESS": "Amaorie Ozziza, Along Orra Beach Road, Afikpo North, Ebonyi State",
    "CELLPHONE": 8032031111,
    "LATITUDE": 8.029865,
    "LONGITUDE": 6.168
  },
  {
    "AgentName": "Ibeabuche  Nkiru",
    "TYPE": "Retail Agent",
    "STATE": "Enugu",
    "LGA": "Udi",
    "ADDRESS": "4 Nsukka Road 9Th Mile, Enugu State",
    "CELLPHONE": 9060609042,
    "LATITUDE": 6.506872,
    "LONGITUDE": 7.40602
  },
  {
    "AgentName": "Ifeacho Blessing",
    "TYPE": "Retail Agent",
    "STATE": "Enugu",
    "LGA": "Enugu North",
    "ADDRESS": "139 Agbani Road Enugu State",
    "CELLPHONE": 8030462800,
    "LATITUDE": 7.781404,
    "LONGITUDE": 4.556105
  },
  {
    "AgentName": "Obodo  Michael Emeka",
    "TYPE": "Retail Agent",
    "STATE": "Enugu",
    "LGA": "Udi",
    "ADDRESS": "35 Abagana Street 9Th Mile, Enugu State",
    "CELLPHONE": 8145600029,
    "LATITUDE": 6.506872,
    "LONGITUDE": 7.40602
  },
  {
    "AgentName": "Kelechi  Madu",
    "TYPE": "Retail Agent",
    "STATE": "FCT",
    "LGA": "Amac",
    "ADDRESS": "Jahi2 Primary School, Amac, FCT",
    "CELLPHONE": 8136071351,
    "LATITUDE": 6.294637,
    "LONGITUDE": 5.595481
  },
  {
    "AgentName": "Chilaka Smart Faith",
    "TYPE": "Retail Agent",
    "STATE": "Imo",
    "LGA": "Owerri North",
    "ADDRESS": "Kilometer 3 Hydrobustop Mpama Egbu, Imo State",
    "CELLPHONE": 7032810880,
    "LATITUDE": 5.77599,
    "LONGITUDE": 6.8206
  },
  {
    "AgentName": "Chilaka Smart Precious",
    "TYPE": "Retail Agent",
    "STATE": "Imo",
    "LGA": "Owerri Municipal",
    "ADDRESS": "No 42 Crescent Egbeada Housing Estate Owerri, Imo State",
    "CELLPHONE": 7031991543,
    "LATITUDE": 5.513565,
    "LONGITUDE": 7.011262
  },
  {
    "AgentName": "Igbo Augustine Ikechi",
    "TYPE": "Retail Agent",
    "STATE": "Imo",
    "LGA": "Owerri West",
    "ADDRESS": "Umuofela Nekede By Maternity Junction, Imo State",
    "CELLPHONE": 7035760486,
    "LATITUDE": 13.149238,
    "LONGITUDE": 5.298635
  },
  {
    "AgentName": "Igbo Augustine Ikechi",
    "TYPE": "Retail Agent",
    "STATE": "Imo",
    "LGA": "Owerri North",
    "ADDRESS": "Umuezuo Street Naze, Imo State",
    "CELLPHONE": 8108535369,
    "LATITUDE": 6.48081,
    "LONGITUDE": 3.30655
  },
  {
    "AgentName": "Nwachukwu Amarachi Grace",
    "TYPE": "Retail Agent",
    "STATE": "Imo",
    "LGA": "Owerri Municipal",
    "ADDRESS": "No 8 Wethdral Road Owerri, Imo State",
    "CELLPHONE": 8140373365,
    "LATITUDE": 5.48333,
    "LONGITUDE": 7.03041
  },
  {
    "AgentName": "Nwachukwu Amarachi Grace",
    "TYPE": "Retail Agent",
    "STATE": "Imo",
    "LGA": "Owerri West",
    "ADDRESS": "Federal Poly Junction Nekede, Imo State",
    "CELLPHONE": 8163836650,
    "LATITUDE": 5.43404,
    "LONGITUDE": 7.02738
  },
  {
    "AgentName": "Ali Bashir Usiman",
    "TYPE": "Retail Agent",
    "STATE": "Kano",
    "LGA": "Gwale",
    "ADDRESS": "Kabuga Bursili Street Opposite Customs Training College G/Dutse Kano.",
    "CELLPHONE": 9038116659,
    "LATITUDE": 8.5086,
    "LONGITUDE": 11.9914
  },
  {
    "AgentName": "Muhammad Mansur Lawan",
    "TYPE": "Retail Agent",
    "STATE": "Kano",
    "LGA": "Fagge",
    "ADDRESS": "Filing Parking Kofar Gida Alaramma. Kasuwa Kwari. Kano State",
    "CELLPHONE": 9039696223,
    "LATITUDE": 8.3225,
    "LONGITUDE": 13.0314
  },
  {
    "AgentName": "Warda Habu Stephen",
    "TYPE": "Retail Agent",
    "STATE": "Kano",
    "LGA": "Kano Municipal",
    "ADDRESS": "No 4 Hassan Dan Kabo Street Gandun Albasa, Kano State",
    "CELLPHONE": 8066623389,
    "LATITUDE": 8.5296,
    "LONGITUDE": 11.9773
  },
  {
    "AgentName": "Yakubu Inuwa",
    "TYPE": "Retail Agent",
    "STATE": "Kano",
    "LGA": "Fagge",
    "ADDRESS": "No. 20 Line 2 Filling Parking Yan Tibura Mall Kantin Kuri, Kano State",
    "CELLPHONE": 8038963896,
    "LATITUDE": 8.5311,
    "LONGITUDE": 12.0017
  },
  {
    "AgentName": "Ahmad Anas",
    "TYPE": "Retail Agent",
    "STATE": "Katsina",
    "LGA": "Katsina",
    "ADDRESS": "Along Dutsinma Road Close To FCE, Katsina State",
    "CELLPHONE": 8039695959,
    "LATITUDE": 8.1537,
    "LONGITUDE": 12.5996
  },
  {
    "AgentName": "Ahmad Shamsu",
    "TYPE": "Retail Agent",
    "STATE": "Katsina",
    "LGA": "Funtua",
    "ADDRESS": "Katsina Road Funtua, Katsina State",
    "CELLPHONE": 7063029731,
    "LATITUDE": 6.4037,
    "LONGITUDE": 12.3329
  },
  {
    "AgentName": "Kabir  Abubakar",
    "TYPE": "Retail Agent",
    "STATE": "Katsina",
    "LGA": "Kusada",
    "ADDRESS": "Kofar Gabbas, Ingawa, Katsina State",
    "CELLPHONE": 8060322225,
    "LATITUDE": 7.8337,
    "LONGITUDE": 12.5521
  },
  {
    "AgentName": "Olaniyan Yusuf",
    "TYPE": "Retail Agent",
    "STATE": "Katsina",
    "LGA": "Daura",
    "ADDRESS": "Muhammad Bashar Road Before Nasfah Plaza Daura, Katsina State",
    "CELLPHONE": 7039769423,
    "LATITUDE": 8.3197,
    "LONGITUDE": 13.0373
  },
  {
    "AgentName": "Sulaiman Abubakar",
    "TYPE": "Retail Agent",
    "STATE": "Katsina",
    "LGA": "Daura",
    "ADDRESS": "Baru Layout Kiribati Daura, Katsina State",
    "CELLPHONE": 9035088770,
    "LATITUDE": 7.6168,
    "LONGITUDE": 11.7928
  },
  {
    "AgentName": "Suleman Babangida",
    "TYPE": "Retail Agent",
    "STATE": "Katsina",
    "LGA": "Malumfashi",
    "ADDRESS": "Liyin Tomato Malunfashi Central Market, Katsina State",
    "CELLPHONE": 7032481884,
    "LATITUDE": 7.6008,
    "LONGITUDE": 12.9718
  },
  {
    "AgentName": "Adejoh A.  Anthony",
    "TYPE": "Retail Agent",
    "STATE": "Kogi",
    "LGA": "Dekina",
    "ADDRESS": "Beside St. Joseph Catholic Church Anyigba, Kogi State",
    "CELLPHONE": 7062141508,
    "LATITUDE": 7.37058,
    "LONGITUDE": 7.6253
  },
  {
    "AgentName": "Ekwueme Henry Ejike",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Amuwo Odofin",
    "ADDRESS": "Hp12, Harieth Plaza, Alaba Intl, Lagos State",
    "CELLPHONE": 8066663376,
    "LATITUDE": 6.45934,
    "LONGITUDE": 3.21585
  },
  {
    "AgentName": "Eyanuku Gabriel Omojefe",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Eti-Osa",
    "ADDRESS": "Gop Mtn Plaza, Lagos State",
    "CELLPHONE": 8138042513,
    "LATITUDE": 3.427042,
    "LONGITUDE": 6.443689
  },
  {
    "AgentName": "Godwin Juliet",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Oshodi Isolo",
    "ADDRESS": "40 Ago Palace Way, Lagos State",
    "CELLPHONE": 9035481144,
    "LATITUDE": 6.48081,
    "LONGITUDE": 3.30655
  },
  {
    "AgentName": "Jonah Kehinde",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Alimosho",
    "ADDRESS": "12 Aje Street Off Pleasure Bus Stop Oke Odo, Lagos State",
    "CELLPHONE": 8133808351,
    "LATITUDE": 6.496471,
    "LONGITUDE": 3.34577
  },
  {
    "AgentName": "Jonah Kehinde",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Alimosho",
    "ADDRESS": "54 Victor Fagbemi Road Aboru Iyana Ipaja, Lagos State",
    "CELLPHONE": 8101219117,
    "LATITUDE": 6.49877,
    "LONGITUDE": 3.54667
  },
  {
    "AgentName": "Nwagwu Stanley Nwagwu",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Ajeromi Ifelodun",
    "ADDRESS": "16 Gaskiya Road, Ijora, Lagos State",
    "CELLPHONE": 8109670647,
    "LATITUDE": 6.46653,
    "LONGITUDE": 3.35923
  },
  {
    "AgentName": "Ogbu Chidiebere",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Amuwo Odofin",
    "ADDRESS": "C/A21, Alaba Intl Market, Lagos State",
    "CELLPHONE": 7039734071,
    "LATITUDE": 6.45905,
    "LONGITUDE": 3.30409
  },
  {
    "AgentName": "Oguname Favour Ochuko",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Surulere",
    "ADDRESS": "64, Ojuelegba Road, Ojuelegba, Lagos State",
    "CELLPHONE": 8105376624,
    "LATITUDE": 6.496355,
    "LONGITUDE": 3.32577
  },
  {
    "AgentName": "Olawale Olaitan",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Ojo",
    "ADDRESS": "Alaba Rago Along Badagry Expressway Okoko, Lagos State",
    "CELLPHONE": 7063754250,
    "LATITUDE": 6.46083,
    "LONGITUDE": 3.19155
  },
  {
    "AgentName": "Opara Ifeanyi Maxwell",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Oshodi Isolo",
    "ADDRESS": "54 Alor Street. Ago Palace Way , Lagos State",
    "CELLPHONE": 8109649821,
    "LATITUDE": 6.50293,
    "LONGITUDE": 3.30554
  },
  {
    "AgentName": "Suleimon Rasak",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Amuwo Odofin",
    "ADDRESS": "321 Road Junction Festac, Lagos State",
    "CELLPHONE": 8142836309,
    "LATITUDE": 6.45886,
    "LONGITUDE": 3.30359
  },
  {
    "AgentName": "Usoroh Gift",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Amuwo Odofin",
    "ADDRESS": "Alakija Bus Stop, Lagos State",
    "CELLPHONE": 8063289725,
    "LATITUDE": 6.46029,
    "LONGITUDE": 3.30119
  },
  {
    "AgentName": "Ahamed Lawan Bala",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Fagge",
    "ADDRESS": "No 142 Ammani Street Fagge A, Kano",
    "CELLPHONE": 9032539799,
    "LATITUDE": 8.5377,
    "LONGITUDE": 11.9998
  },
  {
    "AgentName": "Okechi Chisom Emmanuella",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Lagos Island",
    "ADDRESS": "45 Oduselu Street Lagos Street",
    "CELLPHONE": 8145575230,
    "LATITUDE": 6.5063,
    "LONGITUDE": 3.32978
  },
  {
    "AgentName": "Adekeye Olajire Moses",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Surulere",
    "ADDRESS": "1B Akerele Extension Beside Post Office Surulere, Lagos State",
    "CELLPHONE": 8102635411,
    "LATITUDE": 6.456338,
    "LONGITUDE": 3.346559
  },
  {
    "AgentName": "Opeyemi Olawale",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Lagos Island",
    "ADDRESS": "12 Martins Street, Lagos State",
    "CELLPHONE": 7033353047,
    "LATITUDE": 6.45633,
    "LONGITUDE": 3.386
  },
  {
    "AgentName": "Okeke Oluchi Chibuzo",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Oshodi-Isolo",
    "ADDRESS": "Shop 11 Modupe Shopping Complex, Jakande Gate Isolo, Lagos State",
    "CELLPHONE": 8161166197,
    "LATITUDE": 6.324266,
    "LONGITUDE": 5.613431
  },
  {
    "AgentName": "Osewehereni Ann Chioma",
    "TYPE": "Retail Agent",
    "STATE": "Lagos",
    "LGA": "Ikorodu",
    "ADDRESS": "1 Banjoko Olowu Street Ikorodu, Lagos State",
    "CELLPHONE": 9065771223,
    "LATITUDE": 6.618,
    "LONGITUDE": 3.50134
  },
  {
    "AgentName": "Kamal Nuhu",
    "TYPE": "Retail Agent",
    "STATE": "Niger",
    "LGA": "Magama",
    "ADDRESS": "Dadin Kowa By Medengen Petrol Station , Salka, Niger State",
    "CELLPHONE": 9038361628,
    "LATITUDE": 5.46568,
    "LONGITUDE": 10.38636
  },
  {
    "AgentName": "Adeolu Akangbe",
    "TYPE": "Retail Agent",
    "STATE": "Ogun",
    "LGA": "Ado Odo Ota",
    "ADDRESS": "Lagos Abeokuta Express Way, Sango, Ogun State",
    "CELLPHONE": 8147996711,
    "LATITUDE": 6.70418492666998,
    "LONGITUDE": 3.24322119355201
  },
  {
    "AgentName": "David Ajetumobi",
    "TYPE": "Retail Agent",
    "STATE": "Ogun",
    "LGA": "Ado Odo Ota",
    "ADDRESS": "20, Ijoko Road, Sango Ota, Ogun State",
    "CELLPHONE": 8144837670,
    "LATITUDE": 7.94783,
    "LONGITUDE": 4.78836
  },
  {
    "AgentName": "Aremola Abiodun",
    "TYPE": "Retail Agent",
    "STATE": "Ondo",
    "LGA": "Ondo West",
    "ADDRESS": "Okelisa Garage, Okelisa, Ondo State",
    "CELLPHONE": 9036315496,
    "LATITUDE": 6.7241,
    "LONGITUDE": 3.93705
  },
  {
    "AgentName": "Ajibola Musiliyu",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Ifelodun",
    "ADDRESS": "Opp. Nipost, Ikirun., Osun State",
    "CELLPHONE": 8068584554,
    "LATITUDE": 7.91667,
    "LONGITUDE": 4.66667
  },
  {
    "AgentName": "Akanji Ganiyu",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Iwo",
    "ADDRESS": "155, Bowen University Road, Oke-Odo, Iwo, Osun State",
    "CELLPHONE": 7034475036,
    "LATITUDE": 7.63527,
    "LONGITUDE": 4.18156
  },
  {
    "AgentName": "Asifat Hammed Olaitan",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Atakumasa East",
    "ADDRESS": "Serafu Junction Near Owo Tutu Central Mosque, Ifon, Osun State",
    "CELLPHONE": 9034545486,
    "LATITUDE": 7.85992,
    "LONGITUDE": 4.47621
  },
  {
    "AgentName": "Awe Oluwatosin Rhoda",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Ife Central",
    "ADDRESS": "47, Oluorogbo Road 7, Ile-Ife, Osun State",
    "CELLPHONE": 9037933556,
    "LATITUDE": 4.55625,
    "LONGITUDE": 7.48412
  },
  {
    "AgentName": "Bada Olawale  Adeyolemi",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Ila",
    "ADDRESS": "Ita Iperin Area, Ila, Osun State",
    "CELLPHONE": 8140127235,
    "LATITUDE": 8.01714,
    "LONGITUDE": 4.90421
  },
  {
    "AgentName": "Ganiyu Taiwo Oluwaseun",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Ife Central",
    "ADDRESS": "Jesu Seun Shopping Complex,Fajuyi Road,Ife, Osun State",
    "CELLPHONE": 8038994244,
    "LATITUDE": 7.781404,
    "LONGITUDE": 4.556105
  },
  {
    "AgentName": "Lamidi Sekooni  Adewolu",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Atakumasa East",
    "ADDRESS": "Ilesa Road,Osu, Osun State",
    "CELLPHONE": 9034834127,
    "LATITUDE": 7.58585,
    "LONGITUDE": 4.6226
  },
  {
    "AgentName": "Lawal Taiwo Damilola",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Irewole",
    "ADDRESS": "Oju-Oja Area,Ikire, Osun State",
    "CELLPHONE": 9066447171,
    "LATITUDE": 7.7,
    "LONGITUDE": 4.416667
  },
  {
    "AgentName": "Moronkola Taofeek Olaoye",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Aiyedaade",
    "ADDRESS": "25, Old Inisha Raod Opposite Skye Bank , Osun State",
    "CELLPHONE": 8147186637,
    "LATITUDE": 7.541105,
    "LONGITUDE": 4.511142
  },
  {
    "AgentName": "Mustapha Sakiru Taiwo",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Ife Central",
    "ADDRESS": "1,Oriowo Street,Ife City,Ile-Ife, Osun State",
    "CELLPHONE": 9037210669,
    "LATITUDE": 4.55628,
    "LONGITUDE": 7.48412
  },
  {
    "AgentName": "Yekeen Akeem Bello",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Olorunda",
    "ADDRESS": "Ota-Efun Market, Osogbo, Osun State",
    "CELLPHONE": 7063590354,
    "LATITUDE": 7.713417,
    "LONGITUDE": 4.489394
  },
  {
    "AgentName": "Yekinni Samson Omotayo",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Osogbo",
    "ADDRESS": "No 9, Igbalaye Street, Osogbo., Osun State",
    "CELLPHONE": 7063380724,
    "LATITUDE": 7.76667,
    "LONGITUDE": 4.56667
  },
  {
    "AgentName": "Taiwo Abayomi",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Osogbo",
    "ADDRESS": "Infront Of Ataoja, Palace, Osogbo, Osun State",
    "CELLPHONE": 9037708087,
    "LATITUDE": 7.756935,
    "LONGITUDE": 4.577226
  },
  {
    "AgentName": "Ojuolape Kabeer",
    "TYPE": "Retail Agent",
    "STATE": "Osun",
    "LGA": "Osogbo",
    "ADDRESS": "Aresa Street, Ita-Olokan, Osogbo, Osun State",
    "CELLPHONE": 7033354136,
    "LATITUDE": 7.756935,
    "LONGITUDE": 4.577226
  },
  {
    "AgentName": "Akinseinde Owolabi Micheal",
    "TYPE": "Retail Agent",
    "STATE": "Oyo",
    "LGA": "Lagelu",
    "ADDRESS": "5, Lexyland Trade Center, Along Akobo Road, General Gas Ibadan, Oyo State",
    "CELLPHONE": 7063119345,
    "LATITUDE": 3.8243472,
    "LONGITUDE": 7.3861816
  },
  {
    "AgentName": "Obiefule Chike",
    "TYPE": "Retail Agent",
    "STATE": "Oyo",
    "LGA": "Ibadan North",
    "ADDRESS": "Civil Engineering Department, University Of Ibadan , Oyo State",
    "CELLPHONE": 9036769883,
    "LATITUDE": 17.727154,
    "LONGITUDE": 83.319952
  },
  {
    "AgentName": "Ojedokun Olawale",
    "TYPE": "Retail Agent",
    "STATE": "Oyo",
    "LGA": "Ibsw",
    "ADDRESS": "Behind Ile Epo Gege, Ibadan, Oyo State",
    "CELLPHONE": 8034664188,
    "LATITUDE": 3.8895503,
    "LONGITUDE": 7.3743366
  },
  {
    "AgentName": "Ahmed Ibrahim",
    "TYPE": "Retail Agent",
    "STATE": "Plateau",
    "LGA": "Barkin Ladi",
    "ADDRESS": "Barkin Ladi Main Market, Plateau State",
    "CELLPHONE": 9069027071,
    "LATITUDE": 8.89383,
    "LONGITUDE": 9.54089
  },
  {
    "AgentName": "Mamuda Adamu",
    "TYPE": "Retail Agent",
    "STATE": "Plateau",
    "LGA": "Pankshin",
    "ADDRESS": "Double Across Pankshin, Plateau State",
    "CELLPHONE": 8039254909,
    "LATITUDE": 9.42159,
    "LONGITUDE": 9.32843
  },
  {
    "AgentName": "Samuel Friday Nankpak",
    "TYPE": "Retail Agent",
    "STATE": "Plateau",
    "LGA": "Shendam",
    "ADDRESS": "Angwan Yelwa, Shedam, Plateau State",
    "CELLPHONE": 8063234981,
    "LATITUDE": 9.81538,
    "LONGITUDE": 9.138789
  },
  {
    "AgentName": "Uthman Muktar Muhammad",
    "TYPE": "Retail Agent",
    "STATE": "Plateau",
    "LGA": "Barkin Ladi",
    "ADDRESS": "Hospital Junction Barkin Ladi, Plateau State",
    "CELLPHONE": 8066674333,
    "LATITUDE": 8.89383,
    "LONGITUDE": 9.54089
  },
  {
    "AgentName": "Ihuoma Chukwuma Marcellinus",
    "TYPE": "Retail Agent",
    "STATE": "Rivers",
    "LGA": "Akuku Toru",
    "ADDRESS": "Market Quare Abonnema, River State",
    "CELLPHONE": 8104822643,
    "LATITUDE": 6.506872,
    "LONGITUDE": 7.40602
  },
  {
    "AgentName": "Ike Precious",
    "TYPE": "Retail Agent",
    "STATE": "Rivers",
    "LGA": "Obio Akpor",
    "ADDRESS": "Liberty Plaza, 50 Airport Road Rumuodumaya, River State",
    "CELLPHONE": 8039160036,
    "LATITUDE": 7.00276,
    "LONGITUDE": 4.88148
  },
  {
    "AgentName": "Okeke Jelle",
    "TYPE": "Retail Agent",
    "STATE": "Rivers",
    "LGA": "Asari Toru",
    "ADDRESS": "No 2 Asari  Toru Head Quarter Buguma, River State",
    "CELLPHONE": 9067626577,
    "LATITUDE": 7.0118,
    "LONGITUDE": 4.80219
  },
  {
    "AgentName": "Okere Ogechukwu",
    "TYPE": "Retail Agent",
    "STATE": "Rivers",
    "LGA": "Obio Akpor",
    "ADDRESS": "School Road Junction Off Elelenwo, River State",
    "CELLPHONE": 9060749758,
    "LATITUDE": 7.058167,
    "LONGITUDE": 4.839449
  },
  {
    "AgentName": "Gidado Salim",
    "TYPE": "Retail Agent",
    "STATE": "Sokoto",
    "LGA": "Kware",
    "ADDRESS": "Moreh Area Sokoto, Sokoto State",
    "CELLPHONE": 8066759834,
    "LATITUDE": 13.149238,
    "LONGITUDE": 5.298635
  },
  {
    "AgentName": "Michael  Mercy Ojali",
    "TYPE": "Retail Agent",
    "STATE": "Sokoto",
    "LGA": "Sokoto",
    "ADDRESS": "No 42 Line E Gsm Village, Sokoto State",
    "CELLPHONE": 9068314374,
    "LATITUDE": 5.224325,
    "LONGITUDE": 13.075015
  },
  {
    "AgentName": "Hassan Usman",
    "TYPE": "Retail Agent",
    "STATE": "Sokoto",
    "LGA": "Sokoto",
    "ADDRESS": "Opposite Uduth Garba Nadama Road Sokoto, Sokoto State",
    "CELLPHONE": 9063177463,
    "LATITUDE": 5.218237,
    "LONGITUDE": 13.056398
  },
  {
    "AgentName": "Alugbue Chibuke",
    "TYPE": "Retail Agent",
    "STATE": "Yobe",
    "LGA": "Potiskum",
    "ADDRESS": "Lawyer Barde Street Opp Fcet Potiskum, Yobe State",
    "CELLPHONE": 8145586047,
    "LATITUDE": 53.20033,
    "LONGITUDE": -2.76236
  },
  {
    "AgentName": "Idriss Mohammed Mohammed",
    "TYPE": "Retail Agent",
    "STATE": "Yobe",
    "LGA": "Damaturu",
    "ADDRESS": "Gujba Road Opp.Zango Junction Damatturu, Yobe State",
    "CELLPHONE": 8145466404,
    "LATITUDE": 9.03426,
    "LONGITUDE": 7.47915
  },
  {
    "AgentName": "Mohammed Yusuf Yusuf",
    "TYPE": "Retail Agent",
    "STATE": "Yobe",
    "LGA": "Potiskum",
    "ADDRESS": "Socal Opp Rauda Filing Station, Yobe State",
    "CELLPHONE": 8132050880,
    "LATITUDE": 11.28498,
    "LONGITUDE": 11.28498
  },
  {
    "AgentName": "Yahaya Hassan",
    "TYPE": "Retail Agent",
    "STATE": "Yobe",
    "LGA": "Potiskum",
    "ADDRESS": "Yindiski Ward Pkm, Yobe State",
    "CELLPHONE": 7033469901,
    "LATITUDE": 11.7091,
    "LONGITUDE": 11.0694
  },
  {
    "AgentName": "Abubakar Mannir",
    "TYPE": "Retail Agent",
    "STATE": "Zamfara",
    "LGA": "Gusau",
    "ADDRESS": "Tullukawa Area, Gusau, Zamfara State",
    "CELLPHONE": 8038952046,
    "LATITUDE": 12.16889,
    "LONGITUDE": 6.66117
  },
  {
    "AgentName": "Abubakar Mannir",
    "TYPE": "Retail Agent",
    "STATE": "Zamfara",
    "LGA": "Gusau",
    "ADDRESS": "Bakin Tsohon Kasuwa, Gusau, Zamfara State",
    "CELLPHONE": 8132374635,
    "LATITUDE": 10.40038,
    "LONGITUDE": 12.00904
  },
  {
    "AgentName": "Adeniji Adedeji",
    "TYPE": "Retail Agent",
    "STATE": "Zamfara",
    "LGA": "Gusau",
    "ADDRESS": "Shop 2 Kortokoshi Street Tudunwada, Zamfara State",
    "CELLPHONE": 8038236617,
    "LATITUDE": 11.20461,
    "LONGITUDE": 7.28953
  },
  {
    "AgentName": "Agbo Ngozi",
    "TYPE": "Retail Agent",
    "STATE": "Zamfara",
    "LGA": "Gusau",
    "ADDRESS": "Beside Living Faith Church Hayin Buba Area,Gusau, Zamfara State",
    "CELLPHONE": 8165639877,
    "LATITUDE": 12.17372,
    "LONGITUDE": 6.68659
  },
  {
    "AgentName": "Musa Saidu",
    "TYPE": "Retail Agent",
    "STATE": "Zamfara",
    "LGA": "Gusau",
    "ADDRESS": "Unguwan Gwaza, Gusau, Zamfara State",
    "CELLPHONE": 8032140521,
    "LATITUDE": 11.94137,
    "LONGITUDE": 8.45943
  },
  {
    "AgentName": "Temenu Emmanuel",
    "TYPE": "Retail Agent",
    "STATE": "Zamfara",
    "LGA": "Gusau",
    "ADDRESS": "No. 32 Tashan Magami, Gusau, Zamfara State",
    "CELLPHONE": 9039696362,
    "LATITUDE": 31.95,
    "LONGITUDE": 49.5
  }
]
  
}