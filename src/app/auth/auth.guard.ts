import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { StoreService } from '../plugins/datastore';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private storeVal: StoreService) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let sessionId = this.storeVal.getSessionId()

     if (sessionId && this.storeVal.getToken() != null) {
      this.setPathAsPerCond(state.url);
      return true;
    } else {
      this.setMTNOnlineProductId(next);
      if (state.url == '/login') {
        return true;
      }
      else {
        this.setMTNOnlineProductId(next);
        this.router.navigateByUrl('');
        return true;
      }
    }
    // return true;
  }

  setPathAsPerCond(url) {
    switch (url) {
      case '/login': {
        this.router.navigateByUrl('/dashboard');
        break;
      }
    }
  }

   setMTNOnlineProductId(activatedUrl) {
    let url = activatedUrl.routeConfig.path;
    if (url == 'buybundles/procced/:productId') {
      let MTNOnlineProductId = activatedUrl.params.productId;
      if (MTNOnlineProductId) {
        this.storeVal.setMTNOnlineProductId(MTNOnlineProductId);
      }
    }
  }
}
