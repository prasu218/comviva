import * as menubar from '../actions/menubar';
export interface State {
    openMenubar: boolean;
}

export const initialState = {
    openMenubar: false
};

export function reducer(state = initialState, action: menubar.Actions) {
    switch (action.type) {
        case menubar.OPEN_MENUBAR:
        return {
            openMenubar : true
        };
        case menubar.CLOSE_MENUBAR:
        return {
            openMenubar : false
        };
        default :
        return state;
    }
}

export const getOpenMenuBar = (state: State) => state.openMenubar;
