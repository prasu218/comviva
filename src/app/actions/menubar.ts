import { Action } from '@ngrx/store';

export const OPEN_MENUBAR = '[openMenuBar] Open_MenuBar';
export const CLOSE_MENUBAR = '[openMenuBar] Close_MenuBar';

export class OpenMenuBarAction implements Action {
    readonly type = OPEN_MENUBAR;
}

export class CloseMenuBarAction implements Action {
    readonly type = CLOSE_MENUBAR;
}

export type Actions =
      OpenMenuBarAction
    | CloseMenuBarAction;

