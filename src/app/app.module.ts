import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './core/app-routing.module';
import { AppComponent } from './core/app.component';
// import { PluginsModule } from './plugins/plugins.module';
import { ThemeModule } from './theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { reducer, reducers } from './reducers';
 import { StoreModule } from '@ngrx/store';
 import { StoreRouterConnectingModule } from '@ngrx/router-store';
 import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { HttpModule } from '@angular/http';
import { HttpClientModule }    from '@angular/common/http';
import { MainpagesModule } from './mainpages/mainpages.module';
import { PluginsModule } from './plugins/plugins.module';
 import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 import { otpService } from './plugins/otp';
import { BnNgIdleService } from 'bn-ng-idle';
import { FeedbackComponent } from './mainpages/feedback/feedback.component';
import { UnlinkConfirmationComponent } from './mainpages/unlink-confirmation/unlink-confirmation.component';
import { ScrolltopComponent } from './theme/component/scrolltop/scrolltop.component';

@NgModule({
  declarations: [
    AppComponent,
    UnlinkConfirmationComponent,
    ScrolltopComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    AppRoutingModule,
   // PluginsModule,
    ThemeModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MainpagesModule,
    HttpClientModule,
    PluginsModule.forRoot(),
	BrowserAnimationsModule,
	//SubscriptionService,
     StoreModule.forRoot(reducers),
    StoreRouterConnectingModule.forRoot(),
    StoreDevtoolsModule.instrument(),
	

  ],
  providers: [otpService,BnNgIdleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
