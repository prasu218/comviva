import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { StoreService } from '../../plugins/datastore';
import { Location } from '@angular/common';

@Component({
  selector: 'app-failure',
  templateUrl: './failure.component.html',
  styleUrls: ['./failure.component.scss']
})
export class FailureComponent implements OnInit {

  message:'Recharge Failed';
  header = true;
 msisdnval:any;


  @Input() getInternalCardDetails ;
  @Input() public masisdnpayment;

  constructor(private router: Router,private _storeVal: StoreService,private location: Location) { }

  ngOnInit() {
       this.msisdnval = this._storeVal.getMSISDNData();
  }
  
  backbuttons(){
      this.router.navigate(['recharge']);
   }

}
