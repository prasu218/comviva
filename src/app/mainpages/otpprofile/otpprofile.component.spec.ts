import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtpprofileComponent } from './otpprofile.component';

describe('OtpprofileComponent', () => {
  let component: OtpprofileComponent;
  let fixture: ComponentFixture<OtpprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtpprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtpprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
