import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
//  import { format } from '../../utils/formatter';
import { Router, ActivatedRoute } from '@angular/router';
import { config } from '../../config';
import { format } from '../../utils/formatter';
import { LoginForm } from '../../utils/interface';
import { ProfileService } from '../../plugins/profile';
import { otpService } from '../../plugins/otp';
import { StoreService } from '../../plugins/datastore';
import { BnNgIdleService } from 'bn-ng-idle';
import { NotificationdbService } from '../../plugins/notificationdb';
import { SessionService } from 'src/app/plugins/session';
// import { Observable } from 'rxjs/';
import { AngularCDRService } from '../../plugins/angularCDR';
import { Location } from '@angular/common';
import { CryptoService } from 'src/app/plugins/crypto';

@Component({
  selector: 'app-otpprofile',
  templateUrl: './otpprofile.component.html',
  styleUrls: ['./otpprofile.component.scss']
})
export class OtpprofileComponent implements OnInit {
  bgColorpp = 'grey';
  selctradio: boolean = false;
  // existing_cust: FormGroup;
  True: boolean = true;
  public existing_cust: FormGroup;
  public otp_cust: FormGroup;
  public hideee: boolean = false;
  public existing_MTN: FormGroup;
  public OTPShow: boolean = true;
  public sendotp: boolean = true;
  // public msisdn:any;
  public emailFlag: boolean = false;
  public errorMsg = '';
  email: string;
  alternateNumber: number;
  primaryNumber: number;
  showLoader: boolean = false;
  public message: string;

  otp_radio: string;
  fmisidn: any;
  phonenum: string;
  header = true;
  otpValue: string;
  valiotp: string;
  timeoutErr = ''
  startTime: any;
  category: any = 'OTP';
  //value="";
  checktOtp: true;
  sessionErr: string = '';
  msisdnval: any;
protectedMsisdn: any;
protectedMail: any;

  responseOTP: boolean = false;
linkAccountFlag:boolean = false;
  constructor(private fb: FormBuilder, private notifiservice: NotificationdbService,
              private _profile: ProfileService, private _otp: otpService, private _storeVal: StoreService, 
              private router: Router, private bnIdle: BnNgIdleService, private _cdr: AngularCDRService, 
              private sessionService: SessionService,private location: Location,private cryptoService : CryptoService) { }

  ngOnInit() {
    if (this._storeVal.getMSISDNData() && this._storeVal.getMSISDNData()!= "") {
      this.msisdnval = this._storeVal.getMSISDNData();
      this.email = this._storeVal.getEmailData();
      this.sendotpClick(this.msisdnval, this.email);
    } else {
      this.router.navigate(['login']);
    }

    this._storeVal.setSecondaryAccountfalse(true);
    this.existing_cust = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(13)]],
    });
    this.otp_cust = this.fb.group({
      enterotp: ['', [Validators.required, Validators.minLength(6)]],
    });

    this.displaySessionErr();
this.protectedMsisdn = this.maskedNumber(this.msisdnval, 8, 13, '*');
    this.protectedMail = this.maskedEmail(this.email, 2, 9, '*');
  }

  // canActivate (next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
  //   const validId = this.validateOtp()
  //   if (validId) {
  //     return true;
  //   } else {
  //     // console.log("Inside the session else:::", state.url);
  //     if (state.url == '/otpprofile') {
  //       return true;
  //     } else {
  //       // console.log("Inside the session 2nd else:::", state.url);
  //       this.router.navigate(['/otpprofile']);
  //       return true;
  //     }
  //   }
  // }
maskedNumber(str: string, startIndex: any, endIndex: any, char: string) {
    if(str == null){
      return str;
    }
    startIndex = 6;
    endIndex = 11;
    char = '*';
    const characters = str.split('');
    for (let i = startIndex; i < endIndex; i++) {
      characters[i] = char;
    }
    return characters.join('');
  }

  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }
   
  maskedEmail(str: string, startIndex: any, endIndex: any, char: string) {
    if(str == null){
      return str;
    }
    startIndex = 3;
    endIndex = 9;
    char = '*';
    const characters = str.split('');
    for (let i = startIndex; i < endIndex && i < characters.length; i++) {
      characters[i] = char;
    }
    return characters.join('');
  }
  displaySessionErr() {
    let sessionStatus = this._storeVal.getSessionStatus();
    if (sessionStatus == 'expired') {
      this.sessionErr = 'Session Time out. To keep you safe, you have been logged out due to inactivity. Please login again';
      localStorage.removeItem('session_status');
    }
  }
  numberOnly(event): boolean {
    if (this.timeoutErr) {
      this.timeoutErr = '';
    }
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length >= 13 && (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length >= 13) {
      return false;
    } else {
      return true;
    }
  }

  numberOnlyotp(event): boolean {
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length >= 6 && (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length >= 6) {
      return false;
    } else {
      return true;
    }
  }


  checkEmail(value) {
    this.emailFlag = value;
  }

  sendotpClick(msisdn, email) {
    this.timeoutErr = '';
    this.showLoader = true;
    this.bnIdle.startWatching(3000).subscribe((res) => {
      this.checktOtp = true;
      this.bnIdle.stopTimer();
    })
    if (email) {
      this.otp_radio = msisdn.toString();
    }
    this._otp.generateOTP(this.otp_radio, email,this.linkAccountFlag).then((resp) => {
      this.valiotp = resp.otp;
      if (resp.status == 0) {
        // this.sendotp = false;
        // this.OTPShow = false;
        this._storeVal.setOtpData(resp.otp)
        this.otp_cust.reset();
        this.showLoader = false;
        this._cdr.writeCDR(this.startTime, this.category, this.otpValue, 'OTP', 'Success');//cdr_log
      }

    }).catch((err) => {
      if (err.message == 'Timeout has occurred') {
        this.sendotp = false;
        this.OTPShow = false;
        this.showLoader = false;
        this.timeoutErr = 'Timeout has occurred, Please try again';
        this._cdr.writeCDR(this.startTime, this.category, this.otpValue, 'OTP', 'Failure');//cdr_log
      }
      this.otp_cust.reset();
    });
  }


  validateOtp() {
    // this.fmisidn = format.msisdn(this.existing_cust.get('msisdn').value)
    this.timeoutErr = '';
    if (!this.checktOtp) {
      this.showLoader = true;
      this.otpValue = this.otp_cust.value.enterotp;
      let msisdn = this._storeVal.getMSISDNData();
      let encryptedData =  this.cryptoService.encryptData(this.otpValue);
      this._otp.validateOTP(encryptedData,this._storeVal.getOtpData(), msisdn).then(resp => {
      this._cdr.writeCDR(this.startTime, this.category, this.otpValue, 'OTP', 'Failure'); //cdr_log
      if (resp.status == 0) {
       // console.log('otp success');
        // this.showLoader = false;
        this.bnIdle.startWatching(1800).subscribe((res) => {
          if (res) {
            this.showLoader = false;
            this.router.navigate(['/login']);
            this.bnIdle.stopTimer();
          }
        });
        if (this._storeVal.getSessionId() !='' && this._storeVal.getMSISDNData() !=''){
          const responseOTP = { validated : 'validated' };
          this.router.navigate(['detailsofprofile'], { state: responseOTP });
        }
      } else {
       // console.log('otp failure');
        this.showLoader = false;
        this.message = resp.Valid;
        this.otp_cust.reset();
      }
      });
    } else {
    // console.log('otp error');
      this.timeoutErr = 'Timeout has occurred, Please click on to Resend OTP';
    }
  }

  backout(){
    this.router.navigate(['dashboard'])
  }

  backtologin1() {
    this.OTPShow = true;
  }
  backtologin2() {
    this.sendotp = true;
  }


  backToLogin() {
    this.errorMsg = '';
    // this.category = 'Login';
    // this.hide_OTP = true;
  }


  resendotpClick() {
this.validateOtp();
    this.hideee = true;
    this.sendotp = true;
  }

  onSubmit(value: FormGroup) {
    this.showLoader = true;
    this.sessionErr = '';
    // const msisdn = this.existing_cust.get('msisdn').value;
    let msisdn = format.msisdn(this.existing_cust.get('msisdn').value);
    if (msisdn.length < 9) {
      this.showLoader = false;
      this.errorMsg = 'Please enter a valid MTN number.';
      this.existing_cust.reset();
    }
    this._profile.getProfileDetails(msisdn).then((resp) => {
      if (resp.status_code == 0) {

        //  this._storeVal.setEmailData(resp.customer_Profile.email);
        // this._storeVal.setAlternateNumberData(resp.customer_Profile.alternateNumber);
        // console.log('this._storeVal.setEmailData-' + resp.customer_Profile.email);
        // console.log('resp---' + JSON.stringify(resp));
        this._storeVal.setMSISDNData(msisdn);
        this._storeVal.setEmailData(resp.customer_Profile.email);
        this._storeVal.setAlternateNumberData(resp.customer_Profile.alternateNumber);
        this._storeVal.setFamilyNameData(resp.customer_Profile.familyName);
        this._storeVal.setFirstNameData(resp.customer_Profile.firstName);
        this._storeVal.setSubsTypeData(resp.subsType);
        this._storeVal.setActivationDate(resp.customer_Profile.activationDate);

        this.email = resp.customer_Profile.email;
        this.primaryNumber = resp.customer_Profile.primaryNumber;
        this._storeVal.setPrimaryMSISDNData(this._storeVal.getMSISDNData());
        this._storeVal.setPrimaryEmailData(this._storeVal.getEmailData());
        this._storeVal.setPrimaryAlternateNumberData(this._storeVal.getAlternateNumberData());
        this._storeVal.setPrimaryFamilyNameData(this._storeVal.getFamilyNameData());
        this._storeVal.setPrimaryFirstNameData(this._storeVal.getFirstNameData());
        this._storeVal.setPrimarySubsTypeData(this._storeVal.getSubsTypeData());

        if (resp.customer_Profile.primaryNumber != resp.customer_Profile.alternateNumber) {
          this.alternateNumber = resp.customer_Profile.alternateNumber;

        }
        this.otp_radio = this.primaryNumber.toString();

        this.OTPShow = false;
        this.showLoader = false;
        //  this.router.navigate(['dashboard'] ).then( nav => {}, (err) => {

        //       this.errorMsg = 'Please try again after sometime';
        //       this.existing_cust.reset();
        //     });
      }
      else {
        this.showLoader = false;
        this.errorMsg = 'Please Enter a valid MTN Number';
        this.existing_cust.reset();
      }
    }).catch((err) => {
      this.showLoader = false;
      this.errorMsg = 'Please Enter a valid MTN Number';
      this.existing_cust.reset();
    });
  }
}
