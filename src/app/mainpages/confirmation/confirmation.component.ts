import { Component, OnInit, Input } from '@angular/core';

// import { bundleMasterConfig } from '../../../app/config/config';
// import { bundleMasterConfig } from '../../../../app/config/config';
// import { config } from '../../../config';
declare var $: any;
import { Router, ActivatedRoute } from '@angular/router';
import { DbCallsService } from 'src/app/plugins/dbCalls';

import { SessionService } from 'src/app/plugins/session';
import { AngularCDRService } from 'src/app/plugins/angularCDR';
import { ProfileService } from 'src/app/plugins/profile';
import { StoreService } from 'src/app/plugins/datastore';
import { BuyBundlesService } from 'src/app/plugins/buybundles';
import { loginrcgService } from 'src/app/plugins/loginrcg';
import { BnNgIdleService } from 'bn-ng-idle';
import { map } from 'rxjs/operators';
import { AddAccountService } from 'src/app/plugins/addaccount';
import { Location } from '@angular/common';
import { ApiService } from 'src/app/plugins/commonAPI';
import { otpService } from 'src/app/plugins/otp';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {
  airttt: boolean = true;
  airttt1: boolean = false;
  showsuccessdiv: Boolean = true;
  failurDiv: Boolean = false;
  suceesDiv: Boolean = false;
  alternateNumber: number;
  primaryNumber: number;
  email: string;
  header=true;

  public errorMsg = '';
  showconfimdiv: Boolean = false;



  public colorpayment: boolean = false;
  showLoader: Boolean = false;
  message = '';
   message1 = '';  




 numberToSwitch: any;
 nickname: any;
 switchCondition: boolean = false;
 parentNumber: any;
 switchedPage: boolean;

 unlinkNumber: any;
 unlinkCondition: boolean = false;
 unlinkName: any;

  constructor(private _profile: ProfileService, private _storeVal: StoreService, private router: Router, 
     private _cdr: AngularCDRService, private sessionService: SessionService, private otpMessage: otpService,
      private activatedRoute: ActivatedRoute, private addAccount: AddAccountService,
     private loginService: loginrcgService, private bnIdle: BnNgIdleService,private location: Location, private apiService: ApiService) {
//  { switchNumber: "9062058584", parentNumber: "2349062058457", nickname: "Emma" }
      let params = this.router.getCurrentNavigation().extras.state

      //switching Params
      if(params.switchCondition){
        this.switchCondition  = params.switchCondition;
        this.numberToSwitch = params.switchNumber;
        this.nickname = params.nickname;
        this.parentNumber = params.parentNumber
      }
   
      //unlinking Params
      if(params.unlinkCondition){
        this.unlinkNumber = params.unlinkNumber;
        this.unlinkCondition = params.unlinkCondition;
        this.unlinkName = params.unlinkName;
      }
    

      //console.log("Abeggi this is the params", params);

      // this.activatedRoute.paramMap.pipe(map(()=>{ 
      //   console.log("Homecard state gotten here from homecard", window.history.state) }))
      }
      // data => {
      //   this.numberToSwitch = data.get("switchNumber");
      //   this.parentNumber = data.get("parentNumber");
      

  // @Input() paymentuipagemigerate : boolean = false;

  ngOnInit() {
   

    	 $(window).resize(function(){
		const header_height = document.getElementById('header-main-mobile').clientHeight;
	//	console.log('Header Height - ' + header_height);
		document.getElementById('main-header-row-div').style.marginBottom = header_height.toString() + "px";
  });
	  const header_height = document.getElementById('header-main-mobile').clientHeight;
    //console.log('Header Height - ' + header_height);
    document.getElementById('main-header-row-div').style.marginBottom = header_height.toString() + "px";
	

  }





  backout(){
  this.location.back();
  // this.router.navigate(['dashboard']);

}


  changeStatus(index) {
    this.colorpayment = true;

  }

  switchAccount() {

  
    this.showLoader = true;
    //get primary number here before clesring session
    let logoutnumber = '' ;
    let oldNumber = JSON.parse(localStorage.getItem("manna")).SecondaryNumber;
    if(localStorage.getItem("switched") != "true"){
      logoutnumber = oldNumber;
    }else{
      logoutnumber = this.parentNumber;
    }
    let oldToken  ="";
    if(this._storeVal.getToken() != null){
      oldToken = this._storeVal.getToken();
    }else{
      oldToken = '';
    }
    // if(sessionStorage.getItem("chakra") != null){
    //   oldToken = sessionStorage.getItem("chakra")
    // }else{
    //   oldToken = '';
    // }
    let oldtokenmain = '';
    if(this._storeVal.getMainToken() != null){
      oldtokenmain = this._storeVal.getMainToken();
    }else{
      oldtokenmain = '';
    }

    // let oldtokenmain = '';
    // if(sessionStorage.getItem("chakramain") != null){
    //   oldtokenmain = sessionStorage.getItem("chakramain")
    // }else{
    //   oldtokenmain = '';
    // }
   // console.log("Old number here", oldNumber);
      
    // } 
    this.loginService.logout(logoutnumber).then(resp => {
      if (resp.status == 0) {
        sessionStorage.clear();
        localStorage.clear();
        this.createSession(oldNumber, oldToken, oldtokenmain, logoutnumber)
        //  this.bnIdle.stopTimer(); 
      }
    }).catch(err => {
      sessionStorage.clear();
      localStorage.clear();
      this.showLoader = false;
      this.router.navigate(['login'])
      Promise.reject(err);
    });

  }

  backbuttons(){
    this.location.back();
  }


  async createSession(primaryNumber, oldToken, oldtokenmain, logoutnumber){
    let msisdn = this.numberToSwitch;     
      //linkedConfirm;
      if(oldtokenmain.length > 0){
        this._storeVal.setMainToken(oldtokenmain);
       // sessionStorage.setItem("chakramain", oldtokenmain);
      }else{
        this._storeVal.setMainToken(oldToken);
        // sessionStorage.setItem("chakramain", oldToken);
      }
   
      this._storeVal.setToken(oldToken);
     // sessionStorage.setItem("chakra", oldToken);
 
    await this.addAccount.validateSwitchAccount(primaryNumber, msisdn, logoutnumber).then(data => {
      console.log("This is supposed number ", primaryNumber);
      console.log("Bros this switch data ", data);
      if (data && data.StatusCode == 0) {
        if(data.token){
          this._storeVal.setToken(data.token);
          // sessionStorage.setItem("chakra",data.token );
        }
      }
      else {
        localStorage.clear();
        sessionStorage.clear();
        this.showLoader = false;
        this.router.navigate(['login'])
        .then(nav => { }, (err) => {
          this.showLoader = false;
        
        });
      }
    
     });


 
   
    const profileResp = await this._profile.getProfileDetails(msisdn).catch((err) => {
      this.showLoader = false;
      this.errorMsg = 'Please enter a valid MTN number';

    });

   // console.log("Na the response " , profileResp)
    if ( profileResp && profileResp.status_code == 0) {
    

      this._storeVal.setMSISDNData(msisdn);
      this._storeVal.setEmailData(profileResp.customer_Profile.email);
      this._storeVal.setAlternateNumberData(profileResp.customer_Profile.alternateNumber);
      this._storeVal.setFamilyNameData(profileResp.customer_Profile.familyName);
      this._storeVal.setFirstNameData(profileResp.customer_Profile.firstName);
      this._storeVal.setSubsTypeData(profileResp.subsType);
      this._storeVal.setActivationDate(profileResp.customer_Profile.activationDate);

      this.email = profileResp.customer_Profile.email;
      this.primaryNumber = profileResp.customer_Profile.primaryNumber;
      this._storeVal.setPrimaryMSISDNData(this._storeVal.getMSISDNData());
      this._storeVal.setPrimaryEmailData(this._storeVal.getEmailData());
      this._storeVal.setPrimaryAlternateNumberData(this._storeVal.getAlternateNumberData());
      this._storeVal.setPrimaryFamilyNameData(this._storeVal.getFamilyNameData());
      this._storeVal.setPrimaryFirstNameData(this._storeVal.getFirstNameData());
      this._storeVal.setPrimarySubsTypeData(this._storeVal.getSubsTypeData());

      if (profileResp.customer_Profile.primaryNumber != profileResp.customer_Profile.alternateNumber) {
        this.alternateNumber = profileResp.customer_Profile.alternateNumber;
      }
  
      this.showLoader = false;
      //  this.router.navigate(['dashboard'] ).then( nav => {}, (err) => {

      //       this.errorMsg = 'Please try again after sometime';
      //       this.existing_cust.reset();
      //     });
    }
    else {
      this.showLoader = false;
      this.errorMsg = 'Please enter a valid MTN number';
   
    }
    
   const sessionResp = await this.sessionService.createSessionId(msisdn);

   //console.log("This is response got ", sessionResp);
   if (sessionResp.status == 0) {
     this.showLoader = false;
     this._storeVal.setSessionId(sessionResp.session_id);
     this._storeVal.setSessionStatus('valid');
   
     if(localStorage.getItem("manna") === null){
      let result = {
        SecondaryNumber: primaryNumber,
        StartDate: "lap",
        NickName:	"stan"
       }
       
      localStorage.setItem("manna", JSON.stringify(result));
     }

     localStorage.setItem("switched", "true");
     await this.otpMessage.sendSwitchSuccess(msisdn, logoutnumber, this.nickname);
       this.router.navigate(['dashboard'])
       .then(nav => { }, (err) => {
         this.showLoader = false;
       
       });
     

   }
   else {
     this.showLoader = false;
     this.errorMsg = 'Unable to create session. Please try again after sometime';
     //console.log("SESSION WAS NOT ABLE TO CREATE")
   }

  }
  

  async unlinkAccount(){
    let msisdn = await this._storeVal.getMSISDNData();
    let sec_msisdn = this.unlinkNumber;
    // let sec_msisdn = "9051351635"
    this.addAccount.unlinkAccount(msisdn,sec_msisdn).then(data => {
      if(data.StatusCode == 0 || data.StatusCode == '0'){
        //this.router.navigate(['unlinkdone'], {state: { unlinknumber:  this.unlinkNumber, unlinksuccess: true}})
       // .then(nav => { }, (err) => {
       //   this.showLoader = false;
       // });    
        this.suceesDiv=true;
        this.showLoader = false;
        this.message="Success!";
        this.unlinkCondition=false;
        this.message1="Request has been Processed Successfully";
      }else{
        this.failurDiv=true;
        this.showLoader = false;
        this.message="Failure!";
        this.unlinkCondition=false;
        this.message1="Request has been Failed,Try Again";
        //this.router.navigate(['unlinkdone'], {state: { unlinknumber:  this.unlinkNumber, unlinkfailure: true}})
      }
    }); 
  }


}
