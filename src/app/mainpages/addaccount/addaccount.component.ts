import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { Location } from '@angular/common';
//  import { format } from '../../utils/formatter';
import { Router } from '@angular/router';
import { config } from '../../config';
import { format } from '../../utils/formatter';
import { LoginForm } from '../../utils/interface';
import { ProfileService } from '../../plugins/profile';
import { otpService } from '../../plugins/otp';
import { StoreService } from '../../plugins/datastore';
import {AddAccountService} from '../../plugins/addaccount';
import {AddAccountProceedComponent} from './addaccount-proceed/addaccount-proceed.component';
// import { Observable } from 'rxjs/';

@Component({
  selector: 'app-addaccount',
  templateUrl: './addaccount.component.html',
  styleUrls: ['./addaccount.component.scss']
})
export class AddaccountComponent implements OnInit {
  selctradio: boolean = false;  
  add_account: FormGroup;
  True : boolean = true;
  public progress:boolean = false;
  public sec_account:boolean = false;
  //public add_account: FormGroup;
  public otp_cust: FormGroup;
  public hideee:boolean=false;
  public showconfimdiv:boolean=false;
  public existing_MTN: FormGroup;
  public OTPShow: boolean = true;
  public sendotp: boolean = true;
  accountdetails: any = [];
  // public msisdn:any;
  public emailFlag: boolean=false;
  public errorMsg = '';
  email: string;
  alternateNumber: number;
  primaryNumber: number;
primaryNumberProvider:number;
  showLoader : boolean = false;
  showsuccessdiv:boolean = false; 
  public message :string;
  statusMessage = ["", "", "MSISDN already linked with XXXXX"];
  otp_radio: string;
   msisdn:any;
  phonenum: any;
  simStatus:any;
  header = false;
  otpValue: string;
  valiotp: string;
  timeoutErr = ''
  //sec_msisdnVal:string;
  sec_msisdn:string;
  label:any;
  sec_accountdetails:string;
  addedno:boolean = false;
  responseLoaded:boolean = false;
  disabledAgreement: boolean = true;
 inputField:boolean = true;
  tempDisable: boolean = true;
 failed:boolean =  false;
  linking:boolean = true;
 secondaryNumberCounter: number = 0;
  secondaryNumberToBeAdded:number = 0;
  mynumber: any;
  alreadylinked:boolean = false;
  linkAccountFlag: boolean = true;
//linking:boolean = true;
 // labelItems1:any
  constructor(private fb: FormBuilder, private _profile: ProfileService, private _otp: otpService, private _storeVal: StoreService, private router: Router,private _addAccount:AddAccountService,private location: Location) { }

  ngOnInit() {
   this.linkedAccounts();
   this.mynumber =  this._storeVal.getMSISDNData()
   this.msisdn = this._storeVal.getMSISDNData();
   this._storeVal.setSecondaryAccount(this.sec_account);
   //let sec_msisdn = this._storeVal.getSecondaryMSISDNData();
 
    this.add_account = this.fb.group({
      sec_msisdn: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(13)]],
      label:['',[Validators, Validators.minLength(5), Validators.maxLength(20)]]
      

    });
     this.otp_cust = this.fb.group({
      enterotp: ['', [Validators.required, Validators.minLength(6)]],
    });

	
  }

  async displayUser(){
    this.primaryNumber = await this._storeVal.getMSISDNData();
    this.email = await this._storeVal.getEmailData();
    this.alternateNumber = this._storeVal.getAlternateNumberData();
  }
  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }
counter(i){
  }
  changeCheck(event){
//    this.inputField = ((this.inputField === undefined)? true : this.inputField);
   this.disabledAgreement = !event.checked;
   let tempDisable = true;
   if(!this.disabledAgreement && !this.inputField){
     tempDisable = false;
     this.tempDisable = tempDisable;
   }else{
    this.tempDisable = tempDisable;
   }
  }
   // numberOnly(event): boolean {
  // //this.inputField = true;
  // if(this.timeoutErr){
  //   this.timeoutErr = '';
  //   }
  //   let test = event.target.value;
  //   let test_length = test.length;
  //   if(test_length>= 10){
  //     this.inputField = false;
  //   }else if(test_length<10){
  //     this.inputField = true;

  //  this.disabledAgreement = false;

  //   }

    
   
 
      // let test = event.target.value;
      // let test_length = test.length;
      // const charCode = (event.which) ? event.which : event.keyCode;
  
      // if (charCode > 64 && (charCode < 96 || charCode > 123) || charCode == 32) {
      //   return false;
      // }
      // if (test_length >= 13&& (charCode == 8 || charCode == 13)) {
      //   return true;
      // } else if (test_length >= 13) {
      //   return false;;
      // } else {
      //   return true;
      // }
    

  numberOnly(event): boolean {
        if (this.timeoutErr) {
          this.timeoutErr = '';
        }
        let test = event.target.value;
        let test_length = test.length;
        const charCode = (event.which) ? event.which : event.keyCode;
    
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
        }
        if (test_length >= 13 && (charCode == 8 || charCode == 13)) {
          return true;
        } else if (test_length >= 13) {
          return false;;
        } else {
          return true;
        }
      }

  numberOnlyotp(event): boolean {
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length >= 6&& (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length >= 6) {
      return false;
    } else {
      return true;
    }
  }
  switchAccount(){
  this.sec_account = true;
  // 9062058850
  this._storeVal.setSecondaryAccount(this.sec_account);
  this._storeVal.setSecondaryAccountfalse(true);
    //this.sec_msisdn = this._storeVal.getSecondaryMSISDNData();
    if(!this.sec_account){
      this.sec_accountdetails = this._storeVal.getSecondaryMSISDNData();
    }
    
// console.log("All Data in addAccount"+
// //this._storeVal.getSecondaryMSISDNData()
// this.sec_accountdetails+""+
// this._storeVal.getSecondaryMSISDNData()
// );this._storeVal.setMSISDNData(this.sec_accountdetails);
    this._storeVal.setMSISDNData(this.sec_accountdetails);
    this._storeVal.setEmailData(this._storeVal.getEmailData());
    this._storeVal.setAlternateNumberData(this._storeVal.getSecondaryAlternateNumberData());
    this._storeVal.setFamilyNameData(this._storeVal.getSecondaryFamilyNameData());
    this._storeVal.setFirstNameData(this._storeVal.getSecondaryFirstNameData());
    this._storeVal.setSubsTypeData(this._storeVal.getSecondarySubsTypeData());

    this._storeVal.setMSISDNData(this.sec_accountdetails);
    this._storeVal.setEmailData(this._storeVal.getSecondaryEmailData());
    this._storeVal.setAlternateNumberData(this._storeVal.getSecondaryAlternateNumberData());
    this._storeVal.setFamilyNameData(this._storeVal.getSecondaryFamilyNameData());
    this._storeVal.setFirstNameData(this._storeVal.getSecondaryFirstNameData());
    this._storeVal.setSubsTypeData(this._storeVal.getSecondarySubsTypeData());
    //this.sec_msisdn = this._storeVal.getSecondaryMSISDNData();
this._storeVal.setSecondaryAccountfalse(false);
    this.router.navigate(['dashboard']).then(nav => { }, (err) => {
      this.errorMsg = 'Please try again after sometime';
      //this.otp_cust.reset();
    });
  }

  checkEmail(value){

    this.emailFlag = value;
  }

  sendotpClick() {
    this.timeoutErr = '';
    this.showLoader = true;
    //this.OTPShow = !this.OTPShow;
    if(this.emailFlag){

      this.otp_radio = this.primaryNumber.toString();
    }
    this._otp.generateOTP(this.otp_radio, this.emailFlag, this.linkAccountFlag).then((resp) => {
      this.valiotp = resp.otp;
      if (resp.status == 0) {
        this.sendotp = false; 
        this.OTPShow = false;
        this.router.navigate(['addaccount','proceed']);
        this._storeVal.setOtpData(resp.otp)    
  this.otp_cust.reset();	
 this.showLoader = false;  
      }
    // }).catch(err => alert(err));

      }).catch((err) => {
	   if(err.message == 'Timeout has occurred'){
        this.sendotp = false; 
        this.OTPShow = false;
        this.showLoader = false;
        this.timeoutErr = 'Timeout has occurred, Please try again';
        }
      this.otp_cust.reset();
    });
  }


validateOtp() {
  //this.progress = true;
   this.otpValue = this.otp_cust.value.enterotp;
    this.msisdn = this._storeVal.getMSISDNData();
    this._otp.validateOTP(this.otpValue, this._storeVal.getOtpData(), this.msisdn).then(resp => {

    if (resp.status == 0) {
   this.showconfimdiv = true;
   this.sec_msisdn = this._storeVal.getSecondaryMSISDNData();
   // this.router.navigate(['dashboard'])
    // .then(nav => { }, (err) => {
    //   this.errorMsg = 'Please try again after sometime';
      this.otp_cust.reset();
   }
  else{
    this.message = resp.Valid;
    }
  });

}
unlinkAccount(){
  this.msisdn = this._storeVal.getMSISDNData();
  this.sec_msisdn = this._storeVal.getLinkedAccount();
  this._addAccount.unlinkAccount(this.msisdn,this.sec_msisdn).then(data => {
    if(data.StatusCode == 0 || data.StatusCode == '0'){
    location.reload();
    }else{
      alert('failed to unlink the number');
    }
  });
  
}
 
 

   backtologin1(){
     this.OTPShow = true;
      }
      backtologin2(){
        this.sendotp = true;
      }
  

  backToLogin() {
    this.errorMsg = '';
    // this.category = 'Login';
    // this.hide_OTP = true;
  }


 resendotpClick() {
       this.hideee=true;
      this.sendotp = true; 
    }
async linkedAccounts(){
 this.showLoader= true;
  this.msisdn = await this._storeVal.getMSISDNData();
 
  this._addAccount.getLinkedAccounts(this.msisdn).then(data => {
   if (data.StatusCode == 0) {
       this.addedno = true;
 this.secondaryNumberCounter = 5;
       this.secondaryNumberToBeAdded = this.secondaryNumberCounter - data.results.length;
    this.accountdetails = data.results;
	 this.showLoader= false;
 //this.showLoader= false;
  //  for(let i = 0; i<this.accountdetails.length; i++){
      this.sec_accountdetails = this.accountdetails[0].SecondaryNumber;
      this._storeVal.setLinkedAccount(this.sec_accountdetails);
      this.responseLoaded=true;
   }
   else if(data.StatusCode == '105' || data.StatusCode == '104'){
     this.addedno = false;
     this.responseLoaded=true;
	  this.showLoader= false;
     this.secondaryNumberToBeAdded = 5;

   }
 
  });
}


   onSubmit(value: FormGroup) {
   const msisdn = this._storeVal.getMSISDNData();
    let sec_msisdn = format.msisdn(this.add_account.get('sec_msisdn').value);
    let label = this.add_account.get('label').value;
    if(sec_msisdn.length < 9){	
	this.showLoader = false;
	this.errorMsg = 'Please enter a valid MTN number.';
        this.add_account.reset();
    }
   this.showLoader = true;
//5 unkonw error //4: MSISDN is not in CLM //3: MSISDN is not active //2:MSISDN already LINKED//1 Allow to linked.
this.simStatus = 5;
    this._profile.getProfileDetails(sec_msisdn).then((resp) => {
      if(resp.status_code == 0){
    this._addAccount.validateAccount(sec_msisdn).then((resp) => {
      if (resp.StatusCode === "0" || resp.StatusCode === 0) {
        if(resp.result && resp.result[0].Status == 'ACTIVE'){
          if (resp.result[0].ProviderServiceId && resp.result[0].ProviderServiceId.trim().length > 0) {
                this.simStatus = 2;
                this.primaryNumberProvider = resp.result[0].ProviderServiceId;
            this.alreadylinked = true;
           this.linking = false;
            this.sendotp = false;
            this.showLoader = false;
          }
            if (resp.result[0].ProviderServiceId == '') {
              this.simStatus = 1;
                this._profile.getProfileDetails(sec_msisdn).then((resp) => {
                  this.OTPShow = false;
                  this.showLoader = false;
              this.sendotp = true;
              this.alreadylinked = false;
                 // this.otp_radio = this.primaryNumber.toString();
                  if (resp.status_code == 0) {
                    this._storeVal.setSecondaryMSISDNData(sec_msisdn);
                    this._storeVal.setAccountTag(label);
                    this._storeVal.setSecondaryEmailData(resp.customer_Profile.email);
                    this._storeVal.setSecondaryAlternateNumberData(resp.customer_Profile.alternateNumber);
                    this._storeVal.setSecondaryFamilyNameData(resp.customer_Profile.familyName);
                    this._storeVal.setSecondaryFirstNameData(resp.customer_Profile.firstName);
                    this._storeVal.setSecondarySubsTypeData(resp.subsType);
					this._storeVal.setActivationDate(resp.customer_Profile.activationDate);
                    this.email = resp.customer_Profile.email;
                    this.primaryNumber = resp.customer_Profile.primaryNumber;
                    this.sec_account = false;
                    this._storeVal.setSecondaryAccountfalse(this.sec_account);
				    if (resp.customer_Profile.primaryNumber != resp.customer_Profile.alternateNumber) {
                     this.alternateNumber = resp.customer_Profile.alternateNumber;

                   }
                    this.otp_radio = this.primaryNumber.toString();
                    //this._storeVal.setSecondaryEmailData()
                    //alert(JSON.stringify(resp));
                 }
                 else if(resp.status_code == '107'){
                  this.phonenum = resp.status_message;
                   this.OTPShow = true;
                   this.linking = false;
                   this.failed = true;
                   
                 }
                })
             }
                       
                    } 
                        
                  
      else {
                         this.simStatus = 3;
                        // alert(this.simStatus);
        //this.alreadylinked = true;
                      }
                }
 else{
                     
                       this.simStatus = 4;
                       this.showLoader = false;
                       this.errorMsg = 'Please enter a valid MTN number';
                       this.add_account.reset();
                   }
if (this.simStatus != 5) {
                        if (this.simStatus == 1) {
                           // var redirectTo="wgt:777733/1.0/linkNumber.html:showOTPInpuScreen("+msisdn+")";
                         
               
                            // window.location.href = "wgt:777733/1.0/linkNumber.html:otpjourney("+msisdn+")";
                          /* var redirectTo="close://&action=wgt:777733/1.0/linkNumber.html:sendLinkedOTP("+msisdn+")";
                           var msg="How would you like to receive OTP?";
                           dispConfirmCommon(KEY_BACK, "Send OTP", "close://", redirectTo, msg, "close://", "", "C");*/
               
                           }else{
                                var txt = this.statusMessage[this.simStatus];
                               if (this.simStatus == 2) {
                                 this.alreadylinked = true;
                                // this.sendotp = false;
                                // this.OTPShow = false;
                                // this.linking = false;
                
                                  // txt = txt.replace("XXXXX", this.primaryNumber);
                               }
                               //dispMessage(KEY_BACK, "close://", txt, "close://", "E", '');
               
                           }
                   }
                  
    }).catch((err) => {
      this.showLoader = false;
      this.errorMsg = 'Please enter a valid MTN number';
      this.add_account.reset();
    });
    }
	
 });
  }
}




	
  



