import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/plugins/datastore';
import { FormGroup, FormBuilder,Validators, FormControl, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import {SendAirtimeService} from 'src/app/plugins/send-airtime';
import { format } from 'src/app/utils/formatter';
import { otpService } from 'src/app/plugins/otp';
import {AddAccountService} from 'src/app/plugins/addaccount';
import { CryptoService } from 'src/app/plugins/crypto';
//declare var $: any;

@Component({
  selector: 'app-addaccount-proceed',
  templateUrl: './addaccount-proceed.component.html',
  styleUrls: ['./addaccount-proceed.component.scss']
})
export class AddAccountProceedComponent implements OnInit {
    public otp_cust: FormGroup;
  airttt: boolean = true;
  airttt1: boolean = false;
  showsuccessdiv: Boolean = true;
  failurDiv: Boolean = false;
  suceesDiv: Boolean = false;
  showconfimdiv: Boolean = true;
  showLoader: Boolean = false;
  success:Boolean = false;
  failure:Boolean = false;
  header=true;
  message = '';
  msisdnval = '';
  debitAmount = '';
  pin='';
  cur_msisdn = '';
  self:boolean;
  Airtime:any;
  public emailFlag: boolean=false;
  public errorMsg = '';
  timeoutErr = '';
  otpValue: string;
  valiotp: string;
  msisdn:string;
  sec_msisdn:string;
  msisdn1:string;
  sec_msisdn1:string;
  email: string;
  Confirmation:Boolean = false;
  otpvarification:Boolean = true;
  label:string;
  linkedno:string;
  firstName:string;
  confirm:boolean = false;
  hideee:boolean = false;
  sendotp:boolean = false;
  protectedMsisdn:any;
  protectedMail:any;
  constructor(private fb: FormBuilder, private _storeData: StoreService, private _otp: otpService, private router:Router,private _addAccount:AddAccountService,private cryptoService : CryptoService ) { }

  ngOnInit() {
    this.otp_cust = this.fb.group({
        enterotp: ['', [Validators.required, Validators.minLength(6)]],
      });
   
    this.sec_msisdn = this._storeData.getSecondaryMSISDNData();
    this.email = this._storeData.getSecondaryEmailData();
    this.protectedMsisdn = this.maskedNumber(this.sec_msisdn, 8, 13, '*');
    this.protectedMail = this.maskedEmail(this.email, 2, 9, '*');

    this.msisdn = this._storeData.getMSISDNData();
    this.firstName =  this._storeData.getFirstNameData();
    this._storeData.setSecondaryAccountfalse(true);
  }

  numberOnly(event): boolean {
	if(this.timeoutErr){
    this.timeoutErr = '';
    }
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length >= 13&& (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length >= 13) {
      return false;;
    } else {
      return true;
    }
  }

  maskedNumber(str: string, startIndex: any, endIndex: any, char: string) {
    if(str == null){
      return str;
    }
    startIndex = 6;
    endIndex = 11;
    char = '*';
    const characters = str.split('');
    for (let i = startIndex; i < endIndex && i < characters.length; i++) {
      characters[i] = char;
    }
    return characters.join('');
  }

 maskedEmail(str: string, startIndex: any, endIndex: any, char: string) {
    if(str == null){
      return str;
    }
    startIndex = 3;
    endIndex = 9;
    char = '*';
    const characters = str.split('');
    for (let i = startIndex; i < endIndex && i < characters.length; i++) {
      characters[i] = char;
    }
    return characters.join('');
  }

  numberOnlyotp(event): boolean {
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length >= 6&& (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length >= 6) {
      return false;;
    } else {
      return true;
    }
  }
  validateOtp() {
this.showLoader = true;

this.msisdn = this._storeData.getMSISDNData();
     this.otpValue = this.otp_cust.value.enterotp;
   let encryptedData =  this.cryptoService.encryptData(this.otpValue);
    this._otp.validateOTP(encryptedData,this._storeData.getOtpData(), this.msisdn).then(resp => {
      if (resp.status == 0) {
     this.showconfimdiv = true;
     this.showLoader = false;
     this.sec_msisdn = this._storeData.getSecondaryMSISDNData();
     this.sec_msisdn1 = "(+234) "+this.sec_msisdn.substring(3);
     this.Confirmation = true;
     this.otpvarification = false;
     this.confirm = true;
        this.otp_cust.reset();
     }
    else{
      this.message = resp.Valid;
this.showLoader = false;
      this.otp_cust.reset();
      }
    });
  
  }
  resendotpClick() {
    this.hideee = true;
    this.sendotp = true;
  }
  linkAccount(){
this.showLoader = true;
    this.msisdn = this._storeData.getMSISDNData();
    this.msisdn1 = "(+234) "+this.msisdn.substring(3);
    this.sec_msisdn = this._storeData.getSecondaryMSISDNData();
    this.sec_msisdn1 = "(+234) "+this.sec_msisdn.substring(3);
    this.label = this._storeData.getAccountTag();
    //console.log("nnnhjjjjdjjd"+this.msisdn.substring(3)+"jjjjj"+this.sec_msisdn.substring(3)+"tfdffdfdf"+this.label);
    this._addAccount.linkAccount(this.msisdn,this.sec_msisdn,this.label).then(data => {
     // console.log("getlinked accounts data...." + JSON.stringify(data));
      if(data.StatusCode == '0' ){
        this.showLoader = false;
        this.suceesDiv = true;
        this.failure = false;
        this.success= true;
        this.Confirmation =  true;
        this.confirm = false;
        this.linkedno = data.StatusDesc;
     //   console.log("response description" + this.linkedno);
      //  this.router.navigate(['/addaccount']);
       // alert('number added successfullay');

        
      }else if(data.StatusCode == '204' ||data.StatusCode=='203' ){
        this.showLoader = false;
        this.suceesDiv = true;
        this.failure = true;
        this.success= false;
        this.Confirmation = true;
        this.confirm = false;
       //this.router.navigate(['/addaccount']);

      }
      else if(data.StatusCode = 'SOANG_V:001'){
        this.showLoader = true;
        this.failure = true;
        this.Confirmation = false;
       // alert('this number already linked with other number');
      }
      else{
        alert("failed");
      }
      }).catch((err) => {
        if(err.message == 'Timeout has occurred'){
        //    this.sendotp = false; 
        //    this.OTPShow = false;
           this.showLoader = false;
           this.timeoutErr = 'Timeout has occurred, Please try again';
           }
    });
   }
  checkEmail(value){

    this.emailFlag = value;
   // console.log('otp---emaialFlag' + this.emailFlag);
  }
  backButton(){
    this.router.navigate(['/addaccount']);
  }


}

