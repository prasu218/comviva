import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.scss']
})
export class FaqsComponent implements OnInit {
  header = true;
//   public plus: boolean = false;
// public sign:any = '+';

selectedData;
  @Input() active_nav: string;
  

  faqsNavElements = [
    {id:'smartWebNav', value:'myMTN',value1:'Navigation', source :'assets/images/navigation.svg',},
    {id:'login',  value:'Login',source :'assets/images/login.svg',},
    {id:'databundle',  value:'Data ',value1:'bundle', source :'assets/images/data_bundle.svg',},
    {id:'accountBalance',  value:'Account ',value1:'Balance',  source :'assets/images/account_balance.svg',},
    // {id:'SIMSwap',  value:'SIM Swap',  source :'assets/images/sim_swap.svg',},
    {id:'Migration',  value:'Migration',  source :'assets/logo/rechargel.svg',},
    {id:'SIMRegistration',  value:'SIM ',value1:"Registration",  source :'assets/images/sim_registration.svg',},
    {id:'PUKRequest',  value:'PUK- Request',  source :'assets/images/puk.svg',},
    {id:'CallRechargeSMS', value:'Call,', value1:'Recharge,',value2:'SMS', source :'assets/images/call_sms_recharge_details.svg',},
    {id:'ManageData',  value:'Manage Data',  source :'assets/images/manage data.svg',},
    {id:'Others',  value:'Others',  source :'assets/images/baseline-more-24px.svg',}
    
  
 ];
  
  constructor(private _router: Router,private location: Location) { }
  
  ngOnInit() {
    this.onLoadMdNav('selectedData');
    $( document ).ready(function() {
    $('.carousel-faq').flickity({
      groupCells: true,
      freeScroll: true,
      cellAlign: 'left', 
      contain: true, 
      prevNextButtons: false,
      pageDots: true, 
      draggable: true,
      watchCSS: true,
      setGallerySize: false,
      resize: true,
      reposition: true
    });
  });
    //this.onLoadMdNav(this.faqsNavElements[0]);
    this.active_nav  = "smartWebNav";

	// horizontal scroll 
    $(function ($) {
      $.fn.hScroll = function (amount) {
          amount = amount || 120;
          $(this).bind("DOMMouseScroll mousewheel", function (event) {
              var oEvent = event.originalEvent, 
                  direction = oEvent.detail ? oEvent.detail * -amount : oEvent.wheelDelta, 
                  position = $(this).scrollLeft();
              position += direction > 0 ? -amount : amount;
              $(this).scrollLeft(position);
              event.preventDefault();
          })
      };
  });
  
  $(document).ready(function() {
      $('.scrollmenu').hScroll(60); // You can pass (optionally) scrolling amount
  });

  }


  ngAfterContentChecked(){
    //this.active_nav = this._router.url.split('/')[2];
    // if(this._router.url === "/faqs"){
    //  // this.active_nav  = "smartWebNav";
    // }
  }


  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this._router.navigate(['dashboard']);
    }
   }
   
  onLoadMdNav(selectedData){
    this.active_nav = "smartWebNav";
    // console.log(selectedData)
    // this._router.navigate(['faqs',selectedData.id]);
    this.active_nav = selectedData.id;
    //this.selectedData = selectedData.id;
    //console.log("this.active_nav",this.active_nav)
    //console.log("selectedData",this.active_nav === selectedData.id)
  }

  backout() {
    this._router.navigate(['dashboard']);
  }

}
