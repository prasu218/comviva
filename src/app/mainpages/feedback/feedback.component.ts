import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { config } from '../../config';
import { StoreService } from '../../plugins/datastore';
import { FeedbackService } from '../../plugins/feedback';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  feedbackEmoj = [{ name: 'Delighted', url: 'assets/images/smile1.svg' }, { name: 'Impartial', url: 'assets/images/smile2.svg' }, { name: 'Disappointed', url: 'assets/images/smile3.svg' }]
  selectedem: string;
  value: boolean = true;
  message = "";
  emoj: string="";
  msisdn: string;
  firstname: string;
  email: string;
  header = true;
  show: boolean = false;
  showLoader: boolean = false;
  text = "";
  clicked: boolean = false;
  content: boolean;
  sucessDiv:boolean;
  latitude: any;
  longitude: any;
  userCurrentLoc: any;
  handleLocationErrStatus: boolean = true;
  public existing_cust: FormGroup;
  constructor(private fb: FormBuilder, private _storeData: StoreService, private _feedback: FeedbackService, private router: Router,private location: Location) { }

  ngOnInit() {
    this.firstname = this._storeData.getFirstNameData();
    this.email = this._storeData.getEmailData();
    this.msisdn = this._storeData.getMSISDNData();
    this.existing_cust = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(10),]],
    })
    this.getUserCurrentLoc();
    this.content=true;
  }
  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }
  submit(emoj) {
    if(!emoj){
      this.showLoader = false;
    }
    else{
      this.showLoader = true;
    }
    var supp_id = "customerare@mtnnigeria.net";
    var ccmailid = "customercare3@mtn.com";
    var from = "mtnnigeria@mtn1app.net";
    var subject = "myMTN Feedback from : " + this.msisdn;

    var mailbody = [mailbody, "<html><p>" + "Dear" + "<b>" + " Customer care" + "</b>,</p><table border='1' style='margin-left:50px' width='60%'><tr><td>MSISDN</td><td><b>" + this.msisdn + "</b></td></tr>"].join("");
    mailbody = [mailbody, "<tr><td>Rating</td><td>" + emoj + "</td></tr><tr>"].join("");
    if (emoj == "Disappointed") {
      if (this.userCurrentLoc) {
        mailbody = [mailbody, "<td>Feedback</td><td>" + this.text + "</td></tr><tr><td>Email</td><td>" + this.email + "</td></tr><tr><td>Address</td><td>" + this.userCurrentLoc + "</td></tr></table><p>" + "Regards," + "<br/><b>" + this.firstname + "</b></html>"].join("");
      } else {
        mailbody = [mailbody, "<td>Feedback</td><td>" + this.text + "</td></tr><tr><td>Email</td><td>" + this.email + "</td></tr></table><p>" + "Regards," + "<br/><b>" + this.firstname + "</b></html>"].join("");
      }
    } else {
      if (this.userCurrentLoc) {
        mailbody = [mailbody, "<tr><td>Email</td><td>" + this.email + "</td></tr><tr><td>Address</td><td>" + this.userCurrentLoc + "</td></tr></table><p>" + "Regards," + "<br/><b>" + this.firstname + "</b></html>"].join("");
      } else {
        mailbody = [mailbody, "<tr><td>Email</td><td>" + this.email + "</td></tr></table><p>" + "Regards," + "<br/><b>" + this.firstname + "</b></html>"].join("");
      }
    }
if(emoj!=""){
    //sending payload to api function  
    this._feedback.feedbackdetailsAPI(this.msisdn, from, supp_id, ccmailid, subject, mailbody)

      .then((resp) => {
        this.showLoader = false;
        if (resp.status === 200) {
          this.content=false;
          this.sucessDiv=true;
          this.message = "Thank You for your feedback";
          this.clicked = false;
        } else {
          this.sucessDiv=false;
          this.content=false;
          this.message = "Feedback Sending Failed,please try again"
        }
      })
      }
  }
  showcontent(){
    this.content=true;
    this.message="";
  }

  click(emoj) {
    this.selectedem = emoj;
    this.emoj = emoj;
    this.value = false;
    this.message="";

    if (emoj == "Disappointed") {
      this.clicked = true;

    } else {
      this.clicked = false;
    }
  }

  getUserCurrentLoc() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.getLocationInfo.bind(this), this.handleLocationError.bind(this));
    } else {
      //console.log("Geolocation is not supported by this browser.");
    }
  }

  getLocationInfo(position) {
    this.latitude = position.coords.latitude;
    this.longitude = position.coords.longitude;
    this.userCurrentLoc = "";
    if (this.handleLocationErrStatus) {
      this._feedback.getUserLocationAddress(this.latitude, this.longitude).then(resp => {
        if (resp.results.length > 0) {
          this.userCurrentLoc = resp.results[0].formatted_address;
        }
      })
    }
  }

  handleLocationError(error) {
    switch (error.code) {
      case error.PERMISSION_DENIED:
        //console.log("User denied the request for Geolocation.")
        this.handleLocationErrStatus = false;
        break;
      case error.POSITION_UNAVAILABLE:
        //console.log("Location information is unavailable.")
        this.handleLocationErrStatus = false;
        break;
      case error.TIMEOUT:
        //console.log("The request to get user location timed out.")
        this.handleLocationErrStatus = false;
        break;
      case error.UNKNOWN_ERROR:
        //console.log("An unknown error occurred.")
        this.handleLocationErrStatus = false;
        break;
    }
  }

}