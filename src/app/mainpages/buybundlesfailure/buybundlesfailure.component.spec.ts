import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybundlesfailureComponent } from './buybundlesfailure.component';

describe('BuybundlesfailureComponent', () => {
  let component: BuybundlesfailureComponent;
  let fixture: ComponentFixture<BuybundlesfailureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybundlesfailureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybundlesfailureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
