import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { StoreService } from '../../plugins/datastore';
import { Location } from '@angular/common';


@Component({
  selector: 'app-buybundlesfailure',
  templateUrl: './buybundlesfailure.component.html',
  styleUrls: ['./buybundlesfailure.component.scss']
})
export class BuybundlesfailureComponent implements OnInit {
  message:'Recharge Failed';
  header = true;

  msisdnval:any;
  constructor(private router: Router,private _storeVal: StoreService,private location: Location) { }

  ngOnInit() {
       this.msisdnval = this._storeVal.getMSISDNData();
  }
  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }

}
