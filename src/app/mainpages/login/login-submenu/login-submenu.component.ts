
import { Component, OnInit,Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-submenu',
  templateUrl: './login-submenu.component.html',
  styleUrls: ['./login-submenu.component.scss']
})
export class LoginSubmenuComponent implements OnInit {
  @Input() active_nav: string;

    rechargeNavElements = [

    {id:'vouchercard', value:'Voucher', source :'assets/images/voucher_card.svg',},
    //{id:'damagecard',  value:'Damage',  source :'assets/logo/rechargel.svg',},
   
  ];

  constructor(private _router: Router) { 
    
  }

  ngOnInit() {
   // this.scrollSubMenu();
    }

  ngAfterContentChecked(){
    this.active_nav = this._router.url.split('/')[2];
    if(this._router.url === "/login"){
      this.active_nav  = "vouchercard";
    }
  }
  
 onLoadMdNav(selectedData){
    this._router.navigate(['login',selectedData.id]);
    this.active_nav = selectedData.id;
  }

}
