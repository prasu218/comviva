import { Routes } from '@angular/router';
import { LoginComponent } from '../login.component';
import { DamageCardComponent } from './damage-card/damage-card.component'
import { VoucherCardComponent } from './voucher-card/voucher-card.component';
import { AuthGuard } from 'src/app/auth/auth.guard';

export const Loginmenuroutes: Routes = [{
  path: 'login', component: LoginComponent, canActivate: [AuthGuard],
  children: [
    {
      path: '',
      component: VoucherCardComponent,
      pathMatch: 'full'
    },
    {
      path: 'damagecard',
      component: DamageCardComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'vouchercard',
      component: VoucherCardComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },

  ]
}];
