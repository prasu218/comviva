import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { format } from '../../../../utils/formatter';
import { loginrcgService } from '../../../../plugins/loginrcg';
import { config } from '../../../../config';

@Component({
  selector: 'app-voucher-card',
  templateUrl: './voucher-card.component.html',
  styleUrls: ['./voucher-card.component.scss']
})
export class VoucherCardComponent implements OnInit {
public existing_cust: FormGroup;
public errorMsg = '';
public formhide: boolean=true ;
public hideee: boolean=false ;
public failure:boolean=false;
public message :string;
public msisdn : string;
showLoader : boolean = false;

  constructor(private fb: FormBuilder,private _LoginRcgService: loginrcgService) { }

  ngOnInit() {
      this.existing_cust = this.fb.group({
        msisdn: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(13)]],
        voucher: ['', [Validators.required, Validators.minLength(11), Validators.maxLength(17)]],
     });
  }
  numberOnly(event): boolean {
    let test = event.target.value;
      let test_length = test.length;
        const charCode = (event.which) ? event.which : event.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
        }
		if(test_length == 13 && ( charCode == 8  || charCode == 13)){
			return true;
        }else if(test_length == 13){
			return false;;
		}else{
			return true;
		}
       }

           loadvoucher(){
             this.formhide=true;
             this.failure=false; 
             this.hideee=false;
             this.existing_cust.reset();               
             }


      onSubmitVoucher(value: FormGroup) {
        this.showLoader = true;
        let msisdn = format.msisdn(this.existing_cust.get('msisdn').value);
        let voucher_code = (this.existing_cust.get('voucher').value);
        //console.log("msisdn" +msisdn);
        //console.log("voucher_code" +voucher_code)       
        this._LoginRcgService.getLoginRcgDetails(msisdn,voucher_code).then((resp) => {
          if (resp.status_code == 0) {
          this.showLoader = false;
          this.formhide=false;
          this.hideee=true;
         this.msisdn=format.msisdn(this.existing_cust.get('msisdn').value); 
         this.message =  resp.status_message;                                    
          }else{
            this.showLoader = false;
            this.failure=true;
            this.formhide=false;
            this.message =  resp.status_message;
            this.msisdn=format.msisdn(this.existing_cust.get('msisdn').value);
          }
    
        }).catch((err) => {
          this.showLoader = false;
          this.msisdn=format.msisdn(this.existing_cust.get('msisdn').value);
          this.failure=true;
          this.formhide=false;
          this.message = 'Please enter valid voucher number';
          this.existing_cust.reset();
        });
    
      }
    }

       
