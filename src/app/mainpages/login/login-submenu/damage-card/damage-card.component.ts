import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';

@Component({
  selector: 'app-damage-card',
  templateUrl: './damage-card.component.html',
  styleUrls: ['./damage-card.component.scss']
})
export class DamageCardComponent implements OnInit {
existing_cust: FormGroup;
public errorMsg = '';


  constructor(private fb: FormBuilder) { }

  ngOnInit() {
      this.existing_cust = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(13), ]],
     });
   
  }

 numberOnly(event): boolean {
    let test = event.target.value;
      let test_length = test.length;
        const charCode = (event.which) ? event.which : event.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
        }
		if(test_length == 11 && ( charCode == 8  || charCode == 13)){
			return true;
        }else if(test_length == 11){
			return false;;
		}else{
			return true;
		}
       }

}
