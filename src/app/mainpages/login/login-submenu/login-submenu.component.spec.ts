import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginSubmenuComponent } from './login-submenu.component';

describe('LoginSubmenuComponent', () => {
  let component: LoginSubmenuComponent;
  let fixture: ComponentFixture<LoginSubmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginSubmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginSubmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
