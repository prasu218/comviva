import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoComponent } from '../demo/demo.component';

export const routes: Routes = [
  { path: 'demo', component: DemoComponent, pathMatch: 'full' }
]

export const RoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
