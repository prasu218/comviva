import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
//  import { format } from '../../utils/formatter';
import { Router } from '@angular/router';
import { config } from '../../config';
import { format } from '../../utils/formatter';
import { LoginForm } from '../../utils/interface';
import { ProfileService } from '../../plugins/profile';
import { otpService } from '../../plugins/otp';
import { StoreService } from '../../plugins/datastore';
import { BnNgIdleService } from 'bn-ng-idle';
import { NotificationdbService } from '../../plugins/notificationdb';
import { SessionService } from 'src/app/plugins/session';
// import { Observable } from 'rxjs/';
import { AngularCDRService } from '../../plugins/angularCDR';
import { CryptoService } from 'src/app/plugins/crypto';
import { DemoService } from '../../plugins/demo';

import { GoogleAnalyticsService } from "../../plugins/googleanalytics";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  bgColorpp = 'grey';
  selctradio: boolean = false;
  // existing_cust: FormGroup;
  True: boolean = true;
  public existing_cust: FormGroup;
  public otp_cust: FormGroup;
  public hideee: boolean = false;
  public existing_MTN: FormGroup;
  public OTPShow: boolean = true;
  public sendotp: boolean = true;
  //msisdn: number;
  public emailFlag: boolean = false;
  public errorMsg = '';
  email: string;
  alternateNumber: string;
  primaryNumber: string;
  showLoader: boolean = false;
  public message: string;
 msisdnval = ''
  otp_radio: any;
  fmisidn: any;
  phonenum: string;
  header = false;
  otpValue: string;
  valiotp: string;
  timeoutErr = ''
  startTime: any;
  category: any = 'OTP';
  //value="";
  checktOtp: true;
  sessionErr: string = '';
  linkAccountFlag:boolean = false;
  protectedMsisdn:any;
  protectedalterMsisdn:any;
  protectedMail:any;
  constructor(private _demoService: DemoService, private fb: FormBuilder, private notifiservice: NotificationdbService, private _profile: ProfileService,
    private _otp: otpService, private _storeVal: StoreService, private router: Router, private bnIdle: BnNgIdleService,
    private _cdr: AngularCDRService, private sessionService: SessionService, private cryptoService : CryptoService,
    public googleAnalyticsService: GoogleAnalyticsService) { 
      this.doAutoLogin();
  }

  ngOnInit() {
    this.startTime = this._cdr.getTransactionTime(0);
    this._storeVal.setSecondaryAccountfalse(true);
    this.existing_cust = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(13)]],
    });
    this.otp_cust = this.fb.group({
      enterotp: ['', [Validators.required, Validators.minLength(6)]],
    });

    this.displaySessionErr();
  }

  displaySessionErr() {
    let sessionStatus = this._storeVal.getSessionStatus();
    if (sessionStatus == 'expired') {
      this.sessionErr = "Session Timeout. To keep you safe, you have been logged out due to inactivity. Please login again";
      localStorage.removeItem('session_status');
    }
  }
  numberOnly(event): boolean {
    if (this.timeoutErr) {
      this.timeoutErr = '';
    }
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length >= 13 && (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length >= 13) {
      return false;;
    } else {
      return true;
    }
  }

  numberOnlyotp(event): boolean {
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length >= 6 && (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length >= 6) {
      return false;;
    } else {
      return true;
    }
  }
  maskedNumber(str: string, startIndex: any, endIndex: any, char: string) {
    if(str == null){
      return str;
    }
    startIndex = 6;
    endIndex = 11;
    char = '*';
    const characters = str.split('');
    for (let i = startIndex; i < endIndex && i < characters.length; i++) {
      characters[i] = char;
    }
    return characters.join('');
  }
  maskedalterNumber(str: string, startIndex: any, endIndex: any, char: string) {
    if(str == null){
      return str;
    }
    startIndex = 3;
    endIndex = 8;
    char = '*';
    const characters = str.split('');
    for (let i = startIndex; i < endIndex && i < characters.length; i++) {
      characters[i] = char;
    }
    return characters.join('');
  }
  maskedEmail(str: string, startIndex: any, endIndex: any, char: string) {
    if(str == null){
      return str;
    }
    startIndex = 3;
    endIndex = 9;
    char = '*';
    const characters = str.split('');
    for (let i = startIndex; i < endIndex && i < characters.length; i++) {
      characters[i] = char;
    }
    return characters.join('');
  }

  checkEmail(value) {

    this.emailFlag = value;
  }

  sendotpClick() {
    this.timeoutErr = '';
    this.showLoader = true;
    this.bnIdle.startWatching(300).subscribe((res) => {
      this.checktOtp = true;
      this.bnIdle.stopTimer();
    })
    //this.primaryNumber = msisdn;

    if (this.emailFlag) {
      this.otp_radio = this.primaryNumber;
    }
    this._otp.generateOTP(this.otp_radio, this.emailFlag,this.linkAccountFlag).then((resp) => {
      
      // google analytics event emmiter and pass in the details
      // this.googleAnalyticsService.eventEmitter("Data General", "Validate OTP", this.otp_radio, 1);
  
      this.valiotp = resp.otp;
      if (resp.status == 0) {
        this.sendotp = true;
        this.OTPShow = false;

        this._storeVal.setOtpData(resp.otp)
        this.otp_cust.reset();
        this.showLoader = false;
        this._cdr.writeCDR(this.startTime, this.category, this.otpValue, 'OTP', 'Success');//cdr_log
      }

    }).catch((err) => {
      if (err.message == 'Timeout has occurred') {
        this.sendotp = false;
        this.OTPShow = false;
        this.showLoader = false;
        this.timeoutErr = 'Timeout has occurred, Please try again';
        this._cdr.writeCDR(this.startTime, this.category, this.otpValue, 'OTP', 'Failure');//cdr_log
      }
      this.otp_cust.reset();
    });
  }


  /*validateOtp() {
    this.fmisidn = format.msisdn(this.existing_cust.get('msisdn').value)
    this.timeoutErr = '';
    if (!this.checktOtp) {
      this.showLoader = true;
      this.otpValue = this.otp_cust.value.enterotp;
      let encryptedData =  this.cryptoService.encryptData(this.otpValue);
      this._otp.validateOTP( encryptedData, this._storeVal.getOtpData()).then(resp => {
        this._cdr.writeCDR(this.startTime, this.category, this.otpValue, 'OTP', 'Failure');//cdr_log


        if (resp.status == 0) {
         // this.showLoader = false;
          this.bnIdle.startWatching(1800).subscribe((res) => {
            if (res) {
              this.showLoader = false;
              this.router.navigate(['/login']);
              this.bnIdle.stopTimer();
            }
          })
          this.sessionService.createSessionId(this.fmisidn).then(resp => {
            if (resp.status == 0) {
              this._storeVal.setSessionId(resp.session_id);
              this._storeVal.setSessionStatus('valid');
// 		if(this._storeVal.getSessionId()!=''&& this._storeVal.getMSISDNData()!='' && this._storeVal.getBuyBundlePlanDetails() !=''){  }
            let result = {
              SecondaryNumber: this.fmisidn.slice(3, 13),
              StartDate: "lap",
              NickName:	"stan"
            }           
          
        localStorage.setItem("manna", JSON.stringify(result));
              let mtnOnlineProdId = this._storeVal.getMTNOnlineProductId();
              if (mtnOnlineProdId) {
                this.router.navigate(['buybundles/procced/' + mtnOnlineProdId]);
                this._storeVal.deleteMTNOnlineProdId();
              } else {
              this.notifiservice.getNotificationData(this.fmisidn).then((resp) => {
                //console.log("This is resp ", resp);
                if (resp.notifi!=null) {
                  let element =resp.notifi;
                    if (element.cust_id != this.fmisidn) {
                      this.router.navigate(['dashboard']).then(nav => { }, (err) => {
                        this.showLoader = false;
                        this.errorMsg = 'Please try again after sometime';
                        this.otp_cust.reset();
                      });
                    }
                    else if (element.cust_id == this.fmisidn && element.flag == "false") {
                      this.router.navigate(['dashboard']).then(nav => { }, (err) => {
                        this.showLoader = false;
                        this.errorMsg = 'Please try again after sometime';
                        this.otp_cust.reset();
                      });
                    } else {
                      this.router.navigate(['dashboard']).then(nav => { }, (err) => {
                        this.showLoader = false;
                        this.errorMsg = 'Please try again after sometime';
                        this.otp_cust.reset();
                      });
                    }
                  
                }
                else {
                  this.router.navigate(['dashboard']).then(nav => { }, (err) => {
                    this.showLoader = false;
                    this.errorMsg = 'Please try again after sometime';
                    this.otp_cust.reset();
                  });
                }
              })
              }
            }
            else {
              this.showLoader = false;
              this.errorMsg = 'Unable to create session. Please try again after sometime';
              console.log("SESSION WAS NOT ABLE TO CREATE")
            }
          })

        }
        else {
          this.showLoader = false;
          this.message = resp.Valid;
          this.otp_cust.reset();
        }
      });
    } else {
      this.timeoutErr = 'Timeout has occurred, Please click on to Resend OTP';
    }

  }
*/


  backtologin1() {
    this.OTPShow = true;
  }
  backtologin2() {
    this.sendotp = true;
  }


  backToLogin() {
    this.errorMsg = '';
    // this.category = 'Login';
    // this.hide_OTP = true;
  }


  resendotpClick() {
   
    this.sendotpClick();
    this.hideee = true;
    this.sendotp = true;
  }

  async onSubmit(value: FormGroup) {
    this.showLoader = true;
    this.sessionErr = '';
    this.errorMsg = '';
    // const msisdn = this.existing_cust.get('msisdn').value;
    let msisdn = format.msisdn(this.existing_cust.get('msisdn').value);
    
    if (msisdn.length < 9) {
      this.showLoader = false;
      this.errorMsg = 'Please enter a valid MTN number.';
      this.existing_cust.reset();
    }
   
       
	      // google analytics event emmiter and pass in the details
        this.googleAnalyticsService.eventEmitter("Data General", "Submit Phone Number", msisdn, 1);
  
        await this._profile.getNewUserStatus(msisdn).then((resp) => {
            if (resp.status_code == 0) {} else {}        
        }).catch((err) => {
          // this.OTPShow = false;
          this.showLoader = false;
          this.existing_cust.reset();
        });
    
    this._profile.validateProfileDetails(msisdn).then((resp) => {
      if (resp.status_code == 0) {
        // this.email = resp.email;
        // this.alternateNumber = resp.alternateNumber;
        this.primaryNumber = msisdn;

        // this.protectedMsisdn = this.maskedNumber(this.primaryNumber, 8, 13, '*');
        // this.protectedMail = this.maskedEmail(this.email, 2, 9, '*');
        // this.protectedalterMsisdn = this.maskedalterNumber(this.alternateNumber, 8, 13, '*');
        // this._storeVal.setFirstNameData(resp.firstName);
        // this._storeVal.setEmailData(resp.email);

        
        // if (resp.primaryNumber != resp.alternateNumber) {
        //   this.alternateNumber = resp.alternateNumber;
        // }
        this.otp_radio = this.primaryNumber;
        this.OTPShow = false;
        this.sendotp =true;
        this.showLoader = false;

        // to send otp and later validate otp
        this.sendotpClick();

      } else {
            this.sendotp =true;
            this.showLoader = false;
            this.errorMsg = 'Please Enter a valid MTN Number';
            this.existing_cust.reset();
          }
        }).catch((err) => {
           this.sendotp =true;
          this.showLoader = false;
          this.errorMsg = 'Please Enter a valid MTN Number';
          this.existing_cust.reset();
        });
  }

  async validateOtp() {
    this.fmisidn = format.msisdn(this.existing_cust.get('msisdn').value)
    let msisdn = {msisdn: this.fmisidn}
    this.timeoutErr = '';
    if (!this.checktOtp) {
      this.showLoader = true;
      this.otpValue = this.otp_cust.value.enterotp;
      //console.log("This is the msisdn", this.fmisidn);
      let encryptedData = this.cryptoService.encryptData(this.otpValue);
      this._otp.validateOTP(encryptedData, this._storeVal.getOtpData(),  this.fmisidn).then(resp => {
        
       // google analytics event emmiter and pass in the details
        this.googleAnalyticsService.eventEmitter("Data General", "Validate OTP", this.otp_radio, 1);

        this._cdr.writeCDR(this.startTime, this.category, this.otpValue, 'OTP', 'Failure');//cdr_log
    
        if (resp.status == 0) {
         // this.showLoader = false;
          this.bnIdle.startWatching(1800).subscribe((res) => {
            if (res) {
              this.showLoader = false;
              this.router.navigate(['/login']);
              this.bnIdle.stopTimer();
            }
          })

            if(resp.token){
            this._storeVal.setToken(resp.token);
            //sessionStorage.setItem("chakra",resp.token );
          }
          this.getProfileDetails(this.fmisidn);


        }
        else {
          this.showLoader = false;
          this.message = resp.Valid;
          this.otp_cust.reset();
        }
      });
    } else {
      this.timeoutErr = 'Timeout has occurred, Please click on to Resend OTP';
    }

  }


  doAutoLogin() {
    // check msisdn if available via header enrichmentcd ../.cd 
    this.showLoader = false;
    
    this._demoService.getMsisdnDetails().then((resp) => {
        // if msisdn available
        const result = resp;
       // console.log(result);
        const msisdn = result.msisdn;
        if (msisdn != null) {
          this.showLoader = false;
          // validate msisdn

             
 	          // google analytics event emmiter and pass in the details
            this.googleAnalyticsService.eventEmitter("Data General", "Submit Phone Number", msisdn, 1);
  
            // go to dashboard
            this._profile.getProfileDetails(msisdn).then((resp) => {
                if (resp.status_code == 0) {
                  this._storeVal.setMSISDNData(msisdn);
                  this._storeVal.setEmailData(resp.customer_Profile.email);
                  this._storeVal.setAlternateNumberData(resp.customer_Profile.alternateNumber);
                  this._storeVal.setFamilyNameData(resp.customer_Profile.familyName);
                  this._storeVal.setFirstNameData(resp.customer_Profile.firstName);
                  this._storeVal.setSubsTypeData(resp.subsType);
                  this._storeVal.setActivationDate(resp.customer_Profile.activationDate);
          
                  this.email = resp.customer_Profile.email;
                  this.primaryNumber = resp.customer_Profile.primaryNumber;
                  this._storeVal.setPrimaryMSISDNData(this._storeVal.getMSISDNData());
                  this._storeVal.setPrimaryEmailData(this._storeVal.getEmailData());
                  this._storeVal.setPrimaryAlternateNumberData(this._storeVal.getAlternateNumberData());
                  this._storeVal.setPrimaryFamilyNameData(this._storeVal.getFamilyNameData());
                  this._storeVal.setPrimaryFirstNameData(this._storeVal.getFirstNameData());
                  this._storeVal.setPrimarySubsTypeData(this._storeVal.getSubsTypeData());
          
                  if (resp.customer_Profile.primaryNumber != resp.customer_Profile.alternateNumber) {
                    this.alternateNumber = resp.customer_Profile.alternateNumber;
                  }
                  this.showLoader = false;

                  this.sessionService.createSessionId(this.fmisidn).then(resp => {
                    if (resp.status == 0) {
                        // this.showLoader = false;
                        this._storeVal.setSessionId(resp.session_id);
                        this._storeVal.setSessionStatus('valid');
                        
                        this.router.navigate(['dashboard']);
                      }
                      else {
                        this.showLoader = false;
                        this.errorMsg = 'Unable to create session. Please try again after sometime';
                      }
                    })

                } else {
                  this.showLoader = false;
                  this.errorMsg = 'Please Enter a valid MTN Number';
                  this.existing_cust.reset();
                }
                  
              }).catch((err) => {
                // this.OTPShow = false;
                this.showLoader = false;
                this.errorMsg = 'Please Enter a valid MTN Number';
                this.existing_cust.reset();
              });
	      
        } else {
          this.showLoader = false;
          this.errorMsg = 'Failed to login';
          this.existing_cust.reset();
        }
        
    }).catch((err) => {
     // console.log(err);
      this.showLoader = false;
      this.errorMsg = 'Failed to retrieve msisdn from header';
    });
    // else stay on this page
}

async getProfileDetails(msisdn){

  await this._profile.getProfileDetails(msisdn).then((resp) => {
    if (resp.status_code == 0) {
      this._storeVal.setMSISDNData(msisdn);
      this._storeVal.setEmailData(resp.customer_Profile.email);
      this._storeVal.setAlternateNumberData(resp.customer_Profile.alternateNumber);
      this._storeVal.setFamilyNameData(resp.customer_Profile.familyName);
      this._storeVal.setFirstNameData(resp.customer_Profile.firstName);
      this._storeVal.setSubsTypeData(resp.subsType);
      this._storeVal.setActivationDate(resp.customer_Profile.activationDate);

      this.email = resp.customer_Profile.email;
      this.primaryNumber = resp.customer_Profile.primaryNumber;
      this._storeVal.setPrimaryMSISDNData(this._storeVal.getMSISDNData());
      this._storeVal.setPrimaryEmailData(this._storeVal.getEmailData());
      this._storeVal.setPrimaryAlternateNumberData(this._storeVal.getAlternateNumberData());
      this._storeVal.setPrimaryFamilyNameData(this._storeVal.getFamilyNameData());
      this._storeVal.setPrimaryFirstNameData(this._storeVal.getFirstNameData());
      this._storeVal.setPrimarySubsTypeData(this._storeVal.getSubsTypeData());

      if (resp.customer_Profile.primaryNumber != resp.customer_Profile.alternateNumber) {
        this.alternateNumber = resp.customer_Profile.alternateNumber;

      }
      this.otp_radio = this.primaryNumber.toString();

      this.OTPShow = false;
      this.showLoader = false;

      
      this.sessionService.createSessionId(this.fmisidn).then(resp => {
        if (resp.status == 0) {
          this._storeVal.setSessionId(resp.session_id);
          this._storeVal.setSessionStatus('valid');

         
// 		if(this._storeVal.getSessionId()!=''&& this._storeVal.getMSISDNData()!='' && this._storeVal.getBuyBundlePlanDetails() !=''){  }
        let result = {
          SecondaryNumber: this.fmisidn.slice(3, 13),
          StartDate: "lap",
          NickName:	"stan"
          
        }           
      
    localStorage.setItem("manna", JSON.stringify(result));
          let mtnOnlineProdId = this._storeVal.getMTNOnlineProductId();
          if (mtnOnlineProdId) {
            this.router.navigate(['buybundles/procced/' + mtnOnlineProdId]);
            this._storeVal.deleteMTNOnlineProdId();
          } else {
          this.notifiservice.getNotificationData(this.fmisidn).then((resp) => {
           
            if (resp.notifi!=null) {
              let element =resp.notifi;
                if (element.cust_id != this.fmisidn) {
                  this.router.navigate(['dashboard']).then(nav => { }, (err) => {
                    this.showLoader = false;
                    this.errorMsg = 'Please try again after sometime';
                    this.otp_cust.reset();
                  });
                }
                else if (element.cust_id == this.fmisidn && element.flag == "false") {
                  this.router.navigate(['dashboard']).then(nav => { }, (err) => {
                    this.showLoader = false;
                    this.errorMsg = 'Please try again after sometime';
                    this.otp_cust.reset();
                  });
                } else {
                  this.router.navigate(['dashboard']).then(nav => { }, (err) => {
                    this.showLoader = false;
                    this.errorMsg = 'Please try again after sometime';
                    this.otp_cust.reset();
                  });
                }
              
            }
            else {
              this.router.navigate(['dashboard']).then(nav => { }, (err) => {
                this.showLoader = false;
                this.errorMsg = 'Please try again after sometime';
                this.otp_cust.reset();
              });
            }
          })
          }
        }
        else {
          this.showLoader = false;
          this.errorMsg = 'Unable to create session. Please try again after sometime';
          
        }
      })
    }
    else {
      this.showLoader = false;
      this.errorMsg = 'Please Enter a valid MTN number';
      this.existing_cust.reset();
    }
  }).catch((err) => {
    this.showLoader = false;
    this.errorMsg = 'Please Enter a valid MTN number';
    this.existing_cust.reset();
  });
}

}
