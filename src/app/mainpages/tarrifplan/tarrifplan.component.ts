import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StoreService } from '../../plugins/datastore';


@Component({
  selector: 'app-tarrifplan',
  templateUrl: './tarrifplan.component.html',
  styleUrls: ['./tarrifplan.component.scss']
})
export class TarrifplanComponent implements OnInit {
  header=true;


  constructor(private _router: Router,private _storeVal: StoreService) { }

  ngOnInit() {
   
  }
    backbuttons(){
      this._router.navigate(['dashboard']);
   }
  

}
