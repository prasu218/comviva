import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarrifplanComponent } from './tarrifplan.component';

describe('TarrifplanComponent', () => {
  let component: TarrifplanComponent;
  let fixture: ComponentFixture<TarrifplanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarrifplanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarrifplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
