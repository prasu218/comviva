import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherplanComponent } from './otherplan.component';

describe('OtherplanComponent', () => {
  let component: OtherplanComponent;
  let fixture: ComponentFixture<OtherplanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherplanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
