import { Product } from './../../../socialmedia/socialmedia.component';
import { getProductName } from './../../../../reducers/index';
import { Action } from '@ngrx/store';
import { initialState } from './../../../../reducers/menubar';
import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { StoreService } from '../../../../plugins/datastore';
import { Router } from '@angular/router';
import { BalanceService } from '../../../../plugins/balance/airtimebalances.service';
import { TarrifplanService } from '../../../../plugins/tarrifplan/tarrifplan.service';

declare var $: any;


@Component({
selector: 'app-otherplan',
templateUrl: './otherplan.component.html',
styleUrls: ['./otherplan.component.scss']
})


export class OtherplanComponent implements OnInit {
public active_nav: string;
public tariffPlanNameId='';
public msisdn:string;
public isChecked: any;
public check_postpaid:any;
public plannameval:string;
public tarrifPlanList:any=[];
public plusminusVar=[];
public productidsplit: any;
public ActionId: any;
public showPersonalError = false;
public showPersonalPlan = false;
public showPersonalMigrate = false;
public showPersonalCompare = false;
public showBusinessError = false;
public showBusinessPlan = false;
public showBusinessMigrate = false;
public showBusinessCompare = false;
public ActiveplanObj:any=[];
public tarffiffPlanObj:any=[];
public tariffPlanArr:any;
public checkedValue:any;
public ProductId: any;
public selectplan:boolean=true;
public migrate:boolean=true;
public ComparePlan:any;
public selectedCount: any;
public ExistOnPlan = false;
public comparePlanNames = [];
public previousSelectedIndex: any = - 1;
public compareMode = false;
 public plusminusVarCost: boolean = false;
 public plusminusVar_1: boolean = false;
public showLoader : boolean = false;
public activeTariffPlanName: string = '';
public initialSelectedPlan: any;
public ProductCode: any;
public initialSelectedPlanbusiness: any;
public secondSelectedPlanbusiness: any;
public secondSelectedPlan: any
public numberOfSelectedPlans: number = 0;

//public planProductID_SC = { "186": "75", "184": "76", "44": "77", "42": "78", "46": "79", "1000": "80", "1009": "81", "1013": "82", "20": "102", "26": "103" };

public CONSUMER_PLANS_ID = ['77', '78', '79', '103', '739' , '263'];

public BUSINESS_PLANS_ID = ['71', '75', '76' ];

public personalPlansList: any = [];
public businessPlanList: any = [];
public tarifdetails:any={};
public formhide: boolean=true ;
public hideee: boolean=false ;
public failure:boolean=false;
public confirm:boolean=false;


public message :string;
public checkbox_1:[];
public selectedCheckBox=[];
public selectedBusinessPlan: any = {};
businesstarriff: any;
personaltarrif: any;

@Output() selected_tab = new EventEmitter<string>();
  router: any;

constructor(private _storeVal: StoreService,private _router: Router,
  private _balance:BalanceService,private _tarrifplan:TarrifplanService) {
    this.BusinessTarrifPlanList();
   }

ngOnInit() {

  this.BalanceDetails();
  this.plusminusVar[0]=true;
  this.selectedCheckBox[0] = true;
  this.selectedCount = 1;
  localStorage.setItem("selectedCounter", "0");
  localStorage.setItem("selectedCounterBusiness", "0");
  document.getElementById('tarr_2').style.display = 'none';
  this.showPersonalPlan = true;
  this.showBusinessPlan = true;
}
  
FieldsChange = function(values:any) {
  //console.log(values.currentTarget.checked);
}
BalanceDetails() {
  let msisdn = this._storeVal.getMSISDNData();
  let subType = this._storeVal.getSubsTypeData();
  this.check_postpaid = subType;
 

  if (true) {
    this._balance.getBalanceAPI(msisdn).then(resp => {

  
    this.tariffPlanNameId = resp.result[0].planName;
 
    });
  }
}
    
 ngAfterViewInit() {
    $(document).ready(function () {
      $('.tabs').click(function (event) {
        $('.personal section > div ').toggleClass('active');
        $('.personal > div').toggleClass('activetab');
      });
    });
  }

   async BusinessTarrifPlanList() {
    this.activeTariffPlanName = localStorage.getItem('ACTIVE_PLAN_NAME');
    this.showLoader  = true;
    let msisdn = this._storeVal.getMSISDNData();
    this._tarrifplan.getcheckPlan(msisdn).then(async (resp) => {
      if (resp.ResoponseCode === 0 || resp.ResoponseCode === '0') {
        this.tarrifPlanList = await resp.ResponseData.ProductDetails.ProductDetails;
        this.checkedValue = this.tarrifPlanList[0].Action;
        const cheched = this.tarrifPlanList[0];
        this.showLoader  = false;
            this.personalPlansList = await this.tarrifPlanList.filter(
            tarriff =>{
            if(this.CONSUMER_PLANS_ID.indexOf(tarriff.ProductID) != -1) {
              return tarriff;
            } 
          }
        );
            this.businessPlanList = await this.tarrifPlanList.filter(
            tarriff =>{
            if(this.BUSINESS_PLANS_ID.indexOf(tarriff.ProductID) != -1) {
              return tarriff;
            } 
          }
        );
       
      }
    }).catch((err) => {
    });

    }

    
  
      

plusminus(index) {
  if (this.plusminusVar[index])
  this.plusminusVar[index] = false;
    else
    this.plusminusVar[index] = true;

}
  plusminuscompare() {
    if (this.plusminusVar_1)
      this.plusminusVar_1 = false;
    else
      this.plusminusVar_1 = true;
  }
  plusminuscomparecost() {
    if (this.plusminusVarCost)
      this.plusminusVarCost = false;
    else
      this.plusminusVarCost = true;
  }

  BusinesscheckboxChange = function (event, index) {
    const isChecked = event.target.checked;
    if (isChecked) {
      if(!this.initialSelectedPlanbusiness && !this.secondSelectedPlanbusiness) {
        this.initialSelectedPlanbusiness = this.businessPlanList[index];
        this.businessPlanList[index].checked = true;
        return;
      }
      if(this.initialSelectedPlanbusiness && !this.secondSelectedPlanbusiness) {
        this.secondSelectedPlanbusiness = this.businessPlanList[index];
        this.businessPlanList[index].checked = true;
      }
      if(this.initialSelectedPlanbusiness && this.secondSelectedPlanbusiness) {

        const oldIndex = this.businessPlanList.indexOf(this.secondSelectedPlanbusiness);
        this.businessPlanList[oldIndex].checked = false;
        this.secondSelectedPlanbusiness = this.businessPlanList[index];
        this.businessPlanList[index].checked = true;
      }
      //console.log('intial plan  => ',this.initialSelectedPlanbusiness);
      //console.log('second plan =>', this.secondSelectedPlanbusiness);
  } else {
      this.businessPlanList[index].checked = false;
      if(this.initialSelectedPlanbusiness == this.businessPlanList[index]) {
        if(this.secondSelectedPlanbusiness) {
          this.initialSelectedPlanbusiness = this.secondSelectedPlanbusiness;
          this.secondSelectedPlanbusiness  = null;
        } else {
          this.initialSelectedPlanbusiness = null;
        }
      }
      if(this.secondSelectedPlanbusiness == this.businessPlanList[index]) {
        this.secondSelectedPlanbusiness = null;
      }
    }
    }
PersonalcheckboxChange = function (event, index) {
const isChecked = event.target.checked;
if (isChecked) {
  if(!this.initialSelectedPlan && !this.secondSelectedPlan) {
    this.initialSelectedPlan = this.personalPlansList[index];
    this.personalPlansList[index].checked = true;
    return;
      }
  if(this.initialSelectedPlan && !this.secondSelectedPlan) {
    this.secondSelectedPlan = this.personalPlansList[index];
    this.personalPlansList[index].checked = true;
  }
  if(this.initialSelectedPlan && this.secondSelectedPlan) {
    const oldIndex = this.personalPlansList.indexOf(this.secondSelectedPlan);
    this.personalPlansList[oldIndex].checked = false;
    this.secondSelectedPlan = this.personalPlansList[index];
    this.personalPlansList[index].checked = true;
  }
  //console.log('intial plan  => ',this.initialSelectedPlan);
 // console.log('second plan =>', this.secondSelectedPlan);
    } else {
  this.personalPlansList[index].checked = false;
  if(this.initialSelectedPlan == this.personalPlansList[index]) {
    if(this.secondSelectedPlan) {
      this.initialSelectedPlan = this.secondSelectedPlan;
      this.secondSelectedPlan  = null;
    } else {
      this.initialSelectedPlan = null;
    }
  }
  if(this.secondSelectedPlan == this.personalPlansList[index]) {
    this.secondSelectedPlan = null;
  }
 }
}

ComparePlans(flag) {
      //BUSINESS PLAN
      let businessCheckbox = <any> document.getElementsByClassName("businessCheckbox") ;
      let holder_business =  document.getElementsByClassName("holder_business");
      let holder_business_name =  document.getElementsByClassName("holder_business_name");
      let holder_business_price = document.getElementsByClassName("holder_business_price");
       for(let i=0; i<businessCheckbox.length; i++) {
           if(businessCheckbox[i].checked) {
            const data = {
              'name':holder_business_name[i].innerHTML,  //this.tarrifPlanList[i].ProductName,
              'desc' :holder_business[i].innerHTML, //this.tarrifPlanList[i].Description,
              'cost': holder_business_price[i].innerHTML //this.tarrifPlanList[i].Price
            }
            this.comparePlanNames.push(data);
            this.compareMode = true;
          }
        }  
        //PERSONAL PLANS
        let personalCheckbox = <any> document.getElementsByClassName("personalCheckbox") ;
        let holder_personal_desc =  document.getElementsByClassName("holder_personal_desc");
        let holder_personal_name =  document.getElementsByClassName("holder_personal_name");
        let holder_personal_price = document.getElementsByClassName("holder_personal_price");
         for(let i=0; i<personalCheckbox.length; i++) {
             if(personalCheckbox[i].checked) {
              const data2 = {
                'name':holder_personal_name[i].innerHTML,  //this.tarrifPlanList[i].ProductName,
                'desc' :holder_personal_desc[i].innerHTML, //this.tarrifPlanList[i].Description,
                'cost': holder_personal_price[i].innerHTML //this.tarrifPlanList[i].Price
              }
              this.comparePlanNames.push(data2);
              this.compareMode = true;
            }
          }  
        /*
  this.formhide=false;
//  console.log('this is to compare', this.selectedBusinessPlan);
  if (flag) {
    for (let i = 0; i < this.selectedCheckBox.length; i++) {
      if (this.selectedCheckBox[i]) {
        const data = {
          'name': this.tarrifPlanList[i].ProductName,
          'desc' : this.tarrifPlanList[i].Description,
          'cost': this.tarrifPlanList[i].Price
        };
        this.comparePlanNames.push(data);
        if (this.comparePlanNames.length === 2) {
      
          this.compareMode = true;
       document.getElementById('tarr').style.display = 'none';
            document.getElementById('tarr_1').style.display = 'none';
            document.getElementById('tarr_2').style.display = 'block';
      
        }
      }
    }
  } else {
    this.compareMode = false;
    this.comparePlanNames.length = 0;
      document.getElementById('tarr_2').style.display = 'none';
  }
  
  */
}



  

  MigratePlans() {
    this.ActionId = this.initialSelectedPlan.Action;
//    console.log('this is the action Id first instance ', this.initialSelectedPlan.Action);
//    console.log('this is the action Id ', this.activeTariffPlanName);
    this.ProductId = this.ActionId.replace(/Subscription:/g, '');
//    console.log('this is the action Id FOR pROD ', this.ProductId);
    this.confirm=false;
    let msisdn = this._storeVal.getMSISDNData();
//    console.log('this is another clicked action', this.ProductId);
//    console.log('selected check box inside MigratePlansMethod ==>', this.isChecked);
   
    this._tarrifplan.migratePlan(msisdn, this.ProductId).then((resp) => {
//    console.log('selected check box inside ==>', this.initialSelectedPlan);
//    console.log('this is another migrtae  here', resp);
      if (resp.Status =='SUCCESS') {
        this.message =  resp.Status;       
        this.formhide = false;
        this.ExistOnPlan = false;
        this.hideee=true;
 
            
      } else {     
                 
        this.message = resp.Status;
        this.failure = true;
        this.formhide = false;
        this.hideee=false;
            
      } }).catch((err) => {
    });
  }

//  migrate plan
//   MigratePlan() {
//     console.log('this is migrate plan ahead ahead ');
//     if (this.initialSelectedPlan.ProductName === this.activeTariffPlanName) {
//       console.log('this is selectedBusiness', this.selectedBusinessPlan.ProductName);
//       console.log('this is selectedBusiness', this.activeTariffPlanName);
//       this.ExistOnPlan = true;
//       this.formhide = false;
//     } else {
//     this.confirm = true;
//     this.formhide = false;
//     this.ExistOnPlan = false;
//   }
// }


//migrate plan
MigratePlan(data) {
  //this.confirm=true;
  this.formhide = false;
  this.ProductCode = data.Action.replace('Subscription:','');
  console.log('the productId is ' , this.ProductCode);
  this.tarifdetails['productid']= this.ProductCode;
  this.tarifdetails['productname']= data.ProductName;
  this.tarifdetails['productDescription'] = data.Description;
  //console.log(' this is the name  of where you want to migrate to ', data.ProductName);
  //console.log(' this is the active name' , this.activeTariffPlanName);
  if(this.ProductCode == "NACT_NG_Others_263"){
    this._router.navigate(['buybundles/Data&VoiceBundles']);
  }else{
  if (data.ProductName === this.activeTariffPlanName ) {
    this.ExistOnPlan = true;
    this.formhide = false;
    this.confirm = false;
 } else {
  this.confirm = true;
  this._storeVal.settopupDetails(this.tarifdetails);
  this._router.navigate(['otherplan/proceed']);
 }
  }
}


// Proceed() {
//   this._router.navigate(['otherplan/proceed']);
//   }


  loadtopup() {
    this.ExistOnPlan = false;
    this.formhide=true;
    this.failure=false; 
    this.hideee=false;
    // window.location.reload();
    // this.topup_card.reset();
    // this.topup_card1.reset();
  }
}

