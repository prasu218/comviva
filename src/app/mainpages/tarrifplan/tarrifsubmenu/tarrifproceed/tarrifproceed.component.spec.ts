import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarrifproceedComponent } from './tarrifproceed.component';

describe('TarrifproceedComponent', () => {
  let component: TarrifproceedComponent;
  let fixture: ComponentFixture<TarrifproceedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarrifproceedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarrifproceedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
