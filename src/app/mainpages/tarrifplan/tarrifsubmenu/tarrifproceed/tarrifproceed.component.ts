import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { StoreService } from '../../../../plugins/datastore';
import { Router } from '@angular/router';
import { BalanceService } from '../../../../plugins/balance/airtimebalances.service';
import { TarrifplanService } from '../../../../plugins/tarrifplan/tarrifplan.service';
// import { Location } from '@angular/common';
@Component({
  selector: 'app-tarrifproceed',
  templateUrl: './tarrifproceed.component.html',
  styleUrls: ['./tarrifproceed.component.scss']
})
export class TarrifproceedComponent implements OnInit {
public header=true;
public msisdn:any="";
public ProductID:any="";
public Productname:any="";
public message:any="";
public tarrif_value:any={};
public suceesDiv:boolean=false;
public showLoader:boolean=false;
public failurDiv:boolean=false;
public showsuccessdiv:boolean=true;
public message1:any="";
public ProductDescription: any;

  constructor(private location: Location,private _storeVal: StoreService,private _router: Router,private _balance:BalanceService,private _tarrifplan:TarrifplanService) { }


  ngOnInit() {
    this.tarrif_value=this._storeVal.gettopupDetails();
    this.msisdn=this._storeVal.getMSISDNData();
    this.Productname=this.tarrif_value.productname;
    this.ProductID=this.tarrif_value.productid;
    this.ProductDescription = this.tarrif_value.productDescription;
    
  }
  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this._router.navigate(['dashboard']);
    }
   }
  MigratePlans() {
    this.showLoader=true;
    // this.confirm=false;
    // let msisdn = this._storeVal.getMSISDNData();
    // let productidsplit = this.checkedValue.split("Subscription:")
    // var ProductID = productidsplit[1];
    this._tarrifplan.migratePlan(this.msisdn, this.ProductID).then((resp) => {

      if (resp.Status =='SUCCESS') {
        this.message1="Success"   
        this.message =  resp.Status;       
        this.showsuccessdiv=false;
        this.suceesDiv=true;
        this.showLoader=false;
 
            
      }else{ 
        this.message1="Failure" ;
        this.message =resp.Status;
        this.failurDiv=true;
        this.showsuccessdiv=false; 
        this.showLoader=false;
            
      }



    }).catch((err) => {
      this.message1="Failure"    
        this.message ="Request failed due to some Server error";
        this.failurDiv=true;
        this.showsuccessdiv=false; 
        this.showLoader=false;
    });
  }
}
