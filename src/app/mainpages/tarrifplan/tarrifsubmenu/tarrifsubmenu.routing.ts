import { Routes } from '@angular/router';
import { TarrifplanComponent } from '../tarrifplan.component';
import { ActiveplanComponent } from './activeplan/activeplan.component'
import { OtherplanComponent } from './otherplan/otherplan.component';
import { TarrifproceedComponent } from './tarrifproceed/tarrifproceed.component';
import { AuthGuard } from 'src/app/auth/auth.guard';

export const Tarrifmenuroutes: Routes = [{
    path: 'tarrifplan', component: TarrifplanComponent,
    children: [
        {
            path: '',
            component: ActiveplanComponent,
            pathMatch: 'full',
            canActivate: [AuthGuard]
        },
        {
            path: 'activeplan',
            component: ActiveplanComponent,
            pathMatch: 'full',
            canActivate: [AuthGuard]
        },
        {
            path: 'otherplan',
            component: OtherplanComponent,
            pathMatch: 'full',
            canActivate: [AuthGuard]
        },
    ]
},{
    path: 'otherplan/proceed',
    component: TarrifproceedComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
}];

