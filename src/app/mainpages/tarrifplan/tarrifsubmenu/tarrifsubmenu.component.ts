import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-tarrifsubmenu',
  templateUrl: './tarrifsubmenu.component.html',
  styleUrls: ['./tarrifsubmenu.component.scss']
})
export class TarrifsubmenuComponent implements OnInit {
  myVar:any;
  @Input() active_nav: string;


  rechargeNavElements = [

    {id:'activeplan',value:'Active plans', source :'assets/images/tariff_plans.svg',},
    {id:'otherplan',value:'Other plans',source :'assets/images/baseline-more-24px.svg',},
  
  ];

  constructor(private _router: Router,private location: Location) { 
    
  }

  ngOnInit() {
    //this.scrollSubMenu();
    // this.myVar = setTimeout(() => {
    //   this._router.navigate(['/tarrifplan/activeplan']);
    //   }, 2000);
    }

      
  backbuttons(){
    clearTimeout(this.myVar);
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this._router.navigate(['dashboard']);
    }
   
    document.getElementById('tarr').style.display = 'block';
    document.getElementById('tarr_1').style.display = 'block';
  }

  ngAfterContentChecked(){
    this.active_nav = this._router.url.split('/')[2];
    if(this._router.url === "/tarrifplan"){
      this.active_nav  = "activeplan";
    }
  }
  
 onLoadMdNav(selectedData){
    this._router.navigate(['tarrifplan',selectedData.id]);
     this.active_nav = selectedData.value;
  }

}
