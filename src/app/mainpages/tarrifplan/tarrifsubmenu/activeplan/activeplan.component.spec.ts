import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveplanComponent } from './activeplan.component';

describe('ActiveplanComponent', () => {
  let component: ActiveplanComponent;
  let fixture: ComponentFixture<ActiveplanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveplanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
