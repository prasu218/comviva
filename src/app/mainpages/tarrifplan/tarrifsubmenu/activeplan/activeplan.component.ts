import { Component, OnInit } from '@angular/core';
import { BalanceService } from '../../../../plugins/balance/airtimebalances.service';
import { Router } from '@angular/router';
import { StoreService } from '../../../../plugins/datastore';
import { TarrifplanService } from '../../../../plugins/tarrifplan/tarrifplan.service';

@Component({
selector: 'app-activeplan',
templateUrl: './activeplan.component.html',
styleUrls: ['./activeplan.component.scss']
})
export class ActiveplanComponent implements OnInit {
tariffPlanName:any;
check_postpaid: any;
description: string = '';
public tarrifPlanList: any = [];
public plusminusVar = [];
public ActiveplanObj: any = [];
public tarffiffPlanObj: any = [];
public tariffPlanArr: any;
public active_nav: string;
public name: string;
public descriptions: string;
public text: string;
public showLoader : boolean = false;
public planProductID_SC = { "186": "75", "184": "76", "44": "77", "42": "78", "46": "79", "1000": "80", "1009": "81", "1013": "82", "20": "102", "26": "103" };

constructor(private _storeVal: StoreService, private _router: Router, private _balance: BalanceService, private _tarrifplan: TarrifplanService) { }

ngOnInit() {
  this.BalanceDetails();
  this.TarrifPlanList();
}

// public tariffPlanNameId: any;
BalanceDetails() {
    let msisdn = this._storeVal.getMSISDNData();
    //console.log("msisdn BundleDetails == ",msisdn);
    let subType = this._storeVal.getSubsTypeData();
    this.check_postpaid = subType;
    this.showLoader = true;  
    if (subType == "prepaid" || subType == "Prepaid" || subType == "Pre-paid")
    {
      this._balance.getBalance(msisdn,"1").then(resp => {
      this.tariffPlanName = resp[1];
      this.showLoader =false;  
      localStorage.setItem('ACTIVE_PLAN_NAME', this.tariffPlanName);
      });
      }
    else
    {
      this._balance.getBalance(msisdn,"2").then(resp => {
      
        this.tariffPlanName = resp[1];
        this.showLoader = false;  

    });


  }
}

TarrifPlanList() {


  let msisdn = this._storeVal.getMSISDNData();
  this._tarrifplan.getcheckPlan(msisdn).then((resp) => {
    if (resp.ResoponseCode === 0 || resp.ResoponseCode === "0") {
      this.tarrifPlanList = resp.ResponseData.ProductDetails.ProductDetails;

      
    }
  }).catch((err) => {
   // console.log("err", err);
  });

}

}
