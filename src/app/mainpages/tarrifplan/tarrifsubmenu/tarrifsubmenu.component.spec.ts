import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarrifsubmenuComponent } from './tarrifsubmenu.component';

describe('TarrifsubmenuComponent', () => {
  let component: TarrifsubmenuComponent;
  let fixture: ComponentFixture<TarrifsubmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarrifsubmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarrifsubmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
