import { TestBed } from '@angular/core/testing';

import { TarrifplanService } from './tarrifplan.service';

describe('TarrifplanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TarrifplanService = TestBed.get(TarrifplanService);
    expect(service).toBeTruthy();
  });
});
