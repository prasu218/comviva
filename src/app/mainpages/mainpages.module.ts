import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//import { MainpagesRoutingModule } from './mainpages-routing.module';
import { MainpagesComponent } from './mainpages.component';
import { LoginModule } from './login/login.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThemeModule } from 'src/app/theme/theme.module';
import { DebitCardComponent } from './recharge/recharge-submenu/debit-card/debit-card.component';
import { TopupCardComponent } from './recharge/recharge-submenu/topup-card/topup-card.component';
import { SendAirtimeComponent } from './recharge/recharge-submenu/send-airtime/send-airtime.component';
import { BorrowAirtimeComponent } from './recharge/recharge-submenu/borrow-airtime/borrow-airtime.component';
import {SuccessComponent} from './success';
import { BuybundlessuccessComponent } from './buybundlessuccess/buybundlessuccess.component';

import { BuybundlesfailureComponent } from './buybundlesfailure/buybundlesfailure.component';
import { FailureComponent } from './failure/failure.component';
import { CallComponent } from './account-history/account-history-submenu/call/call.component';
import { SmsComponent } from './account-history/account-history-submenu/sms/sms.component';
import { DataComponent } from './account-history/account-history-submenu/data/data.component';
import { OnlineRechargeComponent } from './account-history/account-history-submenu/online-recharge/online-recharge.component';
import { SocialmediaComponent } from './socialmedia/socialmedia.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { OtpprofileComponent } from './otpprofile/otpprofile.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { BorrowAirtimeProceedComponent } from '../mainpages/recharge/recharge-submenu/borrow-airtime/borrow-airtime-proceed/borrow-airtime-proceed.component';
import { TarrifproceedComponent } from './tarrifplan/tarrifsubmenu/tarrifproceed/tarrifproceed.component';
import { TopupProceedComponent } from './recharge/recharge-submenu/topup-card/topup-proceed/topup-proceed.component';
import { ProceedComponent } from './complaints/complaints-submenu/log-complaints/proceed/proceed.component';
//import { SwitchmainAccountComponent } from './switchmainaccount/switchmainaccount.component';

@NgModule({
  declarations: [CallComponent, SmsComponent, DataComponent, OnlineRechargeComponent, MainpagesComponent,DebitCardComponent,BorrowAirtimeComponent,TopupCardComponent, SendAirtimeComponent,SuccessComponent,BuybundlessuccessComponent, BuybundlesfailureComponent, FailureComponent,SocialmediaComponent,ConfirmationComponent,OtpprofileComponent ,PrivacypolicyComponent,BorrowAirtimeProceedComponent,TarrifproceedComponent,TopupProceedComponent,ProceedComponent ],
  imports: [
    CommonModule,
    //MainpagesRoutingModule,
   
    LoginModule,
    ThemeModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class MainpagesModule { }
