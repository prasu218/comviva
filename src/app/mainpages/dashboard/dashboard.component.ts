import { Component, OnInit } from '@angular/core';
import { config, purchaseTune_Response } from '../../config';
import { StoreService } from '../../plugins/datastore'

import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';

import { BalanceService } from '../../plugins/balance/airtimebalances.service';
import { BundleBalanceService } from '../../plugins/balance/bundlebalances.service';
import { ViewBillDetailsService } from '../../plugins/viewbilldetails/viewbilldetails.service';
import { SubscriptionService } from '../../plugins/subscription/subscription.service';
import { Common_Strings } from '../../strings/strings';

import { AddAccountService } from 'src/app/plugins/addaccount';

declare var $: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],

})
export class DashboardComponent implements OnInit {
  bgColorpp = 'grey';
  header = true;
  existing_cust: FormGroup;
  check_postpaid = 'Postpaid';
  public tablehidden: boolean = true;
  public msisdn: string;
  //public Airtime:any;

  public airtimeBalance: any;
  public airtimeBonusBalance: any;
  public creditLimitBalance: any;
  public creditLimitAssignedBalance: any;
  public outstandingBalance: any;
  public tariffPlanName: any;
  public bundleName: any;
  public bundleType: any;
  public bundleExpiry: any;
  public bundleValue: any;
  public bundleSubscriptionName: any;
  public bundleSubscriptionDescription: any;
  public OutstandigBal: any;
  public dataBalance: any = 0;
  public dataBonusBalance: any = 0;
  public otherDataBalance: any = [];
  public billDetails: any;
  public firstName: any;
  public yellow_curve_bottom: boolean = true;
  public yellow_curve_bottom_white: any;
  public yellow_curve_bottom_1: any;
  public subscriptionStatus: boolean;
  public ProductNames = [];
  public desc: boolean = false;
  public showTable: boolean = false;
  public message: string;
  public message1: string;
  public formhide: boolean = true;
  public hideee: boolean = false;
  public failure: boolean = false;
  public hideeeautorenewal: boolean = false;
  //public showTableview: any;
  public showTableview: boolean = false;
  public curve_bottom: any;
  public bundle: any;
  public other: any;
  public ProductID: any;
  public tooglebutton: boolean = false;
  public erromessage: any;
  public TransectionNumber: any;
  public DueDate: any;
  public OutstandingAmount: any;
  public TotalInvoiceAmount: any;
  public TotalClearedAmount: any;
  public viewopenbills: boolean = true;
  public mySubscriptionData = [];
  public toggle = [];
  public viewbills: any;
  public Autorenewal: any;

  public primaryNumber: any;
  numberLists = [];
  linkedMainnumbers = [];
  linkedGotten: boolean;
showLoader: boolean = false;

  tblhidden :boolean=false;
   primaryNum;
   switched: boolean;
  parentNumber: any ="";
  mainnumber: any;

  constructor(private fb: FormBuilder, private _storeVal: StoreService, private _router: Router,
     private _balance: BalanceService, private _bunldeBalance: BundleBalanceService, 
     private _viewbilldetails: ViewBillDetailsService, private _subscriptionval: SubscriptionService,
     private addAccount: AddAccountService) {
		this.getPrimaryNumber()
 }

  ngOnInit() {

    

    this.existing_cust = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(config.MIN_MSISDN_LENGTH), Validators.maxLength(config.MAX_MSISDN_LENGTH)]],
    });
    this.BundleDetails();
    this.BalanceDetails();
    this.getMySubscriptionListUI();

    if (this._storeVal.getMSISDNData() && this._storeVal.getMSISDNData() != "") {
      if (this._storeVal.getSecondaryAccount()) {
        this.firstName = this._storeVal.getFirstNameData();
      } else {
        this.firstName = this._storeVal.getFirstNameData();
      }
    }
    
    $(window).resize(function () {
     // const header_height = document.getElementById('header-main-mobile').clientHeight;
    //  document.getElementById('main-header-row-div').style.marginBottom = header_height.toString() + "px";
    });
    const header_height = document.getElementById('header-main-mobile').clientHeight;
    //console.log('Header Height - ' + header_height);
   // document.getElementById('main-header-row-div').style.marginBottom = header_height.toString() + "px";

  }


  async getPrimaryNumber(){
    let number = JSON.parse(localStorage.getItem("manna")).SecondaryNumber;
    if(this._storeVal.getMSISDNData().startsWith('234')){
      this.primaryNumber = this._storeVal.getMSISDNData().slice(3, 13);
    }else{
      this.primaryNumber = this._storeVal.getMSISDNData();
    }
    

 
    this.mainnumber =  JSON.parse(localStorage.getItem("manna")).SecondaryNumber;


              if(localStorage.getItem("switched") == "true"){
                this.switched = true;
              }else{
                this.switched = false;
              }

            let chakramain = "";

            if( this._storeVal.getMainToken() != null){
              //charamain = sessionStorage.getItem("chakramain");
              chakramain =  this._storeVal.getMainToken();
            }

        console.log("Switched status",this.switched)
    if(this.switched){
console.log("na chakrramain be this boss", chakramain);
      this.addAccount.getSwitchedLinkedAccounts(this.primaryNumber, chakramain).then(data => {
        console.log("This is supposed number ", number);
        console.log("Bros this data ", data);
        if (data && data.StatusCode == 0) {
          console.log("Linked nums ", data.results)
          console.log("primary numb ", this.primaryNumber);
        this.numberLists = this.numberLists || []
         this.numberLists = [...this.numberLists, ...data.results];
         console.log("numberlista ", this.numberLists)
         const filteredArray  = this.numberLists.filter(val => val.SecondaryNumber !== this.primaryNumber )
         console.log("This is fltered array ", filteredArray);
           this.numberLists = filteredArray;  
         
    
        }
        else if(data && data.StatusCode == '105'){
     
        }
       });

    }else{
      this.addAccount.getLinkedAccounts(number).then(data => {
        console.log("This is supposed number ", number);
        console.log("Bros this data ", data);
        if (data && data.StatusCode == 0) {
          console.log("Linked nums ", data.results)
          console.log("primary numb ", this.primaryNumber);
        this.numberLists = this.numberLists || []
         this.numberLists = [...this.numberLists, ...data.results];
         console.log("numberlista ", this.numberLists)
        //  let signedNumb = {
        //       SecondaryNumber: this.primaryNumber.slice(3, 13),
        //       StartDate: "lap",          
        //       NickName:	"stan"
        //      }val.SecondaryNumber
       // if(this.numberLists.indexOf(this.primaryNumber) != -1){}
      //  {
      //   console.log("This is arr num ", val.SecondaryNumber);
      //   console.log("This is primary numbas", this.primaryNumber);
  const filteredArray  = this.numberLists.filter(val => val.SecondaryNumber !== this.primaryNumber )
        console.log("This is fltered array ", filteredArray);
          this.numberLists = filteredArray;  
        
       
      // this.numberLists = this.numberLists.filter( (numList) =>{ this.primaryNumber != numList.SecondaryNumber});
         localStorage.setItem("linkednumbers", JSON.stringify(this.numberLists));
        }
        else if(data && data.StatusCode == '105'){
         //  this.addedno = false;
         //  this.responseLoaded=true;
         //  this.secondaryNumberToBeAdded = 5;
     
        }
      
       });
    }
     
      
    

  }


  BalanceDetails() {
    let msisdn = this._storeVal.getMSISDNData();

    // this._storeVal.setEmailData(resp.customer_Profile.email);
    // this._storeVal.setAlternateNumberData(resp.customer_Profile.alternateNumber);
    // this._storeVal.setFamilyNameData(resp.customer_Profile.familyName);
    this.firstName = this._storeVal.getFirstNameData();

    let subType = this._storeVal.getSubsTypeData();
    this.check_postpaid = subType;
    if (subType == "prepaid" || subType == "Prepaid" || subType == "Pre-paid") {
      this._balance.getBalance(msisdn, "1").then(resp => {
        this.airtimeBalance = resp[0];
        this.tariffPlanName = resp[1];
      });
    }
    else {
      // let resp = this._balance.getBalance(msisdn,"2");
      // this.creditLimitBalance = resp[0];
      // this.tariffPlanName = resp[1];

      this._balance.getBalance(msisdn, "2").then(resp => {
        //this.creditLimitBalance = resp[0];
        this.airtimeBalance = resp['0'];
        this.tariffPlanName = resp[1];
        this.creditLimitBalance = resp[2];
      });

      this._balance.getCreditBalances(msisdn).then(resp => {
        this.creditLimitAssignedBalance = resp[0];
        this.outstandingBalance = resp[1];
        //this.creditLimitBalance = resp[0];
        //console.log("this.creditLimitBalance",this.creditLimitBalance);
      });
    }


    // this.airtimeBonusBalance = this._balance.getAirtimeBonusBalance(msisdn);
    this._balance.getAirtimeBonusBalance(msisdn).then(resp => {
      this.airtimeBonusBalance = resp;
    });




  }


  balancesapi() {
    if(this.tablehidden==true){
      this.tablehidden = false;
      this.tblhidden=true;
      this.viewopenbills=true;
      document.getElementById('ylw').style.background = "#fff";
      $('#icn1').removeClass('fa-chevron-down');
      $('#icn1').addClass('fa-chevron-up');
      $('#icn2').removeClass('fa-chevron-up');
      $('#icn2').addClass('fa-chevron-down');
    }else{
      this.tablehidden = true;
      this.tblhidden=false;
      this.viewopenbills=true;
      document.getElementById('ylw').style.background = "#f2f2f2";
      $('#icn1').removeClass('fa-chevron-up');
      $('#icn1').addClass('fa-chevron-down');
    }

  }
  crossIcon() {
    this.tablehidden = true;
    this.viewopenbills = true;
    this.tblhidden=false;
    document.getElementById('ylw').style.background = "#f2f2f2";
    $('#icn1').removeClass('fa-chevron-up');
    $('#icn1').addClass('fa-chevron-down');
    $('#icn2').removeClass('fa-chevron-up');
    $('#icn2').addClass('fa-chevron-down');
  }
  close_other_balance() {
    this.tablehidden = true;
    this.viewopenbills = true;
   this.tblhidden=false;
   document.getElementById('ylw').style.background = "#f2f2f2";
   $('#icn1').removeClass('fa-chevron-up');
   $('#icn1').addClass('fa-chevron-down');
   $('#icn2').removeClass('fa-chevron-up');
   $('#icn2').addClass('fa-chevron-down');
  }

  //detailsapi(){
  //console.log('detailsapi')
  //this.tablehidden = false;
  //}

  BundleDetails() {
    let msisdn = this._storeVal.getMSISDNData();

    this._bunldeBalance.getDataBalance(msisdn).then(bundleResp => {
      this.dataBalance = bundleResp[0];
      this.dataBonusBalance = bundleResp[1];
    });

    this.otherDataBalance = this._bunldeBalance.getOtherDataBalance(msisdn).then(resp => {


      this.other = resp[0];
      this.showTable = this.other[0].hasOwnProperty('bundleName');
    });

  }



  subscriptionapi() {

  }
  contact() {
    this._router.navigate(['help1'])
  }
  faqs() {
    this._router.navigate(['/faqs']);
  }
  detailsapi() {

    let msisdn = this._storeVal.getMSISDNData();
    if(this.viewopenbills==true){
    this.viewopenbills = false;
    this.tblhidden=true;
    this.tablehidden=true;
    document.getElementById('ylw').style.background = "#fff";
    $('#icn2').removeClass('fa-chevron-down');
    $('#icn2').addClass('fa-chevron-up');
    $('#icn1').removeClass('fa-chevron-up');
    $('#icn1').addClass('fa-chevron-down');
  }else{
    this.viewopenbills = true;
    this.tblhidden=false;
    this.tablehidden=true;
    document.getElementById('ylw').style.background = "#f2f2f2";
    $('#icn2').removeClass('fa-chevron-up');
      $('#icn2').addClass('fa-chevron-down');
  }
    this.yellow_curve_bottom = false;
    this.yellow_curve_bottom_white = true;
    this.yellow_curve_bottom_1 = false;
    this._viewbilldetails.viewbilldetailsAPI1(msisdn).then((resp) => {
      this.viewbills = resp[0];
          // this.showTableview = this.other[0].hasOwnProperty('TransectionNumber');
      // resp.billDetails[1].TransectionNumber;
      //  console.log('this.bill',resp.billDetails[1].TransectionNumber);
      if (resp.StatusCode === '0') {
        this.showTableview = true;
        this.TransectionNumber = resp.billDetails[1].TransectionNumber;
        this.DueDate = resp.billDetails[1].DueDate;

        this.OutstandingAmount = Common_Strings.OPCO_CURRENCY + resp.billDetails[1].OutstandingAmount;

        this.TotalInvoiceAmount = Common_Strings.OPCO_CURRENCY + resp.billDetails[1].TotalInvoiceAmount;
        this.TotalClearedAmount = Common_Strings.OPCO_CURRENCY + resp.billDetails[1].TotalClearedAmount;

      } else {
        this.billDetails = "Not available";
      }
    })
    //.catch((err) => {
    //console.log("viewbilldetails"+0);
    //});
  }

  //subscriptionapi(target){
  //target = document.getElementsByClassName('.target');
  //target.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});

  //}




  getMySubscriptionListUI() {
   this.showLoader= true;
    let msisdn = this._storeVal.getMSISDNData();
    this._subscriptionval.getMySubscriptionList(msisdn)
      .then((resp) => {
    this.showLoader= false;
        if (Number(resp.ResoponseCode) == 0) {
          this.ProductNames = resp.ResponseData.products.ProductDetails;
          for (let i = 0; i < this.ProductNames.length; i++) {
            if (this.ProductNames[i].Validity > 1) {
              this.ProductNames[i].ValidityUnit = "Days";
            }
          }
          this.ProductID = resp.ResponseData.products.ProductDetails;
          this.Autorenewal = resp.ResponseData.products.ProductDetails[0].AutoRenewal;
          this.desc = this.ProductNames[0].hasOwnProperty('ProductID');

          //this.tooglebutton=false;


        }
        else if (Number(resp.ResoponseCode) == 0 && resp.ResponseData.products.ProductDetails == '') {
          this.ProductNames = ["Sorry, you do not have any subscribed bundles"];
          this.erromessage = this.ProductNames;
          this.tooglebutton = true;
        }
        for (let i = 0; i < this.ProductNames.length; i++) {
          if (this.ProductNames[i].AutoRenewal === "true") {
            this.toggle[i] = true;
          }
          else {
            this.toggle[i] = false;
          }
        }
      })
      .catch((err) => {
        this.ProductNames = ["Sorry you do not have any subscribed bundles"];
        //this.ProductID=[];
        //this.tooglebutton=true;
        this.erromessage = this.ProductNames;
      });
  }

  sub(event) {

    if (this.toggle[event] === false) {

      let msisdn = this._storeVal.getMSISDNData();
      this._subscriptionval.vasSubscription(msisdn, this.ProductNames[event].ProductID).then((resp) => {
      this.bundleSubscriptionName = this.ProductNames[event].ProductName;
      this.bundleSubscriptionDescription = this.ProductNames[event].ProductDescription;
        if (resp) {
          this.toggle[event] = true;
        }
        this.message = resp.status_message;
        this.formhide = false;
        this.hideee = true;
        this.hideeeautorenewal = false;

      }).catch((err) => {
        this.failure = true;
        this.formhide = false;
        this.message = 'autorenewal off';
      });;
    }
    else {
      this.toggle[event] = false;
      let msisdn = this._storeVal.getMSISDNData();
      this._subscriptionval.unSubscribeVas(msisdn, this.ProductNames[event].ProductID).then((resp) => {
      this.bundleSubscriptionName = this.ProductNames[event].ProductName;
      this.bundleSubscriptionDescription = this.ProductNames[event].ProductDescription;
        this.hideeeautorenewal = true
        this.formhide = false;
        this.hideee = false;

      }).catch((err) => {
        this.failure = true;
        this.formhide = false;
        this.message = 'autorenewal off';
      });
    }
  }


  ngAfterViewInit() {
   // $(".other").click(function () {
     // $('#ylw').css("background-color", "#fff");
    //});
    $(".close_btn").click(function () {
      $('#ylw').css("background-color", "#f2f2f2");
      $('#icn1').removeClass('fa-chevron-up');
      $('#icn1').addClass('fa-chevron-down');
      $('#icn2').removeClass('fa-chevron-up');
      $('#icn2').addClass('fa-chevron-down');
    });
  }

}



 


