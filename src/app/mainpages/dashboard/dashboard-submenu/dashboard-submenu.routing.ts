import { Routes } from '@angular/router';
import {DashboardComponent} from '../dashboard.component';
import { MysubscriptionComponent } from './mysubscription/mysubscription.component';
import { AuthGuard } from 'src/app/auth/auth.guard';


export const dashboardmenuroutes: Routes = [{
    path: 'dashboard', component:DashboardComponent,
    children:[
        { 
            path: 'mysubscription', 
            component: MysubscriptionComponent, 
            pathMatch: 'full', 
            canActivate: [AuthGuard]
        },
        // {
        //     path: '',
        //     component: MysubscriptionComponent,
        //     pathMatch: 'full' 
        //   },
    ]
}]