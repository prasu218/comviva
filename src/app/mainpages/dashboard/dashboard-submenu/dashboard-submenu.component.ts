import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-submenu',
  templateUrl: './dashboard-submenu.component.html',
  styleUrls: ['./dashboard-submenu.component.scss']
})
export class DashboardSubmenuComponent implements OnInit {
  @Input() active_nav: string;
  showLoader: boolean = false;
  dashboarddNav=[
    {id:'mysubscription',value:'My',value1:'subscriptions', source :'assets/images/subscription.svg'}
  ]
  constructor(private _router: Router) { }

  ngOnInit() {
  }
  ngAfterContentChecked(){
   this.showLoader = true;
    this.active_nav = this._router.url.split('/')[2];
    this.showLoader = false;
    if(this._router.url === "/dashboard"){
      this.active_nav  = "mysubscription";
    }
  }
  
 onLoadMdNav(selectedData){
    this._router.navigate(['dashboard',selectedData.id]);
   
  }
}
