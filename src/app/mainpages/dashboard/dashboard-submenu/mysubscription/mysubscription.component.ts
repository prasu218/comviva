import { Component, OnInit,Input, SimpleChanges,EventEmitter,Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mysubscription',
  templateUrl: './mysubscription.component.html',
  styleUrls: ['./mysubscription.component.scss']
})
export class MysubscriptionComponent implements OnInit {
  @Output() sub = new EventEmitter();

  @Input() ProductNames=[];
  @Input() erromessage='';
  //@Input() Productid=[];
    @Input() ProductID=[];
	  @Input() checked;
  
  constructor(private _router: Router) { }

  ngOnInit() {
  }
 
}
