import { Component, OnInit,Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mtnstore-submenu',
  templateUrl: './mtnstore-submenu.component.html',
  styleUrls: ['./mtnstore-submenu.component.scss']
})
export class MtnstoreSubmenuComponent implements OnInit {
  @Input() active_nav: string;
  rechargeNavElements = [

     {id:'mtnstores', value:'MTN Service', source :'assets/logo/rechargel.svg',},
        
  ];

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  ngAfterContentChecked(){
    this.active_nav = this._router.url.split('/')[2];
    if(this._router.url === "/mtnservicestores"){
      this.active_nav  = "mtnstores";
    }
  }

  onLoadMdNav(selectedData){
    this._router.navigate(['mtnservicestores',selectedData.id]);
    this.active_nav = selectedData.id;
  }

}
