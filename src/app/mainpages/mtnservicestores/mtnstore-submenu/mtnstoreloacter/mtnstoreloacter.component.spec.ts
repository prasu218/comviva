import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MtnstoreloacterComponent } from './mtnstoreloacter.component';

describe('MtnstoreloacterComponent', () => {
  let component: MtnstoreloacterComponent;
  let fixture: ComponentFixture<MtnstoreloacterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MtnstoreloacterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MtnstoreloacterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
