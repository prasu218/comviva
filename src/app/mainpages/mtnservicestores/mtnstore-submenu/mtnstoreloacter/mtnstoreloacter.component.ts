import { Component, OnInit, Input, Output,EventEmitter, ElementRef } from '@angular/core';
import { config } from '../../../../config';
import { StoreService } from '../../../../plugins/datastore';
import { HelpService } from '../../../../plugins/help';
import { Router } from '@angular/router';
import { MTN } from '../../../../../app/config/config';
import { cObj } from '../../../../../app/config/config';
import { distanceMappedCenters } from '../../../../../app/config/config';
import { SuperAgent } from '../../../../../app/config/config';
import { IfStmt } from '@angular/compiler';


//import { AgmCoreModule } from '@agm/core';




declare var jQuery :any;

@Component({
  selector: 'app-mtnstoreloacter',
  templateUrl: './mtnstoreloacter.component.html',
  styleUrls: ['./mtnstoreloacter.component.scss']
})
export class MtnstoreloacterComponent implements OnInit {
  previous:any;
  selectedBundle: any = -1;
  viewaddress:string;
  showLoader: Boolean = false;
  public stateList = [];
  public lgaList = [];
  ASSETS: string = config.ASSETS;
  public selectedState;
  public selectedLga;
  public msisdnval:any;
  public isSelectedList = false;
  public isSelectedMap = true;
  public displayMapContent = true;
  public dataSource = [];
  public outputdata = [];
  public length;
  public pageSize = 10;
  plusminusVar=[];
  public active_nav: string;
  @Output() selected_tab = new EventEmitter<string>();
  //lgadata = "Select Your LGA";
  location: any;
  latitude:any;
  longitude:any;
  addressss:string;
  public MTNServiceCenters = [];
  public MTNcountry = [];
  public nearestlatlong = [];
  laticonfig:number;
  longconfig:number;
  public latitudeconfig:number;
  public longitdeconfig:number;
  public deg2rad:any;
  public distancemappedcenters = [];
  public superagent = [];
  public MTNmappedServiceCenters =[];
  public mtnstores = [];
  zoom:number=8;
  public selectedMarker:any;
map: any;
public mapCenter:any;
infoWindowOpened = null
previous_info_window = null
  viewlatitude: number;
  viewCenter: any;
  viewlongitude: number;
  infoWindowOpen = false;
prevSelectedStoreIndex: any;
currSelectedStoreIndex: any; 


  constructor(private _storeVal: StoreService,private _helpService: HelpService,private router: Router) { }
  ngOnInit() {
    this.msisdnval =  this._storeVal.getMSISDNData();
   this.getAllState();
    this.getAllLGAs(this.selectedState);
    this.getcurrent();
    //this.select_marker(infoWindow);
    //this.getrefreshlocation();
     }
  private setCurrentLocation() {
          if ('geolocation' in navigator) {
        navigator.geolocation.getCurrentPosition((position) => {
          this.latitude = position.coords.latitude;
          this.longitude = position.coords.longitude;
          
        });

      }
    }
    colapse: boolean[] = [false];
    
    close_window(){
      if (this.previous_info_window != null ) {
        this.previous_info_window.close()
        }    
      }
      select_marker(event){
        if (this.previous_info_window == null)
         this.previous_info_window = event;
        else{
         this.infoWindowOpened = event
         this.previous_info_window.close()
        }
        this.previous_info_window = event
       }
 
    
  getrefreshlocation(){
    this.showLoader = true;
    
    this._helpService.getUserLocationAddress(this.latitude,this.longitude)
    .then(resp => {
      this.showLoader = false;  
      this.addressss = resp.results[0].formatted_address;
    });


  }
  

  getcurrent(){
    
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition(position => {
        this.latitude= position.coords.latitude;
        this.viewlatitude = position.coords.latitude;
        this.viewlongitude = position.coords.longitude;
        this.viewCenter = this.viewlatitude +','+  this.viewlongitude;
        this.longitude= position.coords.longitude;
        this.MTNServiceCenters = MTN['service_center'];
        this.MTNcountry = cObj['country'];
        this.distancemappedcenters = distanceMappedCenters['service_center'];
        this.superagent = SuperAgent['Details'];
        let distanceJson = [];
        for (var i = 0; i < this.MTNServiceCenters.length; i++) {
          var latitudeconfig = this.MTNServiceCenters[i].LATITUDE;
        var longitdeconfig = this.MTNServiceCenters[i].LONGITUDE;

        let data = this.distanceBetweenCoordinates(this.latitude, this.longitude, latitudeconfig, longitdeconfig);
       
          let locations = {
            lati : this.MTNServiceCenters[i].LATITUDE,
            long : this.MTNServiceCenters[i].LONGITUDE,
            name : this.MTNServiceCenters[i].NAME,
            type : this.MTNServiceCenters[i].TYPE,
            state : this.MTNServiceCenters[i].STATE,
            lga : this.MTNServiceCenters[i].LGA,
            address : this.MTNServiceCenters[i].ADDRESS,
            cellphone : this.MTNServiceCenters[i].CELLPHONE,
            controler : this.MTNServiceCenters[i].CONTROLLER,
            distanceJson : data,
            active: false
          }
           distanceMappedCenters.service_center.push(locations);
         }
         distanceMappedCenters.service_center.sort(function(a, b) {
          return a.distanceJson - b.distanceJson;
      });

      
    this.MTNmappedServiceCenters = distanceMappedCenters.service_center;
     for (var j = 0; j < 5; j++) {
      this.mtnstores.push(this.MTNmappedServiceCenters[j]);
      
    }
        this._helpService.getUserLocationAddress(this.latitude,this.longitude)
        .then(resp => {
          this.addressss = resp.results[0].formatted_address;
   
        });

       });
   }
   
 }

 addMarker(lati: number, long: number) {
  this.mtnstores.push({ lati, long,  alpha: 0.4 });
}

latlongfun(event, marker){
  this.viewlatitude = Number(event.lati);
  this.viewlongitude = Number(event.long);
  
  this.infoWindowOpen = true;
  this.zoom = 15;
}
latlongfunothers(event){
  this.viewlatitude = Number(event.LATITUDE);
  this.viewlongitude = Number(event.LONGITUDE);
  this.zoom = 15;
  this.infoWindowOpen = true;
}

public getMapInstance(map) {
  this.map = map;
}

public updateMapCenter() {
  this.mapCenter = {
    latitude: this.map.center.lat(),
    longitude: this.map.center.lng()
  }
}
selectMarker(event) {
  this.selectedMarker = {
    lati: event.latitude,
    long: event.longitude,
  };
}


 distanceBetweenCoordinates(lat, long, latconfig, longconfig) {
  
var R = 5000; // Radius of the earth in km
var dLat = (latconfig - lat) * Math.PI / 180; // deg2rad below
var dLon = (longconfig - long)* Math.PI / 180;
var a = 
   0.5 - Math.cos(dLat)/2 + 
   Math.cos(lat * Math.PI / 180) * Math.cos(latconfig * Math.PI / 180) * 
   (1 - Math.cos(dLon))/2;
   return R * 2 * Math.asin(Math.sqrt(a));
  }
  getdirectionnearest(nearest){
    let address = '';
    let lati = '';
    let long = '';
    let name = '';

   name = nearest.name;
   address  = nearest.address;
   lati = nearest.lati;
   long = nearest.long;
 window.open('https://www.google.com/maps/search/'+name+','+address+'/@'+lati+','+long,'_blank');
  }

  getdirection(data){
    let address = '';
    let lati = '';
    let long = '';
    let name = '';

   name = data.NAME;
   address  = data.ADDRESS;
   lati = data.LATITUDE;
   long = data.LONGITUDE;
 window.open('https://www.google.com/maps/search/'+name+','+address+'/@'+lati+','+long,'_blank');
  }
  
  plusminus(data, index) {
    this.prevSelectedStoreIndex = this.currSelectedStoreIndex;
    if (this.prevSelectedStoreIndex != undefined) {
      if (this.prevSelectedStoreIndex == index) {
        this.mtnstores[index].active = !this.mtnstores[index].active;
      } else {
        this.mtnstores[index].active = true;
        this.mtnstores[this.prevSelectedStoreIndex].active = false;
      }
    } else {
      this.mtnstores[index].active = true;
    }
    this.currSelectedStoreIndex = index;
  }
  plusminusothers(data, index) {
    this.prevSelectedStoreIndex = this.currSelectedStoreIndex;
    if (this.prevSelectedStoreIndex != undefined) {
      if (this.prevSelectedStoreIndex == index) {
        this.dataSource[index].active = !this.dataSource[index].active;
      } else {
        this.dataSource[index].active = true;
        this.dataSource[this.prevSelectedStoreIndex].active = false;
      }
    } else {
      this.dataSource[index].active = true;
    }
    this.currSelectedStoreIndex = index;
  }



  // ngAfterViewInit(){
  //   jQuery(document).ready(function(){
   
  // jQuery('.tabs').click(function(event){
  //   jQuery('.containerp section > div ').toggleClass('active')
  //   jQuery('.containerp > div').toggleClass('activetab')
  // });
  //  })
  // }
  onLoadQA(_selectedObj){
    this.active_nav = _selectedObj;
    this.selected_tab.emit(this.active_nav);
  }



  getAllState() {
         this.showLoader = true;
       this._helpService.getAllState(this.msisdnval)
      .then(resp => {
        this.showLoader = false;
        if (resp.status === "0" && resp.prodInfo && resp.prodInfo.length > 0) {
           this.stateList = resp.prodInfo;
          this.selectedState = this.stateList[0].STATE;
        } else {
          this.stateList = [];
        }
      })
      .catch(err => {
        this.stateList = [];
      });
  }
  
  getAllLGAs(selectedState) {
    if (selectedState == undefined){
      selectedState ="Abia";
    }
    this._helpService.getAllLGAs(this.msisdnval, selectedState)
      .then(resp => {    
        if (resp.status === "0" && resp.prodInfo && resp.prodInfo.length > 0) {
          
          this.lgaList = resp.prodInfo;


          this.lgaList.sort(function(a, b) {
            return a.selectedLga - b.selectedLga;
        });
        
          this.selectedLga = this.lgaList[0].LGA;
          this.getServiceProvinceCenters(this.selectedState, this.selectedLga);
        } else {
         this.lgaList = [];
        }
      })
      .catch(err => {
        this.lgaList = [];
      });
  }
  

onChangeState(selectedState) {
  this.getAllLGAs(selectedState);
    this.selectedLga="";
    this.getServiceProvinceCenters(selectedState, this.selectedLga);
    this.selectedState = selectedState;
}

onChangeLga(selectedLga) {
  this.getServiceProvinceCenters(this.selectedState, selectedLga);
  this.selectedLga = selectedLga;
}

getServiceProvinceCenters(state, lga) {
  this.showLoader = true;
  this._helpService.getServiceProvinceCenters(this.msisdnval, state, lga)
    .then(resp => {
      this.showLoader = false;
      if (resp.status === "0" && resp.prodInfo && resp.prodInfo.length > 0) {
        this.length = resp.prodInfo.length;
        this.outputdata = resp.prodInfo;
        this.dataSource = this.outputdata.slice(0, this.pageSize);
      } else {
        this.dataSource = [];
       }
      resp.prodInfo.forEach((ele) => {    
  })

    })
    .catch(err => {
      this.dataSource = [];
    });
}

}
