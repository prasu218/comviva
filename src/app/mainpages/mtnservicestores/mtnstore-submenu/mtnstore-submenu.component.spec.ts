import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MtnstoreSubmenuComponent } from './mtnstore-submenu.component';

describe('MtnstoreSubmenuComponent', () => {
  let component: MtnstoreSubmenuComponent;
  let fixture: ComponentFixture<MtnstoreSubmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MtnstoreSubmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MtnstoreSubmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
