import { Routes } from '@angular/router';
import { MtnservicestoresComponent } from '../mtnservicestores.component'
import { MtnstoreloacterComponent } from '../mtnstore-submenu/mtnstoreloacter/mtnstoreloacter.component'
import { AuthGuard } from 'src/app/auth/auth.guard';

export const Storelocaterroutes: Routes = [{
  path: 'mtnservicestores', component: MtnservicestoresComponent,
  children: [
    {
      path: '',
      component: MtnstoreloacterComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'mtnstores',
      component: MtnstoreloacterComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    }

  ]
}];

