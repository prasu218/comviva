import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { config } from '../../config';
import { StoreService } from '../../plugins/datastore';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-mtnservicestores',
  templateUrl: './mtnservicestores.component.html',
  styleUrls: ['./mtnservicestores.component.scss']
})
export class MtnservicestoresComponent implements OnInit {
  header=true;
  bgColorpp = 'grey' ;
  constructor(private _storeVal: StoreService,private router: Router,private location: Location) { }

  ngOnInit() {
    }

   backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }
  
}
