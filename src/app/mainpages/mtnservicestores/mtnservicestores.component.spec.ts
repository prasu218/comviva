import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MtnservicestoresComponent } from './mtnservicestores.component';

describe('MtnservicestoresComponent', () => {
  let component: MtnservicestoresComponent;
  let fixture: ComponentFixture<MtnservicestoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MtnservicestoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MtnservicestoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
