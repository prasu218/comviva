import { Component, OnInit } from '@angular/core';
import { DbCallsService } from 'src/app/plugins/dbCalls';
import { StoreService } from 'src/app/plugins/datastore';
import { Router, ActivatedRoute } from '@angular/router';
import { ActivateBundleService } from '../../plugins/activateBundle';
import { EligibilityService } from 'src/app/plugins/eligibility';
import { Location } from '@angular/common';


@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {
date= new Date();
 dd = this.date.getDate();
  message = 'Successfully Recharged';
  header = true;
  orderObj: any;
  successful = false;
  activated = false;
  msisdn: any;
  // message = 'Successfully Recharged';
  constructor(private mydb: DbCallsService, private _storeData: StoreService, private router: Router, private route: ActivatedRoute, private activateBundleService: ActivateBundleService, private sanitize: EligibilityService,private location: Location) { }

  ngOnInit() {
  /*  //this.mydb.saveDebitcardDetails('Not generated', this._storeData.getMSISDNData(), this._storeData.getFirstNameData(),  'Recharged number',  this._storeData.getEmailData(),  'Recharged amount','Successful' );
 this.msisdn = this._storeData.getMSISDNData();
    this.route.queryParamMap.subscribe(params => {
      this.orderObj = { ...params };
    });
   // console.log('ref', this.orderObj.params.ref);
    // console.log('paymentDetails',paymentDetails);
    this.mydb.getDebitcardDetails(this.orderObj.params.ref, 'Successful',this.msisdn).then(paymentDetails => {
     // console.log('paymentDetails', paymentDetails);
      paymentDetails.payment.forEach(element => {
        if (element.transaction_status == 'Successful' && element.transaction_type == 'Bundle') {
          this.successful = true;
		 
        }
        if (element.transaction_status == 'Activated' && element.transaction_type == 'Bundle') {
          this.activated = true;
		   
        }
        if (element.transaction_status == 'Activation Failed' && element.transaction_type == 'Bundle') {
          this.activated = true;
          this.successful = false;
		  
        }
      });*/

      //console.log('this.successful', this.successful)
      //console.log('this.successful', this.activated)
     /* if (this.activated && this.successful) {
        this.message = 'Bundle is activation is already initiated';
      }
      if (this.activated && !this.successful) {
        this.message = "CIS is trying to process the request";
      }
      if (!this.activated && !this.successful) {
        this.message = "There was an issue whileprocessing your request";
      }

      if (!this.activated && this.successful) {
        this.activateBundle()
      }
    }).catch(err => {
      //console.log('paymentDetails err', err);
    });*/

  }
  
  backbuttons(){
      this.router.navigate(['recharge']);
   }

  navigate()
  {
    this.router.navigate(['feedback'])
  }
  checkdate()
  {
    if(this.dd==1){
      return true;
    }
    else{
      return false;
    }
  }

  /*activateBundle() {

    //console.log('activation bundle')
    const bundleData = this._storeData.getBuyBundlePlanDetails();
    const msisdn = bundleData.msisdn;
    const transactionId = this.orderObj.params.ref;
    const amount = bundleData.price;
    let eligibiltyId = '';
    let action = '';
    if (bundleData.eligibiltyId) {
      //console.log('this.proceedData.eligibiltyId', bundleData.eligibiltyId)
      eligibiltyId = bundleData.eligibiltyId.split(':')[1];
    }
    if (bundleData.action) {
     // console.log('this.proceedData.action', bundleData.action)
      action = bundleData.action.split(':')[1];
    }
    // let action = bundleData.action.split(':')[1];
    const { correctAction } = this.sanitize.dataSanitization(eligibiltyId, action);
    const autoRenew = bundleData.renewal;
    const beneficiaryMsisdn = (bundleData.beneficiaryMsisdn && bundleData.beneficiaryMsisdn != '') ? bundleData.beneficiaryMsisdn : bundleData.msisdn;
    this.activateBundleService.activateBundle(
      msisdn, transactionId, amount, correctAction, autoRenew, beneficiaryMsisdn).then((data) => {
        //console.log(data);
        if (data.ResponseCode == '0') {
          this.message = data.ResponseData.ProductBuy.Notification;
		 
          this.mydb.saveDebitcardDetails(transactionId, this._storeData.getMSISDNData(), this._storeData.getFirstNameData(), beneficiaryMsisdn, this._storeData.getEmailData(), amount, 'Activated', 'Bundle');
        }
        else{
          this.mydb.saveDebitcardDetails(transactionId, this._storeData.getMSISDNData(), this._storeData.getFirstNameData(), beneficiaryMsisdn, this._storeData.getEmailData(), amount, 'Activation Failed', 'Bundle');
          this.message = data.RespDescription;
		  
        }
      });
  }*/

}
