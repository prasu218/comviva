import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybundlessuccessComponent } from './buybundlessuccess.component';

describe('BuybundlessuccessComponent', () => {
  let component: BuybundlessuccessComponent;
  let fixture: ComponentFixture<BuybundlessuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybundlessuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybundlessuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
