import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
// import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { DbCallsService } from 'src/app/plugins/dbCalls';
import { StoreService } from 'src/app/plugins/datastore';
import { Router, ActivatedRoute } from '@angular/router';
import { ActivateBundleService } from '../../plugins/activateBundle';
import { EligibilityService } from 'src/app/plugins/eligibility';
// import { Location } from '@angular/common';

@Component({
  selector: 'app-buybundlessuccess',
  templateUrl: './buybundlessuccess.component.html',
  styleUrls: ['./buybundlessuccess.component.scss']
})
export class BuybundlessuccessComponent implements OnInit {
  message=""
  header = true;
  date= new Date();
 dd = this.date.getDate();
  // message = 'Successfully Recharged';
  // header = false;
  orderObj: any;
  successful = false;
  activated = false;
  msisdn: any;
  constructor(private mydb: DbCallsService, private _storeData: StoreService, 
    private router: Router, private route: ActivatedRoute, private activateBundleService: ActivateBundleService, 
    private sanitize: EligibilityService,private location: Location) { }

  ngOnInit() {
    //this.mydb.saveDebitcardDetails('Not generated', this._storeData.getMSISDNData(), this._storeData.getFirstNameData(),  'Recharged number',  this._storeData.getEmailData(),  'Recharged amount','Successful' );
    this.msisdn = this._storeData.getMSISDNData();
    //this.route.queryParams.subscribe(param => {
     // console.log("QUERY::::", param['ref']);
      //this.orderObj = param['ref'];
    //});
    //this.orderObj = this.route.snapshot.queryParamMap.get('ref');
    //this.orderObj = this.route.snapshot.paramMap.get("ref");
    // console.log("QUERY::::", this.route.snapshot.params['ref']);
     this.route.queryParamMap
      .subscribe(queryParam => {
        console.log("QUERY SUBS PARAM:::", queryParam);
        this.orderObj = queryParam.get('ref');
      });
     console.log('Snapshot ref', this.route.snapshot.queryParamMap.get('ref'));
    console.log('Subscribe ref', this.orderObj);
    // console.log('paymentDetails',paymentDetails);
    this.mydb.getDebitcardDetails(this.orderObj, 'Successful', this.msisdn).then(paymentDetails => {
      console.log('paymentDetails', paymentDetails);
        if (paymentDetails.transaction_status == 'Successful' && paymentDetails.transaction_type == 'Bundle') {
          this.successful = true;
        }
        if (paymentDetails.transaction_status == 'Activated' && paymentDetails.transaction_type == 'Bundle') {
          this.activated = true;
		   
        }
        if (paymentDetails.transaction_status == 'Initiated' && paymentDetails.transaction_type == 'Bundle') {
          this.activated = true;
		   
        }
        if (paymentDetails.transaction_status == 'Pending' && paymentDetails.transaction_type == 'Bundle') {
          this.activated = true;
		   
        }
        if (paymentDetails.transaction_status == 'Activation Failed' && paymentDetails.transaction_type == 'Bundle') {
          this.activated = true;
          this.successful = false;
		  
        }

      //console.log('this.successful', this.successful)
      //console.log('this.successful', this.activated)
      if (this.activated && this.successful) {
        this.message = 'Bundle is activation is already initiated';
      }
      if (this.activated && !this.successful) {
        this.message = "CIS is trying to process the request";
      }
      if (!this.activated && !this.successful) {
        this.message = "There was an issue while processing your request";
      }

      if (!this.activated && this.successful) {
        this.activateBundle(paymentDetails)
      }
    }).catch(err => {
      console.log('paymentDetails err', err);
    });

  }
  // backbuttons(){
  //   var urlPath = window.location.pathname.split("/");
  //   if (urlPath.length > 2){
  //    this.location.back();
  //   }else{
  //     this.router.navigate(['dashboard']);
  //   }
  //  }
  navigate()
  {
    this.router.navigate(['feedback'])
  }
  checkdate()
  {
    if(this.dd==1){
      return true;
    }
    else{
      return false;
    }
  }

activateBundle(paymentDetails) {
 
    console.log('activation bundle')
    const msisdn = paymentDetails.cust_id;
    const transactionId = this.orderObj;
    const amount = paymentDetails.transaction_amount;
    let action = paymentDetails.only_action;
    const autoRenew = false;
    const beneficiaryMsisdn = (paymentDetails.cust_mobile && paymentDetails.cust_mobile != '') ? paymentDetails.cust_mobile : paymentDetails.cust_id;
    this.activateBundleService.activateBundle(msisdn, transactionId, '0', action, autoRenew, beneficiaryMsisdn).then((data) => {
      if (data.ResponseCode == '0') {
        this.message = data.ResponseData.ProductBuy.Notification;
        this.mydb.getDebitCardTransStatus(transactionId, 'Activated', msisdn).then((resp) => {
          console.log("Inside Success activation code::", resp);
        });
      }
      else {
        this.mydb.getDebitCardTransStatus(transactionId, 'Activation Failed', msisdn).then((resp) => {
          console.log("Inside Failure activation code::", resp);
        });
        this.message = data.RespDescription;
 
      }
    });
  }


  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }

}
