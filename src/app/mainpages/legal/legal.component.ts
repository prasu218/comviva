import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.scss']
})
export class LegalComponent implements OnInit {
  header = true;
selectedData;
  @Input() active_nav: string;
  

  faqsNavElements = [
     {id:'privacypolicy', value:'Privacy',value1:'Policy', source :'assets/images/privacy_policy.svg',},
     {id:'content',  value:'Content',  source :'assets/images/content.svg',},
     {id:'complainhandling', value:'Complaints',value1:'Handling',  source :'assets/images/complaints.svg',},
  ];

  constructor(private _router: Router,private location: Location) { }
  
  ngOnInit() {
    
    this.active_nav  = "privacypolicy";
       // mobile carousel
       $( document ).ready(function() {
        $('.carousel-crds').flickity({
          groupCells: true,
          freeScroll: true,
          cellAlign: 'left', 
          contain: true, 
          prevNextButtons: false,
          pageDots: true, 
          draggable: true,
          watchCSS: true,
          setGallerySize: false,
          resize: true,
          reposition: true
        });
      });
  }

  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this._router.navigate(['dashboard']);
    }
   }

  ngAfterContentChecked(){
    //this.active_nav = this._router.url.split('/')[2];
    // if(this._router.url === "/faqs"){
    //  // this.active_nav  = "smartWebNav";
    // }
  }

  onLoadMdNav(selectedData){
    this.active_nav = "privacypolicy";
    // console.log(selectedData)
    // this._router.navigate(['faqs',selectedData.id]);
    this.active_nav = selectedData.id;
    //this.selectedData = selectedData.id;
   // console.log("this.active_nav",this.active_nav)
    //console.log("selectedData",this.active_nav === selectedData.id)
  }

}
