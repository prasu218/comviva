import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-detailsofprofile',
  templateUrl: './detailsofprofile.component.html',
  styleUrls: ['./detailsofprofile.component.scss']
})
export class DetailsofprofileComponent implements OnInit {
  header = true;

  responseOTP: any;

  constructor(private router: Router,private location: Location) {
    let params = this.router.getCurrentNavigation().extras.state;
    if (params != null && params.validated == 'validated') {
     // console.log(params);
      this.responseOTP = params.validated;
    } else {
      this.router.navigate(['otpprofile'])
    }
  }

  ngOnInit() {
   
  }

  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }
   
}
