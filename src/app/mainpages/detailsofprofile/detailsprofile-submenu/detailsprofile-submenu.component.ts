import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detailsprofile-submenu',
  templateUrl: './detailsprofile-submenu.component.html',
  styleUrls: ['./detailsprofile-submenu.component.scss']
})
export class DetailsprofileSubmenuComponent implements OnInit {

  @Input() active_nav: string;

  helpNavElements = [
    //{id:'bankaccount',value:'Bank acount'},
    {id:'detailssim',value:'SIM Details', source :'assets/images/sim.svg',},
    {id:'otherdetails',value:'Other Details',source :'assets/images/baseline-more-24px.svg',},
    // {id:'sendairtime',value:'Send Airtime', source :'assets/logo/rechargel.svg',},
  ];

  constructor(private _router: Router) { 
    
  }

  ngOnInit() {
    //this.scrollSubMenu();
    }

  ngAfterContentChecked(){
    this.active_nav = this._router.url.split('/')[2];
    if(this._router.url === "/detailsofprofile"){
      this.active_nav  = "detailssim";
    }
  }
  
 onLoadMdNav(selectedData){
    this._router.navigate(['detailsofprofile',selectedData.id]);
     this.active_nav = selectedData.value;
  }

}
