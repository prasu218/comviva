import { Routes } from '@angular/router';
import { DetailsofprofileComponent } from '../detailsofprofile.component';
import { OtherdetailsComponent } from './otherdetails/otherdetails.component';
import { DetailssimComponent } from './detailssim/detailssim.component';
import { OtherproProccedComponent } from './otherdetails/otherpro-procced/otherpro-procced.component';
import { AuthGuard } from 'src/app/auth/auth.guard';

export const DetailsProfilemenuroutes: Routes = [{
  path: 'detailsofprofile', component: DetailsofprofileComponent,
  children: [
    {
      path: '',
      component: DetailssimComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'detailssim',
      component: DetailssimComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'otherdetails',
      component: OtherdetailsComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
  ]
},
{

  path: 'detailsofprofile/otherdetails/procced',
  component: OtherproProccedComponent,
  pathMatch: 'full'
},


];
