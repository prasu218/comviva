import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsprofileSubmenuComponent } from './detailsprofile-submenu.component';

describe('DetailsprofileSubmenuComponent', () => {
  let component: DetailsprofileSubmenuComponent;
  let fixture: ComponentFixture<DetailsprofileSubmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsprofileSubmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsprofileSubmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
