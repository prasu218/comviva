import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailssimComponent } from './detailssim.component';

describe('DetailssimComponent', () => {
  let component: DetailssimComponent;
  let fixture: ComponentFixture<DetailssimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailssimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailssimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
