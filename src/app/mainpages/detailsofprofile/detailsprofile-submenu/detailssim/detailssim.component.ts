import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../../plugins/profile';
import { StoreService } from '../../../../plugins/datastore';
import { Router } from '@angular/router';
import { config } from '../../../../config';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { BnNgIdleService } from 'bn-ng-idle';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-detailssim',
  templateUrl: './detailssim.component.html',
  styleUrls: ['./detailssim.component.scss']
})
export class DetailssimComponent implements OnInit {
  True : boolean = true;
  selectedFile = null;
  url:any='';
  inputValue = '';
  public msisdn:string;
  public msisdnval: any;
  email: string;
  alternateNumber: number;
  primaryNumber: number;
 activationdate:number;
  currentDate: any = new Date();

  result:string
  firstName:string;
  familyName: string;
  Fullname:string;
  showLoader : boolean = false;
  pukval:any;
  date: any;
  // profilechange: HTMLElement;

  public selfform: FormGroup;
  public PUKform:FormGroup;
  public firstNameform: FormGroup;
  public alternateNumberform: FormGroup;
  public emailform: FormGroup;
   public activationdateform: FormGroup;

   editevents:any;
   fileextn: any;
   files:any;
   selecteFile: any;

  base64img: any;
  uploadImgErrMsg: string = '';
  existingProfilePic: any;

  constructor(private http: HttpClient, private _sanitizer: DomSanitizer, private _https: HttpClientModule,
    private router: Router, private fb: FormBuilder, private _storeData: StoreService,
    private _storeVal: StoreService, private _profile: ProfileService, private _router: Router) { }
  ngOnInit() {

    this.msisdnval = this._storeVal.getMSISDNData();
    
   this.email = this._storeVal.getEmailData();
   this.alternateNumber = this._storeVal.getAlternateNumberData(); 
   this.firstName = this._storeVal.getFirstNameData();
    this.familyName = this._storeVal.getFamilyNameData();
  this.activationdate = this._storeVal.getActivationDate();
  this.getmypuknum(this.msisdnval);

    this._profile.getProfileDetails(this.msisdn)
    this.fetchingprofile(this.msisdnval);
this.Fullname = this.firstName +""+ this.familyName;
   
    if (this._storeVal.getMSISDNData() && this._storeVal.getMSISDNData()!= "") {  
    }
    else{
      this._router.navigate(['login'])
    }
    this.selfform = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(config.MIN_MSISDN_LENGTH), Validators.maxLength(config.MAX_MSISDN_LENGTH)]],
   });

   this.firstNameform = this.fb.group({
    nameuser: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(13)]],
 });

 this.alternateNumberform = this.fb.group({
  othernumber: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(13)]],
});

this.emailform = this.fb.group({
  email: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(13)]],
});

this.activationdateform = this.fb.group({
      date: ['',],
});

this.PUKform = this.fb.group({
  puknumber: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]],
});



  
}


fetchingprofile(msisdnval){
   this.showLoader = true;
    this._profile.getProfilePic(this.msisdnval).then((res) => {

      var base64image = atob(res.encodedprofilepic);
      // var base64 = res.match(/("base64image": [\W\w]*$)/ig);
      // const base64image = window.atob(base64.toString().replace('"base64image":','').replace('NI"}','').replace('"','').replace(/\s/g, ''));
      // this.base64img = base64image;
      //console.log(this.base64img);
      if (base64image) {
        // this.url = 'data:image/png;base64,'+(base64image);
        this.url = 'data:image/png;base64,' + base64image;
        this.existingProfilePic = this.url;
        this.showLoader = false;
    } 
    }).catch((err) => {
      //console.log("Inide err:::", err);
      this.url = ''
      this.showLoader = false;
    
    });
  
  }

  getmypuknum(msisdn) {
    this._profile.getPUKDetails(msisdn)
      .then((resp:any) => {
        //console.log("pukresp" + JSON.stringify(resp));
         if (resp.status_code === 0){
          this.pukval = resp.puk_result[0].PUK;
        }else{
          
          this.pukval = "Not available";
        }
        
      })
      .catch((err) => {
        this.pukval = "Not available";
      });

  }

  _handleReaderLoaded(e) {
    this.uploadImgErrMsg = "";
    // let datePipe = new DatePipe('en-US');
    // let formattedDate = datePipe.transform(this.currentDate, 'short');
    let reader = e.target;
    let base64Code = reader.result;
    this.url = base64Code;
    let base64Str = base64Code.replace(/^data:image\/\w+;base64,/, '');
    console.log("URL:::::::::::", this.url);
    this.showLoader = true;
    this._profile.updatepicprofile(this.msisdnval, base64Str).then(
      resp => {
       

        if (resp.status == 0) {
           //this.fetchingprofile(this.msisdnval);
           this._profile.changeProfilePic(this.url)
          this.showLoader = false;
        }

      }).catch((err) => {
        let error = err.json();
        console.log("Sorry Not able to upload image", err);
        this.showLoader = false;
        if (error.status == 401) {
          this.uploadImgErrMsg = "Sorry unable to upload image"
          this.url = this.existingProfilePic;
          this.showLoader = false;
        }
      });
  }

  changeListener($event): void {
    console.log("Inside the change event listener::::", $event.target);
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.url = myReader.result;
      this._handleReaderLoaded(e)
    }
    myReader.readAsDataURL(file);
  }
}



