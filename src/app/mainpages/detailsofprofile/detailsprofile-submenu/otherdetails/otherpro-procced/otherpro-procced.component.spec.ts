import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherproProccedComponent } from './otherpro-procced.component';

describe('OtherproProccedComponent', () => {
  let component: OtherproProccedComponent;
  let fixture: ComponentFixture<OtherproProccedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherproProccedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherproProccedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
