
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { bundleMasterConfig } from '../../../../../app/config/config';
import { StoreService } from '../../../../plugins/datastore';
import { MatDialog } from '@angular/material';
import { BuyBundlesService } from '../../../../plugins/buybundles';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { config } from '../../../../config';
import { getRoamingRatesCountries_Response } from '../../../../../app/config/config';
import { ProfileService } from '../../../../plugins/profile';

@Component({
  selector: 'app-otherdetails',
  templateUrl: './otherdetails.component.html',
  styleUrls: ['./otherdetails.component.scss']
})
export class OtherdetailsComponent implements OnInit {
  public roamingcountry = [];
  public roamingcountrydata: any = [];
  public elementVal: any = [];
  public selectedroamingcountry;
  public othernumber :any;
 
  public _selectedObj;
  message :string;
  showLoader: boolean = false;
  public arrow: boolean = false;
  public phonfarconfirm: Boolean = false;
 public suceesDiv:Boolean=false;
 public failurDiv:Boolean=false;
  public voicemailconfirm: Boolean = false;
  public deactivconfirm: Boolean = false;
  public resetcallconfirm: Boolean = false;
  public barrconfirm: Boolean = false;
  public unbarrconfirm: Boolean = false;
  public phonconf: Boolean = false;
  
  public arrow1: boolean = false;
   
  @Input() paymentuipagemigerate: boolean = true;

 
  @Output() grandchildprofile: EventEmitter<any> = new EventEmitter<any>();

  

  internalCardDetails = {};
  buybuttonHidden: boolean = true;
  public roamingPeriodicity = [];
  public displayValidity;

  public profileproc ;
  public active_nav_selected: string;
  public isDataLoaded: boolean = false;
  public confirm: boolean;
  public msisdnval: any;
  public proprofile:any;
public msisdn :any;
public voicemail :any;


  public buybundalval: any;
  public buybundalvalprice: any;
  public bundle_Category: any;


  public bundle_SubCategory: any;
  public bundle_SubCategorydata: any;
  public bundle_SubCategoryvoice: any;
  public bundle_SubCategorydataandvoice: any;
  selectedBundle: any = -1;
  public existing_cust: FormGroup;

  public othersform: FormGroup;
  public errorMsg = '';
  public selctradio: boolean = false;
  public bottoncolr: boolean = false;

  plusminusVar = [];
  plusminusfarvoiceVar = [];
  plusminusdeactforVar = [];
  plusminusresetbarVar = [];
  plusminucallbarVar = [];
  plusminucallunbarVar = [];



  constructor(private _buyBundleService: BuyBundlesService, private _router: Router, private _profile: ProfileService, private _storeVal: StoreService, public dialog: MatDialog, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
   
    this.msisdnval = this._storeVal.getMSISDNData();
    
    this.existing_cust = this.fb.group({
      msisdnother: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(13)]],
    
    });

    
  }


  phoneproceed() {

    this._storeVal.setproceprofileData({
      'msisdnother' : this.existing_cust.value.msisdnother,
      'proceedData' : 'Phone'
    })
    this.router.navigate(['detailsofprofile/otherdetails/procced']);
  }

  Voiceproceed() {
    this._storeVal.setproceprofileData({
      'proceedData' : 'Voice'
    })
    this.router.navigate(['detailsofprofile/otherdetails/procced']);
  }

  deactiveproceed() {
    this._storeVal.setproceprofileData({
      'proceedData' : 'deactivecall'
    })
    this.router.navigate(['detailsofprofile/otherdetails/procced']);

  }

   resebartproceed() {
    this._storeVal.setproceprofileData({
      'proceedData' : 'Resetcall'
    })
    this.router.navigate(['detailsofprofile/otherdetails/procced']);

  }
  
  barringproceed() {
    this._storeVal.setproceprofileData({
      'proceedData' : 'barring'
    })
    this.router.navigate(['detailsofprofile/otherdetails/procced']);

  }
  
  unbarringproceed() {
    this._storeVal.setproceprofileData({
      'proceedData' : 'unbarring'
    })
    this.router.navigate(['detailsofprofile/otherdetails/procced']);

  }


  xtradata() {
    if (this.arrow == false) {
      this.arrow = true;
    }
    else {
      this.arrow = false;
    }

    this.arrow1 = false;
  }

  xtratalk() {
    if (this.arrow1 == false) {
      this.arrow1 = true;
    }
    else {
      this.arrow1 = false;
    }
    this.arrow = false;
  }

  numberOnly(event): boolean {
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length == 11 && (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length == 17) {
      return false;;
    } else {
      return true;
    }
  }


  onslectradio(data) {
    this._selectedObj = data;
  }


  otherproceed() {
    alert("procc");
  }

  plusminus(data, index) {
    if (this.plusminusVar[index])
      this.plusminusVar[index] = false;
    else
      this.plusminusVar[index] = true;

  }

  plusminusfarvoice(data, index) {
    if (this.plusminusfarvoiceVar[index])
      this.plusminusfarvoiceVar[index] = false;
    else
      this.plusminusfarvoiceVar[index] = true;

  }

  plusminusdeactfor(data, index) {
    if (this.plusminusdeactforVar[index])
      this.plusminusdeactforVar[index] = false;
    else
      this.plusminusdeactforVar[index] = true;
  }

  plusminusresetbar(data, index) {
    if (this.plusminusresetbarVar[index])
      this.plusminusresetbarVar[index] = false;
    else
      this.plusminusresetbarVar[index] = true;
  }

  plusminucallbar(data, index) {
    if (this.plusminucallbarVar[index])
      this.plusminucallbarVar[index] = false;
    else
      this.plusminucallbarVar[index] = true;
  }

  plusminucallunbar(data, index) {
    if (this.plusminucallunbarVar[index])
      this.plusminucallunbarVar[index] = false;
    else
      this.plusminucallunbarVar[index] = true;
  }

}

