import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsofprofileComponent } from './detailsofprofile.component';

describe('DetailsofprofileComponent', () => {
  let component: DetailsofprofileComponent;
  let fixture: ComponentFixture<DetailsofprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsofprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsofprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
