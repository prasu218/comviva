import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnlinkConfirmationComponent } from './unlink-confirmation.component';

describe('UnlinkConfirmationComponent', () => {
  let component: UnlinkConfirmationComponent;
  let fixture: ComponentFixture<UnlinkConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnlinkConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnlinkConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
