import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/plugins/datastore';
import { NotificationdbService } from 'src/app/plugins/notificationdb';
import { Router } from '@angular/router';

@Component({
  selector: 'app-unlink-confirmation',
  templateUrl: './unlink-confirmation.component.html',
  styleUrls: ['./unlink-confirmation.component.scss']
})
export class UnlinkConfirmationComponent implements OnInit {

  header=true;
  unlinksuccess :boolean;
  unlinkfailure: boolean;
  message = '';
  unlinkNumber = "";

  constructor(  private _storeVal: StoreService,private notifiservice:NotificationdbService,
    private _storeData: StoreService, private router:Router ) {
      let params = this.router.getCurrentNavigation().extras.state
      this.unlinkNumber = params.unlinknumber;
      console.log("This the unlink params ", params);
      if(params.unlinksuccess){
        this.unlinksuccess = true;
      }else{
        this.unlinkfailure = true;
      }
     }

  ngOnInit() {
   this.message = "You have succesfully unlinked your account"
   
     //if(this.msisdnval.length==9||this.msisdnval.length==10)
   // {
   //   this.msisdnval=this.msisdnval
   // }else{
   //   this.msisdnval=this.msisdnval.slice(3);
   // }

  }

 

  Dashboard(){
    this.router.navigate(['dashboard']);
  }


}
