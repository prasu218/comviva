import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Router } from '@angular/router';
import { StoreService } from '../../plugins/datastore';
import { Location } from '@angular/common';

@Component({
  selector: 'app-socialmedia',
  templateUrl: './socialmedia.component.html',
  styleUrls: ['./socialmedia.component.scss']
})
export class SocialmediaComponent implements OnInit {

  //msisdnval = this._storeData.getMSISDNData();

  constructor(private route: ActivatedRoute,private router: Router,private _storeData: StoreService,private location: Location) { }
  header;
  bgColorpp = 'grey' ;
  public msisdn:any;
 
  backbuttons() {
    this.location.back();
  }



    public products: Product[] = [
    new Product(1, "Facebook","https://www.facebook.com/MTNLoaded"),
    new Product(2, "Instagram","https://www.instagram.com/mtnng/"),
    new Product(3, "Twitter","https://twitter.com/MTNNG"),
    new Product(4, "Twitter180","https://twitter.com/MTN180"),
    new Product(5, "YouTube","https://www.youtube.com/user/MTNNG"),
    new Product(6, "LinkedIn","https://www.linkedin.com/company/mtn-nigeria/"),
  ];
  
  product: Product = this.products[0];

  ngOnInit() {
    if (this._storeData.getMSISDNData() && this._storeData.getMSISDNData()!= "") {  
      this.msisdn = this._storeData.getMSISDNData();
      if(this.msisdn!="undefined" || this.msisdn!="null" || this.msisdn!=""){
        this.header = true;
      }
    }
    else{
      this.header = false;
    }

    this.route.params.subscribe(params => {

      this.products.forEach((p: Product) => {
        if (p.id == params.id) {
          this.product = p;
        }
      });
    });
  }
}

export class Product {
  id: number;
  name: string;
  url: string;
  constructor(id: number, name: string,url: string) {
    this.id = id;
    this.name = name;
    this.url = url;
  }
}
