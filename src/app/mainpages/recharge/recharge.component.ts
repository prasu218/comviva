import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../plugins/datastore'
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { Location } from '@angular/common';
declare var $: any;
@Component({
  selector: 'app-recharge',
  templateUrl: './recharge.component.html',
  styleUrls: ['./recharge.component.scss']
})
export class RechargeComponent implements OnInit {
bgColorpp = 'grey' ;
  header=true;
  constructor(private fb: FormBuilder,private _storeVal: StoreService,private _router: Router,private location: Location) { }

  ngOnInit() {

    $('#wht-crv').css("background-color","#f2f2f2");
    if (this._storeVal.getMSISDNData() && this._storeVal.getMSISDNData()!= "") {  
    }
    else{
      this._router.navigate(['login'])
    }
    
  }
  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this._router.navigate(['dashboard']);
    }
   }

}