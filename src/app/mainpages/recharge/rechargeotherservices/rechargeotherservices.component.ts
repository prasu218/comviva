import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-rechargeotherservices',
  templateUrl: './rechargeotherservices.component.html',
  styleUrls: ['./rechargeotherservices.component.scss']
})
export class RechargeotherservicesComponent implements OnInit {
  status:boolean = false;
  history:boolean = true;
  @Input() active_nav: string = 'rechargehistory';
  rechargeOther = [{ id: 'rechargehistory', value: 'Recharge History', value_1: "history", source: 'assets/logo/rechargel.svg' },
  { id: 'rechargestatus', value: 'Recharge Status', value_2: "status", source: 'assets/images/status.svg' }]
  constructor(private _router: Router) { }

  ngOnInit() {
  }
  // ngAfterContentChecked() {
  //   this.active_nav = this._router.url.split('/')[2];
  //   if (this._router.url === "/recharge") {
  //     this.active_nav = "rechargestatus";

  //   }
  // }

  onLoadMdNav(selectedData){
   if(selectedData.id == "rechargehistory"){
   // console.log("Inside the recharge history");
    this.status = false;
    // this.history = true;
    this.active_nav = selectedData.id;
  // console.log("this.active:::", this.active_nav)
   }
    //console.log("Selected::::", selectedData);
  else if(selectedData.id == "rechargestatus"){
    // console.log("Inside the recharge status");
     this.status = true;
    //  this.history = false;
    this.active_nav = selectedData.id;
   // console.log("this.active:::", this.active_nav)
   }
   
    //this._router.navigate(['recharge',selectedData.id]);
    // this.active_nav = selectedData.value;
  }
}
