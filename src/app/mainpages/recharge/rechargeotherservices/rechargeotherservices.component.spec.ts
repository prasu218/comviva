import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RechargeotherservicesComponent } from './rechargeotherservices.component';

describe('RechargeotherservicesComponent', () => {
  let component: RechargeotherservicesComponent;
  let fixture: ComponentFixture<RechargeotherservicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RechargeotherservicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RechargeotherservicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
