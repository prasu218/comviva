import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RechargestatusComponent } from './rechargestatus.component';

describe('RechargestatusComponent', () => {
  let component: RechargestatusComponent;
  let fixture: ComponentFixture<RechargestatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RechargestatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RechargestatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
