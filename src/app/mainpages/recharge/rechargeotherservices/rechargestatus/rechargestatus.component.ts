import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { format } from '../../../../utils/formatter';
import { config } from '../../../../config';
import { StoreService } from '../../../../plugins/datastore'
import { RechargestatusService } from '../../../../plugins/rechargestatus/rechargestatus.service';
import { AngularCDRService } from '../../../../plugins/angularCDR';
@Component({
  selector: 'app-rechargestatus',
  templateUrl: './rechargestatus.component.html',
  styleUrls: ['./rechargestatus.component.scss']
})
export class RechargestatusComponent implements OnInit {
  recharge_status: FormGroup;
  public msisdn:string;
  public firstName:any;
  public errorMsg = '';
  existing_cust: FormGroup;
  public message :string;
  public formhide: boolean=true;
  public hideee: boolean=false;
  public failure:boolean=false;
    //public msisdn:string;;
  showLoader : boolean = false;
timeoutErr = '';
startTime: any;
category: any = 'Recharge';
value="";
  constructor(private fb: FormBuilder,private _storeData: StoreService,private _rechargestatus:RechargestatusService,private _cdr: AngularCDRService) { }

  ngOnInit() {
    // this.existing_cust = this.fb.group({
    //   msisdn: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(config.MAX_MSISDN_LENGTH)]],
    // });
    this.recharge_status = this.fb.group({
      
      voucher: ['', [Validators.required, Validators.minLength(12),Validators.maxLength(17)]],
   });
  
   if (this._storeData.getMSISDNData() && this._storeData.getMSISDNData()!= "") {  
    this.msisdn = this._storeData.getMSISDNData();
    this.firstName =  this._storeData.getFirstNameData();
	
  }
  else{
    //this._router.navigate(['login'])
	
  }
  }
 numberOnly(event): boolean {
    if(this.timeoutErr){
      this.timeoutErr = '';
      }
      let test = event.target.value;
      let test_length = test.length;
      const charCode = (event.which) ? event.which : event.keyCode;
  
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      if (test_length >= 13&& (charCode == 8 || charCode == 13)) {
        return true;
      } else if (test_length >= 13) {
        return false;;
      } else {
        return true;
      }
    }
 loadtopup(){
        this.formhide=true;
        this.failure=false; 
        this.hideee=false;
        this.recharge_status.reset();    
       
      }
  
  getRechargeDetails(value){
	this.showLoader = true;
   const msisdn = this._storeData.getMSISDNData();
    let recharge_status = (this.recharge_status.get('voucher').value);
   // console.log("msisdn recharge",msisdn);
    this._rechargestatus.getRechargeDetails(msisdn,recharge_status).then((resp) => {
      if (resp.status_code == 0) {
            this.msisdn=this._storeData.getMSISDNData();
           // console.log("respppppp11 " +resp.status_message);
			this._cdr.writeCDR(this.startTime, this.category,this.value, 'Recharge Status', 'Success');
			//this._cdr.writeCDR(this.startTime, this.category, 'Recharge Status', 'Success', resp.status_message);
	    if(resp.card_status){
		//console.log("status details " +resp.card_status);
		//console.log("status details " +resp.card_status.serialDetails);
		if(resp.card_status.serialDetails){
		this.message =  resp.card_status.serialDetails.replace(",",'');
         	this.message = this.message.replace(/\n/g, " ");
		}else{
		this.message =  resp.add_status_message;
		}
	    }else{
	   	 if(resp.add_status_message){
                	this.message =  resp.add_status_message;
            	}else{
                	this.message =  resp.status_message;
            	}
	    }
            this.formhide=false;
            this.hideee=true; 
	    this.showLoader = false;      
          }else{
	    if(resp.add_status_message){
		this.message =  resp.add_status_message;	
	    }else{
	    	this.message =  resp.status_message;
			this._cdr.writeCDR(this.startTime, this.category,this.value, 'Recharge Status', 'Failure');
	    } 
            this.msisdn=this._storeData.getMSISDNData();            
            //console.log("respppppp12 " +resp.status_message); 
            this._cdr.writeCDR(this.startTime, this.category,this.value, 'Recharge Status', 'Failure');			
            this.failure=true;
            this.formhide=false;    
	    this.showLoader = false;   
          } 
      }) .catch((err) => {
          this.msisdn=this._storeData.getMSISDNData();
          this.failure=true;
          this.formhide=false;
          this.message = 'Please enter valid recharge card code';
          this.showLoader = false;
        });
  }
}
