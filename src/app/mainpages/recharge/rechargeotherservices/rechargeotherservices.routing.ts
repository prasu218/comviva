import { Routes } from '@angular/router';
import { RechargeComponent } from '../recharge.component';
import { RechargestatusComponent } from './rechargestatus/rechargestatus.component';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { RechargeHistoryComponent } from './rechargehistory/rechargehistory.component';

export const rechargeOtherServices: Routes = [{
  path: 'recharge', component: RechargeComponent,
  children: [
    {
      path: 'rechargehistory',
      component: RechargeHistoryComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'rechargestatus',
      component: RechargestatusComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    
  ]
}];
