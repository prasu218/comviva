import { Component, OnInit,Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-recharge-submenu',
  templateUrl: './recharge-submenu.component.html',
  styleUrls: ['./recharge-submenu.component.scss']
})
export class RechargeSubmenuComponent implements OnInit {
  @Input() active_nav: string;

  rechargeNavElements = [
    //{id:'bankaccount',value:'Bank acount'},
    {id:'debitcard',value:'Debit card', source :'assets/images/card.svg',},
    {id:'topupcard',value:'Topup card',source :'assets/images/topup.svg',},
    // {id:'scratchedpad',value:'Scratched card', source :'assets/logo/rechargel.svg',},
    {id:'sendairtime',value:'Send Airtime', source :'assets/images/airtime.svg',},
    {id:'borrowAirtime',value:'Borrow', source :'assets/images/borrow.svg',},
  ];

  constructor(private _router: Router) { 
    
  }

  ngOnInit() {
    //this.scrollSubMenu();

      // mobile carousel
    $( document ).ready(function() {
      $('.carousel-crds').flickity({
        groupCells: true,
        freeScroll: true,
        cellAlign: 'left', 
        contain: true, 
        prevNextButtons: false,
        pageDots: true, 
        draggable: true,
        watchCSS: true,
        setGallerySize: false,
        resize: true,
        reposition: true
      });
    });
    }

  ngAfterContentChecked(){
    this.active_nav = this._router.url.split('/')[2];
    if(this._router.url === "/recharge"){
      this.active_nav  = "debitcard";
    }
  }
  
 onLoadMdNav(selectedData){
    this._router.navigate(['recharge',selectedData.id]);
     this.active_nav = selectedData.value;
  }
}