import { Routes } from '@angular/router';
import { RechargeComponent } from '../recharge.component';
import { DebitCardComponent } from './debit-card/debit-card.component'
import { TopupCardComponent } from './topup-card/topup-card.component';
import { SendAirtimeComponent } from './send-airtime/send-airtime.component';
import { DebitCardProceedComponent } from './debit-card/debit-card-proceed/debit-card-proceed.component'
import { SendAirtimeProceedComponent } from './send-airtime/send-airtime-proceed/send-airtime-proceed.component';
import { BorrowAirtimeComponent } from './borrow-airtime/borrow-airtime.component';
import { BorrowAirtimeProceedComponent } from './borrow-airtime/borrow-airtime-proceed/borrow-airtime-proceed.component';
import { TopupProceedComponent } from './topup-card/topup-proceed/topup-proceed.component';
import { AuthGuard } from 'src/app/auth/auth.guard';

export const Rechargemenuroutes: Routes = [{
  path: 'recharge', component: RechargeComponent,
  children: [
    {
      path: '',
      component: DebitCardComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'debitcard',
      component: DebitCardComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'topupcard',
      component: TopupCardComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'borrowAirtime',
      component: BorrowAirtimeComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },

    {
      path: 'sendairtime',
      component: SendAirtimeComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },

  ],
}, {
  path: 'recharge/procced',
  component: DebitCardProceedComponent,
  pathMatch: 'full',
  canActivate: [AuthGuard]
},
{
  path: 'sendairtime/proceed',
  component: SendAirtimeProceedComponent,
  pathMatch: 'full',
  canActivate: [AuthGuard]
},{
  path: 'borrowAirtime/proceed',
  component: BorrowAirtimeProceedComponent,
  pathMatch: 'full',
},
{
  path: 'topupcard/proceed',
  component: TopupProceedComponent,
  pathMatch: 'full',
}];
