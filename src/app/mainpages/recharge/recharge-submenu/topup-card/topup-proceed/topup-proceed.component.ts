import { Component, OnInit } from '@angular/core';
import { format } from '../../../../../utils/formatter';
import { config } from '../../../../../config';
import { rcgtopupService } from '../../../../../plugins/rcgtopup';
import { StoreService } from '../../../../../plugins/datastore';
import { AngularCDRService } from '../../../../../plugins/angularCDR';
import { ProfileService } from '../../../../../plugins/profile';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-topup-proceed',
  templateUrl: './topup-proceed.component.html',
  styleUrls: ['./topup-proceed.component.scss']
})
export class TopupProceedComponent implements OnInit {
public showLoader:boolean=false;
public confirmdiv:boolean=true;
public suceesDiv:boolean=false;
public failurDiv:boolean=false;
public voucher_code:any="";
public msisdn:any="";
public message:any="";
public startTime:any="";
public radioValu:any="";
public topupdetails:any={};
public message1:any="";
public header=true;

  constructor(private location: Location,private  router: Router,private _profile: ProfileService,private _RcgTopupService: rcgtopupService,private _storeData: StoreService,private _cdr: AngularCDRService) { }
   

  ngOnInit() {
    this.startTime = this._cdr.getTransactionTime(0);
    this.topupdetails=this._storeData.gettopupDetails()
    this.msisdn=this.topupdetails.msisdn.split("234")[1];
    this.radioValu=this.topupdetails.radio;
    this.voucher_code=this.topupdetails.vouchercode;

  }
  // loadtopup(){
  //   this.router.navigate(['/topupcard']);
  // }
  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }
  onSubmitTopup() {  
    this.showLoader = true;
          
         
          if(this.radioValu=="false"||this.radioValu==false){
            
            this._RcgTopupService.getTopupDetails(this.msisdn,this.voucher_code).then((resp) => {
            if (resp.status_code == 0) {
              this.msisdn=this._storeData.getMSISDNData();
              console.log("respppppp " +resp.status_message);
              this.message =  "Recharge done successfull"; 
              this.message1="Success"   
              this._cdr.writeCDR(this.startTime, 'Recharge TopUp',this.voucher_code, 'Recharge TopUp', 'Success');      
              this.confirmdiv=false;
              this.suceesDiv=true;
        this.showLoader = false;
                  
            }else{ 
              this.msisdn=this._storeData.getMSISDNData();            
              this.message =  "Please enter valid voucher number";
              this.message1="Failure"   
              this._cdr.writeCDR(this.startTime, 'Recharge TopUp' ,this.voucher_code, 'Recharge TopUp', 'Failure');
              console.log("respppppp " +resp.status_message);         
              this.failurDiv=true;
              this.confirmdiv=false; 
        this.showLoader = false;
                  
            }   
          }).catch((err) => {
     
            this.msisdn=this._storeData.getMSISDNData();
            this.failurDiv=true;
            this.confirmdiv=false;
            this.message1="Failure"   
            this.message = 'Please enter valid voucher number';
            this._cdr.writeCDR(this.startTime, 'Recharge TopUp',this.voucher_code, 'Recharge TopUp', 'Failure');
      this.showLoader = false;
           
          });    
        
      }else{
        this._RcgTopupService.getTopupDetailsOthers(this.msisdn,this.voucher_code).then((resp) => {
          if (resp.status_code == 0) {
            
              this._cdr.writeCDR(this.startTime, 'Recharge TopUp Other',this.voucher_code, 'Recharge TopUp', 'Success');      
            console.log("respppppp " +resp.status_message);
            this.message =    "Recharge done successfull";    
            this.message1="Success"   
            this.confirmdiv=false;
            this.suceesDiv=true;  
      this.showLoader = false;        
          }else{ 
               
             this._cdr.writeCDR(this.startTime, 'Recharge TopUp Other',this.voucher_code, 'Recharge TopUp', 'Failure');     
            this.message =  "Please enter valid voucher number";
            this.message1="Failure"   
            console.log("respppppp " +resp.status_message);         
            this.failurDiv=true;
            this.confirmdiv=false;  
      this.showLoader = false;      
          }   
        }).catch((err) => {
          
           this._cdr.writeCDR(this.startTime, 'Recharge TopUp Other' ,this.voucher_code, 'Recharge TopUp', 'Failure');
          this.failurDiv=true;
          this.confirmdiv=false;
          this.message = 'Please enter valid voucher number';
          this.message1="Failure"   
          this.showLoader = false;
        }); 
      }
  
    }
  }
