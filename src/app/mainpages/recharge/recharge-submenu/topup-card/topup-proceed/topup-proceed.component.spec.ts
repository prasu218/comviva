import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopupProceedComponent } from './topup-proceed.component';

describe('TopupProceedComponent', () => {
  let component: TopupProceedComponent;
  let fixture: ComponentFixture<TopupProceedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopupProceedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopupProceedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
