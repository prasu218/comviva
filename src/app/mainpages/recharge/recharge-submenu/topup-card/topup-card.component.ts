
import { Component, OnInit ,ElementRef, ViewChild,AfterViewInit,OnChanges} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { format } from '../../../../utils/formatter';
import { config } from '../../../../config';
import { rcgtopupService } from '../../../../plugins/rcgtopup';
import { StoreService } from '../../../../plugins/datastore';
import { AngularCDRService } from '../../../../plugins/angularCDR';
import { ProfileService } from '../../../../plugins/profile';
import { Router } from '@angular/router';
@Component({
  selector: 'app-topup-card',
  templateUrl: './topup-card.component.html',
  styleUrls: ['./topup-card.component.scss']
})
export class TopupCardComponent implements OnInit,AfterViewInit {
public topup_card: FormGroup;
public topup_card1: FormGroup;
public errorMsg = '';
public message :string;
public selctradio:boolean = false;
public formhide: boolean=true ;
public hideee: boolean=false ;
public failure:boolean=false;
public msisdn : string;
showLoader : boolean = false;
msisdnval = this._storeData.getMSISDNData();
value:any;
startTime: any;
internalCardDetails ={};
category: any = 'Recharge';

  constructor(private  router: Router,private _profile: ProfileService,private fb: FormBuilder,private _RcgTopupService: rcgtopupService,private _storeData: StoreService,private _cdr: AngularCDRService) { }
   
  ngOnInit() {
      this.startTime = this._cdr.getTransactionTime(0);
      this.topup_card = this.fb.group({
	  msisdnSes: ['', []],
        vouchercode: ['', [Validators.required, Validators.minLength(11), Validators.maxLength(17)]],
     });
     this.topup_card1 = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(10),Validators.maxLength(13)]],
      vouchercode1: ['', [Validators.required, Validators.minLength(11), Validators.maxLength(17)]],   });


  }
 ngAfterViewInit() {  
   // this._cdr.writeCDR(this.startTime, this.category,this.value, 'Recharge Topup_page','Success');
   
  }
  msisdnn(event): boolean {
    let test = event.target.value;
      let test_length = test.length;
        const charCode = (event.which) ? event.which : event.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
        }
		if(test_length == 11 && ( charCode == 8  || charCode == 13)){
			return true;
        }else if(test_length == 11){
			return false;;
		}else{
			return true;
		}
       }

       loadtopup(){
        window.location.reload();
        this.formhide=true;
        this.failure=false; 
        this.hideee=false;
        this.topup_card.reset();    
        this.topup_card1.reset();       
      }


       voucher(event): boolean {
        let test = event.target.value;
          let test_length = test.length;
            const charCode = (event.which) ? event.which : event.keyCode;
    
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
              return false;
            }
        if(test_length == 17 && ( charCode == 8  || charCode == 13)){
          return true;
            }else if(test_length == 17){
          return false;;
        }else{
          return true;
        }
           }


    onslectradio(_selectedObj){
      this.selctradio= _selectedObj;
      }
   

      onSubmitTopup(value: FormGroup,cvalue) {  
        this.internalCardDetails['radio']=this.selctradio
        this.internalCardDetails['msisdn']=format.msisdn(this.topup_card.get('msisdnSes').value);
        this.internalCardDetails['vouchercode']=this.topup_card.get('vouchercode').value;
        this._storeData.settopupDetails(this.internalCardDetails)
        this.router.navigate(['topupcard/proceed']);
  // this.showLoader = true;
        // const msisdn = this._storeData.getMSISDNData();
       // let msisdn = format.msisdn(this.topup_card.get('msisdn').value);
      // const msisdn ='2349062058852';
        // let voucher_code = (this.topup_card.get('vouchercode').value);
    //       this._RcgTopupService.getTopupDetails(msisdn,voucher_code).then((resp) => {
    //       if (resp.status_code == 0) {
    //         this.msisdn=this._storeData.getMSISDNData();
    //         console.log("respppppp " +resp.status_message);
    //         this.message =  resp.status_message; 
    //         this._cdr.writeCDR(this.startTime, 'Recharge TopUp',voucher_code, 'Recharge TopUp', 'Success');      
    //         this.formhide=false;
    //         this.hideee=true;
    //   this.showLoader = false;
                
    //       }else{ 
    //         this.msisdn=this._storeData.getMSISDNData();            
    //         this.message =  resp.status_message;
    //         this._cdr.writeCDR(this.startTime, 'Recharge TopUp' ,voucher_code, 'Recharge TopUp', 'Failure');
    //         console.log("respppppp " +resp.status_message);         
    //         this.failure=true;
    //         this.formhide=false; 
    //   this.showLoader = false;
                
    //       }   
    //     }).catch((err) => {
   
    //       this.msisdn=this._storeData.getMSISDNData();
    //       this.failure=true;
    //       this.formhide=false;
    //       this.message = 'Please enter valid voucher number';
    //       this._cdr.writeCDR(this.startTime, 'Recharge TopUp',voucher_code, 'Recharge TopUp', 'Failure');
    //       this.topup_card.reset();
    // this.showLoader = false;
         
    //     });    
      }


      onSubmitTopupOthers(value: FormGroup,cvalue) {  
        //const msisdn = this._storeData.getMSISDNData().toString();
	this.showLoader = true;
        let msisdn = format.msisdn(this.topup_card1.get('msisdn').value);
      // const msisdn ='2349062058850';
        // let voucher_code = (this.topup_card1.get('vouchercode1').value);
        // console.log("msisdn" +msisdn);
        // console.log("voucher_code" +voucher_code);  
        if( msisdn!=""){
          this._profile.getProfileDetails(msisdn).then((res) => {
                       // console.log("debit card++++++------=========",res)
                        if (res.status_code == 0) {
                          this.internalCardDetails['radio']=this.selctradio;
                          this.internalCardDetails['msisdn']=format.msisdn(this.topup_card1.get('msisdn').value);
        this.internalCardDetails['vouchercode']=this.topup_card1.get('vouchercode1').value;
        this._storeData.settopupDetails(this.internalCardDetails)
        this.router.navigate(['topupcard/proceed']);
      //   this._RcgTopupService.getTopupDetailsOthers(msisdn,voucher_code).then((resp) => {
      //     if (resp.status_code == 0) {
      //       this.msisdn=format.msisdn(this.topup_card1.get('msisdn').value);
      //         this._cdr.writeCDR(this.startTime, 'Recharge TopUp Other',voucher_code, 'Recharge TopUp', 'Success');      
      //       console.log("respppppp " +resp.status_message);
      //       this.message =  resp.status_message;       
      //       this.formhide=false;
      //       this.hideee=true;  
      // this.showLoader = false;        
      //     }else{ 
      //       this.msisdn=format.msisdn(this.topup_card1.get('msisdn').value);    
      //        this._cdr.writeCDR(this.startTime, 'Recharge TopUp Other',voucher_code, 'Recharge TopUp', 'Failure');     
      //       this.message =  resp.status_message;
      //       console.log("respppppp " +resp.status_message);         
      //       this.failure=true;
      //       this.formhide=false;   
      // this.showLoader = false;      
      //     }   
      //   }).catch((err) => {
      //     this.msisdn=format.msisdn(this.topup_card1.get('msisdn').value);
      //      this._cdr.writeCDR(this.startTime, 'Recharge TopUp Other' ,voucher_code, 'Recharge TopUp', 'Failure');
      //     this.failure=true;
      //     this.formhide=false;
      //     this.message = 'Please enter valid voucher number';
      //     this.topup_card1.reset();
      //     this.showLoader = false;
      //   });   
        }
      else{
   this.showLoader = false;
   this.topup_card1.get('msisdn').reset();
         
      } 
    }).catch((err) => {
      this.showLoader = false;
      this.topup_card1.get('msisdn').reset();
    
    });
}
       
      }
  

}
