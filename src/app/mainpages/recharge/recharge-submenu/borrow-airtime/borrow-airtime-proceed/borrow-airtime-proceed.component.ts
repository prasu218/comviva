import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../../../../plugins/datastore';
import { format } from '../../../../../utils/formatter';
import { rcgborrowService } from '../../../../../plugins/rcgborrow';
import { checkAndUpdateDirectiveDynamic } from '@angular/core/src/view/provider';
import { AngularCDRService } from '../../../../../plugins/angularCDR';
import { ActivatedRoute } from "@angular/router";
import { Router } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-borrow-airtime-proceed',
  templateUrl: './borrow-airtime-proceed.component.html',
  styleUrls: ['./borrow-airtime-proceed.component.scss']
})
export class BorrowAirtimeProceedComponent implements OnInit {
public product:any;
public showLoader:boolean=false;
public message:any="";
public message1:any="";
public failurDiv:boolean=false;
public suceesDiv:boolean=false;
public showsuccessdiv:boolean=true;
public header=true;

  constructor(private location: Location,private route: ActivatedRoute,private _router: Router,private _rechargeBorrowService:rcgborrowService,private _storeData: StoreService,private _cdr: AngularCDRService) { }

  ngOnInit() {
    this.product=this._storeData.getselectradioRech().msisdn;
  }
  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this._router.navigate(['dashboard']);
    }
   }
  borrowAirTime() { 
    // this.confirm = false;
    const msisdn = this._storeData.getMSISDNData();
    var serviceid = this.product; 
    // this.showLoader = true;   
    console.log("product id" +serviceid)  
        this._rechargeBorrowService.getBorrowAirTime(msisdn,serviceid)
          .then((resp) => {
            var statusCode = resp.status_code;
            if(statusCode === "0"){
              this.showLoader = false;
              this.showsuccessdiv=false;
              this.suceesDiv=true;
              // this.confirmdiv=false;
              this.message =  resp.status_message;
              this.message1="Y'ello. Your MTN service request has been processed successfully. Thank you!";
            }else{
              this.showLoader = false;
              this.showsuccessdiv=false;
              //this.suceesDiv=false;
              // this.confirmdiv=false;
              this.failurDiv=true;
              this.message =  resp.status_message;
              this.message1="Sorry, the transaction failed. Kindly, Try again";
            }
            
          })
          .catch((err) => {
            this.showLoader = false;
            this.failurDiv=true;
            this.showsuccessdiv=false;
            this.message1="failure!"
            this.message =  'Something Wrong ! Try later.';
          });     
}
}
