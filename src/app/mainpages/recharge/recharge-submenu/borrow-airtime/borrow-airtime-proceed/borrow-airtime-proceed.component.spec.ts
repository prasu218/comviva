import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowAirtimeProceedComponent } from './borrow-airtime-proceed.component';

describe('BorrowAirtimeProceedComponent', () => {
  let component: BorrowAirtimeProceedComponent;
  let fixture: ComponentFixture<BorrowAirtimeProceedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowAirtimeProceedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowAirtimeProceedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
