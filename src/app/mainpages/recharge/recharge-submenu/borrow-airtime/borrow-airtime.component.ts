import { Component, OnInit,AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { StoreService } from '../../../../plugins/datastore';
import { format } from '../../../../utils/formatter';
import { rcgborrowService } from '../../../../plugins/rcgborrow';
import { checkAndUpdateDirectiveDynamic } from '@angular/core/src/view/provider';
import { AngularCDRService } from '../../../../plugins/angularCDR';
import { Router } from '@angular/router';
@Component({
  selector: 'app-borrow-airtime',
  templateUrl: './borrow-airtime.component.html',
  styleUrls: ['./borrow-airtime.component.scss']
})
export class BorrowAirtimeComponent implements OnInit {
  public eligibilityListData = [];
  public message :string;
  public existing_cust: FormGroup;
   public brweligibility:boolean=false;
   public hideborrow:boolean=true;
   public successMsg: boolean=false ;
   public failureMsg :boolean=false;
   public selctradio:string;
   public confirm: boolean= false;
   public showLoader : boolean = false;
   public msisdn = this._storeData.getMSISDNData();
   startTime: any;
   value:any;
   category: any = 'Recharge';

  constructor(private  router: Router,private _rechargeBorrowService:rcgborrowService,private _storeData: StoreService,private fb: FormBuilder,private _cdr: AngularCDRService) { }

  ngOnInit() {
     this.startTime = this._cdr.getTransactionTime(0);
    this.showLoader = true;
    this._rechargeBorrowService.checkBorrowEligibility(this.msisdn)     
      .then((resp) => {
        if (resp.status_code === '0' && resp.status_message ==="ELIGIBLE" &&  resp.availableList && resp.availableList.length > 0) {
          this.showLoader = false;
          this.eligibilityListData = resp.availableList;
          this.selctradio=this.eligibilityListData[0].serviceid;
          this._cdr.writeCDR(this.startTime, this.category,'Borrow Airtime List','' ,'Success');
        } else {
          this.showLoader = false;
          this.brweligibility=true;
          this.hideborrow=false;
          //this.message =  resp.status_message;
           this.message =  'Sorry, you are not eligible to borrow airtime';
          this.eligibilityListData = [];
          this._cdr.writeCDR(this.startTime, this.category,'Borrow Airtime List','' ,'Failure');
        }
      })
      .catch((err) => {
        this.showLoader = false;
        this.brweligibility=true;
          this.hideborrow=false;
        this.message =  'Something Wrong ! Try later.';
        this._cdr.writeCDR(this.startTime, this.category,'Borrow Airtime List','' ,'Failure');
        this.eligibilityListData = [];
      });  
  }

   ngAfterViewInit() {  
   this._cdr.writeCDR(this.startTime, 'Recharge BorrowAirtime',this.value, 'Recharge BorrowAirtime','Success');
  }
    onslectradio(_selectedObj){   
    this.selctradio= _selectedObj;
    this._storeData.setselectradioRech(this.selctradio);
    }

    Okay(){
      this.successMsg=false;
      this.failureMsg=false;
      this.hideborrow=true;
    }
   Proceed() {
      this.confirm = true;
      this.hideborrow = false;
      this.router.navigate(['borrowAirtime/proceed']);
    }
    

  borrowAirTime(event) { 
    this.confirm = false;
    const msisdn = this._storeData.getMSISDNData();
    var serviceid = this.selctradio; 
    this.showLoader = true;   
        this._rechargeBorrowService.getBorrowAirTime(msisdn,serviceid)
          .then((resp) => {
            var statusCode = resp.status_code;
            if(statusCode === "0"){
              this.showLoader = false;
              this.hideborrow=false;
              this.successMsg=true;
              this.message =  resp.status_message;
             this._cdr.writeCDR(this.startTime, this.category,'Borrow Airtime Proceed',serviceid ,'Success');
            }else{
              this.showLoader = false;
              this.hideborrow=false;
              this.successMsg=false;
              this.failureMsg=true;
              this.message =  resp.status_message;
               this._cdr.writeCDR(this.startTime, this.category,'Borrow Airtime Proceed',serviceid ,'Failure');
            }
            
          })
          .catch((err) => {
            this.showLoader = false;
            this.hideborrow=false;
            this.failureMsg=true;
            this.message =  'Something Wrong ! Try later.';
         this._cdr.writeCDR(this.startTime, this.category,'Borrow Airtime Proceed',serviceid ,'Failure');
          });     
  
  }


  


}
