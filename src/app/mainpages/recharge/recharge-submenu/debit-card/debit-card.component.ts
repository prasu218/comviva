
import { Component, OnInit, ViewEncapsulation, OnChanges, AfterViewInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { format } from '../../../../utils/formatter';
import { config } from '../../../../config';
//import { DebitcardService } from '../../../../plugins/debitcard';
import { StoreService } from '../../../../plugins/datastore';
import { DbCallsService } from '../../../../plugins/dbCalls';
import { Router } from '@angular/router';
import { ProfileService } from '../../../../plugins/profile';
import { AngularCDRService } from '../../../../plugins/angularCDR';

declare var $: any;
@Component({
  selector: 'app-debit-card',
  templateUrl: './debit-card.component.html',
  styleUrls: ['./debit-card.component.scss']
})
export class DebitCardComponent implements  OnInit {
public showLoader = false;
public debit_card: FormGroup;
public debit_card1: FormGroup;
public ASSETS_PATH: string = config.ASSETS;
public errorMsg = '';
public message:string;
paygateURL: any;
 public selctradio:boolean = false;
 public topup_card: FormGroup;
 public topup_card1: FormGroup;
 //public errorMsg = '';
 public formhide: boolean=true ;
 public hideee: boolean=false ;
 public failure:boolean=false;
 //public message :string;
 //public selctradio:boolean = false;

msisdnval = this._storeData.getMSISDNData();
startTime: any;
category: any = 'Recharge';



  constructor(private fb: FormBuilder,private _profile: ProfileService,private _storeData: StoreService, private mydb: DbCallsService,private readonly router: Router,private _cdr: AngularCDRService) { }

 
  ngOnInit() {
    this.startTime = this._cdr.getTransactionTime(0);
      this.debit_card = this.fb.group({
	   msisdnSes: ['', []],
        amount: ['', [Validators.required, Validators.minLength(1)]],
     });
     this.debit_card1 = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(10),Validators.maxLength(13)]],
      amount1: ['', [Validators.required, Validators.minLength(1)]],
   });
   this.paygateURL = config.PLUGIN_URL + 'debitcard';
  }

    ngAfterViewInit() {  
    //this._cdr.writeCDR(this.startTime, 'Recharge Debit_card', this.value, 'Recharge Debit_card','Success');
  }


  msisdn(event): boolean {
        let test = event.target.value;
          let test_length = test.length;
            const charCode = (event.which) ? event.which : event.keyCode;
    
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
              return false;
            }
        if(test_length == 11 && ( charCode == 8  || charCode == 13)){
          return true;
            }else if(test_length ==13){
          return false;;
        }else{
          return true;
        }
           }


       amount(event): boolean {
        let test = event.target.value;
          let test_length = test.length;
            const charCode = (event.which) ? event.which : event.keyCode;
    
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
              return false;
            }
        if(test_length == 6 && ( charCode == 8  || charCode == 13)){
          return true;
            }else if(test_length == 6){
          return false;;
        }else{
          return true;
        }
           }
           
	onSubmitDebitcard(value: FormGroup) {  
            this.mydb.saveDebitcardDetails('Not generated', this._storeData.getMSISDNData(), this._storeData.getFirstNameData(),  this._storeData.getMSISDNData(),  this._storeData.getEmailData(), this.debit_card.get('amount').value,'Initiated','Recharge' ,'');
            let amount = (this.debit_card.get('amount').value);
            //console.log("amount$$$$$"+this.amount);
            let data = {'msisdn':this._storeData.getMSISDNData(),
                        'amount' : amount,
                        'self' : true             
            }
            this._storeData.setDebitAmt(JSON.stringify(data));
            this.router.navigate(['recharge/procced']);
             this._cdr.writeCDR(this.startTime, this.category,'Debit card Self Proceed',amount, 'Success');

      }


    onSubmitDebitcardOthers(value: FormGroup) {
    this.showLoader = true;
            this.mydb.saveDebitcardDetails('Not generated', this._storeData.getMSISDNData(), this._storeData.getFirstNameData(),  this.debit_card1.get('msisdn').value,  this._storeData.getEmailData(), this.debit_card.get('amount').value,'Initiated','Recharge' ,'');  
            
            let amount = (this.debit_card1.get('amount1').value);
            let data = {'msisdn':format.msisdn(this.debit_card1.get('msisdn').value),
                        'amount' : amount,
                        'self' : false                  
            }
          
            // console.log("amountother$$$$$"+this.amount);
            this._storeData.setDebitAmt(JSON.stringify(data));
            this._profile.getProfileDetails(data.msisdn).then((resp) => {
              //console.log("debit card++++++------=========",resp)
              if (resp.status_code == 0) {
              
                     this.showLoader = false;
                   this.router.navigate(['recharge/procced']);

                    this._cdr.writeCDR(this.startTime, this.category,'Debit card Other Proceed',amount, 'Success');     

              }
              else{
           this.showLoader = false;
                 this.errorMsg = 'The phone number you entered is not valid. Please Enter a valid MTN Number';
                 this.debit_card1.get('msisdn').reset();
                  this._cdr.writeCDR(this.startTime,' Recharge DebitCard others', amount, 'Recharge DebitCard', 'Failure');     
              } 
            }).catch((err) => {
              this.showLoader = false;
              this.errorMsg = 'Due to some server error your request was failed.Please try again';
              this.debit_card1.reset();
             this._cdr.writeCDR(this.startTime,' Recharge DebitCard others', amount, 'Recharge DebitCard', 'Failure');
            });
        
    }
    
    
         
    onslectradio(_selectedObj){
      this.selctradio = _selectedObj;
      }

  }    

