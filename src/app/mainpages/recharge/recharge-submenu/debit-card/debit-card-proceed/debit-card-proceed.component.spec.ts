import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebitCardProceedComponent } from './debit-card-proceed.component';

describe('DebitCardProceedComponent', () => {
  let component: DebitCardProceedComponent;
  let fixture: ComponentFixture<DebitCardProceedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebitCardProceedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebitCardProceedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
