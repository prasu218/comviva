import { Component, OnInit, Input } from '@angular/core';
import { StoreService } from 'src/app/plugins/datastore';
import { Router } from '@angular/router';
import { DbCallsService } from 'src/app/plugins/dbCalls';
import { config } from '../../../../../plugins/config';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from 'src/app/plugins/commonAPI';
declare var $: any;

@Component({
  selector: 'app-debit-card-proceed',
  templateUrl: './debit-card-proceed.component.html',
  styleUrls: ['./debit-card-proceed.component.scss']
})
export class DebitCardProceedComponent implements OnInit {

  airttt: boolean = true;
  airttt1: boolean = false;
  showsuccessdiv: Boolean = true;
  failurDiv: Boolean = false;
  suceesDiv: Boolean = false;
  showconfimdiv: Boolean = false;
  showLoader: Boolean = false;
  header=true;
  message = '';
  msisdnval = '';
  msisdnval1 = '';
  debitAmount = '';
  self:boolean;
  rechargeform: FormGroup;
  path: any;
  sessionId: any;

  constructor( private _storeData: StoreService, private router:Router,private fb: FormBuilder,
    private apiService: ApiService ) { }

  ngOnInit() {
    this.rechargeform = this.fb.group({
      msisdn: [''],
    });
    let data =  this._storeData.getDebitAmt();
    let data1 = JSON.parse(data);
    this.msisdnval = data1.msisdn;
    this.debitAmount = data1.amount;
    this.self = data1.self;
    this.sessionId = this._storeData.getSessionId();
    this.msisdnval1=this.msisdnval.split("234")[1];
  }

 
  backButton(){
    this.router.navigate(['/recharge']);
  }

  paymentconfirm() {
    const payload = {
      'msisdn': this._storeData.getMSISDNData(),
      'mtnepg_transaction_amount': this.debitAmount,
      'mtnepg_cust_id': this._storeData.getMSISDNData(),
      'mtnepg_cust_name': this._storeData.getFirstNameData(),
      'mtnepg_cust_mobile': this.msisdnval,
      'mtnepg_cust_email': this._storeData.getEmailData(),
      'transaction_type': 'Recharge',
      'bundleid': '',
      'price': '',
      'autorenew': ''
      };


      if(this.self){
        this.path ="/api/debitcard";
      }
      else{
        this.path ="/api/debitcardOthers";
      }

      
return this.apiService.postRedirectSessionValidation(this.path, this.msisdnval, this.sessionId, payload)

      
    } 

}
