import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { StoreService } from '../../../../plugins/datastore';
import {SendAirtimeService} from '../../../../plugins/send-airtime';
import { format } from '../../../../utils/formatter';
import { DbCallsService } from '../../../../plugins/dbCalls';
import { Router } from '@angular/router';
import { ProfileService } from '../../../../plugins/profile';
import { AngularCDRService } from '../../../../plugins/angularCDR';

@Component({
  selector: 'app-send-airtime',
  templateUrl: './send-airtime.component.html',
  styleUrls: ['./send-airtime.component.scss']
})
export class SendAirtimeComponent implements OnInit {
  [x: string]: any;
  send_airtime: FormGroup;
public errorMsg = '';
showLoader : boolean = false;
isShow:boolean= false;
startTime: any;

  constructor(private fb: FormBuilder,private _profile: ProfileService,private _storeData: StoreService,private _sendAirtime:SendAirtimeService,private mydb: DbCallsService,private readonly router: Router,private _cdr: AngularCDRService) { }

  ngOnInit() {
      this.send_airtime = this.fb.group({
      amount: ['', [Validators.required, Validators.minLength(2)]],
      pin: ['', [Validators.required, Validators.minLength(4)]],
      msisdn: ['', [Validators.required, Validators.minLength(10),Validators.maxLength(13)]],
     });
   
  }

  amount(event): boolean {
    let test = event.target.value;
      let test_length = test.length;
        const charCode = (event.which) ? event.which : event.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
        }
		if(test_length == 6 && ( charCode == 8  || charCode == 13)){
			return true;
        }else if(test_length == 6){
			return false;;
		}else{
			return true;
		}
       }

       msisdn(event): boolean {
        let test = event.target.value;
          let test_length = test.length;
            const charCode = (event.which) ? event.which : event.keyCode;
    
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
              return false;
            }
        if(test_length == 11 && ( charCode == 8  || charCode == 13)){
          return true;
            }else if(test_length ==13){
          return false;;
        }else{
          return true;
        }
           }
           voucher(event): boolean {
            let test = event.target.value;
              let test_length = test.length;
                const charCode = (event.which) ? event.which : event.keyCode;
        
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                  return false;
                }
            if(test_length == 17 && ( charCode == 8  || charCode == 13)){
              return true;
                }else if(test_length == 17){
              return false;;
            }else{
              return true;
            }
               }
          functionName(){
          this.isShow = !this.isShow;
       }
               sendairtime(){
		            this.showLoader = true;
                const cur_msisdn = this._storeData.getMSISDNData();
                let amount = (this.send_airtime.get('amount').value);
                let pin = (this.send_airtime.get('pin').value);
                let msisdn = format.msisdn(this.send_airtime.get('msisdn').value);
               // console.log("curmsisdn"+ cur_msisdn+"amount"+amount+"pin"+pin+"msisdn"+msisdn);
               // this.mydb.saveDebitcardDetails('Not generated', this._storeData.getMSISDNData(), this.send_airtime.get('amount').value, this.send_airtime.get('pin').value,  this._storeData.getEmailData(), this.debit_card.get('amount').value,'Initiated' );
                //let amount = (this.debit_card.get('amount').value);
    
                let data = {'cur_msisdn':this._storeData.getMSISDNData(),
                            'amount' : amount,
                            'pin':this.send_airtime.get('pin').value,
                            'msisdn' : format.msisdn(this.send_airtime.get('msisdn').value)         
                }
                this._storeData.setAirtimeAmt(JSON.stringify(data));
                 this._profile.getProfileDetails(msisdn).then((resp) => {
                  //console.log("debit card++++++------=========",resp)
                  if (resp.status_code == 0) {
                  
                         this.showLoader = false;
                this.router.navigate(['sendairtime/proceed']);

                this._cdr.writeCDR(this.startTime, 'Recharge send_airtime',amount, 'Recharge send_airtime', 'Success');      

               }
                  else{
               this.showLoader = false;
                     this.errorMsg = 'Please enter a valid MTN number';
                     this.send_airtime.get('msisdn').reset();
                      this._cdr.writeCDR(this.startTime, 'Recharge send_airtime' ,amount, 'Recharge send_airtime', 'Failure');
                  } 
                }).catch((err) => {
                  this.showLoader = false;
                  this.errorMsg = 'Due to some server error your request was failed.Please try again';
                  this.send_airtime.reset();
                   this._cdr.writeCDR(this.startTime, 'Recharge send_airtime' ,amount, 'Recharge send_airtime', 'Failure');
                });
    //            this._sendAirtime.sendairtime(cur_msisdn,amount,pin,msisdn).then((resp) => {
		// //this.showLoader = false;
    //            if (resp.status_code == 0) {
		// //this.showLoader = false;
    //              alert(resp.RespDescription);
    //            }
    //            else{
 		//   //this.showLoader = false;
    //               alert(resp.RespDescription);
    //            }
    //           });
     }

       abc(){
      // console.log("myname");
   
       }


}
