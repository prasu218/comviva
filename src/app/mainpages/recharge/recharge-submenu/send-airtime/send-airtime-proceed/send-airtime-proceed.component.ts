import { Component, OnInit, Input } from '@angular/core';
import { StoreService } from 'src/app/plugins/datastore';
import { Router } from '@angular/router';
import {SendAirtimeService} from 'src/app/plugins/send-airtime';
import { format } from 'src/app/utils/formatter';
import { AngularCDRService } from '../../../../../plugins/angularCDR';

//declare var $: any;

@Component({
  selector: 'app-send-airtime-proceed',
  templateUrl: './send-airtime-proceed.component.html',
  styleUrls: ['./send-airtime-proceed.component.scss']
})
export class SendAirtimeProceedComponent implements OnInit {

  airttt: boolean = true;
  airttt1: boolean = false;
  showsuccessdiv: Boolean = true;
  failurDiv: Boolean = false;
  suceesDiv: Boolean = false;
  showconfimdiv: Boolean = true;
  showLoader: Boolean = false;
  success:Boolean = false;
  failure:Boolean = false;
  header=true;
  message = '';
  msisdnval = '';
  msisdnval1 = '';
  debitAmount = '';
  pin='';
  cur_msisdn = '';
  self:boolean;
  Airtime:any;
  startTime: any;
  value='';
 category: any = 'Recharge';
  constructor( private _storeData: StoreService, private router:Router,private _sendAirtime:SendAirtimeService,private _cdr: AngularCDRService ) { }

  ngOnInit() {
    this.startTime = this._cdr.getTransactionTime(0);
    let data =  this._storeData.getAirtimeAmt();
    let data1 = JSON.parse(data);
    this.cur_msisdn = data1.cur_msisdn;
    this.msisdnval = data1.msisdn;
    this.debitAmount = data1.amount;
    this.pin = data1.pin;
    this.msisdnval1=this.msisdnval.split("234")[1];

  }

 
  backButton(){
    this.router.navigate(['/recharge']);
  }

  paymentconfirm() {
   //this.showLoader = true;
   //const cur_msisdn = this._storeData.getMSISDNData();
  //  let amount = (this.send_airtime.get('amount').value);
  //  let pin = (this.send_airtime.get('pin').value);
  let data =  this._storeData.getAirtimeAmt();
    let data1 = JSON.parse(data);
    this.cur_msisdn = data1.cur_msisdn;
    this.msisdnval = data1.msisdn;
    this.debitAmount = data1.amount;
    this.pin = data1.pin;
  //  let msisdn = format.msisdn(this.send_airtime.get('msisdn').value);
  // console.log("curmsisdn"+ this.cur_msisdn+"amount"+this.debitAmount+"pin"+this.pin+"msisdn"+this.msisdnval);
  this._sendAirtime.sendairtime(this.cur_msisdn,this.debitAmount,this.pin,this.msisdnval).then((resp) => {
//this.showLoader = false;
  if (resp.ResponseCode == 0) {
    this.failure = false;
    this.success = true;
	this._cdr.writeCDR(this.startTime, this.category, this.value,'SendAirtime Proceed', 'Success');
    this.showconfimdiv = false;
    this.Airtime = resp.RespDescription;
    this._cdr.writeCDR(this.startTime, this.category, 'SendAirtime',this.debitAmount,'Success');
//this.showLoader = false;
    //alert(resp.RespDescription);
  }
  else if(resp.ResponseCode == 1106){
    this.failure = true;
    this.success = false;
    this.showconfimdiv = false;
    this.Airtime = resp.RespDescription;
     this._cdr.writeCDR(this.startTime, this.category, 'SendAirtime',this.debitAmount,'Failure');
//this.showLoader = false;
    // alert(resp.RespDescription);
  }
  });
}
}
