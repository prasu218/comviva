import { Component, OnInit } from '@angular/core';

import { DemoService } from '../../plugins/demo';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {
  public headerDetails: any;
  public msisdnDetails: any;

  constructor(private _demoService : DemoService) { }

  ngOnInit() {
    this.getHeaderFromServer();
    this.getMsisdnFromServer();
  }

  ngAfterViewInit() {}

  getHeaderFromServer() {
    this._demoService.getHeaderDetails().then((resp) => {
        this.headerDetails = resp;
      }).catch((err) => {
          console.log(err);
    });
  }

  getMsisdnFromServer() {
    this._demoService.getMsisdnDetails().then((resp) => {
      this.msisdnDetails = resp;
    }).catch((err) => {
      console.log(err);
    });
  }

}

