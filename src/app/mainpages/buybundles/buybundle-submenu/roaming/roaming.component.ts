import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { bundleMasterConfig } from '../../../../../app/config/config';
import { StoreService } from '../../../../plugins/datastore';
import { MatDialog } from '@angular/material';
import { BuyBundlesService } from '../../../../plugins/buybundles';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { config } from '../../../../config';
import { getRoamingRatesCountries_Response } from '../../../../../app/config/config';
import { NgxXml2jsonService } from 'ngx-xml2json';
import { Key } from 'protractor';
import { findIndex } from 'rxjs-compat/operator/findIndex';
import { PLATFORM_SERVER_ID } from '@angular/common/src/platform_id';

@Component({
  selector: 'app-roaming',
  templateUrl: './roaming.component.html',
  styleUrls: ['./roaming.component.scss']
})
export class RoamingComponent implements OnInit {
  public roamingcountry = [];
  public roamingcountrydata: any = [];
  public elementVal: any = [];
  public selectedroamingcountry;
  public _selectedObj;
  showLoader: boolean = false;
  public arrow: boolean = false;
  public arrow1: boolean = false;
  @Input() paymentuipagemigerate: boolean = true;
  internalCardDetails = {};
  buybuttonHidden: boolean = true;
  public roamingPeriodicity = [];
  public roamingDataPeriodicity: any;
  public displayValidity;
  public active_nav_selected: string;
  public isDataLoaded: boolean = false;
  public confirm: boolean;
  public msisdnval: any;
  public countryinput:any=['NACT_NG_Others_223','NACT_NG_Others_226','NACT_NG_Others_224','NACT_NG_Others_227'];
  msisdnother = '';
  public buybundalval: any;
  public buybundalvalprice: any;
  public bundle_Category: any;
  public headinghide:boolean = false;
  public countryDropdown:boolean=false;
  public dropdownval: any = 'MTN Countries';
  public bundle_SubCategory: any;
  public bundle_SubCategorydata: any;
  public bundle_SubCategoryvoice: any;
  public bundle_SubCategorydataandvoice: any;
  public bundle_SubCategorydataMtn: any;
  selectedBundle: any = -1;
  public selfform: FormGroup;
  public othersform: FormGroup;
  public errorMsg = '';
  public selctradio: boolean = false;
  public bottoncolr: boolean = false;
  offerAvailTotalCountries: any;
  listOfOfferAvailCountry: any= ' ';
  inputCode: string;
 


  constructor(private _buyBundleService: BuyBundlesService, private _storeVal: StoreService, 
  public dialog: MatDialog, private fb: FormBuilder, private router: Router,
  public ngxXml2jsonService: NgxXml2jsonService) { }

  ngOnInit() {
    this.selfform = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(config.MIN_MSISDN_LENGTH), Validators.maxLength(config.MAX_MSISDN_LENGTH)]],
    });
    this.othersform = this.fb.group({
      msisdn1: ['', [Validators.required, Validators.minLength(config.MIN_MSISDN_LENGTH), Validators.maxLength(config.MAX_MSISDN_LENGTH)]],
    });

    this.roamingcountry = getRoamingRatesCountries_Response['prodInfo'];
    this.selectedroamingcountry = this.roamingcountry[1].COUNTRY;
    this.onChangeState(this.selectedroamingcountry);



    this.buybuttonHidden = false;
    this.msisdnval = this._storeVal.getMSISDNData();

    bundleMasterConfig.prodInfo[3].bundleTabs.forEach(element => {
      this.elementVal.push(element);
    });

    this.active_nav_selected = this.elementVal[1];
    this.bundle_Category = bundleMasterConfig.prodInfo[3].bundleCategory;
    this.bundle_SubCategory = bundleMasterConfig.prodInfo[3].bundleSubCategory[3];

    this.isDataLoaded = true;
    this.countryDropdown=true;  
    this.roamingPeriodicity = [];
    this._buyBundleService.getRoamingdata(this.msisdnval, this.bundle_Category, this.bundle_SubCategory).then((resp) => {
      this.buybundalval = resp.ResponseData.ProductDetails;
      this._storeVal.setbuybundleinternalData(resp.ResponseData.ProductDetails);
      if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {

        if (resp.ResponseData) { this.isDataLoaded = true;
                //console.log('responde data from bundle servuce,===>',resp.ResponseData)
               // console.log('the product details ', resp.ResponseData.ProductDetails.ProductDetails[2])
                const ROAMING_DATA_SUBCATEGORY = 'Roaming Data MTN Countries';
                const roamingDataBundles = resp.ResponseData.ProductDetails.ProductDetails.filter(
                  (productDetail)=> productDetail.SubCategory == ROAMING_DATA_SUBCATEGORY);
               //console.log('Roaming Data Bundles ===>', roamingDataBundles );
                this.roamingDataPeriodicity = roamingDataBundles;
                //console.log(this.roamingDataPeriodicity);
                this.roamingDataPeriodicity.sort(function (a, b) {
                return a.Price - b.Price;
          });
          this.getcountry(this.countryinput[3]);
        }

        else {
  this.showLoader=false;
          this.isDataLoaded = false;
          this.roamingPeriodicity = [];
        }

        } else {
  this.showLoader=false;
        this.roamingPeriodicity = [];
      
      }
    }).catch((err) => {
 this.showLoader=false;
      this.roamingPeriodicity = [];
    });
  }


  buyprocce(_selectedObj) {
    if (this.bottoncolr) {
      this.bottoncolr = true;
      this.internalCardDetails['msisdn'] = this.msisdnval;
      this.internalCardDetails['selfOthers'] = this._selectedObj;
      this.internalCardDetails['beneficiaryMsisdn'] = this.msisdnother;
      this._storeVal.setBuyBundlePlanDetails(this.internalCardDetails);
      this.router.navigate(['buybundles/procced/'+this.internalCardDetails['productId']]);
      this.paymentuipagemigerate = false;
    }
  }



onChangedataCountries(value){
    this.showLoader=true;
    this.dropdownval=value
    this.roamingPeriodicity=[];
    this.bundle_SubCategorydata = bundleMasterConfig.prodInfo[3].bundleSubCategory[0];
    this.bundle_SubCategorydataMtn = bundleMasterConfig.prodInfo[3].bundleSubCategory[3];
    if(value=="MTN Countries"){
    this._buyBundleService.getRoamingdata(this.msisdnval, this.bundle_Category, this.bundle_SubCategorydataMtn).then((resp) => {
      this.buybundalval = resp.ResponseData.ProductDetails;
      this._storeVal.setbuybundleinternalData(resp.ResponseData.ProductDetails);
       if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
        if (resp.ResponseData) {
                this.isDataLoaded = true;
              //  console.log('responde data from bundle servuce,===>',resp.ResponseData)
               // console.log('the product details ', resp.ResponseData.ProductDetails.ProductDetails[2])
                const ROAMING_DATA_SUBCATEGORY = 'Roaming Data MTN Countries';
                const roamingDataBundles = resp.ResponseData.ProductDetails.ProductDetails.filter(
                  (productDetail)=> productDetail.SubCategory == ROAMING_DATA_SUBCATEGORY);
               // console.log('Roaming Data Bundles ===>', roamingDataBundles );
                this.roamingDataPeriodicity = roamingDataBundles;
               // console.log(this.roamingDataPeriodicity);
                this.roamingDataPeriodicity.sort(function (a, b) {
                return a.Price - b.Price;
          });
          

        }
        else {
          this.isDataLoaded = false;
          this.roamingPeriodicity = [];
        }
       } else {
        this.roamingPeriodicity = [];
      }
        this.getcountry(this.countryinput[3]);
        this.showLoader=false;
    }).catch((err) => {
      this.roamingPeriodicity = [];
    });
  }else{
    this._buyBundleService.getRoamingdata(this.msisdnval, this.bundle_Category, this.bundle_SubCategorydata).then((resp) => {
      this.buybundalval = resp.ResponseData.ProductDetails;
      this._storeVal.setbuybundleinternalData(resp.ResponseData.ProductDetails);
      if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
        if (resp.ResponseData) {
        //console.log('responde data from bundle servuce,===>',resp.ResponseData)
               // console.log('the product details ', resp.ResponseData.ProductDetails.ProductDetails[2])
                const ROAMING_DATA_SUBCATEGORY = 'Roaming Data Bundle';
                const roamingDataBundles = resp.ResponseData.ProductDetails.ProductDetails.filter(
                (productDetail)=> productDetail.SubCategory == ROAMING_DATA_SUBCATEGORY);
                //console.log('Roaming Data Bundle ===>', roamingDataBundles );
                this.isDataLoaded = true;
                this.roamingDataPeriodicity = roamingDataBundles;
                this.isDataLoaded = true;
                //console.log(this.roamingDataPeriodicity);
                this.roamingDataPeriodicity.sort(function (a, b) {
                return a.Price - b.Price;
          });
          

        }
        else {
          this.isDataLoaded = false;
          this.roamingPeriodicity = [];
        }
       } else {
        this.roamingPeriodicity = [];
      }
        this.getcountry(this.countryinput[0]);
        this.showLoader=false;
    }).catch((err) => {
      this.roamingPeriodicity = [];
    });
  }
}
  onLoadData(_selectedObj) {
    this.showLoader=true;
    this._selectedObj = _selectedObj;
    // console.log("this._selectoblect"+ this._selectedObj);
    var index = 0;
    let bundle_Category = bundleMasterConfig.prodInfo[3].bundleCategory;
    this.bundle_SubCategorydata = bundleMasterConfig.prodInfo[3].bundleSubCategory[0];
    this.bundle_SubCategoryvoice = bundleMasterConfig.prodInfo[3].bundleSubCategory[1];
    this.bundle_SubCategorydataandvoice = bundleMasterConfig.prodInfo[3].bundleSubCategory[2];
    this.bundle_SubCategorydataMtn = bundleMasterConfig.prodInfo[3].bundleSubCategory[3];
    this._selectedObj = _selectedObj;
    this.roamingPeriodicity = [];
   if ( this.bundle_SubCategorydata == this._selectedObj) {
    this.countryDropdown=true;
    //  console.log("this.bundle_SubCategorydata2222222222"+this.bundle_SubCategorydata);
    this._buyBundleService.getRoamingdata(this.msisdnval, this.bundle_Category, this.bundle_SubCategory).then((resp) => {
      this.buybundalval = resp.ResponseData.ProductDetails;
      this._storeVal.setbuybundleinternalData(resp.ResponseData.ProductDetails);
      if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
        if (resp.ResponseData) {
          this.isDataLoaded = true; 
          //console.log('responde data from bundle servuce,===>',resp.ResponseData)
               // console.log('the product details ', resp.ResponseData.ProductDetails.ProductDetails[2])
                const ROAMING_DATA_SUBCATEGORY = 'Roaming Data MTN Countries';
                const roamingDataBundles = resp.ResponseData.ProductDetails.ProductDetails.filter(
                (productDetail)=> productDetail.SubCategory == ROAMING_DATA_SUBCATEGORY);
                //console.log('Roaming Data Bundle ===>', roamingDataBundles );
                this.isDataLoaded = true;
                this.roamingDataPeriodicity = roamingDataBundles;
                this.isDataLoaded = true;
                //console.log(this.roamingDataPeriodicity);
                this.roamingDataPeriodicity.sort(function (a, b) {
                return a.Price - b.Price;
          });

        }
        else {
          this.isDataLoaded = false;
 this.showLoader=false;
          this.roamingPeriodicity = [];
        }
       } else {
this.showLoader=false;
        this.roamingPeriodicity = [];
      }
        this.getcountry(this.countryinput[3]);
        this.showLoader=false;
    }).catch((err) => {
      this.roamingPeriodicity = [];
      this.showLoader=false;
    });


  } 
  
  if(this.bundle_SubCategoryvoice == this._selectedObj){
    this.countryDropdown=false;
    this._buyBundleService.getRoamingvoice(this.msisdnval, this.bundle_Category, this.bundle_SubCategoryvoice).then((resp) => {
      this.buybundalval = resp.ResponseData.ProductDetails;
      this._storeVal.setbuybundleinternalData(resp.ResponseData.ProductDetails);
      if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
        if (resp.ResponseData) {
          this.isDataLoaded = true;
                //console.log('responde data from bundle servuce,===>',resp.ResponseData)
                //console.log('the product details ', resp.ResponseData.ProductDetails.ProductDetails[2])
                const ROAMING_DATA_SUBCATEGORY = 'Roaming Voice Bundle';
                const roamingDataBundles = resp.ResponseData.ProductDetails.ProductDetails.filter(
                (productDetail)=> productDetail.SubCategory == ROAMING_DATA_SUBCATEGORY);
                //console.log('Roaming Data Bundle ===>', roamingDataBundles );
                this.isDataLoaded = true;
                this.roamingDataPeriodicity = roamingDataBundles;
                this.isDataLoaded = true;
                //console.log(this.roamingDataPeriodicity);
                this.roamingDataPeriodicity.sort(function (a, b) {
                return a.Price - b.Price;
          });

        }
        else {
          this.isDataLoaded = false;
          this.roamingPeriodicity = [];
    this.showLoader=false;
        }
       } else {
        this.roamingPeriodicity = [];
  this.showLoader=false;
      }
        this.getcountry(this.countryinput[1]);
        this.showLoader=false;
    }).catch((err) => {
      this.roamingPeriodicity = [];
      this.showLoader=false;
    });

  }   
 
   if(this.bundle_SubCategorydataandvoice == this._selectedObj){
    this.countryDropdown=false;
 
    this._buyBundleService.getRoamingdataandvoice(this.msisdnval, this.bundle_Category, this.bundle_SubCategorydataandvoice).then((resp) => {
      this.buybundalval = resp.ResponseData.ProductDetails;
      this._storeVal.setbuybundleinternalData(resp.ResponseData.ProductDetails);
      if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
        if (resp.ResponseData) {
          this.isDataLoaded = true;
                //console.log('responde data from bundle servuce,===>',resp.ResponseData)
               //console.log('the product details ', resp.ResponseData.ProductDetails.ProductDetails[2])
                const ROAMING_DATA_SUBCATEGORY = 'Roaming Voice and Data Bundle';
                const roamingDataBundles = resp.ResponseData.ProductDetails.ProductDetails.filter(
                (productDetail)=> productDetail.SubCategory == ROAMING_DATA_SUBCATEGORY);
               // console.log('Roaming Data Bundle ===>', roamingDataBundles );
                this.isDataLoaded = true;
                this.roamingDataPeriodicity = roamingDataBundles;
                this.isDataLoaded = true;
                //console.log(this.roamingDataPeriodicity);
                this.roamingDataPeriodicity.sort(function (a, b) {
                return a.Price - b.Price;
          });
        }
        else {
          this.isDataLoaded = false;
          this.roamingPeriodicity = [];
        }
       } else {
        this.roamingPeriodicity = [];
      }
        this.getcountry(this.countryinput[2]);
        this.showLoader=false;
    }).catch((err) => {
      this.roamingPeriodicity = [];
    });

  } 
  
  }

  xtradata() {
    if (this.arrow == false) {
      this.arrow = true;
    }
    else {
      this.arrow = false;
    }

    this.arrow1 = false;
  }

  xtratalk() {
    if (this.arrow1 == false) {
     
      this.arrow1 = true;
    }
    else {
      this.arrow1 = false;
    }
    this.arrow = false;
  }

  countries(dataAction, roamingData) {
    if (roamingData.offerCountriesArrow == false) {
      roamingData.offerCountriesArrow = true;
    }
    else {
      roamingData.offerCountriesArrow = false;
    }

  }
getcountry(data3)
{
  this._buyBundleService.getBundleCatCISCountry(this.msisdnval, data3).then((resp) => {
    this.parseStr(resp);
  
  }).catch((err) => {
  });
}
  parseStr(str) {
    var xmlStr = str;
    let jsonObj;
    if (xmlStr) {
      const parser = new DOMParser();
      xmlStr = xmlStr.replace("&", "&").replace(/\\n/g, ' ').replace(/\\/g, '').replace(/(^")|("$)/g, '');
      const xml = parser.parseFromString(xmlStr, 'text/xml');
      jsonObj = this.ngxXml2jsonService.xmlToJson(xml);
      let countryList = jsonObj.fulfillmentService.responseData.productBuy.notification;
      let inputCode = jsonObj.fulfillmentService.responseData.inputCode;
      if (countryList) {
        this.getListOfNotificationCountry(countryList)
      }
    }
  }

  getListOfNotificationCountry(listOfCountries) {
    let totalNoOfCountries = listOfCountries.split("\|")
   
    if (totalNoOfCountries) {
      this.offerAvailTotalCountries = totalNoOfCountries.length;
    
      this.showLoader=false;
      this.listOfOfferAvailCountry = totalNoOfCountries.join(", ");

    }
  }
  numberOnly(event): boolean {
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length == 11 && (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length == 17) {
      return false;;
    } else {
      return true;
    }
  }


  onslectradio(data) {
    this._selectedObj = data;
  }

  changeStatus(data, index) {
    //this.internalCardDetails = data;
    this.internalCardDetails['pname']=data.ProductName;
    this.internalCardDetails['description']=data.Description;
    this.internalCardDetails['price']=data.Price;
    this.internalCardDetails['vailidity']=data.Validity;
    this.internalCardDetails['renewal']=data.Renewal;
    this.internalCardDetails['isConsentRequired']=data.isConsentRequired;
    this.internalCardDetails['BuyForOthers']=data.BuyForOthers;
    this.internalCardDetails['action']=data.Action;
    this.internalCardDetails['active']=data.active;
     this.internalCardDetails['eligibiltyId']=data.eligibiltyId;
      this.internalCardDetails['productId']=data.ProductID;
        this.internalCardDetails['subcat']=data.SubCategory;
    this.buybundalval = this._storeVal.getbuybundleinternalData();
    if (this.selectedBundle > -1 && this.selectedBundle !== index) {
    this.roamingDataPeriodicity[this.selectedBundle].active = false;
      
    }
    this.roamingDataPeriodicity[index].active = !this.roamingDataPeriodicity[index].active;
    this.selectedBundle = index;
     this.bottoncolr = this.roamingDataPeriodicity[index].active;
  }



  //Roaming Countries of origion

  onChangeState(selectedroamingcountry) {
    this.showLoader = true;
    this.selectedroamingcountry = selectedroamingcountry;
    this._buyBundleService.getRoamingRateCountryDetails(this.msisdnval, this.selectedroamingcountry)
      .then((resp) => {
        this.showLoader = false;
        this.headinghide = true;

        
        resp.prodInfo.forEach((ele) => {
         
          let object = {
            operater: ele.OPERATOR,
            service: ele.SERVICES,
            callnig: ele.CALL_TO_NIGERIA,
            calltocurrrent: ele.CALL_TO_CURRENT_COUNTRY,
            calltoworld: ele.CALL_TO_ROW,
            incoming:ele.INCOMING_CALL,
            outgoingsms:ele.OUTGOING_SMS,
            GPRSrate:ele.GPRS_RATE,
            GPRScharging:ele.GPRS_CHARGING      
          }
          this.roamingcountrydata.push(object);
        
        });
   
      });

  }

}

