import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybundleProccedComponentRoaming } from './buybundle-procced.component';

describe('BuybundleProccedComponentGoodybag', () => {
  let component: BuybundleProccedComponentRoaming;
  let fixture: ComponentFixture<BuybundleProccedComponentRoaming>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybundleProccedComponentRoaming ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybundleProccedComponentRoaming);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
