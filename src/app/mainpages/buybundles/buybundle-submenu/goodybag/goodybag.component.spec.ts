import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodybagComponent } from './goodybag.component';

describe('GoodybagComponent', () => {
  let component: GoodybagComponent;
  let fixture: ComponentFixture<GoodybagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodybagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodybagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
