import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybundleProccedComponentGoodybag } from './buybundle-procced.component';

describe('BuybundleProccedComponentGoodybag', () => {
  let component: BuybundleProccedComponentGoodybag;
  let fixture: ComponentFixture<BuybundleProccedComponentGoodybag>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybundleProccedComponentGoodybag ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybundleProccedComponentGoodybag);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
