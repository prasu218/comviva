import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { bundleMasterConfig } from '../../../../../app/config/config';
import { StoreService } from '../../../../plugins/datastore';
import { MatDialog } from '@angular/material';
import { BuyBundlesService } from '../../../../plugins/buybundles';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { config } from '../../../../config';
import { AngularCDRService } from '../../../../plugins/angularCDR';

@Component({
  selector: 'app-goodybag',
  templateUrl: './goodybag.component.html',
  styleUrls: ['./goodybag.component.scss']
})
export class GoodybagComponent implements OnInit {
  public whatsup: boolean = false;
  public facebook: boolean = false;
  public instagram: boolean = false;
  public twogo: boolean = false;
  public wechat: boolean = false;
  public eskimi: boolean = false;
  public nimbuzz: boolean = false;
  public socialmedia: boolean = false;
  internalCardDetails ={};
  startTime: any;
category: any = 'buybundles';
value="";
  showLoader:boolean=false;
  
  

  
  @Input() paymentuipagemigerate : boolean = true;
  public elementVal: any = [];
  //public periodicityData :any= [];
   //public elementXtraValPerioicity = [];
   public elementGBFacebookPeriodicity = [];
   public displayValidity;
   
  public active_nav_selected: string;
  public _selectedObj: any;
  public isDataLoaded : boolean = false;
  public confirm: boolean;
  public msisdnval:any;
  public buybundalval:any;
  public buybundalvalprice:any;
  public bundle_Category:any;
  public bundle_SubCategory:any;
  //public benefeciaryMSISDN : any;
  selectedBundle: any = -1;
  public selfform: FormGroup;
  public othersform: FormGroup;
  public errorMsg = '';

  public selctradio:boolean = false;
public bottoncolr : boolean = false;



  constructor(private _buyBundleService: BuyBundlesService, private _storeVal: StoreService, public dialog: MatDialog, private fb: FormBuilder, private router: Router,private _cdr: AngularCDRService) { }

  ngOnInit() {

    this.selfform = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(config.MIN_MSISDN_LENGTH), Validators.maxLength(config.MAX_MSISDN_LENGTH)]],
   });
   this.othersform = this.fb.group({
    msisdn1: ['', [Validators.required, Validators.minLength(config.MIN_MSISDN_LENGTH), Validators.maxLength(config.MAX_MSISDN_LENGTH)]],
 });

 this.msisdnval =  this._storeVal.getMSISDNData();

 this.apiCall('Whatsapp');    
   const Category = bundleMasterConfig.prodInfo[4];
    let categoryArray = bundleMasterConfig.prodInfo[4].bundleSubCategory;
 
categoryArray['Whatsapp'].forEach(element => {
      this.elementVal.push(element);
    }); 


  this.active_nav_selected = this.elementVal[4];
    this.bundle_Category = bundleMasterConfig.prodInfo[4].bundleCategory; 
   this.bundle_SubCategory = categoryArray;

 this.isDataLoaded = true;
  }

  buyprocce(_selectedObj){
    
    if (this.bottoncolr){
    this.internalCardDetails['msisdn'] = this.msisdnval;
    this.internalCardDetails['selfOthers'] = this._selectedObj;
    this.paymentuipagemigerate = false;
    this._storeVal.setBuyBundlePlanDetails(this.internalCardDetails);
    this.router.navigate(['buybundles/procced/'+this.internalCardDetails['productId']]);

}
    
   }


   onLoadData(_selectedObj, bundle_SubCategory) {
    var index = 0;
    for (; index < bundle_SubCategory.length; index++) {
      // console.log("onLoadData   inside for loop ");
      // console.log("onLoadData   inside for loop bundleMasterConfig.prodInfo[4].bundleTabs", bundleMasterConfig.prodInfo[4].bundleTabs[index]);
      if (_selectedObj === bundle_SubCategory[index]) {
        this.bundle_Category = bundleMasterConfig.prodInfo[4].bundleCategory;
        this.bundle_SubCategory = bundle_SubCategory;
        break;
      }
      else {
        continue;
      }
    }



    
    this.elementGBFacebookPeriodicity = [];


    this._buyBundleService.getBuyBundleCatCIS(this.msisdnval, 'GoodyBag', bundle_SubCategory).then((resp) => {
        if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
        
          if (resp.ResponseData) {

            this.isDataLoaded = true;
            resp.ResponseData.ProductDetails.ProductDetails.sort(function (a, b) {
              return a.Price - b.Price;
            });
           



            resp.ResponseData.ProductDetails.ProductDetails.forEach((ele) => {
             
            const name = ele.SubCategory;
            
            const tabs = this.bundle_SubCategory[name];
            
            this.elementVal = [];
            tabs.forEach(element => {
              this.elementVal.push(element);
            });


              let dataObj;
              if (_selectedObj === 'Daily') {
                dataObj = {
                  pname: ele.ProductName,
                  description: ele.Description,
                  price: ele.Price,
                  vailidity: ele.Validity,
                  renewal: ele.Renewal,
                  isConsentRequired: ele.isConsentRequired,
                  BuyForOthers: ele.BuyForOthers,
                  action: ele.Action,
                  active: false,
                  eligibiltyId: ele.eligibiltyId,
                  productId: ele.ProductID,
                  subcat:ele.Category+ele.SubCategory,
                };
                if (dataObj.vailidity === '1 day') {
                  this.elementGBFacebookPeriodicity.push(dataObj);
                }
               }
              if (_selectedObj === 'Weekly') {
                dataObj = {
                  pname: ele.ProductName,
                  description: ele.Description,
                  price: ele.Price,
                  vailidity: ele.Validity,
                  renewal: ele.Renewal,
                  isConsentRequired: ele.isConsentRequired,
                  BuyForOthers: ele.BuyForOthers,
                  action: ele.Action,
                  active: false,
                  eligibiltyId: ele.eligibiltyId,
                productId: ele.ProductID,
                subcat:ele.Category+ele.SubCategory,
                };
                if (dataObj.vailidity === '7 days') {
                  this.elementGBFacebookPeriodicity.push(dataObj);
                }
               }

              if (_selectedObj === 'Monthly') {

                dataObj = {
                  pname: ele.ProductName,
                  description: ele.Description,
                  price: ele.Price,
                  vailidity: ele.Validity,
                  renewal: ele.Renewal,
                  isConsentRequired: ele.isConsentRequired,
                  BuyForOthers: ele.BuyForOthers,
                  action: ele.Action,
                  active: false,
                  eligibiltyId: ele.eligibiltyId,
                  productId: ele.ProductID,
                 subcat:ele.Category+ele.SubCategory,
                };

                if (dataObj.vailidity === '30 days') {
                  this.elementGBFacebookPeriodicity.push(dataObj);
                }
              }
            });
          }
          else {
            this.isDataLoaded = false;
            this.elementGBFacebookPeriodicity = [];
          }


        } else {
          this.elementGBFacebookPeriodicity = [];
        }
      })
      .catch((err) => {
        this.elementGBFacebookPeriodicity = [];
      });
  }

Whaatsapp(){
  if(this.whatsup == false ){
    this.whatsup = true;
  }
  else{
    this.whatsup = false;
  }
  //api call
  this.apiCall('Whatsapp');
  this.facebook = false;
  this.instagram = false;
  this.twogo = false;
  this.wechat = false;
  this.eskimi = false;
  this.nimbuzz = false;
  this.socialmedia = false;
}

Facebook(){
  if(this.facebook == false ){
    this.facebook = true;
  }
  else{
    this.facebook = false;
  }
  this.whatsup = false;
  this.instagram = false;
  this.twogo = false;
  this.wechat = false;
  this.eskimi = false;
  this.nimbuzz = false;
  this.socialmedia = false;
  //api call
  this.apiCall('Facebook');
}

Instagramm(){
  if(this.instagram == false ){
    this.instagram = true;
  }
  else{
    this.instagram = false;
  }
  this.whatsup = false;
  this.facebook = false;
  this.twogo = false; 
  this.wechat = false; 
  this.eskimi = false;
  this.nimbuzz = false;
  this.socialmedia = false;
  //api call
  this.apiCall('Instagram');
}
TwoGo(){
  if(this.twogo == false ){
    this.twogo = true;
  }
  else{
    this.twogo = false;
  }
  this.whatsup = false;
  this.facebook = false;  
  this.instagram = false;
  this.wechat = false;
  this.eskimi = false;
  this.nimbuzz = false;
  this.socialmedia = false;
  //api call
  this.apiCall('2Go');
}
Wechatt(){
  if(this.wechat == false ){
    this.wechat = true;
  }
  else{
    this.wechat = false;
  }
  this.whatsup = false;
  this.facebook = false;  
  this.instagram = false;
  this.twogo = false;
  this.eskimi = false;
  this.nimbuzz = false;
  this.socialmedia = false;

  //api call
  this.apiCall('Wechat');
}
Eskimii(){
  if(this.eskimi == false ){
    this.eskimi = true;
  }
  else{
    this.eskimi = false;
  }
  this.whatsup = false;
  this.instagram = false;
  this.twogo = false;
  this.wechat = false;
  this.facebook = false;
  this.nimbuzz = false;
  this.socialmedia = false;
  //api call
  this.apiCall('ESKIMI');
}

Nimbuzzz(){
  if(this.nimbuzz == false ){
    this.nimbuzz = true;
  }
  else{
    this.nimbuzz = false;
  }
  this.whatsup = false;
  this.instagram = false;
  this.twogo = false;
  this.wechat = false;
  this.facebook = false;
  this.eskimi = false;
  this.socialmedia = false;
  //api call
  this.apiCall('Youtube and Instagram');
}

SocialMediaa(){
  if(this.socialmedia == false ){
    this.socialmedia = true;
  }
  else{
    this.socialmedia = false;
  }
  this.whatsup = false;
  this.instagram = false;
  this.twogo = false;
  this.wechat = false;
  this.facebook = false;
  this.eskimi = false;
  this.nimbuzz = false;
  //api call
  this.apiCall('Social Bundle');
}


apiCall(bundle_SubCategory){
    this.showLoader = true;
  this.elementGBFacebookPeriodicity= [];
  
   this._buyBundleService.getBuyBundleCatCIS(this.msisdnval, 'GoodyBag', bundle_SubCategory).then((resp) => {
   this.showLoader = false;
     this.buybundalval = resp.ResponseData.ProductDetails;
    this._cdr.writeCDR(this.startTime, this.category,this.value, 'buybundles GoodyBag', 'Success to procced');
    this._storeVal.setbuybundleinternalData (resp.ResponseData.ProductDetails);
           if( (resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
 
        if (resp.ResponseData) {
               this.isDataLoaded = true;
               resp.ResponseData.ProductDetails.ProductDetails.sort(function(a, b) {
                return a.Price - b.Price;
            });
               resp.ResponseData.ProductDetails.ProductDetails.forEach((ele) => {
                  const name = ele.SubCategory;
            const tabs = this.bundle_SubCategory[name];
            
            this.elementVal = [];
            tabs.forEach(element => {
              this.elementVal.push(element);
            });
                
                   let dataObj = {
                   pname:ele.ProductName,
                   description: ele.Description,
                   price: ele.Price,
                   vailidity:ele.Validity,
                   renewal:ele.Renewal,
                   isConsentRequired:ele.isConsentRequired,
                   BuyForOthers:ele.BuyForOthers,
                   action:ele.Action,
              active: false,
              eligibiltyId:ele.eligibiltyId,
              productId: ele.ProductID,
               subcat:ele.Category+ele.SubCategory,
                 };
                  if(dataObj.vailidity === "1 day"){
          this.elementGBFacebookPeriodicity.push(dataObj);
                 }
                 
         });
             }

             else{
               this.isDataLoaded = false;
               this.elementGBFacebookPeriodicity=[];
           }
             

      
           } else {
             this.elementGBFacebookPeriodicity=[];
       //       let dataObj = {
       //         pname:"Can not Featch Pro"
       //       };
       //       this.isDataLoaded = false;
       // this.periodicityData.push(dataObj);
           }
         }).catch((err) => {
     //       let dataObj = {
     //         pname:"API Fail"
     //       };
     //       this.isDataLoaded = false;
     // this.periodicityData.push(dataObj);
	 this._cdr.writeCDR(this.startTime, this.category, this.value,'buybundles GoodyBag', 'Failure'); 
     this.elementGBFacebookPeriodicity=[];
         });

}


buyBundles(eve, bundleData) {
  //console.log('item : ' + JSON.stringify(item));
  // var msisdn = '2348102947676';
  var Action = bundleData.action;
  Action= Action.substring(Number(Action.indexOf(":")) + 1);
  var Price = bundleData.price;
  this.msisdnval =  this._storeVal.getMSISDNData();
  //var fName =  this._storeVal.getFirstnameData();
  var email = this._storeVal.getEmailData();
 var  eCheckId =""; var bankelibibality = false;
 let autorenewal:any;
let resObj:any;


}

numberOnly(event): boolean {
  let test = event.target.value;
    let test_length = test.length;
      const charCode = (event.which) ? event.which : event.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
  if(test_length == 11 && ( charCode == 8  || charCode == 13)){
    return true;
      }else if(test_length == 11){
    return false;;
  }else{
    return true;
  }
     }


    onslectradio(data){
      this._selectedObj= data;
      }

changeStatus(data, index){
  
  this.internalCardDetails = data;
  this.buybundalval =  this._storeVal.getbuybundleinternalData();
  if(this.selectedBundle > -1 && this.selectedBundle !== index){
    this.elementGBFacebookPeriodicity[this.selectedBundle].active = false;
    
    
  }
  this.elementGBFacebookPeriodicity[index].active = !this.elementGBFacebookPeriodicity[index].active;
  this.selectedBundle = index;

  this.bottoncolr = this.elementGBFacebookPeriodicity[index].active;
}



  }
