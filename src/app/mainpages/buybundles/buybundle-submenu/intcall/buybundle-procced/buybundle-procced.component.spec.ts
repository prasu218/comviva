import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybundleProccedComponentIntcall } from './buybundle-procced.component';

describe('BuybundleProccedComponentIntcall', () => {
  let component: BuybundleProccedComponentIntcall;
  let fixture: ComponentFixture<BuybundleProccedComponentIntcall>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybundleProccedComponentIntcall ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybundleProccedComponentIntcall);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
