import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntcallComponent } from './intcall.component';

describe('IntcallComponent', () => {
  let component: IntcallComponent;
  let fixture: ComponentFixture<IntcallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntcallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntcallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
