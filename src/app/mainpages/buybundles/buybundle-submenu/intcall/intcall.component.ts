// import { Component, OnInit } from '@angular/core';
// import { bundleMasterConfig } from '../../../../../app/config/config';
 import { StoreService } from '../../../../plugins/datastore';
 import { format } from '../../../../utils/formatter';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { MatDialog } from '@angular/material';
// import { BuyBundlesService } from '../../../../plugins/buybundles';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { ProfileService } from '../../../../plugins/profile';
import { config } from '../../../../config';
import { Component, OnInit, Input,ViewChild, AfterContentChecked, ChangeDetectorRef} from '@angular/core';
// import { MatDialog } from '@angular/material';
import { BuyBundlesService } from '../../../../plugins/buybundles';
import { bundleMasterConfig } from '../../../../../app/config/config';
import { Router } from '@angular/router';
// import { StoreService } from '../../../../plugins/datastore';


//import { prepaidBalanceResponse_Array, postpaidBalanceResponse_Array, activePlan_Response, otherPlan_Response, migrate_tariff_plan_Response } from '../../../../../app/config/config';

// import { BuyBundlesSubmenuComponent } from '../../../../../app/pages/mainpage/buy-bundles/buy-bundles-submenu/buy-bundles-submenu.component'


@Component({
  selector: 'app-intcall',
  templateUrl: './intcall.component.html',
  styleUrls: ['./intcall.component.scss']
})
export class IntcallComponent implements OnInit, AfterContentChecked {
  [x: string]: any;
  internalCardDetails ={};
  buybuttonHidden:boolean = true;
  gdscontentshow:boolean = false;
  public elementVal: any = [];
  selectedTabs = true;
  public intecallData :any= [];
  public active_nav_selected: string;
 public _selectedObj: boolean= false;
 public _selectradi: boolean=false;
  public isDataLoaded : boolean = false;
  public confirm: boolean;
  public activation_Channel:any;
  showLoader:boolean=false;
  msisdnval:any;
  msisdnother='';
  public bundle_Category:any;
  public bundle_SubCategory:any;
  public benefeciaryMSISDN : any;
  public selfform: FormGroup;
  public othersform: FormGroup;
  public errorMsg = '';
  public selctradio:boolean = false;
public buybundalval: any;
public bottoncolr : boolean = false;
public otherValid : Boolean= false;


  selectedBundle: any = -1;
     @Input() paymentuipagemigerate : boolean = true;

  constructor(  private fb: FormBuilder, private ref: ChangeDetectorRef,
    private readonly router: Router,
    private _buyBundleService: BuyBundlesService, private _storeVal: StoreService,private _router: Router,  private _profile: ProfileService, public dialog: MatDialog) { };
  ngOnInit() {  

    this.selfform = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(config.MIN_MSISDN_LENGTH), Validators.maxLength(config.MAX_MSISDN_LENGTH)]],
   });
   this.othersform = this.fb.group({
    msisdnother: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(13)]],
 });
    this.buybuttonHidden=false;
    this.msisdnval =  this._storeVal.getMSISDNData();
   
    

    bundleMasterConfig.prodInfo[7].bundleTabs.forEach(element => {
      this.elementVal.push(element);
    });
    
    this.active_nav_selected = this.elementVal[7];
    this.bundle_Category=bundleMasterConfig.prodInfo[7].bundleCategory;
    this.bundle_SubCategory = bundleMasterConfig.prodInfo[7].bundleSubCategory[0];


    this.isDataLoaded = true;
   this.showLoader = true;
    this._buyBundleService.getBuyBundleCatCIS(this.msisdnval, this.bundle_Category,this.bundle_SubCategory)
            .then((resp) => {
              this.showLoader = false;
      if( (resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
        if(resp.ResponseData)
        {
          this.isDataLoaded = true;
        
          resp.ResponseData.ProductDetails.ProductDetails.sort(function(a, b) {
            return a.Price - b.Price;
        });

          resp.ResponseData.ProductDetails['ProductDetails'].forEach((ele) => {
              let dataObj = {
              pname:ele.ProductName,
              description: ele.Description,
              price: ele.Price,
              vailidity:ele.Validity,
              renewal:ele.Renewal,
              isConsentRequired:ele.isConsentRequired,
              BuyForOthers:ele.BuyForOthers,
              action: ele.Action,
              active: false,
              eligibiltyId:ele.eligibiltyId,
              productId: ele.ProductID,
               subcat:ele.Category+ele.SubCategory,
            };

      this.intecallData.push(dataObj);

     
    });
        }
        else{
          this.isDataLoaded = false;
          this.intecallData=[];
      }
        
 
      } else {
        this.intecallData=[];

      }
    })
    .catch((err) => {

this.intecallData=[];
    });


        
   }
   ngAfterViewInit() {
    this.ref.detectChanges();
    
}

numberOnlyself(event): boolean {
  let test = event.target.value;
    let test_length = test.length;
      const charCode = (event.which) ? event.which : event.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
  if(test_length == 11 && ( charCode == 8  || charCode == 13)){
    return true;
      }else if(test_length == 11){
    return false;;
  }else{
    return true;
  }
     }


ngAfterContentChecked(){}


changeStatus(data,index){
  this.internalCardDetails =data;
  // this.buybundalval =  this._storeVal.getbuybundleinternalData(); 
  if(this.selectedBundle > -1 && this.selectedBundle !== index){
    this.intecallData[this.selectedBundle].active = false;
    this.selectedTabs = false;
  }
  this.intecallData[index].active = !this.intecallData[index].active;
  this.selectedBundle = index;
this.bottoncolr = this.intecallData[index].active;
this.otherValid = this.intecallData[index].active;

}




    onslectradio(data){
      this._selectradi = data;
    
    }

   onLoadData(_selectedObj) {
     this._selectedObj=_selectedObj;
     let index = 0;
     for (index; index < bundleMasterConfig.prodInfo[7].bundleTabs.length; index++) {
     
        if(_selectedObj === bundleMasterConfig.prodInfo[7].bundleTabs[index])
        {
          
          // this.bundle_Category=bundleMasterConfig.prodInfo[0].bundleCategory;
          // this.bundle_SubCategory = bundleMasterConfig.prodInfo[0].bundleSubCategory[0];
          break;
        }
        else
        {
          continue;
        }
    }

    let bundle_Category=bundleMasterConfig.prodInfo[7].bundleCategory;
    let bundle_SubCategory = bundleMasterConfig.prodInfo[7].bundleSubCategory[index];
    

    this.intecallData= [];
    this._buyBundleService.getBuyBundleCatCIS(this.msisdnval, bundle_Category,bundle_SubCategory)
            .then((resp) => {
				  this._storeVal.setbuybundleinternalData (resp.ResponseData.ProductDetails);
              
              if( (resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
                if(resp.ResponseData)
                {
                  this.isDataLoaded = true;
                  resp.ResponseData.ProductDetails.ProductDetails.sort(function(a, b) {
                    return a.Price - b.Price;
                });
                resp.ResponseData.ProductDetails.ProductDetails.forEach((ele)  => {
                      let dataObj = {
                      pname:ele.ProductName,
                      description: ele.Description,
                      price: ele.Price,
                      vailidity:ele.Validity,
                      renewal:ele.Renewal,
                      isConsentRequired:ele.isConsentRequired,
                      BuyForOthers:ele.BuyForOthers,
                      action:ele.Action,
                      active: false,
                      eligibiltyId:ele.eligibiltyId,
                      productId: ele.ProductID,
                        subcat:ele.Category+ele.SubCategory,

                    };
              this.intecallData.push(dataObj);
            });
                }
                else{
                  this.isDataLoaded = false;
                  this.intecallData=[];
              }
                
         
              } else {
                this.intecallData=[];
         
              }
            })
            .catch((err) => {
    
        this.intecallData=[];
            });

   }

  


   buyprocce(_selectradi){

    if (this.bottoncolr || this.otherValid){
      this.bottoncolr = true;
      this.otherValid = false;
      this.internalCardDetails['msisdn'] = format.msisdn(this.msisdnval);
    this.internalCardDetails['selfOthers'] = this._selectradi;
    this.internalCardDetails['beneficiaryMsisdn'] = format.msisdn(this.msisdnother);
	    this._storeVal.setBuyBundlePlanDetails(this.internalCardDetails);
    
this.checkothers();
    
    }
     }

checkothers()
     {
      this.showLoader = true;
      if( this.msisdnother!=""){
        this._profile.getProfileDetails(format.msisdn(this.msisdnother)).then((resp) => {
                      if (resp.status_code == 0) {
                      
                             this.showLoader = false;
                             this.paymentuipagemigerate = false;
                           this.router.navigate(['buybundles/procced/'+this.internalCardDetails['productId']]);
                           
                      }
                      else{
                   this.showLoader = false;
                         this.errorMsg = '';
                          this.othersform.get('msisdnother').reset();
                          this.periodicityData[this.selectedBundle].active = false;
                      } 
                    }).catch((err) => {
                      this.showLoader = false;
                      //this.errorMsg = 'Due to some server error your request was failed.Please try again';
                      // this.debit_card1.reset();
                    });
           }else{
		this.showLoader = false;
               this.paymentuipagemigerate = false;
               this.router.navigate(['buybundles/procced/'+this.internalCardDetails['productId']]);
	}  
     }

     
  onNoClick(): void {
  
    let jsonObj = {
      apicall:false
   };
    
  
 }
 
 onYesClick(): void {
 
 let jsonObj = {
   apicall:true,
   modeOfPayment:this.modeOfPayment
   
 };

 }
}
    
            
