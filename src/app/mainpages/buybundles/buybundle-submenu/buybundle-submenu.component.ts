import { Component, OnInit,Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-buybundle-submenu',
  templateUrl: './buybundle-submenu.component.html',
  styleUrls: ['./buybundle-submenu.component.scss']
})
export class BuybundleSubmenuComponent implements OnInit {
  @Input() active_nav: string;
  rechargeNavElements = [

     {id:'DataBundles', value:'Data Bundles', source :'assets/images/buy_bundle.svg',},
     {id:'Data&VoiceBundles',  value:'Data & Voice Bundles',  source :'assets/images/data_bundle.svg',},
     {id:'intcall',  value:'Int’l Calling Bundles',  source :'assets/images/international.svg',},
     {id:'roaming',  value:'Roaming Bundles',  source :'assets/images/roaming.svg',},
     {id:'goodybag',  value:'Goodybag Bundles',  source :'assets/images/social_media_bundle.svg',},
     {id:'smebundles',  value:'SME Bundles',  source :'assets/images/sme_bundle.svg',},
     {id:'hynetflex',  value:'Hynetflex Bundles',  source :'assets/images/sme_bundle.svg',},
   
  ];

  constructor(private _router: Router) { }

  ngOnInit() {

    $( document ).ready(function() {
      $('.carousel-crds').flickity({
      groupCells: true,
      freeScroll: true,
      cellAlign: 'left', 
      contain: true, 
      prevNextButtons: false,
      pageDots: true, 
      draggable: true,
      watchCSS: true,
      setGallerySize: false,
      resize: true,
      reposition: true
    });
  });
  }

  ngAfterContentChecked(){
    this.active_nav = this._router.url.split('/')[2];
    if(this._router.url === "/buybundles"){
      this.active_nav  = "DataBundles";
    }
  }

  onLoadMdNav(selectedData){
    this._router.navigate(['buybundles',selectedData.id]);
    this.active_nav = selectedData.id;
  }

}
