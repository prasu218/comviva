import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybundleProccedComponentSME } from './buybundle-procced.component';

describe('BuybundleProccedComponentSME', () => {
  let component: BuybundleProccedComponentSME;
  let fixture: ComponentFixture<BuybundleProccedComponentSME>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybundleProccedComponentSME ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybundleProccedComponentSME);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
