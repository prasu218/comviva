import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmeBundlesComponent } from './smebundles.component';

describe('SmeBundlesComponent', () => {
  let component: SmeBundlesComponent;
  let fixture: ComponentFixture<SmeBundlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmeBundlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmeBundlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
