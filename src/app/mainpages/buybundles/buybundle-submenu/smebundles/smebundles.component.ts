import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { bundleMasterConfig } from '../../../../../app/config/config';
import { format } from '../../../../utils/formatter';
import { StoreService } from '../../../../plugins/datastore';
import { MatDialog } from '@angular/material';
import { BuyBundlesService } from '../../../../plugins/buybundles';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { config } from '../../../../config';
import { ProfileService } from '../../../../plugins/profile';
import { BundleBalanceService } from 'src/app/plugins/balance';



@Component({
  selector: 'app-smebundles',
  templateUrl: './smebundles.component.html',
  styleUrls: ['./smebundles.component.scss']
})
export class SmeBundlesComponent implements OnInit {
  public bizplus: boolean = false;
  public dataplan: boolean = false;
  selectedTabs = true;
  internalCardDetails = {};
  msisdnother = '';
  @Input() paymentuipagemigerate: boolean = true;
  public elementVal: any = [];
  //public periodicityData :any= [];
  //public elementXtraValPerioicity = [];
  public elementGBSMEPeriodicity = [];
  public displayValidity;
  public bottoncolr: boolean = false;
  public _selectradi: boolean = false;
  public otherValid: Boolean = false;
  showLoader: boolean = false;
  public active_nav_selected: string;
  public _selectedObj: any;
  public isDataLoaded: boolean = false;
  public confirm: boolean;
  //public activation_Channel:any;
  public msisdnval: any;
  public buybundalval: any;
  public buybundalvalprice: any;
  public bundle_Category: any;
  public bundle_SubCategory: any;
  public benefeciaryMSISDN: any;
  selectedBundle: any = -1;
  public selfform: FormGroup;
  public othersform: FormGroup;
  public errorMsg = '';
  public input: any;
  public selctradioshare: string;
  public ProductNames: string;
  public datasharebundles: any;
  public share_id: any;
  public arrow: boolean = false;
  public arrow1: boolean = false;
  public datashare: boolean = true;
  public smedatashare1: boolean = false;
  public smedatashare: boolean = false;
  public DataShare: FormGroup;
public message:any;

  showRemainingBalnc: boolean = false;
  smedatashareBlancemsg:boolean=false;
  //public selctradio:boolean = false;

  smedatashareBlance:boolean = false;
  public responseMessage:boolean = false;
  public responseDesc: any;

  constructor(private _buyBundleService: BuyBundlesService, private _profile: ProfileService, private _storeVal: StoreService, public dialog: MatDialog, private fb: FormBuilder, private router: Router, private bundleBalnc: BundleBalanceService) { }

  ngOnInit() {
    this.remaining_balance = ''
    this.selfform = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(10),Validators.maxLength(13)]],
    });
    this.othersform = this.fb.group({
      msisdn1: ['', [Validators.required, Validators.minLength(10),Validators.maxLength(13)]],
    });
   this.DataShare=this.fb.group({
      msisdn2: ['', [Validators.required, Validators.minLength(10),Validators.maxLength(13)]],
      PIN: ['', [Validators.required, Validators.minLength(4)]],
      dataPlan: ['', Validators.required]
    });
    this.smedatashareapi();
    this.msisdnval = format.msisdn(this._storeVal.getMSISDNData());
    this.getRemainingBalnc();


    this.apiCall('SME BizPlus');
    let categoryArray = bundleMasterConfig.prodInfo[5].bundleSubCategory;

    categoryArray['SME BizPlus'].forEach(element => {
      this.elementVal.push(element);
    });

    this.active_nav_selected = this.elementVal[5];
    this.bundle_Category = bundleMasterConfig.prodInfo[5].bundleCategory;
    this.bundle_SubCategory = categoryArray;
    this.isDataLoaded = true;
  }


  buyprocce(_selectradi) {
    if (this.bottoncolr || this.otherValid) {
      this.bottoncolr = true;
      this.otherValid = false;
      this.internalCardDetails['msisdn'] = format.msisdn(this.msisdnval);
      this.internalCardDetails['selfOthers'] = this._selectradi;
      this.internalCardDetails['beneficiaryMsisdn'] = format.msisdn(this.msisdnother);
      this._storeVal.setBuyBundlePlanDetails(this.internalCardDetails);
      // this.paymentuipagemigerate = false;
      this.checkothers();
    }
  }
  checkothers() {
    this.showLoader = true;
    if (this.msisdnother != "") {
      this._profile.getProfileDetails(format.msisdn(this.msisdnother)).then((resp) => {
        if (resp.status_code == 0) {

          this.showLoader = false;
          this.paymentuipagemigerate = false;
          this.router.navigate(['buybundles/procced/'+this.internalCardDetails['productId']]);

        }
        else {
          this.showLoader = false;
          this.errorMsg = '';
          this.othersform.get('msisdnother').reset();
          this.elementGBSMEPeriodicity[this.selectedBundle].active = false;
        }
      }).catch((err) => {
        this.showLoader = false;
        this.errorMsg = 'Due to some server error your request was failed.Please try again';
        // this.debit_card1.reset();
      });
    } else {
      this.paymentuipagemigerate = false;
      this.router.navigate(['buybundles/procced/'+this.internalCardDetails['productId']]);
    }
  }


  onLoadData(_selectedObj, bundle_SubCategory) {
    var index = 0;




    this.elementGBSMEPeriodicity = [];



    this._buyBundleService.getBuyBundleCatSme(this.msisdnval, 'SME Bundles', bundle_SubCategory)
      .then((resp) => {

        if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {



          if (resp.ResponseData) {

            this.isDataLoaded = true;
            resp.ResponseData.ProductDetails.ProductDetails.sort(function (a, b) {
              return a.Price - b.Price;
            });
            resp.ResponseData.ProductDetails.ProductDetails.forEach((ele) => {
              const name = ele.SubCategory;
              const tabs = this.bundle_SubCategory[name];

              this.elementVal = [];
              tabs.forEach(element => {
                this.elementVal.push(element);
              });
              let dataObj;

              if (_selectedObj === 'Monthly') {

                dataObj = {
                  pname: ele.ProductName,
                  description: ele.Description,
                  price: ele.Price,
                  vailidity: ele.Validity,
                  renewal: ele.Renewal,
                  isConsentRequired: ele.isConsentRequired,
                  BuyForOthers: ele.BuyForOthers,
                  action: ele.Action,
                  active: false,
                  eligibiltyId: ele.eligibiltyId,
                  productId: ele.ProductID
                };

                if (dataObj.vailidity === '30 days') {
                  this.elementGBSMEPeriodicity.push(dataObj);
                }
              }
              if (_selectedObj === '2 Months') {

                dataObj = {
                  pname: ele.ProductName,
                  description: ele.Description,
                  price: ele.Price,
                  vailidity: ele.Validity,
                  renewal: ele.Renewal,
                  isConsentRequired: ele.isConsentRequired,
                  BuyForOthers: ele.BuyForOthers,
                  action: ele.Action,
                  productId: ele.ProductID
                };

                if (dataObj.vailidity === '60 days') {
                  this.elementGBSMEPeriodicity.push(dataObj);
                }
              }

            });
          }
          else {
            this.isDataLoaded = false;
            this.elementGBSMEPeriodicity = [];
          }


        } else {
          this.elementGBSMEPeriodicity = [];
        }
      })
      .catch((err) => {
        this.elementGBSMEPeriodicity = [];
      });
  }






  Bizplus() {
    if (this.bizplus == false) {
      this.bizplus = true;
    }
    else {
      this.bizplus = false;
    }
    //api call
    this.apiCall('SME BizPlus');
    this.dataplan = false;
    this.smedatashare = false;
    this.datashare = true;
  }

  Dataplan() {
    if (this.dataplan == false) {
      this.dataplan = true;
    }
    else {
      this.dataplan = false;
    }
    this.apiCall('SME Data Share Bundles');
    this.bizplus = false;
    this.smedatashare = false;
    this.datashare = true;
	
  }



  apiCall(bundle_SubCategory) {

    this.elementGBSMEPeriodicity = [];

    this._buyBundleService.getBuyBundleCatSme(this.msisdnval, 'SME Bundles', bundle_SubCategory).then((resp) => {
      // this.buybundalval = resp.ResponseData.ProductDetails.ProductDetails[5].ProductName;
      this.buybundalval = resp.ResponseData.ProductDetails;
      // this.buybundalvalprice = resp.ResponseData.ProductDetails.ProductDetails[5].Price;
      // this.buybundalval = resp.ResponseData.ProductDetails.ProductName;

      this._storeVal.setbuybundleinternalData(resp.ResponseData.ProductDetails);
      // this._storeVal.setOtpData(resp.otp)   
      if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
        if (resp.ResponseData) {
          this.isDataLoaded = true;
          resp.ResponseData.ProductDetails.ProductDetails.sort(function (a, b) {
            return a.Price - b.Price;
          });
          resp.ResponseData.ProductDetails.ProductDetails.forEach((ele) => {

            const name = ele.SubCategory;
            const tabs = this.bundle_SubCategory[name];

            this.elementVal = [];
            tabs.forEach(element => {
              this.elementVal.push(element);
            });

            let dataObj = {
              pname: ele.ProductName,
              description: ele.Description,
              price: ele.Price,
              vailidity: ele.Validity,
              renewal: ele.Renewal,
              isConsentRequired: ele.isConsentRequired,
              BuyForOthers: ele.BuyForOthers,
              action: ele.Action,
              active: false,
              productId: ele.ProductID
            };
            if (dataObj.vailidity === "30 days") {
              this.elementGBSMEPeriodicity.push(dataObj);
            }

          });
        }

        else {
          this.isDataLoaded = false;
          this.elementGBSMEPeriodicity = [];
        }


      } else {
        this.elementGBSMEPeriodicity = [];
      }
    }).catch((err) => {
      this.elementGBSMEPeriodicity = [];
    });

  }




  buyBundles(eve, bundleData) {
    // var msisdn = '2348102947676';
    var Action = bundleData.action;
    Action = Action.substring(Number(Action.indexOf(":")) + 1);
    var Price = bundleData.price;
    this.msisdnval = format.msisdn(this._storeVal.getMSISDNData());
    //var fName =  this._storeVal.getFirstnameData();
    var email = this._storeVal.getEmailData();
    var eCheckId = ""; var bankelibibality = false;
    let autorenewal: any;
    let resObj: any;


  }

  numberOnly(event): boolean {
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length == 11 && (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length == 13) {
      return false;;
    } else {
      return true;
    }
  }


  //onslectradio(data){
  //this._selectedObj= data;
  //}

  changeStatus(data, index) {
    this.internalCardDetails = data;
    this.buybundalval = this._storeVal.getbuybundleinternalData();
    if (this.selectedBundle > -1 && this.selectedBundle !== index) {
      this.elementGBSMEPeriodicity[this.selectedBundle].active = false;
      this.selectedTabs = false;


    }
    this.elementGBSMEPeriodicity[index].active = !this.elementGBSMEPeriodicity[index].active;
    this.selectedBundle = index;
    this.bottoncolr = this.elementGBSMEPeriodicity[index].active;
    this.otherValid = this.elementGBSMEPeriodicity[index].active;
  }

  onslectradio(data) {
    this._selectradi = data;
  }

  selctradio_1: any;
  public product: any;

  onslectradioshare(m2u, prodId) {
    this.selctradioshare = m2u.split(":");
    this.product = this.selctradioshare[1];

  }

  smedatashared() {
    if (this.smedatashare1 == false) {
      this.smedatashare1 = true;
      this.datashare = false;
      this.getRemainingBalnc();
     // console.log("getRemainingBalnc@" +this.remaining_balance);
      if(this.remaining_balance == 0 || this.remaining_balance == ''){
        this.message='Please buy a SME bundle to avail data share feature.';
            this.smedatashareBlancemsg=true;
      }
      else{
        this.smedatashareBlance=true;
      }
    }
    else {
      this.smedatashare1 = false;
      this.datashare=true;
      this.smedatashareBlance=false;
      this.smedatashareBlancemsg=false;
    }
    this.smedatashare = false;
    this.bizplus = false;
    this.dataplan = false;
  }
  smedatashareapi() {
    let msisdn = this._storeVal.getMSISDNData();

    this._buyBundleService.getSmeDataShare(msisdn).then(resp => {
      //this.datasharebundles=resp;

      if (true) {
        //alert("inside");
        this.ProductNames = resp.ResponseData.ProductDetails.ProductDetails;
        // this.ProductNames_1=this.datasharebundles.ResponseData.ProductDetails.ProductDetails[1].ProductName;
        // this.ProductNames_2=this.datasharebundles.ResponseData.ProductDetails.ProductDetails[2].ProductName;
        // this.ProductNames_3=this.datasharebundles.ResponseData.ProductDetails.ProductDetails[3].ProductName;
        // this.ProductNames_4=this.datasharebundles.ResponseData.ProductDetails.ProductDetails[4].ProductName;
        //this.share_id=resp.ResponseData.ProductDetails.ProductDetails.Me2U;
        //console.log("share_id",this.share_id);

      }

    });
  }

  DataSharedSme(value: FormGroup) {
    let msisdn = this._storeVal.getMSISDNData();
    let msisdn2 = format.msisdn(this.DataShare.get('msisdn2').value);
    let PIN = (this.DataShare.get('PIN').value);
    // console.log("this.Beneficiary",this.befiniciaryNumber);
    this.share_id = this.product;
    //this.input=this.selctradio_1;
    //console.log("input", this.input);
    this._buyBundleService.SmeDataShare(msisdn, msisdn2, PIN, this.share_id).then(resp => {
      //this.datasharebundles=resp;

      this.responseDesc = resp.RespDescription;
      this.responseMessage = true;

      this.DataShare.reset();

    });

  }
  public remaining_balance: any = '';
  getRemainingBalnc() {
    let new_balance: any;
    this.showLoader = true;

    this.bundleBalnc.getOtherBalanceCalculated(this.msisdnval).then(
      resp => {
        //console.log("resp::::::@", resp)
        this.showLoader = false;

        if (resp.result.length > 0) {
          for (var j = 0; j < resp.result.length; j++) {
           // console.log("iujdhfhjdfsfdo" + resp.result[j].daID)
            if (resp.result[j].daID == '8054') {
              new_balance = ((Number(resp.result[j].daBalance)) / (Number(resp.result[j].Con_Rate)));
              this.remaining_balance = Number(new_balance);
              //console.log("fuysg" + new_balance);
            }
          }
          if (this.remaining_balance >= 1024) {
            this.remaining_balance = (this.remaining_balance / 1024).toFixed(2);
            this.remaining_balance = this.remaining_balance + " GB";
            //console.log("this.remaining_balance:::::GB");
          }
        }
      })
  }
}
