import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybundleSubmenuComponent } from './buybundle-submenu.component';

describe('BuybundleSubmenuComponent', () => {
  let component: BuybundleSubmenuComponent;
  let fixture: ComponentFixture<BuybundleSubmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybundleSubmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybundleSubmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
