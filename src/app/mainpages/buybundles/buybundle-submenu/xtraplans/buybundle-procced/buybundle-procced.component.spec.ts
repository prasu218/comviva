import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybundleProccedComponentXtraplans } from './buybundle-procced.component';

describe('BuybundleProccedComponentXtraplans', () => {
  let component: BuybundleProccedComponentXtraplans;
  let fixture: ComponentFixture<BuybundleProccedComponentXtraplans>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybundleProccedComponentXtraplans ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybundleProccedComponentXtraplans);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
