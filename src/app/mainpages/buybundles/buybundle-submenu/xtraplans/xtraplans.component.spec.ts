import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XtraplansComponent } from './xtraplans.component';

describe('XtraplansComponent', () => {
  let component: XtraplansComponent;
  let fixture: ComponentFixture<XtraplansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtraplansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtraplansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
