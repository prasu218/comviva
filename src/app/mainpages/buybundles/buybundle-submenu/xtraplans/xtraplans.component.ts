import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { bundleMasterConfig } from '../../../../../app/config/config';
import { StoreService } from '../../../../plugins/datastore';
import { MatDialog } from '@angular/material';
import { BuyBundlesService } from '../../../../plugins/buybundles';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
// import { format } from '../../utils/formatter';
 //import { config } from '../../../config';
import { config } from '../../../../config';


@Component({
  selector: 'app-xtraplans',
  templateUrl: './xtraplans.component.html',
  styleUrls: ['./xtraplans.component.scss']
})
export class XtraplansComponent implements OnInit {
  public arrow: boolean = false;
  public arrow1: boolean = false;
   //@Input() payment : boolean = true;
   @Input() paymentuipagemigerate : boolean = true;
   showLoader:boolean=false;
  //  public selectxtra:boolean = true;
  internalCardDetails ={};
  selectedTabs = true;
  
  buybuttonHidden:boolean = true;
  //gdscontentshow:boolean = false;
  public elementVal: any = [];
  //public periodicityData :any= [];
   public elementXtraValPerioicity = [];
   public displayValidity;
  public _selectradi: boolean=false;

  // @Input() data : boolean = true;
   
  public active_nav_selected: string;
  public _selectedObj: any;


public _selected  
  public isDataLoaded : boolean = false;
  public confirm: boolean;
  //public activation_Channel:any;
  public msisdnval:any;
  
  msisdnother='';
  public buybundalval:any;
  public buybundalvalprice:any;
  public bundle_Category:any;
  public bundle_SubCategory:any;
  public benefeciaryMSISDN : any;
  selectedBundle: any = -1;

  //public existing_cust: FormGroup;

  public selfform: FormGroup;
public othersform: FormGroup;
public errorMsg = '';

 public selctradio:boolean = false;
public bottoncolr : boolean = false;
public otherValid : Boolean= false;




  



   constructor(private _buyBundleService: BuyBundlesService, private _storeVal: StoreService, public dialog: MatDialog, private fb: FormBuilder, private router: Router) { }

   ngOnInit() {
 
    this.selfform = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(config.MIN_MSISDN_LENGTH), Validators.maxLength(config.MAX_MSISDN_LENGTH)]],
   });
   this.othersform = this.fb.group({
    msisdn1: ['', [Validators.required, Validators.minLength(config.MIN_MSISDN_LENGTH), Validators.maxLength(config.MAX_MSISDN_LENGTH)]],
 });

 this.buybuttonHidden=false;
 this.msisdnval =  this._storeVal.getMSISDNData();

 

    //this.buybuttonHidden=false;
    // this.msisdnval =  this._storeVal.getMSISDNData();
    // console.log("this.msisdnval",this.msisdnval);
    // console.log(' activationChannel ---' + bundleMasterConfig.prodInfo[1].activationChannel);
    // console.log(' bundleCategory ---' + bundleMasterConfig.prodInfo[1].bundleCategory);
    // console.log(' bundleSubCategory ---' + bundleMasterConfig.prodInfo[1].bundleSubCategory);
    // console.log(' bundleTabs ---' + bundleMasterConfig.prodInfo[1].bundleTabs);
    // console.log(' msisdn 2019---' + this.msisdnval);

    this.apiCall('XTRADATA');    
    
    // for(var i=0; i<bundleMasterConfig.prodInfo[1].bundleSubCategory.length; i++)
    // {

    //   console.log(' bundleSubCategory -----'+ i + '---' + bundleMasterConfig.prodInfo[1].bundleSubCategory[i]);
    //   console.log(' bundleTabs -----'+ i + '---' + bundleMasterConfig.prodInfo[1].bundleTabs[i]);
    // }


    bundleMasterConfig.prodInfo[1].bundleTabs.forEach(element => {
      this.elementVal.push(element);
    });
    
    this.active_nav_selected = this.elementVal[1];
    this.bundle_Category=bundleMasterConfig.prodInfo[1].bundleCategory;
    this.bundle_SubCategory = bundleMasterConfig.prodInfo[1].bundleSubCategory[1];
    this.isDataLoaded = true;

    
   }

   
    



  onLoadData(_selectedObj,subCat) {
    this._selectedObj=_selectedObj;

    
    
     var index = 0;
    for (; index < bundleMasterConfig.prodInfo[1].bundleTabs.length; index++) {
      if (_selectedObj === bundleMasterConfig.prodInfo[1].bundleTabs[index]) {
         // this.bundle_Category=bundleMasterConfig.prodInfo[1].bundleCategory;
         // this.bundle_SubCategory = bundleMasterConfig.prodInfo[1].bundleSubCategory[1];
         break;
       }
      else {
         continue;
       }
   }



   this.bundle_Category=bundleMasterConfig.prodInfo[1].bundleCategory;
   this.bundle_SubCategory = bundleMasterConfig.prodInfo[1].bundleSubCategory[index];
   this.elementXtraValPerioicity= [];
   

  //  this._buyBundleService.getBuyBundleCatCIS(this.msisdnval, this.bundle_Category,this.bundle_SubCategory)
   this._buyBundleService.getBuyBundleCatCIS(this.msisdnval, 'Data and Voice',subCat)
           .then((resp) => {
         // console.log("respprasa " +JSON.stringify(resp));
         //   console.log("dataannnnuuuu "+JSON.stringify(resp.ResponseData.ProductDetails.ProductDetails[1].ProductName));

             if( (resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {


           
          

               if(resp.ResponseData)
               {
            
                 this.isDataLoaded = true;
                 resp.ResponseData.ProductDetails.ProductDetails.sort(function(a, b) {
                  return a.Price - b.Price;
              });
                 resp.ResponseData.ProductDetails.ProductDetails.forEach((ele) => {
                  
                  // console.log("resp.ProductDetails.ProductDetails      insidew====2019" + resp.ResponseData.ProductDetails.ProductDetails[0].ProductName);
                  // console.log("resp.ProductDetails.ProductDetails      insidew====2019 validity" + resp.ResponseData.ProductDetails.ProductDetails[0].Validity);
                   let dataObj;
                    // console.log("pnamedata" +dataObj.pname);
                   // alert("jdskjd"+_selectedObj);
                     if(_selectedObj === 'Weekly'){// &&  resp.ResponseData.ProductDetails.ProductDetails[0].Validity === "7 days"){
                  
                     dataObj = {
                      pname:ele.ProductName,
                      description: ele.Description,
                      price: ele.Price,
                      vailidity:ele.Validity,
                      renewal:ele.Renewal,
                      isConsentRequired:ele.isConsentRequired,
                      BuyForOthers:ele.BuyForOthers,
                      action:ele.Action,
                      active: false,
                      eligibiltyId:ele.eligibiltyId,
                  productId: ele.ProductID,
                   subcat:ele.Category+ele.SubCategory,
                    };
                    if(dataObj.vailidity === "7 days"){
                    this.elementXtraValPerioicity.push(dataObj);
                    }
                  }

                     if(_selectedObj === 'Monthly'){
                    
                     dataObj = {
                      pname:ele.ProductName,
                      description: ele.Description,
                      price: ele.Price,
                      vailidity:ele.Validity,
                      renewal:ele.Renewal,
                      isConsentRequired:ele.isConsentRequired,
                      BuyForOthers:ele.BuyForOthers,
                      action:ele.Action,
                      active: false,
                      eligibiltyId:ele.eligibiltyId,
                  productId: ele.ProductID,
                   subcat:ele.Category+ele.SubCategory,
                    };

                    if(dataObj.vailidity === "30 days"){
                   this.elementXtraValPerioicity.push(dataObj);
                    }
                  }
                  // else{
                  //   console.log("_selectedObj"+_selectedObj+"-----resp.ResponseData.ProductDetails.ProductDetails[0].Validity----->"+resp.ResponseData.ProductDetails.ProductDetails[0].ProductID)
                  // }
                
                   //console.log("The data ---->     "+this.elementXtraValPerioicity)
                   
            // this.elementXtraValPerioicity.push(dataObj);

             //console.log("elementXtraValPerioicitypnamedata" +this.elementXtraValPerioicity.toString());
           });
               }
               else{
                 this.isDataLoaded = false;
                 this.elementXtraValPerioicity=[];
             }
               

        
             } else {
               this.elementXtraValPerioicity=[];
         //       let dataObj = {
         //         pname:"Can not Featch Pro"
         //       };
         //       this.isDataLoaded = false;
         // this.periodicityData.push(dataObj);
             }
           })
           .catch((err) => {
       //       let dataObj = {
       //         pname:"API Fail"
       //       };
       //       this.isDataLoaded = false;
       // this.periodicityData.push(dataObj);
       this.elementXtraValPerioicity=[];
           });



  }

  





  xtradata(){
    if(this.arrow == false ){
      this.arrow = true;
    }
    else{
      this.arrow = false;
    }
    //api call
    this.apiCall('XTRADATA');
    this.arrow1 = false;
  }

  xtratalk(){
    if(this.arrow1 == false ){
      this.arrow1 = true;
    }
    else{
      this.arrow1 = false;
    }
    this.arrow = false;
    
    //api call
    this.apiCall('XTRATALK');
  }

  
  apiCall(subCat){
    this.showLoader = true;
    this.elementXtraValPerioicity= [];

    this._buyBundleService.getBuyBundleCatCIS(this.msisdnval, 'Data and Voice',subCat).then((resp) => {
this.showLoader = false;
      // this.buybundalval = resp.ResponseData.ProductDetails.ProductDetails[0].ProductName;
       this.buybundalval = resp.ResponseData.ProductDetails;
      // this.buybundalvalprice = resp.ResponseData.ProductDetails.ProductDetails[0].Price;
      // this.buybundalval = resp.ResponseData.ProductDetails.ProductName;

    
      this._storeVal.setbuybundleinternalData (resp.ResponseData.ProductDetails);
      // this._storeVal.setOtpData(resp.otp)   
             if( (resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
        if (resp.ResponseData) {
                 this.isDataLoaded = true;
                 resp.ResponseData.ProductDetails.ProductDetails.sort(function(a, b) {
                  return a.Price - b.Price;
              });
                 resp.ResponseData.ProductDetails.ProductDetails.forEach((ele) => {

                  
                  
                     let dataObj = {
                     pname:ele.ProductName,
                     description: ele.Description,
                     price: ele.Price,
                     vailidity:ele.Validity,
                     renewal:ele.Renewal,
                     isConsentRequired:ele.isConsentRequired,
                     BuyForOthers:ele.BuyForOthers,
                     action:ele.Action,
              active: false,
              eligibiltyId:ele.eligibiltyId,
              productId: ele.ProductID,
               subcat:ele.Category+ele.SubCategory,
                               };
                   //console.log("dataojectgoody" +JSON.stringify(this.elementXtraValPerioicity));
                  
                   if(dataObj.vailidity === "7 days"){
            this.elementXtraValPerioicity.push(dataObj);
                   }
           });
               }

               else{
                 this.isDataLoaded = false;
                 this.elementXtraValPerioicity=[];
             }
               

        
             } else {
               this.elementXtraValPerioicity=[];
         //       let dataObj = {
         //         pname:"Can not Featch Pro"
         //       };
         //       this.isDataLoaded = false;
         // this.periodicityData.push(dataObj);
             }
           }).catch((err) => {
       //       let dataObj = {
       //         pname:"API Fail"
       //       };
       //       this.isDataLoaded = false;
       // this.periodicityData.push(dataObj);
       this.elementXtraValPerioicity=[];
           });

  }


  buyBundles(eve, bundleData) {
    //console.log('item : ' + JSON.stringify(item));
    // var msisdn = '2348102947676';
    var Action = bundleData.action;
    Action= Action.substring(Number(Action.indexOf(":")) + 1);
    var Price = bundleData.price;
    this.msisdnval =  this._storeVal.getMSISDNData();
    //var fName =  this._storeVal.getFirstnameData();
    var email = this._storeVal.getEmailData();
   var  eCheckId =""; var bankelibibality = false;
   let autorenewal:any;
  let resObj:any;
  
  
  }

  numberOnly(event): boolean {
    let test = event.target.value;
      let test_length = test.length;
        const charCode = (event.which) ? event.which : event.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
        }
		if(test_length == 11 && ( charCode == 8  || charCode == 13)){
			return true;
        }else if(test_length == 11){
			return false;;
		}else{
			return true;
		}
       }


  onslectradio(data){
   this._selectradi = data;
    }

    buyprocce(_selectradi){

      if (this.bottoncolr || this.otherValid){
         this.bottoncolr = true;
         this.otherValid = false;
         this.internalCardDetails['msisdn'] = this.msisdnval;
       this.internalCardDetails['selfOthers'] = this._selectradi;
       this.internalCardDetails['beneficiaryMsisdn'] = this.msisdnother;
       this._storeVal.setBuyBundlePlanDetails(this.internalCardDetails);
      this.router.navigate(['buybundles/procced/'+this.internalCardDetails['productId']]);
   
       this.paymentuipagemigerate = false;
         
       
       }
        }

  changeStatus(data, index){
    this.internalCardDetails = data;

    this.buybundalval =  this._storeVal.getbuybundleinternalData();
    // console.log('changestatusthis.buybundalval' +this.buybundalval);
    if(this.selectedBundle > -1 && this.selectedBundle !== index){
      this.elementXtraValPerioicity[this.selectedBundle].active = false;

      this.selectedTabs = false;
      
    }
    this.elementXtraValPerioicity[index].active = !this.elementXtraValPerioicity[index].active;
    this.selectedBundle = index;
 this.bottoncolr = this.elementXtraValPerioicity[index].active;
 this.otherValid = this.elementXtraValPerioicity[index].active;
  }
}
