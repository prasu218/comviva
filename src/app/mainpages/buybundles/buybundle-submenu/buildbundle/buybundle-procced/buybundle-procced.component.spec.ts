import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybundleProccedComponenbuildbundle } from './buybundle-procced.component';

describe('BuybundleProccedComponenSponsored', () => {
  let component: BuybundleProccedComponenbuildbundle;
  let fixture: ComponentFixture<BuybundleProccedComponenbuildbundle>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybundleProccedComponenbuildbundle]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybundleProccedComponenbuildbundle);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
