import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/plugins/datastore/datastore.service';
import { Router } from '@angular/router';
import { EligibilityService } from 'src/app/plugins/eligibility';
@Component({
  selector: 'app-buybundle-procced',
  templateUrl: './buybundle-procced.component.html',
  styleUrls: ['./buybundle-procced.component.scss']
})
export class BuybundleProccedComponenbuildbundle implements OnInit {
  public msisdnval:any;
  public proceedData: any;
  public eligibility: any;
 showLoader: boolean = false;
  header=true;
  constructor(
    private _eligibilityService: EligibilityService, private _storeVal: StoreService,private router: Router) { }

  ngOnInit() {
     let eligibiltyId = '';
    let action = '';
    this.proceedData = this._storeVal.getBuyBundlePlanDetails();
    this.msisdnval =  this._storeVal.getMSISDNData();
    //console.log('this.proceedData.eligibiltyId',this.proceedData.eligibiltyId)
    //console.log('this.proceedData.action',this.proceedData.action)
    if(this.proceedData.eligibiltyId) {
      //console.log('this.proceedData.eligibiltyId',this.proceedData.eligibiltyId)
      eligibiltyId = this.proceedData.eligibiltyId.split(':')[1];
      this._eligibilityService.getEligibility(this.msisdnval,eligibiltyId,action).then((resp) => {
    
        this.showLoader = false;
              this.eligibility = resp.ResponseCode;
            }).catch(err =>{
        
            });
    }else{
      this.eligibility = '0';
      this.showLoader = false;
    }
    if(this.proceedData.action) {
      //console.log('this.proceedData.action',this.proceedData.action)
      action = this.proceedData.action.split(':')[1];
    }
    //console.log(' this.proceedData ', this.proceedData )
  }

  
  backout(){
    this.router.navigate(['buybundles'])
  }


}
