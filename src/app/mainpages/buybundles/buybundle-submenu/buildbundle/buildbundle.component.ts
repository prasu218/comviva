import { Component, OnInit, Input} from '@angular/core';
import { bundleMasterConfig } from '../../../../../app/config/config';
import { StoreService } from '../../../../plugins/datastore';
import { MatDialog } from '@angular/material';
import { BuyBundlesService } from '../../../../plugins/buybundles';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { config } from '../../../../config';

@Component({
  selector: 'app-buildbundle',
  templateUrl: './buildbundle.component.html',
  styleUrls: ['./buildbundle.component.scss']
})
export class BuildbundleComponent implements OnInit {
  public arrow: boolean = false;
  public arrow1: boolean = false;
  @Input() paymentuipagemigerate: boolean = true;
  showLoader: boolean = false;
  public selfform: FormGroup;
  public msisdnval: any;
  public elementVal: any = [];
  internalCardDetails = {};
  selectedTabs = true;
  public bundle_Category: any;
  public bundle_SubCategory: any;
  public _selectradi: boolean = false;
  public active_nav_selected: string;
  public isDataLoaded: boolean = false;
  public _selectedObj: any;
   public buildbundle = [];
   public buybundalval: any;
   public buybundalvalprice: any;
   msisdnother = '';
   public benefeciaryMSISDN: any;
   selectedBundle: any = -1;

   public othersform: FormGroup;
   public errorMsg = '';
   public selctradio: boolean = false;
   public bottoncolr: boolean = false;
   public otherValid: Boolean = false;
   public formdata: boolean = false;
   public annu = '';
 SenderIDForm:FormGroup; 
   apply:boolean;
   icons:boolean;
  //  public benficiarynumbers = "8142907336,1234567890";
   productForm: FormGroup;
   BenficiaryNum: FormGroup;
   isValid : boolean = false;
  constructor(private _buyBundleService: BuyBundlesService, private _storeVal: StoreService, public dialog: MatDialog, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.selfform = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(config.MIN_MSISDN_LENGTH), Validators.maxLength(config.MAX_MSISDN_LENGTH)]],
    });


    this.msisdnval = this._storeVal.getMSISDNData();
    // this.apiCall('Buildbundle');    

    bundleMasterConfig.prodInfo[8].bundleTabs.forEach(element => {
      this.elementVal.push(element);
    });

    this.active_nav_selected = this.elementVal[8];
    this.bundle_Category = bundleMasterConfig.prodInfo[8].bundleCategory;
    this.bundle_SubCategory = bundleMasterConfig.prodInfo[8].bundleSubCategory[0];
    this.isDataLoaded = true;
  
  }

  onslectradio(data) {
    this._selectradi = data;
  }

  // buycart(){
    // alert("hiiiiiiiiiiiii");
    // this.router.navigate(['/cartbuildproceed']);

  // }

  buyprocce(_selectradi) {
   
      this.internalCardDetails['msisdn'] = this.msisdnval;
      this.internalCardDetails['selfOthers'] = this._selectradi;
      this.internalCardDetails['beneficiaryMsisdn'] = this.msisdnother;
      this._storeVal.setBuyBundlePlanDetails(this.internalCardDetails);
    alert("buyproc"+JSON.stringify(this.internalCardDetails));
    console.log("buyproce"+JSON.stringify(this.internalCardDetails));
   this.router.navigate(['Cartbuildproceed']);
      // this.router.navigate(['buybundles/procced/' + this.internalCardDetails['productId']]);

      // this.paymentuipagemigerate = false;
 
   
  }

  // buyprocced() {
// payment page

    // if (this.bottoncolr || this.otherValid) {
    //   this.bottoncolr = true;
    //   this.otherValid = false;
    //   this.internalCardDetails['msisdn'] = this.msisdnval;
    //   this.internalCardDetails['selfOthers'] = this._selectradi;
    //   this.internalCardDetails['beneficiaryMsisdn'] = this.msisdnother;
    //   this._storeVal.setBuyBundlePlanDetails(this.internalCardDetails);
    //   this.router.navigate(['buybundles/procced/' + this.internalCardDetails['productId']]);

    //   this.paymentuipagemigerate = false;
    // }
  // }

  changeStatus(data, index) {
    this.internalCardDetails = data;

    this.buybundalval = this._storeVal.getbuybundleinternalData();
    if (this.selectedBundle > -1 && this.selectedBundle !== index) {
      this.buildbundle[this.selectedBundle].active = false;
      this.selectedTabs = false;
    }
    if(data.bundleCounter > 0) {
      this.isValid = true;
    } else {
      this.isValid = false;
    }
    this.buildbundle[index].active = !this.buildbundle[index].active;
    this.selectedBundle = index;
    this.bottoncolr = this.buildbundle[index].active;
    this.otherValid = this.buildbundle[index].active;
  }





  buybuild() {
    // alert("buybuild");
    if (this.arrow == false) {
      this.arrow = true;
    }
    else {
      this.arrow = false;
    }
    //api call
   this.apiCall('Carte');
    this.arrow1 = false;
  }

  topbuild() {
    // alert("hiiiiiiiiiiiiii");
    if (this.arrow1 == false) {
      this.arrow1 = true;
    }
    else {
      this.arrow1 = false;
    }
    this.arrow = false;
    //api call
    this.apiCall('CarteTopUp');
  }
  buyBundles(eve, bundleData) {
    var Action = bundleData.action;
    Action = Action.substring(Number(Action.indexOf(":")) + 1);
    var Price = bundleData.price;
    this.msisdnval = this._storeVal.getMSISDNData();
    var email = this._storeVal.getEmailData();
    var eCheckId = ""; var bankelibibality = false;
    let autorenewal: any;
    let resObj: any;
  }

  numberOnly(event): boolean {
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length == 11 && (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length == 11) {
      return false;;
    } else {
      return true;
    }
  }


  onLoadData(_selectedObj, subCat) {
    // alert("onloaddata");
    this._selectedObj = _selectedObj;
    var index = 0;
    for (; index < bundleMasterConfig.prodInfo[8].bundleTabs.length; index++) {
      if (_selectedObj === bundleMasterConfig.prodInfo[8].bundleTabs[0]) {
        break;
      }
      else {
        continue;
      }
    }
    this.bundle_Category = bundleMasterConfig.prodInfo[8].bundleCategory;
    this.bundle_SubCategory = bundleMasterConfig.prodInfo[8].bundleSubCategory[0];
    console.log("this.bundle_Category"+this.bundle_Category);
  
    this.buildbundle = [];
    this._buyBundleService.getBuyBundleCatCIS(this.msisdnval, 'Carte', subCat)
    .then((resp) => {
    // this._buyBundleService.getBuyBundleCatCIS(this.msisdnval, bundle_Category, bundle_SubCategory)
    //   .then((resp) => {
        if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
          if (resp.ResponseData) {
            this.isDataLoaded = true;
            resp.ResponseData.ProductDetails.ProductDetails.sort(function (a, b) {
              return a.Price - b.Price;
            });
            resp.ResponseData.ProductDetails.ProductDetails.forEach((ele) => {
              let dataObj;
              //if (_selectedObj === 'Weekly') {// &&  resp.ResponseData.ProductDetails.ProductDetails[0].Validity === "7 days"){
                dataObj = {
                  pname: ele.ProductName,
                  description: ele.Description,
                  price: ele.Price,
                  vailidity: ele.Validity,
                  renewal: ele.Renewal,
                  isConsentRequired: ele.isConsentRequired,
                  BuyForOthers: ele.BuyForOthers,
                  action: ele.Action,
                  active: false,
                  eligibiltyId: ele.eligibiltyId,
                  productId: ele.ProductID,
                  subcat: ele.Category + ele.SubCategory,
                   promotionApplicable:ele.promotionApplicable,
                  bundleCounter: 0
                };
                
              //}
              
            });
          }
          else {
            this.isDataLoaded = false;
            //this.buildbundle = [];
          }



        } else {
          //this.buildbundle = [];
        }
      })
      .catch((err) => {
        this.buildbundle = [];
      });
  }

  apiCall(subCat) {
    this.showLoader = true;
    this.buildbundle = [];

    this._buyBundleService.getBuyBundleCatCIS(this.msisdnval, 'Carte', subCat).then((resp) => {
      this.showLoader = false;
      this.buybundalval = resp.ResponseData.ProductDetails;
//       if(this.buybundalval == '' && this.buybundalval){
// var annu = "hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii";
//       }
      this._storeVal.setbuybundleinternalData(resp.ResponseData.ProductDetails);
      if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
        if (resp.ResponseData) {
          this.isDataLoaded = true;
          resp.ResponseData.ProductDetails.ProductDetails.sort(function (a, b) {
            return a.Price - b.Price;
          });
          resp.ResponseData.ProductDetails.ProductDetails.forEach((ele) => {
            let dataObj = {
              pname: ele.ProductName,
              description: ele.Description,
              price: ele.Price,
              vailidity: ele.Validity,
              renewal: ele.Renewal,
              isConsentRequired: ele.isConsentRequired,
              BuyForOthers: ele.BuyForOthers,
              action: ele.Action,
              active: false,
              eligibiltyId: ele.eligibiltyId,
              productId: ele.ProductID,
              subcat: ele.Category + ele.SubCategory,
               promotionApplicable:ele.promotionApplicable,
            };

            this.buildbundle.push(dataObj);

          });
        }

        else {
          this.isDataLoaded = false;
         // this.buildbundle = [];
        }
      } else {
        //this.Sponsoredwebpass = [];
      }
    }).catch((err) => {
      this.buildbundle = [];
    });
  }
}
