import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildbundleComponent } from './buildbundle.component';

describe('BuildbundleComponent', () => {
  let component: BuildbundleComponent;
  let fixture: ComponentFixture<BuildbundleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildbundleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildbundleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
