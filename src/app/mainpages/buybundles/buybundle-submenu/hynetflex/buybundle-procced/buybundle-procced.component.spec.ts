import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybundleProccedComponentHynet } from './buybundle-procced.component';

describe('BuybundleProccedComponentHynet', () => {
  let component: BuybundleProccedComponentHynet;
  let fixture: ComponentFixture<BuybundleProccedComponentHynet>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybundleProccedComponentHynet ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybundleProccedComponentHynet);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
