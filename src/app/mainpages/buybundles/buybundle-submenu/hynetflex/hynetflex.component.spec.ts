import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HynetflexComponent } from './hynetflex.component';

describe('HynetflexComponent', () => {
  let component: HynetflexComponent;
  let fixture: ComponentFixture<HynetflexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HynetflexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HynetflexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
