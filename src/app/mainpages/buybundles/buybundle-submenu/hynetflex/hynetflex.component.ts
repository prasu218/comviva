import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { bundleMasterConfig } from '../../../../../app/config/config';
import { format } from '../../../../utils/formatter';
import { StoreService } from '../../../../plugins/datastore';
import { MatDialog } from '@angular/material';
import { BuyBundlesService } from '../../../../plugins/buybundles';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { config } from '../../../../config';
import { ProfileService } from '../../../../plugins/profile';



@Component({
  selector: 'app-hynetflex',
  templateUrl: './hynetflex.component.html',
  styleUrls: ['./hynetflex.component.scss']
})
export class HynetflexComponent implements OnInit {
  public capped: boolean = false;
  public unlimited: boolean = false;
  selectedTabs = true;
  internalCardDetails = {};
  msisdnother = '';
  @Input() paymentuipagemigerate: boolean = true;
  public elementVal: any = [];
  public elementValUn: any = [];
  //public periodicityData :any= [];
  //public elementXtraValPerioicity = [];
  public elementGBSMEPeriodicity = [];
  public displayValidity;
  public bottoncolr: boolean = false;
  public _selectradi: boolean = false;
  public otherValid: Boolean = false;

  public active_nav_selected: string;
  public _selectedObj: any;
  public isDataLoaded: boolean = false;
  public confirm: boolean;
  //public activation_Channel:any;
  public msisdnval: any;
  public buybundalval: any;
  public buybundalvalprice: any;
  public bundle_Category: any;
  public bundle_SubCategory: any;
  public benefeciaryMSISDN: any;
  selectedBundle: any = -1;
  public selfform: FormGroup;
  public othersform: FormGroup;
  public errorMsg = '';
  public input: any;
  public selctradio: string;
  public ProductNames: string;
  public arrow: boolean = false;
  public arrow1: boolean = false;
  limitedCheck: any
  showLoader: boolean = false;



  constructor(private _buyBundleService: BuyBundlesService, private _storeVal: StoreService, private _profile: ProfileService, public dialog: MatDialog, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.selfform = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(10),Validators.maxLength(13)]],
    });
    this.othersform = this.fb.group({
      msisdn1: ['', [Validators.required, Validators.minLength(10),Validators.maxLength(13)]],
    });


    this.msisdnval = format.msisdn(this._storeVal.getMSISDNData());

    this.apiCall('SME New Bundles');

    bundleMasterConfig.prodInfo[6].bundleTabs.forEach(element => {
      this.elementVal.push(element);
    });
    bundleMasterConfig.prodInfo[8].bundleTabs.forEach(element => {
      this.elementValUn.push(element);
    });

    this.active_nav_selected = this.elementVal[6];
    this.bundle_Category = bundleMasterConfig.prodInfo[6].bundleCategory;
    this.bundle_SubCategory = bundleMasterConfig.prodInfo[6].bundleSubCategory[6];
    this.isDataLoaded = true;

  }


  buyprocce(_selectradi) {
    if (this.bottoncolr || this.otherValid) {
      this.bottoncolr = true;
      this.otherValid = false;
      this.internalCardDetails['msisdn'] = format.msisdn(this.msisdnval);
      this.internalCardDetails['selfOthers'] = this._selectradi;
      this.internalCardDetails['beneficiaryMsisdn'] = format.msisdn(this.msisdnother);
      //this.paymentuipagemigerate = false;
      this._storeVal.setBuyBundlePlanDetails(this.internalCardDetails);
      this.checkothers();
    }
  }
  checkothers() {
    this.showLoader = true;
    if (this.msisdnother != "") {
      this._profile.getProfileDetails(format.msisdn(this.msisdnother)).then((resp) => {
        if (resp.status_code == 0) {
          this.showLoader = false;
          this.paymentuipagemigerate = false;
          this.router.navigate(['buybundles/procced/' + this.internalCardDetails['productId']]);
        }
        else {
          this.showLoader = false;
          this.errorMsg = '';
          this.othersform.get('msisdn1').reset();
          this.elementGBSMEPeriodicity[this.selectedBundle].active = false;
        }
      }).catch((err) => {
        this.showLoader = false;
        this.errorMsg = 'Due to some server error your request was failed.Please try again';
      });
    }
    else {
      this.showLoader = false;
      this.paymentuipagemigerate = false;
      this.router.navigate(['buybundles/procced/' + this.internalCardDetails['productId']]);
    }
  }





  onLoadData(_selectedObj, subCat) {
    this.showLoader = true;
    var index = 0;
    for (; index < bundleMasterConfig.prodInfo[6].bundleTabs.length; index++) {

      if (_selectedObj === bundleMasterConfig.prodInfo[6].bundleTabs[index]) {
        break;
      }
      else {
        continue;
      }
    }



    this.bundle_Category = bundleMasterConfig.prodInfo[6].bundleCategory;
    this.bundle_SubCategory = bundleMasterConfig.prodInfo[6].bundleSubCategory[index];
    this.elementGBSMEPeriodicity = [];




    this._buyBundleService.getBuyBundleCatSme(this.msisdnval, 'SME Bundles', subCat)
      .then((resp) => {



        if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
          this.showLoader = false;

          if (resp.ResponseData) {
            this.showLoader = false;
            this.isDataLoaded = true;
            resp.ResponseData.ProductDetails.ProductDetails.sort(function (a, b) {
              return a.Price - b.Price;
            });
            resp.ResponseData.ProductDetails.ProductDetails.forEach((ele) => {

              let dataObj;

              if (_selectedObj === 'Monthly') {
                dataObj = {
                  pname: ele.ProductName,
                  description: ele.Description,
                  price: ele.Price,
                  vailidity: ele.Validity,
                  renewal: ele.Renewal,
                  isConsentRequired: ele.isConsentRequired,
                  BuyForOthers: ele.BuyForOthers,
                  action: ele.Action,
                  active: false,
                  eligibiltyId: ele.eligibiltyId,
                  productId: ele.ProductID,
                   subcat:ele.Category+ele.SubCategory,

                };
                this.limitedCheck = ((dataObj.description.indexOf("speed") >= 0))
                if ((dataObj.vailidity === "30 days") && (!this.limitedCheck)) {
                  this.elementGBSMEPeriodicity.push(dataObj);
                }
              }

              if (_selectedObj === '2 Month') {
                dataObj = {
                  pname: ele.ProductName,
                  description: ele.Description,
                  price: ele.Price,
                  vailidity: ele.Validity,
                  renewal: ele.Renewal,
                  isConsentRequired: ele.isConsentRequired,
                  BuyForOthers: ele.BuyForOthers,
                  action: ele.Action,
                  active: false,
                  eligibiltyId: ele.eligibiltyId,
                  productId: ele.ProductID,
                   subcat:ele.Category+ele.SubCategory,

                };
                this.limitedCheck = ((dataObj.description.indexOf("speed")>= 0))
                if ((dataObj.vailidity === "60 days") && (!this.limitedCheck)) {
                  this.elementGBSMEPeriodicity.push(dataObj);
                }

              }
              if (_selectedObj === '6 Month') {
                dataObj = {
                  pname: ele.ProductName,
                  description: ele.Description,
                  price: ele.Price,
                  vailidity: ele.Validity,
                  renewal: ele.Renewal,
                  isConsentRequired: ele.isConsentRequired,
                  BuyForOthers: ele.BuyForOthers,
                  action: ele.Action,
                  active: false,
                  eligibiltyId: ele.eligibiltyId,
                  productId: ele.ProductID,
                   subcat:ele.Category+ele.SubCategory,

                };
                this.limitedCheck = ((dataObj.description.indexOf("speed") >= 0))
                if ((dataObj.vailidity === "180 days") && (!this.limitedCheck)) {
                  this.elementGBSMEPeriodicity.push(dataObj);
                }
              }

              if (_selectedObj === '1 Year') {
                dataObj = {
                  pname: ele.ProductName,
                  description: ele.Description,
                  price: ele.Price,
                  vailidity: ele.Validity,
                  renewal: ele.Renewal,
                  isConsentRequired: ele.isConsentRequired,
                  BuyForOthers: ele.BuyForOthers,
                  action: ele.Action,
                  active: false,
                  eligibiltyId: ele.eligibiltyId,
                  productId: ele.ProductID,
                   subcat:ele.Category+ele.SubCategory,

                };
                this.limitedCheck = ((dataObj.description.indexOf("speed") >= 0))
                if ((dataObj.vailidity === "365 days") && (!this.limitedCheck)) {
                  this.elementGBSMEPeriodicity.push(dataObj);
                }
              }

            });
          }
          else {
            this.showLoader = false;
            this.isDataLoaded = false;
            this.elementGBSMEPeriodicity = [];
          }


        } else {
          this.showLoader = false;
          this.elementGBSMEPeriodicity = [];
        }
      })
      .catch((err) => {
        this.showLoader = false;
        this.elementGBSMEPeriodicity = [];
      });
  }

  //For UlimitedPlan

  onLoadDataUnlimited(_selectedObj, subCat) {
    this.showLoader = true;
    var index = 0;
    for (; index < bundleMasterConfig.prodInfo[8].bundleTabs.length; index++) {
      if (_selectedObj === bundleMasterConfig.prodInfo[8].bundleTabs[index]) {
        break;
      }
      else {
        continue;
      }
    }



    this.bundle_Category = bundleMasterConfig.prodInfo[8].bundleCategory;
    this.bundle_SubCategory = bundleMasterConfig.prodInfo[8].bundleSubCategory[index];
    this.elementGBSMEPeriodicity = [];


    this._buyBundleService.getBuyBundleCatSme(this.msisdnval, 'SME Bundles', subCat)
      .then((resp) => {



        if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
          this.showLoader = false;

          if (resp.ResponseData) {
            this.showLoader = false;
            this.isDataLoaded = true;
            resp.ResponseData.ProductDetails.ProductDetails.sort(function (a, b) {
              return a.Price - b.Price;
            });
            resp.ResponseData.ProductDetails.ProductDetails.forEach((ele) => {

              let dataObj;
              if (_selectedObj === 'Weekly') {
                dataObj = {
                  pname: ele.ProductName,
                  description: ele.Description,
                  price: ele.Price,
                  vailidity: ele.Validity,
                  renewal: ele.Renewal,
                  isConsentRequired: ele.isConsentRequired,
                  BuyForOthers: ele.BuyForOthers,
                  action: ele.Action,
                  active: false,
                  eligibiltyId: ele.eligibiltyId,
		  productId: ele.ProductID,
                  subcat:ele.Category+ele.SubCategory,
                };
                this.limitedCheck = ((dataObj.description.indexOf("speed") >= 0))
                if ((dataObj.vailidity === "7 days") && ((dataObj.description.indexOf("speed") >= 0))) {
                  this.elementGBSMEPeriodicity.push(dataObj);
                }
              }

              if (_selectedObj === 'Monthly') {
                dataObj = {
                  pname: ele.ProductName,
                  description: ele.Description,
                  price: ele.Price,
                  vailidity: ele.Validity,
                  renewal: ele.Renewal,
                  isConsentRequired: ele.isConsentRequired,
                  BuyForOthers: ele.BuyForOthers,
                  action: ele.Action,
                  active: false,
                  eligibiltyId: ele.eligibiltyId,
		  productId: ele.ProductID,
                   subcat:ele.Category+ele.SubCategory,
                };
                this.limitedCheck = ((dataObj.description.indexOf("speed") >= 0))
                if ((dataObj.vailidity === "30 days") && ((dataObj.description.indexOf("speed") >= 0))
                ) {
                  this.elementGBSMEPeriodicity.push(dataObj);
                }
              }
              //console.log("elementGBFacebookPeriodicitypnamedata ---->   " + JSON.stringify(this.elementGBSMEPeriodicity));
            });
          }
          else {
            this.showLoader = false;
            this.isDataLoaded = false;
            this.elementGBSMEPeriodicity = [];
            this.showLoader = false;
          }


        } else {
          this.showLoader = false;
          this.elementGBSMEPeriodicity = [];

        }
      })
      .catch((err) => {
        this.showLoader = false;
        this.elementGBSMEPeriodicity = [];
      });
  }






  Capped() {
    if (this.capped == false) {
      this.capped = true;
    }
    else {
      this.capped = false;
    }
    //api call
    this.apiCall('New SME Bundles');
    this.unlimited = false;
  }

  Unlimited() {

    if (this.unlimited == false) {
      this.unlimited = true;
    }
    else {
      this.unlimited = false;
    }
    this.apiCallUnlimited('New SME Bundles');
    this.capped = false;
  }



  apiCall(subCat) {
    this.showLoader = true;
    this.elementGBSMEPeriodicity = [];

    this._buyBundleService.getBuyBundleCatSme(this.msisdnval, 'SME Bundles', subCat).then((resp) => {
      this.buybundalval = resp.ResponseData.ProductDetails;


      this._storeVal.setbuybundleinternalData(resp.ResponseData.ProductDetails);
      if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
        this.showLoader = false;
        if (resp.ResponseData) {
          this.showLoader = false;
          this.isDataLoaded = true;
          resp.ResponseData.ProductDetails.ProductDetails.sort(function (a, b) {
            return a.Price - b.Price;
          });
          resp.ResponseData.ProductDetails.ProductDetails.forEach((ele) => {


            let dataObj = {
              pname: ele.ProductName,
              description: ele.Description,
              price: ele.Price,
              vailidity: ele.Validity,
              renewal: ele.Renewal,
              isConsentRequired: ele.isConsentRequired,
              BuyForOthers: ele.BuyForOthers,
              action: ele.Action,
              active: false,
              eligibiltyId: ele.eligibiltyId,
              productId: ele.ProductID,
               subcat:ele.Category+ele.SubCategory,
            };
            this.limitedCheck = ((dataObj.description.indexOf("speed") >= 0))

            if ((dataObj.vailidity === "30 days") && (!this.limitedCheck)) {

              this.elementGBSMEPeriodicity.push(dataObj);
            }

          });
        }

        else {
          this.showLoader = false;
          this.isDataLoaded = false;
          //this.message
        }


      } else {
        this.showLoader = false;
        this.elementGBSMEPeriodicity = [];
      }
    }).catch((err) => {
      this.showLoader = false;
      this.elementGBSMEPeriodicity = [];
    });

  }

  apiCallUnlimited(subCat) {
    this.showLoader = true;
    this.elementGBSMEPeriodicity = [];

    this._buyBundleService.getBuyBundleCatSme(this.msisdnval, 'SME Bundles', subCat).then((resp) => {
      this.buybundalval = resp.ResponseData.ProductDetails;


      this._storeVal.setbuybundleinternalData(resp.ResponseData.ProductDetails);
      if ((resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
        this.showLoader = false;
        if (resp.ResponseData) {
          this.isDataLoaded = true;
          resp.ResponseData.ProductDetails.ProductDetails.sort(function (a, b) {
            return a.Price - b.Price;
          });
          resp.ResponseData.ProductDetails.ProductDetails.forEach((ele) => {


            let dataObj = {
              pname: ele.ProductName,
              description: ele.Description,
              price: ele.Price,
              vailidity: ele.Validity,
              renewal: ele.Renewal,
              isConsentRequired: ele.isConsentRequired,
              BuyForOthers: ele.BuyForOthers,
              action: ele.Action,
              active: false,
              eligibiltyId: ele.eligibiltyId,
              productId: ele.ProductID,
               subcat:ele.Category+ele.SubCategory,
            };
            this.limitedCheck = ((dataObj.description.indexOf("speed") >= 0))
            if ((dataObj.vailidity === "7 days") && ((dataObj.description.indexOf("speed") >= 0))) {

              this.elementGBSMEPeriodicity.push(dataObj);

            }

          });
        }

        else {
          this.showLoader = false;
          this.isDataLoaded = false;
          this.elementGBSMEPeriodicity = [];
        }


      } else {
        this.showLoader = false;
        this.elementGBSMEPeriodicity = [];
      }
    }).catch((err) => {
      this.showLoader = false;
      this.elementGBSMEPeriodicity = [];
    });

  }




  buyBundles(eve, bundleData) {
    //console.log('item : ' + JSON.stringify(item));
    // var msisdn = '2348102947676';
    var Action = bundleData.action;
    Action = Action.substring(Number(Action.indexOf(":")) + 1);
    var Price = bundleData.price;
    this.msisdnval = format.msisdn(this._storeVal.getMSISDNData());
    //var fName =  this._storeVal.getFirstnameData();
    var email = this._storeVal.getEmailData();
    var eCheckId = ""; var bankelibibality = false;
    let autorenewal: any;
    let resObj: any;


  }

  numberOnly(event): boolean {
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length == 11 && (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length == 13) {
      return false;;
    } else {
      return true;
    }
  }

  onslectradio(data) {
    this._selectradi = data;
  }

  changeStatus(data, index) {
    this.internalCardDetails = data;
    this.buybundalval = this._storeVal.getbuybundleinternalData();
    if (this.selectedBundle > -1 && this.selectedBundle !== index) {
      this.elementGBSMEPeriodicity[this.selectedBundle].active = false;
      this.selectedTabs = false;


    }
    this.elementGBSMEPeriodicity[index].active = !this.elementGBSMEPeriodicity[index].active;
    this.selectedBundle = index;
    this.bottoncolr = this.elementGBSMEPeriodicity[index].active;
    this.otherValid = this.elementGBSMEPeriodicity[index].active;
  }

}
