import { Routes } from '@angular/router';
// import { LoginComponent } from '../../../login.component';

import { XtraplansComponent } from './xtraplans/xtraplans.component';
import { DataplansComponent } from './dataplans/dataplans.component';
import { BuybundlesComponent } from '../buybundles.component';
import { SmeBundlesComponent } from './smebundles/smebundles.component';
import { BuybundleProccedComponent } from './dataplans/buybundle-procced/buybundle-procced.component';
import { BuybundleProccedComponentXtraplans } from './xtraplans/buybundle-procced/buybundle-procced.component';
import { GoodybagComponent } from './goodybag/goodybag.component';
import { BuybundleProccedComponentGoodybag } from './goodybag/buybundle-procced/buybundle-procced.component';
import { BuybundleProccedComponentSME } from './smebundles/buybundle-procced/buybundle-procced.component';
import { BuybundleProccedComponentRoaming } from './roaming/buybundle-procced/buybundle-procced.component';
import { HynetflexComponent } from './hynetflex/hynetflex.component';
import { BuybundleProccedComponentHynet } from './hynetflex/buybundle-procced/buybundle-procced.component';
import { RoamingComponent } from './roaming/roaming.component';
import { IntcallComponent } from './intcall/intcall.component';
import { BuybundleProccedComponentIntcall } from './intcall/buybundle-procced/buybundle-procced.component';
import { AuthGuard } from 'src/app/auth/auth.guard';

export const Buybundlesmenuroutes: Routes = [{
  path: 'buybundles', component: BuybundlesComponent,
  children: [
    {
      path: '',
      component: DataplansComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'Data&VoiceBundles',
      component: XtraplansComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'DataBundles',
      component: DataplansComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'intcall',
      component: IntcallComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'roaming',
      component: RoamingComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'goodybag',
      component: GoodybagComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'smebundles',
      component: SmeBundlesComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'hynetflex',
      component: HynetflexComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
  ],
},
{
  path: 'buybundles/procced/:productId',
  component: BuybundleProccedComponent,
  pathMatch: 'full',
  canActivate: [AuthGuard]
},
{
  path: 'buybundles/Data&VoiceBundles/procced',
  component: BuybundleProccedComponentXtraplans,
  pathMatch: 'full',
  canActivate: [AuthGuard]
},
{
  path: 'buybundles/roaming/procced',
  component: BuybundleProccedComponentRoaming,
  pathMatch: 'full',
  canActivate: [AuthGuard]
},
{
  path: 'buybundles/goodybag/procced',
  component: BuybundleProccedComponentGoodybag,
  pathMatch: 'full',
  canActivate: [AuthGuard]
},
{
  path: 'buybundles/smebundles/procced',
  component: BuybundleProccedComponentSME,
  pathMatch: 'full',
  canActivate: [AuthGuard]
},
{
  path: 'buybundles/intcall/procced',
  component: BuybundleProccedComponentIntcall,
  pathMatch: 'full',
  canActivate: [AuthGuard]
},
{
  path: 'buybundles/hynetflex/procced',
  component: BuybundleProccedComponentHynet,
  pathMatch: 'full',
  canActivate: [AuthGuard]
}];

