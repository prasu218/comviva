// import { Component, OnInit } from '@angular/core';
// import { bundleMasterConfig } from '../../../../../app/config/config';
 import { StoreService } from '../../../../plugins/datastore';
 import { format } from '../../../../utils/formatter';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { MatDialog } from '@angular/material';
// import { BuyBundlesService } from '../../../../plugins/buybundles';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { ProfileService } from '../../../../plugins/profile';
import { config } from '../../../../config';
import { Component, OnInit, Input,ViewChild, AfterContentChecked, ChangeDetectorRef} from '@angular/core';
// import { MatDialog } from '@angular/material';
import { BuyBundlesService } from '../../../../plugins/buybundles';
import { bundleMasterConfig } from '../../../../../app/config/config';
import { Router } from '@angular/router';
import { Hotdeals_resp } from '../../../../../app/config/config';
import { AngularCDRService } from '../../../../plugins/angularCDR';
import { getRoamingRatesCountries_Response } from '../../../../../app/config/config';
import { NgxXml2jsonService } from 'ngx-xml2json';

@Component({
  selector: 'app-dataplans',
  templateUrl: './dataplans.component.html',
  styleUrls: ['./dataplans.component.scss']
})
export class DataplansComponent implements OnInit, AfterContentChecked {
  public Hotdealsplans = [];
  [x: string]: any;
  internalCardDetails ={};
  buybuttonHidden:boolean = true;
  gdscontentshow:boolean = false;
  public elementVal: any = [];
  selectedTabs = true;
  public periodicityData :any= [];
  public active_nav_selected: string;
  public _selectedObj: boolean= false;
  public _selectradi: boolean=false;
 public radiotherhide: boolean=true;
  public isDataLoaded : boolean = false;
  public confirm: boolean;
  public activation_Channel:any;
  showLoader:boolean=false;
  msisdnval:any;
  msisdnother='';
  public bundle_Category:any;
  public bundle_SubCategory:any;
  public benefeciaryMSISDN : any;
  public selfform: FormGroup;
  public othersform: FormGroup;
  public errorMsg = '';
  public selctradio:boolean = false;
public buybundalval: any;
public bottoncolr : boolean = false;
public otherValid : Boolean= false;
  selectedOption: any = "Hot Deals";
  startTime: any;
category: any = 'buybundles';
value="";



  selectedBundle: any = -1;
     @Input() paymentuipagemigerate : boolean = true;

  constructor(  private fb: FormBuilder, private ref: ChangeDetectorRef,
    private readonly router: Router,
    private _buyBundleService: BuyBundlesService, private _storeVal: StoreService,private _router: Router,  private _profile: ProfileService, public dialog: MatDialog,private _cdr: AngularCDRService) { };
  ngOnInit() {
    this._selectedObj = this.selectedOption;
    // this.selectedOption = Hotdeals_resp['ProductDetails'];

  
    this.selfform = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(config.MIN_MSISDN_LENGTH), Validators.maxLength(config.MAX_MSISDN_LENGTH)]],
    });
    this.othersform = this.fb.group({
      msisdnother: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(13)]],
    });
    this.buybuttonHidden = false;
    this.msisdnval = this._storeVal.getMSISDNData();


    bundleMasterConfig.prodInfo[0].bundleTabs.forEach(element => {
      this.elementVal.push(element);
    });

    this.active_nav_selected = this.elementVal[0];
    this.bundle_Category = bundleMasterConfig.prodInfo[0].bundleCategory;
    this.bundle_SubCategory = bundleMasterConfig.prodInfo[0].bundleSubCategory[1];


    this.isDataLoaded = true;


    if (this._selectedObj == this.selectedOption) {
      this.hotdeals();
    }



    //    this._buyBundleService.getBuyBundleCatCIS(this.msisdnval, this.bundle_Category, this.bundle_SubCategory).then((resp) => {

    //      if( (resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
    //        console.log("respprasa " +JSON.stringify(resp)); 
    //        if(resp.ResponseData)
    //        {
    //          this.isDataLoaded = true;

    //          resp.ResponseData.ProductDetails.ProductDetails.sort(function(a, b) {
    //            return a.Price - b.Price;
    //        });

    //          resp.ResponseData.ProductDetails['ProductDetails'].forEach((ele) => {
    //            // console.log("resp.ProductDetails.ProductDetails      insidew====");
    //              let dataObj = {
    //              pname:ele.ProductName,
    //              description: ele.Description,
    //              price: ele.Price,
    //              vailidity:ele.Validity,
    //              renewal:ele.Renewal,
    //              isConsentRequired:ele.isConsentRequired,
    //              BuyForOthers:ele.BuyForOthers,
    //              action:ele.Action,
    //              active: false,
    //              eligibiltyId:ele.eligibiltyId,
    //            };

    //      this.periodicityData.push(dataObj);
    //      console.log(this.periodicityData);

    //    });
    //        }
    //        else{
    //          this.isDataLoaded = false;
    //          this.periodicityData=[];
    //      }

    //  // console.log("getBuyBundlePeriodicityData      periodicityData===="+this.periodicityData.toString());

    //      } else {
    //        this.periodicityData=[];

    //      }
    //    })
    //      .catch((err) => {

    //  this.periodicityData=[];
    //      });



  }

  hotdeals() {

    Hotdeals_resp['ProductDetails'].forEach((ele) => {
      // console.log("resp.ProductDetails.ProductDetails      insidew====");
      let dataObj = {
        pname: ele.ProductName,
        description: ele.Description,
        price: ele.Price,
        vailidity: ele.Validity,
        renewal: ele.Renewal,
        isConsentRequired: ele.isConsentRequired,
        BuyForOthers: ele.BuyForOthers,
        action: ele.Action,
        active: false,
        eligibiltyId: ele.eligibiltyId,
        productId: ele.ProductID,
        subcat:ele.Category+ele.SubCategory,
      };

      this.periodicityData.push(dataObj);

    });
    this.showLoader=false;
  }

   ngAfterViewInit() {
    this.ref.detectChanges();
    
}


numberOnlyself(event): boolean {
    let test = event.target.value;
      let test_length = test.length;
        const charCode = (event.which) ? event.which : event.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
        }
		if(test_length == 13 && ( charCode == 8  || charCode == 13)){
			return true;
        }else if(test_length == 17){
			return false;;
		}else{
			return true;
		}
       }


ngAfterContentChecked(){}

changeStatus(data,index){
    this.internalCardDetails =data;
    // this.buybundalval =  this._storeVal.getbuybundleinternalData(); 
    if(this.selectedBundle > -1 && this.selectedBundle !== index){
      this.periodicityData[this.selectedBundle].active = false;
      this.selectedTabs = false;
    }
   if(data.productId=="92"){
    this.radiotherhide = false;
    }else{
      this.radiotherhide = true;
    }
    this.periodicityData[index].active = !this.periodicityData[index].active;
    this.selectedBundle = index;
this.bottoncolr = this.periodicityData[index].active;
this.otherValid = this.periodicityData[index].active;
  }




    onslectradio(data){
     this._selectradi = data;
      
    
    }

   onLoadData(_selectedObj) {
   this.showLoader=true;
     this._selectedObj=_selectedObj;
     let index = 0;
     for (index; index < bundleMasterConfig.prodInfo[0].bundleTabs.length; index++) {
     
        if(_selectedObj === bundleMasterConfig.prodInfo[0].bundleTabs[index])
        {
          
          // this.bundle_Category=bundleMasterConfig.prodInfo[0].bundleCategory;
          // this.bundle_SubCategory = bundleMasterConfig.prodInfo[0].bundleSubCategory[0];
          break;
        }
        else
        {
          continue;
        }
    }

    let bundle_Category=bundleMasterConfig.prodInfo[0].bundleCategory;
    let bundle_SubCategory = bundleMasterConfig.prodInfo[0].bundleSubCategory[index];
    

    this.periodicityData= [];
if (this._selectedObj == this.selectedOption) {
      this.hotdeals();
      // console.log("insidehotdealif");
    } else {
    this._buyBundleService.getBuyBundleCatCIS(this.msisdnval, bundle_Category,bundle_SubCategory)
            .then((resp) => {
				  this._storeVal.setbuybundleinternalData (resp.ResponseData.ProductDetails);
              
              if( (resp.ResoponseCode === "0" || resp.ResoponseCode === 0) && resp.ResponseData && resp.ResponseData.ProductDetails.ProductDetails.length > 0) {
				this._cdr.writeCDR(this.startTime, this.category,this.value, 'buybundles dataplans', 'Success');
                if(resp.ResponseData)
                {
                  this.isDataLoaded = true;
                  resp.ResponseData.ProductDetails.ProductDetails.sort(function(a, b) {
                    return a.Price - b.Price;
                });
                  resp.ResponseData.ProductDetails.ProductDetails.forEach((ele) => {
                      let dataObj = {
                      pname:ele.ProductName,
                      description: ele.Description,
                      price: ele.Price,
                      vailidity:ele.Validity,
                      renewal:ele.Renewal,
                      isConsentRequired:ele.isConsentRequired,
                      BuyForOthers:ele.BuyForOthers,
                      action:ele.Action,
                      active: false,
                      eligibiltyId:ele.eligibiltyId,
                      productId : ele.ProductID,
                      subcat:ele.Category+ele.SubCategory,
                    };
              this.periodicityData.push(dataObj);
            });
            this.showLoader=false;
                }
                else{
                  this.isDataLoaded = false;
                  this.periodicityData=[];
              }
                
         
              } else {
                this.periodicityData=[];
         
              }
            })
            .catch((err) => {
    
        this.periodicityData=[];
		this._cdr.writeCDR(this.startTime, this.category,this.value, 'buybundles ', 'Failuer');
            });
}

   }

  


  buyprocce(_selectradi){

    if (this.bottoncolr || this.otherValid){
      this.bottoncolr = true;
      this.otherValid = false;
      this.internalCardDetails['msisdn'] = this.msisdnval;
    this.internalCardDetails['selfOthers'] = this._selectradi;
    this.internalCardDetails['beneficiaryMsisdn'] = format.msisdn(this.msisdnother);
	    this._storeVal.setBuyBundlePlanDetails(this.internalCardDetails);
      this.checkothers();
    
    }
     }
      checkothers()
     {
      this.showLoader = true;
      if( this.msisdnother!=""){
        this._profile.getProfileDetails(format.msisdn(this.msisdnother)).then((resp) => {
                      if (resp.status_code == 0) {
                      
                             this.showLoader = false;
                             this.paymentuipagemigerate = false;
                           this.router.navigate(['buybundles/procced/'+this.internalCardDetails['productId']]);
                           
                      }
                      else{
                   this.showLoader = false;
                         this.errorMsg = '';
                          this.othersform.get('msisdnother').reset();
                          this.periodicityData[this.selectedBundle].active = false;
                      } 
                    }).catch((err) => {
                      this.showLoader = false;
                      this.errorMsg = 'Due to some server error your request was failed.Please try again';
                      // this.debit_card1.reset();
                    });
           }
	else{
		this.showLoader = false;
                this.paymentuipagemigerate = false;
                this.router.navigate(['buybundles/procced/'+this.internalCardDetails['productId']]);
	}  
     }
     
 
}
    
            
