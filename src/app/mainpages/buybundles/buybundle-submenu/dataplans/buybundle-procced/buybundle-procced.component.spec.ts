import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybundleProccedComponent } from './buybundle-procced.component';

describe('BuybundleProccedComponent', () => {
  let component: BuybundleProccedComponent;
  let fixture: ComponentFixture<BuybundleProccedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybundleProccedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybundleProccedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
