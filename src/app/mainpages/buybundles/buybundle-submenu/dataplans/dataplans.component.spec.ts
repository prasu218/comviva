import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataplansComponent } from './dataplans.component';

describe('DataplansComponent', () => {
  let component: DataplansComponent;
  let fixture: ComponentFixture<DataplansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataplansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataplansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
