import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
//  import { format } from '../../utils/formatter';
import { Router } from '@angular/router';
import { format } from '../../utils/formatter';
import { config } from '../../config';
import { StoreService } from 'src/app/plugins/datastore';
import { Location } from '@angular/common';





@Component({
  selector: 'app-buybundles',
  templateUrl: './buybundles.component.html',
  styleUrls: ['./buybundles.component.scss']
})
export class BuybundlesComponent implements OnInit {
bgColorpp = 'grey' ;
  public existing_cust: FormGroup;
  header=true;
  msisdn: any;
  firstName: any;

  constructor(private fb: FormBuilder,private router: Router,private _storeVal: StoreService,private location: Location) { }



  ngOnInit() {
if (this._storeVal.getMSISDNData() && this._storeVal.getMSISDNData()!= "") {  
      this.msisdn = this._storeVal.getMSISDNData();
      this.firstName =  this._storeVal.getFirstNameData();
    }
    this.existing_cust = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(config.MIN_MSISDN_LENGTH), Validators.maxLength(config.MAX_MSISDN_LENGTH)]],
    });
  }



  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }
 
    
  // buyprocce(){
  // alert("hi");
  //   this.paymentuipagemigerate = false;
  
  //  }
  

 
  onSubmit(value: FormGroup) {
    // const msisdn = this.existing_cust.get('msisdn').value;
    let msisdn = format.msisdn(this.existing_cust.get('msisdn').value);
    // this._profile.getProfileDetails(msisdn).then((resp) => {
    //   if (resp.status_code == 0) {

    //     // this._storeVal.setEmailData(resp.customer_Profile.email);
    //     // this._storeVal.setAlternateNumberData(resp.customer_Profile.alternateNumber);
    //     // console.log('this._storeVal.setEmailData-' + resp.customer_Profile.email);
    //     // console.log('resp---' + JSON.stringify(resp));
    //     this.email = resp.customer_Profile.email;
    //     this.primaryNumber = resp.customer_Profile.primaryNumber;
    //     this.alternateNumber = resp.customer_Profile.alternateNumber;
    //     this.OTPShow = false;
        
    //     //  this.router.navigate(['dashboard'] ).then( nav => {}, (err) => {

    //     //       this.errorMsg = 'Please try again after sometime';
    //     //       this.existing_cust.reset();
    //     //     });
    //   }

    //   // else if (resp.status_code == 1) {
    //   //       this.errorMsg = 'please enter correct MSISDN number';
    //   //        this.existing_cust.reset();
    //   //     } 
    // }).catch((err) => {
    //   this.errorMsg = 'Please enter valid catch MSISDN number';
      
    //   this.existing_cust.reset();
    // });

  }
}



