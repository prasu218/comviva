import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { StoreService } from '../../plugins/datastore'

@Component({
  selector: 'app-terms-condition',
  templateUrl: './terms-condition.component.html',
  styleUrls: ['./terms-condition.component.scss']
})
export class TermscondComponent implements OnInit {
  header :boolean;
  public msisdn: string;
  constructor(private router: Router,private location: Location,private _storeVal: StoreService) { }
  ngOnInit() {
    let msisdn = this._storeVal.getMSISDNData();

    if (msisdn != null) {  
      this.header=true;
    }else{
      this.header=false;

    }
  }

  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }

}

