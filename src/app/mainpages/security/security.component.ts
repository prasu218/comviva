import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.scss']
})
export class SecurityComponent implements OnInit {
  header = true;
  constructor(private router: Router,private location: Location) { }
  backout(){
    this.router.navigate(['dashboard'])

  }
  ngOnInit() {
  }
  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }
}
