import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { ComplaintsService } from '../../../../plugins/complaints';
import { StoreService } from '../../../../plugins/datastore';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AngularCDRService } from '../../../../plugins/angularCDR';
import { ProceedcomplaintService } from '../../../../plugins/proceedcomplaint/proceedcomplaint.service'; 
import { Router } from '@angular/router';
import 'rxjs/Rx';

@Component({
  selector: 'app-log-complaints',
  templateUrl: './log-complaints.component.html',
  styleUrls: ['./log-complaints.component.scss']
})
export class LogComplaintsComponent implements OnInit {
  showLoader : boolean = false;
  public selectedState; 
  public message :string;
  issueTypes: any = [];
  isValid: boolean = false;
  issueTypeId;
  subCatId;
  priority;
  description;
  subCategoryList: any = [];
  msisdn;
  loading;
  errMsg;
  errorMsg1;
  complaintForm: FormGroup;
  public complaintsForm: boolean= true ;
  public successMsg: boolean=false ;
  public failureMsg:boolean=false;
  public statusMessage:string;
  public docketnumber:number;
  startTime: any;
  category: any = 'Complaints';
    // Following event will be called when user select item in first dropwdown.
  


   constructor(private _complaintsService: ComplaintsService,
    private _storeData: StoreService,private _listComplaintsService: ComplaintsService,private _submitComplaintsService: ComplaintsService,
    private formBuilder: FormBuilder,private _cdr: AngularCDRService,private sharingService:ProceedcomplaintService,private router:Router) {  }


  ngOnInit() {
  this.startTime = this._cdr.getTransactionTime(0);
  this.errorMsg1 = false;
    this.showLoader = true;
    this.createComplaintForm();
    this.msisdn = this._storeData.getMSISDNData();
    this._complaintsService.getcategoryDetails(this.msisdn)
      .then(resp => {
        if (resp.status == 0) {
          this.showLoader = false;
          this.issueTypes = resp.prodInfo;
        } else {
          this.showLoader = false;
          this.errorMsg1 = true;
        }
      })
      .catch(err => {
        this.showLoader = false;
        this.errorMsg1 = true;
      });
  }

  createComplaintForm() {
    this.complaintForm = this.formBuilder.group({
      'issueType': new FormControl('', Validators.required),
      'subCat': new FormControl('', Validators.required),
      'priority': new FormControl('', Validators.required),
      'desc':new FormControl('', Validators.required)
    });
  }

  getSubCatAsPerIssueType(issueType) {
    this.showLoader = true;
    const issueTypeVal = issueType.target.value
      this._complaintsService.getsubcategoryDetails(this.msisdn, issueTypeVal)
      .then(resp => {
        if (resp.status == 0) {
          this.showLoader = false;
          this.subCategoryList = resp.prodInfo;
        } else {
          this.showLoader = false;
        }
      })

      .catch(err => {
        this.showLoader = false;
      });
  }

ExecuteMyFunction():void{
  this.loading = true;
  this.errMsg= false;
    const msisdn = this._storeData.getMSISDNData(); 
    this._listComplaintsService.listComplaintsDetails(msisdn).then((resp) => {
      if (resp.StatusCode == 0) {
        this.message = resp.ticket;   
        this.loading = false;                          
      }else{
        this.isValid = true;
        this.loading = false;     
      }
    }).catch((err) => {    
      this.loading = false;
      this.errMsg= true;

    });
}

submitComplaint() {

  this.sharingService.setData(this.msisdn,this.subCatId,this.priority,this.description);
  this.router.navigate(['logComplaints/proceed']);

//  this.showLoader = true;
//  this._submitComplaintsService.complaintSubmitFormDetails(this.msisdn,this.subCatId,this.priority,this.description).then((resp)=>{
//       if (resp.status_code == 0) {
//               this.issueTypeId='undefined';
//               this.subCatId='undefined';
//               this.priority='undefined';
//               this.description='';      
//               this.complaintsForm=false;
//               this.successMsg=true;
//               this.failureMsg=false;
//               this.showLoader = false;
//               this.loading=false;
//               this.errMsg=false;
//               this.errorMsg1=false;
//               this.docketnumber = resp.addtional_details[0].DocketNumber;
//               this.statusMessage="'Y'ello, your ticket has been successfully logged. please note down the ticket reference number "+this.docketnumber;
//               this._cdr.writeCDR(this.startTime, 'Log Complaint',this.docketnumber, 'Log Complaint', 'Success');
//           }else{ 
//             this.issueTypeId='undefined';
//               this.subCatId='undefined';
//               this.priority='undefined';
//               this.description='';      
//               this.complaintsForm=false;
//               this.successMsg=false;
//               this.failureMsg=true;
//               this.showLoader = false;    
//               this.loading=false;
//               this.errMsg=false;
//               this.errorMsg1=false;
//               this._cdr.writeCDR(this.startTime, 'Log Complaint',"0000000", 'Log Complaint', 'Failure');
//               this.statusMessage="'Y'ello, currently we are unable to process your request, please try again later. thank you";       
//           }
//   }).catch((err) => {
//          this.issueTypeId='undefined';
//               this.subCatId='undefined';
//               this.priority='undefined';
//               this.description='';      
//               this.showLoader = false;
//               this.loading=false;
//               this.errMsg=true;
//               this.errorMsg1=false;
//               this._cdr.writeCDR(this.startTime, 'Log Complaint',"0000000", 'Log Complaint', 'Failure');
//         });    
}


loadForm(){
 this.ExecuteMyFunction();
   this.complaintsForm=true;
   this.successMsg=false;
   this.failureMsg=false;
}

ngAfterViewInit(){
  this.ExecuteMyFunction();
}

}
