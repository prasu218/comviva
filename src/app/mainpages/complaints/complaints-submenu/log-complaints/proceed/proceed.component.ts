import { Component, OnInit } from '@angular/core';
import { ProceedcomplaintService } from '../../../../../plugins/proceedcomplaint/proceedcomplaint.service'; 
import { Router } from '@angular/router';
import { ComplaintsService } from '../../../../../plugins/complaints';
import { Location } from '@angular/common';


@Component({
  selector: 'app-proceed',
  templateUrl: './proceed.component.html',
  styleUrls: ['./proceed.component.scss']
})
export class ProceedComponent implements OnInit {
  msisdn:any;
  subCatId:any;
  priority:any;
  description:any;
  public header=true;
public message:any="";
public suceesDiv:boolean=false;
public showLoader:boolean=false;
public failurDiv:boolean=false;
public showsuccessdiv:boolean=true;
public message1:any="";
public docketnumber:number;
public referrer:any;
public referrer1:any;

    constructor(private sharingService:ProceedcomplaintService,private router:Router,private _submitComplaintsService: ComplaintsService,private location: Location){}

    ngOnInit(){
      this.msisdn = this.sharingService.getMsisdn();
      this.subCatId = this.sharingService.getSubCatId();
      this.priority = this.sharingService.getPriority();
      this.description = this.sharingService.getDescription().replace(/\n/g, "-").replace(/ /g, "-");
    }





  backbuttons(){

    this.location.back();
   }

 backbutton(){
     this.router.navigate(['complaints/searchComplaints']);
   }
   submitTicket() {
    if(this.msisdn != undefined || this.subCatId != undefined || this.priority != undefined || this.description != undefined  ){

    this.showLoader=true;
    this._submitComplaintsService.complaintSubmitFormDetails(this.msisdn,this.subCatId,this.priority,this.description).then((resp)=>{
            if (resp.status_code == 0) {
              this.message1="Success" 
              this.docketnumber = resp.addtional_details[0].DocketNumber;  
              this.showsuccessdiv=false;
              this.suceesDiv=true;
              this.showLoader=false;
              this.message=this.docketnumber;     
                }else{ 
                  this.message1="Failure"    
                  this.failurDiv=true;
                  this.showsuccessdiv=false; 
                  this.showLoader=false;
                  
                }

              }).catch((err) => {
                this.message1="Failure"    
                  this.message ="Unable to ";
                  this.failurDiv=true;
                  this.showsuccessdiv=false; 
                  this.showLoader=false;
              });   


}else{
  this.backbuttons();
}

}
}
