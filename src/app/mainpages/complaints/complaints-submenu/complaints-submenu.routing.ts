import { Routes } from '@angular/router';
import { ComplaintsComponent } from '../../complaints/complaints.component';
import { LogComplaintsComponent } from './log-complaints/log-complaints.component';
import { SearchComplaintsComponent } from './search-complaints/search-complaints.component';
import {ProceedComponent} from './log-complaints/proceed/proceed.component';
import { AuthGuard } from 'src/app/auth/auth.guard';

export const Complaintsmenuroutes: Routes = [{
  path: 'complaints', component: ComplaintsComponent,
  children: [
    {
      path: '',
      component: LogComplaintsComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'logComplaints',
      component: LogComplaintsComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'searchComplaints',
      component: SearchComplaintsComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'logComplaints/proceed',
      component: ProceedComponent,
      pathMatch: 'full',
    //   canActivate: [AuthGuard]
    }

  ]
}];
