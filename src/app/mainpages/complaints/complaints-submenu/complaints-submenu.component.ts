import { Component, OnInit,Input, SimpleChanges  } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-complaints-submenu',
  templateUrl: './complaints-submenu.component.html',
  styleUrls: ['./complaints-submenu.component.scss']
})
export class ComplaintsSubmenuComponent implements OnInit {
	@Input() active_nav: string;

  complaintNavElements = [
    {id:'logComplaints',value:'Log a Complaint', source :'assets/images/log_complaints.svg',},
    {id:'searchComplaints',value:'Search', source :'assets/images/search.svg',},
  ];

  constructor(private _router: Router) { }

  ngOnInit() {
  }

    ngAfterContentChecked(){
    this.active_nav = this._router.url.split('/')[2];
    if(this._router.url === "/complaints"){
      this.active_nav  = "logComplaints";
    }
  }
  
 onLoadMdNav(selectedData){
    this._router.navigate(['complaints',selectedData.id]);
     this.active_nav = selectedData.value;
  }

}
