import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComplaintsComponent } from './search-complaints.component';

describe('SearchComplaintsComponent', () => {
  let component: SearchComplaintsComponent;
  let fixture: ComponentFixture<SearchComplaintsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchComplaintsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComplaintsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
