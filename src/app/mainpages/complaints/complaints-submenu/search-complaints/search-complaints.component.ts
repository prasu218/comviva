import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { config } from '../../../../config';
import { format } from '../../../../utils/formatter';
import { StoreService } from '../../../../plugins/datastore';
import { ComplaintsService } from '../../../../plugins/complaints';
import { AngularCDRService } from '../../../../plugins/angularCDR';

import 'rxjs/Rx';
declare var $: any;

@Component({
  selector: 'app-search-complaints',
  templateUrl: './search-complaints.component.html',
  styleUrls: ['./search-complaints.component.scss']
})
export class SearchComplaintsComponent implements OnInit {
  public existing_cust: FormGroup;
  public message :string;
  showLoader : boolean = false;
  public errorMsg = '';
  public items: any;
  public min: any;
  footerWhite:boolean=true;
  footerGray:boolean= false;
  errMsg:boolean=false;
  isValid: boolean = false;
  response: boolean = false;
  startTime: any;
  category: any = 'Complaints';
  loading;
  constructor(private _listComplaintsService: ComplaintsService,private fb: FormBuilder,private _storeData: StoreService,private _LoginRcgService: ComplaintsService,private _cdr: AngularCDRService) { }

  ngOnInit() {
    this.startTime = this._cdr.getTransactionTime(0);
    this.showLoader = true;
    this.existing_cust = this.fb.group({
      ticket: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(9)]],
     
   });
  }


  onSubmit(value: FormGroup) { 
   
    this.showLoader = true;
    const msisdn = this._storeData.getMSISDNData();
    let ticket = (this.existing_cust.get('ticket').value);
    
    this._LoginRcgService.getLoginRcgDetails(msisdn,ticket).then((resp) => {
      if (resp.StatusCode == 0) {
        this.showLoader = false;
        this.response = true;
        this.isValid = false;
        this.errMsg= false;
        this.message = resp.ticket;  
        
        $('#wht-crv').css("background-color","#f2f2f2");
        this.footerWhite=false;
        this.footerGray= true;
        this._cdr.writeCDR(this.startTime, 'Search Complaint',ticket, 'Search Complaint', 'Success');
      }else{
        this.showLoader = false;
        this.existing_cust.reset();
        //this.message =  resp.status_message;

        $('#wht-crv').css("background-color","#f2f2f2");
        this.footerWhite=false;
        this.footerGray= true;
        this.isValid = true;
        this.response = false;
        this.errMsg=false;
        this._cdr.writeCDR(this.startTime, 'Search Complaint',ticket, 'Search Complaint', 'Failure');
      }

    }).catch((err) => {    
      this.showLoader = false;
      this.errorMsg = 'Please Enter a valid ticket Number';
      this.existing_cust.reset();
      this._cdr.writeCDR(this.startTime, 'Search Complaint',ticket, 'Search Complaint', 'Failure');
      $('#wht-crv').css("background-color","#f2f2f2");
        this.footerWhite=false;
        this.footerGray= true;
        this.errMsg=true;
        this.isValid = false;
        this.response = false;
    });

  }

  ExecuteMyFunction():void{
    this.loading = true;
    this.errMsg= false;
      const msisdn = this._storeData.getMSISDNData(); 
      this._listComplaintsService.listComplaintsDetails(msisdn).then((resp) => {
        if (resp.StatusCode == 0) {
          this.response = true;
          this.message = resp.ticket;   
          this.loading = false;     
          this.showLoader = false;                     
        }else{
          this.isValid = true;
          this.loading = false;    
          this.showLoader = false; 
        }
      }).catch((err) => {    
        this.loading = false;
        this.errMsg= true;
        this.showLoader = false;
  
      });
  }

  // loadForm(){
  //   this.ExecuteMyFunction();
  //     this.complaintsForm=true;
  //     this.successMsg=false;
  //     this.failureMsg=false;
  //  }
   
   ngAfterViewInit(){
     this.ExecuteMyFunction();
   }

}
