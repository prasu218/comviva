import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplaintsSubmenuComponent } from './complaints-submenu.component';

describe('ComplaintsSubmenuComponent', () => {
  let component: ComplaintsSubmenuComponent;
  let fixture: ComponentFixture<ComplaintsSubmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplaintsSubmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplaintsSubmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
