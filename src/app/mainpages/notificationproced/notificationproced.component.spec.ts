import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationprocedComponent } from './notificationproced.component';

describe('NotificationprocedComponent', () => {
  let component: NotificationprocedComponent;
  let fixture: ComponentFixture<NotificationprocedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationprocedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationprocedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
