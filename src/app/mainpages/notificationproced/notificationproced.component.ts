import { Component, OnInit } from '@angular/core';
import { NotificationdbService } from '../../plugins/notificationdb';
import { format } from '../../utils/formatter';
import { StoreService } from 'src/app/plugins/datastore';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-notificationproced',
  templateUrl: './notificationproced.component.html',
  styleUrls: ['./notificationproced.component.scss']
})
export class NotificationprocedComponent implements OnInit {
  airttt: boolean = true;
  airttt1: boolean = false;
  showsuccessdiv: Boolean = true;
  failurDiv: Boolean = false;
  suceesDiv: Boolean = false;
  showconfimdiv: Boolean = false;
  showsuccessdiv1:Boolean=true;
  showLoader: Boolean = false;
  header=false;
  message = '';
   message1 = '';
  msisdnval:any = '';
  email:any = '';
  msisdn:any="";
  self:boolean;
  constructor(  private _storeVal: StoreService,private notifiservice:NotificationdbService,private _storeData: StoreService, private router:Router,private location: Location ) { }

  ngOnInit() {
   
    this.msisdnval = this._storeVal.getMSISDNupData();
    this.email =  this._storeVal.getEmailupData();
    this.msisdn=this._storeVal.getMSISDNData();
    //if(this.msisdnval.length==9||this.msisdnval.length==10)
   // {
   //   this.msisdnval=this.msisdnval
   // }else{
   //   this.msisdnval=this.msisdnval.slice(3);
   // }

  }

  confirm(){
       
    this.showLoader=true;
    this.notifiservice.notificationAPI(format.msisdn(this.msisdn),format.msisdn(this.msisdnval),this.email).then((resp) => {
      if(resp.status==200)
      {this.showLoader=false;
        
        this._storeVal.setAlternateNumberData(this.msisdnval);
        
         this._storeVal.setEmailData(this.email);
         this.showsuccessdiv1=false;
        this.showsuccessdiv=false;
        this.showconfimdiv=true;
        this.suceesDiv=true;
        this.message1="Success"
        this. message = 'All details successfully updated in your profile.  Please visit profile page for more corrections.';
        this.notifiservice.updateNotificationData(format.msisdn( this.msisdn),"true").then((resp) => {
        })
      }else{
        this.showsuccessdiv1=true;
        this.showsuccessdiv=false;
        this.showLoader=false;
        this.failurDiv=true;
        this.message1="Failure"
        this.message = 'Your request has been failed due to server issue.Please try agin.';
      }
    
    }).catch((err)=>{
      this.showsuccessdiv1=true;
      this.showsuccessdiv=false;
        this.showLoader=false;
        this.failurDiv=true;
        this.message1="Failure"
        this.message = 'Your request has been failed due to server issue.Please try agin.';
    })
    
  }

  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }

   
  Dashboard(){
    this.router.navigate(['dashboard']);
  }

 


}




