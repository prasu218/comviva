import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../plugins/datastore';
import { Router } from '@angular/router';
import { NotificationdbService } from '../../plugins/notificationdb';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { format } from '../../utils/formatter';
import { ProfileService } from '../../plugins/profile';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
public showLoader:boolean=false;
header=false;
  emailng;
  msisdnng;
  a_msisdnng;
  name='';
  date= new Date();
  public existing_cust: FormGroup;
  constructor(private fb: FormBuilder,private _profile: ProfileService, private _storeVal: StoreService,private notifiservice:NotificationdbService, private router: Router) { }

  ngOnInit() {
   this.name=this._storeVal.getFirstNameData();
    this.a_msisdnng = this._storeVal.getAlternateNumberData();
    this.msisdnng = this._storeVal.getMSISDNData();
    this.emailng =  this._storeVal.getEmailData();
    this.existing_cust = this.fb.group({
     // msisdn: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(13)]],
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[.]+[a-zA-Z0-9-.]+$')
      ])],
          });
    
  }
  numberOnly(event): boolean {
    
      let test = event.target.value;
      let test_length = test.length;
      const charCode = (event.which) ? event.which : event.keyCode;
  
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      if (test_length >= 13&& (charCode == 8 || charCode == 13)) {
        return true;
      } else if (test_length >= 13) {
        return false;;
      } else {
        return true;
      }
    }

  godashboard()
  {
    this.router.navigate(['dashboard']);
    this.notifiservice.updateNotificationData(format.msisdn( this.msisdnng),"false").then((resp) => {
  
    })
   
  }
  onSubmit(value){
    
    this._storeVal.setMSISDNupData(this.a_msisdnng);
    this._storeVal.setEmailupData(this.emailng);
    this.router.navigate(['notificationproced']);
//this.checkothers()
    
  }
  checkothers()
  {
   this.showLoader = true;
   if( this.a_msisdnng!=""){
     this._profile.getProfileDetails(format.msisdn(this.a_msisdnng)).then((resp) => {
                   //console.log("debit card++++++------=========",resp)
                   if (resp.status_code == 0) {
                   
                          this.showLoader = false;
                          
                          this.router.navigate(['notificationproced']);
                        
                   }
                   else{
                this.showLoader = false;
               // this.existing_cust.get('msisdn').reset();
                      
                   } 
                 }).catch((err) => {
                   this.showLoader = false;
                 //  this.existing_cust.get('msisdn').reset();
                 
                 });
        }

  }
  

}
