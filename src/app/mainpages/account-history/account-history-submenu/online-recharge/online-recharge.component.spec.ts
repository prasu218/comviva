import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineRechargeComponent } from './online-recharge.component';

describe('OnlineRechargeComponent', () => {
  let component: OnlineRechargeComponent;
  let fixture: ComponentFixture<OnlineRechargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlineRechargeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineRechargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
