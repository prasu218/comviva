import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { GridModel } from '@syncfusion/ej2-angular-grids';
import { AccountHistoryService } from 'src/app/plugins/accountHistory';
import { DatePipe } from '@angular/common';
import { StoreService } from 'src/app/plugins/datastore';
import { Router } from '@angular/router';
import { maxAccountHistoryDate } from 'src/app/config';
import { AngularCDRService } from '../../../../plugins/angularCDR';
declare var $: any;
@Component({
  selector: 'app-online-recharge',
  templateUrl: './online-recharge.component.html',
  styleUrls: ['./online-recharge.component.scss', '../call/call.component.scss']
})
export class OnlineRechargeComponent implements OnInit {

  rechargehistoryForm: FormGroup;
  currentDate = new Date();
  setMinRechargeStartDate;
  startDate = new Date();
  endDate = new Date();
  msisdn;
  historyType = 'rechargeHistory';
  showLoader: boolean = false;
  rechargeMaxDateErrMsg: string;
  rechargeErrMsg: string;
  footerWhite:boolean=true;
  footerGray:boolean= false;
   category: any = 'Account History';
  startTime:any;
  listOfFields = [{ field: 'Charged_amount', headerText: 'Amount', width: '140', textAlign: 'Left' },
  { field: 'Event_dt', headerText: 'Date', width: '130', format: "yMd", textAlign: 'Left' }];
  rechargeHistoryData: any = [];
  dwnldPdfBtnLabel: string = "Download recharge record in PDF";
  dwnldExcelBtnLabel: string = "Download recharge record in Excel";
  fileName: string = "Recharge";
  maxDaysIncludingPresentDate = 6;

  constructor(private formBuilder: FormBuilder,
    private accountHistoryService: AccountHistoryService,
    private storeVal: StoreService, private router: Router,private _cdr: AngularCDRService) { }

  ngOnInit() {
    this.startTime = this._cdr.getTransactionTime(0);
    this.createRechargehistoryForm();
    if (this.storeVal.getMSISDNData() && this.storeVal.getMSISDNData() != "") {
      this.msisdn = this.storeVal.getMSISDNData();
    }
   
    this.startDate.setDate(this.startDate.getDate() - this.maxDaysIncludingPresentDate);
    // this.setMinRechargeStartDate = new Date();
    // this.setMinRechargeStartDate.setDate(this.setMinRechargeStartDate.getDate() - maxAccountHistoryDate.maxDays);
  }

  createRechargehistoryForm() {
    this.rechargehistoryForm = this.formBuilder.group({
      'startdate': new FormControl('', Validators.required),
      'enddate': new FormControl('', Validators.required)
    });
  }

  getStartDate(selectedStartDate) {
    this.rechargehistoryForm.value.startdate = selectedStartDate;
    this.startDate = selectedStartDate;
    // this.currentDate.setDate(selectedStartDate.getDate() + this.maxDaysIncludingPresentDate);
    // if (this.currentDate > new Date()) {
    //   this.currentDate = new Date();
    // }
  }

  getEndDate(selectedEndDate) {
    this.rechargehistoryForm.value.enddate = selectedEndDate;
    this.endDate = selectedEndDate;
    // this.setMinRechargeStartDate = new Date();
    // this.setMinRechargeStartDate.setDate(selectedEndDate.getDate() - maxAccountHistoryDate.maxDays);
  }

  formatDate(selectedDate) {
    var datePipe = new DatePipe("en-US");
    return datePipe.transform(selectedDate, 'yyyy-MM-dd') + 'T00:00:00.000';
  }

  validateMaxDateRange() {
      document.querySelector('#target').scrollIntoView({ behavior: 'smooth', block: 'center' });
    this.rechargeMaxDateErrMsg = "";
    this.rechargeErrMsg = "";
    this.rechargehistoryForm.value.startdate = new Date((this.rechargehistoryForm.value.startdate).setHours(0, 0, 0, 0))
    this.rechargehistoryForm.value.enddate = new Date((this.rechargehistoryForm.value.enddate).setHours(0, 0, 0, 0))
    let diffInTime = Math.abs(this.rechargehistoryForm.value.enddate.getTime() - this.rechargehistoryForm.value.startdate.getTime());
    let noOfSelectedDays = Math.ceil(diffInTime / (1000 * 3600 * 24));
    if (noOfSelectedDays <= maxAccountHistoryDate.maxDays) {
      this.getRechargeHistory();
    } else {
      this.rechargeHistoryData = [];
      this.rechargeMaxDateErrMsg = "Maximum limit is 7 Days. Please try again"
    }
  }

  getRechargeHistory() {
    let selectedStartDate = this.formatDate(this.rechargehistoryForm.value.startdate)
    let selectedEndDate = this.formatDate(this.rechargehistoryForm.value.enddate)
    this.showLoader = true;
    this.rechargeMaxDateErrMsg = "";
    this.rechargeErrMsg = "";
    this.rechargeHistoryData = [];
    this.accountHistoryService.accountHistory(this.msisdn, this.historyType, selectedStartDate, selectedEndDate)
      .then(resp => {
        if (resp.status_code == 0) {
          if (resp.call_details.length > 0) {
             this._cdr.writeCDR(this.startTime, this.category,'Online-Recharge','', 'Success');
            let rechargeRecords = resp.call_details;
            for (let i = 0; i < rechargeRecords.length; i++) {
              rechargeRecords[i].Event_dt = this.convertDateFormat(rechargeRecords[i].Event_dt);
              rechargeRecords[i].Number_called = "+" + rechargeRecords[i].Number_called;
              rechargeRecords[i].Charged_amount = "N " + parseFloat(rechargeRecords[i].Event_value_amt).toFixed(2);
            }
            this.rechargeHistoryData = rechargeRecords;
            $('#wht-crv').css("background-color","#f2f2f2");
            this.footerWhite=false;
            this.footerGray= true;
          } else {
            this.rechargeErrMsg = "No Recharge records within this date range";
            this._cdr.writeCDR(this.startTime, this.category,'Online-Recharge','', 'Failure');
            $('#wht-crv').css("background-color","#f2f2f2");
            this.footerWhite=false;
            this.footerGray= true;
          }
          this.showLoader = false;
        }
      }).catch((err) => {
        this.showLoader = false;
        this.rechargeMaxDateErrMsg = "Unable to connect, please try again.";
        this._cdr.writeCDR(this.startTime, this.category,'Online-Recharge','', 'Failure');
        $('#wht-crv').css("background-color","#f2f2f2");
            this.footerWhite=false;
            this.footerGray= true;
      });
  }

  convertDateFormat(date) {
    return date.substr(6, 2) + "/" + date.substr(4, 2) + "/" + date.substr(0, 4)
  }
}
