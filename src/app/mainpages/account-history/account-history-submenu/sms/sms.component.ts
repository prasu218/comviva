import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { GridModel } from '@syncfusion/ej2-angular-grids';
import { AccountHistoryService } from 'src/app/plugins/accountHistory';
import { DatePipe } from '@angular/common';
import { StoreService } from 'src/app/plugins/datastore';
import { Router } from '@angular/router';
import { maxAccountHistoryDate } from 'src/app/config';
import { AngularCDRService } from '../../../../plugins/angularCDR';
declare var $: any;
@Component({
  selector: 'app-sms',
  templateUrl: './sms.component.html',
  styleUrls: ['./sms.component.scss', '../call/call.component.scss']
})
export class SmsComponent implements OnInit {

  smshistoryForm: FormGroup;
  currentDate = new Date();
  setMinSmsStartDate;
  startDate = new Date();
  endDate = new Date();
  footerWhite:boolean=true;
  footerGray:boolean= false;
  msisdn;
  historyType = 'smsHistory';
  showLoader: boolean = false;
  smsMaxDateErrMsg: string;
  smsErrMsg: string;
  category: any = 'Account History';
  startTime:any;
  listOfFields = [{ field: 'Number_called', headerText: 'Number', width: '170', textAlign: 'Left' },
  { field: 'Event_dt', headerText: 'Date', width: '130', format: "yMd", textAlign: 'Left' },
  { field: 'Charged_amount', headerText: 'Amount', width: '140', textAlign: 'Left' },
  ];
  smsHistoryData: any = [];
  dwnldPdfBtnLabel: string = "Download SMS record in PDF";
  dwnldExcelBtnLabel: string = "Download SMS record in Excel";
  fileName: string = "SMS";
  maxDaysIncludingPresentDate = 6;

  constructor(private formBuilder: FormBuilder,
    private accountHistoryService: AccountHistoryService,
    private storeVal: StoreService, private router: Router ,private _cdr: AngularCDRService) { }

  ngOnInit() {
    this.startTime = this._cdr.getTransactionTime(0);
    this.createSmshistoryForm();
    if (this.storeVal.getMSISDNData() && this.storeVal.getMSISDNData() != "") {
      this.msisdn = this.storeVal.getMSISDNData();
    }
  
    this.startDate.setDate(this.startDate.getDate() - this.maxDaysIncludingPresentDate);
    // this.setMinSmsStartDate = new Date();
    // this.setMinSmsStartDate.setDate(this.setMinSmsStartDate.getDate() - maxAccountHistoryDate.maxDays);
  }

  createSmshistoryForm() {
    this.smshistoryForm = this.formBuilder.group({
      'startdate': new FormControl('', Validators.required),
      'enddate': new FormControl('', Validators.required)
    });
  }

  getStartDate(selectedStartDate) {
    this.smshistoryForm.value.startdate = selectedStartDate;
    this.startDate = selectedStartDate;
    // this.currentDate.setDate(selectedStartDate.getDate() + this.maxDaysIncludingPresentDate);
    // if (this.currentDate > new Date()) {
    //   this.currentDate = new Date();
    // }
  }

  getEndDate(selectedEndDate) {
    this.smshistoryForm.value.enddate = selectedEndDate;
    this.endDate = selectedEndDate;
    // this.setMinSmsStartDate = new Date();
    // this.setMinSmsStartDate.setDate(selectedEndDate.getDate() - maxAccountHistoryDate.maxDays);
  }

  formatDate(selectedDate) {
    var datePipe = new DatePipe("en-US");
    return datePipe.transform(selectedDate, 'yyyy-MM-dd') + 'T00:00:00.000';
  }

  validateMaxDateRange() {
      document.querySelector('#target').scrollIntoView({ behavior: 'smooth', block: 'center' });
    this.smsMaxDateErrMsg = "";
    this.smsErrMsg = "";
    this.smshistoryForm.value.startdate = new Date((this.smshistoryForm.value.startdate).setHours(0, 0, 0, 0))
    this.smshistoryForm.value.enddate = new Date((this.smshistoryForm.value.enddate).setHours(0, 0, 0, 0))
    let diffInTime = Math.abs(this.smshistoryForm.value.enddate.getTime() - this.smshistoryForm.value.startdate.getTime());
    let noOfSelectedDays = Math.ceil(diffInTime / (1000 * 3600 * 24));
    if (noOfSelectedDays <= maxAccountHistoryDate.maxDays) {
      this.getSmsHistory();
    } else {
      this.smsHistoryData = [];
      this.smsMaxDateErrMsg = "Maximum limit is 7 Days. Please try again";
    }
  }

  getSmsHistory() {
    let selectedStartDate = this.formatDate(this.smshistoryForm.value.startdate)
    let selectedEndDate = this.formatDate(this.smshistoryForm.value.enddate)
    this.showLoader = true;
    this.smsMaxDateErrMsg = "";
    this.smsErrMsg = "";
    this.smsHistoryData = [];
    this.accountHistoryService.accountHistory(this.msisdn, this.historyType, selectedStartDate, selectedEndDate)
      .then(resp => {
        if (resp.status_code == 0) {
          if (resp.call_details.length > 0) {
            this._cdr.writeCDR(this.startTime, this.category,'SMS','', 'Success');
            let smsRecords = resp.call_details;
            for (let i = 0; i < smsRecords.length; i++) {
              smsRecords[i].Event_dt = this.convertDateFormat(smsRecords[i].Event_dt);
              smsRecords[i].Number_called = "+" + smsRecords[i].Number_called;
              smsRecords[i].Charged_amount = "N " + smsRecords[i].Charged_amount;
            }
            this.smsHistoryData = smsRecords;
            $('#wht-crv').css("background-color","#f2f2f2");
            this.footerWhite=false;
            this.footerGray= true;
          } else {
            this.smsErrMsg = "No SMS records within this date range";
             this._cdr.writeCDR(this.startTime, this.category,'SMS','', 'Failure');
            $('#wht-crv').css("background-color","#f2f2f2");
            this.footerWhite=false;
            this.footerGray= true;
          }
        }
        this.showLoader = false;
      }).catch((err) => {
        this.showLoader = false;
        this.smsMaxDateErrMsg = "Unable to connect, please try again.";
        this._cdr.writeCDR(this.startTime, this.category,'SMS','', 'Failure');
        $('#wht-crv').css("background-color","#f2f2f2");
            this.footerWhite=false;
            this.footerGray= true;
      });
  }

  convertDateFormat(date) {
    return date.substr(6, 2) + "/" + date.substr(4, 2) + "/" + date.substr(0, 4)
  }

}
