import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AccountHistoryService } from 'src/app/plugins/accountHistory';
import { DatePipe } from '@angular/common';
import { StoreService } from 'src/app/plugins/datastore';
import { Router } from '@angular/router';
import { maxAccountHistoryDate } from 'src/app/config';
import { AngularCDRService } from '../../../../plugins/angularCDR';
declare var $: any;
@Component({
  selector: 'app-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.scss']
})

export class CallComponent implements OnInit {

  callhistoryForm: FormGroup;
  currentDate = new Date();
  setMinCallStartDate;
  startDate = new Date();
  endDate = new Date();
  msisdn;
  historyType = 'voiceHistory';
  showLoader: boolean = false;
  callMaxDateErrMsg: string;
  callErrMsg: string;
  footerWhite:boolean=true;
  footerGray:boolean= false;
  category: any = 'Account History';
  startTime:any;
  listOfFields = [
    { field: 'Number_called', headerText: 'Number', width: '170' },
    { field: 'Event_dt', headerText: 'Date', width: '130', format: "yMd" },
    { field: 'Call_duration_qty', headerText: 'Duration', width: '150' },
    { field: 'Charged_amount', headerText: 'Amount', width: '140' }
  ];
  callHistoryData: any = [];
  dwnldPdfBtnLabel: string = "Download call record in PDF";
  dwnldExcelBtnLabel: string = "Download call record in Excel";
  fileName: string = "Call";
  maxDaysIncludingPresentDate = 6;

  constructor(private formBuilder: FormBuilder,
    private accountHistoryService: AccountHistoryService,
    private storeVal: StoreService, private router: Router,private _cdr: AngularCDRService) { }

  ngOnInit() {
    this.startTime = this._cdr.getTransactionTime(0);
    this.createCallhistoryForm();
    if (this.storeVal.getMSISDNData() && this.storeVal.getMSISDNData() != "") {
      this.msisdn = this.storeVal.getMSISDNData();
    }
    this.startDate.setDate(this.startDate.getDate() - this.maxDaysIncludingPresentDate);
    // this.setMinCallStartDate = new Date();
    // this.setMinCallStartDate.setDate(this.setMinCallStartDate.getDate() - maxAccountHistoryDate.maxDays);
  }

  createCallhistoryForm() {
    this.callhistoryForm = this.formBuilder.group({
      'startdate': new FormControl('', Validators.required),
      'enddate': new FormControl('', Validators.required)
    });
  }
 
  getStartDate(selectedStartDate) {
    this.callhistoryForm.value.startdate = selectedStartDate;
    this.startDate = selectedStartDate;
    // this.currentDate.setDate(selectedStartDate.getDate() + this.maxDaysIncludingPresentDate);
    // if (this.currentDate > new Date()) {
    //   this.currentDate = new Date();
    // }
  }

  getEndDate(selectedEndDate) {
    this.callhistoryForm.value.enddate = selectedEndDate;
    this.endDate = selectedEndDate;
    // this.setMinCallStartDate = new Date();
    // this.setMinCallStartDate.setDate(selectedEndDate.getDate() - maxAccountHistoryDate.maxDays);
  }

  formatDate(selectedDate) {
    var datePipe = new DatePipe("en-US");
    return datePipe.transform(selectedDate, 'yyyy-MM-dd') + 'T00:00:00.000';
  }

  validateMaxDateRange() {
      document.querySelector('#target').scrollIntoView({ behavior: 'smooth', block: 'center' });
    this.callMaxDateErrMsg = "";
    this.callErrMsg = "";
    this.callhistoryForm.value.startdate = new Date((this.callhistoryForm.value.startdate).setHours(0, 0, 0, 0))
    this.callhistoryForm.value.enddate = new Date((this.callhistoryForm.value.enddate).setHours(0, 0, 0, 0))
    let diffInTime = Math.abs(this.callhistoryForm.value.enddate.getTime() - this.callhistoryForm.value.startdate.getTime());
    let noOfSelectedDays = Math.ceil(diffInTime / (1000 * 3600 * 24));
    if (noOfSelectedDays <= maxAccountHistoryDate.maxDays) {
      this.getCallHistory();
    } else {
      this.callHistoryData = [];
      this.callMaxDateErrMsg = "Maximum limit is 7 Days. Please try again"
    }
  }

  getCallHistory() {
    let selectedStartDate = this.formatDate(this.callhistoryForm.value.startdate)
    let selectedEndDate = this.formatDate(this.callhistoryForm.value.enddate)
    this.showLoader = true;
    this.callErrMsg = "";
    this.callMaxDateErrMsg = "";
    this.callHistoryData = [];
    this.accountHistoryService.accountHistory(this.msisdn, this.historyType, selectedStartDate, selectedEndDate)
      .then((resp) => {
        if (resp.status_code == 0) {
          if (resp.call_details.length > 0) {
            this._cdr.writeCDR(this.startTime, this.category,'Call','', 'Success');
            let callRecords = resp.call_details;
            for (let i = 0; i < callRecords.length; i++) {
              callRecords[i].Event_dt = this.convertDateFormat(callRecords[i].Event_dt);
              callRecords[i].Number_called = "+" + callRecords[i].Number_called;
              callRecords[i].Call_duration_qty = callRecords[i].Call_duration_qty + " sec";
              callRecords[i].Charged_amount = "N " + callRecords[i].Charged_amount;
            }
            this.callHistoryData = callRecords;
            $('#wht-crv').css("background-color","#f2f2f2");
            this.footerWhite=false;
            this.footerGray= true;
          } else {
            this.callErrMsg = "No Call records within this date range";
             this._cdr.writeCDR(this.startTime, this.category,'Call','', 'Failure');
            $('#wht-crv').css("background-color","#f2f2f2");
            this.footerWhite=false;
            this.footerGray= true;
          }
        }
        this.showLoader = false;
      }).catch((err) => {
        this.showLoader = false;
        this.callMaxDateErrMsg = "Unable to connect, please try again.";
         this._cdr.writeCDR(this.startTime, this.category,'Call','', 'Failure');
        $('#wht-crv').css("background-color","#f2f2f2");
        this.footerWhite=false;
        this.footerGray= true;
      });
  }

  convertDateFormat(date) {
    return date.substr(6, 2) + "/" + date.substr(4, 2) + "/" + date.substr(0, 4)
  }

}
