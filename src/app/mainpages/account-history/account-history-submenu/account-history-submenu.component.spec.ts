import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountHistorySubmenuComponent } from './account-history-submenu.component';

describe('AccountHistorySubmenuComponent', () => {
  let component: AccountHistorySubmenuComponent;
  let fixture: ComponentFixture<AccountHistorySubmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountHistorySubmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountHistorySubmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
