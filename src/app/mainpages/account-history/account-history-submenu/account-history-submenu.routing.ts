import { Routes } from '@angular/router';
import { AccountHistoryComponent } from '../account-history.component';
import { CallComponent } from './call/call.component';
import { SmsComponent } from './sms/sms.component';
import { DataComponent } from './data/data.component';
import { OnlineRechargeComponent } from './online-recharge/online-recharge.component';
import { AuthGuard } from 'src/app/auth/auth.guard';

export const Accounthistorymenuroutes: Routes = [{
  path: 'history', component: AccountHistoryComponent,
  children: [
    {
      path: '',
      component: CallComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'call',
      component: CallComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'sms',
      component: SmsComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'data',
      component: DataComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
    {
      path: 'onlinerecharge',
      component: OnlineRechargeComponent,
      pathMatch: 'full',
      canActivate: [AuthGuard]
    },
  ]
}]