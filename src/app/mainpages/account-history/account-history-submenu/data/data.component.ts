import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { GridModel } from '@syncfusion/ej2-angular-grids';
import { AccountHistoryService } from 'src/app/plugins/accountHistory';
import { DatePipe } from '@angular/common';
import { StoreService } from 'src/app/plugins/datastore';
import { Router } from '@angular/router';
import { maxAccountHistoryDate } from 'src/app/config';
import { AngularCDRService } from '../../../../plugins/angularCDR';
declare var $: any;
@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss', '../call/call.component.scss']
})
export class DataComponent implements OnInit {

  datahistoryForm: FormGroup;
  currentDate = new Date();
  setMinDataStartDate;
  startDate = new Date();
  endDate = new Date();
  msisdn;
  historyType = 'dataHistory';
  showLoader: boolean = false;
  dataMaxDateErrMsg: string;
  dataErrMsg: string;
  footerWhite:boolean=true;
  footerGray:boolean= false;
   category: any = 'Account History';
   startTime:any;
  listOfFields = [{ field: 'Charged_amount', headerText: 'Amount', width: '140', textAlign: 'Left' },
  { field: 'Data_volume', headerText: 'Consumed' },
  { field: 'Event_dt', headerText: 'Date', width: '490', textAlign: 'Left' }];
  totalConsumedData;
  dataHistoryData: any = [];
  childDataRecords: any = [];
  hierarchyDataToExport: any = [];
  childGrid: GridModel = {
    dataSource: this.childDataRecords,
    queryString: 'Event_dt',
   
    columns: [
      { field: 'Time', textAlign: 'Left', width: '400' },
      { field: 'Amount', textAlign: 'Right', width: '100' },
      { field: 'Dash', textAlign: 'Center', width: '30' },
      { field: 'Data', textAlign: 'Left', width: '130' }
    ],
  };
  dwnldPdfBtnLabel: string = "Download data record in PDF";
  dwnldExcelBtnLabel: string = "Download data record in Excel";
  fileName: string = "Data";
  maxDaysIncludingPresentDate = 3;

  constructor(private formBuilder: FormBuilder,
    private accountHistoryService: AccountHistoryService,
    private storeVal: StoreService, private router: Router,private _cdr: AngularCDRService) { }

  ngOnInit() {
    this.startTime = this._cdr.getTransactionTime(0);
    this.createDatahistoryForm();
    if (this.storeVal.getMSISDNData() && this.storeVal.getMSISDNData() != "") {
      this.msisdn = this.storeVal.getMSISDNData();
    }

    this.startDate.setDate(this.startDate.getDate() - this.maxDaysIncludingPresentDate);
    // this.setMinDataStartDate = new Date();
    // this.setMinDataStartDate.setDate(this.setMinDataStartDate.getDate() - maxAccountHistoryDate.dataMaxDays);
  }

  createDatahistoryForm() {
    this.datahistoryForm = this.formBuilder.group({
      'startdate': new FormControl('', Validators.required),
      'enddate': new FormControl('', Validators.required)
    });
  }

  getStartDate(selectedStartDate) {
    this.datahistoryForm.value.startdate = selectedStartDate;
    this.startDate = selectedStartDate;
    // this.currentDate.setDate(selectedStartDate.getDate() + this.maxDaysIncludingPresentDate);
    // if (this.currentDate > new Date()) {
    //   this.currentDate = new Date();
    // }
  }

  getEndDate(selectedEndDate) {
    this.datahistoryForm.value.enddate = selectedEndDate;
    this.endDate = selectedEndDate;
    // this.setMinDataStartDate = new Date();
    // this.setMinDataStartDate.setDate(selectedEndDate.getDate() - maxAccountHistoryDate.dataMaxDays);
  }

  formatDate(selectedDate) {
    var datePipe = new DatePipe("en-US");
    return datePipe.transform(selectedDate, 'yyyy-MM-dd') + 'T00:00:00.000';
  }

  validateMaxDateRange() {
      document.querySelector('#target').scrollIntoView({ behavior: 'smooth', block: 'center' });
    this.dataMaxDateErrMsg = "";
    this.dataErrMsg = "";
    this.datahistoryForm.value.startdate = new Date((this.datahistoryForm.value.startdate).setHours(0, 0, 0, 0))
    this.datahistoryForm.value.enddate = new Date((this.datahistoryForm.value.enddate).setHours(0, 0, 0, 0))
    let diffInTime = Math.abs(this.datahistoryForm.value.enddate.getTime() - this.datahistoryForm.value.startdate.getTime());
    let noOfSelectedDays = Math.ceil(diffInTime / (1000 * 3600 * 24));
    if (noOfSelectedDays <= maxAccountHistoryDate.dataMaxDays) {
      this.getDataHistory();
    } else {
      this.dataHistoryData = [];
      this.dataMaxDateErrMsg = "Maximum limit is 4 Days. Please try again";
    }
  }

  getDataHistory() {
    let selectedStartDate = this.formatDate(this.datahistoryForm.value.startdate)
    let selectedEndDate = this.formatDate(this.datahistoryForm.value.enddate)
    this.showLoader = true;
    this.dataMaxDateErrMsg = "";
    this.dataErrMsg = "";
    this.dataHistoryData = [];
    this.accountHistoryService.accountHistory(this.msisdn, this.historyType, selectedStartDate, selectedEndDate)
      .then(resp => {
        if (resp.status_code == 0) {
          if (resp.call_details.length > 0) {
             this._cdr.writeCDR(this.startTime, this.category,'Data','', 'Success');
            this.calculateTotalDataByDate(resp.call_details);
            $('#wht-crv').css("background-color","#f2f2f2");
            this.footerWhite=false;
            this.footerGray= true;
          }
          else {
             this._cdr.writeCDR(this.startTime, this.category,'Data','', 'Failure');
            this.dataErrMsg = "No Data records within this date range";
            $('#wht-crv').css("background-color","#f2f2f2");
            this.footerWhite=false;
            this.footerGray= true;
          }
          this.showLoader = false;
        }
      }).catch((err) => {
        this._cdr.writeCDR(this.startTime, this.category,'Data','', 'Failure');
        this.showLoader = false;
        this.dataMaxDateErrMsg = "Unable to connect, please try again.";
        $('#wht-crv').css("background-color","#f2f2f2");
            this.footerWhite=false;
            this.footerGray= true;
      });
  }

  convertDateFormat(date) {
    return date.substr(6, 2) + "/" + date.substr(4, 2) + "/" + date.substr(0, 4)
  }

  calculateTotalDataByDate(dataHistoryRecords) {
    let sumOfData = 0;
    let totalData = 0;
    let parentDataRecords;

    parentDataRecords = this.getUniqueDate(dataHistoryRecords);
    if (parentDataRecords) {
      for (var i = 0; i < dataHistoryRecords.length; i++) {
        let dataTimeAsPerDate = {
          Event_dt: '',
          Time: '',
          Amount: '',
          Dash: '|',
          Data: ''
        };
        let hierarchyExportObj = {
          Event_dt: '',
          Charged_amount: '',
          Data_volume: ''
        }
        totalData += parseFloat(dataHistoryRecords[i].Data_volume);
        for (var j = 0; j < parentDataRecords.length; j++) {
          if (this.convertDateFormat(dataHistoryRecords[i].Event_dt) == parentDataRecords[j].Event_dt) {
            let splitChargedAmt = parentDataRecords[j].Charged_amount.substr(2);
            let charged_amount = parseFloat(splitChargedAmt) + parseFloat(dataHistoryRecords[i].Charged_amount);
            parentDataRecords[j].Charged_amount = "N " + parseFloat(charged_amount.toString()).toFixed(2);
            sumOfData += parseFloat(dataHistoryRecords[i].Data_volume);
            parentDataRecords[j].Data_volume = this.formatBytes(sumOfData);
            dataTimeAsPerDate.Event_dt = this.convertDateFormat(dataHistoryRecords[i].Event_dt);
            dataTimeAsPerDate.Time = this.formatTime(dataHistoryRecords[i].Event_dt);
            dataTimeAsPerDate.Amount = "N " + dataHistoryRecords[i].Charged_amount;
            dataTimeAsPerDate.Data = this.formatBytes(dataHistoryRecords[i].Data_volume);
            hierarchyExportObj.Event_dt = this.convertDateFormat(dataHistoryRecords[i].Event_dt) + " " + this.formatTime(dataHistoryRecords[i].Event_dt);
            hierarchyExportObj.Charged_amount = dataHistoryRecords[i].Charged_amount;
            hierarchyExportObj.Data_volume = this.formatBytes(sumOfData);
            this.childDataRecords.push(dataTimeAsPerDate);
            this.hierarchyDataToExport.push(hierarchyExportObj);
          }
        }
      }
      this.dataHistoryData = parentDataRecords;
      this.totalConsumedData = this.formatBytes(totalData);
    }
  }

  formatBytes(bytes) {
    if (bytes < 1024) return (bytes / 1024).toFixed(2) + " KB";
    else if (bytes < 1048576) {
      let dataToMB;
      dataToMB = (bytes / 1048576).toFixed(2);
      if (dataToMB == 0) {
        dataToMB = (bytes / 1024).toFixed(2) + " KB"
      } else {
        dataToMB = dataToMB + " MB"
      }
      return dataToMB;
    }
    // else return (bytes / 1073741824).toFixed(2) + " GB";
    else {
      let dataToGB;
      dataToGB = (bytes / 1073741824).toFixed(2);
      if (dataToGB == 0 || dataToGB <= 0.09) {
        dataToGB = (bytes / 1048576).toFixed(2) + " MB"
      } else {
        dataToGB = dataToGB + " GB"
      }
      return dataToGB;
    }
  }

  formatTime(time) {
    return time.substr(8, 2) + ":" + time.substr(10, 2) + ":" + time.substr(12, 2)
  }

  getUniqueDate(dataHistoryRecords) {
    let uniqueDatesArr = Array.from(new Set(dataHistoryRecords.map((item: any) => this.convertDateFormat(item.Event_dt))))
    let listOfUniqueDataRecords = [];
    if (uniqueDatesArr) {
      for (var m = 0; m < uniqueDatesArr.length; m++) {
        let data = {
          Event_dt: {},
          Charged_amount: 'N 0.0',
          Data_volume: ''
        };
        data.Event_dt = uniqueDatesArr[m];
        listOfUniqueDataRecords.push(data);
      }
      return listOfUniqueDataRecords;
    }
  }

}
