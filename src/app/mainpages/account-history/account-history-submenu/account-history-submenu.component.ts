import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-account-history-submenu',
  templateUrl: './account-history-submenu.component.html',
  styleUrls: ['./account-history-submenu.component.scss']
})
export class AccountHistorySubmenuComponent implements OnInit {

  @Input() active_nav: string;

  accounthistoryNavElements = [
    { id: 'call', value: 'Call', source: 'assets/images/call.svg', },
    { id: 'sms', value: 'SMS', source: 'assets/images/sms.svg', },
    { id: 'data', value: 'Data', source: 'assets/images/data.svg', },
    { id: 'onlinerecharge', value: 'Recharge', source: 'assets/images/online_recharge.svg', },
  ];

  constructor(private _router: Router) { }

  ngOnInit() {

    $( document ).ready(function() {
      $('.carousel-crds').flickity({
        groupCells: true,
        freeScroll: true,
        cellAlign: 'left', 
        contain: true, 
        prevNextButtons: false,
        pageDots: true, 
        draggable: true,
        watchCSS: true,
        setGallerySize: false,
        resize: true,
        reposition: true
      });
    });
  }

  ngAfterContentChecked(){
    this.active_nav = this._router.url.split('/')[2];
    if(this._router.url === "/history"){
      this.active_nav  = "call";
    }
  }
  
 onLoadMdNav(selectedData){
    this._router.navigate(['history',selectedData.id]);
     this.active_nav = selectedData.value;
  }

}
