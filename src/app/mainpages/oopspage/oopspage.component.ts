import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StoreService } from '../../plugins/datastore';
import { Location } from '@angular/common';

@Component({
  selector: 'app-oopspage',
  templateUrl: './oopspage.component.html',
  styleUrls: ['./oopspage.component.scss']
})
export class OopspageComponent implements OnInit {

  header = false;
  constructor(private router: Router, private _storeVal: StoreService,private location: Location) { }

  ngOnInit() {
  }
  backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }
  Gobuybundles(){
    this.router.navigate(['buybundles']);
    if (this._storeVal.getMSISDNData() && this._storeVal.getMSISDNData()!= "") {  
    }
    else{
      this.router.navigate(['login'])
    }
  }


}
