// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';

// import { RoutingModule } from './login-routing.module';
// import { LoginComponent } from './login.component';
// import { ThemeModule } from 'src/app/theme/theme.module';
// import { DashboardComponent } from '../dashboard/dashboard.component';
// import { DemoComponent } from '../demo/demo.component';
// import { FormsModule, ReactiveFormsModule }         from '@angular/forms';
// import { LoginSubmenuComponent } from './login-submenu/login-submenu.component';
// import { VoucherCardComponent } from './login-submenu/voucher-card/voucher-card.component';
// import { DamageCardComponent } from './login-submenu/damage-card/damage-card.component';
// import { loginrcgService } from 'src/app/plugins/loginrcg';
// import { RechargeComponent } from '../recharge/recharge.component';
// import { RechargeSubmenuComponent } from '../recharge/recharge-submenu/recharge-submenu.component';
// import { SmeBundlesComponent } from '../buybundles/buybundle-submenu/smebundles/smebundles.component';
// import { BuybundlesComponent } from '../buybundles/buybundles.component';
// import { BuybundleSubmenuComponent } from '../buybundles/buybundle-submenu/buybundle-submenu.component';
// import { XtraplansComponent } from '../buybundles/buybundle-submenu/xtraplans/xtraplans.component';
// import { DataplansComponent } from '../buybundles/buybundle-submenu/dataplans/dataplans.component';
// import { MysubscriptionComponent } from '../dashboard/dashboard-submenu/mysubscription/mysubscription.component';
// import {DashboardSubmenuComponent} from '../dashboard/dashboard-submenu/dashboard-submenu.component';
// import { RechargeotherservicesComponent } from '../recharge/rechargeotherservices/rechargeotherservices.component';
// import { RechargestatusComponent } from '../recharge/rechargeotherservices/rechargestatus/rechargestatus.component';
// import { GoodybagComponent } from '../buybundles/buybundle-submenu/goodybag/goodybag.component';
// import { HynetflexComponent } from '../buybundles/buybundle-submenu/hynetflex/hynetflex.component';
// import { RoamingComponent } from '../buybundles/buybundle-submenu/roaming/roaming.component';
// import { IntcallComponent } from '../buybundles/buybundle-submenu/intcall/intcall.component';
// import { DebitCardProceedComponent } from '../recharge/recharge-submenu/debit-card/debit-card-proceed/debit-card-proceed.component';
// import {SendAirtimeProceedComponent} from '../recharge/recharge-submenu/send-airtime/send-airtime-proceed/send-airtime-proceed.component';
// import { FaqsComponent } from '../faqs/faqs.component';
// import { LegalComponent } from '../legal/legal.component';
// import { SecurityComponent } from '../security/security.component';
// import { ContactusComponent } from '../contactus/contactus.component';
// import { TermscondComponent } from '../terms-condition/terms-condition.component';
// import { MtnservicestoresComponent } from '../mtnservicestores/mtnservicestores.component';
// import { MtnstoreSubmenuComponent } from '../mtnservicestores/mtnstore-submenu/mtnstore-submenu.component';
// import { MtnstoreloacterComponent } from '../mtnservicestores/mtnstore-submenu/mtnstoreloacter/mtnstoreloacter.component';
// import { ComplaintsComponent } from '../complaints/complaints.component';
// import { ComplaintsSubmenuComponent } from '../complaints/complaints-submenu/complaints-submenu.component';
// import { LogComplaintsComponent } from '../complaints/complaints-submenu/log-complaints/log-complaints.component';
// import { SearchComplaintsComponent } from '../complaints/complaints-submenu/search-complaints/search-complaints.component';
// import { FeedbackComponent } from '../feedback/feedback.component';
// import { DetailsofprofileComponent } from '../detailsofprofile/detailsofprofile.component';
// import { DetailsprofileSubmenuComponent } from '../detailsofprofile/detailsprofile-submenu/detailsprofile-submenu.component';
// import { DetailssimComponent } from '../detailsofprofile/detailsprofile-submenu/detailssim/detailssim.component';
// import { OtherdetailsComponent } from '../detailsofprofile/detailsprofile-submenu/otherdetails/otherdetails.component';

// import { AccountHistoryComponent } from '../account-history/account-history.component';
// import { AccountHistorySubmenuComponent } from '../account-history/account-history-submenu/account-history-submenu.component';
// import { AddaccountComponent } from '../addaccount/addaccount.component';
// import {AddAccountProceedComponent} from'../addaccount/addaccount-proceed/addaccount-proceed.component';
// import { NotificationComponent } from '../notification/notification.component';
// import { NotificationprocedComponent } from '../notificationproced/notificationproced.component';
// import { TarrifplanComponent } from '../tarrifplan/tarrifplan.component';
// import { TarrifsubmenuComponent } from '../tarrifplan/tarrifsubmenu/tarrifsubmenu.component';
// import { ActiveplanComponent } from '../tarrifplan/tarrifsubmenu/activeplan/activeplan.component';
// import { OtherplanComponent } from '../tarrifplan/tarrifsubmenu/otherplan/otherplan.component';
// import { OtherproProccedComponent } from '../detailsofprofile/detailsprofile-submenu/otherdetails/otherpro-procced/otherpro-procced.component';
// import {SwitchmainAccountComponent} from'../switchmainaccount/switchmainaccount.component';
// import { AgmCoreModule } from '@agm/core';

// @NgModule({
//   declarations: [AccountHistoryComponent, AccountHistorySubmenuComponent, LegalComponent,TermscondComponent,ContactusComponent,SecurityComponent,LoginComponent,RechargestatusComponent,RechargeotherservicesComponent,MysubscriptionComponent,DashboardSubmenuComponent,
// DashboardComponent,DemoComponent,NotificationprocedComponent,LoginSubmenuComponent ,FeedbackComponent,VoucherCardComponent ,DamageCardComponent,RechargeComponent,RechargeSubmenuComponent,
//  BuybundlesComponent, BuybundleSubmenuComponent, XtraplansComponent, DataplansComponent,GoodybagComponent,
// SmeBundlesComponent,RoamingComponent, HynetflexComponent,IntcallComponent,
// DebitCardProceedComponent,SendAirtimeProceedComponent,FaqsComponent,MtnservicestoresComponent, MtnstoreSubmenuComponent, MtnstoreloacterComponent,ComplaintsComponent,NotificationComponent,ComplaintsSubmenuComponent,LogComplaintsComponent,SearchComplaintsComponent,DetailsofprofileComponent,DetailsprofileSubmenuComponent, DetailssimComponent, OtherdetailsComponent,
// AddaccountComponent,AddAccountProceedComponent,TarrifplanComponent, TarrifsubmenuComponent, ActiveplanComponent, OtherplanComponent,OtherproProccedComponent,SwitchmainAccountComponent ],
//   imports: [
//     CommonModule,
//     RoutingModule,
//     ThemeModule,
//     FormsModule,
//     ReactiveFormsModule,
//     AgmCoreModule.forRoot({
//       apiKey: 'AIzaSyCNh7O2WN1enMTcDry9z8SNojRzIQiv93M'
//     })
//   ],
//   providers:[loginrcgService]
// })
// export class LoginModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutingModule } from './login-routing.module';

import { ThemeModule } from 'src/app/theme/theme.module';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { DemoComponent } from '../demo/demo.component';
import { FormsModule, ReactiveFormsModule }         from '@angular/forms';

import { loginrcgService } from 'src/app/plugins/loginrcg';
import { RechargeComponent } from '../recharge/recharge.component';
import { RechargeSubmenuComponent } from '../recharge/recharge-submenu/recharge-submenu.component';
import { SmeBundlesComponent } from '../buybundles/buybundle-submenu/smebundles/smebundles.component';
import { BuybundlesComponent } from '../buybundles/buybundles.component';
import { BuybundleSubmenuComponent } from '../buybundles/buybundle-submenu/buybundle-submenu.component';
import { XtraplansComponent } from '../buybundles/buybundle-submenu/xtraplans/xtraplans.component';
import { DataplansComponent } from '../buybundles/buybundle-submenu/dataplans/dataplans.component';
import { MysubscriptionComponent } from '../dashboard/dashboard-submenu/mysubscription/mysubscription.component';
import {DashboardSubmenuComponent} from '../dashboard/dashboard-submenu/dashboard-submenu.component';
import { RechargeotherservicesComponent } from '../recharge/rechargeotherservices/rechargeotherservices.component';
import { RechargestatusComponent } from '../recharge/rechargeotherservices/rechargestatus/rechargestatus.component';
import { GoodybagComponent } from '../buybundles/buybundle-submenu/goodybag/goodybag.component';
import { HynetflexComponent } from '../buybundles/buybundle-submenu/hynetflex/hynetflex.component';
import { RoamingComponent } from '../buybundles/buybundle-submenu/roaming/roaming.component';
import { IntcallComponent } from '../buybundles/buybundle-submenu/intcall/intcall.component';
import { DebitCardProceedComponent } from '../recharge/recharge-submenu/debit-card/debit-card-proceed/debit-card-proceed.component';
import {SendAirtimeProceedComponent} from '../recharge/recharge-submenu/send-airtime/send-airtime-proceed/send-airtime-proceed.component';
import { FaqsComponent } from '../faqs/faqs.component';
import { LegalComponent } from '../legal/legal.component';
import { SecurityComponent } from '../security/security.component';
import { ContactusComponent } from '../contactus/contactus.component';
import { TermscondComponent } from '../terms-condition/terms-condition.component';
import { MtnservicestoresComponent } from '../mtnservicestores/mtnservicestores.component';
import { MtnstoreSubmenuComponent } from '../mtnservicestores/mtnstore-submenu/mtnstore-submenu.component';
import { MtnstoreloacterComponent } from '../mtnservicestores/mtnstore-submenu/mtnstoreloacter/mtnstoreloacter.component';
import { ComplaintsComponent } from '../complaints/complaints.component';
import { ComplaintsSubmenuComponent } from '../complaints/complaints-submenu/complaints-submenu.component';
import { LogComplaintsComponent } from '../complaints/complaints-submenu/log-complaints/log-complaints.component';
import { SearchComplaintsComponent } from '../complaints/complaints-submenu/search-complaints/search-complaints.component';
import { FeedbackComponent } from '../feedback/feedback.component';
import { DetailsofprofileComponent } from '../detailsofprofile/detailsofprofile.component';
import { DetailsprofileSubmenuComponent } from '../detailsofprofile/detailsprofile-submenu/detailsprofile-submenu.component';
import { DetailssimComponent } from '../detailsofprofile/detailsprofile-submenu/detailssim/detailssim.component';
import { OtherdetailsComponent } from '../detailsofprofile/detailsprofile-submenu/otherdetails/otherdetails.component';

import { AccountHistoryComponent } from '../account-history/account-history.component';
import { AccountHistorySubmenuComponent } from '../account-history/account-history-submenu/account-history-submenu.component';
import { AddaccountComponent } from '../addaccount/addaccount.component';
import {AddAccountProceedComponent} from'../addaccount/addaccount-proceed/addaccount-proceed.component';
import { NotificationComponent } from '../notification/notification.component';
import { NotificationprocedComponent } from '../notificationproced/notificationproced.component';
import { TarrifplanComponent } from '../tarrifplan/tarrifplan.component';
import { TarrifsubmenuComponent } from '../tarrifplan/tarrifsubmenu/tarrifsubmenu.component';
import { ActiveplanComponent } from '../tarrifplan/tarrifsubmenu/activeplan/activeplan.component';
import { OtherplanComponent } from '../tarrifplan/tarrifsubmenu/otherplan/otherplan.component';
import { OtherproProccedComponent } from '../detailsofprofile/detailsprofile-submenu/otherdetails/otherpro-procced/otherpro-procced.component';
import {SwitchmainAccountComponent} from'../switchmainaccount/switchmainaccount.component';
import { AgmCoreModule } from '@agm/core';
import { OopspageComponent } from '../oopspage/oopspage.component';
import { OtpConfirmComponent } from './otpconfirm.component';

@NgModule({
  declarations: [OtpConfirmComponent
   ],
  imports: [
    CommonModule,
    RoutingModule,
    ThemeModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCNh7O2WN1enMTcDry9z8SNojRzIQiv93M'
    })
  ],
  providers:[loginrcgService]
})
export class OtpConfirmModule { }
