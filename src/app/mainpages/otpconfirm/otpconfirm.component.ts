import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
//  import { format } from '../../utils/formatter';
import { Router } from '@angular/router';
import { config } from '../../config';
import { format } from '../../utils/formatter';
import { LoginForm } from '../../utils/interface';
import { ProfileService } from '../../plugins/profile';
import { otpService } from '../../plugins/otp';
import { StoreService } from '../../plugins/datastore';
import { BnNgIdleService } from 'bn-ng-idle';
import { NotificationdbService } from '../../plugins/notificationdb';
import { SessionService } from 'src/app/plugins/session';
// import { Observable } from 'rxjs/';
import { AngularCDRService } from '../../plugins/angularCDR';
import { Location } from '@angular/common';

@Component({
  selector: 'app-otpconfirm',
  templateUrl: './otpconfirm.component.html',
  styleUrls: ['./otpconfirm.component.scss']
})
export class OtpConfirmComponent implements OnInit {
  bgColorpp = 'grey';
  selctradio: boolean = false;
  // existing_cust: FormGroup;
  True: boolean = true;
  public existing_cust: FormGroup;
  public otp_cust: FormGroup;
  public hideee: boolean = false;
  public existing_MTN: FormGroup;
  public OTPShow: boolean = true;
  public sendotp: boolean = true;
  // public msisdn:any;
  public emailFlag: boolean = false;
  public errorMsg = '';
  email: string;
  alternateNumber: number;
  primaryNumber: number;
  showLoader: boolean = false;
  public message: string;

  otp_radio: string;
  fmisidn: any;
  phonenum: string;
  header = false;
  otpValue: string;
  valiotp: string;
  timeoutErr = ''
  startTime: any;
  category: any = 'OTP';
  //value="";
  checktOtp: true;
  sessionErr: string = '';
linkAccountFlag:boolean = false;
  constructor(private fb: FormBuilder, private notifiservice: NotificationdbService, private _profile: ProfileService, private _otp: otpService, private _storeVal: StoreService, private router: Router, private bnIdle: BnNgIdleService, private _cdr: AngularCDRService, private sessionService: SessionService,private location: Location) { }

  ngOnInit() {
    this._storeVal.setSecondaryAccountfalse(true);
    this.displayEmail();
    this.existing_cust = this.fb.group({
      msisdn: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(13)]],


    });
    this.otp_cust = this.fb.group({
      enterotp: ['', [Validators.required, Validators.minLength(6)]],
    });

    this.displaySessionErr();
  }
  async displayEmail(){
    this.primaryNumber = await this._storeVal.getMSISDNData();
    this.email = await this._storeVal.getEmailData();
    this.alternateNumber = this._storeVal.getAlternateNumberData();
  }

  displaySessionErr() {
    let sessionStatus = this._storeVal.getSessionStatus();
    if (sessionStatus == 'expired') {
      this.sessionErr = "Session Time out. To keep you safe, you have been logged out due to inactivity. Please login again";
      localStorage.removeItem('session_status');
    }
  }
  numberOnly(event): boolean {
    if (this.timeoutErr) {
      this.timeoutErr = '';
    }
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length >= 13 && (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length >= 13) {
      return false;;
    } else {
      return true;
    }
  }

 backbuttons(){
    this.location.back();
  }


  numberOnlyotp(event): boolean {
    let test = event.target.value;
    let test_length = test.length;
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (test_length >= 6 && (charCode == 8 || charCode == 13)) {
      return true;
    } else if (test_length >= 6) {
      return false;;
    } else {
      return true;
    }
  }


  checkEmail(value) {

    this.emailFlag = value;
  }

  sendotpClick() {
    if(!this.primaryNumber || !this.emailFlag){
      this.timeoutErr = 'Please select a channel for Otp';
      return;
    }
    this.timeoutErr = '';
    this.showLoader = true;
    this.bnIdle.startWatching(300).subscribe((res) => {
      this.checktOtp = true;
      this.bnIdle.stopTimer();
    })
    // alert("this.bnIdle.startWatching");
    //this.OTPShow = !this.OTPShow;
    if (this.emailFlag) {
      this.otp_radio = this.primaryNumber.toString();
    }
    this._otp.generateOTP(this.otp_radio, this.emailFlag,this.linkAccountFlag).then((resp) => {
      this.valiotp = resp.otp;
      if (resp.status == 0) {
        this.sendotp = false;
        this.OTPShow = false;

        this._storeVal.setOtpData(resp.otp)
        this.otp_cust.reset();
        this.showLoader = false;
        this._cdr.writeCDR(this.startTime, this.category, this.otpValue, 'OTP', 'Success');//cdr_log
      }

    }).catch((err) => {
      if (err.message == 'Timeout has occurred') {
        this.sendotp = false;
        this.OTPShow = false;
        this.showLoader = false;
        this.timeoutErr = 'Timeout has occurred, Please try again';
        this._cdr.writeCDR(this.startTime, this.category, this.otpValue, 'OTP', 'Failure');//cdr_log
      }
      this.otp_cust.reset();
    });
  }


  async validateOtp() {
    let number =  await this._storeVal.getMSISDNData()
    // this.fmisidn = format.msisdn(this.existing_cust.get('msisdn').value)
    this.timeoutErr = '';
    if (!this.checktOtp) {
      this.showLoader = true;
      this.otpValue = this.otp_cust.value.enterotp;
      let msisdn = this._storeVal.getMSISDNData();
      this._otp.validateOTP(this.otpValue, this._storeVal.getOtpData(), msisdn).then(resp => {
        // this._cdr.writeCDR(this.startTime, this.category, this.otpValue, 'OTP', 'Failure');//cdr_log


        if (resp.status == 0) {
         // this.showLoader = false;
         this.showLoader = false;
         this.router.navigate(['/detailsofprofile']);
          // this.bnIdle.startWatching(1800).subscribe((res) => {
          //   if (res) {
          //     this.showLoader = false;
          //     this.router.navigate(['/detailsofprofile']);
          //     this.bnIdle.stopTimer();
          //   }
          // })
        

        }
        else {
          this.showLoader = false;
          this.message = resp.Valid;
          this.otp_cust.reset();
        }
      });
    } else {
      this.timeoutErr = 'Timeout has occurred, Please click on to Resend OTP';
    }

  }



  backtologin1() {
    this.OTPShow = true;
  }
  backtologin2() {
    this.sendotp = true;
  }


  backToLogin() {
    this.errorMsg = '';
    // this.category = 'Login';
    // this.hide_OTP = true;
  }


  resendotpClick() {
    this.hideee = true;
    this.sendotp = true;
  }
  

  // onSubmit(value: FormGroup) {
  //   this.showLoader = true;
  //   this.sessionErr = '';
  //   // const msisdn = this.existing_cust.get('msisdn').value;
  //   // let msisdn = format.msisdn(this.existing_cust.get('msisdn').value);
  //   const msisdn = this._storeVal.getMSISDNData();
  //   if (msisdn.length < 9) {
  //     this.showLoader = false;
  //     this.errorMsg = 'Please enter a valid MTN number.';
  //     this.existing_cust.reset();
  //   }
  //   this._profile.getProfileDetails(msisdn).then((resp) => {
  //     if (resp.status_code == 0) {

  //       //  this._storeVal.setEmailData(resp.customer_Profile.email);
  //       // this._storeVal.setAlternateNumberData(resp.customer_Profile.alternateNumber);
  //       // console.log('this._storeVal.setEmailData-' + resp.customer_Profile.email);
  //       // console.log('resp---' + JSON.stringify(resp));
  //       this._storeVal.setMSISDNData(msisdn);
  //       this._storeVal.setEmailData(resp.customer_Profile.email);
  //       this._storeVal.setAlternateNumberData(resp.customer_Profile.alternateNumber);
  //       this._storeVal.setFamilyNameData(resp.customer_Profile.familyName);
  //       this._storeVal.setFirstNameData(resp.customer_Profile.firstName);
  //       this._storeVal.setSubsTypeData(resp.subsType);
  //       this._storeVal.setActivationDate(resp.customer_Profile.activationDate);

  //       this.email = resp.customer_Profile.email;
  //       this.primaryNumber = resp.customer_Profile.primaryNumber;
  //       this._storeVal.setPrimaryMSISDNData(this._storeVal.getMSISDNData());
  //       this._storeVal.setPrimaryEmailData(this._storeVal.getEmailData());
  //       this._storeVal.setPrimaryAlternateNumberData(this._storeVal.getAlternateNumberData());
  //       this._storeVal.setPrimaryFamilyNameData(this._storeVal.getFamilyNameData());
  //       this._storeVal.setPrimaryFirstNameData(this._storeVal.getFirstNameData());
  //       this._storeVal.setPrimarySubsTypeData(this._storeVal.getSubsTypeData());

  //       if (resp.customer_Profile.primaryNumber != resp.customer_Profile.alternateNumber) {
  //         this.alternateNumber = resp.customer_Profile.alternateNumber;

  //       }
  //       this.otp_radio = this.primaryNumber.toString();

  //       this.OTPShow = false;
  //       this.showLoader = false;
  //       //  this.router.navigate(['dashboard'] ).then( nav => {}, (err) => {

  //       //       this.errorMsg = 'Please try again after sometime';
  //       //       this.existing_cust.reset();
  //       //     });
  //     }
  //     else {
  //       this.showLoader = false;
  //       this.errorMsg = 'Please Enter a valid MTN Number';
  //       this.existing_cust.reset();
  //     }
  //   }).catch((err) => {
  //     this.showLoader = false;
  //     this.errorMsg = 'Please Enter a valid MTN Number';
  //     this.existing_cust.reset();
  //   });
  // }
}
