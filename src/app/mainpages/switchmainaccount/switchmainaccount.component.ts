import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { Location } from '@angular/common';
//  import { format } from '../../utils/formatter';
import { Router } from '@angular/router';
import { config } from '../../config';
import { format } from '../../utils/formatter';
import { LoginForm } from '../../utils/interface';
import { ProfileService } from '../../plugins/profile';
import { otpService } from '../../plugins/otp';
import { StoreService } from '../../plugins/datastore';
import {AddAccountService} from '../../plugins/addaccount';
import { loginrcgService } from 'src/app/plugins/loginrcg';
import { SessionService } from 'src/app/plugins/session';
//import { Location } from '@angular/common';
import { ApiService } from 'src/app/plugins/commonAPI';

@Component({
    selector: 'app-switchmainaccount',
    templateUrl: './switchmainaccount.component.html',
    styleUrls: ['./switchmainaccount.component.scss']
  })
  export class SwitchmainAccountComponent implements OnInit{
    msisdn:any;
    public emailFlag: boolean=false;
    public errorMsg = '';
    primary_msisdn:any;
showLoader : boolean = false;
header = false;

nickname;
numberToSwitch;
parentNumber;

switched;
switchNumber;
email;
primaryNumber;
alternateNumber;
mainnumber;

    constructor(private fb: FormBuilder, private _profile: ProfileService, private _otp: otpService,
       private _storeVal: StoreService, private router: Router,private addAccount:AddAccountService,
       private location: Location, private loginService: loginrcgService,  private sessionService: SessionService, private apiService: ApiService) { 
        let oldNumber = JSON.parse(localStorage.getItem("manna")).SecondaryNumber;
        this.mainnumber = oldNumber;
        let params = this.router.getCurrentNavigation().extras.state
      
          this.numberToSwitch = params.switchNumber;
          this.parentNumber = params.parentNumber;
        
       }
    ngOnInit() {
        this.msisdn = this._storeVal.getMSISDNData();
       // console.log("primary accout in switch"+ this.msisdn);
        this.primary_msisdn = this._storeVal.getPrimaryMSISDNData();
       //console.log("primary msisdn for switch to main"+ this._storeVal.getPrimaryFirstNameData());
    }

    switchMainAccount(){

    }
    // switchMainAccount(){

    //     console.log("INSIDE Switch Main Acooount "+this._storeVal.getSecondaryAccountfalse());
    // this._storeVal.setMSISDNData(this._storeVal.getPrimaryMSISDNData());
    // this._storeVal.setEmailData(this._storeVal.getPrimaryEmailData());
    // this._storeVal.setAlternateNumberData(this._storeVal.getPrimaryAlternateNumberData());
    // this._storeVal.setFamilyNameData(this._storeVal.getPrimaryFamilyNameData());
    // this._storeVal.setFirstNameData(this._storeVal.getPrimaryFirstNameData());
    // this._storeVal.setSubsTypeData(this._storeVal.getPrimarySubsTypeData());
    // this._storeVal.setSecondaryAccount(false);
    //   this._storeVal.setSecondaryAccountfalse(true);

    
    // //this.sec_msisdn = this._storeVal.getSecondaryMSISDNData();
    // //console.log("secondary acnt dtls"+ this._storeVal.getSecondaryMSISDNData());
    // this.router.navigate(['dashboard']).then(nav => { }, (err) => {
    //   this.errorMsg = 'Please try again after sometime';
    //   //this.otp_cust.reset();
    // });

    // }
 backbuttons(){
    var urlPath = window.location.pathname.split("/");
    if (urlPath.length > 2){
     this.location.back();
    }else{
      this.router.navigate(['dashboard']);
    }
   }

   switchAccount() {

  
    this.showLoader = true;
    //get primary number here before clesring session

    let oldToken  ="";
    if(localStorage.getItem("switched") == "true" && this._storeVal.getToken()){
      // oldToken = sessionStorage.getItem("chakra");
      oldToken =  this._storeVal.getToken();
    }else{
      oldToken =  this._storeVal.getMainToken();
      //oldToken = sessionStorage.getItem("chakramain")
    }
  
      let oldNumber = JSON.parse(localStorage.getItem("manna")).SecondaryNumber;
      this.mainnumber = oldNumber;
      let loggednum = this._storeVal.getMSISDNData();

      
    let linkedMainnumbers;

    this.loginService.logout(loggednum).then(resp => {
      if (resp.status == 0) {
        sessionStorage.clear();
        localStorage.clear();
        this.createSession(oldNumber, oldToken)
        //  this.bnIdle.stopTimer();
        
      }
      this.showLoader = false;
    }).catch(err => {
      this.showLoader = false;
      Promise.reject(err);
    });
  }



  async createSession(primaryNumber, oldtoken){
    let msisdn = this.mainnumber;        
    let parentNumb = this.parentNumber;
    
    // sessionStorage.setItem("chakra", oldtoken);
    this._storeVal.setToken(oldtoken);
    console.log("Switching to this number boss", msisdn);
//validate the main account switch
//console.log("This isparent to main number", parentNumb);
await this.addAccount.validateSwitchMain(msisdn, parentNumb).then(data => {

//console.log("Bros this switch main data ", data);
if (data && data.StatusCode == 0) {
  if(data.token){
    this._storeVal.setToken(data.token);
    //sessionStorage.setItem("chakra",data.token );
  }
}
else {
  localStorage.clear();
  sessionStorage.clear();
  this.router.navigate(['login'])
  .then(nav => { }, (err) => {
    this.showLoader = false;
  
  });
}

});
    
   
    const profileResp = await this._profile.getProfileDetails(msisdn).catch((err) => {
      this.showLoader = false;
      this.errorMsg = 'Please Enter a valid MTN Number';

    });

   // console.log("Na the response " , profileResp)
    if ( profileResp && profileResp.status_code == 0) {
    

      this._storeVal.setMSISDNData(msisdn);
      this._storeVal.setEmailData(profileResp.customer_Profile.email);
      this._storeVal.setAlternateNumberData(profileResp.customer_Profile.alternateNumber);
      this._storeVal.setFamilyNameData(profileResp.customer_Profile.familyName);
      this._storeVal.setFirstNameData(profileResp.customer_Profile.firstName);
      this._storeVal.setSubsTypeData(profileResp.subsType);
      this._storeVal.setActivationDate(profileResp.customer_Profile.activationDate);

      this.email = profileResp.customer_Profile.email;
      this.primaryNumber = profileResp.customer_Profile.primaryNumber;
      this._storeVal.setPrimaryMSISDNData(this._storeVal.getMSISDNData());
      this._storeVal.setPrimaryEmailData(this._storeVal.getEmailData());
      this._storeVal.setPrimaryAlternateNumberData(this._storeVal.getAlternateNumberData());
      this._storeVal.setPrimaryFamilyNameData(this._storeVal.getFamilyNameData());
      this._storeVal.setPrimaryFirstNameData(this._storeVal.getFirstNameData());
      this._storeVal.setPrimarySubsTypeData(this._storeVal.getSubsTypeData());

      if (profileResp.customer_Profile.primaryNumber != profileResp.customer_Profile.alternateNumber) {
        this.alternateNumber = profileResp.customer_Profile.alternateNumber;

      }
  
      this.showLoader = false;
      //  this.router.navigate(['dashboard'] ).then( nav => {}, (err) => {

      //       this.errorMsg = 'Please try again after sometime';
      //       this.existing_cust.reset();
      //     });
    }
    else {
      this.showLoader = false;
      this.errorMsg = 'Please Enter a valid MTN Number';
   
    }
    
   const sessionResp = await this.sessionService.createSessionId(msisdn);

  // console.log("This is response got ", sessionResp);
   if (sessionResp.status == 0) {
     this.showLoader = false;
     this._storeVal.setSessionId(sessionResp.session_id);
     this._storeVal.setSessionStatus('valid');
     localStorage.setItem("switched", "false");
     if(localStorage.getItem("manna") === null){
      let result = {
        SecondaryNumber: msisdn,
        StartDate: "lap",
        NickName:	"stan"
       }
       
      localStorage.setItem("manna", JSON.stringify(result));
     }

       this.router.navigate(['dashboard'])
       .then(nav => { }, (err) => {
         this.showLoader = false;
         //  this.errorMsg = 'Please try again after sometime';
       });
     

   }
   else {
     this.showLoader = false;
     this.errorMsg = 'Unable to create session. Please try again after sometime';
   //  console.log("SESSION WAS NOT ABLE TO CREATE")
   }

  }
  }