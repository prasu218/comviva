import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { ApiService } from '../commonAPI';
import { StoreService } from '../datastore';

@Injectable()
export class loginrcgService {
    constructor(private _http: Http, private http: HttpClient,
        private apiService: ApiService, private storeVal: StoreService) { }

    getLoginRcgDetails(msisdn: string, voucher_code: string) {
        const LoginRcgDetailsURL = config.PLUGIN_URL + '/api/loginrcg';
        //console.log("LoginRcgDetailsURL" + LoginRcgDetailsURL);
        const payload = {
            msisdn: msisdn,
            voucher_code: voucher_code
        };
        //console.log("msisdn service" + msisdn);
        //console.log("voucher_code service" + voucher_code);

        return this._http.post(LoginRcgDetailsURL, payload).timeout(config.TIME_OUT)
            .toPromise().then(resp => {
                //console.log("service resp" + resp);
                return Promise.resolve(resp.json());
            }).catch(err => Promise.reject(err));
    }

 logout(msisdn: string) {
    let sessionId = this.storeVal.getSessionId();
    const logoutURL = '/api/logoutSessionStatus';
    const payload = {
        cust_id: msisdn,
        session_id: sessionId
    };
    return this.apiService.postWithSessionValidation(logoutURL, msisdn, sessionId, payload);
}

}
