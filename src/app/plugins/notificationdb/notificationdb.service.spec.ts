import { TestBed } from '@angular/core/testing';

import { NotificationdbService } from './notificationdb.service';

describe('NotificationdbService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotificationdbService = TestBed.get(NotificationdbService);
    expect(service).toBeTruthy();
  });
});
