import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { config } from '../config';
import { ApiService } from '../commonAPI';
import { StoreService } from '../datastore';

@Injectable({
  providedIn: 'root'
})

export class NotificationdbService {

  constructor(private _http: Http, private apiService: ApiService, private storeVal: StoreService) { }

  sessionId: any;
  notificationAPI(msisdn: string,a_msisdn:string, eMailId: string, ) {
    const payload = {
      msisdn: msisdn,
      a_msisdn:a_msisdn,
      eMailId: eMailId,
    };
    const notificationURL = '/api/notification';
    this.sessionId = this.storeVal.getSessionId();
    
    // return this._http.post(notificationURL, payload).timeout(config.TIME_OUT)
    //   .toPromise().then(resp => {
    //     return Promise.resolve(resp.json());
    //   }).catch(err => {
    //     return Promise.reject(err)
    //   });
    return this.apiService.postWithSessionValidation(notificationURL, msisdn, this.sessionId, payload)
  }

  getNotificationData(msisdn: string) {
    const payload = {
      cust_id: msisdn
    };
    const notificationURL = '/api/getnotificationtab';
    this.sessionId = this.storeVal.getSessionId();
    // return this._http.post(notificationURL, payload).timeout(config.TIME_OUT)
    //   .toPromise().then(resp => {
    //     return Promise.resolve(resp.json());
    //   }).catch(err => 
    //   {
    //     return Promise.reject(err)
    //   });
    return this.apiService.postWithSessionValidation(notificationURL, msisdn, this.sessionId, payload)
  }

  updateNotificationData(msisdn: string, flag: string) {
    const payload = {
      cust_id: msisdn,
      flag: flag,
    };
    const notificationURL = '/api/savenotificationtab';
    this.sessionId = this.storeVal.getSessionId();
    // return this._http.post(notificationURL, payload).timeout(config.TIME_OUT)
    //   .toPromise().then(resp => {
    //     return Promise.resolve(resp.json());
    //   }).catch(err => {
    //     return Promise.reject(err)
    //   });

    return this.apiService.postWithSessionValidation(notificationURL, msisdn, this.sessionId, payload)
  }
}
