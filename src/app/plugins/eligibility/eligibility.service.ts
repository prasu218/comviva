import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../commonAPI';
import { StoreService } from '../../plugins/datastore';

@Injectable()
export class EligibilityService {

  dataPlanDetails;
  buybundleplansDetails;

  constructor(private _http: Http, private apiService: ApiService, private _storeVal: StoreService) { }

  getEligibility(msisdn, eligibilityId, action) {
    //  let {correctId} = this.dataSanitization(eligibilityId,action);
    let sessionId = this._storeVal.getSessionId();
    const payload = {
      msisdn: msisdn.toString(),
      eligibilityId: eligibilityId
    };


    // console.log("cgetBuyBundleCatCIS");
    // console.log("msisdn---"+msisdn);
    const buybundleXtraTalkCatURL = '/api/getEligibility ';
    // return this._http.post(buybundleXtraTalkCatURL, payload).toPromise().then(resp => {      
    //   return Promise.resolve(resp.json());
    // }).catch((err) => {
    //   return Promise.reject(err);
    // });

    return this.apiService.postWithSessionValidation(buybundleXtraTalkCatURL, msisdn, sessionId, payload)
  }

  dataSanitization(eligibilityId, action) {
    let base = null;
    if (eligibilityId) base = eligibilityId;
    if (action) base = action;
    if (base == null) return { eligibilityId: null, action: null };
    let correctId = 'ECHK';
    let correctAction = 'RACT';
    base.split('_').forEach((element, index) => {
      if (index > 0) {
        correctId += '_' + element;
        correctAction += '_' + element;
      }
    });

    return { correctId, correctAction };

  }

}
