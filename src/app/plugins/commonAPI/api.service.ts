import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response, Headers } from '@angular/http';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { SessionService } from '../session';
import { Router } from '@angular/router';
import { StoreService } from '../datastore';

@Injectable()
export class ApiService {

  BASE_URL: string = config.PLUGIN_URL;
  token: any;
  constructor(private _http: Http, private http: HttpClient,
    private sessionService: SessionService, private router: Router,
    private storeService: StoreService) { }

    postWithSessionValidation(path: string, msisdn: string, sessionId: string, body: Object = {}) {
      let primaryMsisdn = this.storeService.getPrimaryMSISDNData();
      const validateSessionURL = '/api/getSessionStatus';
      const validateSessionPayload = {
        cust_id: primaryMsisdn,
        session_id: sessionId
      };
      let tokdat;
     // if(sessionStorage.getItem('chakra') != null){
       if(this.storeService.getToken() != null){
       // tokdat = sessionStorage.getItem('chakra');
       tokdat = this.storeService.getToken();
        const headers: Headers = new Headers();
        //if(path == '/api/linkedaccountlist'){
          headers.append('Authorization', 'Bearer ' + tokdat);
          headers.append('Content-Type', 'application/json');
    
        return this._http.post(this.BASE_URL + validateSessionURL, validateSessionPayload, {headers: headers}).toPromise().then(resp => {
          let validateSessionStatus = resp.json();
          if (validateSessionStatus.status == 0) {
           
            //if(sessionStorage.getItem('chakra') != null){
              if(this.storeService.getToken() != null){
             // this.token = sessionStorage.getItem('chakra');
             this.token = this.storeService.getToken();
            }
            else{
              this.token = '';
            }
            const headers: Headers = new Headers();
            //if(path == '/api/linkedaccountlist'){
              headers.append('Authorization', 'Bearer ' + this.token);
              headers.append('Content-Type', 'application/json');
            
            return this._http.post(this.BASE_URL + path, body, {headers: headers}).toPromise().then(resp => {
              return Promise.resolve(resp.json());
            }).catch(err => Promise.reject(err));
          } else {
            this.deleteSessionId(primaryMsisdn);
          }
          Promise.resolve();
        }).catch(err => {
          let error = err.json();
          if (error.status == 401) {
            this.deleteSessionId(primaryMsisdn);
          }
          Promise.reject(error);
        });
      }
    
      else{
        tokdat = '';
        localStorage.clear();
        sessionStorage.clear();
        this.router.navigateByUrl('/login');
      }
    }

    postWithoutSessionValidation(path: string, msisdn: string, body: Object = {}): Promise<any> {

      // if(sessionStorage.getItem('chakra') != null){
        if(this.storeService.getToken() != null){
          this.token = this.storeService.getToken();
        //this.token = sessionStorage.getItem('chakra');
        const headers: Headers = new Headers();
        //if(path == '/api/linkedaccountlist'){
          headers.append('Authorization', 'Bearer ' + this.token);
          headers.append('Content-Type', 'application/json');
        
        return this._http.post(this.BASE_URL + path, body, {headers: headers}).toPromise().then(resp => {
          return Promise.resolve(resp.json());
        }).catch(err => Promise.reject(err));
      }
      else{
        this.token = '';
        localStorage.clear();
        sessionStorage.clear();
        this.router.navigateByUrl('/login');
      }
    }
  
 deleteSessionId(msisdn: string) {
    let session_id = this.storeService.getSessionId();
    const logoutURL = '/api/logoutSessionStatus';
    const payload = {
      cust_id: msisdn,
      session_id: session_id
    };
    
    // if(sessionStorage.getItem('chakra') != null){
    //   this.token = sessionStorage.getItem('chakra');
    if(this.storeService.getToken() != null){
      this.token = this.storeService.getToken();

      const headers: Headers = new Headers();
      //if(path == '/api/linkedaccountlist'){
        headers.append('Authorization', 'Bearer ' + this.token);
        headers.append('Content-Type', 'application/json');
  
      return this._http.post(this.BASE_URL + logoutURL, payload, {headers: headers}).toPromise().then(resp => {
        let delResp = resp.json();
        if (delResp.status == 0) {
          let sessionStatus = this.storeService.getSessionStatus();
          if (sessionStatus == 'valid') {
            localStorage.clear();
            sessionStorage.clear();
            this.storeService.setSessionStatus('expired');
          }
          this.router.navigateByUrl('/login');
        }
      }).catch(err => {
        localStorage.clear();
        sessionStorage.clear();
        this.router.navigateByUrl('/login');
        return Promise.reject(err)});
    }
    
      else{
        this.token = '';
        localStorage.clear();
        sessionStorage.clear();
        this.router.navigateByUrl('/login');
      }
  }
  
  postRedirectSessionValidation(path: string, msisdn: string, sessionId: string, body: Object = {}) {
    let primaryMsisdn = this.storeService.getPrimaryMSISDNData();
    const validateSessionURL = '/api/getSessionStatus';
    const validateSessionPayload = {
      cust_id: primaryMsisdn,
      session_id: sessionId
    };
    let tokdat;
    // if(sessionStorage.getItem('chakra') != null){
    //   tokdat = sessionStorage.getItem('chakra');
    if(this.storeService.getToken() != null){
      tokdat = this.storeService.getToken();

      const headers: Headers = new Headers();
    //if(path == '/api/linkedaccountlist'){
      headers.append('Authorization', 'Bearer ' + tokdat);
      headers.append('Content-Type', 'application/json');
      headers.append('Accept', 'text/html');
    return this._http.post(this.BASE_URL + validateSessionURL, validateSessionPayload, {headers: headers}).toPromise().then(resp => {
      let validateSessionStatus = resp.json();
      if (validateSessionStatus.status == 0) {
        // if(sessionStorage.getItem('chakra')){
        //   this.token = sessionStorage.getItem('chakra');
        if(this.storeService.getToken() != null){
          this.token = this.storeService.getToken();
        }
        else{
          this.token = '';
          localStorage.clear();
          sessionStorage.clear();
          this.router.navigateByUrl('/login');
        }
        const headers: Headers = new Headers();
        //if(path == '/api/linkedaccountlist'){
          headers.append('Authorization', 'Bearer ' + this.token);
          headers.append('Content-Type', 'application/json');
        return this._http.post(this.BASE_URL + path, body, {headers: headers})
        .subscribe( (val) =>
          { 
            console.log("Bane this is response", val);
         document.getElementById("tat").innerHTML =  val["_body"];
         document.forms['paymentResponse'].submit();
         }  );
      } else {
        this.deleteSessionId(primaryMsisdn);
      }
      Promise.resolve();
    }).catch(err => {
      let error = err.json();
      if (error.status == 401) {
        this.deleteSessionId(primaryMsisdn);
      }
      Promise.reject(error);
    });
    }

    else{
      tokdat = '';
      localStorage.clear();
      sessionStorage.clear();
      this.router.navigateByUrl('/login');
    }
  }
}