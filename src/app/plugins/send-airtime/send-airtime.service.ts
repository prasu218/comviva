import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { StoreService } from '../datastore/datastore.service';
import { ApiService } from '../commonAPI';

@Injectable()
export class SendAirtimeService {
    sendairtimeObj: any = {}

    constructor(private _http: Http, private _storeVal: StoreService, private apiService: ApiService) { }

    sendairtime(cur_msisdn: string, amount: string, pin: string, msisdn: string) {
        let sessionId = this._storeVal.getSessionId();
        const payload = {
            cur_msisdn: cur_msisdn,
            amount: amount,
            pin: pin,
            msisdn: msisdn
        }
        const shareAirtimeURL = '/api/shareairtime';

        // return this._http.post(ShareAirtimeURL, payload).toPromise().then(resp => {
        //     this.sendairtimeObj = resp.json();
        //     return Promise.resolve(resp.json());
        // }).catch(err => Promise.reject(err));

        return this.apiService.postWithSessionValidation(shareAirtimeURL, msisdn, sessionId, payload)
    }
}


