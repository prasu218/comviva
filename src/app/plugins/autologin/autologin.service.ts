import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AutologinService {

  constructor(private _http: Http, private http: HttpClient) { }

  getMsisdnImsi() {
    const getMsisdnURL = config.PLUGIN_URL + '/api/getmsisdn';
    console.log("get headermsidnURL:: ", getMsisdnURL);
    return this._http.post(getMsisdnURL, null).toPromise().then(resp => {
      console.log("msisdn resp " + resp);
      return Promise.resolve(resp.json());
    }).catch(err => Promise.reject(err));
  }

}
