import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../commonAPI';
import { StoreService } from '../../plugins/datastore'

@Injectable()
export class AddAccountService {

    public msisdn: string;

    constructor(private _http: Http, private http: HttpClient, private apiService: ApiService, private _storeVal: StoreService) { }
    validateAccount(msisdn: string) {
        let sessionId = this._storeVal.getSessionId();
        const validateaccountDetailsURL = '/api/validateaccount';
        const payload = {
            msisdn: this._storeVal.getMSISDNData(),
            secmsisdn: msisdn.toString()
            //sec_msisdn: sec_msisdn.toString()
        };

        return this.apiService.postWithSessionValidation(validateaccountDetailsURL, msisdn, sessionId, payload)
    }

    linkAccount(msisdn: string, sec_msisdn: string, label: string) {
        let sessionId = this._storeVal.getSessionId();
        const addaccountDetailsURL = '/api/linkaccount';
        const payload = {
            msisdn: msisdn,
            sec_msisdn: sec_msisdn,
            label: label
        };

        //console.log("linking payloads" + JSON.stringify(payload));
        //return this._http.post(addaccountDetailsURL, payload).toPromise().then(resp => {
        //console.log("service resp" +resp);
        //return Promise.resolve(resp.json());
        //}).catch(err => Promise.reject(err));
        return this.apiService.postWithSessionValidation(addaccountDetailsURL, msisdn, sessionId, payload)
    }

    unlinkAccount(msisdn: string, sec_msisdn: string) {
        let sessionId = this._storeVal.getSessionId();
        const addaccountDetailsURL = '/api/unlinkaccount';
        const payload = {
            msisdn: msisdn.toString(),
            sec_msisdn: sec_msisdn.toString()
        };

        //console.log("linking payloads" + JSON.stringify(payload));
        //return this._http.post(addaccountDetailsURL, payload).toPromise().then(resp => {
        //console.log("service resp" +resp);
        //return Promise.resolve(resp.json());
        //}).catch(err => Promise.reject(err));
        return this.apiService.postWithSessionValidation(addaccountDetailsURL, msisdn, sessionId, payload)
    }

    getLinkedAccounts(msisdn: string) {
        let sessionId = this._storeVal.getSessionId();
        const linkedccountDetailsURL = '/api/linkedaccountlist';
        const payload = {
            msisdn: msisdn.toString()
        }
        //return this._http.post(linkedccountDetailsURL, payload).toPromise().then(resp => {
        //console.log("respppp"+resp);
        // return resp.json();
        // return Promise.resolve(resp.json());        
        //}).catch(err => Promise.reject(err));
        return this.apiService.postWithSessionValidation(linkedccountDetailsURL, msisdn, sessionId, payload)
    }

    getSwitchedLinkedAccounts(msisdn: string, tokdata) {
        let sessionId = this._storeVal.getSessionId();
        const linkedccountDetailsURL = '/api/linkedaccountswitchlist';
        const payload = {
            msisdn: msisdn.toString(),
            tokdata: tokdata.toString()
        }
      
        return this.apiService.postWithSessionValidation(linkedccountDetailsURL, msisdn, sessionId, payload)
    }

    async validateSwitchAccount(msisdn: string, switchnumber: string, logoutnumber: string) {
        //let sessionId = this._storeVal.getSessionId();
        const linkedccountDetailsURL = '/api/verifyaccountswitch';
  
        const payload = {
            msisdn: msisdn.toString(),
            switchnumber: switchnumber.toString(),
             signednumber: logoutnumber
        }
    
        return this.apiService.postWithoutSessionValidation(linkedccountDetailsURL, msisdn, payload)
    }

    validateSwitchMain(msisdn: string, switchnumber: string) {
      //  let sessionId = this._storeVal.getSessionId();
        const linkedccountDetailsURL = '/api/verifymainswitch';
        const payload = {
            msisdn: msisdn.toString(),
            switchnumber: switchnumber.toString()
        }
    
        return this.apiService.postWithoutSessionValidation(linkedccountDetailsURL, msisdn, payload)
    }
}
