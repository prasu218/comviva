import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http,Response} from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { checkBorrowAirBalance_Response, getBorrowAirTime_Response, checkBorrowEligibility_Response, } from '../../config';
import { ApiService } from '../commonAPI';
import { StoreService } from '../../plugins/datastore'

@Injectable()
export class rcgborrowService {

    public msisdn:string;
    public chkBorEligibilityResponse: any = checkBorrowEligibility_Response;
    public getBorrowAirTimeResponse: any = getBorrowAirTime_Response;
    public chkBorrowAirBalanceResponse: any = checkBorrowAirBalance_Response;

    constructor(private _http: Http,private http: HttpClient,private apiService: ApiService,private _storeVal: StoreService) { }
    //console.log("session ------",session);

    checkBorrowEligibility(msisdn: string) {
        let sessionId = this._storeVal.getSessionId();
        //console.log("session ------",sessionId);
        const payload = {
            msisdn: msisdn
        };
        const checkBorrowEligibiliytURL:string = '/api/checkBorrowEligibility';
        //  const checkBorrowEligibiliytURL = config.PLUGIN_URL + '/api/checkBorrowEligibility';
         //return this._http.post(checkBorrowEligibiliytURL,payload).timeout(config.TIME_OUT)
        // .toPromise().then(resp =>{
        //     console.log("resp eligibility" +resp)
        //     return resp.json();
       // })
        // .catch(err => Promise.reject(err));
        return this.apiService.postWithSessionValidation(checkBorrowEligibiliytURL,msisdn,sessionId,payload)
        }
        
    


    getBorrowAirTime(msisdn: string, serviceId: string) {
        let sessionId =this._storeVal.getSessionId();
        const payload = {
            msisdn: msisdn,
            serviceId: serviceId
        };
          const borrowAirtimeURL ='/api/getBorrowAirTime';
    //      return this._http.post(borrowAirtimeURL,payload).timeout(config.TIME_OUT)
    //    .toPromise().then(resp =>{
    //        return resp.json();
    //    })
    //    .catch(err => Promise.reject(err)); 
    return this.apiService.postWithSessionValidation(borrowAirtimeURL,msisdn,sessionId,payload)    
    }

    // checkBorrowAirBalance(msisdn: string) {
    //     const payload = {
    //         msisdn: msisdn
    //     };
    //     const checkBorrowAirBalanceURL = config.PLUGIN_URL + '/api/checkBorrowAirBalance';
    //     return Promise.resolve(this.chkBorrowAirBalanceResponse);
     
    // }


}