import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Jsonp, Headers } from '@angular/http';
import {   getState_Response, getLga_Response, serviceProvinceCentre_Response } from '../../config';
import { StoreService } from '../datastore';

@Injectable()
export class HelpService{
   
    public getStateResponse: any;
    public getLgaResponse: any ;
    public getServiceProvinceCentreResponse: any ;
    token: any;

    constructor(
      private _http: Http, private storeData: StoreService) {
        
      }
 
    

      
 getAllState(msisdn: string) {
        const payload = {
          msisdn: msisdn,
          query:"getState"
        };
        const getAllStateURL = config.PLUGIN_URL + '/api/getAllState';
        if(this.storeData.getToken() != null){
          this.token = this.storeData.getToken();
        }
        else{
          this.token = '';
        }
        const headers: Headers = new Headers();
        //if(path == '/api/linkedaccountlist'){
          headers.append('Authorization', 'Bearer ' + this.token);
          headers.append('Content-Type', 'application/json');
        return this._http.post(getAllStateURL, payload, {headers: headers}).timeout(config.TIME_OUT)

        .toPromise().then((resp) => {
         // return resp.json();
         return Promise.resolve(resp.json());
        }).catch(err => Promise.reject(err));
      }

      getAllLGAs(msisdn: string,state:string) {
   
        const payload = {
          msisdn: msisdn,
          state: state,
           query:"getLGAS"
        };
        const getAllLgaURL = config.PLUGIN_URL + '/api/getAllLGAs';
        if(this.storeData.getToken() != null){
          this.token = this.storeData.getToken();
        }
        else{
          this.token = '';
        }
        const headers: Headers = new Headers();
        //if(path == '/api/linkedaccountlist'){
          headers.append('Authorization', 'Bearer ' + this.token);
          headers.append('Content-Type', 'application/json');
        
        return this._http.post(getAllLgaURL, payload, {headers: headers}).timeout(config.TIME_OUT)
        .toPromise().then((resp) => {
          // return resp.json();
          return Promise.resolve(resp.json());
         }).catch(err => Promise.reject(err));
      }

      getServiceProvinceCenters(msisdn: string,state:string,lga:string){
      
        const payload = {
          msisdn: msisdn,
          state: state,
          lga:lga,
          query:"getServiceCenters"
        };
        const getServiceProvinceCentersURL = config.PLUGIN_URL + '/api/getServiceProvinceCenters';
        if(this.storeData.getToken() != null){
          this.token = this.storeData.getToken();
        }
        else{
          this.token = '';
        }
        const headers: Headers = new Headers();
        //if(path == '/api/linkedaccountlist'){
          headers.append('Authorization', 'Bearer ' + this.token);
          headers.append('Content-Type', 'application/json');
        return this._http.post(getServiceProvinceCentersURL, payload, {headers: headers}).timeout(config.TIME_OUT)
        .toPromise().then((resp) => {
          // return resp.json();
          return Promise.resolve(resp.json());
         }).catch(err => Promise.reject(err));
      }

getUserLocationAddress(latitude, longitude) {
      const apiURL = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCNh7O2WN1enMTcDry9z8SNojRzIQiv93M&latlng="+latitude+","+longitude+"&sensor=false";
      if(this.storeData.getToken() != null){
        this.token = this.storeData.getToken();
      }
      else{
        this.token = '';
      }
      const headers: Headers = new Headers();
      //if(path == '/api/linkedaccountlist'){
        headers.append('Authorization', 'Bearer ' + this.token);
        headers.append('Content-Type', 'application/json');
      return this._http.get(apiURL).timeout(config.TIME_OUT)
      .toPromise().then(resp => {
        return Promise.resolve(resp.json());
      }).catch(err => {
        return Promise.reject(err)
      });
    }
      

      

      

}