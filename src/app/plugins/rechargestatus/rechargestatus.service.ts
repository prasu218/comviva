import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Jsonp } from '@angular/http';
import { Common_Strings } from '../../strings/strings';
import { StoreService } from '../datastore/datastore.service';
import { ApiService } from '../commonAPI';

@Injectable()
export class RechargestatusService {
 

  constructor(private _http: Http,private _ds: StoreService,private apiService: ApiService) { }
      getRechargeDetails(msisdn:string,voucherCode: string) {
      let sessionId = this._ds.getSessionId();
        //let method: string;
        const payload = {
          msisdn: msisdn,
          voucherCode : voucherCode
        };
      const rechargeCardStatusURL:string ='/api/getRechargeDetails';
      // return this._http.post(rechargeCardStatusURL, payload)
      // .toPromise().then(resp => {
      //   return resp.json();
      // }).catch(err => Promise.reject(err));
      return this.apiService.postWithSessionValidation(rechargeCardStatusURL,msisdn,sessionId,payload);
      }




}