import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../commonAPI';
import { StoreService } from '../../plugins/datastore'

@Injectable()
export class AccountHistoryService {

    public msisdn: string;

    constructor(private _http: Http, private http: HttpClient, private apiService: ApiService, private _storeVal: StoreService) { }

    accountHistory(msisdn: string, historyType: string, startDate, endDate) {

        const accountHistoryURL = '/api/accountHistory';
        let sessionId = this._storeVal.getSessionId();
        const payload = {
            msisdn: msisdn,
            historyType: historyType,
            startDate: startDate,
            endDate: endDate,
        };

        //return this._http.post(accountHistoryURL, payload).toPromise().then(resp => {
        // console.log("service resp" + resp);
        //return Promise.resolve(resp.json());
        //}).catch(err => Promise.reject(err));
        
        return this.apiService.postWithSessionValidation(accountHistoryURL, msisdn, sessionId, payload)
    }

}
