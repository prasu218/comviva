import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../commonAPI';
import { StoreService } from '../../plugins/datastore';

@Injectable()
export class DebitcardService {

    public msisdn: string;

    constructor(private _http: Http, private apiService: ApiService, private _storeVal: StoreService) { }

    getDebitcardDetails(msisdn: string, self_amount: string, email: string, firstName: string, mtnepg_cust_mobile: string) {
        const debitcardDetailsURL = '/api/debitcard';
        let sessionId = this._storeVal.getSessionId();
        const payload = {
            msisdn: msisdn,
            self_amount: self_amount,
            email: email,
            firstName: firstName,
            mtnepg_cust_mobile
        };

        // return this._http.post(DebitcardDetailsURL, payload)
        //     .toPromise().then(resp => {
        //         console.log("service resp" + resp);
        //         return Promise.resolve(resp.json());
        //     }).catch(err => Promise.reject(err));

        return this.apiService.postWithSessionValidation(debitcardDetailsURL, msisdn, sessionId, payload)
    }
}
