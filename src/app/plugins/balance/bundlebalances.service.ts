import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { dataBundleDACodes } from '../balance/index';
import { dataBonusBundleDACodes } from '../balance/index';
import { dataBalanceConversionRate } from '../balance/index';
import { other_Balance_Latest_DAID } from '../balance/index';
import { OTHER_BALANCE_CATEGORIES_LIST } from '../balance/index';
import { SampleJSON } from '../balance/index';
import { OTHER_BALANCE_CATEGORIES_LIST_DETAIL_SPECIFIC } from '../balance/index';
import { OTHER_BALANCE_CATEGORIES_LIST_DETAIL_EXCLUDED } from '../balance/index';
import { Response } from '@angular/http';
import { Common_Strings } from '../../strings/strings';
import { SubscriptionService } from '../subscription/subscription.service';
import { ApiService } from '../commonAPI';
import { StoreService } from '../datastore';
import { SessionService } from '../session';

@Injectable()
export class BundleBalanceService {
    bundleObj: any = {};
    balanceObj: any = {};
    subtype: string;

    public bundleName: any;
    public bundleType: any;
    public bundleExpiry: any;
    public bundleValue: any;

    public dataBalance: any = 0;
    public dataBonusBalance: any = 0;
    public otherDataBalance: any = []; //Bundle Name, Bundle Type, Expiry, Value
    sessionId: any;

    exp_date_ID_Bonus = {
        181: [73, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97],
        195: [118, 119, 120, 121],
        194: [115, 116, 117, 122],
        223: [115, 116, 117, 122],
        224: [115, 116, 117, 122],
        129: [153, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204],
        155: [193, 153, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 209, 210, 211, 212, 213],
        191: [123, 192, 152, 193, 153, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 209, 210, 211, 212, 213],
        222: [191, 123, 192, 152, 193, 153, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 209, 210, 211, 212, 213],
        180: [191, 123, 192, 152, 193, 153, 194, 195, 196, 197, 198, 209, 210, 211, 212, 213]
    };

    constructor(private _http: Http, private _https: HttpClientModule,
        private subscriptionService: SubscriptionService, private sessionService: SessionService,
        private storeVal: StoreService, private apiService: ApiService) { }

    // API Call for Data Bundle Balance
    getBundleBalanceAPI(msisdn: string) {

        const balanceDetailsURL = '/api/bundle';
        const payload = {
            msisdn: msisdn,
            //msisdn:msisdn
        };
        this.sessionId = this.storeVal.getSessionId();
        //         return this._http.post(balanceDetailsURL, payload).toPromise().then(resp => {
        //             const response = resp.json();
        //             return Promise.resolve(response);
        //         }).catch(err => Promise.reject(err));

        return this.apiService.postWithSessionValidation(balanceDetailsURL, msisdn, this.sessionId, payload)
    }
    // Fetch and Calculate the Main Data Bundle Balance
    // 0 -main Data balance , 1 - Bonus data balancve
    getDataBalance(msisdn: string) {
        this.sessionId = this.storeVal.getSessionId();
        let primaryMsisdn = this.storeVal.getPrimaryMSISDNData();
        return this.sessionService.validateSession(primaryMsisdn, this.sessionId).then(resp => {
            let validateSessionStatus = resp;
            if (validateSessionStatus.status == 0) {
                return this.getBundleBalanceAPI(msisdn).then((bundleObj) => {
                    let balanceBundle = 0;
                    let balanceBundleBonus = 0;
                    if (bundleObj.da_result && (bundleObj.status_code == "0" || bundleObj.status_code == 0)) {
                        //Below for Main Data Balance
                        var ind = 0;
                        for (var i = 0; i < bundleObj.da_result.length && ind < dataBundleDACodes.result.length; i++) {
                            if (Number(bundleObj.da_result[i].daid) == Number(dataBundleDACodes.result[ind].daID)) {
                                 dataBundleDACodes.result[ind].daBalance = 0;
                                dataBundleDACodes.result[ind].daBalance = Number(dataBundleDACodes.result[ind].daBalance) + Number(bundleObj.da_result[i].balance);
                                dataBundleDACodes.result[ind].expDate = bundleObj.da_result[i].expiryDate;
                            } else if ((Number(bundleObj.da_result[i].daid)) > Number(dataBundleDACodes.result[ind].daID)) {
                                ind++;
                                i = i - 1;
                            }
                        }
                        for (var a = 0; a < dataBundleDACodes.result.length; a++) {
                            balanceBundle = Number(balanceBundle) + (dataBundleDACodes.result[a].daBalance);
                        }
                        this.dataBalance = this.convertData(balanceBundle);

                        //Below for Data Bonus Balance
                        var key = 0;
                        for (var i = 0; i < bundleObj.da_result.length && key < dataBonusBundleDACodes.result.length; i++) {

                            if (Number(bundleObj.da_result[i].daid) == Number(dataBonusBundleDACodes.result[key].daID)) {
                                dataBonusBundleDACodes.result[key].daBalance = Number(dataBonusBundleDACodes.result[key].daBalance) + Number(bundleObj.da_result[i].balance);
                                dataBonusBundleDACodes.result[key].expDate = bundleObj.da_result[i].expiryDate;
                            } else if ((Number(bundleObj.da_result[i].daid)) > Number(dataBonusBundleDACodes.result[key].daID)) {
                                key++;
                                i = i - 1;
                            }
                        }
                        for (var a = 0; a < dataBonusBundleDACodes.result.length; a++) {
                     //  balanceBundleBonus = Number(balanceBundleBonus) + Number(dataBonusBundleDACodes.result[a].daBalance);
                     balanceBundleBonus = Number(balanceBundleBonus) + this.addzeros(Number(dataBonusBundleDACodes.result[a].daBalance));
                        }
                        this.dataBonusBalance = this.convertData(balanceBundleBonus);
                    }

                    return Promise.resolve([this.dataBalance, this.dataBonusBalance]);
                }).catch(err => {
                    return Promise.resolve([this.dataBalance, this.dataBonusBalance]);
                });
            } else {
                this.apiService.deleteSessionId(msisdn);
            }
        }).catch(err => {
            let error = err.json();
            if (error.status == 401) {
                this.apiService.deleteSessionId(msisdn);
            }
        });

    }


    // Fetch and Calculate the Bonus Data Bundle Balance
    convertData(balanceInKobo: any) {
        let balnceInGB, converted_dataBalance;
        let balanceInMB = Number(balanceInKobo / dataBalanceConversionRate).toFixed(2);
        if (Number(balanceInMB) >= 1024) {
            balnceInGB = Number(Number(balanceInMB) / 1024).toFixed(2);
            converted_dataBalance = balnceInGB + " GB";
        }
        else {
            converted_dataBalance = balanceInMB + " MB";
        }

        return converted_dataBalance;

    }



    // Fetch and Calculate the Other Data Bundle Balance       shruthi work
    getOtherDataBalance(msisdn: string) {
        let dataExpiry;
        //let other_Balance_Calculated = this.getOtherBalanceCalculated(msisdn);
        let other_Balance_Calculated: any;
        let other_Balance_Category = OTHER_BALANCE_CATEGORIES_LIST_DETAIL_SPECIFIC;
        let dataObj: any;

        this.sessionId = this.storeVal.getSessionId();
        let primaryMsisdn = this.storeVal.getPrimaryMSISDNData();
        return this.sessionService.validateSession(primaryMsisdn, this.sessionId).then(resp => {
            let validateSessionStatus = resp;
            if (validateSessionStatus.status == 0) {
                return this.getOtherBalanceCalculated(msisdn).then(other_Balance_Calculated => {
                    for (var j = 0; j < other_Balance_Calculated.result.length; j++) {
                        let new_balance = Number(other_Balance_Calculated.result[j].daBalance) / Number(other_Balance_Calculated.result[j].Con_Rate);
                        let new_balance_unit = "";
                        //Data Balances
                        if (new_balance > 0) {
                            if (new_balance > 0 && other_Balance_Calculated.result[j].unit == "MB") {
                                if (Number(new_balance) >= 1024) {
                                    //new_balance = parseFloat(new_balance / 1024).toFixed(2);
                                    new_balance = Number(new_balance / 1024);
                                    new_balance = Math.round(new_balance * 100) / 100;
                                    new_balance_unit = "GB";
                                }
                                else {
                                    //new_balance = parseFloat(new_balance).toFixed(2);
                                    new_balance = parseFloat((new_balance / 1024).toFixed(2));
                                    new_balance = Number(Number(new_balance / 1024).toFixed(2));
                                    new_balance = this.addzeros(Math.round(new_balance * 100) / 100);
                                   new_balance_unit = "MB";
                                }
                                dataExpiry = other_Balance_Calculated.result[j].expDate;
                                if (dataExpiry) {
                                    dataExpiry = this.parseDate(other_Balance_Calculated.result[j].expDate)
                                } else {
                                    dataExpiry = this.getExpiryDate(msisdn, other_Balance_Calculated.result[j].daID, other_Balance_Calculated.result[j].daName);
                                }
                             new_balance = this.addzeros(new_balance).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                                //Bundle Name, Bundle Type, Expiry, Value
                                dataObj = {
                                    bundleName: other_Balance_Calculated.result[j].daName,
                                    bundleType: other_Balance_Calculated.result[j].Category,
                                    bundleExpiry: dataExpiry,
                                    bundleValue: new_balance + new_balance_unit

                                };
                            }

                            //Airtime Balances
                            if (new_balance > 0 && other_Balance_Calculated.result[j].unit == "NGN") {
                                //new_balance = parseInt(new_balance);
                                new_balance = new_balance;
                                new_balance = this.addzeros(Math.round(new_balance * 100) / 100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                new_balance_unit = Common_Strings.OPCO_CURRENCY;
                                // dataExpiry = other_Balance_Calculated.result[j].expDate;
                                // if (dataExpiry) {
                                //     dataExpiry = this.parseDate(other_Balance_Calculated.result[j].expDate)
                                // } else {
                                //     dataExpiry = this.getExpiryDate(msisdn, other_Balance_Calculated.result[j].daID, other_Balance_Calculated.result[j].daName);
                                // }
                                //Bundle Name, Bundle Type, Expiry, Value
                                dataObj = {
                                    bundleName: other_Balance_Calculated.result[j].daName,
                                    bundleType: other_Balance_Calculated.result[j].Category,
                                    bundleExpiry: this.parseDate(other_Balance_Calculated.result[j].expDate),
                                    bundleValue: new_balance_unit + new_balance

                                };
                            }

                            //Mins/Voice Balances
                            if (new_balance > 0 && other_Balance_Calculated.result[j].unit == "MIN") {
                                //new_balance = parseInt(new_balance);
                                new_balance = new_balance;
                                new_balance = Math.round(new_balance * 100) / 100;
                                new_balance_unit = " Min";
                                if (new_balance > 1) {
                                    new_balance_unit = " Mins";
                                }
                                // dataExpiry = other_Balance_Calculated.result[j].expDate;
                                // if (dataExpiry) {
                                //     dataExpiry = this.parseDate(other_Balance_Calculated.result[j].expDate)
                                // } else {
                                //     dataExpiry = this.getExpiryDate(msisdn, other_Balance_Calculated.result[j].daID, other_Balance_Calculated.result[j].daName);
                                // }
                                //Bundle Name, Bundle Type, Expiry, Value
                                dataObj = {
                                    bundleName: other_Balance_Calculated.result[j].daName,
                                    bundleType: other_Balance_Calculated.result[j].Category,
                                    bundleExpiry: this.parseDate(other_Balance_Calculated.result[j].expDate),
                                    bundleValue: new_balance + new_balance_unit

                                };
                            }

                            if (new_balance > 0 && other_Balance_Calculated.result[j].unit == "SMS") {
                                //new_balance = parseInt(new_balance);
                                new_balance = new_balance;
                                new_balance = this.addzeros(Math.round(new_balance * 100) / 100);
                                new_balance_unit = " SMS";
                                // dataExpiry = other_Balance_Calculated.result[j].expDate;
                                // if (dataExpiry) {
                                //     dataExpiry = this.parseDate(other_Balance_Calculated.result[j].expDate)
                                // } else {
                                //     dataExpiry = this.getExpiryDate(msisdn, other_Balance_Calculated.result[j].daID, other_Balance_Calculated.result[j].daName);
                                // }
                                //Bundle Name, Bundle Type, Expiry, Value
                                dataObj = {
                                    bundleName: other_Balance_Calculated.result[j].daName,
                                    bundleType: other_Balance_Calculated.result[j].Category,
                                    bundleExpiry: this.parseDate(other_Balance_Calculated.result[j].expDate),
                                    bundleValue: new_balance + new_balance_unit

                                };
                            }


                            this.otherDataBalance.push(dataObj);
                            other_Balance_Category = this.otherDataBalance;

                            this.otherDataBalance.forEach((element, index) => {
                            });

                        }
                    }
                    return Promise.resolve([other_Balance_Category, other_Balance_Category]);
                    //return Promise.resolve(other_Balance_Calculated);
                })

                    .catch(err => {
                        return Promise.resolve(other_Balance_Calculated);
                        //return Promise.resolve([other_Balance_Calculated,other_Balance_Calculated]);
                    });
            } else {
                this.apiService.deleteSessionId(msisdn);
            }
        }).catch(err => {
            let error = err.json();
            if (error.status == 401) {
                this.apiService.deleteSessionId(msisdn);
            }
        });

    }

    getExpiryDate(msisdn, daID, bundleName) {
        let expiry_Date;
        this.sessionId = this.storeVal.getSessionId();
        let primaryMsisdn = this.storeVal.getPrimaryMSISDNData();
        return this.sessionService.validateSession(primaryMsisdn, this.sessionId).then(resp => {
            let validateSessionStatus = resp;
            if (validateSessionStatus.status == 0) {
                this.subscriptionService.getMySubscriptionList(msisdn).then(
                    resp => {
                        if (resp) {
                            if (resp.ResponseData) {
                                if (resp.ResponseData.products) {
                                    let ProductDetails = resp.ResponseData.products.ProductDetails;
                                    let exp_date_ID_Bonus_list = this.exp_date_ID_Bonus[daID];
                                    var flag = 0;
                                    if (exp_date_ID_Bonus_list) {
                                        for (var i = 0; i < exp_date_ID_Bonus_list.length; i++) {
                                            for (var j = 0; j < ProductDetails.length; j++) {
                                                //widget.logWrite(7,"{findExpiryDateBonus}"+exp_date_ID_list[i]);
                                                //widget.logWrite(7,"{findExpiryDateBonus}"+ProductDetails[j].ProductID);
                                                if (Number(exp_date_ID_Bonus_list[i]) == Number(ProductDetails[j].ProductID)) {
                                                    expiry_Date = ProductDetails[j].ExpiryDate;
                                                    expiry_Date = expiry_Date.slice(0, 10);
                                                    expiry_Date = expiry_Date.split("-").reverse().join("/");
                                                    flag = 1;
                                                    this.replaceExpiryDate(bundleName, expiry_Date);
                                                    break;
                                                }
                                            }
                                            if (flag == 1)
                                                break;
                                        }
                                    } else {
                                        expiry_Date = "N.A";
                                    }
                                }
                            }
                        }
                        // return expiry_Date;
                    }).catch(err => Promise.reject(err));
            } else {
                this.apiService.deleteSessionId(msisdn);
            }
        }).catch(err => {
            let error = err.json();
            if (error.status == 401) {
                this.apiService.deleteSessionId(msisdn);
            }
        });

    }

    replaceExpiryDate(bundleName, expiryDate) {
        this.otherDataBalance.filter(dataObj => {
            if (dataObj.bundleName == bundleName) {
                dataObj.bundleExpiry = expiryDate;
            }
        })
    }

    getOtherBalanceCalculated(msisdn: string) {
        //var jsonObj = this.bundleObj;
        return this.getBundleBalanceAPI(msisdn).then((jsonObj) => {
            if (jsonObj && Number(jsonObj.status_code) == 0) {
                if (jsonObj && jsonObj.da_result) {
                    if (jsonObj.status_code == 0) {
                        var ind = 0;
                        for (var i = 0; (i < jsonObj.da_result.length) && (ind < other_Balance_Latest_DAID.result.length); i++) {
                            if (Number(jsonObj.da_result[i].daid) == Number(other_Balance_Latest_DAID.result[ind].daID)) {
                              other_Balance_Latest_DAID.result[ind].daBalance = "0";
                               let otherBalnc = Number(other_Balance_Latest_DAID.result[ind].daBalance) + Number(jsonObj.da_result[i].balance);
                                other_Balance_Latest_DAID.result[ind].daBalance = otherBalnc.toString(); 
                                other_Balance_Latest_DAID.result[ind].expDate = jsonObj.da_result[i].expiryDate;
                            } else if ((Number(jsonObj.da_result[i].daid)) > Number(other_Balance_Latest_DAID.result[ind].daID)) {
                                ind++;
                                i = i - 1;
                            }
                        }
                    }
                }
                return Promise.resolve(other_Balance_Latest_DAID);
            }
        })
            .catch(err => {
                return Promise.resolve(other_Balance_Latest_DAID);
            });
    }


    parseDate(date) {
        var modifiedDate = "";
        var dateFormat = 1;
        var dateSeperator = "/";

        var yyyy = "";
        var mm = "";
        var dd = "";

        if (date) {

            date = date.replace(/-/g, "/");
            date = date.replace(/:/g, "/");

            if (date.indexOf("/") < 3) {
                yyyy = date.slice(6, 10);
                mm = date.slice(3, 5);
                dd = date.slice(0, 2);
            }
            else {
                yyyy = date.slice(0, 4);
                mm = date.slice(5, 7);
                dd = date.slice(8, 10);
            }

            if (dateFormat == 1) { // dd/mm/yyy
                modifiedDate = dd + dateSeperator + mm + dateSeperator + yyyy;
            }
            else if (dateFormat == 2) { // mm/dd/yyy
                modifiedDate = mm + dateSeperator + dd + dateSeperator + yyyy;
            }
            else if (dateFormat == 3) { // yyyy/mm/dd
                modifiedDate = yyyy + dateSeperator + mm + dateSeperator + dd;
            }
        }
        else {
            modifiedDate = Common_Strings.STR_NA;
        }

        return modifiedDate;
    }
   addzeros(num){
        return num.toLocaleString("en", {useGrouping: false, minimumFractionDigits: 2});

    }
}