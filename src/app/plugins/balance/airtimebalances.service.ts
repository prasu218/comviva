import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Jsonp } from '@angular/http';
import { Response } from '@angular/http';
import { BundleBalanceService } from '../balance/bundlebalances.service';
import { airtimeBonusDACodes } from '../balance/index';
import { Common_Strings } from '../../strings/strings';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from '../commonAPI';
import { StoreService } from '../datastore';
import { SessionService } from '../session';

//import {CommonService} from '../common';


@Injectable()
export class BalanceService {
    bundleObj: any = {};
    balanceObj: any = {};
    planObj: any = {};
    subtype: string;

    public airtimeBalance: any;
    public airtimeBonusBalance: any;
    public creditLimitBalance: any;
    public creditLimitAssignedBalance: any;
    public outstandingBalance: any;
    public tariffPlanName: any;
    sessionId: any;

    constructor(private _http: Http, public _bundleBalance: BundleBalanceService,
        private _https: HttpClientModule, private sessionService: SessionService,
        private storeVal: StoreService, private apiService: ApiService) { }


    getBalanceAPI(msisdn: string) {

        const balanceDetailsURL = '/api/balance';
        const payload = {
            msisdn: msisdn
        };
        this.sessionId = this.storeVal.getSessionId();
        //         return this._http.post(balanceDetailsURL, payload).toPromise().then(resp => {
        
        //             //this.bundleObj = resp.json();
        //             const response = resp.json();
        //             return Promise.resolve(response);
        //         }).catch(err => Promise.reject(err));
        return this.apiService.postWithSessionValidation(balanceDetailsURL, msisdn, this.sessionId, payload)
    }



    getCreditBalanceAPI(msisdn: string) {

        const balanceDetailsURL = '/api/getCreditBalance';
        const payload = {
            msisdn: msisdn
        };

        //return this._http.post(balanceDetailsURL, payload, { withCredentials: true }).timeout(config.TIME_OUT)
        this.sessionId = this.storeVal.getSessionId();
        //         return this._http.post(balanceDetailsURL, payload).toPromise().then(resp => {
        //             //this.bundleObj = resp.json();
        //             const response = resp.json();
        //             return Promise.resolve(response);
        //         }).catch(err => Promise.reject(err));
        return this.apiService.postWithSessionValidation(balanceDetailsURL, msisdn, this.sessionId, payload)

    }

    // subtype 1 - Prepaid Airtime Balance, 2 - Credit Limit Balance
    // position 0 - airtime/credit balance , 1 - tarriff plan name
    getBalance(msisdn: string, subtype: string) {

        // let balanceObj = this.getBalanceAPI(msisdn);
        // this.getBalanceAPI(msisdn);
        this.sessionId = this.storeVal.getSessionId();
        let primaryMsisdn = this.storeVal.getPrimaryMSISDNData();
        return this.sessionService.validateSession(primaryMsisdn, this.sessionId).then(resp => {
            let validateSessionStatus = resp;
            if (validateSessionStatus.status == 0) {
                return this.getBalanceAPI(msisdn).then((balanceObj) => {
                    let balance = 0;
                    if (balanceObj) {
                        if (balanceObj.result) {
                            let jsonResult = balanceObj.result;

                            //After balance api, using SC to fetch planname form DB
                            return this.getPlanNameDB(msisdn, jsonResult[0].planName).then(resp => {
                                if (resp.status == '0' && resp.prodInfo) {

                                    this.tariffPlanName = resp.prodInfo[0].NAME;

                                } else {
                                    this.tariffPlanName = Common_Strings.STR_NOTAVAILBLE;

                                }

                                if (subtype == "2") {
                                    if (jsonResult) {
                                        if (Number(jsonResult[0].planName) == 402) {
                                            if (jsonResult[0].Amount) {
                                                balance = Number(jsonResult[0].Amount)
                                            }
                                            if (jsonResult[0].Amount1) {
                                                this.creditLimitBalance = Number(jsonResult[0].Amount1)
                                            }
                                            if (jsonResult[0].Amount4) {
                                                balance = balance + Number(jsonResult[0].Amount4)
                                                this.creditLimitBalance = this.creditLimitBalance + Number(jsonResult[0].Amount4)
                                            }
                                        }
                                        else if (Number(jsonResult[0].planName) == 400 || Number(jsonResult[0].planName) == 401 || Number(jsonResult[0].planName) == 410) {
                                            if (jsonResult[0].Amount1) {
                                                balance = balance + Number(jsonResult[0].Amount1)
                                                this.creditLimitBalance = Number(jsonResult[0].Amount1)
                                            }
                                            if (jsonResult[0].Amount4) {
                                                this.creditLimitBalance = this.creditLimitBalance + Number(jsonResult[0].Amount4)
                                            }
                                        }
                                        else {
                                            this.creditLimitBalance = 0;
                                            if (jsonResult[0].Amount) {
                                                balance = Number(jsonResult[0].Amount)
                                            }
                                            if (jsonResult[0].Amount1) {
                                                balance = balance + Number(jsonResult[0].Amount1)
                                                this.creditLimitBalance = Number(jsonResult[0].Amount1)
                                            }
                                            if (jsonResult[0].Amount2) {
                                                balance = balance + Number(jsonResult[0].Amount2)
                                            }
                                            if (jsonResult[0].Amount3) {
                                                balance = balance + Number(jsonResult[0].Amount3)
                                            }
                                            if (jsonResult[0].Amount4) {
                                                this.creditLimitBalance = this.creditLimitBalance + Number(jsonResult[0].Amount4)
                                            }
                                        }
                                    }
                                     this.airtimeBalance = Common_Strings.OPCO_CURRENCY + this.addzeros(balance).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    this.creditLimitBalance = Common_Strings.OPCO_CURRENCY + this.addzeros(this.creditLimitBalance).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    //  this.creditLimitBalance = Number(balance.toFixed(2));
                                    return [this.airtimeBalance, this.tariffPlanName, this.creditLimitBalance];
                                }
                                else {

                                    balance = Number(balanceObj.result[0].Amount);
                                    balance = this.addzeros(balance);
                                    if (jsonResult) {
                                        if (Number(jsonResult[0].planName) == 402) {
                                            if (jsonResult[0].Amount4) {
                                                balance = balance + Number(jsonResult[0].Amount4)
                                            }

                                        }
                                    }
                                }

                                this.airtimeBalance = Number(balance).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                return Promise.resolve([Common_Strings.OPCO_CURRENCY + this.airtimeBalance, this.tariffPlanName]);

                            })


                        }
                    }
                }).catch((err) => {
                    return Promise.resolve([Common_Strings.OPCO_CURRENCY + 0, "N.A"]);
                });
            } else {
                this.apiService.deleteSessionId(msisdn);
            }
        }).catch(err => {
            let error = err.json();
            if (error.status == 401) {
                this.apiService.deleteSessionId(msisdn);
            }
        });
    }

    //To Fctch the Plan name form Database
    getPlanNameDB(msisdn: string, planName: string) {
        const payload = {
            msisdn: msisdn,
            planName: planName
        };

        const getcheckPlanandvaluesURL = '/api/checkPlanandvalues';
        //return this._http.post(getcheckPlanandvaluesURL, payload).timeout(config.TIME_OUT)
        this.sessionId = this.storeVal.getSessionId();
        //         return this._http.post(getcheckPlanandvaluesURL, payload)
        //             .toPromise().then((resp) => {
        //                 //this.planObj = resp.json();
        //                 return Promise.resolve(resp.json());
        //             }).catch(err => Promise.reject(err));

        return this.apiService.postWithSessionValidation(getcheckPlanandvaluesURL, msisdn, this.sessionId, payload)
    }

    //To Fetch the Airtime Bonus applicable for Prepaid Custoemrs
    getAirtimeBonusBalance(msisdn: string) {
        //this._bundleBalance.bundleObj = this._bundleBalance.getBundleBalanceAPI(msisdn);
        this.sessionId = this.storeVal.getSessionId();
        let primaryMsisdn = this.storeVal.getPrimaryMSISDNData();
        return this.sessionService.validateSession(primaryMsisdn, this.sessionId).then(resp => {
            let validateSessionStatus = resp;
            if (validateSessionStatus.status == 0) {
                return this._bundleBalance.getBundleBalanceAPI(msisdn).then(resp => {
                    let bonusAirtime = 0;
                    this.bundleObj = resp;
                    if (this.bundleObj && (this.bundleObj.status_code == "0" || this.bundleObj.status_code == 0) && this.bundleObj.da_result) {
                        for (var i = 0; i < this.bundleObj.da_result.length; i++) {
                            for (var k = 0; k < airtimeBonusDACodes.result.length; k++) {
                                if (this.bundleObj.da_result[i].daid == airtimeBonusDACodes.result[k].daID) {
                                    bonusAirtime = Number(bonusAirtime) + Number(this.bundleObj.da_result[i].balance);
                                }
                            }
                        }
                    }

                    if (Number(bonusAirtime) > 0) {

                        bonusAirtime = Number(bonusAirtime) / 100;
			this.airtimeBonusBalance = Common_Strings.OPCO_CURRENCY + this.addzeros("0");
                        //this.airtimeBonusBalance = this._common.splitAmt(this.airtimeBonusBalance, 0)[0] + this._common.splitAmt(this.airtimeBonusBalance, 0)[1];
                        this.airtimeBonusBalance = Common_Strings.OPCO_CURRENCY + " " + this.addzeros(Number(bonusAirtime.toFixed(2)));
                    } else {
                    this.airtimeBonusBalance = Common_Strings.OPCO_CURRENCY + this.addzeros(0);
                    }

                    return this.airtimeBonusBalance;
                }).catch(err => Promise.reject(err));
            } else {
                this.apiService.deleteSessionId(msisdn);
            }
        }).catch(err => {
            let error = err.json();
            if (error.status == 401) {
                this.apiService.deleteSessionId(msisdn);
            }
        });


    }



    //To Fetch the Assigned Credit Limit and Oust. Balance of Postpaid Customers
    getCreditBalances(msisdn: string) {
        this.sessionId = this.storeVal.getSessionId();
        let primaryMsisdn = this.storeVal.getPrimaryMSISDNData();
        return this.sessionService.validateSession(primaryMsisdn, this.sessionId).then(resp => {
            let validateSessionStatus = resp;
            if (validateSessionStatus.status == 0) {
                return this.getCreditBalanceAPI(msisdn).then(resp => {
                    if (resp.StatusCode == "0") {
                        if(resp.ActualCreditLim == '' || resp.OutstandigBal == ''){
                            resp.ActualCreditLim = 0;
                            resp.OutstandigBal = 0;
                        }
                        this.creditLimitAssignedBalance = Common_Strings.OPCO_CURRENCY + this.addzeros(resp.ActualCreditLim);
                        this.outstandingBalance = Common_Strings.OPCO_CURRENCY + this.addzeros(resp.OutstandigBal);
                    }
                    else {
                        this.creditLimitAssignedBalance = Common_Strings.OPCO_CURRENCY + this.addzeros(0);
                        this.outstandingBalance = Common_Strings.OPCO_CURRENCY + this.addzeros(0);
                    }
                    return Promise.resolve([this.creditLimitAssignedBalance, this.outstandingBalance]);
                }).catch((err) => {
                    this.creditLimitAssignedBalance = Common_Strings.OPCO_CURRENCY + this.addzeros(0);
                    this.outstandingBalance = Common_Strings.OPCO_CURRENCY + this.addzeros(0);
                    return Promise.resolve([this.creditLimitAssignedBalance, this.outstandingBalance]);
                });
            } else {
                this.apiService.deleteSessionId(msisdn);
            }
        }).catch(err => {
            let error = err.json();
            if (error.status == 401) {
                this.apiService.deleteSessionId(msisdn);
            }
        });
    }

    addzeros(num){
        return num.toLocaleString("en", {useGrouping: false, minimumFractionDigits: 2});

    }

}
