export * from './airtimebalances.service';
export * from './bundlebalances.service';

export const dataBalanceConversionRate = 51.20;

export let airtimeBonusDACodes = {
    "result": [
        { "daID": "22", "daBalance": 0 },
        { "daID": "26", "daBalance": 0 },
        { "daID": "27", "daBalance": 0 },
        { "daID": "28", "daBalance": 0 },
        { "daID": "29", "daBalance": 0 },
        { "daID": "30", "daBalance": 0 },
        { "daID": "32", "daBalance": 0 },
        { "daID": "33", "daBalance": 0 },
        { "daID": "34", "daBalance": 0 },
        { "daID": "68", "daBalance": 0 },
        { "daID": "76", "daBalance": 0 },
        { "daID": "104", "daBalance": 0 },
        { "daID": "107", "daBalance": 0 },
        { "daID": "120", "daBalance": 0 },
        { "daID": "121", "daBalance": 0 },
        { "daID": "192", "daBalance": 0 },
        { "daID": "165", "daBalance": 0 },
        { "daID": "166", "daBalance": 0 }
    ]
};

export let dataBonusBundleDACodes = {
    "result": [
        { "daID": "5", "daName": "Hynet Bundle Bonus", "daBalance": 0, "expDate": "" },
        { "daID": "8", "daName": "Data Bundle Bonus", "daBalance": 0, "expDate": "" },
        { "daID": "11", "daName": "Comedy + Night Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "16", "daName": "Bonus on Recharge", "daBalance": 0, "expDate": "" },
        { "daID": "39", "daName": "3-in-1 Promo", "daBalance": 0, "expDate": "" },
        { "daID": "129", "daName": "Data Bundle Bonus", "daBalance": 0, "expDate": "" },
        { "daID": "140", "daName": "WinBack Data Bonus", "daBalance": 0, "expDate": "" },
        { "daID": "146", "daName": "VTU Data Bonus on Recharge ", "daBalance": 0, "expDate": "" },
        { "daID": "153", "daName": "WinBack Data Bonus", "daBalance": 0, "expDate": "" },
        { "daID": "155", "daName": "Data Bundle Bonus", "daBalance": 0, "expDate": "" },
        { "daID": "178", "daName": "CLM/Anniversary data offer", "daBalance": 0, "expDate": "" },
        { "daID": "191", "daName": "Data Bonus", "daBalance": 0, "expDate": "" },
        { "daID": "215", "daName": "WeekDays Bundle Bonus", "daBalance": 0, "expDate": "" },
        { "daID": "222", "daName": "All Day Bonus", "daBalance": 0, "expDate": "" },
        { "daID": "252", "daName": "WinBack Data Bonus", "daBalance": 0, "expDate": "" },
        { "daID": "3413", "daName": "HyNet Flex (Device Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "3414", "daName": "HyNet Flex (Loyalty Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "4534", "daName": "Youtube and Instagram", "daBalance": 0, "expDate": "" },
        { "daID": "4720", "daName": "2 Days Plan", "daBalance": 0, "expDate": "" },
        { "daID": "4721", "daName": "Bi-Weekly Plan", "daBalance": 0, "expDate": "" },
        { "daID": "4722", "daName": "Monthly Plan (4G Only)", "daBalance": 0, "expDate": "" },
        { "daID": "4780", "daName": "Kpalasa bonus", "daBalance": 0, "expDate": "" },
        { "daID": "8200", "daName": "LTE (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8201", "daName": "4G (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8202", "daName": "CVM (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8203", "daName": "Kpalasa (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8204", "daName": "Samsung (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8205", "daName": "LCMS (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8206", "daName": "BOBR (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8207", "daName": "Freetel (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8208", "daName": "Upfront (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8209", "daName": "Pulse (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8210", "daName": "ALUMNUS (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8211", "daName": "CLM_Agent_Store (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8212", "daName": "Yafun Yafun (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8213", "daName": "Winback (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8214", "daName": "WinBack (All Day Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8215", "daName": "All Day Bonus", "daBalance": 0, "expDate": "" },
        { "daID": "8300", "daName": "LTE (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8301", "daName": "4G (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8302", "daName": "CVM (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8303", "daName": "Kpalasa (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8304", "daName": "Samsung (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8305", "daName": "LCMS (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8306", "daName": "BOBR (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8307", "daName": "Freetel (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8308", "daName": "Upfront (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8309", "daName": "Pulse (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8310", "daName": "ALUMNUS (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8311", "daName": "CLM_Agent_Store (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8312", "daName": "Yafun Yafun (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8313", "daName": "Winback (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8314", "daName": "WinBack (Night Bonus)", "daBalance": 0, "expDate": "" },
        { "daID": "8315", "daName": "Night Bonus", "daBalance": 0, "expDate": "" }
    ]
};



export let dataBundleDACodes = {
    "result": [
        { "daID": "2", "daName": "Hynet Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "9", "daName": "MTN mPulse Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "10", "daName": "BBM Data Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "13", "daName": "Pulse Bundle Bonus", "daBalance": 0, "expDate": "" },
        { "daID": "20", "daName": "BB10 Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "81", "daName": "Do Box Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "89", "daName": "GDS Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "90", "daName": "GDS Top-up Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "91", "daName": "BizPlus data bundles", "daBalance": 0, "expDate": "" },
        { "daID": "93", "daName": "AutoDevice", "daBalance": 0, "expDate": "" },
        { "daID": "151", "daName": "MTN Teen Weekly Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "156", "daName": "MTN Teen Monthly Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "211", "daName": "Daily Plan", "daBalance": 0, "expDate": "" },
        { "daID": "213", "daName": "Weekly Plan	Data Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "214", "daName": "Monthly Plan", "daBalance": 0, "expDate": "" },
        { "daID": "216", "daName": "60 Days Plan", "daBalance": 0, "expDate": "" },
        { "daID": "217", "daName": "90 Days Plan", "daBalance": 0, "expDate": "" },
        { "daID": "218", "daName": "180 Days Plan", "daBalance": 0, "expDate": "" },
        { "daID": "219", "daName": "365 Days Plan", "daBalance": 0, "expDate": "" },
        { "daID": "220", "daName": "Monthly Plan", "daBalance": 0, "expDate": "" },
        { "daID": "221", "daName": "Data Plan Weekend", "daBalance": 0, "expDate": "" },
        { "daID": "244", "daName": "Night Pulse Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "3412", "daName": "HyNet Flex", "daBalance": 0, "expDate": "" },
        { "daID": "8052", "daName": "Hynet LTE Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "8053", "daName": "Hynet LTE Staff Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "8068", "daName": "MTN Teen WhatsApp Bundle", "daBalance": 0, "expDate": "" },
        { "daID": "8395", "daName": "MYMTN APP", "daBalance": 0, "expDate": "" },
        { "daID": "8397", "daName": "XtraValue Carte Data (Top-Up)(Night)", "daBalance": 0, "expDate": "" },
        { "daID": "8398", "daName": "XtraValue Carte Data (Top-Up)(Night)", "daBalance": 0, "expDate": "" }
    ]
};

export let other_Balance_Latest_DAID = {
        "result": [

 {
   "daID": "1",
   "unit": "MB",
   "type": "Bonus",
   "daName": "Blaast Event Bundle",
   "daBalance": "",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "4",
   "unit": "MB",
   "type": "Main",
   "daName": "Staff Data Bundle",
   "daBalance": "",
   "Category": "Staff Data Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "6",
   "unit": "MB",
   "type": "Main",
   "daName": "2Go Bundle",
   "daBalance": "",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "7",
   "unit": "MB",
   "type": "Main",
   "daName": "Twitter Bundle",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "12",
   "unit": "MB",
   "type": "Main",
   "daName": "Facebook",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "14",
   "unit": "MB",
   "type": "Bonus",
   "daName": "WhatsApp Bonus on Recharge",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "15",
   "unit": "MB",
   "type": "Main",
   "daName": "BizPlus Bundle - Data",
   "daBalance": "0",
   "Category": "BizPlus Bundle",//should be bizplus
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "17",
   "unit": "MB",
   "type": "Main",
   "daName": "Eskimi Bundle",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "18",
   "unit": "MB",
   "type": "Main",
   "daName": "SME Device Offer",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "19",
   "unit": "MB",
   "type": "Main",
   "daName": "Instagram",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "26",
   "unit": "MIN",
   "type": "Main",
   "daName": "BizPlus Bundle - Voice",
   "daBalance": "0",
   "Category": "BizPlus Bundle",///should be biz plus bundle
   "expDate": "",
   "Con_Rate": "660"
},
{
   "daID": "28",
   "unit": "MIN",
   "type": "Main",
   "daName": "SME Onebox Voice International",
   "daBalance": "",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "1200"
},
{
   "daID": "32",
   "unit": "NGN",
   "type": "Main",
   "daName": "VTU",
   "daBalance": "0",
   "Category": "VTU Wallet",
   "expDate": "",
   "Con_Rate": "100"
},
{
   "daID": "34",
   "unit": "MIN",
   "type": "Main",
   "daName": "SME Voice Bundle",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "667"
},
{
   "daID": "35",
   "unit": "MIN",
   "type": "Main",
   "daName": "International Destination Bundle",
   "daBalance": "0",
   "Category": "International Destination Bundle",
   "expDate": "",
   "Con_Rate": "1200"
},
{
   "daID": "37",
   "unit": "MIN",
   "type": "Main",
   "daName": "International Destination Bundle",
   "daBalance": "0",
   "Category": "International Destination Bundle",
   "expDate": "",
   "Con_Rate": "600"
},
{
   "daID": "38",
   "unit": "MIN",
   "type": "Main",
   "daName": "International Destination Bundle",
   "daBalance": "0",
   "Category": "International Destination Bundle",
   "expDate": "",
   "Con_Rate": "1500"
},
{
   "daID": "40",
   "unit": "SMS",
   "type": "Main",
   "daName": "SMS OnNet M2M",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "400"
},
{
   "daID": "41",
   "unit": "SMS",
   "type": "Main",
   "daName": "BizPlus Bundle - SMS",
   "daBalance": "0",
   "Category": "BizPlus Bundle",//should be Bizplus
   "expDate": "",
   "Con_Rate": "400"
},
{
   "daID": "58",
   "unit": "MB",
   "type": "Main",
   "daName": "SME Flex Bundle",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "59",
   "unit": "MIN",
   "type": "Main",
   "daName": "SME Flex Bundle Voice",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "660"
},
{
   "daID": "82",
   "unit": "MB",
   "type": "Main",
   "daName": "Nimbuzz",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "83",
   "unit": "MB",
   "type": "Main",
   "daName": "Viber Event Based Bundle",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "84",
   "unit": "MB",
   "type": "Main",
   "daName": "Wechat",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "86",
   "unit": "MB",
   "type": "Main",
   "daName": "SME Data",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "87",
   "unit": "MB",
   "type": "Main",
   "daName": "SME Data Share",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "99",
   "unit": "MB",
   "type": "Main",
   "daName": "Whatsapp",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "100",
   "unit": "NGN",
   "type": "Main",
   "daName": "XtraTime",
   "daBalance": "0",
   "Category": "XtraTime/XtraByte Balance",
   "expDate": "",
   "Con_Rate": "100"
},
{
   "daID": "128",
   "unit": "MIN",
   "type": "Main",
   "daName": "Super Roamer  (Free Incoming Calls)",
   "daBalance": "0",
   "Category": "Roaming Bundle",
   "expDate": "",
   "Con_Rate": "2000"
},
{
   "daID": "134",
   "unit": "MIN",
   "type": "Main",
   "daName": "SME Flex bundle",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "600"
},
{
   "daID": "159",
   "unit": "NGN",
   "type": "Bonus",
   "daName": "Yafun Data Bonus",
   "daBalance": "0",
   "Category": "Yafun-Yafun Balance",
   "expDate": "",
   "Con_Rate": "100"
},
{
   "daID": "161",
   "unit": "MB",
   "type": "Main",
   "daName": "Game Plus",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "165",
   "unit": "MIN",
   "type": "Bonus",
   "daName": "Roaming Bonus",
   "daBalance": "0",
   "Category": "Roaming Bonus",
   "expDate": "",
   "Con_Rate": "12400"
},
{
   "daID": "166",
   "unit": "MIN",
   "type": "Bonus",
   "daName": "Roaming Bonus",
   "daBalance": "0",
   "Category": "Roaming Bonus",
   "expDate": "",
   "Con_Rate": "12400"
},
{
   "daID": "170",
   "unit": "MB",
   "type": "Main",
   "daName": "Binge Night and Off Peak",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "176",
   "unit": "MB",
   "type": "Bonus",
   "daName": "Binge Weekend",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "177",
   "unit": "MB",
   "type": "Bonus",
   "daName": "Social Media Pass",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "181",
   "unit": "MB",
   "type": "Main",
   "daName": "Data",
   "daBalance": "0",
   "Category": "Data & Voice Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "182",
   "unit": "NGN",
   "type": "Main",
   "daName": "Voice",
   "daBalance": "0",
   "Category": "Data & Voice Bundle",
   "expDate": "",
   "Con_Rate": "100"
},
{
   "daID": "192",
   "unit": "NGN",
   "type": "Bonus",
   "daName": "Yafun Voice Bonus",
   "daBalance": "0",
   "Category": "Yafun-Yafun Balance",
   "expDate": "",
   "Con_Rate": "100"
},
{
   "daID": "194",
   "unit": "SMS",
   "type": "Main",
   "daName": "Outgoing Voice (SMS)",
   "daBalance": "0",
   "Category": "Roaming Bundle",
   "expDate": "",
   "Con_Rate": "1000"
},
{
   "daID": "195",
   "unit": "MB",
   "type": "Main",
   "daName": "Roaming Data Bundle",
   "daBalance": "0",
   "Category": "Roaming Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "197",
   "unit": "NGN",
   "type": "Bonus",
   "daName": "BetaTalk Bonus",
   "daBalance": "0",
   "Category": "BetaTalk Bonus",
   "expDate": "",
   "Con_Rate": "100"
},
{
   "daID": "199",
   "unit": "MB",
   "type": "Main",
   "daName": "XtraByte",
   "daBalance": "0",
   "Category": "XtraTime/XtraByte Balance",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "223",
   "unit": "MIN",
   "type": "Main",
   "daName": "Roaming Voice Bundle (Out-Going)",
   // "daName": "Roaming Bundle (Voice OutGoing)",
   "daBalance": "0",
   "Category": "Roaming Bundle",
   "expDate": "",
   "Con_Rate": "3600"
},
{
   "daID": "224",
   "unit": "MIN",
   "type": "Main",
   // "daName": "Roaming Bundle",
   "daName": "Roaming Voice Bundle (In-Coming)",
   "daBalance": "0",
   "Category": "Roaming Bundle",
   "expDate": "",
   "Con_Rate": "2000"
},
{
   "daID": "250",
   "unit": "MB",
   "type": "Main",
   "daName": "Youtube",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "265",
   "unit": "MIN",
   "type": "Main",
   "daName": "SME Voice Bundle",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "667"
},
{
   "daID": "266",
   "unit": "MIN",
   "type": "Main",
   "daName": "SME Voice Bundle",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "656"
},
{
   "daID": "267",
   "unit": "MIN",
   "type": "Main",
  "daName": "SME Voice Bundle",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "650"
},
{
   "daID": "280",
   "unit": "MB",
   "type": "Main",
   "daName": "Instabinge",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "3412",
   "unit": "MB",
   "type": "Main",
   "daName": "HyNet Flex",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "3413",
   "unit": "MB",
   "type": "Bonus",
   "daName": "HyNet Flex (Device Bonus)",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "3414",
   "unit": "MB",
   "type": "Bonus",
   "daName": "HyNet Flex (Loyalty Bonus)",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "4778",
   "unit": "MB",
   "type": "Bonus",
   "daName": "SmartFeature Phone",
   "daBalance": "0",
   "Category": "Data Bonus",
   "expDate": "",
   "Con_Rate": "100"
},
{//Not other balance instead it is airtime bonus  
   "daID": "7515",
   "unit": "NGN",
   "type": "Main",
   "daName": "SIM Registration Bonus",
   "daBalance": "",
   "Category": "Airtime Bonus",
   "expDate": "",
   "Con_Rate": "100"
},
  {
   "daID": "8045",
   "unit": "MIN",
   "type": "Main",
   "daName": "XtraValue Carte - IDB",
   "daBalance": "0",
   "Category": "XtraValue Carte",
   "expDate": "",
   "Con_Rate": "1140"
},
   {
   "daID": "8046",
   "unit": "MIN",
   "type": "Main",
   "daName": "XtraValue Carte - IDB (Top-Up)",
   "daBalance": "0",
   "Category": "XtraValue Carte",
   "expDate": "",
   "Con_Rate": "1250"
},
{
   "daID": "8054",
   "unit": "MB",
   "type": "Main",
   "daName": "BizPlus Bundle - Data",
   "daBalance": "",
   "Category": "BizPlus Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "8057",
   "unit": "MB",
   "type": "Main",
   "daName": "SME OneBox (Data) Bundle",
   "daBalance": "",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "8061",
   "unit": "MB",
   "type": "Main",
   "daName": "N-Power Bundle",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "8062",
   "unit": "MB",
   "type": "Main",
   "daName": "Whatsapp Bundle",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "8066",
   "unit": "MB",
   "type": "Bonus",
   "daName": "MyMTN Download Bonus ",
   "daBalance": "0",
   "Category": "MyMTN App Bonus",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "8071",
   "unit": "MB",
   "type": "Main",
   "daName": "Social Media Pass",
   "daBalance": "0",
   "Category": "GoodyBag Social Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "8088",
   "unit": "MB",
   "type": "Main",
   "daName": "SME VP Device Bundle",
   "daBalance": "",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "51.2"
},
{
   "daID": "8099",
   "unit": "NGN",
   "type": "Main",
   "daName": "XtraValue Carte - Roaming Voice",
   "daBalance": "0",
   "Category": "XtraValue Carte",
   "expDate": "",
   "Con_Rate": "100"
},
{
   "daID": "8155",
   "unit": "SMS",
   "type": "Main",
   "daName": "SME OneBox (SMS)",
   "daBalance": "0",
   "Category": "SME Bundle",
   "expDate": "",
   "Con_Rate": "400"
},
{
   "daID": "8395",
   "unit": "MB",
   "type": "Main",
   "daName": "XtraValue Carte - Data",
   "daBalance": "0",
   "Category": "XtraValue Carte",
   "expDate": "",
   "Con_Rate": "/650"
},
{
   "daID": "8396",
   "unit": "MB",
   "type": "Main",
   "daName": "XtraValue Carte - Roaming Data",
   "daBalance": "0",
   "Category": "XtraValue Carte",
   "expDate": "",
   "Con_Rate": "/2500"
},
{
   "daID": "8397",
   "unit": "MB",
   "type": "Main",
   "daName": "XtraValue Carte Data (Top-Up)(Night)",
   "daBalance": "0",
   "Category": "XtraValue Carte",
   "expDate": "",
   "Con_Rate": "650"
},
{
   "daID": "8398",
   "unit": "MB",
   "type": "Main",
   "daName": "XtraValue Carte Data (Top-Up)(Night)",
   "daBalance": "0",
   "Category": "XtraValue Carte",
   "expDate": "",
   "Con_Rate": "650"
},
{
   "daID": "8400",
   "unit": "SMS",
   "type": "Main",
   "daName": "BizPlus Bundle - SMS",
   "daBalance": "0",
   "Category": "BizPlus Bundle",//should be bizplus
   "expDate": "",
   "Con_Rate": "400"
},
{
   "daID": "8498",
   "unit": "NGN",
   "type": "Main",
   "daName": "XtraValue Carte - Airtime",
   "daBalance": "0",
   "Category": "XtraValue Carte",
   "expDate": "",
   "Con_Rate": "100"
},
{
   "daID": "8500",
   "unit": "NGN",
   "type": "Main",
   "daName": "XtraValue Carte - Airtime (Top-Up)",
   "daBalance": "0",
   "Category": "XtraValue Carte",
   "expDate": "",
   "Con_Rate": "100"
}
]
}

export const SampleJSON = {
	"status_code": 0,
	"status_message": "OK",
	"transaction_id": "2940802254",
	"balance_result": [{
		"mapid": "222",
		"used": "122.70",
		"total": "2621.44"
	}, {
		"mapid": "630",
		"total": ""
	}, {
		"mapid": "3507",
		"used": "657.50"
	}, {
		"mapid": "4305",
		"used": "102125.00"
	}, {
		"mapid": "5003",
		"used": "1905.44",
		"total": "5313.36"
	}, {
		"mapid": "5097",
		"used": "955.27"
	}, {
		"mapid": "9761",
		"used": "2000.00",
		"total": "2000.00"
	}, {
		"mapid": "9761",
		"used": "2000.00",
		"total": "2000.00"
	}, {
		"mapid": "9781",
		"used": "240.00",
		"total": "240.00"
	}, {
		"mapid": "9781",
		"used": "240.00",
		"total": "240.00"
	}, {
		"mapid": "11014",
		"used": "1141.85",
		"total": "1003145.72"
	}, {
		"mapid": "15",
		"used": "0",
		"total": "25.60"
	}, {
		"mapid": "16",
		"used": "0",
		"total": "1000.00"
	}, {
		"mapid": "25",
		"used": "0",
		"total": "102.40"
	}, {
		"mapid": "26",
		"used": "0",
		"total": "300.00"
	}, {
		"mapid": "27",
		"used": "0",
		"total": "256.00"
	}, {
		"mapid": "28",
		"used": "0",
		"total": "500.00"
	}, {
		"mapid": "191",
		"used": "0",
		"total": "256.00"
	}, {
		"mapid": "222",
		"used": "0",
		"total": "2621.44"
	}, {
		"mapid": "604",
		"used": "0",
		"total": "15.36"
	}, {
		"mapid": "620",
		"used": "0",
		"total": "38.40"
	}, {
		"mapid": "623",
		"used": "0",
		"total": "3670.01"
	}, {
		"mapid": "632",
		"total": ""
	}, {
		"mapid": "1439",
		"total": ""
	}, {
		"mapid": "2009",
		"total": ""
	}, {
		"mapid": "3128",
		"used": "0",
		"total": "6000.00"
	}, {
		"mapid": "9632",
		"used": "0",
		"total": "10000.00"
	}, {
		"mapid": "9652",
		"total": ""
	}, {
		"mapid": "9754",
		"used": "0",
		"total": "500.00"
	}, {
		"mapid": "9761",
		"used": "0",
		"total": "2000.00"
	}, {
		"mapid": "9774",
		"used": "0",
		"total": "50.00"
	}, {
		"mapid": "9781",
		"used": "0",
		"total": "240.00"
	}, {
		"mapid": "9824",
		"used": "0",
		"total": "10.24"
	}, {
		"mapid": "9826",
		"used": "0",
		"total": "1000163.84"
	}, {
		"mapid": "19210",
		"used": "0",
		"total": "50944.00"
	}, {
		"mapid": "19211",
		"used": "0",
		"total": "10982.40"
	}, {
		"mapid": "19212",
		"used": "0",
		"total": "5000.00"
	}, {
		"mapid": "19213",
		"used": "0",
		"total": "4000.00"
	}, {
		"mapid": "19214",
		"used": "0",
		"total": "3000.00"
	}, {
		"mapid": "19216",
		"used": "0",
		"total": "325.00"
	}, {
		"mapid": "19217",
		"used": "0",
		"total": "1500.00"
	}, {
		"mapid": "19218",
		"used": "0",
		"total": "8098.40"
	}],
	"da_result": [{
		"daid": "0",
		"balance": "23442",
		"startDate": "",
		"expiryDate": ""
	}, {
		"daid": "54",
		"balance": "0",
		"startDate": "9999-12-31T00:00:00+12",
		"expiryDate": "9999-12-31T00:00:00+12"
	}, {
		"daid": "61",
		"balance": "0",
		"startDate": "9999-12-31T00:00:00+12",
		"expiryDate": "9999-12-31T00:00:00+12"
	}, {
		"daid": "75",
		"balance": "0",
		"startDate": "9999-12-31T00:00:00+12",
		"expiryDate": "9999-12-31T00:00:00+12"
	}, {
		"daid": "121",
		"balance": "50000",
		"startDate": "9999-12-31T00:00:00+12",
		"expiryDate": "2019-07-18T12:00:00+00"
	}, {
		"daid": "138",
		"balance": "0",
		"startDate": "9999-12-31T00:00:00+12",
		"expiryDate": "9999-12-31T00:00:00+12"
	}, {
		"daid": "140",
		"balance": "0",
		"startDate": "9999-12-31T00:00:00+12",
		"expiryDate": "9999-12-31T00:00:00+12"
	}, {
		"daid": "146",
		"balance": "0",
		"startDate": "9999-12-31T00:00:00+12",
		"expiryDate": "9999-12-31T00:00:00+12"
	}, {
		"daid": "162",
		"balance": "0",
		"startDate": "",
		"expiryDate": ""
	}, {
		"daid": "162",
		"balance": "0",
		"startDate": "",
		"expiryDate": ""
	}, {
		"daid": "184",
		"balance": "0",
		"startDate": "",
		"expiryDate": ""
	}, {
		"daid": "184",
		"balance": "0",
		"startDate": "",
		"expiryDate": ""
	}, {
		"daid": "196",
		"balance": "0",
		"startDate": "9999-12-31T00:00:00+12",
		"expiryDate": "9999-12-31T00:00:00+12"
	}, {
		"daid": "197",
		"balance": "0",
		"startDate": "9999-12-31T00:00:00+12",
		"expiryDate": "9999-12-31T00:00:00+12"
	}, {
		"daid": "211",
		"balance": "0",
		"startDate": "9999-12-31T00:00:00+12",
		"expiryDate": "9999-12-31T00:00:00+12"
	}, {
		"daid": "4526",
		"balance": "50000",
		"startDate": "9999-12-31T00:00:00+12",
		"expiryDate": "2019-07-17T12:00:00+00"
	}, {
		"daid": "4788",
		"balance": "512",
		"startDate": "9999-12-31T00:00:00+12",
		"expiryDate": "2019-08-09T12:00:00+00"
	}, {
		"daid": "8045",
		"balance": "100000",
		"startDate": "2019-07-11T12:00:00+00",
		"expiryDate": "2019-08-10T12:00:00+00"
	}, {
		"daid": "8046",
		"balance": "150000",
		"startDate": "2019-07-11T12:00:00+00",
		"expiryDate": "2019-07-18T12:00:00+00"
	}, {
		"daid": "8099",
		"balance": "100000",
		"startDate": "2019-07-11T12:00:00+00",
		"expiryDate": "2019-08-10T12:00:00+00"
	}, {
		"daid": "8395",
		"balance": "96135",
		"startDate": "2019-07-11T12:00:00+00",
		"expiryDate": "2019-08-10T12:00:00+00"
	}, {
		"daid": "8396",
		"balance": "100000",
		"startDate": "2019-07-11T12:00:00+00",
		"expiryDate": "2019-08-10T12:00:00+00"
	}, {
		"daid": "8498",
		"balance": "410000",
		"startDate": "2019-07-11T12:00:00+00",
		"expiryDate": "2019-08-10T12:00:00+00"
	}],
	"PrefLang": "1",
	"planName": "52",
	"CreditClearanceDate": "2037-12-31T12:00:00+00",
	"ServiceFeeExpiryDate": "2037-12-31T12:00:00+00",
	"ServiceRemovalDate": "2037-12-31T12:00:00+00",
	"SupervisionExpiryDate": "2037-12-31T12:00:00+00"
};


export const OTHER_BALANCE_CATEGORIES_LIST=["GoodyBag Social Bundle","Staff Data Bundle","SME Bundle","International Destination Bundle","Roaming Bundle","MyMTN App Bonus","XtraTime/XtraByte Balance","VTU Wallet","Roaming Bonus","Data & Voice Bundle","Yafun-Yafun Balance","XtraValue Carte","Data Bonus","BizPlus Bundle"];
export const OTHER_BALANCE_CATEGORIES_LIST_DETAIL_EXCLUDED=["Data & Voice Bundle"];
export const OTHER_BALANCE_CATEGORIES_LIST_DETAIL_SPECIFIC=["Airtime Bonus","GoodyBag Social Bundle","Staff Data Bundle","SME Bundle","International Destination Bundle","Roaming Bundle","MyMTN App Bonus","XtraTime/XtraByte Balance","VTU Wallet","Roaming Bonus","Yafun-Yafun Balance","XtraValue Carte","Data Bonus","BizPlus Bundle"];




export const bundleMasterConfig = {
    "prodInfo": [{ "id": "dataplans", "value": "Data plans", "activationChannel": "CIS", "bundleCategory": "DataPlan", "bundleSubCategory": ["DataPlan Daily", "DataPlan Weekly", "DataPlan Monthly", "DataPlan 60Days", "DataPlan 90Days", "DataPlan 180Days", "DataPlan Yearly"], "bundleTabs": ["Daily", "Weekly", "Monthly", "2 Months", "3 Months", "6 Months", "Yearly"] },
    { "id": "xtraValue", "value": "XtraValue", "activationChannel": "CIS", "bundleCategory": "XTRAVALUE", "bundleSubCategory": ["XTRADATA", "XTRATALK"], "bundleTabs": ["XtraData", "XtraTalk"] },
    { "id": "intlcalling", "value": "Int'l calling", "activationChannel": "CIS", "bundleCategory": "IDB", "bundleSubCategory": ["IDB300", "IDB500", "IDB1500"], "bundleTabs": ["Daily", "Weekly", "Monthly"] },
    { "id": "roaming", "value": "Roaming", "activationChannel": "CIS", "bundleCategory": "Roaming Bundles", "bundleSubCategory": ["Roaming Data Bundle", "Roaming Voice and Data", "Roaming Voice Only", "Roaming UAE", "Roaming Discount Call Rate"], "bundleTabs": ["Roaming Data Bundle", "Roaming Voice and Data", "Roaming Voice Only", "Roaming UAE", "Roaming Discount Call Rate"] },
    { "id": "goodybag", "value": "Goodybag", "activationChannel": "CIS", "bundleCategory": "GoodyBag", "bundleSubCategory": ["WhatsApp", "FaceBook", "Instagram", "Twitter", "WeChat", "Nimbuzz", "Eskimi", "2Go", "Social Media"], "bundleTabs": ["WhatsApp", "FaceBook", "Instagram", "Twitter", "WeChat", "Nimbuzz", "Eskimi", "2Go", "Social Media"] },

    ]

};

