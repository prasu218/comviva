import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response } from '@angular/http';
// import { Store } from '@ngrx/store';
// import { bundleMasterConfig } from '../../../../../../app/config/config';
// import { buyBundlesResponse_Array, buyBundlesGdsResponse_Array, buyBundlesDailyData_Response, buyBundlesMonthlyData_Response, buyBundlesWeeklyData_Response, buyBundles2MonthsData_Response, buyBundlesGoodyBagFaceBookMonthlyResponse_Array, buyBundlesXtravalueResponse_Array, buyBundlesXtravalueWeeklyResponse_Array, buyBundlesXtravaluMonthlyResponse_Array, buyBundlesXtraTalkResponse_Array, buyBundlesXtraTalkMonthlyResponse_Array, buyBundlesXtraTalkWeeklyResponse_Array, buyBundlesGoodyBagFaceBookResponse_Array, buyBundlesGoodyBagFaceBookWeeklyResponse_Array, buyBundlesGoodyBagWhatsAppResponse_Array, buyBundlesGoodyBagWhatsAppWeeklyResponse_Array, buyBundlesGoodyBagWhatsAppMonthlyResponse_Array, buyBundlesGoodyBagInstagramResponse_Array, buyBundlesGoodyBagInstagramWeeklyResponse_Array, buyBundlesGoodyBagInstagramMonthlyResponse_Array, buyBundlesGoodyBagSMEResponse_Array, buyBundlesGoodyBagSMEWeeklyResponse_Array, buyBundlesGoodyBagSMEMonthlyResponse_Array, buyBundlesGoodyBagEskimiResponse_Array, buyBundlesGoodyBagEskimiWeeklyResponse_Array, buyBundlesGoodyBagEskimiMonthlyResponse_Array, buyBundlesGoodyBagTwoGoResponse_Array, buyBundlesGoodyBagTwoGoWeeklyResponse_Array, buyBundlesGoodyBagTwoGoMonthlyResponse_Array, buyBundlesGoodyBagWeChatResponse_Array, buyBundlesGoodyBagWeChatWeeklyResponse_Array, buyBundlesGoodyBagWeChatMonthlyResponse_Array, bundleActivation_Response } from '../../config';

import { HttpClient } from '@angular/common/http';
import { ApiService } from '../commonAPI';
import { StoreService } from '../../plugins/datastore'

@Injectable()
export class BuyBundlesService {
  dataPlanDetails;
  buybundleplansDetails;
  constructor(private _http: Http, private http: HttpClient, private apiService: ApiService, private _storeVal: StoreService) { }
  setDataPlanDetails(data) {
    this.dataPlanDetails = data;
  }
  getDataPlanDetails() {
    return this.dataPlanDetails;
  }
  setBuyBundlePlanDetails(data) {
    this.buybundleplansDetails = data;
  }
  getBuyBundlePlanDetails() {
    return this.buybundleplansDetails;
  }



  getBuyBundleCatCIS(msisdn: string, bundle_Category: string, bundle_SubCategory: string) {
    let sessionId = this._storeVal.getSessionId();
    const payload = {
      msisdn: msisdn,
      type: bundle_Category,
      subtype: bundle_SubCategory
    };
    // console.log("cgetBuyBundleCatCIS");
    // console.log("msisdn---"+msisdn);
    const buybundleXtraTalkCatURL = '/api/getBundleCatCIS ';
    // return this._http.post(buybundleXtraTalkCatURL, payload).toPromise().then(resp => {
    //   return Promise.resolve(resp.json());
    // }).catch((err) => {
    //   return  Promise.reject(err);
    // });
    return this.apiService.postWithSessionValidation(buybundleXtraTalkCatURL, msisdn, sessionId, payload);
  }
  
  getAllBuyBundleProducts(msisdn: string) {
    let sessionId = this._storeVal.getSessionId();
    const buybundleProductListURL = '/api/getAllBundleProductListCIS ';
    const payload = {
      msisdn: msisdn
    };

    return this.apiService.postWithSessionValidation(buybundleProductListURL, msisdn, sessionId, payload);
  }

  getBuyBundleCatSme(msisdn: string, bundle_Category: string, bundle_SubCategory: string) {
    let sessionId = this._storeVal.getSessionId();
    const payload = {
      msisdn: msisdn,
      type: bundle_Category,
      subtype: bundle_SubCategory
    };
    // console.log("cgetBuyBundleCatCIS");
    // console.log("msisdn---"+msisdn);
    const buybundleSmeCatURL = '/api/getBundleCatCISSME ';
    // return this._http.post(buybundleSmeCatURL, payload).toPromise().then(resp => {
    //   return Promise.resolve(resp.json());
    // }).catch((err) => {
    //   return  Promise.reject(err);
    // });
    return this.apiService.postWithSessionValidation(buybundleSmeCatURL, msisdn, sessionId, payload)
  }





  getBuyForSelfCIS(msisdn: string, action: string, price: string, autorenewal: any) {
    let sessionId = this._storeVal.getSessionId();
    const payload = {
      msisdn: msisdn,
      input: action,
      price: price,
      autorenewal: autorenewal
    };
    //   console.log('insidebuyforSelfCISafterpayload');
    //  console.log('buy data bundle for self' + payload);
    const buyforSelfURL = '/api/buyforSelfCIS';
    // console.log("config.PLUGIN_UcdRL talk---"+buyforSelfURL);

    // return this._http.post(buyforSelfURL, payload).toPromise().then(resp => {
    //   return Promise.resolve(resp.json());
    // }).catch((err) => {
    //   Promise.reject(err);
    // });
    return this.apiService.postWithSessionValidation(buyforSelfURL, msisdn, sessionId, payload)
  }

  getBuyForOthersCIS(msisdn: string, action: string, price: string, beneficiaryMsisdn: string) {
    let sessionId = this._storeVal.getSessionId();
    const payload = {
      msisdn: msisdn,
      input: action,
      price: price,
      beneficiaryMsisdn: beneficiaryMsisdn
    };
    // console.log("beneficiaryMsisdn---"+beneficiaryMsisdn);
    // console.log('buy data bundle for others' + payload);
    const buyforothersURL = '/api/buyforothersCIS';
    // console.log("config.PLUGIN_URL talk---"+config.PLUGIN_URL);

    //return Promise.resolve(this.buyBundlesXtravalueResponse);
    // return this._http.post(buyforothersURL, payload).toPromise().then(resp => {
    //   return Promise.resolve(resp.json());
    // }).catch((err) => {
    //   Promise.reject(err);
    // });
    return this.apiService.postWithSessionValidation(buyforothersURL, msisdn, sessionId, payload)
  }
  getSmeDataShare(msisdn) {
    let sessionId = this._storeVal.getSessionId();
    const payload = {
      msisdn: msisdn,

    };

    const buyforothersURL = '/api/datashare';
    //console.log("config.PLUGIN_URL talk---" + config.PLUGIN_URL);

    //return Promise.resolve(this.buyBundlesXtravalueResponse);
    // return this._http.post(buyforothersURL, payload).toPromise().then(resp => {
    //   console.log("getSmeDataShare",resp);

    //   return Promise.resolve(resp.json());
    // }).catch((err) => {
    //   Promise.reject(err);
    // });
    return this.apiService.postWithSessionValidation(buyforothersURL, msisdn, sessionId, payload)
  }



  SmeDataShare(msisdn, Beneficiary, pin, share_id) {
    let sessionId = this._storeVal.getSessionId();
    const payload = {
      msisdn: msisdn,
      pin: pin,
      share_id: share_id,
      Beneficiary: Beneficiary,
      //input:input

    };

    const buyforothersURL = '/api/shareddata';
    //console.log("config.PLUGIN_URL talk---" + config.PLUGIN_URL);

    //return Promise.resolve(this.buyBundlesXtravalueResponse);
    // return this._http.post(buyforothersURL, payload).toPromise().then(resp => {
    //   console.log("getSmeDataShare",resp);

    //   return Promise.resolve(resp.json());
    // }).catch((err) => {
    //   Promise.reject(err);
    // });
    return this.apiService.postWithSessionValidation(buyforothersURL, msisdn, sessionId, payload)
  }


  //countries  getRoamingRateCountryDetails
  getRoamingRateCountryDetails(msisdn: string, values) {
    let sessionId = this._storeVal.getSessionId();
    const payload = {
      msisdn: msisdn,
      values: values

    };


    const getRoamingRateCountryDetailsURL = '/api/getRoamingRateCountryDetails';
    // console.log("config.PLUGIN_URL talk---"+config.PLUGIN_URL);

    // return this._http.post(getRoamingRateCountryDetailsURL, payload).toPromise().then(resp => {
    //   return Promise.resolve(resp.json());
    // }).catch((err) => {
    //   Promise.reject(err);
    // });
    return this.apiService.postWithSessionValidation(getRoamingRateCountryDetailsURL, msisdn, sessionId, payload)
  }

  //getRoamingdata
  getRoamingdata(msisdn: string, bundle_Category: string, bundle_SubCategory: string) {
    let sessionId = this._storeVal.getSessionId();

    const payload = {
      msisdn: msisdn,
      type: bundle_Category,
      subtype: bundle_SubCategory
    };
    //console.log(payload);
    const getRoamingdataURl = '/api/getRoamingdata';
    // return this._http.post(getRoamingdataURl, payload).toPromise().then(resp => {
    //   return Promise.resolve(resp.json());
    // }).catch((err) => {
    //   Promise.reject(err);
    // });
    return this.apiService.postWithSessionValidation(getRoamingdataURl, msisdn, sessionId, payload)
  }


  //getRoamingvoice
  getRoamingvoice(msisdn: string, bundle_Category: string, bundle_SubCategory: string) {
    let sessionId = this._storeVal.getSessionId();
    const payload = {
      msisdn: msisdn,
      type: bundle_Category,
      subtype: bundle_SubCategory
    };
    const getRoamingvoiceURl = '/api/getRoamingvoice';
    // return this._http.post(getRoamingvoiceURl, payload).toPromise().then(resp => {
    //   return Promise.resolve(resp.json());
    // }).catch((err) => {
    //   Promise.reject(err);
    // });
    return this.apiService.postWithSessionValidation(getRoamingvoiceURl, msisdn, sessionId, payload)
  }


  //getRoamingdataandvoice
  getRoamingdataandvoice(msisdn: string, bundle_Category: string, bundle_SubCategory: string) {
    let sessionId = this._storeVal.getSessionId();
    const payload = {
      msisdn: msisdn,
      type: bundle_Category,
      subtype: bundle_SubCategory
    };
    const getRoamingdataandvoiceURl = '/api/getRoamingdataandvoice';
    // return this._http.post(getRoamingdataandvoiceURl, payload).toPromise().then(resp => {
    //   return Promise.resolve(resp.json());
    // }).catch((err) => {
    //   Promise.reject(err);
    // });
    return this.apiService.postWithSessionValidation(getRoamingdataandvoiceURl, msisdn, sessionId, payload)
  }

  getBundleCatCISCountry(msisdn: string, action: string) {
    let sessionId = this._storeVal.getSessionId();

    const payload = {
      msisdn: msisdn,
      ProductId: action


    };

    const buybundleXtraTalkCatURL = '/api/getBundleCatCISCountry ';
    //return this._http.post(buybundleXtraTalkCatURL, payload).toPromise().then(resp => {

    //return Promise.resolve(resp);
    //}).catch((err) => {
    //return  Promise.reject(err);
    // });
    return this.apiService.postWithSessionValidation(buybundleXtraTalkCatURL, msisdn, sessionId, payload)

  }


}
