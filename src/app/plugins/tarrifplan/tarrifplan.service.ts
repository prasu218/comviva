import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { config } from '../config';
import { StoreService } from '../datastore/datastore.service';
import { ApiService } from '../commonAPI';

@Injectable({
  providedIn: 'root'
})
export class TarrifplanService {

  constructor(private _http: Http, private _ds: StoreService,private apiService: ApiService) { }

  getcheckPlan(msisdn:string) { 
    let sessionId = this._ds.getSessionId();
    const payload = {
      msisdn: msisdn,
    
    };
    //console.log('getTarrifPlans---msisdn' + msisdn);
    //console.log('getTarrifPlans---planName' + planName);
    const getTarrifPlansURL ='/api/getcheckPlan';
    // return this._http.post(getTarrifPlansURL, payload).toPromise().then((resp) => {
    //   const response = resp.json();
    //   console.log("<<<<<<<<<>>>>>>>>>>>>" + resp)
    //   return Promise.resolve(response);
    // }).catch(err => Promise.reject(err));
    return this.apiService.postWithSessionValidation(getTarrifPlansURL,msisdn,sessionId,payload);

   }



   migratePlan(msisdn:string,ProductID:string) { 
    let sessionId = this._ds.getSessionId(); 
    const payload = {
      msisdn: msisdn,
      productId:ProductID
    };
    const migrateTariffPlanChangeURL ='/api/migrateTariffPlan';
    // return this._http.post(migrateTariffPlanChangeURL, payload).timeout(config.TIME_OUT)
    // .toPromise().then(resp => {
    //   return resp.json();
    // }).catch(err => Promise.reject(err));
    return this.apiService.postWithSessionValidation(migrateTariffPlanChangeURL,msisdn,sessionId,payload);
   }
  
  
}
