import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../commonAPI';
import { StoreService } from '../../plugins/datastore';

@Injectable()
export class DbCallsService {

    public msisdn: string;

    constructor(private _http: Http, private http: HttpClient,
        private apiService: ApiService, private _storeVal: StoreService) { }

    saveDebitcardDetails(payment_id: string, msisdn: string, firstName: string, mtnepg_cust_mobile: string, email: string, amount: string, transaction_status, transaction_type, onlyAction) {
        const debitcardDetailsURL = '/api/saveDebitCardPaymentStatus';
        let sessionId = this._storeVal.getSessionId();
        const payload = {
            pg_trans_id: payment_id,
            cust_id: msisdn,
            cust_name: firstName,
            cust_mobile: mtnepg_cust_mobile,
            cust_email: email,
            transaction_amount: amount,
            transaction_status: transaction_status,
            transaction_type: transaction_type,
            only_action : onlyAction
        };

        // console.log("Debit_card service" +msisdn);
        // console.log("Debit_card service" +self_amount);
        // console.log("Debit_card service" +email);
        // console.log("Debit_card service" +firstName);

        // return this._http.post(debitcardDetailsURL, payload)
        //     .toPromise().then(resp => {
        //         console.log("service resp" + resp);
        //         return Promise.resolve(resp.json());
        //     }).catch(err => Promise.reject(err));

        return this.apiService.postWithSessionValidation(debitcardDetailsURL, msisdn, sessionId, payload)
    }

    setDebit() {
        // /airtimeType", airtimeType 
        // /cardType",
         const typeurl = '/api/cardType';
         let sessionId = this._storeVal.getSessionId();
         let msisdn = this._storeVal.getMSISDNData();
         const payload = {
             type: "airtime",
             msisdn
         };
         return this.apiService.postWithSessionValidation(typeurl, msisdn, sessionId, payload);
     }
 
     setAirtime() {
         // /airtimeType", airtimeType 
         // /cardType",
          const typeurl = '/api/airtimeType';
          let sessionId = this._storeVal.getSessionId();
          let msisdn = this._storeVal.getMSISDNData();
          const payload = {
              type: "airtime",
              msisdn
          };
          return this.apiService.postWithSessionValidation(typeurl, msisdn, sessionId, payload);
      }


    getDebitcardDetails(mtnepg_transaction_ref, transaction_status, msisdn) {
        const DebitcardDetailsURL ='/api/getDebitCardPaymentStatuses';
        let sessionId = this._storeVal.getSessionId();
        //console.log("DebitcardDetailsURL" + DebitcardDetailsURL);
        //     pg_trans_id: req.body.mtnepg_transaction_ref, 
        //   transaction_status: req.body.transaction_status, 
        //   transaction_type: req.body.transaction_status

      const payload = {
            mtnepg_transaction_ref: mtnepg_transaction_ref,
            transaction_status: transaction_status,
            msisdn: msisdn
        };


   return this.apiService.postWithSessionValidation(DebitcardDetailsURL, msisdn, sessionId, payload)

        // return this._http.post(DebitcardDetailsURL,payload, {headers: headers})
        //     .toPromise().then(resp => {
        //         //console.log("getDebitCardPaymentStatuses" + resp);
        //         return Promise.resolve(resp.json());
        //     }).catch(err => Promise.reject(err));
    }


    saveAirtimeDetails(payment_id: string, msisdn: string, amount: string, mtnepg_cust_mobile: string, transaction_status) {
        let sessionId = this._storeVal.getSessionId();
        const debitcardDetailsURL = '/api/saveAirtimePaymentStatus';
        const payload = {
            pg_trans_id: payment_id,
            cust_id: msisdn,
            cust_mobile: mtnepg_cust_mobile,
            transaction_amount: amount,
            transaction_status: transaction_status
        };

        // console.log("Debit_card service" +msisdn);
        // console.log("Debit_card service" +self_amount);
        // console.log("Debit_card service" +email);
        // console.log("Debit_card service" +firstName);

        // return this._http.post(DebitcardDetailsURL, payload)
        //     .toPromise().then(resp => {
        //         console.log("service resp" + resp);
        //         return Promise.resolve(resp.json());
        //     }).catch(err => Promise.reject(err));

        return this.apiService.postWithSessionValidation(debitcardDetailsURL, msisdn, sessionId, payload)
    }
	
	
	getDebitCardTransStatus(mtnepg_transaction_ref, transaction_status, msisdn) {
        const debitCardTransStatusURL = '/api/getDebitCardTransStatus';
        let sessionId = this._storeVal.getSessionId();
        const payload = {
            mtnepg_transaction_ref: mtnepg_transaction_ref,
            transaction_status: transaction_status,
            msisdn: msisdn
        };
        return this.apiService.postWithSessionValidation(debitCardTransStatusURL, msisdn, sessionId, payload)
    }

}
