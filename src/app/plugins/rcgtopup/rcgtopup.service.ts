import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http,Response} from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { ApiService } from '../commonAPI';
import { StoreService } from '../../plugins/datastore'
@Injectable()
export class rcgtopupService {

    public msisdn:string;

    constructor(private _http: Http,private http: HttpClient,private apiService: ApiService,private _storeVal: StoreService) { }

    getTopupDetails(msisdn: string,voucher_code: string) {
        let sessionId = this._storeVal.getSessionId();

        const TopupRcgDetailsURL:string ='/api/rcgtopup';
        //console.log("LoginRcgDetailsURL" +TopupRcgDetailsURL);
        const payload = {
            msisdn: msisdn,
            voucher_code: voucher_code
        };
        //console.log("msisdn service" +msisdn);
        //console.log("voucher_code service" +voucher_code);

        //return this._http.post(TopupRcgDetailsURL, payload).timeout(config.TIME_OUT)
        // .toPromise().then(resp => {
        //     console.log("service resp" +resp);
        // return Promise.resolve(resp.json());
        // }).catch(err => Promise.reject(err));
        return this.apiService.postWithSessionValidation(TopupRcgDetailsURL,msisdn,sessionId,payload)
    }

    getTopupDetailsOthers(msisdn: string,voucher_code: string) {
        let sessionId = this._storeVal.getSessionId();
        const TopupRcgDetailsOtherURL:string ='/api/rcgtopup';
        //console.log("LoginRcgDetailsURL" +TopupRcgDetailsOtherURL);
        const payload = {
            msisdn: msisdn,
            voucher_code: voucher_code
        };
        // console.log("msisdn service" +msisdn);
        // console.log("voucher_code service" +voucher_code);

        // return this._http.post(TopupRcgDetailsOtherURL, payload).timeout(config.TIME_OUT)
        // .toPromise().then(resp => {
        //     console.log("service resp" +resp);
        // return Promise.resolve(resp.json());
        // }).catch(err => Promise.reject(err));
        return this.apiService.postWithSessionValidation(TopupRcgDetailsOtherURL,msisdn,sessionId,payload);
    }

}