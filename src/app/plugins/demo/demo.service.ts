import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response} from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DemoService {

  constructor(private _http: Http,private http: HttpClient) { }

    // getHeaderDetails(msisdn: string) {
    getHeaderDetails() {
        const headersURL = config.HTTP_PLUGIN_URL + '/api/getheaders';

        return this._http.post(headersURL, null).toPromise().then(resp => {

            return Promise.resolve(resp.json());
        }).catch(err => Promise.reject(err));
    }

    getMsisdnDetails() {
        const getHeaderMsisdnURL = config.HTTP_PLUGIN_URL + '/api/getmsisdn';
        return this._http.post(getHeaderMsisdnURL, null).toPromise().then(resp => {
            return Promise.resolve(resp.json());
        }).catch(err => Promise.reject(err));
    }
}