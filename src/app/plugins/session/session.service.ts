import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { StoreService } from '../datastore';

@Injectable()
export class SessionService {

  constructor(private _http: Http, private http: HttpClient, private storeService: StoreService) { }

  async createSessionId(msisdn: string) {
    const sessionURL = config.PLUGIN_URL + '/api/createSessionStatus';
    //console.log("accountHistoryURL:::", sessionURL);
    const payload = {
      cust_id: msisdn
    };

    let token = '';
    // if(sessionStorage.getItem('chakra')){
    //   token = sessionStorage.getItem('chakra');
    //     }
    if(this.storeService.getToken() != null){
      token = this.storeService.getToken()
    }
      else{
      token = '';
      }
const headers  = new Headers();
//token = 'ehjdkhjkhf';
//if(path == '/api/linkedaccountlist'){
await headers.append('Authorization', 'Bearer ' + token);
await headers.append('Content-Type', 'application/json');

    return this._http.post(sessionURL, payload, {headers: headers }).toPromise().then(resp => {
      //console.log("service resp" + resp);
      return Promise.resolve(resp.json());
    }).catch(err => Promise.reject(err));
  }

  validateSession(msisdn: string, sessionId: string) {
    const validateSessionURL = config.PLUGIN_URL + '/api/getSessionStatus';
    //console.log("accountHistoryURL:::", validateSessionURL);
    const payload = {
      cust_id: msisdn,
      session_id: sessionId
    };

    let token = '';
    // if(sessionStorage.getItem('chakra')){
    //   token = sessionStorage.getItem('chakra');
    //     }
    if(this.storeService.getToken() != null){
      token = this.storeService.getToken()
    }
      else{
      token = '';
      }
const headers  = new Headers();
//token = 'ehjdkhjkhf';
//if(path == '/api/linkedaccountlist'){
headers.append('Authorization', 'Bearer ' + token);
headers.append('Content-Type', 'application/json');

    return this._http.post(validateSessionURL, payload, { headers: headers}).toPromise().then(resp => {
      console.log("service resp" + resp);
      return Promise.resolve(resp.json());
    }).catch(err => Promise.reject(err));
  }
}
