import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Jsonp, Headers } from '@angular/http';
import { Response } from '@angular/http';
// import { airtimeBonusDACodes } from '../balance/index';
import { Common_Strings } from '../../strings/strings';
import { CommonConfig } from '../common';
import { StoreService } from '../datastore';
// import 'Rxjs/rs'

@Injectable()
export class CommonService {

    subtype: string;
    serviceID: string = "N.A.";

    constructor(private _http: Http, private storeService: StoreService) { }

    getServiceClass(msisdn: string) {
        const balanceDetailsURL = config.PLUGIN_URL + '/api/balance';
        const payload = {
            msisdn: msisdn
        };

        let tokdat;
              // sessionStorage.getItem('chakra')
              if(this.storeService.getToken()){
                // tokdat = sessionStorage.getItem('chakra');
                tokdat = this.storeService.getToken();
               }
        else{
          tokdat = '';
        }
        const headers: Headers = new Headers();
        //if(path == '/api/linkedaccountlist'){
          headers.append('Authorization', 'Bearer ' + tokdat);
          headers.append('Content-Type', 'application/json');

        // return this._http.post(balanceDetailsURL, payload).timeout(config.TIME_OUT)
        return this._http.post(balanceDetailsURL, payload, { withCredentials: true, headers:headers }).toPromise()
            .then(resp => {
                let jsonObj = resp.json();

                if (jsonObj) {
                    var jsonResult = jsonObj.result;
                    if (jsonResult) {

                        this.serviceID = jsonResult[0].planName;
                    }
                }
                return this.serviceID;
                //return Promise.resolve(resp.json());
            })
            .catch(err => Promise.reject(err));

    }

    isStaffNum(msisdn: string) {
        var serviceclass = this.getServiceClass(msisdn);
        if (Number(serviceclass) == 402 || Number(serviceclass) == 413) {

            return true;
        }
        else {

            return false;
        }
    }

    splitAmt(amount, noSpace) {
        amount = amount + "";
        noSpace = false;
        var amtSep = ".";
        var splitAmount = amount.split(".");
        if (!noSpace) {
            var amount1 = "";
            var len = splitAmount[0].length;
            for (var i = len; i >= 0; i -= 3) {
                if (i > 3)
                    amount1 = amtSep + (splitAmount[0].substring(i - 3, i)) + amount1;
                else
                    amount1 = (splitAmount[0].substr(0, i)) + amount1;

            }
            splitAmount[0] = amount1;
        }

        if (config.ALLOW_CENTS) {
            if (splitAmount[1] == undefined) {
                splitAmount[1] = ".00";
            }
            else if (splitAmount[1].length < 2) {
                splitAmount[1] = "." + splitAmount[1] + "0";
            }
            else {
                splitAmount[1] = "." + splitAmount[1];
            }
        }
        else {
            splitAmount[1] = "";
        }
        return splitAmount;
    }

    getInitialDate(incomDate) {
        var dd = incomDate.getDate();
        var mm = incomDate.getMonth() + 1; //January is 0!
        var yyyy = incomDate.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var toCurrentDate = dd + '/' + mm + '/' + yyyy;
        return toCurrentDate;

    }


    parseDate(date) {
        var modifiedDate = "";
        var dateFormat = CommonConfig.DATE_FORMAT;
        var dateSeperator = CommonConfig.DATE_SEPERATOR.replace(/\s+$/, '');

        var yyyy = "";
        var mm = "";
        var dd = "";

        if (date) {

            date = date.replace(/-/g, "/");
            date = date.replace(/:/g, "/");

            if (date.indexOf("/") < 3) {
                yyyy = date.slice(6, 10);
                mm = date.slice(3, 5);
                dd = date.slice(0, 2);
            }
            else {
                yyyy = date.slice(0, 4);
                mm = date.slice(5, 7);
                dd = date.slice(8, 10);
            }

            if (dateFormat == 1) { // dd/mm/yyy
                modifiedDate = dd + dateSeperator + mm + dateSeperator + yyyy;
            }
            else if (dateFormat == 2) { // mm/dd/yyy
                modifiedDate = mm + dateSeperator + dd + dateSeperator + yyyy;
            }
            else if (dateFormat == 3) { // yyyy/mm/dd
                modifiedDate = yyyy + dateSeperator + mm + dateSeperator + dd;
            }
        }
        else {
            modifiedDate = Common_Strings.STR_NA;
        }

        return modifiedDate;
    }

    formMSISDN(msisdn) {
        if (msisdn.length == (Number(CommonConfig.MSISDN_LENGTH)) && msisdn.charAt(0) != "0") {
            return CommonConfig.COUNTRY_CODE + msisdn;
        } else if (msisdn.length == (Number(CommonConfig.MSISDN_LENGTH) + 1) && msisdn.charAt(0) == "0") {
            return CommonConfig.COUNTRY_CODE + msisdn.slice(-CommonConfig.MSISDN_LENGTH);
        } else if (msisdn.length == (Number(CommonConfig.MSISDN_LENGTH) + Number(CommonConfig.COUNTRY_CODE.toString().length)) && msisdn.substring(0, CommonConfig.COUNTRY_CODE.toString().length) == CommonConfig.COUNTRY_CODE) {
            return msisdn;
        } else if (msisdn.length == (Number(CommonConfig.MSISDN_LENGTH) + Number(CommonConfig.COUNTRY_CODE.toString().length) + 1) && msisdn.substring(1, (CommonConfig.COUNTRY_CODE.toString().length + 1)) == CommonConfig.COUNTRY_CODE) {
            return msisdn.substring(1, (Number(CommonConfig.MSISDN_LENGTH) + Number(CommonConfig.COUNTRY_CODE.toString().length) + 1));
        } else {
            return 1;
        }
    }
}
