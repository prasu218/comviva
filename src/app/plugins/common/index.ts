export * from './common.service';

export const CommonConfig = {
    DATE_FORMAT : 1,
    DATE_SEPERATOR: "/",
    MSISDN_LENGTH : 10,
    MSISDN_LENGTH_TEN : 10,
    MSISDN_LENGTH_ELEVEN : 11,
    MSISDN_LENGTH_THIRTEEN : 13 ,
    MSISDN_LENGTH_FOURTEEN : 14,
    COUNTRY_CODE : 234,
    SENDER_EMAIL_ADDRESS : "myMTN@mtn.com"

};
