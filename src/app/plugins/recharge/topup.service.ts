import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Jsonp } from '@angular/http';
import { Common_Strings } from '../../strings/strings';
import { ApiService } from '../commonAPI';
import { StoreService } from '../../plugins/datastore'

@Injectable()
export class TopupService {
    rechargeCardObj: any = {};
    damagedCardObj: any = {};
    subtype: string;

    constructor(private _http: Http, private apiService: ApiService, private _storeVal: StoreService) { }

    //The following will invoke top up card or damaged based on the value of damagedFlag
    // damagedFlag = True [Damaged PIN], damagedFlag = False [Topup Card],
    getTopUpCardAPI(msisdn: string, serial_number: string, voucher_code: string, damagedFlag: boolean) {
        let sessionId = this._storeVal.getSessionId();
        if (damagedFlag == true) {
            const payload = {
                msisdn: msisdn,
                serial_number: serial_number,
                voucher_code: voucher_code
            };
            const damagedPINURL = '/api/scratched';

            // return this._http.post(damagedPINURL, payload)
            //     //return this._http.post(damagedPINURL, payload).timeout(config.TIME_OUT)
            //     .toPromise().then(resp => {
            //         this.rechargeCardObj = resp.json();
            //         return Promise.resolve(resp.json());
            //     }).catch(err => Promise.reject(err));

            return this.apiService.postWithSessionValidation(damagedPINURL, msisdn, sessionId, payload)
        }
        else {
            const payload = {
                msisdn: msisdn,
                voucher_code: voucher_code
            };
            const rechargesURL = '/api/recharge';
            //return this._http.post(rechargesURL, payload).timeout(config.TIME_OUT)

            // return this._http.post(rechargesURL, payload)
            //     .toPromise().then(resp => {
            //         this.damagedCardObj = resp.json();
            //         return Promise.resolve(resp.json());
            //     }).catch(err => Promise.reject(err));

            return this.apiService.postWithSessionValidation(rechargesURL, msisdn, sessionId, payload)
        }
    }
}