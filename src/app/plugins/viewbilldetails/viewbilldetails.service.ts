import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { config } from '../config';
import { StoreService } from '../datastore/datastore.service';
import { ApiService } from '../commonAPI';
import { SessionService } from '../session';

@Injectable()
export class ViewBillDetailsService {

  sessionId: any;

  constructor(private _http: Http, private _ds: StoreService,
    private sessionService: SessionService, private apiService: ApiService) { }

  viewbilldetailsAPI1(msisdn: string) {
    const viewbilldetailsURL = '/api/viewbilldetails1';
    const payload = {
      msisdn: msisdn
    };
    this.sessionId = this._ds.getSessionId();

    // return this._http.post(viewbilldetailsURL, payload).toPromise().then(resp => {
    //   const response = resp.json();
    //   //return Promise.resolve(resp.json());
    //   return Promise.resolve(response);
    // }).catch(err => Promise.reject(err));

    return this.apiService.postWithSessionValidation(viewbilldetailsURL, msisdn, this.sessionId, payload)
  }

}
