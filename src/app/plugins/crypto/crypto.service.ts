import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { config } from '../config';

@Injectable()
export class CryptoService {

  constructor() { }

  encryptData(value) {
    let encrypt = CryptoJS.AES.encrypt(value, config.CRYPTO_KEY).toString();
    if (encrypt) {
      return encrypt;
    }
  }

  decryptData(value) {
    var decrytedData = CryptoJS.AES.decrypt(value, config.CRYPTO_KEY);
    var plaintext = decrytedData.toString(CryptoJS.enc.Utf8);
    return plaintext;
  }
}
