import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { config } from '../config';
import { StoreService } from '../datastore/datastore.service';
import { ApiService } from '../commonAPI';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  sessionId: any;

  constructor(private _http: Http, private _ds: StoreService, private apiService: ApiService) { }

  getMySubscriptionList(msisdn: string) {

    const payload = {
      msisdn: msisdn
    };
    this.sessionId = this._ds.getSessionId();

    const mySubscriptionListURL = '/api/subscription';
    let mySubscriptionListData = [];

    // return this._http.post(mySubscriptionListURL, payload).toPromise().then(resp => {
    //   //console.log('vasSubscriptionURLresponsssssssss'+JSON.stringify(resp));
    //   return Promise.resolve(resp.json());
    // })
    // .catch(err => Promise.reject(err));
    return this.apiService.postWithSessionValidation(mySubscriptionListURL, msisdn, this.sessionId, payload);
  }



  vasSubscription(msisdn: string, ProductID: any) {

    const payload = {
      msisdn: msisdn,
      ProductID: ProductID,
      //text:text,

    };
    const vasSubscriptionURL = '/api/Subscriptionnew';
    //return Promise.resolve(this.vasSubscriptionResponse);
    // return this._http.post(vasSubscriptionURL, payload)
    // .toPromise().then(resp => {
    //   //console.log('vasSubscriptionURLresponsssssssss'+ JSON.stringify(resp));
    //   return Promise.resolve(resp.json());

    // }).catch(err => Promise.reject(err));
    return this.apiService.postWithSessionValidation(vasSubscriptionURL, msisdn, this.sessionId, payload);
  }

  unSubscribeVas(msisdn: string, ProductID: any) {
    const payload = {
      msisdn: msisdn,
      ProductID: ProductID
    };
    const unSubscribeVasURL = '/api/unSubscribeVas';
    // return this._http.post(unSubscribeVasURL, payload)
    //   .toPromise().then(resp => {
    //     return Promise.resolve(resp.json());
    //   }).catch(err => Promise.reject(err));
    return this.apiService.postWithSessionValidation(unSubscribeVasURL, msisdn, this.sessionId, payload)
  }


  getactiveservicelist(msisdn: string, ProductID: any) {
    const payload = {
      msisdn: msisdn,
      ProductID: ProductID
    };
    const getactiveservicelistURL = '/api/getactiveservicelist';
    // return this._http.post(getactiveservicelistURL, payload, { withCredentials: true })
    //   .toPromise().then(resp => {
    //     return resp.json();
    //   }).catch(err => Promise.reject(err));
    return this.apiService.postWithSessionValidation(getactiveservicelistURL, msisdn, this.sessionId, payload)
  }

}
