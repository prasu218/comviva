import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class StoreService {
  public msisdn_store: string;
  public firstName_store: string;
  public familyName_store: string;
  public alternateNumber_store: string;
  public activationDate_store: string;
  public email_store: string;
  public email_store1: string;
  public otp_store: string;
  public profilproce_store: string;
  public buybundale_store: string;
  public primary_email_store: string;

  constructor(private router: Router) {
    this.msisdn_store = '';
    this.activationDate_store = '';
    this.firstName_store = '';
    this.familyName_store = '';
    this.alternateNumber_store = '';
    this.email_store = '';
    this.primary_email_store = '';
    this.otp_store = '';
    this.buybundale_store = '';
    this.profilproce_store = '';
  }
 settopupDetails(data) {
    localStorage.setItem('selected_topup', JSON.stringify(data));
  }
  gettopupDetails() {
    return JSON.parse(localStorage.getItem('selected_topup'));
  }

  setselectradioRech(data: any) {
    const radioObj = { msisdn: data };
    localStorage.setItem('radioValue', JSON.stringify(radioObj));
  }
  getselectradioRech() {
    return JSON.parse(localStorage.getItem('radioValue'));
  }


  getToken(){
    return localStorage.getItem('chakra');
  }
  setToken(token: any){
    localStorage.setItem("chakra", token);
  }

  getMainToken(){
    return localStorage.getItem('chakramain');
  }
  setMainToken(token: any){
    localStorage.setItem("chakramain", token);
  }

  
  setMSISDNupData(data: any) {
    const msisdnObj = { msisdn: data };
    localStorage.setItem('msisdn_store1', JSON.stringify(msisdnObj));
  }

  getMSISDNupData() {
    if (localStorage.getItem('msisdn_store1')) {
      return JSON.parse(localStorage.getItem('msisdn_store1')).msisdn;
    } else {
      return null;
    }
  }

  setEmailupData(data: string) {
    localStorage.setItem('email_store1', JSON.stringify(data));
  }

  getEmailupData() {
    return JSON.parse(localStorage.getItem('email_store1'));
  }

  // profilepreoceed

  setproceprofileData(data: any) {
    localStorage.setItem('profilproce_store', JSON.stringify(data));
  }

  getproceprofileData() {
    return JSON.parse(localStorage.getItem('profilproce_store'));
  }

  // MSISDN Data
  setMSISDNData(data: any) {
    const msisdnObj = { msisdn: data };
    localStorage.setItem('msisdn_store', JSON.stringify(msisdnObj));
  }

  getMSISDNData() {
    if (localStorage.getItem('msisdn_store')) {
      return JSON.parse(localStorage.getItem('msisdn_store')).msisdn;
    } else {
      return null;
    }
  }

  setDebitAmt(data: string) {
    localStorage.setItem('debit_amount', JSON.stringify(data));
  }

  getDebitAmt() {
    return JSON.parse(localStorage.getItem('debit_amount'));
  }

  setAirtimeAmt(data: string) {
    localStorage.setItem('airtime_amount', JSON.stringify(data));
  }

  getAirtimeAmt() {
    return JSON.parse(localStorage.getItem('airtime_amount'));
  }

  //AlternateNumber
  setAlternateNumberData(data: number) {
    const msisdnObj = { msisdn: data };
    localStorage.setItem('alternateNumber_store', JSON.stringify(msisdnObj));
  }

  getAlternateNumberData() {
    if (localStorage.getItem('alternateNumber_store')) {
      return JSON.parse(localStorage.getItem('alternateNumber_store')).msisdn;
    } else {
      return null;
    }
  }

  //activationDate_store
  setActivationDate(data: number) {
    const msisdnObj = { msisdn: data };
    localStorage.setItem('activationDate_store', JSON.stringify(msisdnObj));
  }

  getActivationDate() {
    if (localStorage.getItem('activationDate_store')) {
      return JSON.parse(localStorage.getItem('activationDate_store')).msisdn;
    } else {
      return null;
    }
  }

  //Familyname data

  setFamilyNameData(data: string) {
    localStorage.setItem('familyName_store', JSON.stringify(data));
  }

  getFamilyNameData() {
    return JSON.parse(localStorage.getItem('familyName_store'));
  }

  //FirstName data

  setFirstNameData(data: string) {
    localStorage.setItem('firstName_store', JSON.stringify(data));
  }

  getFirstNameData() {
    return JSON.parse(localStorage.getItem('firstName_store'));
  }

  //set substype customer profile data typw post paid or prepaid 

  setSubsTypeData(data: string) {
    localStorage.setItem('subsType_store', JSON.stringify(data));
  }

  getSubsTypeData() {
    return JSON.parse(localStorage.getItem('subsType_store'));
  }

  // CheckplanNameData 

  setplanNameData(data: string) {
    localStorage.setItem('planName_store', JSON.stringify(data));
  }

  getplanNameData() {
    return JSON.parse(localStorage.getItem('planName_store'));
  }

  //email
  setEmailData(data: string) {
    localStorage.setItem('email_store', JSON.stringify(data));
  }

  getEmailData() {
    return JSON.parse(localStorage.getItem('email_store'));
  }

  //otp
  setOtpData(data: string) {
    localStorage.setItem('otp_store', JSON.stringify(data));
  }

  getOtpData() {
    return JSON.parse(localStorage.getItem('otp_store'));
  }

  //paymentdatatbuybundle
  setbuybundleinternalData(data: string) {
    localStorage.setItem('buybundale_store', JSON.stringify(data));
  }

  getbuybundleinternalData() {
    return JSON.parse(localStorage.getItem('buybundale_store'));
  }

  //BuyBundlePlanDetails
  setBuyBundlePlanDetails(data) {
    localStorage.setItem('selected_bundle_detail', JSON.stringify(data));
  }

  getBuyBundlePlanDetails() {
    return JSON.parse(localStorage.getItem('selected_bundle_detail'));
  }

  setAccountTag(data: {}) {
    const tagObj = { tag: data };
    localStorage.setItem('tags_store', JSON.stringify(tagObj));
  }

  getAccountTag() {
    if (localStorage.getItem('tags_store')) {
      return JSON.parse(localStorage.getItem('tags_store')).tag;
    } else {
      return null;
    }
  }

  setSecondaryMSISDNData(data: any) {
    const sec_msisdnObj = { sec_msisdn: data };
    localStorage.setItem('sec_msisdn_store', JSON.stringify(sec_msisdnObj));
  }

  setLinkedAccount(data: any) {
    const sec_linkedaccntObj = { sec_linkedaccnt: data };
    localStorage.setItem('linkedaccnt_store', JSON.stringify(sec_linkedaccntObj));
  }

  getLinkedAccount() {
    if (localStorage.getItem('linkedaccnt_store')) {
      return JSON.parse(localStorage.getItem('linkedaccnt_store')).sec_linkedaccnt;
    } else {
      return null;
    }
  }

  getSecondaryMSISDNData() {
    if (localStorage.getItem('sec_msisdn_store')) {
      return JSON.parse(localStorage.getItem('sec_msisdn_store')).sec_msisdn;
    } else {
      return null;
    }
  }

  setSecondaryAlternateNumberData(data: number) {
    const sec_msisdnObj = { sec_msisdn: data };
    localStorage.setItem('sec_alternateNumber_store', JSON.stringify(sec_msisdnObj));
  }

  getSecondaryAlternateNumberData() {
    if (localStorage.getItem('sec_alternateNumber_store')) {
      return JSON.parse(localStorage.getItem('sec_alternateNumber_store')).sec_msisdn;
    } else {
      return null;
    }
  }

  setSecondaryFamilyNameData(data: string) {
    localStorage.setItem('sec_familyName_store', JSON.stringify(data));
  }

  getSecondaryFamilyNameData() {
    return JSON.parse(localStorage.getItem('sec_familyName_store'));
  }

  setSecondaryFirstNameData(data: string) {
    localStorage.setItem('sec_firstName_store', JSON.stringify(data));
  }

  getSecondaryFirstNameData() {
    return JSON.parse(localStorage.getItem('sec_firstName_store'));
  }

  setSecondarySubsTypeData(data: string) {
    localStorage.setItem('sec_subsType_store', JSON.stringify(data));
  }

  getSecondarySubsTypeData() {
    return JSON.parse(localStorage.getItem('sec_subsType_store'));
  }

  setSecondaryAccount(data: boolean) {
    localStorage.setItem('secondaccount_store', JSON.stringify(data));
  }

  getSecondaryAccount() {
    return JSON.parse(localStorage.getItem('secondaccount_store'));
  }

  setSecondaryEmailData(data: string) {
    localStorage.setItem('sec_email_store', JSON.stringify(data));
  }

  getSecondaryEmailData() {
    return JSON.parse(localStorage.getItem('sec_email_store'));
  }

  setSecondaryAccountfalse(data: boolean) {
    localStorage.setItem('secondaccount_store_false', JSON.stringify(data));
  }

  getSecondaryAccountfalse() {
    return JSON.parse(localStorage.getItem('secondaccount_store_false'));
  }

  setPrimaryMSISDNData(data: any) {
    const primary_msisdnObj = { primary_msisdn: data };
    localStorage.setItem('primary_msisdn_store', JSON.stringify(primary_msisdnObj));
  }

  getPrimaryMSISDNData() {
    if (localStorage.getItem('primary_msisdn_store')) {
      return JSON.parse(localStorage.getItem('primary_msisdn_store')).primary_msisdn;
    } else {
      return null;
    }
  }

  setPrimaryAlternateNumberData(data: number) {
    const msisdnObj = { msisdn: data };
    localStorage.setItem('primary_alternateNumber_store', JSON.stringify(msisdnObj));
  }

  getPrimaryAlternateNumberData() {
    if (localStorage.getItem('primary_alternateNumber_store')) {
      return JSON.parse(localStorage.getItem('primary_alternateNumber_store')).msisdn;
    } else {
      return null;
    }
  }

  setPrimaryFamilyNameData(data: string) {
    localStorage.setItem('primary_familyName_store', JSON.stringify(data));
  }

  getPrimaryFamilyNameData() {
    return JSON.parse(localStorage.getItem('primary_familyName_store'));
  }

  setPrimaryFirstNameData(data: string) {
    localStorage.setItem('primary_firstName_store', JSON.stringify(data));
  }

  getPrimaryFirstNameData() {
    return JSON.parse(localStorage.getItem('primary_firstName_store'));
  }

  setPrimarySubsTypeData(data: string) {
    localStorage.setItem('primary_subsType_store', JSON.stringify(data));
  }

  getPrimarySubsTypeData() {
    return JSON.parse(localStorage.getItem('primary_subsType_store'));
  }

  setPrimaryEmailData(data: string) {
    localStorage.setItem('primary_email_store', JSON.stringify(data));
  }

  getPrimaryEmailData() {
    return JSON.parse(localStorage.getItem('primary_email_store'));
  }

  getSessionId() {
    return localStorage.getItem("session_id")
  }

  setSessionId(session_id) {
    localStorage.setItem('session_id', session_id);
  }

  setSessionStatus(sessionStatus) {
    localStorage.setItem('session_status', sessionStatus);
  }

  getSessionStatus() {
    return localStorage.getItem("session_status");
  }

  setMTNOnlineProductId(product_id) {
    localStorage.setItem('mtnOnline_prodId', product_id);
  }

  getMTNOnlineProductId() {
    return localStorage.getItem("mtnOnline_prodId");
  }

  deleteMTNOnlineProdId() {
    localStorage.removeItem('mtnOnline_prodId');
  }
}
