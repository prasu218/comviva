import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { config } from '../config';
@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private _http: Http, ) { }
  feedbackdetailsAPI(msisdn: string, eMailId: string, supp_id: string, ccmailid: string, subject: string, mailbody: string) {
    const payload = {
      msisdn: msisdn,
      eMailId: eMailId,
      supp_id: supp_id,
      ccmailid: ccmailid,
      subject: encodeURIComponent(subject),
      mailbody: encodeURIComponent(mailbody),
    };
    const feedbackURL = config.PLUGIN_URL + '/api/feedback';

    return this._http.post(feedbackURL, payload).timeout(config.TIME_OUT)
      .toPromise().then(resp => {
        return Promise.resolve(resp.json());
      }).catch(err => {
        return Promise.reject(err)
      });
  }

  getUserLocationAddress(latitude, longitude) {
    const googleMapURL = "https://maps.googleapis.com/maps/api/geocode/json?key="+config.GOOGLE_MAP_API_KEY+"&latlng="+latitude+","+longitude+"&sensor=false";
    return this._http.get(googleMapURL).timeout(config.TIME_OUT)
    .toPromise().then(resp => {
      return Promise.resolve(resp.json());
    }).catch(err => {
      return Promise.reject(err)
    });
  }
}