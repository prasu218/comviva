import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProceedcomplaintService {

  constructor() { }

  private msisdn:any;
  
  private subCatId:any;
  
  private description:any;
  
  private priority:any;
  

   setData(msisdn:string,subCatId:string,priority:string,description:string){
     this.msisdn = msisdn;
     this.subCatId = subCatId;
     this.description = description;
     this.priority = priority;
    }

    getMsisdn():any{
        return this.msisdn;
    }
    getSubCatId():any{
      return this.subCatId;
  }
  getDescription():any{
    return this.description;
}
getPriority():any{
  return this.priority;
}

}
