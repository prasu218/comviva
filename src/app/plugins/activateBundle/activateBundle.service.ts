import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../commonAPI';
import { StoreService } from '../../plugins/datastore';

@Injectable()
export class ActivateBundleService {

    public msisdn: string;

    constructor(private _http: Http, private http: HttpClient, 
        private apiService: ApiService, private _storeVal: StoreService) { }

    activateBundle(msisdn: string, transactionId: string, amount,action,autoRenew, beneficiaryMsisdn) {
        let sessionId = this._storeVal.getSessionId();
        const LoginRcgDetailsURL = '/api/activateBundle';
        const payload = {
            msisdn: msisdn.toString(),
            transactionId: transactionId.toString(),
            amount: amount.toString(),
            action: action.toString(),
            autoRenew: autoRenew.toString(),
            beneficiaryMsisdn: beneficiaryMsisdn.toString()
        };

        //return this._http.post(LoginRcgDetailsURL, payload).toPromise().then(resp => {
        //console.log("service resp" +resp);
        //return Promise.resolve(resp.json());
        // }).catch(err => Promise.reject(err));

        return this.apiService.postWithSessionValidation(LoginRcgDetailsURL, msisdn, sessionId, payload)
    }

}
