import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { StoreService } from '../datastore/datastore.service';
import { CommonConfig } from 'src/app/plugins/common';
import 'rxjs/Rx';
import { ProfileService } from '../../plugins/profile';
import { ApiService } from '../commonAPI';

@Injectable()
export class otpService {

    constructor(private _http: Http,private _profile: ProfileService, private http: HttpClient, 
        private _ds: StoreService,  private apiService: ApiService  ) { }
    //The below function will invoke the generate and send OTP via SMS or EMail channel, based on the email Falg.
    //emaFlag = true, means email will be sent, if emaFlag = false, SMS will be sent
    // generateOTP(msisdn,emailFlag,eMailId,supp_id,ccmailid) {
        generateOTP(msisdn:string, emailFlag:boolean,linkAccountFlag:boolean) {
       const payload = {
            msisdn: msisdn.toString(),
            emailFlag: emailFlag.toString(),
            linkAccountFlag:linkAccountFlag.toString(),
            eMailId: CommonConfig.SENDER_EMAIL_ADDRESS, //Sender's Email Address - will be configured.
            supp_id: this._ds.getEmailData(), //Receiver's Email Address
            ccmailid: this._ds.getEmailData(), //Receiver's Email Address
	    firstName: this._ds.getFirstNameData()
        };

             /*var payload;
            if(linkAccountFlag){
                var otherSupp_id;
                var otherccmailid;
                var othername;
                this._profile.getProfileDetails(msisdn).then((resp) => {
                    otherSupp_id= resp.customer_Profile.email;
                    otherccmailid= resp.customer_Profile.email;
                    othername = "rajnish";
			
                });
            console.log("OtherName::::"+othername+"::::"+otherccmailid);
                payload = {
                    msisdn: msisdn.toString(),
                    emailFlag: emailFlag.toString(),
                    linkAccountFlag:linkAccountFlag.toString(),
                    eMailId: CommonConfig.SENDER_EMAIL_ADDRESS, //Sender's Email Address - will be configured.
                    supp_id: otherSupp_id, //Receiver's Email Address
                    ccmailid: otherccmailid, //Receiver's Email Address
                firstName: othername
                };
            }else{
         payload = {
            msisdn: msisdn.toString(),
            emailFlag: emailFlag.toString(),
            linkAccountFlag:linkAccountFlag.toString(),
            eMailId: CommonConfig.SENDER_EMAIL_ADDRESS, //Sender's Email Address - will be configured.
            supp_id: this._ds.getEmailData(), //Receiver's Email Address
            ccmailid: this._ds.getEmailData(), //Receiver's Email Address
	    firstName: this._ds.getFirstNameData()
        };
    }*/


        
          const otpURL = config.PLUGIN_URL + '/api/generateotp';
        // const otpURL = config.PLUGIN_URL + '/api/generateotp'; 
	//EMAIL_PLGUN_URL;
        // const otpURL = config.EMAIL_PLGUN_URL + '/api/generateotp';
        return this._http.post(otpURL,payload).timeout(50000).toPromise().then(resp => {
            return Promise.resolve(resp.json());
        }).catch(err => Promise.reject(err));
    }


    sendSwitchSuccess(msisdn:string,switchParent: string, switchName: string) {
        const sessionId = this._ds.getSessionId();
        const payload = {
            msisdn: msisdn.toString(),
            switchName,
            switchParent,
        };
          const messageUrl =  '/api/sendsms';
        
          return this.apiService.postWithSessionValidation(messageUrl, msisdn, sessionId, payload)
        // return this._http.post(otpURL,payload).timeout(50000).toPromise().then(resp => {
        //     return Promise.resolve(resp.json());
        // }).catch(err => Promise.reject(err));
    }


    validateOTP(otp, sessionOtp, msisdn) {
        // let sessionOtp = this._storeVal.getOtpData()       
        const payload = {
            otp: otp,
            sessionOtp : sessionOtp.toString(),
            msisdn
        };
        const validateOTPURL = config.PLUGIN_URL + '/api/validateOTP';
        //  const newotpURL =   config.PLUGIN_URL + '/api/getOTP';
        // this._http.post(validateOTPURL, payload, { withCredentials: true }).timeout(config.TIME_OUT)
        return this._http.post(validateOTPURL, payload).toPromise().then(resp => {
                return Promise.resolve(resp.json());
            }).catch(err => Promise.reject(err));
    }

     //The below function will invoke the re-generate and re-send OTP via SMS or EMail channel, based on the email Falg.
    resendOTP(msisdn: string, emailFlag: boolean) {
        const payload = {
            msisdn: msisdn,
            emailFlag: emailFlag,
        //    eMailId: CommonConfig.SENDER_EMAIL_ADDRESS, //Sender's Email Address - will be configured.
           // supp_id: this._ds.getEmailData(), //Receiver's Email Address
           // ccmailid: this._ds.getEmailData() //Receiver's Email Address
        };

        const otpURL = config.PLUGIN_URL + '/api/resendOTP';

        //  const newotpURL =   config.PLUGIN_URL + '/api/getOTP';
        // return this._http.post(newotpURL, payload,{withCredentials:true}).timeout(config.TIME_OUT)
        // .toPromise().then((resp) => {
        return this._http.post(otpURL, payload).toPromise().then(resp => {
            return Promise.resolve(resp.json());
        }).catch(err => Promise.reject(err));
    }
}


// import { Injectable } from '@angular/core';

// import { config } from '../config';
// import { Http, Response } from '@angular/http';

// import { HttpClient } from '@angular/common/http';



// @Injectable()
// export class otpService {

//     // public loginResponse: any = loginResponse_Array;
//     constructor(private _http: Http, private http: HttpClient) { }
//     getOTP(msisdn: string) {
//         const payload = {
//             msisdn: msisdn
//         };

//         const otpURL = config.PLUGIN_URL + '/api/otp';
        
//         return this._http.post(otpURL, payload).toPromise().then(resp => {
//             return Promise.resolve(resp.json());
//         }).catch(err => Promise.reject(err));
//     }

// }

