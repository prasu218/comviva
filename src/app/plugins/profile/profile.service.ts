
import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http,Response, Headers} from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { StoreService } from '../datastore';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ProfileService {

  public msisdn:string;
  private PUKURL: any;
  private store_cust_obj = null;
  private profilePic = new BehaviorSubject('URL');
  currentProfilePic = this.profilePic.asObservable();

  constructor(private _http: Http,private http: HttpClient,private storeData :StoreService) { }

  //generate token
  generateToken(msisdn: string){
    const profileDetailsURL : string = config.PLUGIN_URL + '/api/generateToken';

    const payload = {
        msisdn: msisdn
    };
    return this._http.post(profileDetailsURL, payload).toPromise().then(resp => {
            return Promise.resolve(resp.json());
        }).catch(err => Promise.reject(err));
  }
    // API Call for Profilr API
    getProfileDetails(msisdnOther: string) {
	
        const profileDetailsURL : string = config.PLUGIN_URL + '/api/profile';
	const storedMSISDN : string = this.storeData.getMSISDNData();
	if(storedMSISDN == null){
	 var payload = {
            msisdn: msisdnOther,
            msisdnother: msisdnOther,

        };

	}
	else{
        var payload = {
            msisdn: storedMSISDN,
	    msisdnother: msisdnOther,
	    
        };
	}
      let token = '';
      if(this.storeData.getToken() != null){
        token = this.storeData.getToken()
      }
      //        if(sessionStorage.getItem('chakra')){
      //   token = sessionStorage.getItem('chakra');
      // }
      else{
        token = '';
      }
      const headers  = new Headers();
      //token = 'ehjdkhjkhf';
      //if(path == '/api/linkedaccountlist'){
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        return this._http.post(profileDetailsURL, payload, {headers: headers }).toPromise().then(resp => {
                return Promise.resolve(resp.json());
            }).catch(err => Promise.reject(err));
    }

    //validate Profile Details
    validateProfileDetails(msisdn: string) {
      const profileDetailsURL : string = config.PLUGIN_URL + '/api/validateprofile';
      const payload = {
          msisdn: msisdn
      };
    
      return this._http.post(profileDetailsURL, payload).toPromise().then(resp => {
              return Promise.resolve(resp.json());
          }).catch(err => Promise.reject(err));
  }


  getNewUserStatus(msisdn: string) {
    const profileDetailsURL : string = config.PLUGIN_URL + '/api/getUserStatus';
    const payload = {
      cust_id: msisdn
    };
  
    return this._http.post(profileDetailsURL, payload).toPromise().then(resp => {
            return Promise.resolve(resp.json());
        }).catch(err => Promise.reject(err));
}

  getfarwphone(msisdnval: string, voicemail: any) {
    const getfarwphoneURL: string = config.PLUGIN_URL + '/api/getfarwphone';
    const payload = {
      msisdn: msisdnval,
      voicemail: "23480310140"
    };

    let token = '';
    if(this.storeData.getToken() != null){
      token = this.storeData.getToken()
    }
    // if(sessionStorage.getItem('chakra')){
    //   token = sessionStorage.getItem('chakra');
    // }
    else{
    token = '';
    }
const headers  = new Headers();
//token = 'ehjdkhjkhf';
//if(path == '/api/linkedaccountlist'){
headers.append('Authorization', 'Bearer ' + token);
headers.append('Content-Type', 'application/json');

    return this._http.post(getfarwphoneURL, payload, {headers: headers}).toPromise().then(resp => {
      return Promise.resolve(resp.json());
    }).catch(err => Promise.reject(err));
  }


  getfarwvoicemail(msisdnval: string, voicemail: string) {
    const getfarwvoicemailURL: string = config.PLUGIN_URL + '/api/getfarwvoicemail';
    const payload = {
      msisdn: msisdnval,
      voicemail: "23480310140"
    };

    let token = '';
//     if(sessionStorage.getItem('chakra')){
// token = sessionStorage.getItem('chakra');
// }
if(this.storeData.getToken() != null){
  token = this.storeData.getToken()
}
else{
token = '';
}
const headers  = new Headers();
//token = 'ehjdkhjkhf';
//if(path == '/api/linkedaccountlist'){
headers.append('Authorization', 'Bearer ' + token);
headers.append('Content-Type', 'application/json');

    return this._http.post(getfarwvoicemailURL, payload, {headers: headers}).toPromise().then(resp => {
      return Promise.resolve(resp.json());
    }).catch(err => Promise.reject(err));
  }


  getdeactivatecallfarw(msisdnval: string, voicemail: string) {
    const getdeactivatecallfarwURL: string = config.PLUGIN_URL + '/api/getdeactivatecallfarw';
    const payload = {
      msisdn: msisdnval,
      voicemail: "23480310140"
    };

    let token = '';
//     if(sessionStorage.getItem('chakra')){
// token = sessionStorage.getItem('chakra');
// }
if(this.storeData.getToken() != null){
  token = this.storeData.getToken()
}
else{
token = '';
}
const headers  = new Headers();
//token = 'ehjdkhjkhf';
//if(path == '/api/linkedaccountlist'){
headers.append('Authorization', 'Bearer ' + token);
headers.append('Content-Type', 'application/json');

    return this._http.post(getdeactivatecallfarwURL, payload, {headers: headers } ).toPromise().then(resp => {
      return Promise.resolve(resp.json());
    }).catch(err => Promise.reject(err));
  }

  getresetbarr(msisdnval: string) {
    const getresetbarURL: string = config.PLUGIN_URL + '/api/getresetbarr';
    const payload = {
      msisdn: msisdnval,
    };

    let token = '';
//     if(sessionStorage.getItem('chakra')){
// token = sessionStorage.getItem('chakra');
// }
if(this.storeData.getToken() != null){
  token = this.storeData.getToken()
}
else{
token = '';
}
const headers  = new Headers();
//token = 'ehjdkhjkhf';
//if(path == '/api/linkedaccountlist'){
headers.append('Authorization', 'Bearer ' + token);
headers.append('Content-Type', 'application/json');

    return this._http.post(getresetbarURL, payload, {headers: headers }).toPromise().then(resp => {
      return Promise.resolve(resp.json());
    }).catch(err => Promise.reject(err));
  }

  getcallbarring(msisdnval: string) {
    const getcallbarringURL: string = config.PLUGIN_URL + '/api/getcallbarring';
    const payload = {
      msisdn: msisdnval,
    };

    let token = '';
//     if(sessionStorage.getItem('chakra')){
// token = sessionStorage.getItem('chakra');
// }
if(this.storeData.getToken() != null){
  token = this.storeData.getToken()
}
else{
token = '';
}
const headers  = new Headers();
//token = 'ehjdkhjkhf';
//if(path == '/api/linkedaccountlist'){
headers.append('Authorization', 'Bearer ' + token);
headers.append('Content-Type', 'application/json');

    return this._http.post(getcallbarringURL, payload, {headers: headers }).toPromise().then(resp => {
      return Promise.resolve(resp.json());
    }).catch(err => Promise.reject(err));
  }

  getcallunbarring(msisdnval: string) {
    const getcallunbarringURL: string = config.PLUGIN_URL + '/api/getcallunbarring';
    const payload = {
      msisdn: msisdnval,
    };

    let token = '';
//     if(sessionStorage.getItem('chakra')){
// token = sessionStorage.getItem('chakra');
// }
if(this.storeData.getToken() != null){
  token = this.storeData.getToken()
}
else{
token = '';
}
const headers  = new Headers();
//token = 'ehjdkhjkhf';
//if(path == '/api/linkedaccountlist'){
headers.append('Authorization', 'Bearer ' + token);
headers.append('Content-Type', 'application/json');

    return this._http.post(getcallunbarringURL, payload, {headers: headers }).toPromise().then(resp => {
      return Promise.resolve(resp.json());
    }).catch(err => Promise.reject(err));
  }


  getPUKDetails(msisdn: string) {
    const getPUKDetailsURL: string = config.PLUGIN_URL + '/api/puk';
    const payload = {
      msisdn: msisdn
      // sub_type : this.store_cust_obj.sub_type
    };

    let token = '';
//     if(sessionStorage.getItem('chakra')){
// token = sessionStorage.getItem('chakra');
// }
if(this.storeData.getToken() != null){
  token = this.storeData.getToken()
}
else{
token = '';
}
const headers  = new Headers();
//token = 'ehjdkhjkhf';
//if(path == '/api/linkedaccountlist'){
headers.append('Authorization', 'Bearer ' + token);
headers.append('Content-Type', 'application/json');

    return this._http.post(getPUKDetailsURL, payload, {headers: headers }).toPromise().then(resp => {
      return Promise.resolve(resp.json());
    }).catch(err => Promise.reject(err));
  }

  getProfilePic(msisdn: string) {
    const getpicprofileURL: string = config.PLUGIN_URL + '/api/profileImg';
    const payload = {
      msisdn: msisdn
    };

    let token = '';
//     if(sessionStorage.getItem('chakra')){
// token = sessionStorage.getItem('chakra');
// }
if(this.storeData.getToken() != null){
  token = this.storeData.getToken()
}
else{
token = '';
}
const headers  = new Headers();
//token = 'ehjdkhjkhf';
//if(path == '/api/linkedaccountlist'){
headers.append('Authorization', 'Bearer ' + token);
headers.append('Content-Type', 'application/json');

    return this._http.post(getpicprofileURL, payload, {headers: headers}).toPromise().then(resp => {
      //console.log("updated response========>", resp);
      return Promise.resolve(resp.json());
    }).catch(err => Promise.reject(err));
  }

  getfetchingprofile(msisdn: string){
    //console.log(msisdn);
    const getfetchingprofileURL: string = config.PLUGIN_URL + '/api/profilepic';
    const payload = {
      msisdn: msisdn
    };

    let token = '';
  //   if(sessionStorage.getItem('chakra')){
  //     token = sessionStorage.getItem('chakra');
  //  } 
  if(this.storeData.getToken() != null){
    token = this.storeData.getToken()
  }
else{
token = '';
}
const headers  = new Headers();
//token = 'ehjdkhjkhf';
//if(path == '/api/linkedaccountlist'){
headers.append('Authorization', 'Bearer ' + token);
headers.append('Content-Type', 'application/json');

    return this._http.post(getfetchingprofileURL, payload, {headers: headers }).toPromise().then(resp => {
      var resp1 = resp.json();
      return Promise.resolve(resp1);
    }).catch(err => Promise.reject(err));
  }


updatepicprofile(msisdn: string, encodedprofilepic: string) {
  const getpicprofileURL: string = config.PLUGIN_URL + '/api/addprofileImg';
  const payload = {
    msisdn: msisdn,
    encodedprofilepic: btoa(encodedprofilepic)
  };

  let token = '';
//     if(sessionStorage.getItem('chakra')){
// token = sessionStorage.getItem('chakra');
// }
if(this.storeData.getToken() != null){
  token = this.storeData.getToken()
}
else{
token = '';
}
const headers  = new Headers();
//token = 'ehjdkhjkhf';
//if(path == '/api/linkedaccountlist'){
headers.append('Authorization', 'Bearer ' + token);
headers.append('Content-Type', 'application/json');

  // console.log("Encrypted PROFILE:::", btoa(encodedprofilepic))
  return this._http.post(getpicprofileURL, payload, {headers: headers}).toPromise().then(resp => {
    //console.log("updated response========>", resp);
    return Promise.resolve(resp.json());
  }).catch(err => Promise.reject(err));
}

changeProfilePic(imageUrl: string) {
    this.profilePic.next(imageUrl)
  }

}



