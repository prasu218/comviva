import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../commonAPI';
import { StoreService } from '../../plugins/datastore'

@Injectable()
export class ComplaintsService {

    public msisdn: string;

    constructor(private _http: Http, private apiService: ApiService, private _storeVal: StoreService) { }

    getLoginRcgDetails(msisdn: string, ticket: string) {
        let sessionId = this._storeVal.getSessionId();
        const complaintURL: string = '/api/complaints';
        const payload = {
            msisdn: msisdn,
            ticket: ticket
        };

        // return this._http.post(LoginRcgDetailsURL, payload)
        //     .toPromise().then(resp => {
        //         return Promise.resolve(resp.json());
        //     }).catch(err => Promise.reject(err));

        return this.apiService.postWithSessionValidation(complaintURL, msisdn, sessionId, payload)
    }


    getcategoryDetails(msisdn: string) {
        let sessionId = this._storeVal.getSessionId();
        const payload = {
            msisdn: msisdn
        };
        const getcategoryURL: string = '/api/complaintscategory';

        // return this._http.post(getcategoryURL, payload)
        //     .toPromise().then(resp => {
        //         return Promise.resolve(resp.json());
        //     }).catch(err => Promise.reject(err));

        return this.apiService.postWithSessionValidation(getcategoryURL, msisdn, sessionId, payload)
    }

    getsubcategoryDetails(msisdn: string, issueType: string) {
        let sessionId = this._storeVal.getSessionId();
        const payload = {
            msisdn: msisdn,
            issueType: issueType
        };
        const getsubcategoryURL: string = '/api/complaintssubcategory';

        // return this._http.post(getsubcategoryURL, payload)
        //     .toPromise().then(resp => {
        //         return Promise.resolve(resp.json());
        //     }).catch(err => Promise.reject(err));

        return this.apiService.postWithSessionValidation(getsubcategoryURL, msisdn, sessionId, payload)
    }

    listComplaintsDetails(msisdn: string) {
        let sessionId = this._storeVal.getSessionId();
        const listComplaintsDetailsURL: string = '/api/complaintslist';
        const payload = {
            msisdn: msisdn
        };

        // return this._http.post(ListComplaintsDetailsURL, payload)
        //     .toPromise().then(resp => {
        //         return Promise.resolve(resp.json());
        //     }).catch(err => Promise.reject(err));

        return this.apiService.postWithSessionValidation(listComplaintsDetailsURL, msisdn, sessionId, payload)
    }


    complaintSubmitFormDetails(msisdn: string, subCatId: number, priority: number, description: string) {
        let sessionId = this._storeVal.getSessionId();
        const complaintSubmitDetailsURL: string = '/api/complaintsubmit';
        const payload = {
            msisdn: msisdn,
            subCatId: subCatId,
            priority: priority,
            description: description
        };

        // return this._http.post(complaintSubmitDetailsURL, payload)
        //     .toPromise().then(resp => {
        //         return Promise.resolve(resp.json());
        //     }).catch(err => Promise.reject(err));

        return this.apiService.postWithSessionValidation(complaintSubmitDetailsURL, msisdn, sessionId, payload)
    }
}

