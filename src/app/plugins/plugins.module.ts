import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
//import 'rxjs/add/operator/timeout';
import { BalanceService } from './balance';
import { StoreService } from './datastore';
import { ProfileService } from './profile';
import { BundleBalanceService } from './balance';
import { TopupService } from './recharge';
import { rcgborrowService } from 'src/app/plugins/rcgborrow';
import { otpService } from './otp';
//import { CommonService } from './common';
import { ViewBillDetailsService } from '../plugins/viewbilldetails/viewbilldetails.service';
import { rcgtopupService } from 'src/app/plugins/rcgtopup';
import { DebitcardService } from './debitcard';
import { BuyBundlesService } from './buybundles';

import { RechargestatusService } from '../plugins/rechargestatus/rechargestatus.service'
import { CommonService } from '../plugins/common';
import { SubscriptionService } from './subscription/subscription.service';

import { SendAirtimeService } from 'src/app/plugins/send-airtime';
import { EligibilityService } from 'src/app/plugins/eligibility';
import { DbCallsService } from './dbCalls';
import { ActivateBundleService } from './activateBundle';
import { HelpService } from './help';
import { AccountHistoryService } from './accountHistory';
import { ComplaintsService } from './complaints/complaints.service';
import { FeedbackService } from 'src/app/plugins/feedback';
import { AddAccountService } from './addaccount';
import { AngularCDRService } from './angularCDR';
import { SessionService } from './session';
import { ApiService } from './commonAPI';
import { DemoService } from './demo';
import { CryptoService } from './crypto';
import {RouterURLService} from './routerurl/routerurl.service';
import {ProceedcomplaintService} from './proceedcomplaint/proceedcomplaint.service';
import { GoogleAnalyticsService } from './googleanalytics';


export const PLUGIN_SERVICE = [

  BalanceService,
  BundleBalanceService,
  ProfileService,
  CommonService,
  StoreService,
  FeedbackService,
  otpService,
  ViewBillDetailsService,
  rcgtopupService,
  rcgborrowService,
  DebitcardService,
  BuyBundlesService,
  RechargestatusService,
  SubscriptionService,
  SendAirtimeService,
  EligibilityService,
  DbCallsService,
  ActivateBundleService,
  HelpService,
  ComplaintsService,
  AccountHistoryService,
  AddAccountService,
  AngularCDRService,
  SessionService,
  ApiService,
  CryptoService,
  RouterURLService,
  ProceedcomplaintService,
  DemoService,
  GoogleAnalyticsService

];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],

})


export class PluginsModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: PluginsModule,
      providers: [
        ...PLUGIN_SERVICE
      ]
    };
  }
}

