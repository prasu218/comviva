import { config } from '../../config';
export const format = {
  msisdn: (msisdn: string) => {
    if(msisdn != null){
    if (msisdn[0] === '0' && msisdn.length === 11) {
      return config.COUNTRY_CODE + msisdn.trim().slice(1);
	} else if (msisdn[0] === '2' && msisdn[1] === '3' && msisdn[2] === '4' && msisdn.length === 13) {
		return config.COUNTRY_CODE + msisdn.trim().slice(3);
    } else {
      return config.COUNTRY_CODE + msisdn;
    }
   }
   else{
	return config.COUNTRY_CODE + msisdn;
    }
  }
};

