export interface LoginForm {
  msisdn: number;
  pin: number;
};
export interface OTPForm {
  otp: number;
};
export interface buyBundleConf_Form {
  msisdnOthers: number;
}
export interface RequestPinForm {
  msisdn: number;
  mothers_name: string;
  dob: string;
  state: string;
};
export interface RechargeForm {
  msisdn: number;
  serial_number: string;
  voucher_code: string;
};