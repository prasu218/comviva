export interface SlotAddress {
  address: string;
};

export interface CustomerDetails {
  sim: number;
  mobile: number;
  email: string;
  id_type: string;
  id_number: string;
}
