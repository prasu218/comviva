var config = require('../config');
var request = require('../utils/request');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');

exports.getcomplaintsDetails = (req, res, cdrParams) => {
    var msisdn = req.body.msisdn;
    var uniqueID = parseInt(Math.random()*1000000000, 10); 
    var ticket = req.body.ticket;
    cdrParams.activityCategory = "Profile";
    cdrParams.subCategory = "getcomplaintsDetails";
    cdrParams.component = "getcomplaintsDetails";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";
    options = {
      method: 'GET',
      //uri:  config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+ "&category="+TroubleTicket+ "&method="+searchTroubleTicket+"&alc="+ticket,
 	uri:  config.PLUGIN_URL + "&transID=" + req.tid + "&Format=json&opcoID=NG&group=Nigeria&msisdn="+msisdn+ "&category=TroubleTicket&method=searchTroubleTicket&alc="+ticket+"&group=Nigeria",
      json: true
    }
    cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
    request.send(req, res, cdrParams, options);
  }
  