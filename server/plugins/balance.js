var config = require('../config');
var request = require('../utils/request');
//var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');

exports.getBalance = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
//  logger.writeLog('Get Balance MSISDN ' + msisdn);
  //req.msisdn = msisdn;
  cdrParams.activityCategory = "Balance";
  cdrParams.subCategory = "getBalance";
  cdrParams.component = "getBalance";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  // req.transStartTime = '16/05/2018';
  // req.apiStatus = '200';
  // req.transEndTime = '16.1/05/2018';
 
    var options = {
      method: 'GET',
      uri:  config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+"&category=myAccount&method=balance",
      json: true
    }
 
//  logger.writeLog('debug','Request for Balance '+ options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);

  
  // if (req.headers.authorization) {
	//   if (req.headers.authorization === sha256(config.AUTH_KEY)) {
		  request.send(req, res, cdrParams, options);
	//   }
  // }
}


exports.getCreditBalances = (req, res, cdrParams) => {
	  var msisdn = req.body.msisdn;
	//  logger.writeLog('Get Balance MSISDN ' + msisdn);
	  //req.msisdn = msisdn;
	  cdrParams.activityCategory = "Balance";
	  cdrParams.subCategory = "getCreditBalance";
	  cdrParams.component = "getCreditBalance";
	  cdrParams.offerCode = "NA";
	  cdrParams.amount = "NA";

	    var options = {
	      method: 'GET',
	      uri:  config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+"&category=myAccount&method=getCLMBalance",
	      json: true
	    }
	 
	//  logger.writeLog('debug','Request for Balance '+ options.uri);
	  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);	  
	  // if (req.headers.authorization) {
		//   if (req.headers.authorization === sha256(config.AUTH_KEY)) {
			  request.send(req, res, cdrParams, options);
		//   }
	  // }
	}
