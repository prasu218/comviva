var config = require('../config');
var request = require('../utils/request');
//var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');
exports.getLinkAccount = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
  var sec_msisdn = req.body.sec_msisdn;
  var label = req.body.label;
  var uniqueID = parseInt(Math.random()*1000000000, 10);
  cdrParams.activityCategory = "LinkAccount";
  cdrParams.subCategory = "getLinkAccount";
  cdrParams.component = "getLinkAccount";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',
    uri: config.PLUGIN_URL_0 +"&transID="+uniqueID+"&Format=json&opcoID=NG&group=Nigeria&category=linkNumber&method=linkSecondNumber&group=Nigeria&opcoID=ng&msisdn="+msisdn+"&secMsisdn="+sec_msisdn+"&nickname="+label,
    json: true
  }
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  request.send(req, res, cdrParams, options);
}





