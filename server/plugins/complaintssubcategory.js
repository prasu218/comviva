var config = require('../config');
var request = require('../utils/request');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');

  exports.getsubcategoryDetails = (req, res, cdrParams) => {
    var msisdn = req.body.msisdn;
    var values = req.body.issueType;
    cdrParams.activityCategory = "getTTCategory";
    cdrParams.subCategory = "getComplaintsSubCategory";
    cdrParams.component = "getComplaintsSubCategory";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";
    options = {
      method: 'GET',
      uri:  config.DB_PLUGIN_URL_1 + "&transID=" + req.tid + "&msisdn="+msisdn + "&query=getTTSubCategory&values="+values,
      json: true
    }
    cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
    request.send(req, res, cdrParams, options);
  }