var config = require('../config');
var request = require('../utils/request');
var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');

//CIS

exports.getBundleCatCIS = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
  //msisdn=req.session.msisdn;
  cdrParams.activityCategory = "BuyBundlesCIS";
  cdrParams.subCategory = "getBundleCatCIS";
  cdrParams.component = "getBundleCatCIS";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";

  options = {
    method: 'GET',
	uri: config.CIS_PLUGIN_URL + "&clientTransactionId=" + req.tid + "&msisdn=" + msisdn +"&category=bundles&method=viewCatalogue&type=" + req.body.type + "&subtype=" + req.body.subtype, 
    json: true
  }
  logger.writeLog('debug', 'Request for Login ' + options.uri);

console.log("getBundleCatCIS ::::::::::::::::::::"+ options.uri)
  cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
  request.send(req, res, cdrParams, options);
console.log("encodeURI(req)::::::::::::::::::::"+ encodeURI(req))
}

//GET ALL PRODUCT LIST

exports.getAllBundleProductListCIS = (req, res, cdrParams) => {
  console.log("inside getBundleProductListCIS ")

  var msisdn = req.body.msisdn;
  //msisdn=req.session.msisdn;
  console.log('MSISDN in server Session  getBundleProductListCIS', msisdn);
  cdrParams.activityCategory = "BuyBundlesCIS";
  cdrParams.subCategory = "getBundleCatCIS";
  cdrParams.component = "getBundleCatCIS";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";

  options = {
    method: 'GET',
    uri: config.CIS_PLUGIN_URL + "&clientTransactionId=" + req.tid + "&msisdn=" + msisdn + "&category=bundles&method=viewCatalogueAll",
    json: true
  }
  console.log("getBundleProductListCIS-----------" + options.uri);
  logger.writeLog('debug', 'Request for Login ' + options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
  request.send(req, res, cdrParams, options);
}

//SME

exports.getBundleCatCISSME = (req, res, cdrParams) => {
console.log("inside getBundleCatSmeCIS ")

  var msisdn = req.body.msisdn;
  //msisdn=req.session.msisdn;
  console.log( 'MSISDN in server Session  getBundleCatCIS',msisdn);
  cdrParams.activityCategory = "BuyBundlesCIS";
  cdrParams.subCategory = "getBundleCatCIS";
  cdrParams.component = "getBundleCatCIS";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";

  options = {
    method: 'GET',
    uri: config.CIS_PLUGIN_URL + "&clientTransactionId=" + req.tid + "&msisdn=" + msisdn +"&category=bundles&method=viewCatalogue&type=" + req.body.type + "&subtype=" + req.body.subtype, 
    json: true
  }
  console.log("getBundleCatCIS-----------" +options.uri);
  logger.writeLog('debug', 'Request for Login ' + options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
  request.send(req, res, cdrParams, options);
}


exports.buyforSelfCIS= (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
 // msisdn=req.session.msisdn;
  console.log( 'MSISDN in server Session  buyforselfCIS',msisdn); 
  var input = req.body.input;
  var price = req.body.price;
  var autorenewal = req.body.autorenewal;
//  logger.writeLog('debug', 'rajnish:::' + autorenewal+":::");
//cdrParams.activityCategory = "buyforSelfCIS";
//cdrParams.subCategory = "buyforSelfCIS";
//cdrParams.component = "buyforSelfCIS";
//cdrParams.offerCode = "NA";
//cdrParams.amount = "NA";

  options = {
    method: 'GET',
    uri: config.CIS_PLUGIN_URL + "&clientTransactionId=" + req.tid + "&msisdn=" + msisdn +"&category=bundles&method=bundleProv&client=&input=" + req.body.input +"&price=" + req.body.price+"&autorenewal=" + req.body.autorenewal, 
    json: true
  };
  logger.writeLog('debug', 'buyforSelfCIS' + options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
  request.send(req, res, cdrParams, options);
}



exports.buyforothersCIS = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
  //msisdn=req.session.msisdn;
   console.log( 'MSISDN in server Session  buyforothersCIS',msisdn);
  var input = req.body.input;
var price = req.body.price;
  var beneficiaryMsisdn = req.body.beneficiaryMsisdn;

  options = {
    method: 'GET',
    uri: config.CIS_PLUGIN_URL + "&clientTransactionId=" + req.tid + "&msisdn=" + msisdn +"&category=bundles&method=bundleProvOther&client=&input=" + req.body.input +"&price=" + req.body.price+"&beneficiaryMsisdn=" + req.body.beneficiaryMsisdn, 
    json: true
  };
  logger.writeLog('debug', 'buyforothersCIS ' + options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
  request.send(req, res, cdrParams, options);
}

exports.smedatasharebundle = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;

   console.log( 'MSISDN in server Session  buyforothersCIS',msisdn);
  //var input = req.body.input;
  //var price = req.body.price;
  //var beneficiaryMsisdn = req.body.beneficiaryMsisdn;

  options = {
    method: 'GET',
	//"http://10.1.218.78:8080/webaxn/plugin?plugin=cis&AppsName=selfcare&Interface=client&Format=json&group=mtnng&category=bundles&method=viewCatalogue&clientTransactionId=1565001208249&type=Share%20SME&subtype=Share%20SME&msisdn=2349062058583
	 uri: config.CIS_PLUGIN_URL +"&clientTransactionId=" + req.tid + "&msisdn=" + msisdn +"&category=bundles&method=viewCatalogue&type=Share%20SME&subtype=Share%20SME", 
    //uri: config.CIS_PLUGIN_URL + "&clientTransactionId=" + req.tid + "&msisdn=" + msisdn +"&category=bundles&method=bundleProvOther&client=&input=" + req.body.input +"&price=" + req.body.price+"&beneficiaryMsisdn=" + req.body.beneficiaryMsisdn, 
    json: true
  };
  logger.writeLog('debug', 'buyforothersCIS ' + options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
  request.send(req, res, cdrParams, options);
}

exports.smedata = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;

   console.log( 'MSISDN in server Session  buyforothersCIS',msisdn);
  //var input = req.body.input;

  var Beneficiary= req.body.Beneficiary;
  var pin=req.body.pin;
  //var input='427';
   var input=req.body.share_id;

  options = {
    method: 'GET',
	//"http://10.1.218.78:8080/webaxn/plugin?plugin=cis&AppsName=selfcare&Interface=client&Format=json&group=mtnng&category=bundles&method=viewCatalogue&clientTransactionId=1565001208249&type=Share%20SME&subtype=Share%20SME&msisdn=2349062058583
	 uri: config.CIS_PLUGIN_URL +"&clientTransactionId=" + req.tid + "&msisdn=" + msisdn +"&Beneficiary=" + Beneficiary+ "&pin="+pin+"&input="+input+"&category=bundles&method=ME2U", 
    //uri: config.CIS_PLUGIN_URL + "&clientTransactionId=" + req.tid + "&msisdn=" + msisdn +"&category=bundles&method=bundleProvOther&client=&input=" + req.body.input +"&price=" + req.body.price+"&beneficiaryMsisdn=" + req.body.beneficiaryMsisdn, 
    json: true
  };
  logger.writeLog('debug', 'buyforothersCIS ' + options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
  request.send(req, res, cdrParams, options);
}

//getRoamingRateCountryDetails

exports.getRoamingRateCountryDetails= (req, res, cdrParams) => {
var msisdn = req.body.msisdn;
var values = req.body.values;


  cdrParams.activityCategory = "getRoamingRateCountryDetails";
  cdrParams.subCategory = "getRoamingRateCountryDetails";
  cdrParams.component = "getRoamingRateCountryDetails";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',
    uri: config.DB_PLUGIN_URL + "&clientTransactionId=" + req.tid + "&msisdn=" + msisdn +"&query=getRoamingRateCountryDetails&values=" +values,                                                                                                     
    json: true
  };
  logger.writeLog('debug', 'getRoamingRateCountryDetails' + options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
  request.send(req, res, cdrParams, options);
}

//getRoamingdata


exports.getRoamingdata = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
var subtype = req.body.subtype;
 var type = req.body.type;
 
  cdrParams.activityCategory = "getRoamingdata";
  cdrParams.subCategory = "getRoamingdata";
  cdrParams.component = "getRoamingdata";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',
    // uri: config.CIS_PLUGIN_URL + "&clientTransactionId=" + req.tid + "&msisdn=" + msisdn +"&category=bundles&method=viewCatalogue&type=" + req.body.type + "&subtype=" + req.body.subtype, 
    uri: config.CIS_PLUGIN_URL + "&clientTransactionId=" + req.tid + "&msisdn=" + msisdn +"&category=bundles&method=viewCatalogue&type=" + type + "&subtype=" +subtype, 
    json: true                    

  };
  logger.writeLog('debug', 'getRoamingdata ' + options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
  request.send(req, res, cdrParams, options);
}

//getRoamingvoice

exports.getRoamingvoice = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
 var values = req.body.values;
var subtype = req.body.subtype;
 var type = req.body.type;


  cdrParams.activityCategory = "getRoamingvoice";
  cdrParams.subCategory = "getRoamingvoice";
  cdrParams.component = "getRoamingvoice";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',                                                                             
    uri: config.CIS_PLUGIN_URL + "&clientTransactionId=" + req.tid + "&msisdn=" + msisdn +"&category=bundles&method=viewCatalogue&type=" + type + "&subtype=" +subtype,                                                                                                      
    json: true
  };
  logger.writeLog('debug', 'getRoamingvoice ' + options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
  request.send(req, res, cdrParams, options);
}

//getRoamingdataandvoice

exports.getRoamingdataandvoice = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
 var values = req.body.values;
var subtype = req.body.subtype;
 var type = req.body.type;


  cdrParams.activityCategory = "getRoamingdataandvoice";
  cdrParams.subCategory = "getRoamingdataandvoice";
  cdrParams.component = "getRoamingdataandvoice";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',
   uri: config.CIS_PLUGIN_URL + "&clientTransactionId=" + req.tid + "&msisdn=" + msisdn +"&category=bundles&method=viewCatalogue&type=" + type + "&subtype=" +subtype,                         
    json: true
  };
  logger.writeLog('debug', 'getRoamingdataandvoice ' + options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
  request.send(req, res, cdrParams, options);
}
exports.getBundleCatCISCountry = (req, res, cdrParams) => {
  console.log("inside getBundleContryCIS ")
  
    var msisdn = req.body.msisdn;
    var ProductId =req.body.ProductId;
    //msisdn=req.session.msisdn;
    console.log( 'MSISDN in server Session  getBundleContryCIS',msisdn);
    cdrParams.activityCategory = "BuyBundlesCIS";
    cdrParams.subCategory = "getBundleContryCIS";
    cdrParams.component = "getBundleContryCIS";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";
  
    options = {
      method: 'GET',
    uri: config.CIS_PLUGIN_URL + "&category=services&method=countrylist&clientTransactionId="+req.tid+"&msisdn="+msisdn+"&input="+ProductId+"&msisdn="+msisdn, 
      json: true
    }
    console.log("getBundleContryCIS-----------" +options.uri);
    logger.writeLog('debug', 'Request for Login ' + options.uri);
    cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
    request.send(req, res, cdrParams, options);
  }


