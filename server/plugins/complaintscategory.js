var config = require('../config');
var request = require('../utils/request');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');

  exports.getcategoryDetails = (req, res, cdrParams) => {
    var msisdn = req.body.msisdn;
    cdrParams.activityCategory = "getTTCategory";
    cdrParams.subCategory = "getComplaintsCategory";
    cdrParams.component = "getComplaintsCategory";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";
    options = {
      method: 'GET',
      uri:  config.DB_PLUGIN_URL_1 + "&transID=" + req.tid + "&msisdn="+msisdn + "&query=getTTCategory", 
      json: true
    }
    cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
    request.send(req, res, cdrParams, options);
  }