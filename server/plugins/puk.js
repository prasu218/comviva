var config = require('../config');
var request = require('../utils/request');
//var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');


exports.getPUKDetails = (req, res, cdrParams) => {
    var msisdn = req.body.msisdn;
    cdrParams.activityCategory = "PUK";
    cdrParams.subCategory = "getPUKDetails";
    cdrParams.component = "getPUKDetails";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";
    options = {
      method: 'GET',
      // uri: config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+"&voicemail="+voicemail + "&category=myAccount&method=getPUK",
      uri:  config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+"&opcoID=NG&category=myAccount&method=getPUK&group=Nigeria&ToChannel=",
      json: true
    }
   console.log('Request for PUK '+ options.uri);
   cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  request.send(req, res, cdrParams, options);
  
  }