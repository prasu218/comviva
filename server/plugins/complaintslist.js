var config = require('../config');
var request = require('../utils/request');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');

  exports.listComplaintsDetails = (req, res, cdrParams) => {
    var msisdn = req.body.msisdn;
    cdrParams.activityCategory = "TroubleTicket";
    cdrParams.subCategory = "listComplaintsDetails";
    cdrParams.component = "listComplaintsDetails";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";
    options = {
      method: 'GET',
         uri: config.PLUGIN_URL + "&transID=" +req.tid+ "&Format=json&opcoID=NG&group=Nigeria&msisdn="+msisdn+"&category=TroubleTicket&method=searchTroubleTicketList&alc="+msisdn+"&group=Nigeria",
      //uri:  config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+ "&category="+TroubleTicket+ "&method="+searchTroubleTicketList+"&alc="+msisdn,
      json: true
    }
    cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
    request.send(req, res, cdrParams, options);
  }
  