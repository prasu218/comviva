var config = require('../config');
var request = require('../utils/request');
var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');

exports.getcheckPlan = (req, res, cdrParams) => {
  var msisdn =req.body.msisdn;
  //var planName = req.body.planName;
  //var clientTransactionId =req.tid;
  cdrParams.activityCategory = "plans";
 
  cdrParams.subCategory = "TariffMigration";
  cdrParams.component = "TariffMigration";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
   method: 'GET',
   
   uri:config.CIS_PLUGIN_URL+ "&clientTransactionId="+req.tid+"&msisdn="+msisdn+"&category=plans&method=viewCatalogue&type=TariffMigration&subtype=TariffMigration",
   

    json: true
  }

  logger.writeLog('debug','TariffMigration'+ options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  console.log('TariffMigration '+ options.uri);
  request.send(req, res, cdrParams, options);
}

exports.migrateTariffPlan = (req, res, cdrParams) => {
var msisdn =req.body.msisdn;
  var input = req.body.productId;
  console.log("migration plan ",input);
  
  cdrParams.activityCategory = "plans";
 
  cdrParams.subCategory = "migratePlan";
  cdrParams.component = "migratePlan";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
   method: 'GET',
   uri:config.CIS_PLUGIN_URL +"&clientTransactionId="+req.tid+"&msisdn="+msisdn+"&input="+input+"&category=plans&method=migratePlan",
   
   //uri:"http://10.1.218.78:8080/webaxn/plugin?plugin=cis&AppsName=selfcare&Interface=client&Format=json&group=mtnng&category=plans&method=migratePlan&clientTransactionId=1565001208249&input=NACT_NG_Others_71&msisdn=2349062058850",

    json: true
  }

  logger.writeLog('debug','migratePlan'+ options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  console.log('migratePlan '+ options.uri);
  request.send(req, res, cdrParams, options);
}
