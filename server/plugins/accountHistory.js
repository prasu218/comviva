var config = require('../config');
var request = require('../utils/request');
//var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');

exports.getAccountHistory = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
  var historyType = req.body.historyType;
  var startDate = req.body.startDate;
  var endDate = req.body.endDate;
  var uniqueID = parseInt(Math.random() * 1000000000, 10);
  options = {
    method: 'GET',
    uri: config.PLUGIN_URL + "&transID=" + uniqueID + "&msisdn=" + msisdn + "&opcoID=NG&category=History&method=" + historyType + "&startDate=" + startDate + "&endDate=" + endDate,
    json: true
  }
  cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
  request.send(req, res, cdrParams, options);
}