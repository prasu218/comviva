var config = require('../config');
var request = require('request');
var request1 = require('../utils/request');
//var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');
exports.paymentgatewaydataplanothers= (req, res, cdrParams) => {
//   var msisdn = req.body.mtnepg_cust_mobile1;
//   var amount1 = req.body.amount1;
//   var email = req.body.mtnepg_cust_email1;
//   var firstName = req.body.mtnepg_cust_name1; 
//   var mtnepg_cust_id = req.body.mtnepg_cust_id1;
var msisdn = req.body.mtnepg_cust_mobile;
var amount = req.body.mtnepg_transaction_amount;
var email = req.body.mtnepg_cust_email;
var firstName = req.body.mtnepg_cust_name; 
var mtnepg_cust_id =  req.body.mtnepg_cust_id;
cdrParams.activityCategory = "Login";
cdrParams.subCategory = "getDebitcardRcgDetails";
cdrParams.component = "getDebitcardRcgDetails";
cdrParams.offerCode = "NA";
cdrParams.amount = "NA";
var uniqueID = parseInt(Math.random()*1000000000, 10);
console.log("uniqueID"+uniqueID);
console.log("amount"+amount);
options = {
  method: 'GET',
  uri:  config.PAYMENT_URL +"Amount="+ amount+"&CustomerID="+mtnepg_cust_id+"&CustomerMobile="+msisdn+"&CustomerName="+firstName+"&CustomerEmail="+email+"&TransID="+uniqueID+"&CustomerDesc=test&Category=Bundle&Method=Purchase",
  json: true
}
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
   request(options, function(error, response, body) {
   console.log("testing the request");
    console.log("responce" +JSON.stringify(body) );
 if( (body.statusmessage == 'success' || body.statuscode === 0)) {
   console.log("responce" +JSON.stringify(body) );
       amount = body.mtnepg_transaction_amount,
       msisdn = body.mtnepg_cust_id,
       firstName = body.mtnepg_cust_name,
       mtnepg_cust_mobile = body.mtnepg_cust_mobile,
       email  = body.mtnepg_cust_email,      
       mtnepg_cust_desc = body.mtnepg_cust_desc,
       mtnepg_transaction_detail = body.mtnepg_transaction_detail,
       mtnepg_secret_key = body.mtnepg_secret_key
      var payload = {
        transaction_amount: body.mtnepg_transaction_amount,
        cust_id: req.body.msisdn,
        cust_name: body.mtnepg_cust_name,
        cust_mobile: body.mtnepg_cust_mobile,
        cust_email: body.mtnepg_cust_email,
        cust_desc: body.mtnepg_cust_desc,
        transaction_detail: body.mtnepg_transaction_detail,
        secret_key: body.mtnepg_secret_key,
        transaction_ref: body.mtnepg_transaction_ref,
        only_action: req.body.only_action,
        transaction_type: req.body.transaction_type,
        transaction_status: ''
      }
      saveDebitCardPaymentData(payload, req, res, cdrParams)
      console.log("parameters" + body.mtnepg_transaction_ref + 'amount' + body.mtnepg_transaction_amount + 'cust id' + body.mtnepg_cust_id + 'name' + body.mtnepg_cust_name + 'mobile' + body.mtnepg_cust_mobile + 'email' + body.mtnepg_cust_email + 'cust_desc' + body.mtnepg_cust_desc + 'transaction details' + body.mtnepg_transaction_detail + 'key' + body.mtnepg_secret_key)
      paymentgatewayRedirect(body, res, cdrParams);
 }
 else{
   console.log("failed" + error);
 }
});
}
 function paymentgatewayRedirect(body, res, cdrParams) {
   console.log("testing redirection");
  try {
    console.log("checking try");
//       //logger.writePaymentLog(LOG_ORDER_NUMBER, 'debug', 'Step 5 - Payment Gateway Redirection -  STARTS');
     
     console.log("parameters" +body.mtnepg_transaction_ref + 'amount' +body.mtnepg_transaction_amount +'cust id'+ body.mtnepg_cust_id +'name'+ body.mtnepg_cust_name + 'mobile'+ body.mtnepg_cust_mobile + 'email' + body.mtnepg_cust_email + 'cust_desc' + body.mtnepg_cust_desc + 'transaction details' +body.mtnepg_transaction_detail + 'key' + body.mtnepg_secret_key)
   var form = "<!DOCTYPE html><html><body><p>You are being redirected...Please wait</p>" +
       //"<form action=" + config.PAYGATE_URL + " method='POST' name='paymentResponse'>" +
        "<form action=" + body.paymentGwUrl + " method='POST' name='paymentResponse'>" +
       "<input type='hidden' name='mtnepg_transaction_ref' id='mtnepg_transaction_ref' value='"+body.mtnepg_transaction_ref+"'> " +
        "<input type='hidden' name='mtnepg_transaction_amount' id='mtnepg_transaction_amount' value='"+body.mtnepg_transaction_amount+"'><br>" +
        "<input type='hidden' name='mtnepg_cust_id' id='mtnepg_cust_id' value='"+body.mtnepg_cust_id+"'><br>" +
        "<input type='hidden' name='mtnepg_cust_name' id='mtnepg_cust_name' value='"+body.mtnepg_cust_name+"'><br>" +
        "<input type='hidden' name='mtnepg_cust_mobile' id='mtnepg_cust_mobile' value='"+body.mtnepg_cust_mobile+"'><br>" +
        "<input type='hidden' name='mtnepg_cust_email' id='mtnepg_cust_email' value='"+body.mtnepg_cust_email+"'><br>" +
        "<input type='hidden' name='mtnepg_cust_desc' id='mtnepg_cust_desc' value='"+body.mtnepg_cust_desc+"'><br>" +
        "<input type='hidden' id='mtnepg_secret_key' name='mtnepg_secret_key' value='"+body.mtnepg_secret_key+"'><br>"+
        "<input type='hidden' name='mtnepg_transaction_detail' id='mtnepg_transaction_detail' value='"+body.mtnepg_transaction_detail.replace(/\\/g, "")+"'><br></form> " +
       "<script language='JavaScript'>document.forms['paymentResponse'].submit();</script></body></html>";
     res.set('Content-Type', 'text/html');
//       //logger.writePaymentLog(LOG_ORDER_NUMBER, 'debug', 'Step 5 - Payment Gateway Redirection -  REDIRECTION STARTS');
    res.send(form);
    } catch(error) {
    console.log('failed- ' + error);
//      // logger.writePaymentLog(LOG_ORDER_NUMBER, 'warn','Payment Flow - Step 5 - Payment Gateway Redirection - Error Caught - ' + error + '\n' +error.stack);
//       //utils.sendFailureBeforePayment(req, res, cdrParams);
//   }
  
  }
}

var saveDebitCardPaymentData = function (payload, req, res, cdrParams) {
  var uniqueID = parseInt(Math.random() * 1000000000, 10);
  // var session_id = req.body.session_id;
  var pg_trans_id = payload.pg_trans_id;
  var cust_id = payload.cust_id;
  var cust_name = payload.cust_name;
  var cust_mobile = payload.cust_mobile;
  var cust_email = payload.cust_email;
  var transaction_amount = payload.transaction_amount;
  var transaction_status = payload.transaction_status;
  var transaction_type = payload.transaction_type;
  var transaction_detail = payload.transaction_detail;
  var transaction_ref = payload.transaction_ref;
  var secret_key = payload.secret_key;
  var only_action = payload.only_action;
  var currentDate = new Date();

  var encodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.DEBITCARD_TABLE_NAME + "&method=insert&header=" + config.DEBITCARD_HEADER + "&values=" + pg_trans_id + ":::" + cust_id + ":::" + cust_name + ":::" + cust_mobile + ":::" + cust_email + ":::" + transaction_amount + ":::" + transaction_status + ":::" + transaction_type + ":::" + transaction_detail + ":::" + transaction_ref + ":::" + secret_key + ":::" + only_action + ":::" + currentDate + ":::" + currentDate);
  try {
    options = {
      method: 'GET',
      uri: encodeURL,
      json: true
    }
    cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
    request1.sendResponse(req, res, cdrParams, options).then(function (resp) {
      console.log('Got the following DEBIT CARD resp:::::::::::::::', resp)
      if (resp.status == 0) {
        res.status(201).send(resp.prodInfo[0]);
      }

    })
  } catch (e) {
    console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
    res.status(400).send({ 'message': e, status: 401 })
  }
}
