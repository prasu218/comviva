var config = require('../config');
var request = require('../utils/request');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');

exports.complaintSubmitFormDetails = (req, res, cdrParams) => {
    var msisdn = req.body.msisdn;
    var subCatId = req.body.subCatId;
    var priority = req.body.priority;
    var description = req.body.description;
    var uniqueID = parseInt(Math.random()*1000000000, 10); 
    cdrParams.activityCategory = "submitcomplaint";
    cdrParams.subCategory = "complaintSubmitFormDetails";
    cdrParams.component = "complaintSubmitFormDetails";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";
    options = {
      method: 'GET',
      //&transID=7533533276&Format=json&opcoID=NG&group=Nigeria&msisdn=2349062058854&category=TroubleTicket&method=logTroubleTicket&group=Nigeria&msisdn=2349062058854&categorycode=10000505&errordesc=test&priority=5"
      uri:config.PLUGIN_URL_0 +"&transID="+uniqueID+"&Format=json&opcoID=NG&group=Nigeria&msisdn="+msisdn+"&category=TroubleTicket&method=logTroubleTicket&group=Nigeria&categorycode="+subCatId+"&errordesc="+description+"&priority="+priority,
     // uri:  config.PLUGIN_URL + "&transID=" + req.tid + "&Format=json&opcoID=NG&group=Nigeria&msisdn="+msisdn+ "&category=TroubleTicket&method=logTroubleTicket&group=Nigeria&msisdn="+msisdn+ "&categorycode="+subCatId+"&errordesc="+description+"&priority="+priority,
      //uri:  "http://10.1.218.78:8080/webaxn/plugin?plugin=smartweb&AppsName=selfcare&Interface=client&transID=" + req.tid + "&Format=json&opcoID=NG&group=Nigeria&msisdn="+msisdn+"&category=TroubleTicket&method=logTroubleTicket&group=Nigeria&msisdn="+msisdn+ "&categorycode="+subCatId+"&errordesc="+description+"&priority="+priority,
      json: true
    }
    cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
    request.send(req, res, cdrParams, options);
  }