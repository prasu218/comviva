var config = require('../config');
var request = require('../utils/request');
//var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');


exports.getTopupDetails = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
  var tpoup_code = req.body.voucher_code;
  cdrParams.activityCategory = "Recharge";
  cdrParams.subCategory = "getTopupDetails";
  cdrParams.component = "getTopupDetails";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',
    uri: config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn=" + msisdn + "&category=myAccount&method=doVoucherTopup&idType1=ServiceID&idValue1=" + msisdn + "&activationCode=" + tpoup_code,
    json: true
  }
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  request.send(req, res, cdrParams, options);
}

exports.getTopupDetailsOthers = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
  var voucher_code = req.body.voucher_code;
  cdrParams.activityCategory = "Recharge";
  cdrParams.subCategory = "getTopupDetails";
  cdrParams.component = "getTopupDetails";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',
    uri: config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn=" + msisdn + "&category=myAccount&method=doVoucherTopup&idType1=ServiceID&idValue1=" + msisdn + "&activationCode=" + voucher_code,
    json: true
  }
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  request.send(req, res, cdrParams, options);
}