var config = require('../config');
var request = require('../utils/request');
var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');

exports.getcheckPlanandvalues = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
  var planName = req.body.planName;
   msisdn=req.session.msisdn;
  cdrParams.activityCategory = "checkPlanandvalues";
  cdrParams.subCategory = "getcheckPlanandvalues";
  cdrParams.component = "getcheckPlanandvalues";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
   method: 'GET',
   uri:  config.DB_PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn + "&query=checkPlan&values="+planName, 
    json: true
  }

  logger.writeLog('debug','checkPlanandvalues'+ options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  request.send(req, res, cdrParams, options);

 // if (req.headers.authorization) {
	 //// if (req.headers.authorization === sha256(config.AUTH_KEY)) {
  //request.send(req, res, cdrParams, options);
	  //}
  //}
}
