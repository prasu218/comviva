var config = require('../config');
var request = require('../utils/request');
//var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');
exports.validateAccount = (req, res, cdrParams) => {
   // console.log("url send airtime" +config.SHARE_AIRTIME_URL );
  var msisdn = req.body.msisdn;
  //var sec_msisdn = req.body.sec_msisdn;
  var uniqueID = parseInt(Math.random()*1000000000, 10);
  cdrParams.activityCategory = "ValidateAccount";
  cdrParams.subCategory = "ValidateAccount";
  cdrParams.component = "ValidateAccount";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',
    uri: config.PLUGIN_URL_0 +"&transID="+uniqueID+"&Format=json&opcoID=NG&group=Nigeria&category=linkNumber&method=validateSecondNumber&group=Nigeria&opcoID=NG&msisdn="+msisdn,
    json: true
  }
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  request.send(req, res, cdrParams, options);
}