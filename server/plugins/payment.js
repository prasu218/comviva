var config = require('../config');
var request = require('../utils/request');
const DebitCardPayment = require('../models/debitCard')
const NotificationTab = require('../models/notification')
const Airtime = require('../models/airtime')
const Session = require('../models/session')
var request = require('../utils/request');
var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var crypto = require('../utils/crypto');
const uuidv4 = require('uuid/v4');
var he = require('he');

exports.saveDebitCardPaymentStatus = async (req, res, cdrParams) => {
  var uniqueID = parseInt(Math.random() * 1000000000, 10);
    // var session_id = req.body.session_id;
    var pg_trans_id = req.body.pg_trans_id;
    var cust_id = req.body.cust_id;
    var cust_name = req.body.cust_name;
    var cust_mobile = req.body.cust_mobile;
    var cust_email = req.body.cust_email;
    var transaction_amount = req.body.transaction_amount;
    var transaction_status = req.body.transaction_status;
    var transaction_type = req.body.transaction_type;
    var currentDate = new Date();
    var encodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.DEBITCARD_TABLE_NAME + "&method=insert&header=" + config.DEBITCARD_HEADER + "&values=" + pg_trans_id + ":::" + cust_id + ":::" + cust_name + ":::" + cust_mobile + ":::" + cust_email + ":::" + transaction_amount + ":::" + transaction_status + ":::" + transaction_type + ":::" + currentDate + ":::" + currentDate);
    try {
        options = {
            method: 'GET',
            uri: encodeURL,
            json: true
        }
        cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
        request.sendResponse(req, res, cdrParams, options).then(function (resp) {
            console.log('Got the following DEBIT CARD resp:::::::::::::::', resp)
            if (resp.status == 0) {
                res.status(201).send(resp.prodInfo[0]);
            }

        })
    } catch (e) {
        console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
        res.status(400).send({ 'message': e, status: 401 })
    }
}

exports.getAirtimePaymentStatuses = async (req, res, cdrParams) => {
    try {
        const payment = await Airtime.find()
        res.send({ payment })
    } catch (e) {
        res.status(400).send(e)
    }
}
function addSession(session_id, cust_id, date, req, res, cdrParams) {
 var uniqueID = parseInt(Math.random() * 1000000000, 10);
    var encodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.SESSION_TABLE_NAME + "&method=insert&header=" + config.SESSION_HEADER + "&values=" + session_id + ":::" + cust_id + ":::" + date + ":::" + date)
                try {
                    options = {
                        method: 'GET',
                        uri: encodeURL,
                        json: true
                    }
                    cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
                    request.sendResponse(req, res, cdrParams, options).then(function (resp) {
                        console.log('Got the following resp:::::::::::::::', resp)
                        if (resp.status == 0) {
                                var respVal = resp.values;
                                var splitVal = respVal.split(':::');
                                console.log("SPLIT VALUE::::::::::::", splitVal);
                                var decodeVal = decodeURI(splitVal[0])
                                var encryptedSessionId = crypto.encryptValue(decodeVal);
                                console.log("encryptedOtp ::::::::::::", encryptedSessionId);
                                res.status(201).send({ session_id: encryptedSessionId, status: 0 })
                        } else {
                            res.status(400).send({ 'message': 'Error occured while adding the session record', status: 401 })
                        }
                    })
                } catch (e) {
                    console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
                    res.status(400).send({ 'message': e, status: 401 })
                }
}

function updateSessionStatus(session_id, current_date, message, req, res, cdrParams){
    var uniqueID = parseInt(Math.random() * 1000000000, 10);
    var updateEncodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.SESSION_TABLE_NAME + "&method=update&equationName=session_id&equationValue=" + session_id + "&fieldValue=" + session_id + "&fieldName=session_id&fieldValue=" + current_date + "&fieldName=updatedAt");
    try {
        options = {
            method: 'GET',
            uri: updateEncodeURL,
            json: true
        }
        cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
        var encryptedSessionId = crypto.encryptValue(session_id);
        request.sendResponse(req, res, cdrParams, options).then(function (resp) {
 //           console.log('Got the following resp:::::::::::::::', resp)
            if (resp.status == 0) {
                res.status(201).send({session_id: encryptedSessionId , message : message, status: 0});
            } else {
                res.status(400).send({ 'message': 'Error occured while updating the session record', status: 401 })
            }
        })
    } catch (e) {
        console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
        res.status(400).send({ 'message': e, status: 401 })
    }
}

exports.createSessionStatus = (req, res, cdrParams) => {

    var uniqueID = parseInt(Math.random() * 1000000000, 10);
    var session_id = uuidv4();
    var cust_id = req.body.cust_id
    var current_date = new Date()
    var encodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.SESSION_TABLE_NAME + "&method=getCondtional&header=cust_id:::session_id&values=" + cust_id + ":::" + session_id);
    try {
        options = {
            method: 'GET',
            uri: encodeURL,
            json: true
        }
        cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
        request.sendResponse(req, res, cdrParams, options).then(function (resp) {
            console.log('GET SESSION ID the following resp:::::::::::::::', resp.status)
            if (resp.status == 0) {
                updateSessionStatus(session_id,current_date,'Updated session successfully',req,res, cdrParams)
            } else {
                addSession(session_id, cust_id,current_date,req, res, cdrParams);
            }
        })
    } catch (e) {
        console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
        res.status(400).send(e)
    }

}

exports.getAllSessionId = async (req, res, cdrParams) => {
    // const session = new Session(req.body)
    var uniqueID = parseInt(Math.random() * 1000000000, 10);
    // var session_id = req.body.session_id;
    var msisdn = req.body.msisdn;
    var encodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&AppsName=dba&Interface=client&msisdn=" + msisdn + "&Format=json&opcoID=CDI&tableName=sessions&method=get");
    try {
        options = {
            method: 'GET',
            uri: encodeURL,
            json: true
        }
        cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
        request.send(req, res, cdrParams, options)
    } catch (e) {
        console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
        res.status(400).send({ 'message': e, status: 401 })
    }
}

exports.getSessionStatus = async (req, res, cdrParams) => {
    var uniqueID = parseInt(Math.random() * 1000000000, 10);
    var session_id = crypto.decryptValue(req.body.session_id);
    var cust_id = req.body.cust_id
    var current_date = new Date()
    var encodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.SESSION_TABLE_NAME + "&method=getCondtional&header=cust_id:::session_id&values=" + cust_id+ ":::" + session_id);
    try {
        options = {
            method: 'GET',
            uri: encodeURL,
            json: true
        }
        cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
        request.sendResponse(req, res, cdrParams, options).then(function (resp) {
            console.log('GET SESSION ID the following resp:::::::::::::::', resp.status)
            let state = true;
            if (resp.status == 0) {
                console.log("Inside resp success status::::::::::::::::::::::", new Date());
                let updatedTime = decodeURI(resp.prodInfo[0].updatedAt);
                console.log("updatedTime ::::::::::::::::::::::", updatedTime);
                let createdAt = decodeURI(resp.prodInfo[0].createdAt);
                console.log("createdAt ::::::::::::::::::::::", createdAt);
                timeDiff = new Date().getTime() - new Date(updatedTime).getTime();
                console.log("CURR TIME::: ", new Date().getTime(), "UPDTAE TIME:::: ", new Date(updatedTime).getTime())
                console.log(" UPDATE TIME DIFFF::::", timeDiff);
                min = Math.abs(Math.round(timeDiff / 60000));
                console.log(" UPDATE min DIFFF::::", min);
                if (min > config.IDLE_TIMEOUT) {
                    // res.send({ status: 400 })
                    state = false;
                }

                timeDiff = new Date().getTime() - new Date(createdAt).getTime();
                console.log(" CREATE TIME DIFFF::::", timeDiff);
                min = Math.abs(Math.round(timeDiff / 60000));
                console.log(" CREATE min DIFFF::::", min);
                if (min > config.LOGIN_TIMEOUT) {
                    state = false;
                }
                if (state) {
                    console.log("Inside state TRUE:::::::::");
                    try {
                        console.log("Inside state TRY:::::::::");
                        updateSessionStatus(session_id, current_date, 'Valid session',req, res, cdrParams);

                    } catch (e) {
                        res.status(400).send({ 'message': 'Error', status: 401 })
                    }
                } else {
                    res.status(400).send({ 'message': 'Session Timed Out', status: 401 })
                }
            } else {
                res.status(400).send({ 'message': 'Session Not Found', status: 401 })
            }
        })
    } catch (e) {
        console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
        res.status(400).send(e)
    }


}

exports.deleteSessionStatus = async (req, res, cdrParams) => {
    var uniqueID = parseInt(Math.random() * 1000000000, 10);
    var session_id = crypto.decryptValue(req.body.session_id);
    var encodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.SESSION_TABLE_NAME + "&method=delete&equationName=session_id&equationValue=" + session_id)
    try {
        options = {
            method: 'GET',
            uri: encodeURL,
            json: true
        }
        cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
        request.sendResponse(req, res, cdrParams, options).then(function (resp) {
            console.log('Got the following resp:::::::::::::::', resp)
            if (resp.status == 0) {
                res.status(201).send(resp.prodInfo[0]);
            }
        })
    } catch (e) {
        console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
        res.status(400).send({ 'message': e, status: 401 })
    }
}

exports.logoutSessionStatus = async (req, res, cdrParams) => {
   var session_id = crypto.decryptValue(req.body.session_id);
   var current_date = new Date();
   updateSessionStatus(session_id, current_date, 'Session Cleared',req, res, cdrParams);
}

exports.getUserStatus = async (req, res, cdrParams) => {
    var uniqueID = parseInt(Math.random() * 1000000000, 10);
    var cust_id = req.body.cust_id
    var encodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.SESSION_TABLE_NAME + "&method=getCondtional&header=cust_id&values=" + cust_id);
    try {
        options = {
            method: 'GET',
            uri: encodeURL,
            json: true
        }
        cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
        request.sendResponse(req, res, cdrParams, options).then(function (resp) {
            if (resp.status == 0) {
               res.status(200).send({newUser: false, status : 0})
            } else {
                res.status(200).send({newUser: true, status : 0})
            }
        })
    } catch (e) {
        res.status(400).send(e)
    }
}

exports.saveAirtimePaymentStatus = async (req, res, cdrParams) => {
    const payment = new Airtime(req.body)
    try {
        const response = await payment.save()
        res.status(201).send({ response })
    } catch (e) {
        res.status(400).send(e)
    }
}


exports.getDebitCardPaymentStatuses = async (req, res, cdrParams) => {
   var uniqueID = parseInt(Math.random() * 1000000000, 10);
    // var session_id = req.body.session_id;
    var pg_trans_id = req.body.mtnepg_transaction_ref;
    var transaction_status = req.body.transaction_status;

    var encodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.DEBITCARD_TABLE_NAME + "&method=getCondtional&header=transaction_ref&values=" + pg_trans_id);
    try {
        options = {
            method: 'GET',
            uri: encodeURL,
            json: true
        }
        cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
        request.sendResponse(req, res, cdrParams, options).then(function (resp) {
            console.log('Got the following resp:::::::::::::::', resp)
            if (resp.status == 0) {
                res.status(201).send(resp.prodInfo[0]);
            }

        })
    } catch (e) {
        console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
        res.status(400).send({ 'message': e, status: 401 })
    }
}



// exports.getDebitCardPaymentStatuses = async (req, res, cdrParams) => {
//     try {
        
//         // const payment = await DebitCardPayment.findOne({
//         //     pg_trans_id: req.body.mtnepg_transaction_ref,
//         //     transaction_status: req.body.transaction_status,
//         //     transaction_type: req.body.transaction_type
//         // });
//         // res.send({ payment })

//         await DebitCardPayment.findOne({
//             pg_trans_id: req.body.mtnepg_transaction_ref,
//             transaction_status: req.body.transaction_status,
//             transaction_type: req.body.transaction_type
//         }, (err, payResponse) => {       
            
//             console.log("get debit status resp####", payResponse);
            
//             console.log("get debit status err####", err); 
  
//             if(payResponse) {
//                 res.send({ payment: payResponse })
//             } else {
//                 res.send({ payment: null })
//             }
//         })

//     } catch (e) {
//         res.status(400).send(e)
//     }
// }

exports.savenotificationtab = async (req, res, cdrParams) => {
  // const notifi = new NotificationTab(req.body)
    try {
        const response = await NotificationTab.update({ 'cust_id': req.body.cust_id },req.body,{ upsert: true,multi : true },function(err, doc){if(err) throw err;console.log(doc); })        
        res.status(201).send({ response })
    } catch (e) {
        res.status(400).send(e)
    }
}
exports.getnotificationtab = async (req, res, cdrParams) => {
    try {
      const notifi = await NotificationTab.findOne({ 'cust_id': req.body.cust_id })      
        res.send({ notifi })
    } catch (e) {
        res.status(400).send(e)
    }
}

exports.eligibilityCheck = (req, res, cdrParams) => {
    var msisdn = req.body.msisdn;
    console.log('MSISDN in server Session  eligibilityCheck', msisdn);
    cdrParams.activityCategory = "BuyBundlesCIS";
    cdrParams.subCategory = "getBundleCatCIS";
    cdrParams.component = "getBundleCatCIS";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";

    var options = {
        method: 'GET',
        uri: config.CIS_PLUGIN_URL + "&category=services&method=bankECheck&clientTransactionId=" + req.tid + "&msisdn=" + msisdn + "&input=" + req.body.eligibilityId,
        json: true
    }
    console.log("eligibilityCheck-----------" + options.uri);
    logger.writeLog('debug', 'Request for Login ' + options.uri);
    cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
    request.send(req, res, cdrParams, options);
}

exports.activateBundle = (req, res, cdrParams) => {
    var msisdn = req.body.msisdn;
    var transactionId = req.body.transactionId;
    var amount = req.body.amount;
    var action = req.body.action;
    var autoRenew = req.body.autoRenew;
    var beneficiaryMsisdn = req.body.beneficiaryMsisdn;

    console.log('MSISDN in server Session  eligibilityCheck', msisdn);
    cdrParams.activityCategory = "BuyBundlesCIS";
    cdrParams.subCategory = "getBundleCatCIS";
    cdrParams.component = "getBundleCatCIS";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";


    //if(benmsisdn!=null){
    if (beneficiaryMsisdn != msisdn) {
        var options = {
            method: 'GET',
            uri: config.CIS_PLUGIN_URL + "&category=services&method=bankBuyOthers&skipcharging=true&clientTransactionId=" + transactionId + "&price=" + amount + "&input=" + action + "&autorenewal=" + autoRenew + "&msisdn=" + msisdn + "&3ppChargedAmount=" + amount + "&beneficiaryMsisdn=" + beneficiaryMsisdn,
            json: true
        }
    } else {
        var options = {
            method: 'GET',
            uri: config.CIS_PLUGIN_URL + "&category=services&method=bankBuy&skipcharging=true&clientTransactionId=" + transactionId + "&price=" + amount + "&input=" + action + "&autorenewal=" + autoRenew + "&msisdn=" + msisdn + "&3ppChargedAmount=" + amount,
            json: true
        }
    }
    console.log("eligibilityCheck-----------" + options.uri);
    logger.writeLog('debug', 'Request for Login ' + options.uri);
    cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
    request.send(req, res, cdrParams, options);
}


