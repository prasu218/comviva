var config = require('../config');
var request = require('../utils/request');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');
const MsisdnSchema = require('../models/msisdn');

exports.addMsisdn = async (req, res, cdrParams) => {
    cust_msisdn = req.body.cust_msisdn;

    try {
        let msisdn = await MsisdnSchema.findOne({
            cust_msisdn: req.body.cust_msisdn
        });
        if (msisdn) {
            res.status(201).send({ message: 'Msisdn already exist', status: 0 })
        } else {
            new_msisdn = new MsisdnSchema({ cust_msisdn })
            const response = await new_msisdn.save()
            res.status(201).send({ message: cust_msisdn + " added successfully", status: 0 })
        }
    } catch (e) {
        res.status(400).send({ message: e, status: 401 })
    }
}

exports.validateMSISDN = async (req, res, cdrParams) => {
    var msisdn = req.body.msisdn;
    try {
        const msisdn = await MsisdnSchema.findOne({
            cust_msisdn: req.body.cust_msisdn
        });
        if (msisdn) {
            res.status(201).send({ message: 'Valid Msisdn', status: 0 })
        } else {
            res.status(400).send({ message: 'Msisdn Not Found', status: 401 })
        }
    } catch (e) {
        res.status(400).send({ message: e, status: 401 })
    }
}

exports.updateMSISDN = async (req, res, cdrParams) => {
    try {
        const msisdn = await MsisdnSchema.findByIdAndUpdate({
            cust_msisdn: req.body.cust_msisdn
        }, {
            cust_msisdn: req.body.cust_msisdn
        });
        console.log("UPDATE :::::", msisdn);
        if (msisdn) {
            res.status(201).send({ message: 'Updated Msisdn successfully', status: 0 })
        } else {
            res.status(404).send({ message: req.body.cust_msisdn + ' Msisdn not found', status: 0 })
        }
    } catch (e) {
        res.status(400).send({ message: e, status: 401 })
    }
}

exports.deleteMSISDN = async (req, res, cdrParams) => {
    try {
        const msisdn = await MsisdnSchema.findOneAndDelete({
            cust_msisdn: req.body.cust_msisdn
        });
        if (msisdn) {
            res.status(201).send({ message: req.body.cust_msisdn + ' Msisdn deleted successfully', status: 0 })
        } else {
            res.status(404).send({ message: req.body.cust_msisdn + ' MSISDN not found', status: 0 })
        }
    } catch (e) {
        res.status(400).send({ message: e, status: 401 })
    }
}
