var config = require('../config');
var request = require('request');
var request1 = require('../utils/request');
var logger = require('../utils/logger');
const DebitCardPayment = require('../models/debitCard')
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');
exports.getDebitcardDetails = (req, res, cdrParams) => {
  var msisdn = req.body.mtnepg_cust_mobile;
  var amount = req.body.mtnepg_transaction_amount;
  var email = req.body.mtnepg_cust_email;
  var firstName = req.body.mtnepg_cust_name;
  var uniqueID = parseInt(Math.random()*1000000000, 10); 
  
  // console.log("requestdc" + JSON.stringify(req));
  // var msisdn = "09062058850";
  // var amount = "345";
  // var email = "David.ugowe@mtn.com";
  // var firstName = "Ifor";

  // var  mtnepg_secret_key = "6f6548d04f7e7416a3b62e25169cc9b10d283c3e69eee262c8ad78ae81432725852e0acdb6890f2e2bbeed47ed0ccf68d55a170cde4b6d1af26f0c7c80ac053b"
  // var mtnepg_transaction_ref = "SMARSMAR00000186A0125511711";
  // var mtnepg_cust_desc = "test";
  // var mtnepg_transaction_detail = "{'Airtime Sales',1,100000,100000,NoPartCode}"
  cdrParams.activityCategory = "Login";
  cdrParams.subCategory = "getDebitcardRcgDetails";
  cdrParams.component = "getDebitcardRcgDetails";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  console.log("amount"+amount);
  console.log("uniqueID" + uniqueID);
  options = {
    method: 'GET',
    uri:  config.PAYMENT_URL +"Amount="+ amount+"&CustomerID="+msisdn+"&CustomerMobile="+msisdn+"&CustomerName="+firstName+"&CustomerEmail="+email+"&TransID="+uniqueID+"&CustomerDesc=test&Category=Airtime&Method=Purchase",
    json: true
  }
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  //request1.send(req, res, cdrParams, options);

   // logger.writePaymentLog(LOG_ORDER_NUMBER, 'warn','Payment Flow - Step 5 - Payment Gateway Redirection - Error Caught - ' + error + '\n' +error.stack);
//       //utils.sendFailureBeforePayment(req, res, cdrParams);
   request(options, function(error, response, body) {
   console.log("testing the request");
   console.log("req.body.transaction_type",req.body.transaction_type);
    console.log("responce" +JSON.stringify(body) );
 if( (body.statusmessage == 'success' || body.statuscode === 0)) {
   console.log("responce" +JSON.stringify(body) );
       amount = body.mtnepg_transaction_amount,
       msisdn = body.mtnepg_cust_id,
       firstName = body.mtnepg_cust_name,
       mtnepg_cust_mobile = body.mtnepg_cust_mobile,
       email  = body.mtnepg_cust_email,      
       mtnepg_cust_desc = body.mtnepg_cust_desc,
       mtnepg_transaction_detail = body.mtnepg_transaction_detail,
       mtnepg_secret_key = body.mtnepg_secret_key
	   const payment = new DebitCardPayment({
            pg_trans_id: body.mtnepg_transaction_ref,
            cust_id: msisdn,
            cust_name: firstName,
            cust_mobile: mtnepg_cust_mobile,
            cust_email: email,
            transaction_amount: amount,
            transaction_status: 'Pending',
			transaction_type: req.body.transaction_type
        });
	   payment.save().then((resp)=>{
	     console.log('successful db transaction',resp);
	   }).catch(err => {
	   console.log('Error db transaction',err.message)
	   }); 
 console.log("parameters" +body.mtnepg_transaction_ref + 'amount' +body.mtnepg_transaction_amount +'cust id'+ body.mtnepg_cust_id +'name'+ body.mtnepg_cust_name + 'mobile'+ body.mtnepg_cust_mobile + 'email' + body.mtnepg_cust_email + 'cust_desc' + body.mtnepg_cust_desc + 'transaction details' +body.mtnepg_transaction_detail + 'key' + body.mtnepg_secret_key)
//  body = {"mtnepg_secret_key":"6f6548d04f7e7416a3b62e25169cc9b10d283c3e69eee262c8ad78ae81432725852e0acdb6890f2e2bbeed47ed0ccf68d55a170cde4b6d1af26f0c7c80ac053b",
//  "mtnepg_cust_name":"Ifor","mtnepg_cust_desc":"test","mtnepg_cust_email":"david.ugowe@mtn.com","statusmessage":"success",
//  "mtnepg_cust_mobile":"2349062058852","mtnepg_transaction_detail":"{'Airtime Sales',1,100000,100000,NoPartCode}",
//  "mtnepg_transaction_amount":100000,"mtnepg_cust_id":"2349062058852","statuscode":0,"mtnepg_transaction_ref":"SMARSMAR00000186A0125511711"};
paymentgatewayRedirect(body, res, cdrParams);
 }
 else{
   console.log("failed" + error);
 }
});
}
exports.getDebitcardOthersDetails = (req, res, cdrParams) => {
  var msisdn = req.body.mtnepg_cust_mobile;
  var amount = req.body.mtnepg_transaction_amount;
  var email = req.body.mtnepg_cust_email;
  var firstName = req.body.mtnepg_cust_name; 
  var uniqueID = parseInt(Math.random()*1000000000, 10);
  // console.log("requestdc" + JSON.stringify(req));
  // var msisdn = "09062058850";
  // var amount = "345";
  // var email = "David.ugowe@mtn.com";
  // var firstName = "Ifor";

  // var  mtnepg_secret_key = "6f6548d04f7e7416a3b62e25169cc9b10d283c3e69eee262c8ad78ae81432725852e0acdb6890f2e2bbeed47ed0ccf68d55a170cde4b6d1af26f0c7c80ac053b"
  // var mtnepg_transaction_ref = "SMARSMAR00000186A0125511711";
  // var mtnepg_cust_desc = "test";
  // var mtnepg_transaction_detail = "{'Airtime Sales',1,100000,100000,NoPartCode}"
  cdrParams.activityCategory = "Login";
  cdrParams.subCategory = "getDebitcardRcgDetails";
  cdrParams.component = "getDebitcardRcgDetails";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  console.log("amount"+amount);
  console.log("uniqueID" + uniqueID);
  options = {
    method: 'GET',
    uri:  config.PAYMENT_URL+"Amount="+ amount+"&CustomerID="+msisdn+"&CustomerMobile="+msisdn+"&CustomerName="+firstName+"&CustomerEmail="+email+"&TransID="+uniqueID+"&CustomerDesc=test&Category=Airtime&Method=Purchase",
    json: true
  }
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  //request1.send(req, res, cdrParams, options);

   // logger.writePaymentLog(LOG_ORDER_NUMBER, 'warn','Payment Flow - Step 5 - Payment Gateway Redirection - Error Caught - ' + error + '\n' +error.stack);
//       //utils.sendFailureBeforePayment(req, res, cdrParams);
   request(options, function(error, response, body) {
   console.log("testing the request");
   console.log("req.body.transaction_type",req.body.transaction_type);
    console.log("responce" +JSON.stringify(body) );
 if( (body.statusmessage == 'success' || body.statuscode === 0)) {
   console.log("responce" +JSON.stringify(body) );
       amount = body.mtnepg_transaction_amount,
       msisdn = body.mtnepg_cust_id,
       firstName = body.mtnepg_cust_name,
       mtnepg_cust_mobile = body.mtnepg_cust_mobile,
       email  = body.mtnepg_cust_email,      
       mtnepg_cust_desc = body.mtnepg_cust_desc,
       mtnepg_transaction_detail = body.mtnepg_transaction_detail,
       mtnepg_secret_key = body.mtnepg_secret_key
	   
	   const payment = new DebitCardPayment({
            pg_trans_id: body.mtnepg_transaction_ref,
            cust_id: msisdn,
            cust_name: firstName,
            cust_mobile: mtnepg_cust_mobile,
            cust_email: email,
            transaction_amount: amount,
            transaction_status: 'Pending',
			transaction_type: req.body.transaction_type
        });
	   payment.save().then((resp)=>{
	    console.log('successful db transaction',resp);
	   }).catch(err => {
	   console.log('Error db transaction',err.message)
	   }); 
 console.log("parameters" +body.mtnepg_transaction_ref + 'amount' +body.mtnepg_transaction_amount +'cust id'+ body.mtnepg_cust_id +'name'+ body.mtnepg_cust_name + 'mobile'+ body.mtnepg_cust_mobile + 'email' + body.mtnepg_cust_email + 'cust_desc' + body.mtnepg_cust_desc + 'transaction details' +body.mtnepg_transaction_detail + 'key' + body.mtnepg_secret_key)
//  body = {"mtnepg_secret_key":"6f6548d04f7e7416a3b62e25169cc9b10d283c3e69eee262c8ad78ae81432725852e0acdb6890f2e2bbeed47ed0ccf68d55a170cde4b6d1af26f0c7c80ac053b",
//  "mtnepg_cust_name":"Ifor","mtnepg_cust_desc":"test","mtnepg_cust_email":"david.ugowe@mtn.com","statusmessage":"success",
//  "mtnepg_cust_mobile":"2349062058852","mtnepg_transaction_detail":"{'Airtime Sales',1,100000,100000,NoPartCode}",
//  "mtnepg_transaction_amount":100000,"mtnepg_cust_id":"2349062058852","statuscode":0,"mtnepg_transaction_ref":"SMARSMAR00000186A0125511711"};
paymentgatewayRedirect(body, res, cdrParams);
 }
 else{
   console.log("failed" + error);
 }
});
}
 function paymentgatewayRedirect(body, res, cdrParams) {
   console.log("testing redirection");
  try {
    console.log("checking try");
//     //logger.writePaymentLog(LOG_ORDER_NUMBER, 'debug', 'Step 5 - Payment Gateway Redirection -  STARTS');
     
     console.log("parameters" +body.mtnepg_transaction_ref + 'amount' +body.mtnepg_transaction_amount +'cust id'+ body.mtnepg_cust_id +'name'+ body.mtnepg_cust_name + 'mobile'+ body.mtnepg_cust_mobile + 'email' + body.mtnepg_cust_email + 'cust_desc' + body.mtnepg_cust_desc + 'transaction details' +body.mtnepg_transaction_detail + 'key' + body.mtnepg_secret_key)
   var form = "<!DOCTYPE html><html><body><p>You are being redirected...Please wait</p>" +
       "<form action=" + config.PAYGATE_URL + " method='POST' name='paymentResponse'>" +
       "<input type='hidden' name='mtnepg_transaction_ref' id='mtnepg_transaction_ref' value='"+body.mtnepg_transaction_ref+"'> " +
        "<input type='hidden' name='mtnepg_transaction_amount' id='mtnepg_transaction_amount' value='"+body.mtnepg_transaction_amount+"'><br>" +
        "<input type='hidden' name='mtnepg_cust_id' id='mtnepg_cust_id' value='"+body.mtnepg_cust_id+"'><br>" +
        "<input type='hidden' name='mtnepg_cust_name' id='mtnepg_cust_name' value='"+body.mtnepg_cust_name+"'><br>" +
        "<input type='hidden' name='mtnepg_cust_mobile' id='mtnepg_cust_mobile' value='"+body.mtnepg_cust_id+"'><br>" +
        "<input type='hidden' name='mtnepg_cust_email' id='mtnepg_cust_email' value='"+body.mtnepg_cust_email+"'><br>" +
        "<input type='hidden' name='mtnepg_cust_desc' id='mtnepg_cust_desc' value='"+body.mtnepg_cust_desc+"'><br>" +
        "<input type='hidden' id='mtnepg_secret_key' name='mtnepg_secret_key' value='"+body.mtnepg_secret_key+"'><br>"+
        "<input type='hidden' name='mtnepg_transaction_detail' id='mtnepg_transaction_detail' value='"+body.mtnepg_transaction_detail.replace(/\\/g, "")+"'></form>"+
       "<script language='JavaScript'>document.forms['paymentResponse'].submit();</script></body></html>";
  // var form = "<!DOCTYPE html><html><body><p>You are being redirected...Please wait</p>" +
	// 				"<form action=" + "https://tjccpg.jccsecure.com/EcomPayment/RedirectAuthLink" + " method='POST' name='paymentResponse'>" +
	// 				"<input type='hidden' name='PAY_REQUEST_ID' value='"+"req.body.pay_request_id" +"'> " +
	// 				"<input type='hidden' name='CHECKSUM' value='" + "checkSum" + "'></form> " +
	// 				"<script language='JavaScript'>document.forms['paymentResponse'].submit();</script></body></html>";
  //      console.log("form length"+form.length);
//      // logger.writePaymentLog(LOG_ORDER_NUMBER, 'debug', 'Step 5 - Payment Gateway Redirection -  Form Submitting to Payment Gateway - ' + JSON.stringify(form));
     res.set('Content-Type', 'text/html');
//       //logger.writePaymentLog(LOG_ORDER_NUMBER, 'debug', 'Step 5 - Payment Gateway Redirection -  REDIRECTION STARTS');
    res.send(form);
    } catch(error) {
    console.log('failed- ' + error);
//      // logger.writePaymentLog(LOG_ORDER_NUMBER, 'warn','Payment Flow - Step 5 - Payment Gateway Redirection - Error Caught - ' + error + '\n' +error.stack);
//       //utils.sendFailureBeforePayment(req, res, cdrParams);
//   }
  
}
 }

exports.getDebitCardTransStatus = (req, res, cdrParams) => {
  var pg_trans_id = req.body.mtnepg_transaction_ref;
  var transaction_status = req.body.transaction_status;

  var uniqueID = parseInt(Math.random() * 1000000000, 10);
  var getEncodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.DEBITCARD_TABLE_NAME + "&method=getCondtional&header=transaction_ref&values=" + pg_trans_id);
  try {
    options = {
      method: 'GET',
      uri: getEncodeURL,
      json: true
    }
    // cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
    request1.sendResponse(req, res, cdrParams, options).then(function (resp) {
      // console.log("FAILURE RESP:::::", resp);
      var payload = {
        mtnepg_transaction_ref: pg_trans_id,
        transaction_status: transaction_status
      }
      if (resp.status == 0) {
        var trans_status = resp.prodInfo[0].transaction_status
        if (trans_status != 'Activated') {
          updateDebitCardTransStatus(payload, req, res, cdrParams);
        } else {
          res.status(200).send({ message: "Selected bundle is already Activated", status: 1 });
        }
      } else {
        console.log('Error occured while updating the debit card Failure status record')
      }
    })
  } catch (e) {
    console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
  }
}

var updateDebitCardTransStatus = function (payload, req, res, cdrParams) {
  var uniqueID = parseInt(Math.random() * 1000000000, 10);
  var pg_trans_id = payload.mtnepg_transaction_ref;
  var transaction_status = payload.transaction_status;
  var updateEncodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.DEBITCARD_TABLE_NAME + "&method=updateAll&equationName=transaction_ref&equationValue=" + pg_trans_id + "&header=" + config.UPDATE_DEBITCARD_HEADER + "&values=" + pg_trans_id + ":::" + transaction_status);
  try {
    options = {
      method: 'GET',
      uri: updateEncodeURL,
      json: true
    }
    // cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
    request1.sendResponse(req, res, cdrParams, options).then(function (resp) {
      // console.log("FAILURE RESP:::::", resp);
      if (resp.status == 0) {
        console.log("Updated Debit card Failure status successfully")
      } else {
        console.log('Error occured while updating the debit card Failure status record')
      }
    })
  } catch (e) {
    console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
  }
}
