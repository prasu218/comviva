var config = require('../config');
var request = require('../utils/request');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');
var crypto = require('../utils/crypto');

exports.getfetchingprofile= (req, res, cdrParams) => {
    var msisdn = req.body.msisdn;

    console.log("msisdn getfetchingprofile"+msisdn);
    // var fetchprofilepic = req.body.fetchprofilepic;
    cdrParams.activityCategory = "getfetchingprofile";
    cdrParams.subCategory = "getfetchingprofile";
    cdrParams.component = "getfetchingprofile";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";
    options = {
      method: 'GET',
    uri: config.PLUGIN_URL + "&transID=" + msisdn + "&msisdn=" + msisdn + "&category=myAccount&method=fetchprofilepic",
      json: true
    }

    cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
    request.send(req, res, cdrParams, options);  
 }

exports.getpicprofile= (req, res, cdrParams) => {
var msisdn = req.body.msisdn;
var customerid = msisdn;
// var submissionDate = req.body.submissiondate;
var encodedprofilepic = req.body.encodedprofilepic;

var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0');
var yyyy = today.getFullYear();
var hr = today.getHours() > 12 ? today.getHours() - 12 : (today.getHours() < 10 ? "0" + today.getHours() : today.getHours());
var mins = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
var meridem = today.getHours() > 12 ? "PM" : "AM";

today = mm + '/' + dd + '/' + yyyy + ' ' + hr + ':' + mins + ' '+meridem;

var submissionDate = today;

console.log("IMAGE:::::::::::decoded:::::::::::::::::::", encodedprofilepic);	
var payload = "plugin=smartweb&AppsName=selfcare&Interface=client&Format=json&opcoID=NG&group=Nigeria&transID="+req.tid+"&msisdn="+msisdn+"&category=myAccount&method=addprofilepic&doctitle="+msisdn+"&customerid="+customerid+"&submissiondate="+submissionDate+"&referenceid="+msisdn+"&guid="+msisdn+"&encodedprofilepic="+encodedprofilepic;

console.log("IMAGE:::::::::::payload  --> ", payload );
  cdrParams.activityCategory = "getpicprofile";
  cdrParams.subCategory = "getpicprofile";
  cdrParams.component = "getpicprofile";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  const options = {
    method: 'POST',
    uri: config.PLUGIN_URL,
    body: payload,
    json: true
  }
  cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length); 
  request.send(req, res, cdrParams, options);
}

function addProfileImg(msisdn, profileImg, date, req, res, cdrParams) {
  var uniqueID = parseInt(Math.random() * 1000000000, 10);
  var encodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.PROFILE_TABLE_NAME + "&method=insert&header=" + config.PROFILE_HEADER + "&values=" + msisdn + ":::" + profileImg + ":::" + date + ":::" + date)
  try {
    options = {
      method: 'GET',
      uri: encodeURL,
      json: true
    }
    cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
    request.sendResponse(req, res, cdrParams, options).then(function (resp) {
      console.log('Got the following resp:::::::::::::::', resp)
      if (resp.status == 0) {
        res.status(201).send({ message: 'Added profile image successfully', status: 0 })
      } else {
        res.status(400).send({ 'message': 'Error occured while adding the profile image', status: 401 })
      }
    })
  } catch (e) {
    res.status(400).send({ message: e, status: 401 })
  }
}

function updateProfileImg(msisdn, profileImg, current_date, message, req, res, cdrParams) {
  var uniqueID = parseInt(Math.random() * 1000000000, 10);
  var updateEncodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.PROFILE_TABLE_NAME + "&method=updateAll&equationName=msisdn&equationValue=" + msisdn + "&header=" + config.UPDATE_PROFILE_HEADER + "&values=" + profileImg +":::"+current_date); 
  try {
    options = {
      method: 'GET',
      uri: updateEncodeURL,
      json: true
    }
    cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
    request.sendResponse(req, res, cdrParams, options).then(function (resp) {
      console.log('Got the following resp:::::::::::::::', resp)
      if (resp.status == 0) {
        res.status(201).send({ message: message, status: 0 });
      } else {
        res.status(400).send({ 'message': 'Error occured while updating the profile image', status: 401 })
      }
    })
  } catch (e) {
    console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
    res.status(400).send({ 'message': e, status: 401 })
  }
}

exports.createProfileImg = (req, res, cdrParams) => {
  console.log('createSessionStatus  called:::');
  var uniqueID = parseInt(Math.random() * 1000000000, 10);
  var msisdn = req.body.msisdn;
  var current_date = new Date();
    var profileImg = req.body.encodedprofilepic;
     var encodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.PROFILE_TABLE_NAME + "&method=getCondtional&header=msisdn&values=" + msisdn);
  try {
    options = {
      method: 'GET',
      uri: encodeURL,
      json: true
    }
    cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
    request.sendResponse(req, res, cdrParams, options).then(function (resp) {
            // console.log('GET SESSION ID the following resp:::::::::::::::', resp.status)
      if (resp.status == 0) {
        updateProfileImg(msisdn, profileImg, current_date, 'Updated profile image successfully', req, res, cdrParams)
      } else {
        addProfileImg(msisdn, profileImg, current_date, req, res, cdrParams);
      }
    })
  } catch (e) {
    console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
    res.status(400).send(e)
  }

}

exports.getProfileImg = (req, res, cdrParams) => {
  console.log('createSessionStatus  called:::');
  var uniqueID = parseInt(Math.random() * 1000000000, 10);
  var msisdn = req.body.msisdn;
  var encodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.PROFILE_TABLE_NAME + "&method=getCondtional&header=msisdn&values=" + msisdn);
  try {
    options = {
      method: 'GET',
      uri: encodeURL,
      json: true
    }
    cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
    request.sendResponse(req, res, cdrParams, options).then(function (resp) {
      console.log('GET PROFILE IMAGE the following resp:::::::::::::::', resp)
      if (resp.status == 0) {
        var encodedprofilepic = resp.prodInfo[0].profileImg;
        res.status(201).send({encodedprofilepic: encodedprofilepic, message: "Found the image successfully", status: 0});
      } else {
        res.status(400).send({ 'message': 'Profile image not found for MSISDN ' + msisdn, status: 401 })
      }
    })
  } catch (e) {
    console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
    res.status(400).send(e)
  }

}






