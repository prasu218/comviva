var config = require('../config');
var request = require('../utils/request');
var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');

  
  exports.getAllState = (req, res, cdrParams) => {
    var msisdn = req.body.msisdn;
    cdrParams.activityCategory = "HELP";
    cdrParams.subCategory = "getAllState";
    cdrParams.component = "getAllState";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";
    options = {
     method: 'GET',
     uri:  config.DB_PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn + "&prefLang=EN&dbName=appDB&query=getState", 

     json: true
    }
  
    
    logger.writeLog('debug','getAllState'+ options.uri);
    cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
    request.send(req, res, cdrParams, options);
  
  }
  

  exports.getAllLGAs = (req, res, cdrParams) => {
    var msisdn = req.body.msisdn;
    cdrParams.activityCategory = "HELP";
    cdrParams.subCategory = "getAllLGAs";
    cdrParams.component = "getAllLGAs";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";
    options = {
     method: 'GET',
     uri:  config.DB_PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn + "&prefLang=EN&dbName=appDB&query=getLGAS&values="+req.body.state, 
     json: true
    }
  
    
    logger.writeLog('debug','getAllLGAs'+ options.uri);
    cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
    request.send(req, res, cdrParams, options);
  
  }

  exports.getServiceProvinceCenters = (req, res, cdrParams) => {
    var msisdn = req.body.msisdn;
    cdrParams.activityCategory = "HELP";
    cdrParams.subCategory = "getServiceProvinceCenters";
    cdrParams.component = "getServiceProvinceCenters";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";
    options = {
     method: 'GET',
     uri:  config.DB_PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn + "prefLang=EN&dbName=appDB&query=getServiceCenters&values="+req.body.state+";"+req.body.lga, 
								
     json: true
    }
  
    logger.writeLog('debug','getServiceProvinceCenters'+ options.uri);
    cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
    request.send(req, res, cdrParams, options);
  }

exports.getCurrentaddress = (req, res, cdrParams) => {
    //var msisdn = req.body.msisdn;
    cdrParams.activityCategory = "HELP";
    cdrParams.subCategory = "getCurrentaddress";
    cdrParams.component = "getCurrentaddress";
    cdrParams.offerCode = "NA";
    cdrParams.amount = "NA";
    options = {
     method: 'GET',
     uri: "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCNh7O2WN1enMTcDry9z8SNojRzIQiv93M&latlng=" +req.body.latitude+ "," +req.body.longitude+ "&sensor=false",
     json: true
    }
    logger.writeLog('debug','getCurrentaddress'+ options.uri);
    cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
    request.send(req, res, cdrParams, options);
  }
  
  
  
  
  