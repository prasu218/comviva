var config = require('../config');
var request = require('../utils/request');
var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');


exports.checkBorrowEligibility = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
  cdrParams.activityCategory = "RECHARGE";
  cdrParams.subCategory = "checkBorrowEligibility";
  cdrParams.component = "checkBorrowEligibility";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
    var options = {
      method: 'GET',
       uri:config.PLUGIN_URL +"&transID="+req.tid+"&Format=json&opcoID=NG&group=Nigeria&msisdn="+msisdn+"&category=airtime&method=borrowEligibility",
      //uri:  config.PLUGIN_URL + "&transID="+req.tid+"&msisdn="+msisdn+"&category=airtime&method=borrowEligibility",
      json: true
    }
  logger.writeLog('debug','Request for Check Borrow Eligibility '+ options.uri);
  request.send(req, res,cdrParams, options); 
}



exports.getBorrowAirTime = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
  var serviceid = req.body.serviceId;
  cdrParams.activityCategory = "RECHARGE";
  cdrParams.subCategory = "getBorrowAirTime";
  cdrParams.component = "getBorrowAirTime";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
    var options = {
      method: 'GET',
        uri: config.PLUGIN_URL+ "transID="+req.tid+"&Format=json&opcoID=NG&group=Nigeria&msisdn="+msisdn+"&category=airtime&method=borrowairtime&serviceid="+serviceid,
      //uri:  config.PLUGIN_URL + "&transID="+req.tid+"&msisdn="+msisdn+"&category=airtime&method=borrowairtime&serviceid="+serviceid,
      json: true
    }
  //logger.writeLog('debug','Request for Borrow Air Time '+ options.uri);
   request.send(req, res,cdrParams, options);
}


/*
exports.checkBorrowAirBalance = (req, res,cdrParams) => {
  var msisdn = req.body.msisdn;
  cdrParams.activityCategory = "RECHARGE";
  cdrParams.subCategory = "checkBorrowAirBalance";
  cdrParams.component = "checkBorrowAirBalance";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
    var options = {
      method: 'GET',
      // uri:  config.PLUGIN_URL + "&transID="+req.tid+"&transID=37213197931&msisdn="+msisdn+"&category=airtime&method=checkborrowBalance",
      uri:  config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn=" + msisdn + "&category=airtime&method=checkborrowBalance",
      json: true
    }
  //logger.writeLog('debug','Request for Check Borrow Air Balance '+ options.uri);
 request.send(req, res,cdrParams, options);
}*/