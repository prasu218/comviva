var config = require('../config');
var request = require('../utils/request');
//var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');


exports.getProfileDetails = (req, res, cdrParams) => {
	
	  console.log("MTN_HEADER Details",req.headers);
	  
	  var header=req.headers;
	  	//alert("header");
	  console.log("MTN_HEADER Details--",req.headers);
	  
  //console.log(JSON.stringify(req.headers()))
  var msisdn = req.body.msisdnother;
   //console.log("header request mohan------",(req.headers()));
  //console.log(JSON.stringify(req.headers()));
  
  cdrParams.activityCategory = "Profile";
  cdrParams.subCategory = "getProfileDetails";
  cdrParams.component = "getProfileDetails";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  
  
  options = {
    method: 'GET',
    uri:  config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+"&category=myAccount&method=getCustomerProfile1",
	   
    json: true
  }
//  logger.writeLog('debug','Request for Login '+ options.uri);

  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  request.send(req, res, cdrParams, options);
}

exports.validateProfileDetails = (req, res, cdrParams) => {
	
  console.log("MTN_HEADER Details",req.headers);
  
  var header=req.headers;
    //alert("header");
  console.log("MTN_HEADER Details--",req.headers);
  
//console.log(JSON.stringify(req.headers()))
var msisdn = req.body.msisdn;
 //console.log("header request mohan------",(req.headers()));
//console.log(JSON.stringify(req.headers()));

cdrParams.activityCategory = "Profile";
cdrParams.subCategory = "getProfileDetails";
cdrParams.component = "getProfileDetails";
cdrParams.offerCode = "NA";
cdrParams.amount = "NA";


options = {
  method: 'GET',
  uri:  config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+"&category=myAccount&method=getCustomerProfile1",
   
  json: true
}
//  logger.writeLog('debug','Request for Login '+ options.uri);

cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
request.sendProfileValidity(req, res, cdrParams, options);
}


exports.getfarwphone = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
  var voicemail = req.body.voicemail;
  cdrParams.activityCategory = "callforwardingtophone";
  cdrParams.subCategory = "getfarwphone";
  cdrParams.component = "getfarwphone";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',

uri: config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+"&voicemail="+voicemail + "&category=services&method=activatecallforwarding",
    json: true
  }


  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  request.send(req, res, cdrParams, options);
}

exports.getfarwvoicemail = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
  var voicemail = req.body.voicemail ;
  cdrParams.activityCategory = "callforwardingtovoicemail";
  cdrParams.subCategory = "getfarwvoicemail";
  cdrParams.component = "getfarwvoicemail";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',
      uri: config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+"&voicemail="+voicemail + "&category=services&method=activatecallforwarding",
    json: true
  }


  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  request.send(req, res, cdrParams, options);
}

exports.getdeactivatecallfarw = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
  var voicemail = req.body.voicemail ;
  cdrParams.activityCategory = "calldeactivatecallfarw";
  cdrParams.subCategory = "getdeactivatecallfarw";
  cdrParams.component = "getdeactivatecallfarw";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',
    
 	uri: config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+"&voicemail="+voicemail + "&category=services&method=deactivatecallforwarding",  
    json: true
  }


  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  request.send(req, res, cdrParams, options);
}


exports.getresetbarr = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;

  cdrParams.activityCategory = "Resetcallbarring";
  cdrParams.subCategory = "getresetbarr";
  cdrParams.component = "getresetbarr";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',
    uri: config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+ "&category=myAccount&method=resetcallbarring",  
    json: true
  }

//   console.log('Request for callbarring '+ options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  request.send(req, res, cdrParams, options);
}

exports.getcallbarring = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;

  cdrParams.activityCategory = "callbarringincoming";
  cdrParams.subCategory = "getcallbarring";
  cdrParams.component = "getcallbarring";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',
    uri: config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+ "&category=services&method=callbarring",
    json: true
  }

//   console.log('Request for callbarring '+ options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  request.send(req, res, cdrParams, options);
}

exports.getcallunbarring = (req, res, cdrParams) => {
  var msisdn = req.body.msisdn;
 
  cdrParams.activityCategory = "callunbarringincoming";
  cdrParams.subCategory = "getcallunbarring";
  cdrParams.component = "getcallunbarring";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  options = {
    method: 'GET',
    uri: config.PLUGIN_URL + "&transID=" + req.tid + "&msisdn="+msisdn+ "&category=services&method=callunbarring",      
    json: true
  }

//   console.log('Request for callbarring '+ options.uri);
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  request.send(req, res, cdrParams, options);
}



exports.getHeaders= (req, res,cdrParams) => {
	
	//cdrParams.activityCategory = "callunbarringincoming";
	//cdrParams.subCategory = "getcallunbarring";
	//cdrParams.component = "getcallunbarring";
	//cdrParams.offerCode = "NA";
	//cdrParams.amount = "NA";
	
  var headers = req.headers;

 console.log("header--------------------------------------------------------",header);


}
