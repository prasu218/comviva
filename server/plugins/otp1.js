var config = require('../config');
var request = require('../utils/request');
var logger = require('../utils/logger');
var cdr = require('../utils/cdr');
var sha256 = require('js-sha256');
var http = require('http');
var crypto = require('../utils/crypto');
const jwt = require("jsonwebtoken");

exports.generateOTP = function(req, res, cdrParams) {
	var n = Math.floor(Math.random() * 11);
	var msisdn = req.body.msisdn;
	//var otp = Math.floor(100000 + Math.random() * 900000);
	var otp = "111111";
	req.session.otp=otp;
	const eMailId = req.body.eMailId;
	const supp_id = req.body.supp_id;
	const ccmailid = req.body.ccmailid;
	const emailFlag = req.body.emailFlag;
	const firstName = req.body.firstName;
    const linkAccFlag =  req.body.linkAccountFlag;
	var OTPMessageBody;
        if (linkAccFlag === true || linkAccFlag === "true") {
	      OTPMessageBody = "Dear "+firstName+", someone is trying to add your number, the OTP is " +otp+ ". MTN will never ask for your OTP. Please do not share it";
	} else {
	      OTPMessageBody ="Dear "+firstName+", your myMTN Web OTP is "+otp+". MTN will never ask for your OTP. Please do not share it";
	}

	var subject = decodeURIComponent("myMTN | OTP");

	if(emailFlag == true || emailFlag == "true" ){

var OTPMessageBody = ["<html><p>Dear<b> ", firstName, "</b>,</p><p>,your myMTN Web OTP is ", otp, ". MTN will never ask for your OTP. Please do not share it</p>"].join("");
   OTPMessageBody = [OTPMessageBody, "<p>Thank you.<br/><br/>Regards,<br/><b>myMTN</b>"].join("");
   OTPMessageBody = [OTPMessageBody, "</html>"].join("");
		//API call for Sending Email for the OTP generated
		var options = {
				method: 'GET',
				uri:  config.EMAIL_PLGUN_URL + "&transID=" + req.tid + "&username=" + encodeURIComponent(eMailId) + "&to=" + supp_id + "&subject=" + encodeURIComponent(subject) + "&body=" + encodeURIComponent(OTPMessageBody) + "&msisdn=" + msisdn + "&from=" + encodeURIComponent(eMailId) + "&cc=" + encodeURIComponent(ccmailid),
				json: true
		}
	}else{
	
		//API call for Sending SMS for the OTP generated
		var options = {
				method: 'GET',
				uri:  config.PLUGIN_URL+ "&transID=" + req.tid + "&msisdn="+req.body.msisdn + "&category=service&method=sendSMS&msgbody="+OTPMessageBody, 
				json: true
		}
	}
		
		logger.writeLog('debug','sendSMS'+ options.uri);
		cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
		request.send(req, res, cdrParams, options);
		
		var encryptedOtp = crypto.encryptValue(otp);
		var response = { "status": 0, "otp": encryptedOtp };		
		return res.status(200).json(response);


}


exports.validateOTP = async function(req, res, cdrParams) {
	var msisdn = req.body.msisdn;
	var status=-1;
	// validateotp=req.body.otp;
	var decryptedUserOtp = crypto.decryptValue(req.body.otp);
        console.log("decryptedUserOtp ::::", decryptedUserOtp );
	var decryptedSessionOtp = crypto.decryptValue(req.body.sessionOtp);
        console.log("decryptedSessionOtp ::::", decryptedSessionOtp );

	if(Number(decryptedUserOtp) == Number(decryptedSessionOtp)){
				status=0;
				const payload  =  {
					msisdn: req.body['msisdn']
						}
			 console.log("Thiis is payload before signing", payload);
			 let tokenSave;
			 await jwt.sign(payload, config.tokhash, (err, token) =>{
			   if(!err){
				   tokenSave = token;
				   console.log("This is the token sent to user ", tokenSave);
				//return res.status(200).json({token: token});
			   }else{
				tokenSave = '';
			   }
			 
			 });

	var response = {"status": 0, "Valid": 'Valid OTP', "token": tokenSave };
	return res.status(200).json(response);
	}

	//Params.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
	var response = {"status": -1, "Valid": 'Invalid OTP'};
	return res.status(200).json(response);
	//request.send(req, res, cdrParams, options);
}


exports.resendOTP = function(req, res, cdrParams) {
	var n = Math.floor(Math.random() * 11);
	//var otp = Math.floor(100000 + Math.random() * 900000);
	var otp = "111111";
	req.session.otp=otp;

	const eMailId = req.body.eMailId;
	const supp_id = req.body.supp_id;
	const ccmailid = req.body.ccmailid;
	var subject = decodeURIComponent("myMTN | OTP");
	var OTPMessageBody = "Your myMTN Web OTP code is "+ otp+".Please do not share your OTP with anyone.";

	if(emailFlag == true || emailFlag == "true" ){
		//API call for Sending Email for the OTP generated

	var subject = decodeURIComponent("myMTN | OTP");
   var OTPMessageBody = ["<html><p>Dear<b> ", firstName, ",</b></p><p>,your myMTN Web OTP is ", otp, ". MTN will never ask for your OTP. Please do not share it</p>"].join("");
   OTPMessageBody = [OTPMessageBody, "<p>Thank you.<br/><br/>Regards,<br/><b>myMTN</b>"].join("");
   OTPMessageBody = [OTPMessageBody, "</html>"].join("");

		var options = {
				method: 'GET',
				uri:  config.EMAIL_PLGUN_URL + "&transID=" + req.tid + "&username=" + eMailId + "&to=" + supp_id + "&subject=" + encodeURIComponent(subject) + "&body=" + encodeURIComponent(OTPMessageBody) + "&msisdn=" + msisdn + "&from=" + eMailId + "&cc=" + ccmailid,
		                json: true
                      }
		cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
		var url =  config.EMAIL_PLGUN_URL + "&transID=" + req.tid + "&username=" + eMailId + "&to=" + supp_id + "&subject=" + encodeURIComponent(subject) + "&body=" + encodeURIComponent(OTPMessageBody) + "&msisdn=" + msisdn + "&from=" + eMailId + "&cc=" + ccmailid

		http.sendJSONGetRequest(url, null, function(err, response){
			return res.status(200).json(response);
		});
		logger.writeLog('debug','generateOTPviaSMS'+ options.uri);
	}
	else
	{
		//API call for Sending SMS for the OTP generated
		var options = {
				method: 'GET',
				uri:  config.PLUGIN_URL+ "&transID=" + req.tid + "&msisdn="+req.body.msisdn + "&category=service&method=sendSMS&msgbody="+mailbody, 
				json: true
		}
		logger.writeLog('debug','generateOTPviaEmail'+ options.uri);
	}
	logger.writeLog('debug','sendSMS'+ options.uri);
	cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
	request.send(req, res, cdrParams, options);

}




