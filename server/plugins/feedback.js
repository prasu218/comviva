
var config = require('../config');
var request = require('../utils/request');
var http = require('../utils/http');
var sha256 = require('js-sha256');

exports.feedbackdetailsAPI = function(req, res, cdrParams) {
  const msisdn = req.body.msisdn;
  const eMailId = req.body.eMailId;
  const supp_id = req.body.supp_id;
  const ccmailid = req.body.ccmailid;
  var subject = decodeURIComponent(req.body.subject);
  var mailbody = decodeURIComponent(req.body.mailbody);
  cdrParams.activityCategory = "FEEDBACK";
  cdrParams.subCategory = "feedbackdetails";
  cdrParams.component = "feedbackdetails";
  cdrParams.offerCode = "NA";
  cdrParams.amount = "NA";
  const options = {
    method: 'GET',
    uri:  config.EMAIL_PLGUN_URL + "&transID=" + req.tid + "&username=" + eMailId + "&to=" + supp_id + "&subject=" + encodeURIComponent(subject) + "&body=" + encodeURIComponent(mailbody) + "&msisdn=" + msisdn + "&from=" + eMailId + "&cc=" + ccmailid
  }
  cdrParams.input = options.uri.substring(options.uri.indexOf("?")+1,options.uri.length);
  var url =  config.EMAIL_PLGUN_URL + "&transID=" + req.tid + "&username=" + eMailId + "&to=" + supp_id + "&subject=" + encodeURIComponent(subject) + "&body=" + encodeURIComponent(mailbody) + "&msisdn=" + msisdn + "&from=" + eMailId + "&cc=" + ccmailid
 	http.sendJSONGetRequest(url, null, function(err, response){
		return res.status(200).json(response);
	});

}
