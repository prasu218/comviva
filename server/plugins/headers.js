exports.getHeaders = (req, res, cdrParams) => {
    const headers = req.headers || null
    return res.status(200).json(headers)
}

exports.getHeaderMSISDN = (req, res, cdrParams) => {
    const headers = req.headers;
    if (headers != null) {
        let msisdn = headers['msisdn'];
        let imsi = headers['imsi'];
        if (msisdn != null && imsi != null) {
            const mtnNumber = imsi.substr(0,5);
            if (mtnNumber == "62130") {
                return res.status(200).json(headers);
            } else {
                return res.status(200).json(null);
            }
        } else {
            return res.status(200).json(null);
        }
    } else {        
        return res.status(200).json(null);
    }
}