const moment = require("moment");
require('winston-daily-rotate-file');
const winston = require("winston");
const expressWinston = require("express-winston");

var transport = new (winston.transports.DailyRotateFile)({
  filename: 'logCDR/CDR-Records-%DATE%.log',
  datePattern: 'YYYY-MM-DD-HH',
  zippedArchive: false,

});

// expressWinston.bodyBlacklist.push('body');
// expressWinston.requestWhitelist.push('body')

expressWinston.bodyBlacklist.push('headers', 'httpVersion', 'originalUrl');
expressWinston.requestWhitelist.push('url', 'method');

let ExpresswinLogger = expressWinston.logger({
  transports: [
    new winston.transports.Console({
      json: true,
      colorize: true
    }),
    transport

  ],
  meta: true, // optional: control whether you want to log the meta data about the request (default to true)
//  msg: "{ method:  {{req.method}} url: {{req.url}}, device: {{req.headers['user-agent']}} }", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
  expressFormat: true, // Use the default Express/morgan request formatting, with the same colors. Enabling this will override any msg and colorStatus if true. Will only output colors on transports with colorize set to true
  colorStatus: true, // Color the status code, using the Express/morgan color palette (default green, 3XX cyan, 4XX yellow, 5XX red). Will not be recognized if expressFormat is true
  ignoreRoute: function (req, res) { return false; }, // optional: allows to skip some log messages based on request and/or response
  requestWhitelist: ['url'],
  responseWhitelist: ['statusCode'],
  dynamicMeta: function(req, res) {
    return {
      time_stamp: moment().format('MMMM Do YYYY, h:mm:ss a'),
      // fullUrl: req.url ? "http://172.16.5.239:8300"+req.url : null,
      device: req.headers["user-agent"]? req.headers["user-agent"] : null,
  
  }
},
// blacklistedMetaFields: ['req.headers', 'req.httpVersion', 'req.originalUrl']

})




module.exports = ExpresswinLogger;