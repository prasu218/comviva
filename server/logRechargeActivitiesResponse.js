const cdrLogger = require("./logRechargeActivitiesData");
const ip = require("ip");
const moment = require("moment");
function logResponseBody(req, res, next) {



  let requesttimestamp = moment().format('DD-MM-YYYY HH:mm:ss:SSS ');
    var oldWrite = res.write,
        oldEnd = res.end;
  
    var chunks = [];
    
    const request_obj = req;

  //   console.log("Brossa na uer method o ", req.method);
  //   if(request_obj && request_obj.method =='GET' || request_obj.method == 'OPTIONS'){
   
  //  }
  
    res.write = function (chunk) {
      chunks.push(chunk);
  
      oldWrite.apply(res, arguments);
    };
  
    res.end = function (chunk) {
      if (chunk)
        chunks.push(chunk);

      var body = Buffer.concat(chunks).toString('utf8');
      var parsedBodye = body;
    try{  
      var parsedBody = JSON.parse(body);
    }
    catch(e){
     // console.log(e);
    }

    console.log("This is parsed body",parsedBody)
   
     let pipe = " | ";
     let logObject = "";
// msisdn
if(request_obj.body && request_obj.body['cust_id'] ){
  if(request_obj.body['cust_id'].startsWith('234')){
    logObject +=  request_obj.body['cust_id'] + pipe;
  }else{
    logObject +=  "234"+request_obj.body['cust_id']+ pipe;
  } 
}
else if(request_obj.body && request_obj.body['msisdn'] ){
  //console.log("Misdn always disturbing ", request_obj.body['msisdn']);
  if(request_obj.body['msisdn'].startsWith('234')){
    // logObject +=  "0"+request_obj.body['msisdn'].slice(3, 13)+ pipe;
    logObject +=  request_obj.body['msisdn']+ pipe;
  }else{
    logObject +=  "234"+request_obj.body['msisdn']+ pipe;
  } 
}
else if(request_obj.body && request_obj.body['mtnepg_cust_id'] ){
  if(request_obj.body['mtnepg_cust_id'].startsWith('234')){
    logObject +=  request_obj.body['mtnepg_cust_id']+ pipe;
  }else{
    logObject +=  "234"+request_obj.body['mtnepg_cust_id']+ pipe;
  } 
}
else{
  logObject +=  pipe;
}
//url
if(request_obj && request_obj.path){
  logObject += req.path +pipe;  
}else{
  logObject += pipe
}

//time stamp
logObject += moment().format('DD-MM-YYYY HH:mm:ss ')+ pipe;
//transaction ref transID
 if (parsedBody && parsedBody.transID){
  logObject += parsedBody.transID + pipe;
}
else if(parsedBody && parsedBody.transaction_id){
  logObject += parsedBody.transaction_id + pipe;
}else{
  logObject += "" +pipe;
}
//processing number
if(parsedBody && parsedBody.ProcessingNumber){
  logObject += parsedBody.ProcessingNumber +pipe;
}else{
  logObject += pipe
}
//action borrow bundle
if(request_obj && request_obj.path ){
  let action =  request_obj.path.replace("/", "");
  logObject += action +pipe
}
// else if(request_obj && request_obj.path.indexOf("buy") > -1 ){
//   logObject += "bundle"+pipe
// }
else{
  logObject += pipe
}
// if(request_obj && request_obj.path.indexOf("borrow") > -1  || request_obj && request_obj.path.indexOf("Borrow") > -1){
//   logObject += "borrow"+pipe
// }else if(request_obj && request_obj.path.indexOf("buy") > -1 ){
//   logObject += "bundle"+pipe
// }else{
//   logObject += pipe
// }

//usertype - self, others app/api/buyforSelfCIS 
if(request_obj && request_obj.path == '/buyforSelfCIS' ){
  logObject +=  'self'+ pipe;
}
else if(request_obj && request_obj.path == '/app/api/buyforSelfCIS' ){
  logObject +=  'self'+ pipe;
}else if(request_obj && request_obj.path == '/buyforothersCIS' ) {
  logObject +=  "others"+ pipe;
}
else if(request_obj && request_obj.path == 'app/api/buyforothersCIS' ) {
  logObject +=  "others"+ pipe;
}
else if(request_obj && request_obj.path == 'app/api/debitcarddataplanself'){
  logObject +=  'self'+ pipe;
}
else if(request_obj.path == 'app/api/debitcarddataplanothers' ) {
  logObject +=  "others"+ pipe;
}
else{
  logObject += pipe
}
//airtime or card
// if(request_obj && request_obj.path.indexOf("debit") > -1 ){
//   logObject += "debit card"+pipe
// }else if(request_obj && request_obj.path.indexOf("buyfor") > -1 ){
//   logObject += "airtime"+pipe
// }
if(request_obj && request_obj.path == "/cardType" ){
  logObject += "card"+pipe;
}else if(request_obj && request_obj.path == "/airtimeType" ){
  logObject += "airtime"+pipe;
}
else{
  logObject += pipe
}

//request time stamp
logObject += requesttimestamp + pipe;

//response timestamp
logObject += moment().format('DD-MM-YYYY HH:mm:ss:SSS ') + pipe;

//requeststatus RequestId RespDescription
if(parsedBody && parsedBody.Status){
  logObject += parsedBody.Status+ pipe;
  }
// if(parsedBody && parsedBody.RespDescription){
//   logObject += parsedBody.RespDescription+ pipe;
// }
// else if( parsedBody && parsedBody.ResponseDescription){
//   logObject += parsedBody.ResponseDescription+ pipe;
//   }
else{
    logObject += pipe;
  }

//failure reason
if(parsedBody && parsedBody.Status && parsedBody.Status == 'FAILURE' && parsedBody.RespDescription){
  logObject += parsedBody.RespDescription+ pipe;
}else{
  logObject += pipe
}

//amount
if( request_obj && request_obj.body && request_obj.body.transaction_amount){
  logObject += request_obj.body.transaction_amount;
}
else if( request_obj && request_obj.body && request_obj.body.price){
  logObject += request_obj.body.price+pipe;
}
else{
  logObject += pipe
}
//bundle category
if(request_obj && request_obj.body && request_obj.body.type &&  request_obj.body.subtype){
  logObject += request_obj.body.type +pipe
}
else{
  logObject +=pipe;
}
//ip address
if(ip.address()){
  logObject += ip.address()
}
else{
  logObject += "";
}

//let bannedpaths = ['assets', 'icons','fonts', 'createSessionStatus', '.svg']
  
//if(req.path == "/login" || req.path == "/linkedaccountlist"){}
// if (acceptableMethods.indexOf(data.method) > -1) {}
console.log("Brossa na uer method o ", req.method);
//request_obj && request_obj.method =='GET' || request_obj && request_obj.method == 'OPTIONS'
if(request_obj && request_obj.method =='POST'){

  if(req.path == "/getSessionStatus" || req.path == "/generateotp" || req.path == "/validateOTP" || req.path.includes('assets') ||  req.path.includes('icons') || req.path == '/angularCDR' || req.path.includes('styles') || req.path.includes('createSessionStatus') || req.path.includes('createSessionStatus') || req.path.includes('svg') || req.path.includes('images') || req.path.includes('/getnotificationtab') || req.path.includes('/deleteSessionStatus')  ){
  }
  else{
    cdrLogger.info(logObject);
  }
}

  

  


      //console.log(req.path, body);
  
      oldEnd.apply(res, arguments);
    };
  
    next();
  }
 module.exports = logResponseBody;  