const mongoose = require('mongoose')

const airtimeSchema = new mongoose.Schema({
  trans_id: {
    type: String,    
  },
  pg_trans_id: {
    type: String,
    required: true,
  }, 
  cust_id: {
    type: String,
    required: true,
  },
  cust_mobile: {
    type: Number,
    required: true,
  },  
  transaction_amount: {
    type: Number,
    required: true,
  },
  transaction_status: {
    type: String,
    required: true,
  },
}, {
    timestamps: true
  });
  
 const Airtime = mongoose.model('Airtime', airtimeSchema)

module.exports = Airtime