const mongoose = require('mongoose')

const sessionSchema = new mongoose.Schema({  
  cust_id: {
    type: String,
    required: true,
  },
  session_id: {
    type: String,
    required: true,
  }
}, {
    timestamps: true
  });
  
const Session = mongoose.model('Session', sessionSchema)

module.exports = Session
