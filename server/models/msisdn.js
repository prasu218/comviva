const mongoose = require('mongoose')

//whitelist MSISDN
const msisdnSchema = new mongoose.Schema({  
  cust_msisdn: {
    type: String,
    required: true
  }
}, {
    timestamps: true
  });
  
 const Msisdn = mongoose.model('Msisdn', msisdnSchema)

module.exports = Msisdn
