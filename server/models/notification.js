const mongoose = require('mongoose')

const notificationSchema = new mongoose.Schema({
 
    cust_id: {
    type: String, 
    require:true   
     },
    flag: {
    type: String,   
    require:true   
     },
  
}, {
    timestamps: true
  });
  
 const NotificationTab = mongoose.model('NotificationTab', notificationSchema)

module.exports = NotificationTab
