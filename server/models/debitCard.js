const mongoose = require('mongoose')

const debitSchema = new mongoose.Schema({
    pg_trans_id: {
    type: String,
    required: true,
  }, 
  cust_id: {
    type: String,    
  },
  cust_name: {
    type: String,    
  },
  cust_mobile: {
    type: String,    
  },
  cust_email: {
    type: String,    
  },
  transaction_amount: {
    type: String,    
  },
  transaction_status: {
    type: String,
    required: true,
  },
  transaction_type: {
    type: String,
    required: true,
  },
}, {
    timestamps: true
  });
  
 const DebitCardPayment = mongoose.model('DebitCardPayment', debitSchema)

module.exports = DebitCardPayment
