var express = require('express');
var router = express.Router();
require('./db/mongoose')
var logger = require('./utils/logger');
var cdr = require('./utils/cdr');
var dateFormats = require('./utils/dateformats');
var profile = require('./plugins/profile');
var puk = require('./plugins/puk');
var profilepic = require('./plugins/profilepic');
var complaints = require('./plugins/complaints');
var complaintslist = require('./plugins/complaintslist');
var complaintscategory = require('./plugins/complaintscategory');
var complaintssubcategory = require('./plugins/complaintssubcategory');
var complaintsubmit = require('./plugins/complaintsubmit');
var otp = require('./plugins/otp');
var balance = require('./plugins/balance');
var bundle = require('./plugins/bundle');
var loginrcg = require('./plugins/loginrcg');
var checkPlanandvalues = require('./plugins/checkPlanandvalues');
var viewbilldetails1 = require('./plugins/viewbilldetails1');
var rcgtopup = require('./plugins/rcgtopup');
var debitcard = require('./plugins/debitcard');
var buybundles = require('./plugins/buybundles');
var rcgtopup = require('./plugins/rcgtopup');
var vas_Subscription = require('./plugins/subscription');
var subscription = require('./plugins/subscription');
var un_SubscribeVas = require('./plugins/subscription');
var RechargeDetail = require('./plugins/rechargestatus');
var feedback = require('./plugins/feedback');
var notification = require('./plugins/notification');
var rcgborrow = require('./plugins/rcgborrow');

var debitcarddataplanself = require('./plugins/debitcarddataplanself');
var debitcarddataplanothers = require('./plugins/debitcarddataplanothers');

var shareairtime = require('./plugins/shareairtime');
var smedatashare = require('./plugins/buybundles');

var data = require('./plugins/buybundles');
var payment = require('./plugins/payment');

var help = require('./plugins/help');

var location = require('./plugins/location');

var linkaccount = require('./plugins/linkaccount');
var validateaccount = require('./plugins/validateaccount');
var linkedaccountlist = require('./plugins/linkedaccountlist');

var unlinkaccount = require('./plugins/unlinkaccount');
var getcheklist = require('./plugins/tarrifplans');
var accountHistory = require('./plugins/accountHistory');

var migratePlan = require('./plugins/tarrifplans');
var headers = require('./plugins/profile');
var validateMsisdn = require('./plugins/validateMsisdn');

var success = require('./plugins/success');
/******* Begin of Header Plugin - Kanmi ********/
var HeaderDetails = require('./plugins/headers');
/******* Begin of Header Plugin - Kanmi ********/

const verifyToken = require('./verifyToken');
const config = require("./config");
//JWT Fixes
const switchaccounttoken  = require("./switchaccounttoken");
const switchmaintoken  = require("./switchmaintoken");

var verifyaccountswitch = require('./plugins/verifyaccountswitch');
var verifymainswitch = require('./plugins/verifymainswitch');
var linkedswitchmiddleware = require("./linkedswitchMiddleware");
var linkaccountswitchlist = require('./plugins/linkaccountswitchlist');


router.post('/getUserStatus', getUserStatus);
router.post('/validateMsisdn', verifyToken, getMsisdn);
router.post('/addMsisdn', verifyToken, addMsisdn);
router.post('/updateMsisdn', verifyToken, updateMsisdn);
router.post('/deleteMsisdn', verifyToken, deleteMsisdn);
router.post('/getHeaders', verifyToken, getHeaders);
router.post('/angularCDR', angularCDR);
router.post('/unlinkaccount', verifyToken, UnlinkAccount);
router.post('/airtime/success',GetPaymentDetails);
router.post('/buybundle/success',GetBundlePaymentDetails);
router.post('/location', verifyToken, getCurrentaddress);
router.post('/migrateTariffPlan', verifyToken, migrateTariffPlan);

router.post('/checkBorrowEligibility', verifyToken, checkBorrowEligibility);
router.post('/getBorrowAirTime', verifyToken, getBorrowAirTime);
router.post('/linkaccount', verifyToken, linkAccount);
router.post('/linkedaccountlist', verifyToken, getLinkedAccount);
router.post('/validateaccount', verifyToken, validateAccount);
router.post('/shareddata', verifyToken, smedata);

router.post('/datashare', verifyToken, smedatasharebundle);
router.post('/shareairtime', verifyToken, getShareAirtime);
router.post('/profileImg', verifyToken, getProfileImg);
router.post('/addprofileImg', verifyToken, createProfileImg);
router.post('/getRechargeDetails', verifyToken, getRechargeDetails);
router.post('/feedback', verifyToken, feedbackdetailsAPI);
router.post('/notification', verifyToken, notificationAPI);
router.post('/getBundleCatCIS', verifyToken, getBundleCatCIS);
router.post('/getAllBundleProductListCIS', verifyToken, getAllBundleProductListCIS);
router.post('/getBundleCatCISCountry', verifyToken, getBundleCatCISCountry);
router.post('/getBundleCatCISSME', verifyToken, getBundleCatCISSME);
router.post('/getBundleCatCISCountry', verifyToken, getBundleCatCISCountry);
router.post('/debitcard', verifyToken, getDebitcardDetails);
router.post('/debitcardOthers', verifyToken, getDebitcardOthersDetails)
router.post('/profile', verifyToken, getProfileDetails);
router.post('/getpicprofile', verifyToken, getpicprofile);

router.post('/profilepic', verifyToken, getfetchingprofile);

router.post('/complaints', verifyToken, getcomplaintsDetails);
router.post('/complaintscategory', verifyToken, getcategoryDetails);
router.post('/complaintssubcategory', verifyToken, getsubcategoryDetails);
router.post('/complaintslist', verifyToken, listComplaintsDetails);
router.post('/complaintsubmit', verifyToken, complaintSubmitFormDetails);
router.post('/generateotp', generateOTP);
router.post('/sendsms', verifyToken, sendSms);

router.post('/validateOTP', validateOTP);
router.post('/balance', verifyToken, getBalance);
router.post('/getCreditBalance', verifyToken, getCreditBalance);
router.post('/checkPlanandvalues', verifyToken, getcheckPlanandvalues);
router.post('/bundle', verifyToken, getBundleBalanceAPI);
router.post('/loginrcg', verifyToken, getLoginRcgDetails);
router.post('/getBundleCatCIS', getBundleCatCIS);
router.post('/getRoamingRateCountryDetails', verifyToken, getRoamingRateCountryDetails);
router.post('/getRoamingdata', verifyToken, getRoamingdata);
router.post('/getRoamingvoice', verifyToken, getRoamingvoice);
router.post('/getRoamingdataandvoice', verifyToken, getRoamingdataandvoice);

router.post('/buyforothersCIS', verifyToken, buyforothersCIS);
router.post('/buyforSelfCIS', verifyToken, buyforSelfCIS);
router.post('/rcgtopup', verifyToken, getTopupDetails);
router.post('/rcgtopup', verifyToken, getTopupDetailsOthers);
router.post('/viewbilldetails1', verifyToken, viewbilldetailsAPI1);
router.post('/getRechargeDetails', verifyToken, getRechargeDetails);
router.post('/subscription', verifyToken, mySubscriptionList);

router.post('/Subscriptionnew', verifyToken, vasSubscription);

router.post('/unSubscribeVas', verifyToken, unSubscribeVas);

router.post('/debitcarddataplanself', verifyToken, paymentgatewaydataplanself);
router.post('/debitcarddataplanothers', verifyToken, paymentgatewaydataplanothers);
router.post('/activateBundle', verifyToken, activateBundle);
router.post('/savenotificationtab', verifyToken, savenotificationtab);
router.post('/getnotificationtab', verifyToken, getnotificationtab);
router.post('/saveDebitCardPaymentStatus', verifyToken, saveDebitCardPaymentStatus);
router.post('/getDebitCardPaymentStatuses' , verifyToken, getDebitCardPaymentStatuses);
router.post('/saveDebitCardPaymentStatus', verifyToken, saveDebitCardPaymentStatus);
router.post('/getAirtimePaymentStatuses', verifyToken, getAirtimePaymentStatuses);
router.post('/getEligibility', verifyToken, eligibility);

router.post('/getAllState', verifyToken, getAllState);
router.post('/getAllLGAs', verifyToken, getAllLGAs);
router.post('/getServiceProvinceCenters', verifyToken, getServiceProvinceCenters);
router.post('/help', verifyToken, getCurrentaddress);


router.post('/getcheckPlan', verifyToken, getcheckPlan);
router.post('/accountHistory', verifyToken, accountHistoryAsPerType);


router.post('/getfarwphone', verifyToken, getfarwphone);
router.post('/getfarwvoicemail', verifyToken, getfarwvoicemail);
router.post('/getdeactivatecallfarw', verifyToken, getdeactivatecallfarw);
router.post('/getresetbarr', verifyToken, getresetbarr);
router.post('/getcallbarring', verifyToken, getcallbarring);
router.post('/getcallunbarring', verifyToken, getcallunbarring);

router.post('/createSessionStatus', verifyToken, createSessionStatus);
router.post('/updateSessionStatus', verifyToken, updateSessionStatus);
router.post('/getSessionStatus', verifyToken, getSessionStatus);
router.post('/deleteSessionStatus', verifyToken, deleteSessionStatus);
router.post('/logoutSessionStatus', verifyToken, logoutSessionStatus);

router.post('/puk', getPUKDetails);

/******* Begin of Header Router - Kanmi ********/
router.post('/getheaders', getHeaders);
router.post('/getmsisdn', getHeaderMSISDN);
/******* End of Header Router ********/

router.post("/airtimeType", airtimeType );
router.post("/cardType", cardType);

//JWT Fixes
router.post('/verifyaccountswitch',switchaccounttoken , verifyaccountswitched);
router.post('/verifymainswitch',switchmaintoken , verifymainswitched);
router.post('/linkedaccountswitchlist',linkedswitchmiddleware , getLinkedSwitchAccount);

//check valid profile 
router.post('/validateprofile', validateProfileDetails);

router.post('/getDebitCardTransStatus', getDebitCardTransStatus);

var aCDRParams = {};
aCDRParams.transStartTime = dateFormats.ddMMyyyyhhmmss(0);

/******* Begin of Header Method - Kanmi ********/
function getHeaders(req, res, next) {
	console.log("inside header enrichment api");
	HeaderDetails.getHeaders(req, res, aCDRParams);
}
function getHeaderMSISDN(req, res, next) {
	console.log("inside get msisdn header api");
	HeaderDetails.getHeaderMSISDN(req,res, aCDRParams);
}
/******* End of Header Method - Kanmi ********/

function getUserStatus(req, res, next) {
	payment.getUserStatus(req, res, aCDRParams);
}

function migrateTariffPlan(req, res, next) {
	migratePlan.migrateTariffPlan(req, res, aCDRParams)
}
function UnlinkAccount(req, res, aCDRParams) {
	unlinkaccount.UnlinkAccount(req, res, aCDRParams);
}
function getpicprofile(req, res, next) {
	profilepic.getpicprofile(req, res, aCDRParams);
}

function getDebitCardTransStatus(req, res, next) {
	debitcard.getDebitCardTransStatus(req, res, aCDRParams);
}

function GetPaymentDetails(req, res, aCDRParams) {
	success.GetPaymentDetails(req, res, aCDRParams);
}
function GetBundlePaymentDetails(req,res,aCDRParams){
	console.log("GetBundlePaymentDetails:::::::::::::::::::::::");
	success.GetBundlePaymentDetails(req, res, aCDRParams);
}

function getfarwphone(req, res, next) {
	profile.getfarwphone(req, res, aCDRParams);
}
function getfarwvoicemail(req, res, next) {
	profile.getfarwvoicemail(req, res, aCDRParams);
}
function getdeactivatecallfarw(req, res, next) {
	profile.getdeactivatecallfarw(req, res, aCDRParams);
}

function getresetbarr(req, res, next) {
	profile.getresetbarr(req, res, aCDRParams);
}
function getcallbarring(req, res, next) {
	profile.getcallbarring(req, res, aCDRParams);
}
function getcallunbarring(req, res, next) {
	profile.getcallunbarring(req, res, aCDRParams);
}

function getPUKDetails(req, res, next) {
	puk.getPUKDetails(req, res, aCDRParams);
}

function getfetchingprofile(req, res, next) {
	profilepic.getfetchingprofile(req, res, aCDRParams);
	//res.send('getfetchingprofile ');
}

function getProfileImg(req, res, next) {
	profilepic.getProfileImg(req, res, aCDRParams);
}

function createProfileImg(req, res, next) {
	profilepic.createProfileImg(req, res, aCDRParams);
}

function getLinkedAccount(req, res, next) {
	linkedaccountlist.getLinkAccountlist(req, res, aCDRParams)
}
function checkBorrowEligibility(req, res, next) {
	rcgborrow.checkBorrowEligibility(req, res, aCDRParams)

}
function getBorrowAirTime(req, res, next) {
	rcgborrow.getBorrowAirTime(req, res, aCDRParams)

}
function linkAccount(req, res, next) {
	linkaccount.getLinkAccount(req, res, aCDRParams);
}
function validateAccount(req, res, aCDRParams) {
	validateaccount.validateAccount(req, res, aCDRParams);
}
function getLinkedAccount(req, res, next) {
	linkedaccountlist.getLinkAccountlist(req, res, aCDRParams)
}
function feedbackdetailsAPI(req, res, next) {
	feedback.feedbackdetailsAPI(req, res, aCDRParams);
}

function notificationAPI(req, res, next) {
	notification.notificationAPI(req, res, aCDRParams);
}

function accountHistoryAsPerType(req, res, next) {
	accountHistory.getAccountHistory(req, res, aCDRParams);
}

function addMsisdn(req, res, next) {
	validateMsisdn.addMsisdn(req, res, aCDRParams);
}
function getMsisdn(req, res, next) {
	validateMsisdn.validateMSISDN(req, res, aCDRParams);
}
function updateMsisdn(req, res, next) {
	validateMsisdn.updateMSISDN(req, res, aCDRParams);
}
function deleteMsisdn(req, res, next) {
	validateMsisdn.deleteMSISDN(req, res, aCDRParams);
}
function getAllState(req, res, next) {
	help.getAllState(req, res, aCDRParams);
}

function getAllLGAs(req, res, next) {
	help.getAllLGAs(req, res, aCDRParams);
}

function getServiceProvinceCenters(req, res, next) {
	help.getServiceProvinceCenters(req, res, aCDRParams);
}

function getCurrentaddress(req, res, next) {
	location.getCurrentaddress(req, res, aCDRParams);
}


function saveDebitCardPaymentStatus(req, res, next) {
	payment.saveDebitCardPaymentStatus(req, res, aCDRParams);
}
function savenotificationtab(req, res, next) {
	payment.savenotificationtab(req, res, aCDRParams);
}
function getnotificationtab(req, res, next) {
	payment.getnotificationtab(req, res, aCDRParams);
}

function getDebitCardPaymentStatuses(req, res, next) {
	payment.getDebitCardPaymentStatuses(req, res, aCDRParams);
}

function saveAirtimePaymentStatus(req, res, next) {
	payment.saveAirtimePaymentStatus(req, res, aCDRParams);
}

function getAirtimePaymentStatuses(req, res, next) {
	payment.getAirtimePaymentStatuses(req, res, aCDRParams);
}


function activateBundle(req, res, next) {
	payment.activateBundle(req, res, aCDRParams);
}

function eligibility(req, res, next) {
	payment.eligibilityCheck(req, res, aCDRParams);
}

function paymentgatewaydataplanself(req, res, next) {
	debitcarddataplanself.paymentgatewaydataplanself(req, res, aCDRParams);
}
function paymentgatewaydataplanothers(req, res, next) {
	debitcarddataplanothers.paymentgatewaydataplanothers(req, res, aCDRParams);
}


function getRechargeDetails(req, res, next) {
	RechargeDetail.getRechargeDetails(req, res, aCDRParams);
}

function getShareAirtime(req, res, next) {
	shareairtime.getShareAirtime(req, res, aCDRParams);

}


function getDebitcardDetails(req, res, next) {
	debitcard.getDebitcardDetails(req, res, aCDRParams)

}

function getAllBundleProductListCIS(req, res, next) {
	console.log("getAllBundleProductListCIS-----------");
	 buybundles.getAllBundleProductListCIS(req, res, aCDRParams);
 //	res.send('Rsponse');
 }

function getBundleCatCIS(req, res, next) {
	buybundles.getBundleCatCIS(req, res, aCDRParams);
	//	res.send('Rsponse');
}

function getBundleCatCISSME(req, res, next) {
	buybundles.getBundleCatCISSME(req, res, aCDRParams);
	//	res.send('Rsponse');
}

function getBundleCatCISCountry(req, res, next) {
	buybundles.getBundleCatCISCountry(req, res, aCDRParams);
	//	res.send('Rsponse');
}


function getTopupDetails(req, res, next) {
	rcgtopup.getTopupDetails(req, res, aCDRParams);
}
function getTopupDetailsOthers(req, res, next) {
	rcgtopup.getTopupDetailsOthers(req, res, aCDRParams);
}


function buyforSelfCIS(req, res, next) {
	buybundles.buyforSelfCIS(req, res, aCDRParams);
}

function getRoamingRateCountryDetails(req, res, next) {
	buybundles.getRoamingRateCountryDetails(req, res, aCDRParams);
}

function getRoamingdata(req, res, next) {
	buybundles.getRoamingdata(req, res, aCDRParams);
}

function getRoamingvoice(req, res, next) {
	buybundles.getRoamingvoice(req, res, aCDRParams);
}

function getRoamingdataandvoice(req, res, next) {
	buybundles.getRoamingdataandvoice(req, res, aCDRParams);
}

function buyforothersCIS(req, res, next) {
	buybundles.buyforothersCIS(req, res, aCDRParams);
}
function getBundleCatCISCountry(req, res, next) {
	buybundles.getBundleCatCISCountry(req, res, aCDRParams);
}


function getDebitcardOthersDetails(req, res, next) {
	debitcard.getDebitcardOthersDetails(req, res, aCDRParams)
}

function getProfileDetails(req, res, next) {
	profile.getProfileDetails(req, res, aCDRParams);
}
function validateProfileDetails(req, res, next) {
	profile.validateProfileDetails(req, res, aCDRParams);
}

function getcomplaintsDetails(req, res, next) {
	complaints.getcomplaintsDetails(req, res, aCDRParams);
}

function getcategoryDetails(req, res, next) {
	complaintscategory.getcategoryDetails(req, res, aCDRParams);
}

function listComplaintsDetails(req, res, next) {
	complaintslist.listComplaintsDetails(req, res, aCDRParams);
}

function getsubcategoryDetails(req, res, next) {
	complaintssubcategory.getsubcategoryDetails(req, res, aCDRParams);
}

function complaintSubmitFormDetails(req, res, next) {
	complaintsubmit.complaintSubmitFormDetails(req, res, aCDRParams);
}

function generateOTP(req, res, next) {
	otp.generateOTP(req, res, aCDRParams);
}

function sendSms(req, res, next) {
	otp.sendSms(req, res, aCDRParams);
}



function getLoginRcgDetails(req, res, next) {
	loginrcg.getLoginRcgDetails(req, res, aCDRParams);
}
function getcheckPlanandvalues(req, res, next) {
	checkPlanandvalues.getcheckPlanandvalues(req, res, aCDRParams);
}

function validateOTP(req, res, next) {
	otp.validateOTP(req, res, aCDRParams);
}

function resendOTP(req, res, next) {
	login.resendOTP(req, res, aCDRParams);
}
function getBalance(req, res, next) {
	balance.getBalance(req, res, aCDRParams);
}
function getCreditBalance(req, res, next) {
	balance.getCreditBalances(req, res, aCDRParams);
}
function getBundleBalanceAPI(req, res, next) {
	bundle.getBundleBalanceAPI(req, res, aCDRParams);
}
function viewbilldetailsAPI1(req, res, next) {
	viewbilldetails1.viewbilldetailsAPI1(req, res, aCDRParams);
}

function getTopupDetails(req, res, next) {
	rcgtopup.getTopupDetails(req, res, aCDRParams);
}
function getTopupDetailsOthers(req, res, next) {
	rcgtopup.getTopupDetailsOthers(req, res, aCDRParams);
}
function getRechargeDetails(req, res, next) {
	RechargeDetail.getRechargeDetails(req, res, aCDRParams);
}

function mySubscriptionList(req, res, next) {
	subscription.mySubscriptionList(req, res, aCDRParams);
}
function vasSubscription(req, res, next) {
	vas_Subscription.vasSubscription(req, res, aCDRParams);
}

function unSubscribeVas(req, res, next) {
	un_SubscribeVas.unSubscribeVas(req, res, aCDRParams);
}
function smedatasharebundle(req, res, next) {
	smedatashare.smedatasharebundle(req, res, aCDRParams);
}
function smedata(req, res, next) {
	data.smedata(req, res, aCDRParams);
}


function getcheckPlan(req, res, next) {
	getcheklist.getcheckPlan(req, res, aCDRParams);
}


function createSessionStatus(req, res, next) {
	payment.createSessionStatus(req, res, aCDRParams);
}
function updateSessionStatus(req, res, next) {
	payment.updateSessionStatus(req, res, aCDRParams);
}
function getSessionStatus(req, res, next) {
	payment.getSessionStatus(req, res, aCDRParams);
}
function deleteSessionStatus(req, res, next) {
	payment.deleteSessionStatus(req, res, aCDRParams);
}
function logoutSessionStatus(req, res, next) {
	payment.logoutSessionStatus(req, res, aCDRParams);
}

function angularCDR(req, res, next) {
	req.body.agentName = req.ssoheader_agentName;
	req.body.originIP = req.headers.origin;
	cdr.writeAngularCDR(req, res, req.body);
}

function airtimeType(req,res, next){
	return res.status(200).json({"type": "airtime"});
}
function cardType(req, res, next){
	return res.status(200).json({"type": "card"});
}
module.exports = router;

function verifyAPIAuth(req, res, next) {
	if (req.session && req.session.isLoggedIn) {
		next();
	} else {
		var err = new Error("Unauthorized");
		err.status = 401;
		next(err);
	}
}

function getLinkedSwitchAccount(req, res, next) {
	linkaccountswitchlist.getLinkAccountlist(req, res, aCDRParams)
}

function verifyaccountswitched(req, res, next) {
	verifyaccountswitch.getLinkAccountlist(req, res, aCDRParams)
}

function verifymainswitched(req, res, next) {
	verifymainswitch.getLinkAccountlist(req, res, aCDRParams)
}

