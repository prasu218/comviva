const cdrLogger = require("./logData");
const ip = require("ip");
const moment = require("moment");
function logResponseBody(req, res, next) {
    var oldWrite = res.write,
        oldEnd = res.end;
  
    var chunks = [];
    
    const request_obj = req;
  
    res.write = function (chunk) {
      chunks.push(chunk);
  
      oldWrite.apply(res, arguments);
    };
  
    res.end = function (chunk) {
      if (chunk)
        chunks.push(chunk);
  
      var body = Buffer.concat(chunks).toString('utf8');
      var parsedBody = body;
    try{  
      var parsedBody = JSON.parse(body);
    }
    catch(e){
     // console.log(e);
    }

    console.log("This is parsed body",parsedBody)
   
     let pipe = " | ";
     let logObject = "";
if(request_obj.body && request_obj.body['cust_id'] ){
  if(request_obj.body['cust_id'].startsWith('234')){
    logObject +=  request_obj.body['cust_id'] + pipe;
  }else{
    logObject +=  "234"+request_obj.body['cust_id']+ pipe;
  } 
}
else if(request_obj.body && request_obj.body['msisdn'] ){
  //console.log("Misdn always disturbing ", request_obj.body['msisdn']);
  if(request_obj.body['msisdn'].startsWith('234')){
    // logObject +=  "0"+request_obj.body['msisdn'].slice(3, 13)+ pipe;
    logObject +=  request_obj.body['msisdn'] +pipe;
  }else{
    logObject +=  "234"+request_obj.body['msisdn']+ pipe;
  } 
}
else if(request_obj.body && request_obj.body['mtnepg_cust_id'] ){
  if(request_obj.body['mtnepg_cust_id'].startsWith('234')){
    logObject +=  request_obj.body['mtnepg_cust_id']+ pipe;
  }else{
    logObject +=  "234"+request_obj.body['mtnepg_cust_id']+ pipe;
  } 
}
else{
  logObject += "" + pipe;
}

if( res && res.statusCode){
logObject += res.statusCode+ pipe
}else{
  logObject += pipe;
}
logObject += moment().format('DD-MM-YYYY HH:mm:ss ')+ pipe;
//logObject += request_obj.headers["user-agent"] + pipe;
//device type
if(request_obj && request_obj.headers && request_obj.headers["user-agent"]){
  logObject += request_obj.headers["user-agent"] + pipe;
}
else{
  logObject += pipe;
}
logObject += req.path+ pipe;

//ip address
if(ip.address()){
  logObject += ip.address()
}
else{
  logObject += "";
}      
  //|| req.path !=('/deleteSessionStatus')  || req.path.includes('images')   || req.path !=('/getnotificationtab') 
 if(request_obj && request_obj.method =='POST' && req.path != "/getSessionStatus" && req.path != "/angularCDR"  ){
      cdrLogger.info(logObject);
}
      //console.log(req.path, body);
  
      oldEnd.apply(res, arguments);
    };
  
    next();
  }
 module.exports = logResponseBody;  