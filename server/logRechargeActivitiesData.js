var winston = require('winston');
require('winston-daily-rotate-file');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;

const myFormat = printf(({ level, message, label, timestamp }) => {
  return ` ${label} ${message}`;
});



var transport = new (winston.transports.DailyRotateFile)({
    filename: 'logs/recharge-%DATE%.log',
    datePattern: 'YYYY-MM-DD_HH',
    zippedArchive: false,    
  });

transport.on('rotate', function(oldFilename, newFilename) {
  // do something fun
});

var logger = winston.createLogger({
  format: combine(
    label({ label: '' }),
    timestamp(),
    myFormat
  ),
  transports: [
    transport
  ],
  level:"info"
});

module.exports = logger;