var rp = require('request-promise');
//var logger = require('./logger');
var cdr = require('./cdr');
const jwt  = require('jsonwebtoken');
const config = require("../config");
const hidevalue = require("../utils/hidevalue");
exports.send = (req, res, cdrParams, options) => {
console.log("options in request.js");
console.log(options);
	cdrParams.agentName = req.ssoheader_agentName;
	cdrParams.originIP = req.headers.origin;
  rp(options)
  .then(response => {
    req.endtime = Date.now();
	cdrParams.output = response;
	cdrParams.statusMessage = "Success";
	cdrParams.statusCode = "200";
	
	cdr.writeMiddlewareCDR(req, cdrParams);
    return res.status(200).json(response);
  })
  .catch(err => {
    //console.log('RESPONSE ERROR FOR ', JSON.stringify(err));
	//console.log('RESPONSE FOR res.status ', res.status);
    req.endtime = Date.now();
	cdrParams.output = err;
	cdrParams.statusMessage = "Failure";
	cdrParams.statusCode = "502";
	cdr.writeMiddlewareCDR(req, cdrParams);
    return res.status(502).json({'StatusMessage': 'Bad Request'});
  });
}

exports.sendProfileValidity = (req, res, cdrParams, options) => {
    cdrParams.agentName = req.ssoheader_agentName;
    cdrParams.originIP = req.headers.origin;
    rp(options)
    .then(response => {
  
 
      req.endtime = Date.now();
    cdrParams.output = response;
    cdrParams.statusMessage = "Success";
    cdrParams.statusCode = "200";
    cdr.writeMiddlewareCDR(req, cdrParams);
    if(response.customer_Profile){
      return res.status(200).json(
                    {'status_code': 0, 
                      "StatusMessage": "profileValid"
                     });
    }else{
      return res.status(400).json({'StatusMessage': 'Bad Request'});
    }
    
    })
    .catch(err => {
      //console.log('RESPONSE ERROR FOR ', JSON.stringify(err));
    //console.log('RESPONSE FOR res.status ', res.status);
      req.endtime = Date.now();
    cdrParams.output = err;
    cdrParams.statusMessage = "Failure";
    cdrParams.statusCode = "400";
    cdr.writeMiddlewareCDR(req, cdrParams);
      return res.status(400).json({'StatusMessage': 'Bad Request'});
    });
  }

//send the list after switching validation
exports.sendswitchedList = (req, res, cdrParams, options) => {
  console.log("options in request.js");
  console.log(options);
    cdrParams.agentName = req.ssoheader_agentName;
    cdrParams.originIP = req.headers.origin;
    rp(options)
    .then(response => {
  
      console.log('RESPONSE SUCCESS FOR getting switched main numbers', response);
    console.log('RESPONSE FOR res.status ', res.status);

    //verify if the signed in msisdn is part of the list of linked numbers of main account

    let signedInNumber ="";
    if(req.body && req.body.msisdn){
        signedInNumber =  req.body.msisdn;
    }
 
    let numbsarr = [];
    if(response && response.results ){
      numbsarr = response.results.map(value => value.SecondaryNumber);
    }
console.log("Fresh array for result", numbsarr);
    console.log("Switch number to be compared", signedInNumber);
    console.log( "Get here switched here na ", numbsarr.indexOf(signedInNumber));

    if( numbsarr.indexOf(signedInNumber) != -1){
      return res.status(200).json(response);
    }else{
      return res.status(500).json({"error": "You dont have access"});
    }

      req.endtime = Date.now();
    cdrParams.output = response;
    cdrParams.statusMessage = "Success";
    cdrParams.statusCode = "200";
    
    cdr.writeMiddlewareCDR(req, cdrParams);
      
    })
    .catch(err => {
      console.log('RESPONSE ERROR FOR ', JSON.stringify(err));
    console.log('RESPONSE FOR res.status ', res.status);
      req.endtime = Date.now();
    cdrParams.output = err;
    cdrParams.statusMessage = "Failure";
    cdrParams.statusCode = "502";
    cdr.writeMiddlewareCDR(req, cdrParams);
      return res.status(502).json({'StatusMessage': 'Bad Request'});
    });
  }

//switch account verification
exports.sendswitch = (req, res, cdrParams, options) => {
  console.log("options in request.js");
  console.log(options);
    cdrParams.agentName = req.ssoheader_agentName;
    cdrParams.originIP = req.headers.origin;
    rp(options)
    .then(async response => {
  
      console.log('RESPONSE SUCCESS FOR switching  ', response);
    console.log('RESPONSE FOR res.status ', res.status);
      req.endtime = Date.now();

      let switchnumber ="";
      if(req.body && req.body.switchnumber){
          switchnumber =  req.body.switchnumber;
      }
   
      let numbsarr = []
  
      if(response && response.results ){
        numbsarr = response.results.map(value => value.SecondaryNumber);
      }
  console.log("Fresh array for result", numbsarr);
      console.log("Switch number to be compared", switchnumber);
      console.log( "Get here na ", numbsarr.indexOf(switchnumber));
                if( numbsarr.indexOf(switchnumber) != -1){

              const payload  =  {
                  msisdn: req.body['switchnumber']
                      }
          console.log("SWITCH payload to be signed", payload);
           let tokenSave;
           await jwt.sign(payload, config.tokhash, (err, token) =>{
             if(!err){
                 tokenSave = token;
                 console.log("This is the switch token sent to user ", tokenSave);
                 //return res.status(200).json(response);
                 //response.token = token;
              return res.status(200).json({token: token, StatusCode: 0});
             }else{
              return res.status(500).json({err: err, StatusCode: -1});
             }
           
           });
      
          }
          else{
                  res.status(401).json({
                      "error": "Unauthorised user, you dont have access to switch"
                  });
              }  
    })
    .catch(err => {
      console.log('RESPONSE ERROR FOR ', JSON.stringify(err));
    console.log('RESPONSE FOR res.status ', res.status);
      req.endtime = Date.now();
    cdrParams.output = err;
    cdrParams.statusMessage = "Failure";
    cdrParams.statusCode = "502";
    cdr.writeMiddlewareCDR(req, cdrParams);
      return res.status(502).json({'StatusMessage': 'Bad Request'});
    });
  }

//switch main account verification
exports.switchmain = (req, res, cdrParams, options) => {
  console.log("options in request.js");
  console.log(options);
    cdrParams.agentName = req.ssoheader_agentName;
    cdrParams.originIP = req.headers.origin;
    rp(options)
    .then(async response => {
  
      console.log('RESPONSE SUCCESS FOR switching  ', response);
    console.log('RESPONSE FOR res.status ', res.status);
      req.endtime = Date.now();

      let switchnumber ="";
      if(req.body && req.body.switchnumber){
          switchnumber =  req.body.switchnumber;
      }
   
      let numbsarr = []
  
      if(response && response.results ){
        numbsarr = response.results.map(value => value.SecondaryNumber);
      }
  console.log("Fresh array for result", numbsarr);
      console.log("Switch number to be compared", switchnumber);
      console.log( "Get here na ", numbsarr.indexOf(switchnumber));
                if( numbsarr.indexOf(switchnumber) != -1){

              const payload  =  {
                  msisdn: req.body['msisdn']
                      }
          console.log("SWITCH payload to be signed", payload);
           let tokenSave;
           await jwt.sign(payload, config.tokhash,{ expiresIn: config.logtime}, (err, token) =>{
             if(!err){
                 tokenSave = token;
                 console.log("This is the switch token sent to user ", tokenSave);
                 //return res.status(200).json(response);
                 //response.token = token;
              return res.status(200).json({token: token, StatusCode: 0});
             }else{
              return res.status(500).json({err: err, StatusCode: -1});
             }
           
           });
      
          }
          else{
                  res.status(401).json({
                      "error": "Unauthorised user, you dont have access to switch"
                  });
              }  
    })
    .catch(err => {
      console.log('RESPONSE ERROR FOR ', JSON.stringify(err));
    console.log('RESPONSE FOR res.status ', res.status);
      req.endtime = Date.now();
    cdrParams.output = err;
    cdrParams.statusMessage = "Failure";
    cdrParams.statusCode = "502";
    cdr.writeMiddlewareCDR(req, cdrParams);
      return res.status(502).json({'StatusMessage': 'Bad Request'});
    });
  }

exports.sendResponse = (req, res, cdrParams, options) => {
  console.log("options in request.js");
    cdrParams.agentName = req.ssoheader_agentName;
    cdrParams.originIP = req.headers.origin;
   return rp(options)
    .then(response => {
  
     // console.log('RESPONSE SUCCESS FOR stringify ', JSON.stringify(response));
     //console.log('RESPONSE SUCCESS FOR ', response);
    //console.log('RESPONSE FOR res.status ', res.status);
     // logger.writeLog('debug','RESPONSE FOR ' + JSON.stringify(response));
      req.endtime = Date.now();
  //	logger.writeActionCDR(req);
    cdrParams.output = response;
    cdrParams.statusMessage = "Success";
    cdrParams.statusCode = "200";
    
    cdr.writeMiddlewareCDR(req, cdrParams);

      return response;
    })
    .catch(err => {
    // console.log('RESPONSE ERROR FOR ', err);
    // console.log('RESPONSE FOR res.status ', res.status);
    // logger.writeLog('debug','RESPONSE FOR ' + JSON.stringify(err));
      req.endtime = Date.now();
  //	logger.writeActionCDR(req);
    cdrParams.output = err;
    cdrParams.statusMessage = "Failure";
    cdrParams.statusCode = "502";
    cdr.writeMiddlewareCDR(req, cdrParams);
    var resp = {'StatusMessage': 'Bad Request'}
     // return JSON.parse(resp);
    });
  }
