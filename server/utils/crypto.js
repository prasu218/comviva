var CryptoJS = require("crypto-js");
var Config = require('../config');
var he = require('he');

exports.encryptValue = function (value) {
    var encrytedData = CryptoJS.AES.encrypt(value, Config.CRYPTO_KEY).toString();
    return encrytedData;
}

exports.decryptValue = function (value) {
    const decodedValue = he.decode(value);
    var decrytedData = CryptoJS.AES.decrypt(decodedValue, Config.CRYPTO_KEY);
    var plaintext = decrytedData.toString(CryptoJS.enc.Utf8);
    return plaintext; 
}