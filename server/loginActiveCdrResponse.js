//const cdrLogger = require("./logData");
const ip = require("ip");
const cdrLogger = require("./logActiveUserData");
const moment = require("moment");
function logResponseBody(req, res, next) {
    var oldWrite = res.write,
        oldEnd = res.end;
  
    var chunks = [];
    
    const request_obj = req;

    // if(request_obj && request_obj.method =='GET' || request_obj.method == 'OPTIONS'){
    //   return;
    //  }
  
    res.write = function (chunk) {
      chunks.push(chunk);
  
      oldWrite.apply(res, arguments);
    };
  
    res.end = function (chunk) {
      if (chunk)
        chunks.push(chunk);
  
      var body = Buffer.concat(chunks).toString('utf8');
    //  var parsedBody = body;
    try{  
      var parsedBody = JSON.parse(body);
    }
    catch(e){
     // console.log(e);
    }

    console.log("This is parsed body",parsedBody)
   
     let pipe = " | ";
     let logObject = "";
// msisdn
if(request_obj.body && request_obj.body['cust_id'] ){
  if(request_obj.body['cust_id'].startsWith('234')){
    logObject +=  request_obj.body['cust_id'] + pipe;
  }else{
    logObject +=  "234"+request_obj.body['cust_id']+ pipe;
  } 
}
else if(request_obj.body && request_obj.body['msisdn']){
  if(request_obj.body['msisdn'].startsWith('234')){
    // logObject +=  "0"+request_obj.body['msisdn'].slice(3, 13)+ pipe;
    logObject +=  request_obj.body['msisdn'] +pipe;
  }else{
    logObject +=  "234"+request_obj.body['msisdn']+ pipe;
  } 
}
else if(request_obj.body && request_obj.body['mtnepg_cust_id'] ){
  if(request_obj.body['mtnepg_cust_id'].startsWith('234')){
    logObject +=  request_obj.body['mtnepg_cust_id']+ pipe;
  }else{
    logObject +=  "234"+request_obj.body['mtnepg_cust_id']+ pipe;
  } 
}
else{
  logObject += "" + pipe;
}
//time stamp
logObject += moment().format('DD-MM-YYYY HH:mm:ss ')+ pipe;
//is otp sent
if(parsedBody && parsedBody.otp){
  logObject += "true" + pipe
}else{
  logObject += pipe;
}
//is otp received
if(request_obj && request_obj.body['otp'] && request_obj.body['otp'].length > 0 ){
  logObject += true + pipe
}else if(request_obj && request_obj.body['otp'] && request_obj.body['otp'].length < 1 ){
  logObject += false + pipe
}
else{
  logObject += pipe;
}
//validate status
// console.log("Brossa na the otp valid status be this", parsedBody.Valid);
 if(parsedBody && parsedBody.Valid && parsedBody.status == 0){
  logObject += true +pipe;
}
 else if(parsedBody && parsedBody.status && parsedBody.Valid && parsedBody.status != 0){
  logObject += false +pipe;
}
else{
  logObject += pipe
}
//device type
if(request_obj && request_obj.headers && request_obj.headers["user-agent"]){
  logObject += request_obj.headers["user-agent"] + pipe;
}
else{
  logObject += pipe;
}

//logout time stamp
if(req.path == '/logoutSessionStatus'){
  logObject += moment().format(' HH:mm:ss ')+ pipe ;
}else{
  logObject += pipe;
}
//loginmode
// logObject += pipe;'
if(req.path == '/generateotp'){
  logObject += "manual" +pipe;
}
else if(req.path == "/getmsisdn" && parsedBody && parsedBody != null){
 // if(parsedBody && parsedBody != null){
    logObject += "auto" + pipe;
  // }else{
  //   logObject += "manual" +pipe;
  // }
}else{
  logObject += pipe
}
//new user /getUserStatus
if(req.path == "/getUserStatus"){
  if(parsedBody && parsedBody.newUser == true ){
    logObject += true + pipe;
  }else if(parsedBody && parsedBody.newUser == false ){
    logObject += false + pipe;
  }
  else{
    logObject += pipe;
  }
}
else{
  logObject += pipe;
}

//linkednumbers
if(parsedBody && parsedBody.results ){
  let linkednumb= [];
parsedBody.results.forEach(number => linkednumb += number.SecondaryNumber+":");
logObject += linkednumb+ pipe;    
}else{
  logObject += "" +pipe;
}
//ip address
if(ip.address()){
  logObject += ip.address()
}
else{
  logObject += "";
}
// if(parsedBody && parsedBody.results ){
//   let linkednum = [];
// parsedBody.results.map(number => linkednum.push( number.SecondaryNumber+";"));
// logObject += linkednum+pipe;    
// }else{
//   logObject += ""+ pipe;
// }

//logObject += req.path+ pipe; || req.path == "/getSessionStatus"     
  //let allowedPaths  = ["/login"]   
  if(request_obj && request_obj.method =='POST'){            
if(req.path == "/login" || req.path == "/getUserStatus"  || req.path == "/getmsisdn" || req.path == "/logoutSessionStatus" ||  req.path == "/linkedaccountlist" || req.path == "/generateotp" || req.path == "/validateOTP"){
      cdrLogger.info(logObject);
}
  }
      //console.log(req.path, body);
  
      oldEnd.apply(res, arguments);
    };
  
    next();
  }
 module.exports = logResponseBody;  