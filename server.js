const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const http = require('http');
const helmet = require('helmet');
const compression = require('compression');
const bodyParser = require('body-parser');
const validator = require('express-validator');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const MongoStore = require('connect-mongo')(session);
const db = require('./server/utils/database');
const cls = require('continuation-local-storage');
const logger = require('./server/utils/logger');
//const cdr = require('./server/utils/cdr');
const serverConfig = require('./server/config');
const config = require('./server/utils/config');
const api = require('./server/api');
const DebitCardPayment = require('./server/models/debitCard');
//const payment = require('./server/plugins/payment');

const app = express();
const logResponseBody = require("./server/logresponse")
const loginActiveCdrResponse = require('./server/loginActiveCdrResponse')
const rechargeActivitiesResponse = require('./server/logRechargeActivitiesResponse');

app.use(logResponseBody);
app.use(loginActiveCdrResponse);
app.use(rechargeActivitiesResponse);

app.use(helmet());
app.use(compression());
app.use(cookieParser());
cls.createNamespace(config.STORAGE.namespace);

app.use((req, res, next) => {
  var start = new Date();

  req.tid = "";
  console.log("process.env.NODEID---" + process.env.NODEID);
  if(process.env.NODEID){
    req.tid = [req.tid, process.env.NODEID].join("");
  }
  req.tid = [req.tid, Date.now()].join("");
  req.starttime = Date.now();
  req.status = 200;
  req.cdr = new Object;
  res.cookie('agentName', 'User Agent', { httpOnly: true });
  //res.header("Access-Control-Allow-Origin","http://172.16.5.239:9200");
  //res.header("Access-Control-Allow-Origin","http://localhost:8300");
  res.header("Access-Control-Allow-Origin","*");
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers','X-Requested-With,content-type');
  //res.header('Access-Control-Allow-Credentials', true);
  var namespace = cls.getNamespace(config.STORAGE.namespace);
  namespace.bindEmitter(req);
  namespace.bindEmitter(res);
  namespace.run(() => {
    next();
  });
  res.on('finish', () => {
    const end = new Date();
    logger.writeCDR(start, end, req);
  });

});
config.SESSION.store = new MongoStore({url: config.DB.url});
app.use(session(config.SESSION));

app.use(bodyParser.json({strict: false}));
app.use(bodyParser.urlencoded({extended: false}));

app.use(validator());

app.use((req, res, next) => {
  for(var item in req.body){
    req.sanitize(item).escape();
  }
  next();
});

app.use((req, res, next) => {
  req.session._garbage = Date();
  req.session.touch();
  next();
});
app.use(express.static(path.join(__dirname, 'dist')));

//app.use(favicon(path.join(__dirname, 'src/app/assets/logo/favicon.ico')));
app.use('/app/api', api);
console.log("inside api====");
app.post('/success', async (req,res,next)=>{
  console.log('Airtime Response from payment - ' + JSON.stringify(req.body));
  // success.GetPaymentDetails(req, res, aCDRParams);
  if(req.body.mtnepg_status_code == 0 || req.body.mtnepg_status_code == "0")
  res.redirect(301,'http://uat.mymtn.com.ng/success');
  else
  res.redirect(301,'http://uat.mymtn.com.ng/failure');
});

/*app.post('/buybundle/success', async (req,res,next)=> {
          console.log('==========================================================================');
          console.log('==========================================================================');
          console.log(JSON.stringify(req.body));
          var string_body = JSON.stringify(req.body);
          var body_obj = JSON.parse(string_body);
          console.log("trnx status code############", body_obj.mtnepg_status_code);
          console.log("trnx ref ############", body_obj.mtnepg_transaction_ref);

          console.log('==========================================================================');
          console.log('==========================================================================');
          if(body_obj.mtnepg_status_code == 2 || body_obj.mtnepg_status_code == '2') {
                console.log('creating db model ==> ');
                try {
                      await DebitCardPayment.findOne({ pg_trans_id: body_obj.mtnepg_transaction_ref }, (err, payResponse) => {
                      //await DebitCardPayment.findOne({ pg_trans_id: 'SMDASMWB0000002710960081249' }, (err, payResponse) => {
                        
                        if(payResponse) {
                            console.log("Pay reference#################", payResponse);
                            const payment = new DebitCardPayment({
                              'pg_trans_id': body_obj.mtnepg_transaction_ref,           
                              'transaction_status': 'Successful',
                              'transaction_type': payResponse.transaction_type
                            });
                            payment.save(function (err) {
                                if (err) {
                                  console.log("error saving payment in database", err);
                                }
                                console.log("payment successfully saved in database", err);
                                
                                res.redirect('/buybundle/success?ref='+body_obj.mtnepg_transaction_ref);
                            });

                        } else {
                            console.log("payment could not be retrieved from database error#############", err);
                            res.redirect('/buybundle/success?ref='+body_obj.mtnepg_transaction_ref);
                        }

                      })

                  } catch (e) {
                      console.log('error db model ==> ', e);
                      res.redirect('/buybundle/success?ref='+body_obj.mtnepg_transaction_ref);
                  }
          
            } else {
              try {
                  const paymentDetail = await DebitCardPayment.findOne({ pg_trans_id: body_obj.mtnepg_transaction_ref})
                  console.log('paymentDetail',paymentDetail);
                  const payment = new DebitCardPayment({
                            'pg_trans_id': body_obj.mtnepg_transaction_ref,           
                            'transaction_status': 'Failure',
                            'transaction_type': paymentDetail.transaction_type
                  });
                  const response = await payment.save()        
                  console.log('response db model ==> ', response);
                  res.redirect('/buybundlesfailure?ref='+body_obj.mtnepg_transaction_ref);
              } catch (e) {
                  console.log('error db model ==> ', e);
                  res.redirect('/buybundlesfailure?ref='+body_obj.mtnepg_transaction_ref);
              }
            }
    });*/
/*app.post('/buybundle/success', async (req,res,next)=>{
  console.log('Bundle Response from payment - ' + JSON.stringify(req.body));
  if(req.body.mtnepg_status_code == 0 || req.body.mtnepg_status_code == "0")
  res.redirect(301,'https://preprod.mymtn.com.ng/buybundle/success');
  else
  res.redirect(301,'https://preprod.mymtn.com.ng/buybundlesfailure');

  // success.GetBundlePaymentDetails(req, res, aCDRParams);
  //res.redirect('/buybundle/sucess');
});*/
   app.post('/buybundle/success', async (req,res,cdrParams,next)=>{
  console.log('==========================================================================');
  console.log('==========================================================================');
  console.log(JSON.stringify(req.body));
  var string_body = JSON.stringify(req.body);
  var body_obj = JSON.parse(string_body);
  console.log('==========================================================================');
  console.log('==========================================================================');
  console.log(body_obj);
  console.log(body_obj.mtnepg_status_code);
  if(body_obj.mtnepg_status_code == 0 || body_obj.mtnepg_status_code == '0') {
  console.log('creating db model ==> ');
  
  try {
  // const paymentDetail = await DebitCardPayment.findOne({ pg_trans_id: body_obj.mtnepg_transaction_ref,})
  // console.log('paymentDetail',paymentDetail);
  var  pg_trans_id =  body_obj.mtnepg_transaction_ref;         
  var transaction_status = 'Successful';
  // var transaction_type =  paymentDetail.transaction_type;
  //       const response = await payment.save()     
  var uniqueID = parseInt(Math.random() * 1000000000, 10);
  var updateEncodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.DEBITCARD_TABLE_NAME + "&method=updateAll&equationName=transaction_ref&equationValue=" + pg_trans_id + "&header=" + config.UPDATE_DEBITCARD_HEADER + "&values" + pg_trans_id+":::"+transaction_status);
  try {
      options = {
          method: 'GET',
          uri: updateEncodeURL,
          json: true
      }
     cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
      request.sendResponse(req, res, cdrParams, options).then(function (resp) {
          if (resp.status == 0) {
             console.log("Updated Debit card details Success status successfully")
          } else {
            console.log('Error occured while updating the debit card success status record')
          }
      })
  } catch (e) {
      console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
  }   
    console.log('response db model ==> ', response);
        //res.status(201).send({ response })
    res.redirect(301,'http://uat.mymtn.com.ng/buybundle/success?ref='+body_obj.mtnepg_transaction_ref);
                // payment.activateBundle(req, res, cdrParams);
    } catch (e) {
  console.log('error db model ==> ', e);
        //res.status(400).send(e)
    res.redirect(301, 'http://uat.mymtn.com.ng/buybundle/success?ref='+body_obj.mtnepg_transaction_ref);
    }
  
  } else {
    try {
        // const paymentDetail = await DebitCardPayment.findOne({ pg_trans_id: body_obj.mtnepg_transaction_ref})
        // console.log('paymentDetail',paymentDetail);
        // const payment = new DebitCardPayment({
        //           'pg_trans_id': body_obj.mtnepg_transaction_ref,           
        //           'transaction_status': 'Failure',
        //           'transaction_type': paymentDetail.transaction_type
        // });
        // const response = await payment.save()    
        
        var  pg_trans_id =  body_obj.mtnepg_transaction_ref;         
        var transaction_status = 'Failure';
        // var transaction_type =  paymentDetail.transaction_type;
        //       const response = await payment.save()     
        var uniqueID = parseInt(Math.random() * 1000000000, 10);
        var updateEncodeURL = encodeURI(config.MONGODB_PLUGIN_URL + uniqueID + "&tableName=" + config.DEBITCARD_TABLE_NAME + "&method=updateAll&equationName=transaction_ref&equationValue=" + pg_trans_id + "&header=" + config.UPDATE_DEBITCARD_HEADER + "&values" + pg_trans_id+":::"+transaction_status);
        try {
            options = {
                method: 'GET',
                uri: updateEncodeURL,
                json: true
            }
            cdrParams.input = options.uri.substring(options.uri.indexOf("?") + 1, options.uri.length);
            request.sendResponse(req, res, cdrParams, options).then(function (resp) {
                if (resp.status == 0) {
                   console.log("Updated Debit card Failure status successfully")
                } else {
                  console.log('Error occured while updating the debit card Failure status record')
                }
            })
        } catch (e) {
            console.log("ERROR>>>>>>>>>>>>>>>>>>", e);
        } 

        console.log('response db model ==> ', response);
        res.redirect(301, 'http://uat.mymtn.com.ng/buybundlesfailure?ref='+body_obj.mtnepg_transaction_ref);
    } catch (e) {
        console.log('error db model ==> ', e);
        res.redirect(301, 'http://uat.mymtn.com.ng/buybundlesfailure?ref='+body_obj.mtnepg_transaction_ref);
    }
  }
});
 
app.get('/*', (req, res) => {
  console.log("inside api====");
  req.session.cookie.username = 'Agent Name';
  logger.writeLog('debug', 'Headers :' + JSON.stringify(req.headers));
  if(req.protocol == "http" && req.url == '/login') {      
    // res.redirect('https://' + req.headers.host + req.url);
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  } else {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  }
}); 

app.use((req, res, next) => {
  var err = new Error('Page Not Found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  res.locals.message = err.message;
  res.locals.error = process.env.ENV == "dev" ? err : {};
  req.status = err.status || 500;
  res.status(err.status || 500);
  res.send(res.locals.error);
  next();
});

const port = serverConfig.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => {
  console.log(`MyMTNWeb is listening on Dev Environment at port:${port}`);
});
